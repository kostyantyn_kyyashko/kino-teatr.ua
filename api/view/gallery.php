<script src="/api/assets/js/masonry.js"></script>
<script src="/api/assets/js/imagesloaded.js"></script>

<?php $this->widget('ListView', array(
	'items' => $images,
	'viewFile' => '_gallery-item',
	'mainHtmlOptions' => array(
		'id' => 'gallery',
		'class' => 'list masonry js-masonry',
		'data-masonry-options' => '{ "columnWidth":".grid-sizer", "itemSelector": ".item" }',
	),
	'beforeItems' => '<div class="grid-sizer"></div>'
)); ?>

<script>
	document.getElementsByTagName('body')[0].style.overflow = 'hidden';
	$(function() {
		var container = $('#gallery');
		imagesLoaded(container, function() {
//			container.masonry({
//				gutter: '.grid-sizer',
//				itemSelector: '.item'
//			});
		});
	});

</script>