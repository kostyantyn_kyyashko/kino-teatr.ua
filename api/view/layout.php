<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="/api/assets/css/main.css" type="text/css" />
	<script src="/api/assets/js/jquery-1.10.2.min.js"></script>
</head>
<body>
	<?php echo $content; ?>
</body>
</html>