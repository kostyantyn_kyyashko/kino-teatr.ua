	<table id="film-rating">
		<thead>
			<tr>
				<th colspan="2">Фильм</th>
				<th>Рейтинг</th>
			</tr>
		</thead>
		<?php $this->widget('ListView', array(
			'items' => $ratings,
			'viewFile' => '_filmsRating',
			'mainTag' => 'tbody',
			'itemsTag' => 'tr',
		)); ?>
	</table>