<?php

class DatabaseConnection implements IDatabaseConnection {
	/**
	 * @var PDO
	 */
	private $db;

	/**
	 * @return DatabaseConnection
	 */
	public static function getInstance() {
		static $instance;
		if (null === $instance)
			$instance = new self;
		return $instance;
	}

	protected function __construct() {
		$this->init();
	}

	protected function init() {
		$this->initDatabaseConnection();
		$this->configure();
	}

	protected function initDatabaseConnection() {
		$config = Config::getInstance()->db;
		$this->db = new PDO(
			$config['dsn'],
			$config['user'],
			$config['pass']
		);
	}

	protected function configure() {
		$this->db->exec('SET CHARACTER SET utf8');
		$this->db->exec('SET NAMES utf8');
	}

	/**
	 * @param $query
	 * @throws Exception
	 */
	public function execute($query) {
		if ($this->db->exec($query) === false)
			throw new Exception("Couldn't execute query.
				Error message: " . var_export($this->db->errorInfo(), true)
				. 'Error code: ' . $this->db->errorCode()
			);
	}

	/**
	 * Execute SELECT statements.
	 * Return result as an array for compatibility with other database adapters.
	 * @param $query
	 * @return array
	 */
	public function query($query) {
		$stmt = $this->db->query($query);
		return (false !== $stmt)
			? $stmt->fetchAll(PDO::FETCH_ASSOC)
			: array();
	}

	/**
	 * @param $value
	 * @return string
	 */
	public function quote($value) {
		return $this->db->quote((string)$value);
	}

	public function __destruct() {
		$this->db = null;
	}
}