<?php

/**
 * Class Config
 *
 * Using Registry pattern.
 */
class Config {
	private $config;

	/**
	 * @return Config
	 */
	public static function getInstance() {
		static $instance;
		if (null === $instance)
			$instance = new self;
		return $instance;
	}

	/**
	 * @param $key
	 * @param $value
	 */
	public function __set($key, $value) {
		$this->config[$key] = $value;
	}

	/**
	 * @param $key
	 * @return mixed
	 * @throws Exception
	 */
	public function __get($key) {
		if (!isset($this->config[$key]))
			throw new Exception('The property has not been initialized');
		return $this->config[$key];
	}
}