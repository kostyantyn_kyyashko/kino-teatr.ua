<?php

class FilmModel {
	/**
	 * @var IDatabaseConnection
	 */
	protected $db;

	private $filmTable   = 'grifix_main_films';
	private $posterTable = 'grifix_main_films_posters';
	private $photosTable = 'grifix_main_films_photos';
	private $nameTable   = 'grifix_main_films_lng';

	public function __construct(IDatabaseConnection $db) {
		$this->db = $db;
	}

	/**
	 * @param array $filmsId
	 * @param Language $lang
	 * @return mixed
	 */
	public function getRatingByFilms(array $filmsId, Language $lang) {
		$filmsList = $this->arrayToList($filmsId);
		$langId = (int) $lang->getId();

		$query = "
			SELECT f.id, n.title as name, f.rating
			FROM {$this->filmTable} f
			INNER JOIN {$this->nameTable} n
				ON f.id = n.record_id AND n.lang_id = {$langId}
			WHERE f.id IN ($filmsList) AND f.votes > 0
			ORDER BY f.rating DESC
		";

		return $this->db->query($query);
	}

	/**
	 * @param array $films
	 * @return mixed
	 */
	public function getPosters(array $films) {
		$filmsList = $this->arrayToList($films);

		$query = "
			SELECT p.film_id, p.image
			FROM {$this->posterTable} p
			WHERE p.film_id IN ($filmsList)
		";

		return $this->db->query($query);
	}

	/**
	 * @param array $films
	 * @return mixed
	 */
	public function getPhotos(array $films) {
		$filmsList = $this->arrayToList($films);

		$query = "
			SELECT ph.film_id, ph.image
			FROM {$this->photosTable} ph
			WHERE ph.film_id IN ($filmsList)
		";

		return $this->db->query($query);
	}

	/**
	 * Convert an array of films to the list.
	 * @param array $items
	 * @return string
	 */
	protected function arrayToList(array $items) {
		$list = '';
		foreach ($items as $value) {
			if (ctype_digit($value)) { // to avoid sql-inj
				$list .= (int)$value . ',';
			}
		}
		return rtrim($list, ',');
	}
}