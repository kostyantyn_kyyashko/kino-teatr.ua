<?php

class Language {
	private $lang = null;
	private $id = null;

	public function __construct($lang) {
		if ($this->isSupported($lang))
			$this->lang = $lang;
		else
			throw new Exception('Obtained unsupported language');
	}

	/**
	 * @return int
	 * @throws Exception
	 */
	public function getId() {
		if (null === $this->id) {
			$map = $this->getMapping();
			$this->id = $map[$this->lang];
		}
		return $this->id;
	}

	/**
	 * @param string $lang
	 * @return bool
	 */
	public function isSupported($lang) {
		$map = $this->getMapping();
		return isset($map[$lang]);
	}

	/**
	 * @return mixed
	 */
	public function getMapping() {
		return Config::getInstance()->langMap;
	}
}