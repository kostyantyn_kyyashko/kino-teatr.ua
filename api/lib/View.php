<?php

class View extends AbstractView {
	/**
	 * @rewrite as static or create an View child
	 * @param string $viewFile
	 * @param array $data
	 * @param bool $return
	 * @return string
	 */
	public static function renderPartial($viewFile, $data, $return=false) {
		$view = new self;
		isset($data['this']) || $data['this'] = $view;
		$result = $view->renderInternal($viewFile, $data, $return);
		unset($view);
		return $result;
	}

	public static function render($viewFile, $data, $return=false) {
		self::renderPartial(
			'layout',
			array('content' => self::renderPartial($viewFile, $data, true)),
			$return
		);
	}
}