<?php

interface IDatabaseConnection {
	function execute($query);
	function query($query);
	function quote($value);
}