<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cheshir
 * Date: 8/16/13
 * Time: 1:18 PM
 * To change this template use File | Settings | File Templates.
 */

abstract class Widget extends AbstractView {
	public $viewFile;

	public function __construct(array $properties) {
		$class = get_class($this);
		foreach ($properties as $attr => $value) {
			if (property_exists($class, $attr)) {
				$this->$attr = $value;
			}
		}
	}

	abstract public function run();
}