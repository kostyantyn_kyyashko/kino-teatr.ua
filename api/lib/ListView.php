<?php

class ListView extends Widget {
	public $items = array();
	public $commonData = array();

	public $mainTag = 'div';
	public $mainHtmlOptions = array();

	public $itemsTag = 'div';
	public $itemHtmlOptions = array('class' => 'item');

	public $headerTag = 'h2';
	public $headerHtmlOptions = array();
	public $headerText = false;

	public $emptyTag = 'div';
	public $emptyHtmlOptions = array();
	public $emptyContent = 'Not found';

	public $beforeItems = null;

	public function run() {
		$this->openMainContainer();

		if ($this->headerText !== false)
			$this->renderHeader();

		echo (string)$this->beforeItems;

		$this->renderItems();
		$this->closeMainContainer();
	}

	protected function openMainContainer() {
		echo Html::openTag($this->mainTag, $this->mainHtmlOptions);
	}

	protected function renderHeader() {
		echo Html::openTag($this->headerTag, $this->headerHtmlOptions)
			. $this->headerText
			. Html::closeTag($this->headerTag);
	}

	protected function renderItems() {
		if (is_array($this->items) && count($this->items) > 0) {
			foreach ($this->items as $i => $item) {
				$data = array(
					'index' => $i,
					'data' => $item,
					'common' => $this->commonData,
				);

				$itemHtmlOptions = $this->mergeItemHtmlOptions($item);
				echo Html::openTag($this->itemsTag, $itemHtmlOptions);
				$this->renderInternal($this->viewFile, $data);
				echo Html::closeTag($this->itemsTag);
			}
		} elseif ($this->emptyContent) {
			$this->renderEmptyBlock();
		}
	}

	/**
	 * @param array $item
	 * @return array
	 */
	protected function mergeItemHtmlOptions(array $item) {
		return isset($item['__itemHtmlOptions'])
			? array_merge($this->itemHtmlOptions, $item['__itemHtmlOptions'])
			: $this->itemHtmlOptions;
	}

	protected function closeMainContainer() {
		echo Html::closeTag($this->mainTag);
	}

	protected function renderEmptyBlock() {
		echo Html::openTag($this->emptyTag, $this->emptyHtmlOptions)
			. $this->emptyContent
			. Html::closeTag($this->emptyTag);
	}
}