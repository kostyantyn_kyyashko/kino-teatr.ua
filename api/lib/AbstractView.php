<?php

abstract class AbstractView {
	/**
	 * Return full path to view file
	 * @param string $view
	 * @return string
	 * @throws Exception
	 */
	protected function getViewFile($view) {
		$file = APP_DIR . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR . $view . '.php';
		if (!is_file($file))
			throw new Exception('View file not found by this path: ' . $file);
		return $file;
	}

	/**
	 * We use special variable names here to avoid conflict when extracting data
	 * @param $_viewFile_
	 * @param null $_data_
	 * @param bool $_return_
	 * @return string
	 */
	public function renderInternal($_viewFile_, $_data_=null, $_return_=false) {
		$_viewFile_ = $this->getViewFile($_viewFile_);
		if(is_array($_data_))
			extract($_data_,EXTR_PREFIX_SAME,'data');
		else
			$data=$_data_;
		if($_return_) {
			ob_start();
			ob_implicit_flush(false);
			require($_viewFile_);
			return ob_get_clean();
		} else {
			require($_viewFile_);
		}
	}

	public function widget($className,$properties=array(),$captureOutput=false)
	{
		$widget = new $className($properties);
		if($captureOutput) {
			ob_start();
			ob_implicit_flush(false);
			$widget->run();
			return ob_get_clean();
		} else {
			$widget->run();
			return $widget;
		}
	}
}