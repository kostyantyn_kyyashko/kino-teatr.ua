<?php

class HttpRequest {
	public function process() {
		$config = Config::getInstance();

		$config->appLang = isset($_GET['lang'])
			? strtolower($_GET['lang'])
			: (isset($_POST['lang']))
				? strtolower($_POST['lang'])
				: $config->defaultLang;
	}

	/**
	 * @param string $key
	 * @param string $defaultValue
	 * @return string
	 */
	public static function getParam($key, $defaultValue='') {
		return isset($_GET[$key]) ? $_GET[$key] : $defaultValue;
	}

	/**
	 * Returns whether this is an AJAX (XMLHttpRequest) request.
	 * @return boolean whether this is an AJAX (XMLHttpRequest) request.
	 */
	public static function isAjaxRequest()
	{
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']==='XMLHttpRequest';
	}
}