<?php
/**
 * Simple solution for autoloader
 * @param string $class
 * @throws Exception
 */
function __autoload($class) {
	$libPath = dirname(__FILE__);
	$file = $libPath . DIRECTORY_SEPARATOR . "$class.php";

	if (file_exists($file))
		require_once($file);
	else
		throw new Exception("File with required class '$class' was not found in lib directory.");
}