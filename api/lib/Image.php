<?php

class Image {
	private $imagePath = '';
	private $fileExtension = '';
	private $width  = 0;
	private $height = 0;
	/**
	 * Use to create hash
	 * @var array
	 */
	private $additionalData = array();

	public function __construct($imagePath, array $additionalData=array()) {
		if (!is_file($imagePath))
			return; // @todo throw new Exception?

		$this->imagePath      = (string)$imagePath;
		$this->additionalData = $additionalData;
		$this->fileExtension  = $this->getExtensionFromName();

		list($this->width, $this->height, $type, $mime) = getimagesize($imagePath);
	}

	/**
	 * @return string
	 */
	protected function getExtensionFromName() {
		if (!$this->fileExtension)
			$this->fileExtension = substr($this->imagePath, strrpos($this->imagePath, '.') + 1);
		return $this->fileExtension;
	}

	/**
	 * @return string
	 */
	protected function getHashFromFile() {
		$size = filesize($this->imagePath);
		$str = $this->imagePath . serialize($this->additionalData) . $size;
		return md5($str);
	}

	public function getFileName() {
		return $this->getHashFromFile() . '.' . $this->getExtensionFromName();
	}

	public function getType() {
		$ratio = $this->width / $this->height;
		$type = null;

		if ($ratio > 0.9 && $ratio < 1.1) { // Check is square
			$type = 'square';
		} elseif ($ratio > 1.1) {
			$type = 'landscape';
		} else {
			$type = 'portrait';
		}

		return $type;
	}

	/**
	 * Resize, crop and save image
	 * @todo latter rewrite as separated methods.
	 * @param string $destination
	 * @param int|null $desiredWidth
	 * @param int|null $desiredHeight
	 * @param bool $ignoreStretched
	 * @param bool $adaptive
	 * @return string
	 */
	public function resize($destination, $desiredWidth=null, $desiredHeight=null, $ignoreStretched=false, $adaptive=true) {
		list($width, $height, $type) = getimagesize($this->imagePath);

		$originalAspect = $width / $height;

		if ($ignoreStretched) {
			// Ignore images, which have aspect ratio more then 1:2.2
			if ($originalAspect < 0.45 || $originalAspect > 2.2)
				return;
		}

		if ($adaptive) {
			if (!$desiredWidth && !$desiredHeight)
				return; // Do nothing
			elseif ((int)$desiredWidth && !$desiredHeight)
				$desiredHeight = round($height / ($desiredWidth / $width));
			elseif ((int)$height && !$desiredWidth)
				$desiredWidth =  round($width / ($desiredHeight / $height));
		}

		$targetAspect = $desiredWidth / $desiredHeight;

		$newHeight = $desiredHeight;
		$newWidth = $desiredWidth;

		if ($adaptive) {
			if ($originalAspect >= $targetAspect) {
				// If image is wider than target (in aspect ratio sense)
				$newHeight = $desiredHeight;
				$newWidth  = round($width / ($height / $desiredHeight));
			} else {
				// If the target image is wider than the original image
				$newWidth  = $desiredWidth;
				$newHeight = round($height / ($width / $desiredWidth));
			}
		}

		$types = array('','gif','jpeg','png');
		$ext = $types[$type];
		if (!$ext)
			return;

		$func = 'imagecreatefrom'.$ext;
		$img = $func($this->imagePath);

		$img_n = imagecreatetruecolor($desiredWidth, $desiredHeight);

		$offsetX = 0;
		$offsetY = 0;
		if ($adaptive) {
			// @todo Rewrite without using $newWidth and $newHeight
			$offsetX = ($newWidth  != $desiredWidth)  ? $width  - round($width / ($newWidth / $desiredWidth))  : 0;
			$offsetY = ($newHeight != $desiredHeight) ? $height - round($height / ($newHeight / $desiredHeight)) : 0;
		}

		imagecopyresampled(
			$img_n, $img,
			0, 0,
			$offsetX/2, $offsetY/2,
			$desiredWidth, $desiredHeight,
			$width-$offsetX, $height-$offsetY
		);

		$file = $destination . DIRECTORY_SEPARATOR . $this->getFileName();
		if ($type == 2) {
			return imagejpeg($img_n, $file, 75);
		} else {
			$func = 'image'.$ext;
			return $func($img_n, $file);
		}
		return $file;
	}
}