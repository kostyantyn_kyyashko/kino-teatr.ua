<?php
session_start();

function getmicrotime()
{
    list($usec, $sec) = explode(' ',microtime());
    return ((float)$usec + (float)$sec);
}

function setDateFormat()
{
	global $_cfg;
	$time_format=strftime('%X',mktime(1,2,3,4,5,2006));
	$time_format=str_replace('01','%H',$time_format);
	$time_format=str_replace('02','%M',$time_format);
	$time_format=str_replace('03','%S',$time_format);
	$date_format=strftime('%x',mktime(1,2,3,4,5,2006));

	$date_format=str_replace('04','%m',$date_format);
	$date_format=str_replace('05','%d',$date_format);
	$date_format=str_replace('2006','%Y',$date_format);
	$date_format=str_replace('06','%Y',$date_format);


	if(!$_cfg['sys::date_format'])
		$_cfg['sys::date_format']=$date_format;
	if(!$_cfg['sys::time_format'])
		$_cfg['sys::time_format']=$time_format;

	$_cfg['sys::date_time_format']=$date_format.' '.$time_format;
}

function get_map($arr,$prices,&$places,$colors,&$real_prices,$basket){
	
	$site_id = $_GET['site_id'];
	$buffer = explode(',',$prices);
	if(count($buffer) > 2){
		sort($buffer);
		foreach($buffer as $key=>$value){
			if(isset($colors[$key]))
				$show_colors[$buffer[$key]] = $colors[$key];
			else
				$show_colors[$buffer[$key]] = 'rgb(0, 102, 0)';
		}
	}
	elseif(count($buffer) == 2){
		$counter = 0;
		for($i=$buffer[0];$i<$buffer[1]+5;$i=$i+5){
			if(isset($colors[$counter]))
				$show_colors[$i] = $colors[$counter];
			else
				$show_colors[$i] = 'rgb(0, 102, 0)';
			$counter++;
		}
	}
	else{
		$show_colors[$buffer[0]] = $colors[0];
	}
	$q = 0.4;
	$html = '';
	ob_start();
	foreach($arr[0] as $key=>$value){
//		echo '<h1>Key '.$key.'</h1><br>';
		if($key != 'sectors')
			continue;
		
		foreach($value[0] as $subkey=>$subvalue){
			if($subkey != 'rows')
				continue;
			if(is_array($subvalue)){
				foreach($subvalue as $key2=>$value2){
					if(is_array($value2)){
						foreach($value2 as $key3=>$value3){
							if($key3 != 'places')
								continue;
							if(is_array($value3)){
								foreach($value3 as $key4=>$value4){
									
									$end_value = $value4;
									$row = $key4 + 1;
									$price = $end_value['prices'][0]['price']/100;
									if(isset($show_colors[$price]))
										$color = $show_colors[$price];
									else
										$color = 'rgb(0, 102, 0)';

									$end_value['x'] *= $q;
									$end_value['y'] *= $q;
									$end_value['width'] *= $q;
									$end_value['height'] *= $q;
									$real_prices[$price] = $color;
									$status = $end_value['status'];
									if($status == 1)
										$title = '. В корзине';
									elseif($status == 3)
										$title = '. Выкуплено';
									elseif($status == 2)
										$title = '. Забронировано';
									else
										$title = '';
									
									
									if(isset($basket[$site_id][$end_value['placeId']])){
//										$color = '#5bb9d4';
										echo '<a href="#" onclick="set_place(this);return false;" class="wg-places wg-place-'.$end_value['placeId'].' wg-places-status'.$status.' " data-price-id="'.$end_value['prices'][0]['priceId'].'" data-id="'.$end_value['placeId'].'" data-number="'.$end_value['number'].'" title="Ціна: '.$price.' грн., , Ряд: '.$row.', Місце: '.$end_value['number'].$title.'" style="background-color: '.$color.'; width: '.$end_value['width'].'px; height: '.$end_value['height'].'px; top: '.$end_value['y'].'px; left: '.$end_value['x'].'px; font-size: 9.56586px;" data-check="1" data-row="'.$row.'" data-price="'.$price.'" data-color="'.$color.'">
                        <p>'.$end_value['number'].'</p>
                    </a>';
									}
									else{
										echo '<a href="#" onclick="set_place(this);return false;" class="wg-places wg-place-'.$end_value['placeId'].' wg-places-status'.$end_value['status'].' " data-price-id="'.$end_value['prices'][0]['priceId'].'" data-id="'.$end_value['placeId'].'" data-number="'.$end_value['number'].'" title="Ціна: '.$price.' грн., , Ряд: '.$row.', Місце: '.$end_value['number'].$title.'" style="background-color: '.$color.'; width: '.$end_value['width'].'px; height: '.$end_value['height'].'px; top: '.$end_value['y'].'px; left: '.$end_value['x'].'px; font-size: 9.56586px;" data-check="0" data-row="'.$row.'" data-price="'.$price.'" data-color="'.$color.'">
                        <p>'.$end_value['number'].'</p>
                    </a>';
									}
									
									$places++;

								}
							}
						}
					}

				}
			}
		}
		
	}
	$html .= ob_get_contents();
	ob_clean();
	return $html;
}

function eventmap_widget($session_id){
	global $_user;
	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('main::payment');
	sys::useLib('main::basket');
	sys::useLib('sys::form');
	sys::useLib('main::megakino');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('cinema_id','int');
	$cache_name='eventmap_widget_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'.dat';
	if($_SERVER['HTTP_HOST'] == 'kino-teatr.ua'){
//		if($cache=sys::getCache($cache_name,$_cfg['main::cinema_shows_page_cache_period']*_HOUR/12)) // раз в 10 минут
//			return unserialize($cache);
	}
	$show_id = (int)$_GET['show_id'];
	$time_id = (int)$_GET['time_id'];
	$lang_id = (int)$_GET['lang_id'];
	$date = $_GET['date'];
	$site_id = $_GET['site_id'];
	$langs = array(
	1 => 'ru',
	2 => 'en',
	3 => 'uk'
	);
	$q="
		SELECT
		 shw.begin, 
		 shw.end, 
		 shw.film_id, 
		 shw.id AS `show_id`, 
		 shw.partner_id, hls_lng.title AS `hall`, 
		 hls.cinema_id, hls.id AS `hall_id`, 
		 hls.scheme, hls.`3d`, 
		 flm.year, flm.age_limit, 
		 flm.`3d` as `film_3d`, 
		 flm_lng.title AS `film`, 
		 shw.hall_id, 
		 cl.title AS `cinema_name`, 
		 cp.image AS image_cinema, 
		 cl.address, 
		 cc.title AS city, 
		 ev.event_id, 
		 ev.site_id, 
		 ev.response
 	FROM 
 		`#__main_shows` AS `shw`
	LEFT JOIN `#__main_cinemas_halls` AS `hls` 
	ON hls.id=shw.hall_id
	
	LEFT JOIN `#__main_films_lng` AS `flm_lng` 
	ON flm_lng.record_id=shw.film_id AND flm_lng.lang_id=$lang_id
	
	LEFT JOIN `#__main_films` AS `flm` 
	ON flm.id=shw.film_id
	
	LEFT JOIN `#__main_shows_eventmap` AS `ev` 
	ON ev.show_id=shw.id
	
	LEFT JOIN `#__main_cinemas_lng` AS `cl` 
	ON cl.record_id = hls.cinema_id AND cl.lang_id = $lang_id
	
	LEFT JOIN `#__main_cinemas` AS `cnm` 
	ON cnm.id = hls.cinema_id
	
	LEFT JOIN `#__main_cinemas_photos` AS `cp` 
	ON cl.record_id = cp.cinema_id
	
	LEFT JOIN `#__main_countries_cities_lng` AS `cc` 
	ON cc.record_id = cnm.city_id AND cc.lang_id = $lang_id
	
	LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng` 
	ON hls_lng.record_id=hls.id AND hls_lng.lang_id=$lang_id
	
	WHERE 
		shw.id = $show_id";
//if(sys::isDebugIP()) $_db->printR($q);	
		$r=$_db->query($q);
		$result=$_db->fetchAssoc($r);
		$a_dt_begin = explode("-", $result['begin']);
		$a_dt_end = explode("-", $result['end']);
		$dt_begin   = mktime(0,0,0,	$a_dt_begin[1], $a_dt_begin[2], $a_dt_begin[0]);
		$dt_end     = mktime(0,0,0,	$a_dt_end[1], 	$a_dt_end[2], 	$a_dt_end[0]);
		$dtx=$dt_begin;
		$result['date'] = $date;
		$film_id = (int)$result['film_id'];
		$sql = "SELECT image FROM `#__main_films_posters` WHERE film_id = $film_id ORDER BY shows DESC LIMIT 1";
		$res_image = $_db->query($sql);
		if($res_image){
			$result_image = $_db->fetchAssoc($res_image);
			$result['image'] = $result_image['image'];
		}
		$qq = "SELECT time,prices,sale_status FROM `#__main_shows_times` WHERE show_id = $show_id AND id = $time_id";
		$r2=$_db->query($qq);

		if($_db->numRows($r2) == 1){
			$result_time=$_db->fetchAssoc($r2);
			$result['time'] = $result_time['time'];
			$result['prices'] = $result_time['prices'];
        $temp_st=explode('|', $result_time['sale_status']);
        if($temp_st[0]=='megakino' and $temp_st[1]!='' and $temp_st[2] != '') {
            $result['site_id'] = $temp_st[1];
            $result['event_id'] = $temp_st[2];
        }
		}
		else{
			while ($arr=$_db->fetchAssoc($r2))
			{
				$result['time'] = $arr['time'];
				$result['prices'] = $arr['prices'];
            $temp_st=explode('|', $arr['sale_status']);
            if($temp_st[0]=='megakino' and $temp_st[1]!='' and $temp_st[2] != '') {
                $result['site_id'] = $temp_st[1];
                $result['event_id'] = $temp_st[2];
            }
				break;
			}
		}
		$eventmap = get_eventmap($session_id,$result['site_id'],$result['event_id']);
//		$arr = json_decode($result['response'],true);
		if($eventmap)
			$arr = json_decode($eventmap,true);
		else
			$arr = json_decode($result['response'],true);
		$result['background_map'] = $arr[0]['image'];
		$result['background_width'] = $arr[0]['width'];
		$places = 0;
		$colors = array('rgb(155, 7, 2)','rgb(192, 16, 11)','rgb(152, 2, 156)','rgb(198,7,203)','rgb(233,43,238)');
		$real_prices = array();
//		if(isset($_SESSION['session_id'])){
//			$session_id = $_SESSION['session_id'];
		$basket = basket_list($session_id);
//		}
		$result['map'] = get_map($arr,$result['prices'],$places,$colors,$real_prices,$basket);
		$result['show_prices'] = get_prices($result['prices'],$colors,$real_prices);
		$result['places'] = $places;
		$result['site_id'] = $site_id;
		$result['cards_box'] = main_payment::getListCards();
		$result['basket'] = get_basket($basket,$result['cards_box']);
//		$result['basket'] = main_basket::get_basket($basket);
		$result['user_mail'] = $_user['email'];
		$result['username'] = $_user['main_first_name'];
		$result['user_phone'] = $_user['main_phone'];
		$result['add_url'] = '/'.$langs[$lang_id].'/main/user_profile_payment/user_id/'.$_user['id'].'.phtml';
		
		$result['lang_id'] = $lang_id;
		$html = sys::parseTpl('main::_megakino_widget',$result);
//		$html = get_html_widget($result);
//		echo $html;exit;
//		sys::setCache($cache_name,serialize($result));
		$response['html'] = $html;
		$response['places'] = $arr[0]['width'];
		echo json_encode($response);
		exit;
	
}

function get_eventmap($session_id,$site_id,$event_id){
	$site_id = trim($site_id);
	$event_id = trim($event_id);
	$map_url = 'http://server.megakino.com.ua/gate/eventmap?sessionid='.$session_id.'&eventId='.$event_id.'&siteId='.$site_id;
	$buffer = file_get_contents($map_url);
	return $buffer;
}

function unauthorized_widget(){
	
	$html = sys::parseTpl('main::_unauthorized_widget');
	echo $html;
	exit;
}

function basket_list($session_id){
	$url = 'http://server.megakino.com.ua/gate/basket-list';
	$response = file_get_contents($url.'?sessionid='.$session_id);
	$buffer = json_decode($response,true);
	$arr = array();
	foreach($buffer['items'] as $item){
		$arr[$item['siteId']][$item['placeId']] = $item;
	}
	return $arr;
}

function get_basket($items,$cards_box){
	$rows = array();
	$html = '';
	$sum = 0;
	$counter = 0;
	if(!count($items))
		return $html;
	foreach($items as $item){
		foreach($item as $value){
			$rows[$value['rowNumber']][$value['placeNumber']] = $value;
			$sum += $value['price']/100;
			$counter++;
		}
	}
	$buffer_comission = main_payment::CalcPaymentAmount($sum*100);
	$buffer_comission = json_decode($buffer_comission,true);
	if(isset($buffer_comission['response']['error'])){
			
	}
	else{
		$sum_comission = $buffer_comission['response']['amount']/100;
	}
		$html .= '<table>';
		if($cards_box){
			if(isset($sum_comission)){
				$html .= '<tr><td colspan="2"><div>'.count($items).' бiлета - '.$sum.' грн.  (при оплате картой '.$sum_comission.' грн)</td></tr>';
				$html .= '<tr><td colspan="2" align="right"><a href="#" onclick="basket_clean(this);return false;" style="text-decoration: underline;" class="clean">Очистити кошик</a></td></tr>';
			}
			else{
				$html .= '<tr><td><div>'.count($items).' бiлета - '.$sum.'грн.</td><td align="center"><a href="#" onclick="basket_clean(this);return false;" style="text-decoration: underline;" class="clean">Очистити кошик</a></td></tr>';
			}
			
		
			foreach($rows as $row=>$value){
				$html .= '<tr>';
				$html .= '<td></td>';
				$html .= '<td>ряд '.$row;
				$html .= ' мiсце ';
				$count = 0;
				foreach($value as $subkey=>$subvalue){
					$html .= $subkey.'<a href="#" onclick="remove_basket(this,'.$subvalue['placeId'].');return false;" style="margin-left: 2px;"><img src="/mods/main/skins/default/images/failure.png" width="8" height="8" /></a>';	
					if(count($value)-1 != $count)
						$html .= ', ';
					$count++;
				}
		
				$html .= '</td>';
				$html .= '</tr>';
			}
		}
		else{
			$html .= '<tr><td><div>'.count($items).' бiлета - '.$sum.' грн.  </td><td><a href="#" onclick="basket_clean(this);return false;" style="text-decoration: underline;" class="clean">Очистити кошик</a></td></tr>';
			foreach($rows as $row=>$value){
				$html .= '<tr>';
				$html .= '<td></td>';
				$html .= '<td>ряд '.$row;
				$html .= ' мiсце ';
				$count = 0;
				foreach($value as $subkey=>$subvalue){
					$html .= $subkey.'<a href="#" onclick="remove_basket(this,'.$subvalue['placeId'].');return false;" style="margin-left: 2px;"><img src="/mods/main/skins/default/images/failure.png" width="8" height="8" /></a>';	
					if(count($value)-1 != $count)
						$html .= ', ';
					$count++;
				}
		
				$html .= '</td>';
				$html .= '</tr>';
			}
		}
		
		
		$html .= '</table>';
		return $html;
}

function get_prices($prices,$colors,$real_prices){
	$buffer = explode(',',$prices);
	foreach($buffer as $key=>$value){
		$buffer[$key] = trim($value);
	}
	$html = '';
	if(count($buffer) > 2){
		sort($buffer);
		foreach($buffer as $key=>$value){
			$html .= '
		<li>
                <div style="background-color:'.$colors[$key].'" class="wg-chair-color"></div>
                <div class="wg-price">'.trim($buffer[$key]).' грн</div>
            </li>	
		';
		}
	}
	elseif(count($buffer) == 2){
		$counter = 0;
		for($i=$buffer[0];$i<$buffer[1]+5;$i=$i+5){
			if(empty($real_prices[$i]))
				continue;
			$html .= '
		<li>
                <div style="background-color:'.$real_prices[$i].'" class="wg-chair-color"></div>
                <div class="wg-price">'.$i.' грн</div>
            </li>	
		';
		$counter++;
		}
	}
	else{
		$html .= '
		<li>
                <div style="background-color:'.$colors[0].'" class="wg-chair-color"></div>
                <div class="wg-price">'.$buffer[0].' грн</div>
            </li>	
		';
	}
	return $html;
}

define('_MODE','frontend');
$start=getmicrotime();

//Подключить файл конфигурации
if(_MODE=='backend')
	require_once('../cfg.php');
elseif(_MODE=='frontend')
	require_once('cfg.php');
//=====================================//

//Подключить библиоткеку системных функций и класс работы с БД

require_once(_ROOT_DIR.'mods/sys/include/lib.sys.php');
require_once(_ROOT_DIR.'mods/sys/include/cls.db.php');

//Подключить основные библиотеки
if(_MODE=='backend')
	sys::useLib('sys::gui');
elseif(_MODE=='frontend')
	sys::useLib('main');
	
//=========================================//

$_db=new cls_sys_db();//создать объект для работы с БД
$_db->query("SET NAMES 'UTF8'"); //задать кодировку данных БД

//получить e-mail суперпользователя
$_cfg['sys::root_email']=$_db->getValue('sys_users','email',7);

//Стартовать сессию
if($_cfg['sys::session_in_db'])
	require_once(_ROOT_DIR.'mods/sys/include/lib.sess.php');
else
	session_save_path(_SESSION_DIR);
session_name($_cfg['sys::project_id'].'_'._MODE);
session_cache_expire($_cfg['sys::session_minutes']);
session_start();
//======================================================================//

//Проверить айпишник пользователя
if(isset($_SESSION['sys::user_ip']) && $_SESSION['sys::user_ip']!=$_SERVER['REMOTE_ADDR'])
	unset($_SESSION);
//====================================//

//Загрузить настройки модулей
$r=$_db->Query("SELECT CONCAT(mods.name, '::', cfg.name) AS `name`,
		cfg.value
		FROM `#__sys_mods_cfg` AS `cfg`
		LEFT JOIN `#__sys_mods` AS `mods`
		ON cfg.mod_id=mods.id");
while($cfg = $_db->fetchArray($r))
{
	$_cfg[$cfg['name']]=$cfg['value'];
}
unset($cfg);
if(_MODE=='backend') //Если админ панель
	$_cfg['sys::hurl']=false;//Отключить ЧПУ
	

//================================================//

//Загрузить системные языки
$r=$_db->query("SELECT `id`, `code`, `title`, `short_title`
		FROM `#__sys_langs`
		WHERE `on`=1
		ORDER BY `order_number`");
$_langs=array();
while($lang=$_db->fetchAssoc($r))
	$_langs[$lang['id']]=$lang;

$lang_id = (int)$_GET['lang_id'];
//Загрузить переменные языка
$r=$_db->query("SELECT * FROM `#__sys_langs`
					WHERE `id`=$lang_id");
$lang=$_db->fetchAssoc($r);
$_cfg['sys::lang_id']=$lang['id'];
$_cfg['sys::from']=$lang['robot_title'];
$_cfg['sys::mail_charset']=$lang['mail_charset'];
$langs = array(
1 => 'ru',
2 => 'en',
3 => 'uk'
);
$_cfg['sys::lang'] = $langs[$lang_id];


//Проверка флага первого визита
if(!isset($_SESSION['sys::user_id']))
	$_first_visit=true;
//===================================================//

//Определить включены ли кукисы
if(isset($_COOKIE['sys::test']))
	$_cookie=true;
sys::setCookie('sys::test',1);
//==============================================================================//

//Авторизация по кукисам
if(_MODE=='frontend' && (!isset($_SESSION['sys::user_id']) || $_SESSION['sys::user_id']==2))
{
	if(isset($_COOKIE['sys::login']) && isset($_COOKIE['sys::password']))
	{
		sys::authorizeUser($_COOKIE['sys::login'],$_COOKIE['sys::password'],true);
	}
}
//Проверка пользователя
if(!sys::checkSessionUser())	//Если пользователь не прошел проверку
	sys::registerUser(2);		//Загегистрировать в системе гостя

 sys::useLang('main'); //Подключить языковой файл модуля
 $url = 'http://server.megakino.com.ua/gate/'.$method;
$author_url = 'http://server.megakino.com.ua/gate/login';

 $author_params = 'username=kino-teatr.ua&password=vdfv78464rwcs5g34';
// stub();
// var_dump($_SESSION);
if(empty($_SESSION['session_id']) || time() > $_SESSION['session_expire']){
	if( $curl = curl_init() ) {
    	curl_setopt($curl, CURLOPT_URL, $author_url);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    	curl_setopt($curl, CURLOPT_POST, true);
    	curl_setopt($curl, CURLOPT_POSTFIELDS, $author_params);
    	$out = curl_exec($curl);
		curl_close($curl);
		$session_id = trim($out);
		if($session_id){
			$_SESSION['session_id'] = $session_id;
			$_SESSION['session_expire'] = time() + (60 * 15);
		}
//		echo 'session_id '.$session_id.'<br>';
	}
}
else{
	$session_id = $_SESSION['session_id'];
}

if(isset($_REQUEST['unauthorized'])){
	unauthorized_widget();
}
//$_SESSION['url_3ds'] = '/eventmap.php?lang_id='.$_REQUEST['lang_id'].'&cinema_id='.$_REQUEST['cinema_id'].'&site_id='.$_REQUEST['site_id'].'&show_id='.$_REQUEST['show_id'].'&time_id='.$_REQUEST['time_id'].'&date='.$_REQUEST['date'];
$_SESSION['url_error_3ds'] = $_REQUEST['current_url'];
 eventmap_widget($session_id);
 
function stub(){
	$response['html'] = '<p>Виджет продажи билетов временно не работает</p>';
	$response['places'] = 0;
	echo json_encode($response);
	exit;
}

?>