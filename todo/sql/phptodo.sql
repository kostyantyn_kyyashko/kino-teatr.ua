-- MySQL dump 8.21
--
-- Host: localhost    Database: kinoteatr
---------------------------------------------------------
-- Server version	3.23.49

--
-- Table structure for table 'todo'
--

CREATE TABLE todo (
  todo_id int(10) unsigned NOT NULL auto_increment,
  todo_short varchar(255) NOT NULL default '',
  todo_long text NOT NULL,
  todo_priority tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (todo_id)
) TYPE=MyISAM;

--
-- Dumping data for table 'todo'
--



