<?php

// phpToDo
// Copyright (c) 2002 Mattias Nordstrom <matta@ftlight.net>
// $Id: index.php,v 1.7 2002/06/20 20:05:47 mnordstr Exp $

// For more info and new releases go to http://php-todo.sourceforge.net/

// This software is distributed under the GNU GPL.
// http://www.gnu.org/licenses/gpl.html#SEC1

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

$priority = array();

// -----> CONFIGURATION BEGINS HERE <-----

// The database must be set up correctly before using phpToDo.
// Please check your MySQL manual for more info.
$mysql_host = '91.194.251.253';
$mysql_db = 'grifix_kino_v2';
$mysql_user = 'kinoteatrua';
$mysql_passwd = 'aelaevoQu0le';

// The recommended password should be an MD5 encrypted password.
// However, you can specify a cleartext password by uncommenting the line.

// MD5 encrypted password.
$passwd = '045b9e4d8b96dce053950297a8a39665';

// Cleartext password.
// $passwd = md5('cleartextpasswordhere');

// Colors used by CSS can now (0.1.1+) be found in style.css.
// If you want to modify them, look in the source for the place you want to modify
// and modify that class in style.css. As of version 0.1.1 phpToDo is XHTML 1.0 with
// styles 100% CSS2.

// Priorities. The default priority is 3, 1 is most important.
$priority[1] = 'Срочно';
$priority[2] = 'Не срочно';
$priority[3] = 'Нормально';
$priority[4] = 'Как нибудь';
$priority[5] = 'Идея';

// -----> END OF CONFIGURATION <-----


error_reporting(E_ALL);
$version = '0.1.1';
$file = 'index.php';

ob_start();


?>
<?php print "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n" ?>
<!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<title>phpToDo</title>
<link href="/todo/favicon.ico" rel="shortcut icon" type="image/x-icon" />

<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />

<script type="text/javascript">

	function doFocus()
	{
		if (document.form.todo)
			document.form.todo.focus();

		if (document.form.password)
			document.form.password.focus();
	}

	function delTodo(id)
	{

		var msg = "Are you sure you want to delete this Todo?";

		if (confirm(msg))
			location.replace("<?php echo $file ?>?delete=" + id);

	}

</script>

<link href="style.css" rel="stylesheet" title="default" type="text/css" />

</head>
<body onload="doFocus();">

<table class="contentTable">
	<tr>
		<td class="headerTD" style="width: 6%;">&nbsp;</td>
		<td class="headerTD" style="width: 42%; text-align: left;"><a href="<?php print 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] ?>"><font class="pageHeader">phpToDo</font></a></td>
		<td class="headerTD" style="width: 4%;">&nbsp;</td>
		<td class="headerTD" style="width: 42%; text-align: right;"><a href="<?php echo $file ?>?forget=yes"><font class="forgetMe">Forget Me</font></a></td>
		<td class="headerTD" style="width: 6%;">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="5" class="contentTD">
		<br /><center>

<?php

$script_path_array = explode('/', $_SERVER['PHP_SELF']);
$script_path = '';
for ($i=0; $i<count($script_path_array)-1; $i++)
	$script_path .= $script_path_array[$i] . '/';
unset($script_path_array);

if (!empty($_REQUEST['password']))
{
	if (md5($_REQUEST['password']) == $passwd)
	{
		ob_end_clean();
		setcookie('phptodo', md5($passwd . 'phptodo'), time()+31556000, $script_path, $_SERVER['HTTP_HOST'], 0);
		header("Location: $file");
	}
	else
		$login_fail = 'Password incorrect!';
}

mysql_connect($mysql_host, $mysql_user, $mysql_passwd);
mysql_select_db($mysql_db);
####mim
mysql_query("SET NAMES 'utf8'");
#####

if ((!empty($_COOKIE['phptodo'])) && ($_COOKIE['phptodo'] == md5($passwd . 'phptodo')))
{
	// Always set the cookie again to be active for one year.
	setcookie('phptodo', md5($passwd . 'phptodo'), time()+31556000, $script_path, $_SERVER['HTTP_HOST'], 0);

	if ((!empty($_REQUEST['forget'])) && ($_REQUEST['forget'] == 'yes'))
	{
		setcookie('phptodo', md5($passwd . 'phptodo'), time()-3600, $script_path, $_SERVER['HTTP_HOST'], 0);
		print "Bye, bye! <form name='form' action='' method='get'><input type='hidden' name='bye' /></form>";
		$no_list = 1;
	}

	// No more cookie games after this.
	ob_end_flush();

	if (!empty($_REQUEST['delete']))
		if (is_numeric($_REQUEST['delete']))
			mysql_query("DELETE FROM todo WHERE todo_id=" . $_REQUEST['delete']);

	if ((!empty($_REQUEST['action'])) && ($_REQUEST['action'] == 'add') && (!empty($_REQUEST['todo'])))
	{
		$sql = "SELECT * FROM todo WHERE todo_priority=" . strip_tags($_REQUEST['priority']) . " &&
			todo_short='" . strip_tags($_REQUEST['todo'], '<b>,<i>,<u>,<li>') . "' &&
			todo_long='" . strip_tags($_REQUEST['description'], '<b>,<i>,<u>,<li>') . "'";

		$sql_result = mysql_query($sql);
		
		if (!mysql_num_rows($sql_result))
		mysql_query("
			INSERT INTO todo SET
			todo_priority=" . strip_tags($_REQUEST['priority']) . ",
			todo_short='" . strip_tags($_REQUEST['todo'], '<b>,<i>,<u>,<li>') . "',
			todo_long='" . strip_tags($_REQUEST['description'], '<b>,<i>,<u>,<li>') . "'
			");
	}

	if ((!empty($_REQUEST['action'])) && ($_REQUEST['action'] == 'edit') && (!empty($_REQUEST['todo'])))
	{
		mysql_query("
			UPDATE todo SET
			todo_priority=" . strip_tags($_REQUEST['priority']) . ",
			todo_short='" . strip_tags($_REQUEST['todo'], '<b>,<i>,<u>,<li>') . "',
			todo_long='" . strip_tags($_REQUEST['description'], '<b>,<i>,<u>,<li>') . "'
			WHERE todo_id=" . strip_tags($_REQUEST['edit_id'])
			);
	}

	if (empty($no_list))
	{
		$post_action = 'add';
		$post_pr[1] = '';
		$post_pr[2] = '';
		$post_pr[3] = " selected='selected'";
		$post_pr[4] = '';
		$post_pr[5] = '';
		$post_todo = '';
		$post_description = '';
		$post_button = 'Add';

		if ((!empty($_REQUEST['edit'])) && (is_numeric($_REQUEST['edit'])))
		{
			$sql_result = mysql_query("SELECT * FROM todo WHERE todo_id=" . $_REQUEST['edit']);
			if (mysql_num_rows($sql_result))
			{
				$row = mysql_fetch_array($sql_result);
				$post_action = 'edit';

				$post_pr[3] = '';
				$post_pr[$row['todo_priority']] = " selected='selected'";

				$post_todo = $row['todo_short'];
				$post_description = $row['todo_long'];
				$post_button = 'Update';
			}
			else
			{
				print 'No Todo item found by that ID!<br /><br />';
			}
		}

		?>

		<form name="form" action="<?php echo $file ?>" method="post">
		<input type="hidden" name="action" value="<?php echo $post_action ?>" />
		<input type="hidden" name="edit_id" value="<?php echo @$_REQUEST['edit'] ?>" />
		<table class="inputTable">
		<tr style="text-align: left">
			<td style="width: 100px">Priority:</td>
			<td style="width: 300px">
				<select name="priority">
					<option value="1"<?php echo $post_pr[1] ?>><?php echo $priority[1] ?></option>
					<option value="2"<?php echo $post_pr[2] ?>><?php echo $priority[2] ?></option>
					<option value="3"<?php echo $post_pr[3] ?>><?php echo $priority[3] ?></option>
					<option value="4"<?php echo $post_pr[4] ?>><?php echo $priority[4] ?></option>
					<option value="5"<?php echo $post_pr[5] ?>><?php echo $priority[5] ?></option>
				</select>
			</td>
		</tr>
		<tr style="text-align: left">
			<td>Todo:</td>
			<td><input type="text" name="todo" size="40" maxlength="255" value="<?php echo $post_todo ?>" /></td>
		</tr>
		<tr style="text-align: left">
			<td style="vertical-align: top">Description:</td>
			<td><textarea name="description" rows="4" cols="40"><?php echo $post_description ?></textarea></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center"><input type="submit" name="add" value="<?php echo $post_button ?>" /></td>
		</tr>
		</table>
		</form><br />

		<?php

		$todos = 0;

		function modifyURL(&$string)
		{
			if (!empty($string))
			{
				$pos = 0;
				$pos2 = 0;
				$pos = strpos($string, 'http://', $pos2);

				while ($pos !== false)
				{
					$pos2 = strpos($string, ' ', $pos);
					if ($pos2 === false)
						$url = substr($string, $pos, (strlen($string)-$pos));
					else
						$url = substr($string, $pos, $pos2 - $pos);

					$string = str_replace($url, "<a href='$url'>$url</a>", $string);

					if ($pos2 === false)
					{
						$pos = false;
						continue;
					}

					$pos = strpos($string, 'http://', $pos2+strlen($url)+14);
				}
			}
		}

		for ($cur_pri=1; $cur_pri<6; $cur_pri++)
		{
			$sql_result = mysql_query("SELECT * FROM todo
					WHERE todo_priority=$cur_pri
					ORDER BY todo_id ASC");
			if (!mysql_num_rows($sql_result))
				continue;

			?>

			<table class="listTable">
			<tr>
				<td colspan="3" class="priority"><?php echo $priority[$cur_pri] ?></td>
			</tr>

			<?php

			$list = 2;
			while ($row = mysql_fetch_array($sql_result))
			{
				$todos++;
				$list = ($list == 1) ? 2 : 1;

				$desc = nl2br($row['todo_long']);
				modifyURL($desc);

				modifyURL($row['todo_short']);

				print "\t\t\t<tr class='list$list'>\n\t\t\t\t<td class='list1' style='width: 30px'>&nbsp;</td>\n\t\t\t\t" .
					"<td class='list$list' style='width: 75px; vertical-align: top'><a href='" . $file . "?edit=" . $row['todo_id'] .
					"'><img src='img/edit.png' border='0' alt='Edit' title='Edit' /></a>&nbsp;&nbsp;" .
					"<a href='javascript:delTodo(" . $row['todo_id'] . ")'>" .
					"<img src='img/delete.png' border='0' alt='Delete' title='Delete' /></a></td>\n\t\t\t\t" .
					"<td class='listLastTD$list'><b>" . $row['todo_short'] . "</b></td>\n\t\t\t</tr>\n\t\t\t";

				print "<tr class='list$list'>\n\t\t\t\t<td colspan='2' class='list1' style='text-align: right'></td>" .
					"\n\t\t\t\t<td class='listLastTD$list'>" .
					$desc . "</td>\n\t\t\t</tr>\n\n";
			}

			print "\t\t\t</table><br />";
		}

		if (!$todos)
			print 'No Todos found.<br />';
	}
}
else
{
ob_end_flush();

if (!empty($login_fail))
	print "<font class='error'>$login_fail</font> <br /><br />";

?>

<form name="form" action="<?php echo $file ?>" method="post">
<input type="password" name="password" size="10" /><br />
<input type="submit" name="login" value="Login" />
</form>

<?php
}

print "\n\n\t<br /><br /><center><a href='http://php-todo.sourceforge.net/'>phpToDo</a> $version - Copyright &copy; 2002 Mattias Nordstrom</center>\n";

?>

</center>
</td></tr></table>

</body></html>
