<?php
include "mods/main/libs/Barcode39.php";
if(empty($_REQUEST['barcode']))
	exit;
$barcode = $_REQUEST['barcode'];
// set Barcode39 object
$bc = new Barcode39($barcode);
if(isset($_GET['text_size']))
	$bc->barcode_text_size=(int)$_GET['text_size'];
if(isset($_GET['height']))
	$bc->barcode_height=(int)$_GET['height'];
if(isset($_GET['width']))
	$bc->barcode_width=(int)$_GET['width'];
// display new barcode
$bc->draw();

?>