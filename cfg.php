<?php
error_reporting(E_ALL);

if(basename($_SERVER['PHP_SELF'])=='cfg.php')
	die();

define('_HOUR',3600);
define('_DAY',_HOUR*24);
define('_KB',1024);
define('_MB',_KB*1024);

//Задать кодировку utf-8--------------------------------------------------------
#ini_set('mbstring.internal_encoding', 'UTF-8');
mb_regex_encoding('UTF-8');
//==============================================================================

//Инициализировать глобальные переменные----------------------------------------
global $_db; $_db=false;//Объект для работы с БД
global $_cfg; $_cfg=array();//Массив настроек
global $_debug; $_debug=array();//Массив с отладочной информацей
global $_lng; $_lng=array();//Массив языковых переменных
global $_rights; $_rights=array();//Массив прав доступа текущего пользователя
global $_err; $_err=false;//пользовательская ошибка
global $_var;//Массив переменных главного шаблона
global $_msg; $_msg=false;//сообщение для польователя
global $_errors; $_errors=array();//Массив ошибок
global $_cache; $_cache=false;//Кэш
global $_user; $_user=array();//Данные о текущем пользователе
global $_mods; $_mods=array();//Массив включенных модулей
global $_langs; $langs=array();//Ма ссив языков системы
global $_meta_keywords; $_meta_keywords=false;//Содержимое мета-тэга с ключевыми словами
global $_meta_description; $_meta_description=false;//Содержимое мета-тэга описания
global $_meta_other; $_meta_other=false;//Другие мета тэги
global $_meta_title; $_meta_title=false;//Заголовок страницы
global $_title; $_title=false;//Заголовок сайта
global $_root_title; $_root_title=false;//Корневой заголовок сайта
global $_js_footer; $_js_footer=false;//Кода java-script который выполняется после загрузки страницы
global $_js_header; $_js_header=false;//Кода java-script который выполняется перед загрузкой страницы
global $_js_include; $_js_include=array();//Список подключаемых js-библиотек
global $_tpl;//Главный шаблон
global $_first_visit; $_first_visit=false; //Флаг первого визита на сайт
global $_cookie; $_cookie=false; //Флаг вклченнык кукисов
global $_hotkeys; $_hotkeys=array(); //Массив горячих клавиш
global $_charsets; //Массив кодировок
//==============================================================================

//Кодировки-----------------------------------------------------------
$_charsets=array(
		'windows-1251'=>'Cyrillic(windows)',
		'koi8-r'=>'Cyrillic(KOI8-R)',
		'koi8-u'=>'Cyrillic(KOI8-U)',
		'cp866'=>'Cyrillic(DOS)',
		'iso-8859-5'=>'Cyrillic(ISO)',
		'maccyrillic'=>'Cyrillic(Mac)',
		'utf-8'=>'Unicode(UTF-8)'
	);
//===============================================================
	
/*Инициализировать дебаг переменные*/
$_debug['sql::time']=0;//Общее время выполнения запросов к БД
$_debug['sql::number']=0;//Количество запросов к БД
$_debug['sql::explain']=array();//Массив эксплейнов БД
//==============================================================//

//Инициализировать конфигрурационные переменные---------------------------------
$_cfg['sys::hurl']=true;//ЧПУ
$_cfg['sys::date_format']=false;//Формат даты
$_cfg['sys::date_time_format']=false;//Формат данных для вывода даты и времени
$_cfg['sys::time_format']=false;//Формат данных для вывода даты и времени
$_cfg['sys::on_page']=false;//Кол-во объектов на странице
$_cfg['sys::num_of_pages']=false;//Кол-во страниц в постраничной разбивке
$_cfg['sys::from']='Mail Robot';//Значение поля От: для всех системных писем
$_cfg['sys::frontend_skin']='default';//Фронтэнд-скин
$_cfg['sys::backend_skin']='default';//Бэкенд-скин
$_cfg['sys::lang_id']=1;//ИД языка
$_cfg['sys::lang']='rus';//Имя языка
$_cfg['sys::main_page']=false;//Имя языка
$_cfg['sys::root_email']='smikebox@gmail.com';//E-mail суперпользователя системы

    /*Определить главный шаблон*/
    if(_MODE=='backend')
		$_tpl='sys::__default';			
    else 
		$_tpl='main::__default';


$_cfg['sys::on_page']=20;//Количество записей на странцу по умолчанию
//=================================================================//

/*Пропарсить ини-файл настроек*/
if(_MODE=='backend')
	$ini=parse_ini_file('../cfg.ini');
elseif(_MODE=='cmd')
	$ini=parse_ini_file('../../cfg.ini');
elseif(_MODE=='frontend') 
	$ini=parse_ini_file('cfg.ini');
foreach ($ini as $key=>$val)
{
	$_cfg['sys::'.$key]=$val;	
}
//===================================================================//

date_default_timezone_set($_cfg['sys::timezone']);//Задать часовой пояс по умолчанию

//Основные пути-----------------------------------------------------------------
define('_ROOT_URL',$ini['root_url']);
define('_ROOT_DIR',$ini['root_dir']);
define('_SECURE_DIR',$ini['secure_dir']);
define('_FILES_DIR',$ini['secure_dir'].'files/');
//==============================================================================

//Дополнительные пути-----------------------------------------------------------
define('_LOGS_DIR',_SECURE_DIR.'logs/');
define('_PRIVATE_DIR',_SECURE_DIR.'private/');
define('_SESSION_DIR',_SECURE_DIR.'session/');
define('_CACHE_DIR',_SECURE_DIR.'cache/');
define('_3PARY_DIR',_ROOT_DIR.'third_pary/');
define('_3PARY_URL',_ROOT_URL.'third_pary/');
define('_MODS_DIR',_ROOT_DIR.'mods/');
define('_MODS_URL',_ROOT_URL.'mods/');
define('_TEMP_DIR',_SECURE_DIR.'tmp/');
define('_PUBLIC_DIR',_ROOT_DIR.'public/');
define('_PUBLIC_URL',_ROOT_URL.'public/');
define('_COMMON_DIR',_ROOT_DIR.'common/');
define('_CMD_DIR',_ROOT_DIR.'cmd/');
//==============================================================================

//Настройки БД------------------------------------------------------------------
define('_DB_HOST',$ini['db_host']);
define('_DB_USER',$ini['db_user']);
define('_DB_NAME',$ini['db_name']);
define('_DB_PASS',$ini['db_pass']);
define('_DB_PREFIX',$ini['db_prefix']);
//==============================================================================

unset($ini);

//Задать обработчик ошибок------------------------------------------------------
error_reporting($_cfg['sys::error_reporting']);
if($_cfg['sys::error_reporting']==6143 && $_cfg['sys::debugger'])
	set_error_handler('displayError');
//==============================================================================

//Функция обработчик ошибок-----------------------------------------------------
function displayError($err_no, $err_str, $err_file, $err_line)
{
	global $_cfg, $_errors;
	$types[2]='E_WARNING';
	$types[8]='E_NOTICE';
	$types[256]='E_USER_ERROR';
	$types[512]='E_USER_WARNING';
	$types[1024]='E_USER_NOTICE';

	$trace=debug_backtrace();
	if(isset($types[$err_no]))
		$err['type']=$types[$err_no];
	else 
		$err['type']='undefined';
	$err['file']=$err_file;
	$err['level']=0;
	$err['line']=$err_line;
	$err['error']=$err_str;
	$_errors[]=$err;
	unset($err);
	
	if(count($trace))
	{
		$i=1;
	    foreach($trace as $err)
	    {
			if($err['function']!='displayError')
            {
				$err['level']=$i;
				$_errors[]=$err;
				$i++;
            }
                    
	    }
	}
}
//==============================================================================
?>