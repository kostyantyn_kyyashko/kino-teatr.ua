<?php
	$url=parse_url($_SERVER['REQUEST_URI']);
	parse_str($url['query'],$params);
	$location=false;
	switch ($url['path'])
	{
		case '/afisha.php':
			$location='/ru/main/bill.phtml';
		break;

		case '/film.php':
			if(!isset($params['action']))
				$location='/ru/main/films.phtml';
			elseif ($params['action']=='film')
				$location='/ru/main/film/film_id/'.$params['id'].'.phtml';
			elseif ($params['action']=='picture')
				$location='/ru/main/film_photos/film_id/'.$params['id'].'.phtml';
			elseif ($params['action']=='poster')
				$location='/ru/main/film_posters/film_id/'.$params['id'].'.phtml';
			elseif ($params['action']=='trailer')
				$location='/ru/main/film_trailers/film_id/'.$params['id'].'.phtml';
			else
				$location='/ru/main/films.phtml';
		break;

		case '/review.php':
			if(!isset($params['action']))
				$location='/ru/main/reviews.phtml';
			elseif ($params['action']=='film')
				$location='/ru/main/film_reviews/film_id/'.$params['id'].'.phtml';
			elseif ($params['action']=='review')
				$location='/ru/main/review/review_id/'.$params['id'].'.phtml';
			else
				$location='/ru/main/reviews.phtml';
		break;

		case '/kinoteatr.php':
			if(!isset($params['action']))
				$location='/ru/main/cinemas.phtml';
			elseif ($params['action']=='cinema')
				$location='/ru/main/cinema_shows/cinema_id/'.$params['id'].'.phtml';
			elseif($params['action']=='picture')
				$location='/ru/main/cinema_photos/cinema_id/'.$params['id'].'.phtml';
			else
				$location='/ru/main/cinemas.phtml';
		break;

		case '/news.php':
			if(!isset($params['action']))
				$location='/ru/main/news.phtml';
			elseif ($params['action']=='archive')
				$location='/ru/main/news/news_id/'.$params['section_id'].'.phtml';
			elseif ($params['action']=='detail')
				$location='/ru/main/news_article/article_id/'.$params['id'].'.phtml';
			else
				$location='/ru/main/news.phtml';
		break;
	}
   
	if($location)
	{
		header('HTTP/1.1 301 Moved Permanently');
		header('Location:'.$location);
		exit();
	}
?>