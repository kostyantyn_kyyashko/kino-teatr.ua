/*********************
//* jQuery Drop Line Menu- By Dynamic Drive: http://www.dynamicdrive.com/
//* Last updated: May 9th, 11'
//* Menu avaiable at DD CSS Library: http://www.dynamicdrive.com/style/
*********************/

var droplinemenu={

arrowimage: {classname: 'downarrowclass', src: 'images/down.png', leftpadding: 3}, //customize down arrow image
animateduration: {over: 200, out: 100}, //duration of slide in/ out animation, in milliseconds

buildmenu:function(menuid){
	jQuery(document).ready(function($){
		var $mainmenu=$("#"+menuid+">ul")
		var $headers=$mainmenu.find("ul").parent()
		$headers.each(function(i){
			var $curobj=$(this)
			var $subul=$(this).find('ul:eq(0)')
			this._dimensions={h:$curobj.find('a:eq(0)').outerHeight()}
			this.istopheader=$curobj.parents("ul").length==1? true : false
			if (!this.istopheader)
				$subul.css({left:0, top:this._dimensions.h})
			var $innerheader=$curobj.children('a').eq(0)
			$innerheader=($innerheader.children().eq(0).is('span'))? $innerheader.children().eq(0) : $innerheader //if header contains inner SPAN, use that
			$innerheader.append(
				'<img src="'+ droplinemenu.arrowimage.src
				+'" class="' + droplinemenu.arrowimage.classname
				+ '" style="border:0; padding-left: '+droplinemenu.arrowimage.leftpadding+'px" />'
			)
		}) //end $headers.each()

			$mainmenu.find("ul").css({display:'none', visibility:'visible', width:$mainmenu.width()})
			this._dimensions={h:27}
			$('ul.currentul').css({left: $mainmenu.position().left, top: $mainmenu.position().top+this._dimensions.h, display: 'block'})

			$('div.droplinebar ul li a.tmenu').hover(
				function(e){
					menucount = document.getElementById('menucount').value;
					number = $(this).attr("name").substring(9);


					if ($("#mydroplinemenu").find('#ul_'+number).length >0)
					{
						for (i=1;i<=menucount;i++)
						{
							$('a[name="topmenua_'+i+'"]').removeClass('current');
							$('#ul_'+i).css('display','none');
						}
						$(this).addClass('current');
					}

					this._dimensions={h:27}
					$('#ul_'+number).css({left: $mainmenu.position().left, top: $mainmenu.position().top+this._dimensions.h})



					$('#ul_'+number).css('display','block');
				},
				function(e){

				}
			) //end hover

	}) //end document.ready
}
}

