(function(window) {
// holds all our boxes
var boxes2 = []; 
var boxes = []; 
// Hold canvas information
var canvas;
var ctx;
var WIDTH;
var HEIGHT;
var INTERVAL = 20;  // how often, in milliseconds, we check to see if a redraw is needed
var mx, my; // mouse coordinates
 // when set to true, the canvas will redraw everything
 // invalidate() just sets this to false right now
 // we want to call invalidate() whenever we make a change
var canvasValid = false;
// The node (if any) being selected.
// If in the future we want to select multiple objects, this will get turned into an array
var mySel = null;
// The selection color and width. Right now we have a red selection with a small width
var mySelColor = '#CC0000';
var mySelWidth = 2;
var mySelBoxColor = 'darkred'; // New for selection boxes
var mySelBoxSize = 6;
// we use a fake canvas to draw individual shapes for selection testing
var ghostcanvas;
var gctx; // fake canvas context
// Padding and border style widths for mouse offsets
var stylePaddingLeft, stylePaddingTop, styleBorderLeft, styleBorderTop;
// Box object to hold data
var sessionguid;
var serverconnection;

var currentPrice = 0;
var selectedPlaces = 0;

var chairRed;
var chairGreen;
var chairYellow;
var chairBlack;
var chairGray;




function Box2() {
  this.x = 0;
  this.y = 0;
  this.w = 1; // default width and height?
  this.h = 1;
  this.Row = 1;
  this.Place = 1;
  this.State = 0;
  this.ChairType = 1;
  this.fill = '#444444';
}
// New methods on the Box class
Box2.prototype = {
  // we used to have a solo draw function
  // but now each box is responsible for its own drawing
  // maindraw() will call this with the normal canvas
  // mydown will call this with the ghost canvas with 'black'
  draw: function(context, optionalColor) {
      if (context === gctx) {
        context.fillStyle = 'black'; // always want black for the ghost canvas
		context.fillRect(this.x,this.y,this.w,this.h);
      } else {

		  context.fillStyle = this.fill;
		  if(this.State === 0){
			if(this.ChairType === 1){
				context.drawImage(chairGreen,this.x,this.y,this.w,this.h);      	
			    context.fillStyle = 'white';
			}
			if(this.ChairType === 2){
				context.drawImage(chairYellow,this.x,this.y,this.w,this.h);      	
			    context.fillStyle = 'black';
			}
			if(this.ChairType === 3){
				context.drawImage(chairRed,this.x,this.y,this.w,this.h);      	
			    context.fillStyle = 'white';
			}
		  }
		  if(this.State === 1){
			context.drawImage(chairGray,this.x,this.y,this.w,this.h);      	
		    context.fillStyle = 'black';
		  }
 		  if(this.State === 2){
			context.drawImage(chairBlack,this.x,this.y,this.w,this.h);      	
			context.fillStyle = 'white';			   
		  }
			// draw selection
			// this is a stroke along the box and also 8 new selection handles
		  context.font = "11px Arial";
		  if(this.Place>9) context.fillText(this.Place, this.x + 4.5,this.y + 15);
	 	  if(this.Place<10) context.fillText(this.Place, this.x + 7.5,this.y + 15);
      }
  } // end draw
}

//Initialize a new Box, add it, and invalidate the canvas
function addRect(x, y, w, h, row, place, state, chairType) {
  var rect = new Box2;
  rect.x = x;
  rect.y = y;
  rect.w = w
  rect.h = h;
  rect.Row = row;
  rect.Place = place;
  rect.State = state;
  rect.ChairType = chairType;
  boxes2.push(rect);

}
// initialize our canvas, add a ghost canvas, set draw loop
// then add everything we want to intially exist on the canvas
function init2() {

	chairRed = new Image();
	chairGreen = new Image();
	chairYellow = new Image();
	chairBlack = new Image();
	chairGray = new Image();
	chairRed.src = '//kino-teatr.ua/svg/redChair.svg';
	chairGreen.src = '//kino-teatr.ua/svg/greenChair.svg';
	chairYellow.src = '//kino-teatr.ua/svg/yellowChair.svg';
	chairBlack.src = '//kino-teatr.ua/svg/ChairBlack.svg';
	chairGray.src = '//kino-teatr.ua/svg/greyChair.svg';

	chairRed.onload = function() {
	  invalidate();
	};
	chairGreen.onload = function() {
  	  invalidate();
	};
	chairYellow.onload = function() {
	  invalidate();
	};
	chairBlack.onload = function() {
	  invalidate();
	};
	chairGray.onload = function() {
	  invalidate();
	};

  canvas = document.getElementById('canvas2');
  HEIGHT = canvas.height;
  WIDTH = canvas.width;
  ctx = canvas.getContext('2d');
  ghostcanvas = document.createElement('canvas');
  ghostcanvas.height = HEIGHT;
  ghostcanvas.width = WIDTH;
  gctx = ghostcanvas.getContext('2d');
  //fixes a problem where double clicking causes text to get selected on the canvas
  canvas.onselectstart = function () { return false; }
  // fixes mouse co-ordinate problems when there's a border or padding
  // see getMouse for more detail
  if (document.defaultView && document.defaultView.getComputedStyle) {
    stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingLeft'], 10)     || 0;
    stylePaddingTop  = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingTop'], 10)      || 0;
    styleBorderLeft  = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderLeftWidth'], 10) || 0;
    styleBorderTop   = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderTopWidth'], 10)  || 0;
  }
  
  // make mainDraw() fire every INTERVAL milliseconds
  setInterval(mainDraw, INTERVAL);
  
  // set our events. Up and down are for dragging,
  // double click is for making new boxes
  canvas.onmousedown = myDown;
  canvas.onmouseup = myUp;
  canvas.ondblclick = myDblClick;
  canvas.onmousemove = myMove;
  // add custom initialization here:
 
  setInterval(function(){
		  $.getJSON(serverconnection+'/ImOnline?sessionguid='+sessionguid+'&callback=?', null, function (res) {
			  if(res === "error"){
				alert("Нет доступа");
			  }
		  });
  },60000);




}

function drawInformationData(context){
  context.fillStyle = "blue";
  context.font = "bold 12px Arial";
  context.fillText("Всего выбрано:", 23, 20);
  context.fillText("Общая стоимость:", 10, 40);

  context.fillText(selectedPlaces, 120, 20);
  context.fillText(currentPrice + ' грн.', 120, 40);
}

//wipes the canvas context
function clear(c) {
  c.clearRect(0, 0, WIDTH, HEIGHT);
}

// Main draw loop.
// While draw is called as often as the INTERVAL variable demands,
// It only ever does something if the canvas gets invalidated by our code
function mainDraw() {
  if (canvasValid == false) {
    clear(ctx);
    // Add stuff you want drawn in the background all the time here
  
    // draw all boxes
    var l = boxes2.length;
    for (var i = 0; i < l; i++) {
      boxes2[i].draw(ctx); // we used to call<F5> drawshape, but now each box draws itself
    }
    // Add stuff you want drawn on top all the time here

	drawInformationData(ctx);
  	canvasValid = true;
  }
}

// Happens when the mouse is moving inside the canvas
function myMove(e){
  getMouse(e);
  if (mySel !== null ) {
    this.style.cursor='auto';
  }
}

function myDown(e){
  getMouse(e);
  clear(gctx);
//
    if (typeof e.layerX === "undefined") {
	coords = canvas.relMouseCoords(event);
	canvasX = coords.x;
	canvasY = coords.y;
		
	}else{
	canvasX = e.layerX;
	canvasY = e.layerY;
	}
  var l = boxes2.length;
  for (var i = l-1; i >= 0; i--) {
    // draw shape onto ghost context
	//
	if(boxes2[i].x <= canvasX && boxes2[i].x + boxes2[i].w >= canvasX){
	    boxes2[i].draw(gctx, 'black');
	    // get image data at the mouse x,y pixel
	    var imageData = gctx.getImageData(mx, my, 1, 1);
	    // if the mouse pixel exists, select and break
	    if (imageData.data[3] > 0) {
	      mySel = boxes2[i];
		  if(mySel.State === 0){
			  reserveplace(mySel);
		  } else {
			if(mySel.State === 2){
			  reserveplace(mySel)
			} else{
				if(mySel.State === 321){
					
				}
			}
		  }
	      clear(gctx);
	  	  invalidate();
	      return;
		}
	}
  }
  // havent returned means we have selected nothing
  mySel = null;
  // clear the ghost canvas for next time
  clear(gctx);
  // invalidate because we might need the selection border to disappear
  invalidate();
}

function relMouseCoords(event){
    var totalOffsetX = 0;
    var totalOffsetY = 0;
    var canvasX = 0;
    var canvasY = 0;
    var currentElement = this;

    do{
        totalOffsetX += currentElement.offsetLeft - currentElement.scrollLeft;
        totalOffsetY += currentElement.offsetTop - currentElement.scrollTop;
    }
    while(currentElement = currentElement.offsetParent)

    canvasX = event.pageX - totalOffsetX;
    canvasY = event.pageY - totalOffsetY;

    return {x:canvasX, y:canvasY}
}
HTMLCanvasElement.prototype.relMouseCoords = relMouseCoords;


function myUp(){
  isDrag = false;
  isResizeDrag = false;
  expectResize = -1;
}

// adds a new node
function myDblClick(e) {
  getMouse(e);
}

function invalidate() {
  canvasValid = false;
}

// Sets mx,my to the mouse position relative to the canvas
// unfortunately this can be tricky, we have to worry about padding and borders
function getMouse(e) {
      var element = canvas, offsetX = 0, offsetY = 0;
      if (element.offsetParent) {
        do {
          offsetX += element.offsetLeft;
          offsetY += element.offsetTop;
        } while ((element = element.offsetParent));
      }
      // Add padding and border style widths to offset
      offsetX += stylePaddingLeft;
      offsetY += stylePaddingTop;

      offsetX += styleBorderLeft;
      offsetY += styleBorderTop;

      mx = e.pageX - offsetX;
      my = e.pageY - offsetY
  }

function reserveplace(place) {
	var ticket;
	$.getJSON(serverconnection+'/reserveplace?sessionguid='+sessionguid+'&sector='
					+1+'&row='+place.Row+'&place='+place.Place+'&callback=?', null, function (ticket) {
		  if(ticket.State === "2"){
			  place.State = 2;
		  }
		  if(ticket.State === "1"){
		  	  place.State = 1;
		  }
 		  if(ticket.State === "0"){
		  	  place.State = 0;
		  }
		  currentPrice = ticket.TotalPrice;
		  selectedPlaces = ticket.SelectedPlaces;
		   invalidate();
    });
	return ticket
}

function reserve() {
	var ticket;
	$.getJSON(serverconnection+'/ReservSelectedTickets?sessionguid='+sessionguid +'&callback=?', null, function (reservationData) {
		alert('Код брони: ' + reservationData.ReservationCode + 
					' Количество билетов:' + reservationData.SelectedPlaces + 
					' Цена:' +  reservationData.TotalPrice );
    });
}

  function setsessionguid(guid){
	sessionguid	= guid;
  }
  function setserverconnection(conection){
	serverconnection = conection;
  }



window.init2 = init2;
window.addRect = addRect;
window.setsessionguid = setsessionguid;
window.setserverconnection = setserverconnection;
window.reserve = reserve;
window.invalidate = invalidate;
})(window);

