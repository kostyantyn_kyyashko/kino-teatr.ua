<?php
require_once('../init.php');

$hostname = "localhost";
$username = "kinoteatrua";
$password = "aelaevoQu0le";
$dbName = "grifix_kino_v2";
$mail='';
mysql_connect($hostname,$username,$password) OR DIE("Не могу создать соединение ");
mysql_query("SET CHARACTER SET 'utf8'");
mysql_select_db($dbName) or die(mysql_error());

$url_xml=_ROOT_DIR."/export_m/vkino.xml";

$info_mail='';

$show_xml = simplexml_load_file($url_xml);

foreach($show_xml->theaters->theater as $temp_cinema)
{
	foreach($temp_cinema->halls->hall as $temp_hall)
	{
		$query="SELECT * FROM grifix_main_auto_cinema WHERE id_export='2' and id_hall_export='".$temp_hall['id']."' and id_cinema_export='".$temp_cinema['alias']."'";
    	$result = mysql_query($query);
    	if(mysql_num_rows($result)==0)
    	{
    		$query="SELECT * FROM grifix_main_auto_cinema WHERE id_export='2' and id_cinema_export='".$temp_cinema['alias']."' LIMIT 0 , 1";
    		$result = mysql_query($query);
    		if(mysql_num_rows($result)==0)
    		{
    			$query="SELECT MAX(id_cinema_export_num) FROM grifix_main_auto_cinema";
    			$result = mysql_query($query);
    			$row_max = mysql_fetch_array($result);
	    		$cinema_id=$row_max['MAX(id_cinema_export_num)']+1;
    		}
    		else
    		{
    			$row = mysql_fetch_array($result);
    			$cinema_id=$row['id_cinema_export_num'];
    		}
    		$query="INSERT INTO grifix_main_auto_cinema VALUES('','2','".$temp_cinema['alias']."','".$cinema_id."','0','".$temp_cinema->name." (".$temp_cinema->location->city->name.")','".$temp_hall['id']."','0','".$temp_hall->name."','','0')";
    		$result = mysql_query($query);
    		$info_mail.='<p>Новый Кинотеатр\Зал - '.$temp_cinema->name.'</p>';
    	}
    	else
    	{
    		$query="UPDATE grifix_main_auto_cinema SET name_cinema='".$temp_cinema->name." (".$temp_cinema->location->city->name.")' WHERE id_hall_export='".$temp_hall['id']."' and id_cinema_export='".$temp_cinema['alias']."'";
    		$result = mysql_query($query);
    	}
	}
}
//films
foreach($show_xml->shows->show as $temp)
{
	$query="SELECT * FROM grifix_main_auto_films WHERE id_export='2' and id_film_export='".$temp['id']."'";
   	$result = mysql_query($query);
   	if(mysql_num_rows($result)==0)
   	{
   		$query="INSERT INTO grifix_main_auto_films VALUES('','2','".$temp['id']."','0','".$temp->name." (".$temp->year.")','0')";
    	$result = mysql_query($query);
    	$info_mail.='<p>Новый Фильм - '.$temp->name.'</p>';
	}
}
//films
foreach($show_xml->{'shows-soon'}->show as $temp)
{
	$query="SELECT * FROM grifix_main_auto_films WHERE id_export='2' and id_film_export='".$temp['id']."'";
   	$result = mysql_query($query);
   	if(mysql_num_rows($result)==0)
   	{
   		$query="INSERT INTO grifix_main_auto_films VALUES('','2','".$temp['id']."','0','".$temp->name." (".$temp->year.")','0')";
    	$result = mysql_query($query);
    	$info_mail.='<p>Новый Фильм - '.$temp->name.'</p>';
	}
}

$query="TRUNCATE TABLE grifix_main_shows_vkino";
$result=mysql_query($query);

foreach($show_xml->showstimes->showtime as $temp)
{
	$f_id=0;
	$c_id=0;
	$h_id=0;
	$date=substr($temp['date'],0,10); //2014-05-25 20:10
	$time=substr($temp['date'],11,5).':00';
	$price= $temp['priceList'];
	//serch film
	$query="SELECT * FROM grifix_main_auto_films WHERE id_export='2' and id_film_export='".$temp['showId']."' and status=0";
   	$result = mysql_query($query);
   	if(mysql_num_rows($result)!=0)
   	{
   		$row	= mysql_fetch_array($result);
   		$f_id  	= $row['id_film'];
    	//serch hall and cinema
    	$query="SELECT * FROM grifix_main_auto_cinema WHERE id_export='2' and id_hall_export='".$temp['hallId']."' and id_cinema_export='".$temp['theaterAlias']."' and status=0";
	   	$result = mysql_query($query);
	   	if(mysql_num_rows($result)!=0)
	   	{
	   		$row	= mysql_fetch_array($result);
	   		$c_id  	= $row['id_cinema'];
	   		$h_id  	= $row['id_hall'];

   			if($f_id!=0 and $c_id!=0 and $h_id!=0)
   			{
   				$is_3d=0;
				if( $temp['technology']=='dolby-3d' or
					$temp['is3d']=='y' or
					$temp['technology']=='imax-3d' or
					$temp['technology']=='xpand' or
					$temp['technology']=='4dx-3d' or
					$temp['technology']=='reald' or
					$temp['technology']=='dbox-3d')
				{$is_3d=1;}

               	//$alias_sale='no';
                //if($temp['purchase']=='y')
                //{
                   	$alias_sale=$temp['theaterAlias'];
                //}
				$query="INSERT INTO grifix_main_shows_vkino VALUES('',
															'".$temp['id']."',
															'".$alias_sale."',
															'".$c_id."',
															'".$h_id."',
															'".$f_id."',
															'".$is_3d."',
															'".$date."',
															'".$time."',
															'".$price."')";
   				$result = mysql_query($query);
   			}
		}
	}
}

/***********IMAX**************/
$xml_array=array(135=>_ROOT_DIR."/export_m/135.xml",//ТРЦ «Блокбастер» IMAX
				 296=>_ROOT_DIR."/export_m/296.xml",//ТРЦ «City Center»
				 297=>_ROOT_DIR."/export_m/297.xml",//ТРЦ «City Center Котовський»
				 207=>_ROOT_DIR."/export_m/207.xml",//ТРЦ «King Cross Leopolis»
				 262=>_ROOT_DIR."/export_m/262.xml",//ТРЦ «Французький Бульвар»
				 279=>_ROOT_DIR."/export_m/279.xml",//Ялта
				 285=>_ROOT_DIR."/export_m/285.xml",//ТРЦ «Мануфактура»
				 134=>_ROOT_DIR."/export_m/134.xml"//ТРЦ «Блокбастер»
);
//список кинотеатров с залами
$cinema_hall=array(
	135=>array(215=>16), //ТРЦ «Блокбастер» IMAX
	296=>array(594=>46,605=>47,606=>48,607=>49,608=>50,609=>51), //Одесса
	297=>array(595=>17,597=>18,598=>19,599=>20,600=>21), //Одесса
	207=>array(356=>28,357=>29,358=>30,359=>31,360=>27,361=>32,362=>34), //Львов
	262=>array(476=>35,512=>36,515=>41,514=>38,510=>39,513=>40,511=>37), //Харьков
	279=>array(529=>43,549=>45,550=>42), //Ялта
	285=>array(547=>22,545=>24,546=>25,543=>26,544=>23), //Сумы
	134=>array(685=>203,686=>204,687=>200,688=>201,689=>202), //ТРЦ «Блокбастер» IMAX
	315=>array(669=>101,670=>102,671=>103,672=>104,673=>105,704=>106) //Львов ТРЦ «Форум»
);
foreach ($xml_array as $key => $url_xml)
{
	$show_xml = simplexml_load_file($url_xml);
	foreach ($show_xml->movies->movie as $temp_film)
	{
		$query="SELECT * FROM grifix_main_auto_films WHERE id_export='4' and id_film_export='".$temp_film['id']."'";
	   	$result = mysql_query($query);
	   	if(mysql_num_rows($result)==0)
	   	{
	   		$html_symbol_title=array('"',"'",'<','>');
			$spec_symbol_title=array('&quot;',"&apos;",'&lt;','&gt;');
			$title=strip_tags(stripslashes(trim($temp_film->title)));
			$title=str_replace($html_symbol_title, $spec_symbol_title, $title);
	   		$query="INSERT INTO grifix_main_auto_films VALUES('','4','".$temp_film['id']."','0','".$title."','0')";
	    	$result = mysql_query($query);
	    	$info_mail.='<p>Новый Фильм - '.$title.'</p>';
		}
	}
	foreach ($show_xml->showtimes->day as $temp_day)
	{
		$date=$temp_day['date'];

		foreach ($temp_day->show as $temp)
		{
			$time=$temp['time'];
			$f_id=0;
			$c_id=$key;
			$h_id=array_search((int)$temp['hall-id'], $cinema_hall[$key]);
			$h_pr='40-120';

			$s_3d	=0;
			if($temp['technology']=='3d' or $temp['technology']=='imax'){$s_3d=1;}

			//serch film
			$query="SELECT * FROM grifix_main_auto_films WHERE id_export='4' and id_film_export='".$temp['movie-id']."' and status=0";
		   	$result = mysql_query($query);
		   	if(mysql_num_rows($result)!=0)
		   	{
		   		$row	= mysql_fetch_array($result);
		   		$f_id  	= $row['id_film'];
		   	}

			if($f_id!=0 and $c_id!=0 and $h_id!=0)
			{
		 		$query="INSERT INTO grifix_main_shows_vkino VALUES('',
															'0',
															'imax',
															'".$c_id."',
															'".$h_id."',
															'".$f_id."',
															'".$s_3d."',
															'".$date."',
															'".$time."',
															'".$h_pr."')";
		   		$result = mysql_query($query);
		  	}
		}
	}
}
/*******************************/

$info_mail.='<p>Экспорт сеансов прошел успешно!</p>';
$info_mail.='<p>Обновите данные: <a href="https://kino-teatr.ua/export_m/">kino-teatr.ua/export_m/</a></p>';

$message='<html>
			<head>
				<title>Отчет Экспорта</title>
			</head>
			<body>
				'.$info_mail.'
			</body></html>';

$headers ="MIME-Version: 1.0" . "\r\n";
$headers.="Content-Type: text/html; charset=utf-8" . "\r\n";
$headers.="Content-Transfer-Encoding: 8bit" . "\r\n";
$headers.="From: =?utf-8?b?".base64_encode("Отчет Экспорта")."?=<admin@kino-teatr.ua>"  . "\r\n";
$to="admin@kino-teatr.ua,cm@kino-teatr.ua";
$subject="=?utf-8?b?".base64_encode("Отчет Экспорта")."?=";
mail($to,$subject,$message,$headers);

// Вывод времени срабатывания
$fp = fopen("/var/www/html/multiplex/multiplex.in.ua/public/test/logger.txt", "a"); // Открываем файл в режиме записи
fwrite($fp, date("H:i:s  d.m.Y")." vkino_shows \r\n "); // Запись в файл
fclose($fp); //Закрытие файла
?>