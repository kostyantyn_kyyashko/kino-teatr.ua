<?
function translit($str,$id,$name)
{
    $tr = array(
        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        "the"=>"","the "=>""," the"=>""," the "=>"-",
        "The"=>"","The "=>""," The"=>""," The "=>"-",
        "... "=> "", " - "=> "-"," "=> "-", "."=> "", "/"=> "-"
    );
    strtolower(strtr($str,$tr));
    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', strtolower(strtr($str,$tr)));
    return 'http://kino-teatr.ua/'.$name.'/'.$urlstr.'-'.$id.'.phtml';
}

$hostname = "91.194.251.253";
$username = "kinoteatrua";
$password = "aelaevoQu0le";
$dbName = "grifix_kino_v2";

mysql_connect($hostname,$username,$password) OR DIE("Не могу создать соединение ");
mysql_query("SET CHARACTER SET 'utf8'");
mysql_select_db($dbName) or die(mysql_error());

$lang=1;//3
$id_cinema='142'; //142,123,33
$result['shows']=array();
//Показы-----------------------------------------------------------------
$query="
	SELECT
		shw.begin,
		shw.end,
		shw.film_id,
		shw.hall_id,
		shw.id,
		hls.cinema_id
	FROM
		grifix_main_shows AS shw
	LEFT JOIN
		grifix_main_cinemas_halls AS hls ON hls.id=shw.hall_id
	WHERE
		shw.end>='".mysql_real_escape_string(date('Y-m-d'))."'
		AND hls.cinema_id IN('".$id_cinema."')
	ORDER BY
		shw.end ASC";

$r=mysql_query($query) or die(mysql_error());
while($obj=mysql_fetch_assoc($r))
{
	$films[$obj['film_id']]=$obj['film_id'];
	$cinemas[$obj['cinema_id']]=$obj['cinema_id'];
	$halls[$obj['hall_id']]=$obj['hall_id'];
	$shows[$obj['id']]=$obj['id'];

	$obj['times']=array();
	$result['shows'][$obj['id']]=$obj;
}

//Часы показов--------------------------------------------------------

$query="
	SELECT
		tms.time,
		tms.prices,
		tms.3D,
		tms.show_id
	FROM
		grifix_main_shows_times AS tms
	WHERE
		tms.show_id IN(".implode(',',$shows).")";

$r=mysql_query($query) or die(mysql_error());
while($obj=mysql_fetch_assoc($r))
{
	$obj['prices']=str_replace(';',',',$obj['prices']);
	$result['shows'][$obj['show_id']]['times'][]=$obj;
}


//Фильмы------------------------------------------------------------------
$query="
	SELECT
		flm.id,
		lng.title,
		flm.title_orig,
		flm.year,
		flm.age_limit,
		flm.duration,
		lng.intro
	FROM
		grifix_main_films AS flm
	LEFT JOIN
		grifix_main_films_lng AS lng
	ON
		lng.record_id=flm.id
	AND
		lng.lang_id=".$lang."
	WHERE flm.id IN (".implode(',',$films).")";

$r=mysql_query($query) or die(mysql_error());
while ($obj=mysql_fetch_assoc($r))
{
	$films[$obj['id']]=$obj['id'];

	$obj['title']=htmlspecialchars($obj['title']);
	$obj['title_orig']=htmlspecialchars($obj['title_orig']);
	$obj['persons']=array();
	$obj['genres']=array();
	$obj['countries']=array();
	$obj['posters']=array();

	$result['films'][$obj['id']]=$obj;

}
//============================================================================
//Перcоны фильмов------------------------------------------------------
$query="
	SELECT
		fp.film_id,
		fp.person_id,
		fp.profession_id
	FROM
		grifix_main_films_persons AS fp
	LEFT JOIN
		grifix_main_films_persons_lng AS lng
	ON
		lng.record_id=fp.id
	AND
		lng.lang_id=".$lang."
	WHERE
		fp.film_id IN(".implode(',',$films).")
";
$persons=array();
$r=mysql_query($query) or die(mysql_error());
while ($obj=mysql_fetch_assoc($r))
{
	$persons[$obj['person_id']]=$obj['person_id'];
	$obj['role']=htmlspecialchars($obj['role']);
	$result['films'][$obj['film_id']]['persons'][]=$obj;
}
//==============================================================

//Жанры фильмов--------------------------------------
$query="
	SELECT
		gnr.film_id,
		lng.lang_id,
		lng.title AS title
	FROM
		grifix_main_films_genres AS gnr
	LEFT JOIN
		grifix_main_genres_lng AS lng
		ON lng.record_id=gnr.genre_id
	WHERE
		gnr.film_id IN(".implode(',',$films).")
		AND lng.lang_id=1
";

$r=mysql_query($query) or die(mysql_error());
while ($obj=mysql_fetch_assoc($r))
{
	$result['films'][$obj['film_id']]['genres'][]=$obj['title'];
}
//================================================

//Страны фильмов--------------------------------------
$query="
	SELECT
		film_id,
		country_id
	FROM
		grifix_main_films_countries
	WHERE
		film_id IN(".implode(',',$films).")
";
$r=mysql_query($query) or die(mysql_error());
while ($obj=mysql_fetch_assoc($r))
{
	$result['films'][$obj['film_id']]['countries'][]=$obj['country_id'];
}
//================================================
//Постеры фильмов-------------------------------------------------
$query="
	SELECT
		film_id,
		image,
		lang
	FROM
		grifix_main_films_posters
	WHERE
		film_id IN(".implode(',',$films).")
		AND (lang LIKE 'en' OR lang LIKE 'ru')
";
$r=mysql_query($query) or die(mysql_error());
while($obj=mysql_fetch_assoc($r))
{
	$result['films'][$obj['film_id']]['posters'][]=$obj;
}
//================================================================
//Персоны-------------------------------------------------------
$result['persons']=array();
if($persons)
{
	$query="
		SELECT
			prs.id,
			prs.name,
			lng.lastname,
			lng.firstname
		FROM
			grifix_main_persons AS prs
		LEFT JOIN
			grifix_main_persons_lng AS lng
		ON
			lng.record_id=prs.id
		AND
			lng.lang_id=1
		WHERE
			prs.id IN(".implode(',',$persons).")
	";

	$r=mysql_query($query) or die(mysql_error());
	while ($obj=mysql_fetch_assoc($r))
	{
		$obj['lastname']=htmlspecialchars($obj['lastname']);
		$obj['firstname']=htmlspecialchars($obj['firstname']);
		$obj['url']=translit($obj['name'],$obj['id'],'person');
		$result['persons_ru'][]=$obj;
	}
}
/**********************************************/
$sitemap_text='';
$sitemap_text.='<?xml version="1.0" encoding="UTF-8"?>
<billboard>
<films>';
foreach ($result['films'] as $film)
{
	$sitemap_text.='<film id="'.$film['id'].'">
				<title orig="'.$film['title_orig'].'">'.$film['title'].'</title>
				<url>'.translit($film['title_orig'],$film['id'],'film').'</url>
				<persons>
';
				foreach ($film['persons'] as $person)
				{
					$sitemap_text.='<person id="'.$person['person_id'].'" profession_id="'.$person['profession_id'].'"/>';
				}
				$sitemap_text.='</persons>
				<genres>'.implode(', ',$film['genres']).'</genres>
				<countries>'.implode(',',$film['countries']).'</countries>
				<duration>'.$film['duration'].'</duration>
				<year>'.$film['year'].'</year>
				<age_limit>'.$film['age_limit'].'</age_limit>
				<posters>
';
				foreach ($film['posters'] as $poster)
				{
					$sitemap_text.='<poster lang="'.$poster['lang'].'" src="http://kino-teatr.ua/public/main/films/'.$poster['image'].'"/>';
				}
	$sitemap_text.='</posters>
				<intro><![CDATA['.$film['intro'].']]></intro>
				</film>';
}

$sitemap_text.='
</films>';

$sitemap_text.='<persons>
';
foreach ($result['persons_ru'] as $pr)
{
	$sitemap_text.='<pr id="'.$pr['id'].'">
						<lastname>'.$pr['lastname'].'</lastname>
						<firstname>'.$pr['firstname'].'</firstname>
						<url>'.$pr['url'].'</url>
					</pr>
';
}
$sitemap_text.='</persons>';

$sitemap_text.='<shows>
';
foreach ($result['shows'] as $show)
{
	$sitemap_text.='<show id="'.$show['id'].'" film_id="'.$show['film_id'].'" cinema_id="'.$show['cinema_id'].'" hall_id="'.$show['hall_id'].'">
				<begin>'.$show['begin'].'</begin>
				<end>'.$show['end'].'</end>
				<times>
';
		foreach ($show['times'] as $time)
		{
			$sitemap_text.='<time time="'.$time['time'].'" stereo="'.$time['3D'].'">
							<prices>'.$time['prices'].'</prices>
						</time>
';
		}
		$sitemap_text.='</times>
			</show>
';
}
$sitemap_text.='</shows>';
$sitemap_text.='</billboard>';
header("Content-Type: text/xml");
echo $sitemap_text;
?>