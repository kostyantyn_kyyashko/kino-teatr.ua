<?php
$hostname = "91.194.251.253";
$username = "kinoteatrua";
$password = "aelaevoQu0le";

$dbName = "grifix_kino_v2";

mysql_connect($hostname,$username,$password) OR DIE("Не могу создать соединение ");
mysql_query("SET CHARACTER SET 'utf8'");
mysql_select_db($dbName) or die(mysql_error());

//$lang=1;
$lang=3;
$result=array();
/*******************/

//Жанры----------------------------------------------------------------
$result['genres']=array();
$r="
	SELECT
		gnr.id,
		lng.title
	FROM
		grifix_main_genres AS gnr
	LEFT JOIN
		grifix_main_genres_lng AS lng
	ON
		lng.record_id=gnr.id
	AND
		lng.lang_id=".intval($lang)."
	ORDER BY
		gnr.order_number
";
$rs=mysql_query($r);
while ($obj=mysql_fetch_array($rs))
{
	$obj['title']=htmlspecialchars($obj['title']);
	$result['genres'][]=$obj;
}
//Профессии----------------------------------------
$result['professions']=array();
$r="
	SELECT
		prf.id,
		lng.title
	FROM
		grifix_main_professions AS prf
	LEFT JOIN
		grifix_main_professions_lng AS lng
	ON
		lng.record_id=prf.id
	AND
		lng.lang_id=".intval($lang)."
	ORDER BY
		prf.order_number
";
$rs=mysql_query($r);
while ($obj=mysql_fetch_array($rs))
{
	$obj['title']=htmlspecialchars($obj['title']);
	$result['professions'][]=$obj;
}
//===============================================================================
//Страны------------------------------------------------------------------------
$result['countries']=array();
$r="
	SELECT
		cnt.id,
		lng.title
	FROM
		grifix_main_countries AS cnt
	LEFT JOIN
		grifix_main_countries_lng AS lng
	ON
		lng.record_id=cnt.id
	AND
		lng.lang_id=".intval($lang)."
	ORDER BY
		cnt.order_number
";
$rs=mysql_query($r);
while ($obj=mysql_fetch_array($rs))
{
	$obj['title']=htmlspecialchars($obj['title']);
	$result['countries'][]=$obj;
}
//Города------------------------------------------------------------------------
$result['cities']=array();
$r="
	SELECT
		cit.id,
		cit.country_id,
		lng.title
	FROM
		grifix_main_countries_cities AS cit
	LEFT JOIN
		grifix_main_countries_cities_lng AS lng
	ON
		lng.record_id=cit.id
	AND
		lng.lang_id=".intval($lang)."
	ORDER BY
		cit.order_number
";
$rs=mysql_query($r);
while ($obj=mysql_fetch_array($rs))
{
	$obj['title']=htmlspecialchars($obj['title']);
	$result['cities'][]=$obj;
}
//===============================================================================
//Показы-----------------------------------------------------------------
$result['shows']=array();
$films=array();
$cinemas=array();
$halls=array();
$shows=array();

$r="
		SELECT
		shw.begin,
		shw.end,
		shw.film_id,
		shw.hall_id,
		shw.id,
		hls.cinema_id
	FROM
		grifix_main_shows AS shw
	LEFT JOIN
		grifix_main_cinemas_halls AS hls
	ON
		hls.id=shw.hall_id
	WHERE
		shw.end>='2015-12-24'
	ORDER BY
		shw.end ASC
";
$rs=mysql_query($r);
while ($obj=mysql_fetch_array($rs))
{
	$films[$obj['film_id']]=$obj['film_id'];
	$cinemas[$obj['cinema_id']]=$obj['cinema_id'];
	$halls[$obj['hall_id']]=$obj['hall_id'];
	$shows[$obj['id']]=$obj['id'];

	$obj['times']=array();
	$result['shows'][$obj['id']]=$obj;
	//print_r($obj);
}
//if(!$rs) {echo '===='.mysql_error().'<br>';}
echo 'ok';
//Часы показов--------------------------------------------------------

$r="
	SELECT
		tms.time,
		tms.prices,
		tms.3D,
		tms_lng.note,
		tms.show_id
	FROM
		grifix_main_shows_times AS tms
	LEFT JOIN
		grifix_main_shows_times_lng AS tms_lng
	ON
		tms_lng.record_id=tms.id
	AND
		tms_lng.lang_id=".intval($lang)."
	WHERE
		tms.show_id IN(".implode(',',$shows).")
";
$rs=mysql_query($r);
while ($obj=mysql_fetch_array($rs))
{
	$obj['prices']=str_replace(';',',',$obj['prices']);
	$result['shows'][$obj['show_id']]['times'][]=$obj;
}
//=========================================================================
//Фильмы------------------------------------------------------------------
$result['films']=array();
$r="
	SELECT
		flm.id,
		lng.title,
		flm.name,
		flm.title_orig,
		flm.duration,
		flm.year,
		flm.ukraine_premiere,
		flm.world_premiere,
		flm.age_limit,
		flm.budget,
		flm.budget_currency,
		lng.intro,
		lng.text,
		flm.rating,
		flm.votes,
		flm.pro_rating,
		flm.pro_votes,
		flm.3d
	FROM
		grifix_main_films AS flm
	LEFT JOIN
		grifix_main_films_lng AS lng
	ON
		lng.record_id=flm.id
	AND
		lng.lang_id=".$lang."
	WHERE
		flm.id IN(".implode(',',$films).")

";
$rs=mysql_query($r);
while ($obj=mysql_fetch_array($rs))
{
	if ($obj['title_orig']){$urlstr = translit_sourse($obj['title_orig']);}
	else{$urlstr = translit_sourse($obj['name']);}
	$urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

	$obj['title']=htmlspecialchars($obj['title']);
	$obj['title_orig']=htmlspecialchars($obj['title_orig']);
	$obj['photos']=array();
	$obj['trailers']=array();
	$obj['posters']=array();
	$obj['persons']=array();
	$obj['genres']=array();
	$obj['countries']=array();
	$obj['studios']=array();
	$obj['reviews']=array();
	$obj['links']=array();

	$obj['source_url']=_ROOT_URL.'film/'.$urlstr.'-'.$obj['id'].'.phtml';

	$result['films'][$obj['id']]=$obj;
}

//============================================================================
//Кинотеатры-----------------------------------------------------
$result['cinemas']=array();
$r="
	SELECT
		cnm.id,
		cnm.city_id,
		lng.title,
		cnm.site,
		cnm.phone,
		lng.address,
		lng.text
	FROM
		grifix_main_cinemas AS cnm
	LEFT JOIN
		grifix_main_cinemas_lng AS lng
	ON
		lng.record_id=cnm.id
	AND
		lng.lang_id=".intval($lang)."
	WHERE
		cnm.id IN(".implode(',',$cinemas).")
";
$rs=mysql_query($r);
while ($obj=mysql_fetch_array($rs))
{
	$obj['title']=htmlspecialchars($obj['title']);
	$obj['address']=htmlspecialchars($obj['address']);
	$obj['phone']=htmlspecialchars($obj['phone']);
	$obj['site']=htmlspecialchars($obj['site']);
	$obj['halls']=array();
	$obj['photos']=array();
	$result['cinemas'][$obj['id']]=$obj;
}
//============================================================
//Залы кинотеатра--------------------------------------------
$r="
	SELECT
		hls.id,
		hls.cinema_id,
		lng.title,
		hls.scheme,
		hls.3d
	FROM
		grifix_main_cinemas_halls AS hls
	LEFT JOIN
		grifix_main_cinemas_halls_lng AS lng
	ON
		lng.record_id=hls.id
	AND
		lng.lang_id=".intval($lang)."
	WHERE
		hls.id IN(".implode(',',$halls).")

";
$rs=mysql_query($r);
while ($obj=mysql_fetch_array($rs))
{
	$obj['title']=htmlspecialchars($obj['title']);
	if($obj['scheme'])
		$obj['scheme']=$_cfg['main::cinemas_url'].$obj['scheme'];
	$result['cinemas'][$obj['cinema_id']]['halls'][]=$obj;
}
//=============================================================

//Персоны-------------------------------------------------------
$result['persons']=array();
if($persons)
{
	$r="
		SELECT
			prs.id,
			prs.lastname_orig,
			prs.firstname_orig,
			lng.lastname,
			lng.firstname
		FROM
			grifix_main_persons AS prs
		LEFT JOIN
			grifix_main_persons_lng AS lng
		ON
			lng.record_id=prs.id
		AND
			lng.lang_id=".intval($lang)."
		WHERE
			prs.id IN(".implode(',',$persons).")
	";

	$rs=mysql_query($r);
	while ($obj=mysql_fetch_array($rs))
	{
		$obj['lastname']=htmlspecialchars($obj['lastname']);
		$obj['firstname']=htmlspecialchars($obj['firstname']);
		$obj['lastname_orig']=htmlspecialchars($obj['lastname_orig']);
		$obj['firstname_orig']=htmlspecialchars($obj['firstname_orig']);
		$obj['url']=main_persons::getPersonUrl($obj['id']);
		$result['persons'][]=$obj;
	}
}
//===============================================================
//===============================================================
//===============================================================
//===============================================================

$vivod='<?xml version="1.0" encoding="UTF-8"?>';
$vivod.='<billboard>';
$vivod.='<genres>';
	foreach ($result['genres'] as $obj)
	{		$vivod.='<genre id="'.$obj['id'].'">'.$obj['title'].'</genre>
';	}
$vivod.='
	</genres>
	<professions>';
	foreach ($result['professions'] as $obj)
	{
		$vivod.='<profession id="'.$obj['id'].'">'.$obj['title'].'</profession>
';
	}
$vivod.='
	</professions>
	<countries>';
	foreach ($result['countries'] as $obj)
	{
		$vivod.='<country id="'.$obj['id'].'">'.$obj['title'].'</country>
';
	}
$vivod.='
	</countries>
	<cities>';
	foreach ($result['cities'] as $obj)
	{
		$vivod.='<city id="'.$obj['id'].'" country_id="'.$obj['country_id'].'">'.$obj['title'].'</city>
';
	}
$vivod.='
	</cities>
	<studios>';
	foreach ($result['studios'] as $obj)
	{
		$vivod.='<studio id="'.$obj['id'].'" country_id="'.$obj['country_id'].'">'.$obj['title'].'</studio>
';
	}
$vivod.='
	</studios>
	<shows>';
	foreach ($result['shows'] as $show)
	{
		$vivod.='<show id="'.$show['id'].'" film_id="'.$show['film_id'].'" cinema_id="'.$show['cinema_id'].'" hall_id="'.$show['hall_id'].'">
				 	<begin>'.$show['begin'].'</begin>
					<end>'.$show['end'].'</end>
					<times>
';
		foreach ($show['times'] as $time)
		{			$vivod.='<time time="'.$time['time'].'" stereo="'.$time['3D'].'">
					 	<prices>'.$time['prices'].'</prices>
					 </time>
';		}
	}
$vivod.='
	</shows>
	<films>
	</film>';
$vivod.='</billboard>';
echo $vivod;
?>