<?
$hostname = "91.194.251.253";
$username = "kinoteatrua";
$password = "aelaevoQu0le";
$dbName = "grifix_kino_v2";

mysql_connect($hostname,$username,$password) OR DIE("Не могу создать соединение ");
mysql_query("SET CHARACTER SET 'utf8'");
mysql_select_db($dbName) or die(mysql_error());

$lang=3;//3
$id_film_r=file_get_contents('http://adm.rozklad.net/script/r.txt');
$result['films']=array();
$films=array();
//Фильмы------------------------------------------------------------------
$query="
	SELECT
		flm.id,
		lng.title,
		flm.title_orig,
		flm.year,
		flm.age_limit
	FROM
		grifix_main_films AS flm
	LEFT JOIN
		grifix_main_films_lng AS lng
	ON
		lng.record_id=flm.id
	AND
		lng.lang_id=".$lang."
	WHERE flm.id IN (".$id_film_r.")";

$r= mysql_query($query);
while ($obj=mysql_fetch_assoc($r))
{
	$films[$obj['id']]=$obj['id'];

	$obj['title']=htmlspecialchars($obj['title']);
	$obj['title_orig']=htmlspecialchars($obj['title_orig']);
	$obj['persons']=array();
	$obj['genres']=array();
	$obj['countries']=array();

	$result['films'][$obj['id']]=$obj;

}
//============================================================================
//Перcоны фильмов------------------------------------------------------
$query="
	SELECT
		fp.film_id,
		fp.person_id,
		fp.profession_id
	FROM
		grifix_main_films_persons AS fp
	LEFT JOIN
		grifix_main_films_persons_lng AS lng
	ON
		lng.record_id=fp.id
	AND
		lng.lang_id=".$lang."
	WHERE
		fp.film_id IN(".implode(',',$films).")
";
$persons=array();
$r= mysql_query($query);
while ($obj=mysql_fetch_assoc($r))
{
	$persons[$obj['person_id']]=$obj['person_id'];
	$obj['role']=htmlspecialchars($obj['role']);
	$result['films'][$obj['film_id']]['persons'][]=$obj;
}
//==============================================================

//Жанры фильмов--------------------------------------
$query="
	SELECT
		film_id,
		genre_id
	FROM
		grifix_main_films_genres
	WHERE
		film_id IN(".implode(',',$films).")
";
$r= mysql_query($query);
while ($obj=mysql_fetch_assoc($r))
{
	$result['films'][$obj['film_id']]['genres'][]=$obj['genre_id'];
}
//================================================

//Страны фильмов--------------------------------------
$query="
	SELECT
		film_id,
		country_id
	FROM
		grifix_main_films_countries
	WHERE
		film_id IN(".implode(',',$films).")
";
$r= mysql_query($query);
while ($obj=mysql_fetch_assoc($r))
{
	$result['films'][$obj['film_id']]['countries'][]=$obj['country_id'];
}
//================================================

//Персоны-------------------------------------------------------
$result['persons']=array();
if($persons)
{
	$query="
		SELECT
			prs.id,
			prs.lastname_orig,
			prs.firstname_orig,
			lng.lastname,
			lng.firstname
		FROM
			grifix_main_persons AS prs
		LEFT JOIN
			grifix_main_persons_lng AS lng
		ON
			lng.record_id=prs.id
		AND
			lng.lang_id=".$lang."
		WHERE
			prs.id IN(".implode(',',$persons).")
	";

	$r= mysql_query($query);
	while ($obj=mysql_fetch_assoc($r))
	{
		$obj['lastname']=htmlspecialchars($obj['lastname']);
		$obj['firstname']=htmlspecialchars($obj['firstname']);
		$obj['lastname_orig']=htmlspecialchars($obj['lastname_orig']);
		$obj['firstname_orig']=htmlspecialchars($obj['firstname_orig']);
		$result['persons'][]=$obj;
		$result['persons_ua'][]=$obj;
	}
}

if($persons)
{
	$query="
		SELECT
			prs.id,
			prs.lastname_orig,
			prs.firstname_orig,
			lng.lastname,
			lng.firstname
		FROM
			grifix_main_persons AS prs
		LEFT JOIN
			grifix_main_persons_lng AS lng
		ON
			lng.record_id=prs.id
		AND
			lng.lang_id=1
		WHERE
			prs.id IN(".implode(',',$persons).")
	";

	$r= mysql_query($query);
	while ($obj=mysql_fetch_assoc($r))
	{
		$obj['lastname']=htmlspecialchars($obj['lastname']);
		$obj['firstname']=htmlspecialchars($obj['firstname']);
		$result['persons_ru'][]=$obj;
	}
}
/**********************************************/
$sitemap_text='';
$sitemap_text.='<?xml version="1.0" encoding="UTF-8"?>
<billboard>
<films>';
foreach ($result['films'] as $film)
{
	$sitemap_text.='<film id="'.$film['id'].'">
				<title orig="'.$film['title_orig'].'">'.$film['title'].'</title>
				<persons>';
				foreach ($film['persons'] as $person)
				{
					$sitemap_text.='<person id="'.$person['person_id'].'" profession_id="'.$person['profession_id'].'"/>';
				}
				$sitemap_text.='</persons>
				<genres>'.implode(',',$film['genres']).'</genres>
				<countries>'.implode(',',$film['countries']).'</countries>
				<studios>'.implode(',',$film['studios']).'</studios>
			</film>';
}

$sitemap_text.='
</films>
<pr_ua>';
foreach ($result['persons_ua'] as $pr)
{
	$sitemap_text.='<pr id="'.$pr['id'].'">
						<lastname orig="'.$pr['lastname_orig'].'">'.$pr['lastname'].'</lastname>
						<firstname orig="'.$pr['firstname_orig'].'">'.$pr['firstname'].'</firstname>
					</pr>';
}
$sitemap_text.='</pr_ua>';

$sitemap_text.='<pr_ua>';
foreach ($result['persons_ru'] as $pr)
{
	$sitemap_text.='<pr id="'.$pr['id'].'">
						<lastname>'.$pr['lastname'].'</lastname>
						<firstname>'.$pr['firstname'].'</firstname>
					</pr>';
}
$sitemap_text.='</pr_ua>';
$sitemap_text.='</billboard>';
header("Content-Type: text/xml");
echo $sitemap_text;
?>