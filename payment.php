<?php
session_start();
if(isset($_GET['session_destroy'])){
	unset($_SESSION);
	session_destroy();
	exit;
}
function getmicrotime()
{
    list($usec, $sec) = explode(' ',microtime());
    return ((float)$usec + (float)$sec);
}

function setDateFormat()
{
	global $_cfg;
	$time_format=strftime('%X',mktime(1,2,3,4,5,2006));
	$time_format=str_replace('01','%H',$time_format);
	$time_format=str_replace('02','%M',$time_format);
	$time_format=str_replace('03','%S',$time_format);
	$date_format=strftime('%x',mktime(1,2,3,4,5,2006));

	$date_format=str_replace('04','%m',$date_format);
	$date_format=str_replace('05','%d',$date_format);
	$date_format=str_replace('2006','%Y',$date_format);
	$date_format=str_replace('06','%Y',$date_format);


	if(!$_cfg['sys::date_format'])
		$_cfg['sys::date_format']=$date_format;
	if(!$_cfg['sys::time_format'])
		$_cfg['sys::time_format']=$time_format;

	$_cfg['sys::date_time_format']=$date_format.' '.$time_format;
}

define('_MODE','frontend');
$start=getmicrotime();

//Подключить файл конфигурации
if(_MODE=='backend')
	require_once('../cfg.php');
elseif(_MODE=='frontend')
	require_once('cfg.php');
//=====================================//

//Подключить библиоткеку системных функций и класс работы с БД

require_once(_ROOT_DIR.'mods/sys/include/lib.sys.php');
require_once(_ROOT_DIR.'mods/sys/include/cls.db.php');

//Подключить основные библиотеки
if(_MODE=='backend')
	sys::useLib('sys::gui');
elseif(_MODE=='frontend')
	sys::useLib('main');
	
//=========================================//

$_db=new cls_sys_db();//создать объект для работы с БД
$_db->query("SET NAMES 'UTF8'"); //задать кодировку данных БД

//получить e-mail суперпользователя
$_cfg['sys::root_email']=$_db->getValue('sys_users','email',7);

//Стартовать сессию
if($_cfg['sys::session_in_db'])
	require_once(_ROOT_DIR.'mods/sys/include/lib.sess.php');
else
	session_save_path(_SESSION_DIR);
session_name($_cfg['sys::project_id'].'_'._MODE);
session_cache_expire($_cfg['sys::session_minutes']);
session_start();
//======================================================================//

//Проверить айпишник пользователя
if(isset($_SESSION['sys::user_ip']) && $_SESSION['sys::user_ip']!=$_SERVER['REMOTE_ADDR'])
	unset($_SESSION);
//====================================//

//Загрузить настройки модулей
$r=$_db->Query("SELECT CONCAT(mods.name, '::', cfg.name) AS `name`,
		cfg.value
		FROM `#__sys_mods_cfg` AS `cfg`
		LEFT JOIN `#__sys_mods` AS `mods`
		ON cfg.mod_id=mods.id");
while($cfg = $_db->fetchArray($r))
{
	$_cfg[$cfg['name']]=$cfg['value'];
}
unset($cfg);
if(_MODE=='backend') //Если админ панель
	$_cfg['sys::hurl']=false;//Отключить ЧПУ
	

//================================================//

//Загрузить системные языки
$r=$_db->query("SELECT `id`, `code`, `title`, `short_title`
		FROM `#__sys_langs`
		WHERE `on`=1
		ORDER BY `order_number`");
$_langs=array();
while($lang=$_db->fetchAssoc($r))
	$_langs[$lang['id']]=$lang;

$lang_id = (int)$_GET['lang_id'];
//Загрузить переменные языка
$r=$_db->query("SELECT * FROM `#__sys_langs`
					WHERE `id`=$lang_id");
$lang=$_db->fetchAssoc($r);
$_cfg['sys::lang_id']=$lang['id'];
$_cfg['sys::from']=$lang['robot_title'];
$_cfg['sys::mail_charset']=$lang['mail_charset'];
$langs = array(
1 => 'ru',
2 => 'en',
3 => 'uk'
);
$_cfg['sys::lang'] = $langs[$lang_id];
sys::useLib('main::payment');
sys::useLib('main::basket');
sys::useLib('main::orders');
sys::useLib('main::logging');
$langs = array(
1 => 'ru',
2 => 'en',
3 => 'uk'
);
$_cfg['sys::lang'] = $langs[$lang_id];

//if(isset($_GET['logging'])){
//	main_logging::write_megakino_queries('Check','Query1','Query2');
//	exit;
//}

if(isset($_REQUEST['lang']))
	$_cfg['sys::lang'] = $_REQUEST['lang'];
	
if(isset($_REQUEST['lang_id']))
	$_cfg['sys::lang_id'] = $_REQUEST['lang_id'];


//Проверка флага первого визита
if(!isset($_SESSION['sys::user_id']))
	$_first_visit=true;
//===================================================//

//Определить включены ли кукисы
if(isset($_COOKIE['sys::test']))
	$_cookie=true;
sys::setCookie('sys::test',1);
//==============================================================================//

//Авторизация по кукисам
if(_MODE=='frontend' && (!isset($_SESSION['sys::user_id']) || $_SESSION['sys::user_id']==2))
{
	if(isset($_COOKIE['sys::login']) && isset($_COOKIE['sys::password']))
	{
		sys::authorizeUser($_COOKIE['sys::login'],$_COOKIE['sys::password'],true);
	}
}
//Проверка пользователя
if(!sys::checkSessionUser())	//Если пользователь не прошел проверку
	sys::registerUser(2);		//Загегистрировать в системе гостя
//var_dump($_SESSION);
//var_dump($_COOKIE);
sys::useLang('main'); //Подключить языковой файл модуля


$method = $_REQUEST['method'];
$status = $_REQUEST['status'];

if(empty($_SESSION['session_id']) || time() > $_SESSION['session_expire']){
	
	if( $curl = curl_init() ) {
    	curl_setopt($curl, CURLOPT_URL, $author_url);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    	curl_setopt($curl, CURLOPT_POST, true);
    	curl_setopt($curl, CURLOPT_POSTFIELDS, $author_params);
    	$out = curl_exec($curl);
		curl_close($curl);
		$session_id = trim($out);
		if($method == 'lock'){
			main_basket::set_eventmap($session_id);
		}
		if($session_id){
			$_SESSION['session_id'] = $session_id;
			$_SESSION['session_expire'] = time() + (60 * 15);
		}
	}
}
else{
	$session_id = $_SESSION['session_id'];
}

//main_payment::$test_payment = true;
//main_payment::$debug_mode = true;
$is_invite = false;

switch($method){
	case 'deleteCard':
	$response = main_payment::deleteCard($_REQUEST['card_alias']);
	break;
	case 'checkCard':
		$response = main_payment::Check();
	break;
	case 'getUrl':
		if($status == 'notexists'){
			$response = main_payment::registerByUrl();
		}
		elseif($status == 'exists'){
			$response = main_payment::addByUrl();
		}
		elseif($status == 'invite'){
			$response = main_payment::InviteByUrl();
			$is_invite = true;
		}
	break;
	case 'PaymentCreate':
		if(!$session_id){
			if($_GET['session_id'])
				$session_id = $_GET['session_id'];
			else
				$session_id = main_basket::connect();

		}
		if($session_id){
			$basket = main_basket::basket_list($session_id);
			$response = main_payment::PaymentCreate($_REQUEST['card_alias'],$basket['sum']*100);
			$buffer = json_decode($response,true);
			if(isset($buffer['response']['error'])){
				$result['error'] = $buffer['response']['error'];
				echo json_encode($result);
				exit;
			}
			else{
				if($buffer['response']['pmt_status'] == '1'){
					$_REQUEST['pmt_id'] = $buffer['response']['pmt_id'];
					$_REQUEST['invoice'] = $buffer['response']['invoice'];
					$response = main_payment::PaymentSale();
					$buffer_sale = json_decode($response,true);
					if(empty($buffer_sale['response']['error'])){
						if(!$session_id)
							$session_id = main_basket::connect();
						$basket = main_basket::basket_list($session_id);
						createOrder($basket,$session_id,$buffer_sale);
				
					}
					exit;
				}
				elseif($buffer['response']['pmt_status'] == '4'){
					$result['error'] = 'Платеж не прошел!';
					echo json_encode($result);
					exit;
				}
				if($buffer['response']['secure'] == 'otp'){
					$result['type'] = 'otp';
					$result['html'] = main_payment::resultPaymentCreate($buffer['response']);
				}
				elseif($buffer['response']['secure'] == '3ds'){
					$result['type'] = '3ds';
					$result['form_url'] = $buffer['response']['ascUrl'];
					$result['pareq'] = $buffer['response']['pareq'];
					$result['md'] = $buffer['response']['md'];
					if($_SERVER['HTTP_HOST'] == 'kino-teatr.ua')
						$protocol = 'https';
					else
						$protocol = 'http';

					$result['term_url'] = $protocol.'://'.$_SERVER['HTTP_HOST'].'/payment.php?method=3DS&pmt_id='.$buffer['response']['pmt_id'].'&film_id='.$_REQUEST['film_id'].'&hall_id='.$_REQUEST['hall_id'].'&cinema_id='.$_REQUEST['cinema_id'].'&site_id='.$_REQUEST['site_id'].'&lang_id='.$_REQUEST['lang_id'];
				}
				else{
					$result['type'] = 'without_verification';
					$basket = main_basket::basket_list($session_id);
					createOrder($basket,$session_id,$buffer);
				}
				$basket['info'] = $response;
				$result['user_id'] = $_user['id'];
				$basket['session_id'] = $session_id;
				$result['response'] = $response;
				$result['user_id'] = $_user['id'];
				$result['orderId'] = $buffer['response']['pmt_id'];
				echo json_encode($result);
				exit;
			}
			
		}
		
	break;
	case 'PaymentCancel':
		$response = main_payment::PaymentCancel();
		$buffer = json_decode($response,true);
		$result['status'] = $buffer['response']['pmt_status'];
		if($result['status'] == 9){
			main_basket::cancel_order($_REQUEST['megakino_id']);
//			if(!$session_id){
//				if($_GET['session_id'])
//					$session_id = $_GET['session_id'];
//				else
					$session_id = main_basket::connect();

//			}
//		$refund_url = '/basket.php?method=refund&orderId='.$_REQUEST['megakino_id'].'&site_id='.$_REQUEST['site_id'];
		$refund_url = 'http://server.megakino.com.ua/gate/refund?sessionid='.$session_id.'&orderId='.$_REQUEST['megakino_id'].'&siteId='.$_REQUEST['site_id'];
//		echo 'refund_url '.$refund_url.'<br>';
		$result['response'] = $response;
		$result['megakino_response'] = file_get_contents($refund_url);
		main_logging::write_megakino_queries('Refund',$refund_url,$result['megakino_response']);
		file_get_contents('/basket.php?session_destroy=1');
		}
		echo json_encode($result);
		exit;
	break;
	case 'Otp':
		$response = main_payment::Otp();
		$otp_response = $response;
		$buffer = json_decode($response,true);
		if(empty($buffer['response']['error'])){
			if($buffer['response']['pmt_status'] == 1){
				$response = main_payment::PaymentSale();
				$buffer_sale = json_decode($response,true);
				if(empty($buffer_sale['response']['error'])){
					if(!$session_id)
						$session_id = main_basket::connect();
					$basket = main_basket::basket_list($session_id);
					createOrder($basket,$session_id,$buffer_sale);
//					$items = json_decode($basket['info'],true);
//					$result['html'] = main_basket::get_order_info($items,'buying');
				
				}
				$result['sale'] = $response;
				echo json_encode($result);
				exit;
			}
			else{
				$result['error'] = $buffer['response']['status'];
				echo json_encode($result);
				exit;
			}
		}
		$result['otp'] = $otp_response;
		echo json_encode($result);
		exit;
	break;
	case '3DS':
		$response = main_payment::ThreeDS();
		$ds_response = $response;
		$buffer = json_decode($response,true);

		if(empty($buffer['response']['error'])){
			if($buffer['response']['pmt_status'] == 1){
				$response = main_payment::PaymentSale();
				$buffer_sale = json_decode($response,true);
				if(empty($buffer_sale['response']['error'])){
					if(!$session_id)
						$session_id = main_basket::connect();
					$basket = main_basket::basket_list($session_id);
					createOrder($basket,$session_id,$buffer_sale);
					$result['sale'] = $response;
					$redirect_url = '/'.$_cfg['sys::lang'].'/main/user_profile_orders/user_id/'.$_user['id'].'.phtml';

//					echo $redirect_url;exit;
//					unset($_SESSION['url_error_3ds']);
					header('Location: '.$redirect_url);
				}
				else{
					if(isset($_SESSION['url_error_3ds'])){
						$return_url = str_replace('https://','',$_SESSION['url_error_3ds']);
						$return_url = str_replace('http://','',$return_url);
						$buffer = explode('/',$return_url);
						array_shift($buffer);
						$return_url = '/'.implode('/',$buffer);
						header('Location: '.$return_url);
					}
				}
				

			}
			else{
				if(isset($_SESSION['url_error_3ds'])){
					$return_url = str_replace('https://','',$_SESSION['url_error_3ds']);
					$return_url = str_replace('http://','',$return_url);
					$buffer = explode('/',$return_url);
					array_shift($buffer);
					$return_url = '/'.implode('/',$buffer);
					header('Location: '.$return_url);
				}
				
			}
		}
		else{
			echo '<meta charset="utf-8" />';
			echo '<h2>Ошибка верификации</h2>';
			echo '<h3><a href="/">Вернуться на главную страницу сайта</a> </h3>';
			exit;
		}
		$result['3ds'] = $ds_response;
		echo json_encode($result);
		exit;
	
	break;
	case 'Print':
		$megakino_id = $_REQUEST['megakino_id'];
		$result = main_orders::getOrder($_REQUEST['megakino_id']);
		if(isset($_REQUEST['pdf'])){
			main_orders::createPdf($result,$_REQUEST['megakino_id'],'Билет на фильм "'.$result['film_name'].'"');
		}
		else{
			$html = sys::parseTpl('main::_megakino_ticket_widget',$result);
			echo $html;
			exit;
		}
		
	break;
}
	if(isset($response) && $response){
		$result = json_decode($response,true);
		$result['response']['is_invite'] = $is_invite;
		if(isset($result['response']))
			$response = $result['response'];
		elseif(isset($result['error']))
			$response = $result['error'];

		echo json_encode($response);
		exit;
	}
	else{
		$response['error'] = 'Неизвестная ошибка';
		echo json_encode($response);
		exit;
	}
	
function createOrder($basket,$session_id,$info_sale){
	global $_user;
	$url = 'http://server.megakino.com.ua/gate/basket-reserve?sessionid='.$session_id;
			$response = file_get_contents($url);
			$buffer = json_decode($response,true);
//			$basket = basket_list($session_id);
			$basket = main_basket::basket_list($session_id);
			$site_id = $_GET['site_id'];
			$event_id = $_GET['event_id'];
			$show_id = $_GET['show_id'];
			if($buffer['code'] == 0){
				$params = '&phone='.$_user['main_phone'];
				$params .= '&paymentInfo=card';
//				if(isset($_REQUEST['description']) && $_REQUEST['description'])
//					$params .= '&description='.$_REQUEST['description'];
				$buying_url = 'http://server.megakino.com.ua/gate/basket-buy?sessionid='.$session_id.$params;
//				$buying_url = 'http://server.megakino.com.ua/gate/basket-book?sessionid='.$session_id.$params;
				$response_buying = file_get_contents($buying_url);
				$buffer_buying = json_decode($response_buying,true);
				main_logging::write_megakino_queries('Buying',$buying_url,$response_buying);
				if($buffer_buying['code'] == 0){
					$basket['info']=$response_buying;
					$res_basket=main_basket::buying($basket,$buffer_buying['orderId'],$info_sale);
//					if($res_basket){
//					}
					if($res_basket && $_REQUEST['method'] == '3DS'){
						if($res_basket)
							main_basket::sendPdf($buffer_buying['orderId']);
						return true;
					}
					$result['user_id'] = $_user['id'];
					$result['code'] = $buffer_buying['code'];
					$result['url'] = $buying_url;
					$result['orderId'] = $buffer_buying['orderId'];
					$url_print = '/payment.php?method=Print&megakino_id='.$buffer_buying['orderId'].'&lang_id='.$_REQUEST['lang_id'];
					$result['html'] = main_basket::get_order_info($buffer_buying,'buying',$url_print);
					$result['status'] = $info_sale['response']['pmt_status'];
					$result['pmt_id'] = $info_sale['response']['pmt_id'];
					$result['megakino_id'] = $buffer_buying['orderId'];
					if($_REQUEST['method'] == 'PaymentCreate')
						$result['type'] = 'without_verification';
					else
						$result['type'] = 'with_verification';
					if($res_basket)
						main_basket::sendPdf($buffer_buying['orderId']);
//					var_dump($result);

					echo json_encode($result);
					exit;
				}
				else{
					$result['code'] = 55;
					$result['url'] = $buying_url;
					$result['message'] = $buffer['message'];
					echo json_encode($result);
					exit;
				}
			}
			else{
				$result['code'] = 55;
				echo json_encode($result);
				exit;
			}
	
}

?>