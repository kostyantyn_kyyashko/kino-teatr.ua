<?php
function getmicrotime()
{ 
    list($usec, $sec) = explode(' ',microtime()); 
    return ((float)$usec + (float)$sec); 
}
$start=getmicrotime(); 
ob_start("ob_gzhandler");
define('_MODE','backend');
require_once('../init.php');

if($_cfg['sys::timer'] && $_tpl)
{
	?>
	<center>
		<?=sys::translate('sys::execution_time')?>: <?=number_format(getmicrotime()-$start,2)?> <?=sys::translate('sys::seconds')?>
	</center>
	<? 
}

if($_cfg['sys::memory_usage'] && $_tpl)
{
	?>
	<center>
		<?=sys::translate('sys::memory_usage')?>: <?=number_format(memory_get_peak_usage()/1048576,3,'.',' ');?> <?=sys::translate('sys::megabytes');?>
	</center>
	<?
}
ob_end_flush();
?>