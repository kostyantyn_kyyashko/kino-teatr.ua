<?php

session_start();
if(isset($_GET['session_destroy'])){
	unset($_SESSION);
	session_destroy();
	exit;
}
function getmicrotime()
{
    list($usec, $sec) = explode(' ',microtime());
    return ((float)$usec + (float)$sec);
}

function setDateFormat()
{
	global $_cfg;
	$time_format=strftime('%X',mktime(1,2,3,4,5,2006));
	$time_format=str_replace('01','%H',$time_format);
	$time_format=str_replace('02','%M',$time_format);
	$time_format=str_replace('03','%S',$time_format);
	$date_format=strftime('%x',mktime(1,2,3,4,5,2006));

	$date_format=str_replace('04','%m',$date_format);
	$date_format=str_replace('05','%d',$date_format);
	$date_format=str_replace('2006','%Y',$date_format);
	$date_format=str_replace('06','%Y',$date_format);


	if(!$_cfg['sys::date_format'])
		$_cfg['sys::date_format']=$date_format;
	if(!$_cfg['sys::time_format'])
		$_cfg['sys::time_format']=$time_format;

	$_cfg['sys::date_time_format']=$date_format.' '.$time_format;
}

define('_MODE','frontend');
$start=getmicrotime();

//Подключить файл конфигурации
if(_MODE=='backend')
	require_once('../cfg.php');
elseif(_MODE=='frontend')
	require_once('cfg.php');
//=====================================//

//Подключить библиоткеку системных функций и класс работы с БД

require_once(_ROOT_DIR.'mods/sys/include/lib.sys.php');
require_once(_ROOT_DIR.'mods/sys/include/cls.db.php');

//Подключить основные библиотеки
if(_MODE=='backend')
	sys::useLib('sys::gui');
elseif(_MODE=='frontend')
	sys::useLib('main');
	
//=========================================//

$_db=new cls_sys_db();//создать объект для работы с БД
$_db->query("SET NAMES 'UTF8'"); //задать кодировку данных БД

//получить e-mail суперпользователя
$_cfg['sys::root_email']=$_db->getValue('sys_users','email',7);

//Стартовать сессию
if($_cfg['sys::session_in_db'])
	require_once(_ROOT_DIR.'mods/sys/include/lib.sess.php');
else
	session_save_path(_SESSION_DIR);
session_name($_cfg['sys::project_id'].'_'._MODE);
session_cache_expire($_cfg['sys::session_minutes']);
session_start();
//======================================================================//

sys::useLib('main::payment');

//Проверить айпишник пользователя
if(isset($_SESSION['sys::user_ip']) && $_SESSION['sys::user_ip']!=$_SERVER['REMOTE_ADDR'])
	unset($_SESSION);
//====================================//

//Загрузить настройки модулей
$r=$_db->Query("SELECT CONCAT(mods.name, '::', cfg.name) AS `name`,
		cfg.value
		FROM `#__sys_mods_cfg` AS `cfg`
		LEFT JOIN `#__sys_mods` AS `mods`
		ON cfg.mod_id=mods.id");
while($cfg = $_db->fetchArray($r))
{
	$_cfg[$cfg['name']]=$cfg['value'];
}
unset($cfg);
if(_MODE=='backend') //Если админ панель
	$_cfg['sys::hurl']=false;//Отключить ЧПУ
	

//================================================//

//Загрузить системные языки
$r=$_db->query("SELECT `id`, `code`, `title`, `short_title`
		FROM `#__sys_langs`
		WHERE `on`=1
		ORDER BY `order_number`");
$_langs=array();
while($lang=$_db->fetchAssoc($r))
	$_langs[$lang['id']]=$lang;

$lang_id = (int)$_GET['lang_id'];
//Загрузить переменные языка
$r=$_db->query("SELECT * FROM `#__sys_langs`
					WHERE `id`=$lang_id");
$lang=$_db->fetchAssoc($r);
$_cfg['sys::lang_id']=$lang['id'];
$_cfg['sys::from']=$lang['robot_title'];
$_cfg['sys::mail_charset']=$lang['mail_charset'];
$langs = array(
1 => 'ru',
2 => 'en',
3 => 'uk'
);
$_cfg['sys::lang'] = $langs[$lang_id];
sys::useLib('main::basket');
sys::useLib('main::logging');



//Проверка флага первого визита
if(!isset($_SESSION['sys::user_id']))
	$_first_visit=true;
//===================================================//

//Определить включены ли кукисы
if(isset($_COOKIE['sys::test']))
	$_cookie=true;
sys::setCookie('sys::test',1);
//==============================================================================//

//Авторизация по кукисам
if(_MODE=='frontend' && (!isset($_SESSION['sys::user_id']) || $_SESSION['sys::user_id']==2))
{
	if(isset($_COOKIE['sys::login']) && isset($_COOKIE['sys::password']))
	{
		sys::authorizeUser($_COOKIE['sys::login'],$_COOKIE['sys::password'],true);
	}
}
//Проверка пользователя
if(!sys::checkSessionUser())	//Если пользователь не прошел проверку
	sys::registerUser(2);		//Загегистрировать в системе гостя
//var_dump($_SESSION);
//var_dump($_COOKIE);
sys::useLang('main'); //Подключить языковой файл модуля
$method = $_REQUEST['method'];
$url = 'http://server.megakino.com.ua/gate/'.$method;
$author_url = 'http://server.megakino.com.ua/gate/login';
$author_params = 'username=kino-teatr.ua&password=vdfv78464rwcs5g34';
// var_dump($_SESSION);
//if(empty($_SESSION['session_id']) || time() > $_SESSION['session_expire'] || $method == 'basket-buy'){
if(empty($_SESSION['session_id']) || time() > $_SESSION['session_expire']){
	
	if( $curl = curl_init() ) {
    	curl_setopt($curl, CURLOPT_URL, $author_url);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    	curl_setopt($curl, CURLOPT_POST, true);
    	curl_setopt($curl, CURLOPT_POSTFIELDS, $author_params);
    	$out = curl_exec($curl);
		curl_close($curl);
		$session_id = trim($out);
		if($method == 'lock'){
			main_basket::set_eventmap($session_id);
		}
		if($session_id){
			$_SESSION['session_id'] = $session_id;
			$_SESSION['session_expire'] = time() + (60 * 15);
		}
	}
}
else{
	$session_id = $_SESSION['session_id'];
}
if($session_id){
	switch($method){
		case 'lock':
			$site_id = $_GET['site_id'];
			$event_id = $_GET['event_id'];
			$place_id = $_GET['place_id'];
			$url .= '?sessionid='.$session_id.'&siteId='.$site_id.'&eventId='.$event_id.'&placeId='.$place_id;
//			echo 'url '.$url.'<br>';
			$response = file_get_contents($url);
//			echo 'response '.$response.'<br>';
//			exit;
			$buffer = json_decode($response,true);
//			var_dump($buffer);
			/*
			if(is_null($bufer)){
				$session_id = main_basket::connect();
				$_SESSION['session_id'] = $session_id;
				$url = 'http://server.megakino.com.ua/gate/'.$method;
				$url .= '?sessionid='.$session_id.'&siteId='.$site_id.'&eventId='.$event_id.'&placeId='.$place_id;
				$response = file_get_contents($url);
				$buffer = json_decode($response,true);
			}
			*/
//			var_dump($buffer);exit;
		break;
		case 'unlock':
			$site_id = $_GET['site_id'];
			$event_id = $_GET['event_id'];
			$place_id = $_GET['place_id'];
			$url .= '?sessionid='.$session_id.'&siteId='.$site_id.'&eventId='.$event_id.'&placeId='.$place_id;
			$response = file_get_contents($url);
			$buffer = json_decode($response,true);
		break;
		case 'basket-clean':
			$site_id = $_GET['site_id'];
			$url .= '?sessionid='.$session_id.'&siteId='.$site_id;
			$response = file_get_contents($url);
			$buffer = json_decode($response,true);
			unset($_SESSION['session_id']);
			unset($_SESSION['session_expire']);
			break;
		case 'cancel-order':
			$order_id = $_GET['orderId'];
			$site_id = $_GET['site_id'];
			$session_id = main_basket::connect();
			$url = 'http://server.megakino.com.ua/gate/cancel-order?sessionid='.$session_id.'&orderId='.$order_id.'&siteId='.$site_id;
			$response = file_get_contents($url);
			$buffer = json_decode($response,true);
			main_logging::write_megakino_queries('Cancel-order',$url,$response);
			if($buffer['code'] == 0)
				main_basket::cancel_order($order_id);
			unset($_SESSION['session_id']);
			unset($_SESSION['session_expire']);
			echo $response;
			exit;
		break;
		case 'refund':
			$order_id = $_GET['orderId'];
			$site_id = $_GET['site_id'];
			$url = 'http://server.megakino.com.ua/gate/refund?sessionid='.$session_id.'&orderId='.$order_id+'&siteId='.$site_id;
			$response = file_get_contents($url);
			if($response){
				main_logging::write_megakino_queries('Refund',$url,$response);
			}
			$buffer = json_decode($response,true);
			unset($_SESSION['session_id']);
			unset($_SESSION['session_expire']);			
			echo $response;
			exit;
		break;
		case 'basket-book':
			$url = 'http://server.megakino.com.ua/gate/basket-reserve?sessionid='.$session_id;
			$response = file_get_contents($url);
			$buffer = json_decode($response,true);
//			$basket = basket_list($session_id);
			$basket = main_basket::basket_list($session_id);
			$site_id = $_GET['site_id'];
			$event_id = $_GET['event_id'];
			$show_id = $_GET['show_id'];
			if($buffer['code'] == 0){
				$params = '';
				if(isset($_REQUEST['name']) && $_REQUEST['name'])
					$params .= '&name='.$_REQUEST['name'];
				if(isset($_REQUEST['email']) && $_REQUEST['email'])
					$params .= '&email='.$_REQUEST['email'];
				if(isset($_REQUEST['phone']) && $_REQUEST['phone'])
					$params .= '&phone='.$_REQUEST['phone'];
				if(isset($_REQUEST['description']) && $_REQUEST['description'])
					$params .= '&description='.$_REQUEST['description'];
				$booking_url = 'http://server.megakino.com.ua/gate/basket-book?sessionid='.$session_id.$params;
				$response_booking = file_get_contents($booking_url);
				$buffer_booking = json_decode($response_booking,true);
				if($buffer_booking['code'] == 0){
//					main_basket::booking($basket,$buffer_booking['orderId']);
					main_basket::booking($basket,$buffer_booking);
					main_logging::write_megakino_queries('Booking',$booking_url,$response_booking);
					$result['user_id'] = $_user['id'];
					$result['code'] = $buffer_booking['code'];
					$result['url'] = $booking_url;
					$result['orderId'] = $buffer_booking['orderId'];
//					$url_print = '/payment.php?method=Print&megakino_id='.$buffer_booking['orderId'].'&lang_id='.$lang_id;
					$result['html'] = main_basket::get_order_info($buffer_booking,'booking');
					file_get_contents('/basket.php?session_destroy=1');
					echo json_encode($result);
					exit;
				}
				else{
					$result['code'] = 55;
					$result['url'] = $booking_url;
					$result['message'] = $buffer['message'];
					echo json_encode($result);
					exit;
				}
			}
			else{
				$result['code'] = 55;
				echo json_encode($result);
				exit;
			}
		break;
		case 'basket-buy':
			$url = 'http://server.megakino.com.ua/gate/basket-reserve?sessionid='.$session_id;
			$response = file_get_contents($url);
			$buffer = json_decode($response,true);
//			$basket = basket_list($session_id);
			$basket = main_basket::basket_list($session_id);
			$site_id = $_GET['site_id'];
			$event_id = $_GET['event_id'];
			$show_id = $_GET['show_id'];
			if($buffer['code'] == 0){
				$params = '&phone='.$_user['main_phone'];
				$params .= '&paymentInfo=card';
//				if(isset($_REQUEST['description']) && $_REQUEST['description'])
//					$params .= '&description='.$_REQUEST['description'];
				$buying_url = 'http://server.megakino.com.ua/gate/basket-buy?sessionid='.$session_id.$params;
				$response_buying = file_get_contents($buying_url);
				$buffer_buying = json_decode($response_buying,true);
				if($buffer_buying['code'] == 0){
					main_basket::buying($basket,$buffer_buying['orderId']);
					$result['user_id'] = $_user['id'];
					$result['code'] = $buffer_buying['code'];
					$result['url'] = $buying_url;
					$result['orderId'] = $buffer_buying['orderId'];
					$result['html'] = main_basket::get_order_info($buffer_buying);
					file_get_contents('/basket.php?session_destroy=1');
					echo json_encode($result);
					exit;
				}
				else{
					$result['code'] = 55;
					$result['url'] = $buying_url;
					$result['message'] = $buffer['message'];
					echo json_encode($result);
					exit;
				}
			}
			else{
				$result['code'] = 55;
				echo json_encode($result);
				exit;
			}
		break;
		
	}
//		$buffer = json_decode($response,true);
//		$result['basket'] = get_basket($buffer['items']);
		$result['basket'] = main_basket::get_basket($buffer['items']);
		$result['response'] = $response;
		$result['session_id'] = $session_id;
		$result['code'] = $buffer['code'];
		$result['url'] = $url;
		$result['user_id'] = $_user['id'];
		echo json_encode($result);
		exit;
}



?>