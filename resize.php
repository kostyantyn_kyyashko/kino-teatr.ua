<?php

// f - имя файла
// type - способ масштабирования
// q - качество сжатия
// src - исходное изображение
// dest - результирующее изображение
// w - ширниа изображения
// ratio - коэффициент пропорциональности
// str - текстовая строка


$from_path = '/var/www/html/multiplex/multiplex.in.ua/public/main/films/'.$_REQUEST['f'];

$to_width = $_REQUEST['w'];

		if(!is_file($from_path))
			return false;


		$image_info = getImageSize($from_path);
		$from_width = $image_info[0];
		$from_height = $image_info[1];

		$format=exif_imagetype($from_path);
		if($format==IMAGETYPE_GIF)
			$format='gif';
		elseif($format==IMAGETYPE_JPEG)
			$format='jpeg';
		elseif($format==IMAGETYPE_PNG)
			$format='png';
		else
			return false;


		if(!$to_path)
			$to_path='/var/www/html/multiplex/multiplex.in.ua/'.$_REQUEST['f'];

		switch($format)
		{
			case 'jpeg':
				$from_image=imagecreatefromjpeg($from_path);
			break;

			case 'gif':
				$from_image=imagecreatefromgif($from_path);
			break;

			case 'png':
				$from_image=imagecreatefrompng($from_path);
			break;

			default:
				return false;
		}


		$ratio = $from_width/$to_width;


		$to_width = $from_width/$ratio;
		$to_height = $from_height/$ratio;

		if('png'==$format)
			$to_image = imageCreate($to_width, $to_height);
		else
			$to_image = imageCreateTrueColor($to_width, $to_height);

		imageCopyResampled($to_image, $from_image, 0, 0, 0, 0, $to_width, $to_height, $from_width, $from_height);
		header("Content-type: image/jpeg");

		switch($format)
		{
			case 'jpeg':
				imagejpeg($to_image, '',80);
			break;

			case 'gif':
				imagegif($to_image, '');
			break;

			case 'png':
				imagepng($to_image, '');
			break;

			default:
				return false;
		}
	imagedestroy($to_image);
	imagedestroy($from_image);


?>