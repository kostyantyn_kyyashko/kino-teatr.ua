<?php

function getmicrotime()
{
    list($usec, $sec) = explode(' ',microtime());
    return ((float)$usec + (float)$sec);
}

function setDateFormat()
{
	global $_cfg;
	$time_format=strftime('%X',mktime(1,2,3,4,5,2006));
	$time_format=str_replace('01','%H',$time_format);
	$time_format=str_replace('02','%M',$time_format);
	$time_format=str_replace('03','%S',$time_format);
	$date_format=strftime('%x',mktime(1,2,3,4,5,2006));

	$date_format=str_replace('04','%m',$date_format);
	$date_format=str_replace('05','%d',$date_format);
	$date_format=str_replace('2006','%Y',$date_format);
	$date_format=str_replace('06','%Y',$date_format);


	if(!$_cfg['sys::date_format'])
		$_cfg['sys::date_format']=$date_format;
	if(!$_cfg['sys::time_format'])
		$_cfg['sys::time_format']=$time_format;

	$_cfg['sys::date_time_format']=$date_format.' '.$time_format;
}

define('_MODE','frontend');
$start=getmicrotime();

//Подключить файл конфигурации
if(_MODE=='backend')
	require_once('../cfg.php');
elseif(_MODE=='frontend')
	require_once('cfg.php');
//=====================================//

//Подключить библиоткеку системных функций и класс работы с БД

require_once(_ROOT_DIR.'mods/sys/include/lib.sys.php');
require_once(_ROOT_DIR.'mods/sys/include/cls.db.php');

//Подключить основные библиотеки
if(_MODE=='backend')
	sys::useLib('sys::gui');
elseif(_MODE=='frontend')
	sys::useLib('main');
	
//=========================================//

$_db=new cls_sys_db();//создать объект для работы с БД
$_db->query("SET NAMES 'UTF8'"); //задать кодировку данных БД



//получить e-mail суперпользователя
$_cfg['sys::root_email']=$_db->getValue('sys_users','email',7);

//Стартовать сессию
if($_cfg['sys::session_in_db'])
	require_once(_ROOT_DIR.'mods/sys/include/lib.sess.php');
else
	session_save_path(_SESSION_DIR);
session_name($_cfg['sys::project_id'].'_'._MODE);
session_cache_expire($_cfg['sys::session_minutes']);
session_start();

//======================================================================//

//Проверить айпишник пользователя
if(isset($_SESSION['sys::user_ip']) && $_SESSION['sys::user_ip']!=$_SERVER['REMOTE_ADDR'])
	unset($_SESSION);
//====================================//

//Загрузить настройки модулей
$r=$_db->Query("SELECT CONCAT(mods.name, '::', cfg.name) AS `name`,
		cfg.value
		FROM `#__sys_mods_cfg` AS `cfg`
		LEFT JOIN `#__sys_mods` AS `mods`
		ON cfg.mod_id=mods.id");
while($cfg = $_db->fetchArray($r))
{
	$_cfg[$cfg['name']]=$cfg['value'];
}
unset($cfg);
if(_MODE=='backend') //Если админ панель
	$_cfg['sys::hurl']=false;//Отключить ЧПУ
	

//================================================//

//Загрузить системные языки
$r=$_db->query("SELECT `id`, `code`, `title`, `short_title`
		FROM `#__sys_langs`
		WHERE `on`=1
		ORDER BY `order_number`");
$_langs=array();
while($lang=$_db->fetchAssoc($r))
	$_langs[$lang['id']]=$lang;

$lang_id = (int)$_GET['lang_id'];
//Загрузить переменные языка
$r=$_db->query("SELECT * FROM `#__sys_langs`
					WHERE `id`=$lang_id");
$lang=$_db->fetchAssoc($r);
$_cfg['sys::lang_id']=$lang['id'];
$_cfg['sys::from']=$lang['robot_title'];
$_cfg['sys::mail_charset']=$lang['mail_charset'];
$langs = array(
1 => 'ru',
2 => 'en',
3 => 'uk'
);
$_cfg['sys::lang'] = $langs[$lang_id];
sys::useLib('main::basket');



//Проверка флага первого визита
if(!isset($_SESSION['sys::user_id']))
	$_first_visit=true;
//===================================================//

//Определить включены ли кукисы
if(isset($_COOKIE['sys::test']))
	$_cookie=true;
sys::setCookie('sys::test',1);
//==============================================================================//

//Авторизация по кукисам
if(_MODE=='frontend' && (!isset($_SESSION['sys::user_id']) || $_SESSION['sys::user_id']==2))
{
	if(isset($_COOKIE['sys::login']) && isset($_COOKIE['sys::password']))
	{
		sys::authorizeUser($_COOKIE['sys::login'],$_COOKIE['sys::password'],true);
	}
}
//Проверка пользователя
if(!sys::checkSessionUser())	//Если пользователь не прошел проверку
	sys::registerUser(2);		//Загегистрировать в системе гостя
//var_dump($_SESSION);
//var_dump($_COOKIE);
sys::useLang('main'); //Подключить языковой файл модуля



if(isset($_POST['object_id']) && isset($_POST['object_type'])){
	discuss($_POST['object_id'],$_POST['object_type'],$_POST['chpu_base_url']);
}

function discuss($object_id, $object_type, $chpu_base_url=false)
	{
		if(!$object_id) return "";
		
		global $_db, $_cfg, $_user;
		sys::useLib('main::users');
		
			$_POST['text']=trim($_POST['text']);
			if($_user['id']==2 && !$_POST['name'])
				$_POST['name']=sys::translate('main::guest');
			$_POST['name']=strip_tags($_POST['name']);
			// Добавить можно любые строчки. Если строчка найдена в посте, то пост не пройдет.
			$filter_keywords = array();
			$filter_keywords[] = 'kino';
			$filter_keywords[] = 'ПОЛФИЛЬМА ВЫРЕЗАНО';
			$filter_keywords[] = 'сюжет - супер!';
			$filter_keywords[] = '80aqffbe6ay';
			$filter_keywords[] = 'filmgid.ga';
			$filter_keywords[] = 'p1ai';
			$filter_keywords[] = 'filmpro';
			$filter_keywords[] = 'FILMPRO';
			$filter_keywords[] = '.GA';
			$filter_keywords[] = '. GA';
			$filter_keywords[] = 'МАКСФИЛМ';
			$filter_keywords[] = '/film/';
			$filter_keywords[] = '===';
			$filter_keywords[] = 'malohit';
			$filter_keywords[] = 'brend';
			$filter_keywords[] = '.cf';
			$filter_keywords[] = 'color';
			$filter_keywords[] = 'hdmovie';
			$filter_keywords[] = 'online';
			$filter_keywords[] = 'ONLINE';
			$filter_keywords[] = 'GO';
			$filter_keywords[] = 'pub';
			$filter_keywords[] = '720';
			$filter_keywords[] = 'parkos';
			$filter_keywords[] = 'com';
			$filter_keywords[] = 'tinyurl';
			$filter_keywords[] = 'vk';
			$filter_keywords[] = 'cc';
			$filter_keywords[] = 'bit';
			$filter_keywords[] = 'aridan-spb';
			$filter_keywords[] = '1ru.in';
			$filter_keywords[] = '.ac';
			$filter_keywords[] = 'blogspot';
			$filter_keywords[] = '8b.kz';
			$filter_keywords[] = 'is.gd';
			$filter_keywords[] = 'is.qd';
			$filter_keywords[] = 'u.to';
			$filter_keywords[] = '<';
			$filter_keywords[] = '[url';
			$filter_keywords[] = 'http';
			$filter_keywords[] = 'смотрим здесь';
			$bad_filter = false;
			foreach ($filter_keywords as $value) 
				if(mb_strpos($_POST['text'],$value)!==false)
				{
					$bad_filter = true;
					break;
				}		
						

			if($_POST['text'] && ($bad_filter==false))
			{
				$_POST['text']=mb_substr(trim($_POST['text']),0,$_cfg['main::message_limit']);	

				switch(mysql_real_escape_string($object_type)) // переключающее выражение
				{
				   case 'articles_article': // константное выражение 1
						sys::useLib('main::articles');
						$link = main_articles::getArticleUrl($object_id);
				   break;
				   case 'cinema': // константное выражение 1
						sys::useLib('main::cinemas');
						$link = main_cinemas::getCinemaUrl($object_id);
				   break;

				   case 'serials_article': // константное выражение 1
						sys::useLib('main::serials');
						$link = main_serials::getArticleUrl($object_id);
				   break;
				   case 'contest_article': // константное выражение 1
						sys::useLib('main::contest');
						$link = main_contest::getArticleUrl($object_id);
				   break;
				   case 'film': // константное выражение 1
						sys::useLib('main::films');
						$link = main_films::getFilmUrl($object_id);
				   break;
				   case 'trailer': // константное выражение 1
						sys::useLib('main::films');
						$link = main_films::getFilmTrailerUrl($object_id);
				   break;

				   case 'gossip_article': // константное выражение 1
						sys::useLib('main::gossip');
						$link = main_gossip::getArticleUrl($object_id);
				   break;
				   case 'interview_article': // константное выражение 1
						sys::useLib('main::interview');
						$link = main_interview::getArticleUrl($object_id);
				   break;

				   case 'news_article': // константное выражение 1
						sys::useLib('main::news');
						$link = main_news::getArticleUrl($object_id);
				   break;

				   case 'person': // константное выражение 1
						sys::useLib('main::persons');
						$link = main_persons::getPersonUrl($object_id);
				   break;
				   case 'review': // константное выражение 1
						sys::useLib('main::reviews');
						$link = main_reviews::getReviewUrl($object_id);
				   break;
				   default:
				      $link='';
				}

				if($_db->query("
					INSERT INTO `#__main_discuss_messages`
					(
						`id`,
						`object_id`,
						`object_type`,
						`user_id`,
						`user_name`,
						`date`,
						`ip`,
						`text`
					)
					VALUES
					(
						0,
						'".intval($object_id)."',
						'".mysql_real_escape_string($object_type)."',
						'".intval($_user['id'])."',
						'".mysql_real_escape_string($_POST['name'])."',
						'".gmdate('Y-m-d H:i:s')."',
						'".ip2long($_SERVER['REMOTE_ADDR'])."',
						'".mysql_real_escape_string($_POST['text'])."'
					)
				"))
					plusComment($object_type, $object_id);
//					die('155');
					$response['answer'] = 1;
					echo json_encode($response);
					exit;
				
				
			}
				$response['answer'] = 0;
//				$response['error'] = sys::translate('main::links_forbid');
				$response['error'] = 'Ответ содержит запрещенные слова';
				echo json_encode($response);
				exit;
			
		
	}
	
	function plusComment($object_type, $object_id)
	{
		global $_db;
		$table=getObjectTableName($object_type);


		if($table)
		{
			$_db->query("
				UPDATE `".$table."`
				SET `comments`=`comments`+1
				WHERE `id`=".intval($object_id)."
			");
		}
	}
	
	function getObjectTableName($object_type)
	{
		switch ($object_type)
		{
			case 'film':
				return "#__main_films";
			break;

			case 'person':
				return "#__main_persons";
			break;

			case 'review':
				return "#__main_reviews";
			break;

			case 'cinema':
				return "#__main_cinemas";
			break;

			case 'serials_article':
				return "#__main_serials_articles";
			break;

			case 'news_article':
				return "#__main_news_articles";
			break;


			case 'gossip_article':
				return "#__main_gossip_articles";
			break;

			case 'interview_article':
				return "#__main_interview_articles";
			break;

			case 'articles_article':
				return "#__main_articles_articles";
			break;
			
			case 'contest_article':
				return "#__main_contest_articles";
			break;
		}
	}

?>