<?php


//if(basename($_SERVER['PHP_SELF'])!=='index.php' && basename($_SERVER['PHP_SELF'])!=='auth.php')
//	die($_SERVER['PHP_SELF']);


//Убрать лишние кавычки
//set_magic_quotes_runtime(0);
ini_set('magic_quotes_runtime', 0);
function strips(&$var)
{
	if (is_array($var))
		foreach($var as $key=>$val)
			strips($var[$key]);
	else
		$var = stripslashes($var);
}

if(get_magic_quotes_gpc())
{
	strips($_GET);
	strips($_POST);
	strips($_COOKIE);
	strips($_REQUEST);

	if(isset($_SERVER['PHP_AUTH_USER']))
		strips($_SERVER['PHP_AUTH_USER']);
	if(isset($_SERVER['PHP_AUTH_PW']))
		strips($_SERVER['PHP_AUTH_PW']);
}
//======================================================//


//Почистить REQUEST_URI
$_SERVER['REQUEST_URI']=str_replace('onclick','',$_SERVER['REQUEST_URI']);
$_SERVER['REQUEST_URI']=str_replace('ondblclick','',$_SERVER['REQUEST_URI']);
$_SERVER['REQUEST_URI']=str_replace('onkeydown','',$_SERVER['REQUEST_URI']);
$_SERVER['REQUEST_URI']=str_replace('onkeypress','',$_SERVER['REQUEST_URI']);
$_SERVER['REQUEST_URI']=str_replace('onkeyup','',$_SERVER['REQUEST_URI']);
$_SERVER['REQUEST_URI']=str_replace('onmousedown','',$_SERVER['REQUEST_URI']);
$_SERVER['REQUEST_URI']=str_replace('onmouseout','',$_SERVER['REQUEST_URI']);
$_SERVER['REQUEST_URI']=str_replace('onmousemove','',$_SERVER['REQUEST_URI']);
$_SERVER['REQUEST_URI']=str_replace('onsubmit','',$_SERVER['REQUEST_URI']);
$_SERVER['REQUEST_URI']=str_replace('onreset','',$_SERVER['REQUEST_URI']);
//=============================================================


//Задать формат для ввода даты
function setDateFormat()
{
	global $_cfg;
	$time_format=strftime('%X',mktime(1,2,3,4,5,2006));
	$time_format=str_replace('01','%H',$time_format);
	$time_format=str_replace('02','%M',$time_format);
	$time_format=str_replace('03','%S',$time_format);
	$date_format=strftime('%x',mktime(1,2,3,4,5,2006));

	$date_format=str_replace('04','%m',$date_format);
	$date_format=str_replace('05','%d',$date_format);
	$date_format=str_replace('2006','%Y',$date_format);
	$date_format=str_replace('06','%Y',$date_format);


	if(!$_cfg['sys::date_format'])
		$_cfg['sys::date_format']=$date_format;
	if(!$_cfg['sys::time_format'])
		$_cfg['sys::time_format']=$time_format;

	$_cfg['sys::date_time_format']=$date_format.' '.$time_format;
}
//=========================================================================================//

//Подключить файл конфигурации
if(_MODE=='backend')
	require_once('../cfg.php');
elseif(_MODE=='frontend')
	require_once('cfg.php');
//=====================================//



//Подключить библиоткеку системных функций и класс работы с БД

require_once(_ROOT_DIR.'mods/sys/include/lib.sys.php');
require_once(_ROOT_DIR.'mods/sys/include/cls.db.php');


//=========================================================================//

//			if(sys::isDebugIP()) // можно использовать начиная с этого места
//			{
//				print "<pre>";
//				var_dump($request);
//				var_dump($_SERVER['REQUEST_URI']);
//				print "</pre>";
//				die;
//			}

//Вывести информацию из кэша если включено кэширование

/*
if(_MODE=='frontend' && $_cfg['sys::cache_seconds'] && $_SERVER['REQUEST_METHOD']!='POST')
{
	$cache_path=_SECURE_DIR.'cache/'.md5($_SERVER['REQUEST_URI']);
	if(is_file($cache_path)) //Если кэш-файл существует
	{
		$m_time=time()-filemtime($cache_path);
		if($m_time<$_cfg['sys::cache_seconds']) //Если время кеширования не истекло
		{
			//Отдать кеш
			sys::sendHeaders();
			echo file_get_contents($cache_path);
			ob_end_flush();
			exit();
			//========================//
		}
	}
}
*/
//=====================================================================//



/*
if($_SERVER['REQUEST_METHOD']=='POST' && isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'])
{
	$ref_url=parse_url($_SERVER['HTTP_REFERER']);
	$root_url=parse_url(_ROOT_URL);
	if($ref_url['host']!=$root_url['host'])
	{
		header('Location:'._ROOT_URL);
		exit;
	}
}
*/

//Подключить основные библиотеки
if(_MODE=='backend')
	sys::useLib('sys::gui');
elseif(_MODE=='frontend')
	sys::useLib('main');
//=========================================//

$_db=new cls_sys_db();//создать объект для работы с БД
$_db->query("SET NAMES 'UTF8'"); //задать кодировку данных БД
//$_db->query("SET AUTOCOMMIT=0");

//получить e-mail суперпользователя
$_cfg['sys::root_email']=$_db->getValue('sys_users','email',7);


//Вывести сообщение если сайт закрыт
if(_MODE=='frontend' && $_cfg['sys::closed'])
{
	sys::sendHeaders();
	echo sys::parseModTpl('sys::site_closed','html');
	ob_end_flush();
	exit();
}

//Стартовать сессию
if($_cfg['sys::session_in_db'])
	require_once(_ROOT_DIR.'mods/sys/include/lib.sess.php');
else
	session_save_path(_SESSION_DIR);
session_name($_cfg['sys::project_id'].'_'._MODE);
session_cache_expire($_cfg['sys::session_minutes']);
session_start();
//======================================================================//

//Проверить айпишник пользователя
if(isset($_SESSION['sys::user_ip']) && $_SESSION['sys::user_ip']!=$_SERVER['REMOTE_ADDR'])
	unset($_SESSION);
//====================================//

//Загрузить настройки модулей
$r=$_db->Query("SELECT CONCAT(mods.name, '::', cfg.name) AS `name`,
		cfg.value
		FROM `#__sys_mods_cfg` AS `cfg`
		LEFT JOIN `#__sys_mods` AS `mods`
		ON cfg.mod_id=mods.id");
while($cfg = $_db->fetchArray($r))
{
	$_cfg[$cfg['name']]=$cfg['value'];
}
unset($cfg);
if(_MODE=='backend') //Если админ панель
	$_cfg['sys::hurl']=false;//Отключить ЧПУ
//================================================//

//Загрузить системные языки
$r=$_db->query("SELECT `id`, `code`, `title`, `short_title`
		FROM `#__sys_langs`
		WHERE `on`=1
		ORDER BY `order_number`");
$_langs=array();
while($lang=$_db->fetchAssoc($r))
	$_langs[$lang['id']]=$lang;
//============================================//

//Разобрать REQUEST_URI и определить модуль и экшн
if(_MODE=='frontend' && !isset($_GET['mod']) && $_cfg['sys::hurl'])
{
	$request=explode('/',sys::getRootUri());//Убрать из REQUEST_URI path если он есть
	
	switch($_SERVER['REQUEST_URI'])
	{
		case "/ru/main/popular_wallpapers.phtml": 	
		case "/uk/main/popular_wallpapers.phtml": 	
		
		case "/ru/main/last_wallpapers.phtml": 	
		case "/uk/main/last_wallpapers.phtml": 	
		
		case "/ru/main/popular_photos.phtml": 	
		case "/uk/main/popular_photos.phtml": 	

		case "/ru/main/last_photos.phtml": 	
		case "/uk/main/last_photos.phtml": 	
		
		case "/ru/main/popular_posters.phtml": 	
		case "/uk/main/popular_posters.phtml": 	
		
		case "/ru/main/last_posters.phtml": 	
		case "/uk/main/last_posters.phtml": 	
		
		case "/ru/main/last_sound.phtml": 	
		case "/uk/main/last_sound.phtml": 	
		
		case "/ru/main/last_trailers.phtml": 	
		case "/uk/main/last_trailers.phtml": 	
		
		case "/ru/main/popular_trailers.phtml": 	
		case "/uk/main/popular_trailers.phtml": 	
		
		case "/ru/main/news.phtml": 	
		case "/uk/main/news.phtml": 	
		
		case "/ru/main/serials.phtml": 	
		case "/uk/main/serials.phtml": 	
		
		case "/ru/main/articles.phtml": 	
		case "/uk/main/articles.phtml": 	
		
		case "/ru/main/gossip.phtml": 	
		case "/uk/main/gossip.phtml": 	
		
		case "/ru/main/reviews.phtml": 	
		case "/uk/main/reviews.phtml": 	
		
		case "/ru/main/interview.phtml": 	
		case "/uk/main/interview.phtml": 	
		
		case "/ru/main/spec_themes.phtml": 	
		case "/uk/main/spec_themes.phtml": 	
											$url = $_SERVER['REQUEST_URI'];
											$lng = $request[1];
											$lng = ($lng=="ru")?"\/$lng":"";
											if($lng) $url = preg_replace("/$lng\//si","/",$url) ;
											sys::moved("https://".$_SERVER['SERVER_NAME'].$url, "301 Moved Permanently", "/\/main\//si", "/");
	}
		
	if(count($_langs)>1)
	{
		if(isset($request[1]) && sys::isLang($request[1]))//Язык
			$_GET['lang']=$request[1];

		if(isset($request[2]))//Модуль
			$_GET['mod']=$request[2];

		if(isset($request[3]))//Екшн модуля
			$_GET['act']=$request[3];


		if (substr($request[1],0,8)=='cinemas-')
		{
			$city = str_replace('cinemas-','',$request[1]);
		}

		if (substr($request[2],0,8)=='cinemas-')
		{
			$city = str_replace('cinemas-','',$request[2]);
		}

		if (substr($request[1],0,11)=='kinoafisha-' && substr($request[1],0,17)!='kinoafisha-movie-')
		{
			$citykino = str_replace('kinoafisha-','',$request[1]);
		}

		if (substr($request[2],0,11)=='kinoafisha-' && substr($request[2],0,17)!='kinoafisha-movie-')
		{
			$citykino = str_replace('kinoafisha-','',$request[2]);
		}

		if (substr($request[1],0,12)=='afisha-kino-')
		{
			$cityfilmkino = str_replace('afisha-kino-','',$request[1]);
		}

		if (substr($request[2],0,12)=='afisha-kino-')
		{
			$cityfilmkino = str_replace('afisha-kino-','',$request[2]);
		}

// Перекодирование старых прямых адресов вида //kino-teatr.ua/ru/main/gossip_article/article_id/25.phtml
// в вид //kino-teatr.ua/[uk/]gossip/naimenovanie-25.phtml	
// и редирект с кодом 301

			$redirect_url = "";
			if($request[1] == "rus") $request[1] = "ru"; // ???
			
			switch($request[3]) 
			{
				case "person-photos":   // //kino-teatr.ua/ru/main/person-phots/person_id/2456.phtml
				case "person_photos":
				case "person_phots":
											sys::useLib('main::persons');
											$redirect_url = main_persons::getPersonPhotosUrl($request[5],$request[1]);
											break;
				case "person-trailers":	// //kino-teatr.ua/ru/main/person-trailers/person_id/2456.phtml
				case "person_trailers":
											sys::useLib('main::persons');
											$redirect_url = main_persons::getPersonTrailersUrl($request[5],$request[1]);
											break;
				case "person-wallpapers":// //kino-teatr.ua/ru/main/person-wallpapers/person_id/2456.phtml
				case "person_wallpapers":
				case "person_walls":
											sys::useLib('main::persons');
											$redirect_url = main_persons::getPersonWallpapersUrl($request[5],$request[1]);
											break;
				case "person-frames":	// //kino-teatr.ua/ru/main/person-frames/person_id/2456.phtml
				case "person_frames":
				case "person-frams":
				case "person_frams":
											sys::useLib('main::persons');
											$redirect_url = main_persons::getPersonFramesUrl($request[5],$request[1]);
											break;
//				case "person_news":		// //kino-teatr.ua/ru/main/person_news/person_id/2456.phtml
//				case "person-news":
//											sys::useLib('main::persons');
//											$redirect_url = main_persons::getPersonNewsUrl($request[5],$request[1]);
//											break;
				case "person_films":	// //kino-teatr.ua/ru/main/person_films/person_id/1662.phtml
				case "person-films":
											sys::useLib('main::persons');
											$redirect_url = main_persons::getPersonFilmsUrl($request[5],$request[1]);
											break;
				case "person": 			// //kino-teatr.ua/ru/main/person/person_id/1662.phtml
											sys::useLib('main::persons');
											$redirect_url = main_persons::getPersonUrl($request[5],$request[1]);
											break;
				case "film_soundtracks": // //kino-teatr.ua/ru/main/film_soundtracks/film_id/4586.phtml
											sys::useLib('main::films');
											$redirect_url = main_films::getFilmSoundtracksUrl($request[5],$request[1]);
											break;
				case "film_poster":		// //kino-teatr.ua/ru/main/film_poster/poster_id/39314.phtml
											sys::useLib('main::films');
											$redirect_url = main_films::getFilmPosterUrlForRedirect($request[5],$request[1]);
											break;
				case "film_photo":		// //kino-teatr.ua/ru/main/film_photo/photo_id/41185.phtml
											sys::useLib('main::films');
											$redirect_url = main_films::getFilmPhotoUrlForRedirect($request[5],$request[1]);
											break;
				case "film_wallpapers":	// //kino-teatr.ua/ru/main/film_wallpapers/wallpaper_id/1114.phtml
											sys::useLib('main::films');
											$redirect_url = main_films::getFilmWallpapersUrlForRedirect($request[5],$request[1]);
											break;
				case "film_walls":		// //kino-teatr.ua/ru/main/film_walls/film_id/6766.phtml
											sys::useLib('main::films');
											$redirect_url = main_films::getFilmWallpapersUrl($request[5],$request[1]);
											break;

				case "cinema_shows":		sys::useLib('main::cinemas');
											$redirect_url = main_cinemas::getCinemaShowsUrl($request[5],$request[1]);
											break;
				case "cinema":				sys::useLib('main::cinemas');
											$redirect_url = main_cinemas::getCinemaUrl($request[5],$request[1]);
											break;
				case "gossip_article":		sys::useLib('main::gossip');
											$redirect_url = main_gossip::getArticleUrl($request[5],$request[1]);
											break;
				case "articles_article":	sys::useLib('main::articles');
											$redirect_url = main_articles::getArticleUrl($request[5],$request[1]);
											break;
				case "interview_article":	sys::useLib('main::interview');
											$redirect_url = main_interview::getArticleUrl($request[5],$request[1]);
											break;
				case "film_trailers":		
											sys::useLib('main::films');
											$redirect_url = main_films::getTrailerUrl($request[5],$request[1],"film-trailers"); 
											break;
				case "last_trailers":		
											if(isset($request[4])&&($request[4]=="page")&&isset($request[5])) break; // page=xxx
											if(isset($request[6])&&($request[6]=="page")&&isset($request[7])) break; // page=xxx
											sys::useLib('main::films');
											$redirect_url = main_films::getTrailersUrl($request[5],$request[1],$request[3]);
											break;
				case "popular_trailers":		
											if(isset($request[4])&&($request[4]=="page")&&isset($request[5])) break; // page=xxx
											if(isset($request[6])&&($request[6]=="page")&&isset($request[7])) break; // page=xxx
											sys::useLib('main::films');
											$redirect_url = main_films::getTrailersUrl($request[5],$request[1],$request[3]);
											break;
			}
			if($redirect_url) 
			{
				sys::moved($redirect_url, "301 Moved Permanently");
			}

		$none = 0;
		if ($request[1]!='uk')
		{
				$_GET['lang'] = 'ru';
				$_GET['mod'] = 'main';
				$none = 1;
			if ($request[1]=='film')
			{
				$_GET['act'] = 'film';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film-shows')
			{
				$_GET['act'] = 'film_shows';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film-discuss')
			{
				$_GET['act'] = 'film_discuss';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film-reviews')
			{
				$_GET['act'] = 'film_reviews';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film-news')
			{
				$_GET['act'] = 'film_news';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film-anothers')
			{
				$_GET['act'] = 'film_anothers';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film-persons')
			{
				$_GET['act'] = 'film_persons';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film-series')
			{
				$_GET['act'] = 'film_series';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film_photo')
			{
				$_GET['act'] = 'film_photo';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film_photos')
			{
				$_GET['act'] = 'film_photos';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film-photo')
			{
				$_GET['act'] = 'film_phots';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film_posters')
			{
				$_GET['act'] = 'film_posters';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film_poster')
			{
				$_GET['act'] = 'film_poster';
				$arr = explode('-', $request[2]);
				$_GET['poster_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film-poster')
			{
				$_GET['act'] = 'film_posts';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film_walls')
			{
				$_GET['act'] = 'film_walls';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='popular_wallpapers')
			{
				$_GET['act'] = 'popular_wallpapers';
				$arr = explode('-', $request[2]);
				$_GET['wallpaper_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='last_wallpapers')
			{
				$_GET['act'] = 'last_wallpapers';
				$arr = explode('-', $request[2]);
				$_GET['wallpaper_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='popular_photos')
			{
				$_GET['act'] = 'popular_photos';
				$arr = explode('-', $request[2]);
				$_GET['photo_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='last_photos')
			{
				$_GET['act'] = 'last_photos';
				$arr = explode('-', $request[2]);
				$_GET['photo_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film_wallpapers')
			{
				$_GET['act'] = 'film_wallpapers';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];				
			} else if ($request[1]=='popular_posters')
			{
				$_GET['act'] = 'popular_posters';
				$arr = explode('-', $request[2]);
				$_GET['poster_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='last_posters')
			{
				$_GET['act'] = 'last_posters';
				$arr = explode('-', $request[2]);
				$_GET['poster_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film_soundtracks')
			{
				$_GET['act'] = 'film_soundtracks';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='last_sound')
			{
				$_GET['act'] = 'last_sound';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='last_trailers')
			{
				$_GET['act'] = 'last_trailers';
				$arr = explode('-', $request[2]);
				$_GET['trailer_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='popular_trailers')
			{
				$_GET['act'] = 'popular_trailers';
				$arr = explode('-', $request[2]);
				$_GET['trailer_id'] = $arr[count($arr)-1];
			} else if ( ($request[1]=='film-trailers') || ($request[1]=='film-trailer') || ($request[1]=='film_trailers') || ($request[1]=='film_trailer') )
			{
				$_GET['act'] = 'film_trailers';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='film-rating')
			{
				$_GET['act'] = 'film_rating';
				$arr = explode('-', $request[2]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='cinema')
			{
				$_GET['act'] = 'cinema';
				$arr = explode('-', $request[2]);
				$_GET['cinema_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='watch_film')
			{
				$_GET['act'] = 'watch_film';
			} else if ($request[1]=='film_watch')
			{
				$_GET['act'] = 'film_watch';
			} else if ($request[1]=='cinema-news')
			{
				$_GET['act'] = 'cinema_news';
				$arr = explode('-', $request[2]);
				$_GET['cinema_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='cinema-shows')
			{
				$_GET['act'] = 'cinema_shows';
				$arr = explode('-', $request[2]);
				$_GET['cinema_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='cinema-photos')
			{
				$_GET['act'] = 'cinema_photos';
				$arr = explode('-', $request[2]);
				$_GET['cinema_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='cinema-discuss')
			{
				$_GET['act'] = 'cinema_discuss';
				$arr = explode('-', $request[2]);
				$_GET['cinema_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='films')
			{
				$_GET['act'] = 'films';
				$_GET['letter'] = $request[3];
			} else if ($request[1]=='cinemas')
			{
				$_GET['act'] = 'cinemas';
			} else if ($request[1]=='game')
			{
				$_GET['act'] = 'game';
			} else if ($request[1]=='games')
			{
				$_GET['act'] = 'games';
			} else if ($request[1]=='films-near')
			{
				$_GET['act'] = 'films_near';
			} else if ($request[1]=='movie')
			{
				$_GET['act'] = 'movie';
			} else if ($request[1]=='persons')
			{
				$_GET['act'] = 'persons';
				$_GET['letter'] = $request[3];
			} else if ($request[1]=='person')
			{
				$_GET['act'] = 'person';
				$arr = explode('-', $request[2]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if (($request[1]=='person-films') || ($request[1]=='person_films'))
			{
				$_GET['act'] = 'person_films';
				$arr = explode('-', $request[2]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if (($request[1]=='person-news') || ($request[1]=='person_news'))
			{
				$_GET['act'] = 'person_news';
				$arr = explode('-', $request[2]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if (($request[1]=='person-photo') || ($request[1]=='person_phots') || ($request[1]=='person_photos') || ($request[1]=='person_photo'))
			{
				$_GET['act'] = 'person_photos';
				$arr = explode('-', $request[2]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='person_posters')
			{
				$_GET['act'] = 'person_posters';
				$arr = explode('-', $request[2]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if (($request[1]=='person-frames') || ($request[1]=='person_frams') || ($request[1]=='person_fram'))
			{
				$_GET['act'] = 'person_frams';
				$arr = explode('-', $request[2]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if (($request[1]=='person-wallpapers') || ($request[1]=='person_walls') || ($request[1]=='person_wallpapers') || ($request[1]=='person_wallpaper'))
			{
				$_GET['act'] = 'person_wallpapers';
				$arr = explode('-', $request[2]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if (($request[1]=='person-trailers') || ($request[1]=='person_trailers') || ($request[1]=='person_trailer'))
			{
				$_GET['act'] = 'person_trailers';
				$arr = explode('-', $request[2]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if ( ($request[1]=='person-awards') || ($request[1]=='person_awards') )
			{
				$_GET['act'] = 'person_awards';
				$arr = explode('-', $request[2]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='person-discuss')
			{
				$_GET['act'] = 'person_discuss';
				$arr = explode('-', $request[2]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='person-rating')
			{
				$_GET['act'] = 'person_rating';
				$arr = explode('-', $request[2]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if ($request[1]=='reviews')
			{
				$_GET['act'] = 'reviews';
			} else if ($request[1]=='review')							// by Mike
			{
				$_GET['act'] = $request[1];
				if (count($request[2])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/review/films/Transcendence-2643.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/review/Transcendence-2643.phtml
					if(($request[2]=="films") && (count($request[3])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/films\//si", "/");
					$arr = explode('-', $request[2]);
					$_GET['review_id'] = $arr[count($arr)-1];
				}
			} else if ($request[1]=='news')								// by Mike
			{
				$_GET['act'] = $request[1];
				if (count($request[2])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/news/article/novyie-kadryi-prikvela-vo-vse-tyajkie-6366.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/news/novyie-kadryi-prikvela-vo-vse-tyajkie-6366.phtml
					if(($request[2]=="article") && (count($request[3])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/article\//si", "/");
					$_GET['act'] = $request[1].'_article';
					$arr = explode('-', $request[2]);
					$_GET['article_id'] = $arr[count($arr)-1];
				}		
// Старый вариант				
//					$_GET['act'] = 'news';
//					if (count($request[2])>0)
//					{
//						$_GET['act'] = 'news_article';
//						$arr = explode('-', $request[3]);
//						$_GET['article_id'] = $arr[count($arr)-1];
//					}
			} else if ($request[1]=='serials')							// by Mike
			{
				$_GET['act'] = $request[1];
				if (count($request[2])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/serials/article/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/serials/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					if(($request[2]=="article") && (count($request[3])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/article\//si", "/");
					$_GET['act'] = $request[1].'_article';
					$arr = explode('-', $request[2]);
					$_GET['article_id'] = $arr[count($arr)-1];
				}
			} else if ($request[1]=='gossip')							// by Mike
			{
//				if($_SERVER['REMOTE_ADDR'] == "213.87.139.7"){
				$_GET['act'] = $request[1];
				if (count($request[2])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/gossip/article/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/gossip/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					if(($request[2]=="article") && (count($request[3])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/article\//si", "/");
					$_GET['act'] = $request[1].'_article';
					$arr = explode('-', $request[2]);
					$_GET['article_id'] = $arr[count($arr)-1];
				}
//				}// OF if($_SERVER['REMOTE_ADDR']
			} else if ($request[1]=='articles')							// by Mike
			{
//				if($_SERVER['REMOTE_ADDR'] == "213.87.139.7"){	
				$_GET['act'] = $request[1];
				if (count($request[2])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/articles/article/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/articles/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					if(($request[2]=="article") && (count($request[3])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/article\//si", "/");
					$_GET['act'] = $request[1].'_article';
					$arr = explode('-', $request[2]);
					$_GET['article_id'] = $arr[count($arr)-1];
				}				
//				}// OF if($_SERVER['REMOTE_ADDR']
			} else if ($request[1]=='interview')							// by Mike
			{
//				if($_SERVER['REMOTE_ADDR'] == "213.87.139.7"){				
				$_GET['act'] = $request[1];
				if (count($request[2])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/articles/article/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/articles/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					if(($request[2]=="article") && (count($request[3])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/article\//si", "/");
					$_GET['act'] = $request[1].'_article';
					$arr = explode('-', $request[2]);
					$_GET['article_id'] = $arr[count($arr)-1];
				}
//				}// OF if($_SERVER['REMOTE_ADDR']
				
			} else if ($request[1]=='spec_themes')						// by Mike
			{
//				if($_SERVER['REMOTE_ADDR'] == "213.87.139.7"){				
				$_GET['act'] = $request[1];
				if (count($request[2])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/spec_themes/article/omkf-2014-86.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/spec_themes/omkf-2014-86.phtml
					if(($request[2]=="article") && (count($request[3])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/article\//si", "/");
					$_GET['act'] = $request[1].'_article';
					$arr = explode('-', $request[2]);
					$_GET['spec_themes_id'] = $arr[count($arr)-1];
				}
//				}// OF if($_SERVER['REMOTE_ADDR']
			} else if ($request[1]=='bill')
			{
				$_GET['act'] = 'bill';
			} else if ($request[1]=='box')
			{
				$_GET['act'] = 'box';
				if (count($request[2])>0)
				{
					$_GET['act'] = 'box_article';
					$arr = explode('-', $request[2]);
					$_GET['article_id'] = $arr[count($arr)-1];
				}
			} else if ($city)
			{
				$_GET['act'] = 'cinemas';
				$_GET['city'] = $city;
			} else if ($citykino)
			{
				$_GET['act'] = 'bill';
				$_GET['city'] = $city;
			}
			else if ($cityfilmkino)
			{
				$_GET['act'] = 'bill';
				$_GET['city'] = $cityfilmkino;
				$_GET['order'] = 'films';
			}

		} else {
				$_GET['lang'] = 'uk';
				$_GET['mod'] = 'main';
				$none = 1;
			if ($request[2]=='film')
			{
				$_GET['act'] = 'film';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film-shows')
			{
				$_GET['act'] = 'film_shows';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film-discuss')
			{
				$_GET['act'] = 'film_discuss';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film-reviews')
			{
				$_GET['act'] = 'film_reviews';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film-news')
			{
				$_GET['act'] = 'film_news';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film-anothers')
			{
				$_GET['act'] = 'film_anothers';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film-persons')
			{
				$_GET['act'] = 'film_persons';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film-series')
			{
				$_GET['act'] = 'film_series';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film-news')
			{
				$_GET['act'] = 'film_persons';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film_photo')
			{
				$_GET['act'] = 'film_photo';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film_photos')
			{
				$_GET['act'] = 'film_photos';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film-photo')
			{
				$_GET['act'] = 'film_phots';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film_posters')
			{
				$_GET['act'] = 'film_posters';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film_poster')
			{
				$_GET['act'] = 'film_poster';
				$arr = explode('-', $request[3]);
				$_GET['poster_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film-poster')
			{
				$_GET['act'] = 'film_posts';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film_walls')
			{
				$_GET['act'] = 'film_walls';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='popular_wallpapers')
			{
				$_GET['act'] = 'popular_wallpapers';
				$arr = explode('-', $request[3]);
				$_GET['wallpaper_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='last_wallpapers')
			{
				$_GET['act'] = 'last_wallpapers';
				$arr = explode('-', $request[3]);
				$_GET['wallpaper_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film_wallpapers')
			{
				$_GET['act'] = 'film_wallpapers';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='popular_photos')
			{
				$_GET['act'] = 'popular_photos';
				$arr = explode('-', $request[3]);
				$_GET['photo_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='last_photos')
			{
				$_GET['act'] = 'last_photos';
				$arr = explode('-', $request[3]);
				$_GET['photo_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='popular_posters')
			{
				$_GET['act'] = 'popular_posters';
				$arr = explode('-', $request[3]);
				$_GET['poster_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='last_posters')
			{
				$_GET['act'] = 'last_posters';
				$arr = explode('-', $request[3]);
				$_GET['poster_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film_soundtracks')
			{
				$_GET['act'] = 'film_soundtracks';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='last_sound')
			{
				$_GET['act'] = 'last_sound';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='last_trailers')
			{
				$_GET['act'] = 'last_trailers';
				$arr = explode('-', $request[3]);
				$_GET['trailer_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='popular_trailers')
			{
				$_GET['act'] = 'popular_trailers';
				$arr = explode('-', $request[3]);
				$_GET['trailer_id'] = $arr[count($arr)-1];
			} else if ( ($request[2]=='film-trailers') || ($request[2]=='film-trailer') || ($request[2]=='film_trailers') || ($request[2]=='film_trailer') )
			{
				$_GET['act'] = 'film_trailers';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='film-rating')
			{
				$_GET['act'] = 'film_rating';
				$arr = explode('-', $request[3]);
				$_GET['film_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='cinema')
			{
				$_GET['act'] = 'cinema';
				$arr = explode('-', $request[3]);
				$_GET['cinema_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='cinema-news')
			{
				$_GET['act'] = 'cinema_news';
				$arr = explode('-', $request[3]);
				$_GET['cinema_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='cinema-shows')
			{
				$_GET['act'] = 'cinema_shows';
				$arr = explode('-', $request[3]);
				$_GET['cinema_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='cinema-photos')
			{
				$_GET['act'] = 'cinema_photos';
				$arr = explode('-', $request[3]);
				$_GET['cinema_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='cinema-discuss')
			{
				$_GET['act'] = 'cinema_discuss';
				$arr = explode('-', $request[3]);
				$_GET['cinema_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='films')
			{
				$_GET['act'] = 'films';
				$_GET['letter'] = $request[4];
			} else if ($request[2]=='cinemas')
			{
				$_GET['act'] = 'cinemas';
			} else if ($request[2]=='game')
			{
				$_GET['act'] = 'game';
			} else if ($request[2]=='games')
			{
				$_GET['act'] = 'games';
			} else if ($request[2]=='films-near')
			{
				$_GET['act'] = 'films_near';
			} else if ($request[2]=='movie')
			{
				$_GET['act'] = 'movie';

			} else if ($request[2]=='persons')
			{
				$_GET['act'] = 'persons';
				$_GET['letter'] = $request[4];
			} else if ($request[2]=='person')
			{
				$_GET['act'] = 'person';
				$arr = explode('-', $request[3]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if (($request[2]=='person-films') || ($request[2]=='person_films'))
			{
				$_GET['act'] = 'person_films';
				$arr = explode('-', $request[3]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if (($request[2]=='person-news') || ($request[2]=='person_news'))
			{
				$_GET['act'] = 'person_news';
				$arr = explode('-', $request[3]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if (($request[2]=='person-photo') || ($request[2]=='person_phots') || ($request[2]=='person_photos') || ($request[2]=='person_photo'))
			{
				$_GET['act'] = 'person_photos';
				$arr = explode('-', $request[3]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='person_posters')
			{
				$_GET['act'] = 'person_posters';
				$arr = explode('-', $request[3]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if (($request[2]=='person-frames') || ($request[2]=='person_frams') || ($request[2]=='person_fram'))
			{
				$_GET['act'] = 'person_frams';
				$arr = explode('-', $request[3]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if (($request[2]=='person-wallpapers') || ($request[2]=='person_walls') || ($request[2]=='person_wallpapers') || ($request[2]=='person_wallpaper'))
			{
				$_GET['act'] = 'person_wallpapers';
				$arr = explode('-', $request[3]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if (($request[2]=='person-trailers') || ($request[2]=='person_trailers') || ($request[2]=='person_trailer'))
			{
				$_GET['act'] = 'person_trailers';
				$arr = explode('-', $request[3]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if ( ($request[2]=='person-awards') || ($request[2]=='person_awards') )
			{
				$_GET['act'] = 'person_awards';
				$arr = explode('-', $request[3]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='person-discuss')
			{
				$_GET['act'] = 'person_discuss';
				$arr = explode('-', $request[3]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='person-rating')
			{
				$_GET['act'] = 'person_rating';
				$arr = explode('-', $request[3]);
				$_GET['person_id'] = $arr[count($arr)-1];
			} else if ($request[2]=='reviews')
			{
				$_GET['act'] = 'reviews';
			} else if ($request[2]=='review')
			{
				$_GET['act'] = $request[2];
				if (count($request[3])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/uk/review/films/Transcendence-2643.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/uk/review/Transcendence-2643.phtml
					if(($request[3]=="films") && (count($request[4])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/films\//si", "/");
					$arr = explode('-', $request[3]);
					$_GET['review_id'] = $arr[count($arr)-1];
				}
			} else if ($request[2]=='news')									// by Mike
			{
				$_GET['act'] = $request[2];
				if (count($request[3])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/uk/news/article/novyie-kadryi-prikvela-vo-vse-tyajkie-6366.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/uk/news/novyie-kadryi-prikvela-vo-vse-tyajkie-6366.phtml
					if(($request[3]=="article") && (count($request[4])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/article\//si", "/");
					$_GET['act'] = $request[2].'_article';
					$arr = explode('-', $request[3]);
					$_GET['article_id'] = $arr[count($arr)-1];
				}
			} else if ($request[2]=='serials')								// by Mike
			{
				$_GET['act'] = $request[2];
				if (count($request[3])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/uk/serials/article/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/uk/serials/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					if(($request[3]=="article") && (count($request[4])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/article\//si", "/");
					$_GET['act'] = $request[2].'_article';
					$arr = explode('-', $request[3]);
					$_GET['article_id'] = $arr[count($arr)-1];
				}
			} else if ($request[2]=='gossip')							// by Mike
			{
//				if($_SERVER['REMOTE_ADDR'] == "213.87.139.7"){
				$_GET['act'] = $request[2];
				if (count($request[3])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/uk/gossip/article/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/uk/gossip/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					if(($request[3]=="article") && (count($request[4])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/article\//si", "/");
					$_GET['act'] = $request[2].'_article';
					$arr = explode('-', $request[3]);
					$_GET['article_id'] = $arr[count($arr)-1];
				}
//				}// OF if($_SERVER['REMOTE_ADDR']
			} else if ($request[2]=='articles')							// by Mike
			{
//				if($_SERVER['REMOTE_ADDR'] == "213.87.139.7"){	
				$_GET['act'] = $request[2];
				if (count($request[3])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/uk/articles/article/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/uk/articles/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					if(($request[3]=="article") && (count($request[4])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/article\//si", "/");
					$_GET['act'] = $request[2].'_article';
					$arr = explode('-', $request[3]);
					$_GET['article_id'] = $arr[count($arr)-1];
				}				
//				}// OF if($_SERVER['REMOTE_ADDR']
			} else if ($request[2]=='interview')							// by Mike
			{
//				if($_SERVER['REMOTE_ADDR'] == "213.87.139.7"){				
				$_GET['act'] = $request[2];
				if (count($request[3])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/uk/articles/article/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/uk/articles/amerikanskie-serialyi-urojaynaya-osen-7.phtml
					if(($request[3]=="article") && (count($request[4])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/article\//si", "/");
					$_GET['act'] = $request[2].'_article';
					$arr = explode('-', $request[3]);
					$_GET['article_id'] = $arr[count($arr)-1];
				}
//				}// OF if($_SERVER['REMOTE_ADDR']
			} else if ($request[2]=='spec_themes')						// by Mike
			{
//				if($_SERVER['REMOTE_ADDR'] == "213.87.139.7"){				
				$_GET['act'] = $request[2];
				if (count($request[3])>0)
				{
					// если пришли по старой ссылке 			 //kino-teatr.ua/uk/spec_themes/article/omkf-2014-86.phtml
					// то перенаправить по 301 на новую короткую //kino-teatr.ua/uk/spec_themes/omkf-2014-86.phtml
					if(($request[3]=="article") && (count($request[4])>0))	
						sys::moved("https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], "301 Moved Permanently", "/\/article\//si", "/");
					$_GET['act'] = $request[2].'_article';
					$arr = explode('-', $request[3]);
					$_GET['spec_themes_id'] = $arr[count($arr)-1];
				}
//				}// OF if($_SERVER['REMOTE_ADDR']
			} else if ($request[2]=='bill')
			{
				$_GET['act'] = 'bill';
			} else if ($request[2]=='box')
			{
				$_GET['act'] = 'box';
				$_GET['act'] = 'box';
				if (count($request[3])>0)
				{
					$_GET['act'] = 'box_article';
					$arr = explode('-', $request[3]);
					$_GET['article_id'] = $arr[count($arr)-1];
				}
			}  else if ($city)
			{
				$_GET['act'] = 'cinemas';
				$_GET['city'] = $city;
			}
			else if ($citykino)
			{
				$_GET['act'] = 'bill';
				$_GET['city'] = $city;
			} else if ($cityfilmkino)
			{
				$_GET['act'] = 'bill';
				$_GET['city'] = $cityfilmkino;
				$_GET['order'] = 'films';
			}


		}

        if (!$_GET['act'] && $_SERVER['REQUEST_URI']!='/uk/' && $_SERVER['REQUEST_URI']!='/')
		{			
			header("HTTP/1.0 404 Not Found");
		}

	}
	else
	{
		if(isset($request[1]))//Модуль
			$_GET['mod']=$request[1];

		if(isset($request[2]))//Екшн модуля
			$_GET['act']=$request[2];
	}
	unset($arr_url, $request);
}
//=================================================//
if (!$_GET['lang'])
{
	$request=explode('/',sys::getRootUri());//Убрать из REQUEST_URI path если он есть
	if(isset($request[1]) && sys::isLang($request[1]))	$_GET['lang']=$request[1];		
	 else $_GET['lang'] = 'ru';
}

//Задать модуль и экшн по умолчанию
if(!isset($_GET['mod']) || !$_GET['mod'])
{
	if(_MODE=='backend')
		$_GET['mod']='sys';
	elseif(_MODE=='frontend')
		$_GET['mod']='main';
}
if(!isset($_GET['act']) || !$_GET['act'])
	$_GET['act']='default';
//=============================================//

//Подключение разборщика модуля
if(_MODE=='frontend')
{
	if(is_file(_MODS_DIR.$_GET['mod'].'/_parser.php'))
	{
		require_once(_MODS_DIR.$_GET['mod'].'/include/uri_parser.php');
	}
	else
	{
		sys::parseRequestUri();
	}
}
//======================================================//

//Проверка флага первого визита
if(!isset($_SESSION['sys::user_id']))
	$_first_visit=true;
//===================================================//

//Определить включены ли кукисы
if(isset($_COOKIE['sys::test']))
	$_cookie=true;
sys::setCookie('sys::test',1);
//==============================================================================//

//Авторизация по кукисам
if(_MODE=='frontend' && (!isset($_SESSION['sys::user_id']) || $_SESSION['sys::user_id']==2))
{
	if(isset($_COOKIE['sys::login']) && isset($_COOKIE['sys::password']))
	{
		sys::authorizeUser($_COOKIE['sys::login'],$_COOKIE['sys::password'],true);
	}
}
//====================================================//

//Проверка пользователя
if(!sys::checkSessionUser())	//Если пользователь не прошел проверку
	sys::registerUser(2);		//Загегистрировать в системе гостя
//=============================================================================//
//Если не админ заходит в админ панель, перенаправить его на форму входа
if(_MODE=='backend' && (!$_user['backend']))
{
	$_GET['mod']='sys';
	$_GET['act']='login';
}
//===================================================================//

//Определить язык системы
$lang=reset($_langs); //По умолчанию первый язык из таблицы языков

$_cfg['sys::lang']=$lang['code'];
unset($lang);

if(isset($_COOKIE['sys::lang']))
	$_cfg['sys::lang']=$_COOKIE['sys::lang'];

if(isset($_GET['lang']))
	$_cfg['sys::lang']=$_GET['lang'];

//if($_GET['act'] != "ajx_search") 
 sys::setCookie('sys::lang',$_cfg['sys::lang']);

if ($_cfg['sys::lang']=='uk')
{
	$_cfg['sys::rot_url'] = _ROOT_URL.'uk/';
} else {
	$_cfg['sys::rot_url'] = _ROOT_URL;
}
//==============================================================================//

//Загрузить переменные языка
$r=$_db->query("SELECT * FROM `#__sys_langs`
					WHERE `code`='".$_cfg['sys::lang']."'");
$lang=$_db->fetchAssoc($r);
$_cfg['sys::lang_id']=$lang['id'];
$_cfg['sys::from']=$lang['robot_title'];
$_cfg['sys::mail_charset']=$lang['mail_charset'];

eval("setlocale (LC_TIME, ".$lang['locale'].");");
eval("setlocale (LC_CTYPE, ".$lang['locale'].");");
eval("setlocale (LC_MONETARY, ".$lang['locale'].");");
eval("setlocale (LC_TIME, ".$lang['locale'].");");

setDateFormat();

$_meta_title=$lang['meta_title'];
$_title=$_root_title=$lang['project_title'];
$_meta_description=$lang['meta_description'];
$_meta_keywords=$lang['meta_keywords'];
$_meta_other=$lang['meta_other'];
unset($lang);
//============================================================================//

//Определить скин и пути к ресурсам скина-------------------------------------
if(_MODE=='backend')
{
	define('_SKIN',$_cfg['sys::backend_skin']);
	define('_SKIN_URL',_MODS_URL.'sys/backend/skins/'._SKIN.'/');
	define('_SKIN_DIR',_MODS_DIR.'sys/backend/skins/'._SKIN.'/');
}
else
{
	define('_SKIN',$_cfg['sys::frontend_skin']);
	define('_SKIN_URL',_MODS_URL.'main/skins/'._SKIN.'/');
	define('_SKIN_DIR',_MODS_DIR.'main/skins/'._SKIN.'/');
}
define('_IMG_URL',_SKIN_URL.'img/');
define('_IMG_DIR',_SKIN_DIR.'img/');
define('_PIXEL',_SKIN_URL.'img/pixel.gif');
//===========================================================================//

sys::loadUserCfg($_user['id']); //Загрузить настройки пользователя
date_default_timezone_set($_cfg['sys::timezone']);//Задать часовой пояс

//Загрузить список включенных модулей системы
$r=$_db->query("SELECT `id`, `name`
               FROM `#__sys_mods`
               WHERE `on`=1");
while($module=$_db->fetchAssoc($r))
{
	$_mods[$module['id']]=$module['name'];
    sys::useLang($module['name']); //Подключить языковой файл модуля

    //Подключить конфиги модуля
    if(is_file(_MODS_DIR.$module['name'].'/include/_cfg.php'))
   		require_once(_MODS_DIR.$module['name'].'/include/_cfg.php');//Подключить конфиг модуля

   	 //Подключить конфиги скина модуля
    if(is_file(_SKIN_DIR.'cfg/'.$module['name'].'.cfg.php'))
   		require_once(_SKIN_DIR.'cfg/'.$module['name'].'.cfg.php');
   	elseif(is_file(_MODS_DIR.'main/skins/default/cfg/'.$module['name'].'.cfg.php'))
   		require_once(_MODS_DIR.'main/skins/default/cfg/'.$module['name'].'.cfg.php');//Подключить скина модуля

    if(_MODE=='frontend' && is_file(_MODS_DIR.$module['name'].'/include/_init.php'))
		require_once(_MODS_DIR.$module['name'].'/include/_init.php');//Подключить загрузчик модуля

    if(_MODE=='frontend' && $_GET['mod']==$module['name'] && is_file(_MODS_DIR.$module['name'].'/_parser.php'))
        require_once(_MODS_DIR.$module['name'].'/include/_parser.php');//Подключить uri-парсер модуля
}
//==============================================================================//


//Подключить экшн модуля
if($_GET['mod']=='main' && $_GET['act']=='default')
	$_cfg['sys::main_page']=true;
	
//if(sys::isDebugIP()) 
//{
//	sys::printR($_GET['mod'].'::'.$_GET['act']);	 
//	die;
//}



$result=sys::useAct($_GET['mod'].'::'.$_GET['act']);

//if(sys::isDebugIP()) 
//{
//	sys::printR($result);	 
//	die;
//}

if((isset($_GET['DEBUG']) && $_GET['DEBUG'] == '8Z)Vh(m2Kb')){
	sys::printR($result);	 
	die;
}
//============================================
//Обработать результат
switch($result)
{
	case 401;//пользователь неавторизован
		$_var['main']=sys::parseTpl('sys::_err_401',sys::useAct('sys::err_401'));
	break;

	case 403;//доступ закрыт
		$_var['main']=sys::parseTpl('sys::_err_403',sys::useAct('sys::err_403'));
	break;

	case 404;//нет такой страницы
		header("HTTP/1.0 404 Not Found");  // replaced by Mike
		$_var['main']=sys::parseTpl('sys::_err_404',sys::useAct('sys::err_404'));
		$er404 = 1;
	break;

	case false;
	break;

	default:		
		$_var['main']=sys::parseTpl($_GET['mod'].'::_'.$_GET['act'],$result); //Обработать данные в шаблоне и передать в основной шаблон		
//if(sys::isDebugIP()) 
//{
//	sys::printR($result);	 
//	die;
//}
}


//==============================================================================//
//Подключить главный шаблон
if($_tpl)
{
	sys::sendHeaders();
	if (	
		substr_count($_GET['act'],"cinema")>0 
		&& $_GET['act']!='cinemas_table' 
		&& $_GET['act']!='cinema_photo_add' 
		&& $_GET['act']!='cinema_edit' 
		&& $_GET['act']!='cinema_photos_table' 
		&& $_GET['act']!='cinema_halls_table' 
		&& $_GET['act']!='cinema_hall_edit' 
		&& $_GET['act']!='cinema_hall_shows_table' 
		&& $_GET['act']!='cinema_hall_scheme' 
		&& $_GET['act']!='cinema_photo_edit'
	  )
	{
		$_tpl = 'main::__cinema';
	}


	if (substr_count($_GET['act'],"imax")>0)
	{
		$_tpl = 'main::__imax';
	}

	if ($er404)
	{
		$_tpl = 'main::__404';
	}

	echo sys::parseTpl($_tpl,$_var);//Передать данные в главный шаблон
}


//==============================================================================//
//Записать кэш всей страницы
if(_MODE=='frontend' && $_cfg['sys::cache_seconds'] && !count($_POST) && !empty($cache_path))
	sys::writeToFile($cache_path,ob_get_contents(),'w');
//==========================================================================//

//Сделать запсь в журнале сессий
if($_cfg['sys::log_session'])
	sys::writeToFile(_LOGS_DIR.'session.log',gmdate('Y-m-d H:i:s')."\t".$_SERVER['REMOTE_ADDR']."\t".$_SERVER['HTTP_USER_AGENT']."\t".$_SERVER['REQUEST_URI']."\n");
//=====================================================================//

//вывести дебаг информацию
if($_cfg['sys::debug_mode'])
	echo sys::parseTpl('sys::debug',$_debug);
//==================================================//

//Вывести информацию об ошибках
if($_errors)
	echo sys::parseTpl('sys::errors',$_errors);
//====================================================//


/**
 * Array fix
 */
function _arrayFix($value) {
  reset($value);
  while (list($key, $val) = each($value)) {
    if (is_array($val)) $value[$key] = _arrayFix($val);
    elseif (!is_object($val)) $value[$key] = htmlspecialchars($val);
  }
  return $value;
}

/**
 * Print variables
 */
function sprint($val) {
  if ($_SERVER['REMOTE_ADDR'] == '188.239.13.151') {
      $__pre_b = '<pre style="font-family:\'Courier New\', sans-serif; font-size:12px; background:#eee; color:#000; text-align:left; padding:1px 5px 2px 5px; border-bottom:1px dotted #bbb; border-top:1px dotted #bbb; margin:0 0 5px 0;">';
      $__pre_e = '</pre>';
      print($__pre_b);
      if (is_array($val)) {
        $val = _arrayFix($val);
        print_r($val);
        reset($val);
      } else if (is_object($val)) {
        print_r($val);
      } else {
        print(htmlspecialchars($val));
      }
      print($__pre_e);
  }
}


?>