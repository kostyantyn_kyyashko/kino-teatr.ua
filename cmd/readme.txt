* * * * * multiplex /usr/local/www/virtual/multiplex/multiplex.in.ua/cmd/run.sh sys scheduler >> /dev/null 2>&1
* * * * * multiplex /usr/local/www/virtual/multiplex/multiplex.in.ua/cmd/run.sh sys mailer >> /dev/null 2>&1

* * * * *
| | | | |
| | | | +----- Дни недели (диапазон: 1-7)
| | | +------- Месяцы     (диапазон: 1-12)
| | +--------- Дни месяца (диапазон: 1-31)
| +----------- Часы       (диапазон: 0-23)
+------------- Минуты     (диапазон: 0-59)

MAILTO=development@kino-teatr.ua
0 0 * * 1 multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/sitemap_articles.phtml
1 0 * * 1 multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/sitemap_gossip.phtml
2 0 * * 1 multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/sitemap_interview.phtml
3 0 * * 1 multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/sitemap_news.phtml
30 3 * * 1 multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/sitemap_videos.phtml
4 0 * * 1 multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/sitemap_reviews.phtml
10 0 * * 1 multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/sitemap_serials.phtml

#0 0 * * 1 multiplex  cd /usr/local/www/virtual/multiplex/multiplex.in.ua/mods/main/ && php sitemap_articles.phtml; php ./sitemap_gossip.phtml; php ./sitemap_interview.phtml; php ./sitemap_news.phtml; php ./sitemap_reviews.phtml

0 1 1 * * multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/sitemap_cinemas.phtml
1 1 1 * * multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/sitemap_films.phtml
2 1 1 * * multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/sitemap_persons.phtml
0 1 * * 3 multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/box_parse.phtml

30 6 * * * multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/mail_export.phtml
0 13 * * * multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/mail_export.phtml
0 7 * * * multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/mail_export.phtml

#0 1 1 * * multiplex  cd /usr/local/www/virtual/multiplex/multiplex.in.ua/mods/main/ && php ./sitemap_cinemas.phtml; php ./sitemap_films.phtml; php ./sitemap_persons.phtml

30 10 * * * multiplex wget -O /dev/null http://kino-teatr.ua/ru/main/odessa_export.phtml


Таблиця для weekendadmin:
1       1       1       1       1       /dev/null
@daily  /usr/bin/find /usr/local/www/virtual/weekendadmin/services/rest/logs/ -regex '.*[0-9][0-9]' -mtime +7 -exec '/bin/gzip' {} \;
@daily  /usr/bin/find /usr/local/www/virtual/weekendadmin/services/rest/logs/ -name '*.gz' -mtime +31 -exec '/bin/rm' {} \;
*/10 * * * * cd /usr/local/www/virtual/weekendadmin/services/rest/statistics; ./update_unique_sessions.sh
*/11 * * * * cd /usr/local/www/virtual/weekendadmin/services/rest/statistics; ./graph_unique_sessions.sh