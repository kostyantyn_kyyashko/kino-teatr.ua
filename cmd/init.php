<?php
set_time_limit(0);
//Убрать лишние кавычки
//set_magic_quotes_runtime(0);
function getmicrotime()
{ 
    list($usec, $sec) = explode(' ',microtime()); 
    return ((float)$usec + (float)$sec); 
}

function strips(&$var) 
{ 
	if (is_array($var)) 
		foreach($var as $key=>$val) 
			strips($var[$key]); 
	else 
		$var = stripslashes($var); 
}

if(get_magic_quotes_gpc()) 
{ 
	strips($_GET);
	strips($_POST);
	strips($_COOKIE); 
	strips($_REQUEST);
	
	if(isset($_SERVER['PHP_AUTH_USER'])) 
		strips($_SERVER['PHP_AUTH_USER']); 
	if(isset($_SERVER['PHP_AUTH_PW']))   
		strips($_SERVER['PHP_AUTH_PW']);
}
//======================================================//

//Задать формат для ввода даты
function setDateFormat()
{
	global $_cfg;
	$time_format=strftime('%X',mktime(1,2,3,4,5,2006));
	$time_format=str_replace('01','%H',$time_format);
	$time_format=str_replace('02','%M',$time_format);
	$time_format=str_replace('03','%S',$time_format);
	$date_format=strftime('%x',mktime(1,2,3,4,5,2006));
	
	$date_format=str_replace('04','%m',$date_format);
	$date_format=str_replace('05','%d',$date_format);
	$date_format=str_replace('2006','%Y',$date_format);
	$date_format=str_replace('06','%Y',$date_format);
	
	
	if(!$_cfg['sys::date_format'])
		$_cfg['sys::date_format']=$date_format;
	if(!$_cfg['sys::time_format'])
		$_cfg['sys::time_format']=$time_format;
	
	$_cfg['sys::date_time_format']=$date_format.' '.$time_format;
}
//=========================================================================================//

define('_MODE','cmd');

//Подключить файл конфигурации
require_once('../../cfg.php');
$_cfg['error_reporting']=6143;
$_cfg['debugger']=false;
//=====================================//

//Подключить библиоткеку системных функций и класс работы с БД
require_once(_ROOT_DIR.'mods/sys/include/lib.sys.php');
require_once(_ROOT_DIR.'mods/sys/include/cls.db.php');
$_db=new cls_sys_db();
$_db->query("SET NAMES 'UTF8'"); //задать кодировку данных БД
//=========================================================================//

//получить e-mail суперпользователя
$_cfg['sys::root_email']=$_db->getValue('sys_users','email',7);

//Загрузить настройки модулей-------------------------------------
$r=$_db->Query("SELECT CONCAT(mods.name, '::', cfg.name) AS `name`, 
		cfg.value 
		FROM `#__sys_mods_cfg` AS `cfg` 
		LEFT JOIN `#__sys_mods` AS `mods` 
		ON cfg.mod_id=mods.id");
while($cfg = $_db->fetchArray($r))
{	
	$_cfg[$cfg['name']]=$cfg['value'];
}
unset($cfg);
//========================================================

//Загрузить системные языки
$r=$_db->query("SELECT `id`, `code`, `title`, `short_title` 
		FROM `#__sys_langs`
		WHERE `on`=1
		ORDER BY `order_number`");
$_langs=array();
while($lang=$_db->fetchAssoc($r))
	$_langs[$lang['id']]=$lang;
//============================================//

//Определить скин и пути к ресурсам скина-------------------------------------
define('_SKIN',$_cfg['sys::frontend_skin']);
define('_SKIN_URL',_MODS_URL.'main/skins/'._SKIN.'/');
define('_SKIN_DIR',_MODS_DIR.'main/skins/'._SKIN.'/');
define('_IMG_URL',_SKIN_URL.'img/');
define('_IMG_DIR',_SKIN_DIR.'img/');
define('_PIXEL',_SKIN_URL.'img/pixel.gif');
//===========================================================================//

/**
 * Array fix
 */
function _arrayFix($value) {
  reset($value);
  while (list($key, $val) = each($value)) {
    if (is_array($val)) $value[$key] = _arrayFix($val);
    elseif (!is_object($val)) $value[$key] = htmlspecialchars($val);
  }
  return $value;
}

/**
 * Print variables
 */
function sprint($val) {
  $__pre_b = '<pre style="font-family:\'Courier New\', sans-serif; font-size:12px; background:#eee; color:#000; text-align:left; padding:1px 5px 2px 5px; border-bottom:1px dotted #bbb; border-top:1px dotted #bbb; margin:0 0 5px 0;">';
  $__pre_e = '</pre>';
  print($__pre_b);
  if (is_array($val)) {
    $val = _arrayFix($val);
    print_r($val);
    reset($val);
  } else if (is_object($val)) {
    print_r($val);
  } else {
    print(htmlspecialchars($val));
  }
  print($__pre_e);
}
?>
