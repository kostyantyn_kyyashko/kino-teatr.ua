<?

require_once('../init.php');

function check($tpl, $date)
{
	if($tpl=='*' || $tpl==$date)
		return true;
		
	if(strpos($tpl,'/')!==false)
	{
		$range_step=explode('/',$tpl);
		if(strpos($range_step[0],'-')!==false)
		{
			$range=explode('-',$range_step[0]);
			if($date>=$range[0] && $date<=$range[1] && (($date-$range[0]+1)%$range_step[1]==0))
				return true;
			else 
				return false;
		}
		if($date%$range_step[1]==0)
			return true;
		else 
			return false;
	}
		
	if(strpos($tpl,'-')!==false)
	{
		$range=explode('-',$tpl);
		if($date>=$range[0] && $date<=$range[1])
			return true;
		else 
			return false;
	}
	
	if(strpos($tpl,',')!==false)
	{
		$list=explode(',',$tpl);
		return in_array($date,$list);
	}
	

	return false;
	
}

$time=time();
$min=date('i',$time);
$hour=date('G',$time);
$mday=date('j',$time);
$month=date('n',$time);
$wday=date('w',$time);
if($wday==0)
	$wday=7;

//echo $min.' '.$hour.' '.$mday.' '.$month.' '.$wday;

//mail("master@obelchenko.ru", "test-1", "message-1-".rand());

$_r=$_db->query("
	SELECT tsk.*, mods.name AS `mod` 
	FROM `#__sys_mods_tasks` AS `tsk`
	LEFT JOIN `#__sys_mods` AS `mods`
	ON tsk.mod_id=mods.id
	WHERE tsk.`on`=1
");
$tasks=array();
while ($task=$_db->fetchAssoc($_r))
{
	$run=false;
    if(check($task['min'],$min) && check($task['hour'],$hour) && check($task['mday'],$mday) && check($task['month'],$month) && check($task['wday'],$wday))
	{
		$path=_CMD_DIR.$task['mod'].'/'.$task['name'].'.php';
		if(is_file($path))
		{
			$_db->setValue('sys_mods_tasks','last_run',gmdate('Y-m-d H:i:s'),intval($task['id']));
			require($path);
		}
		
	}
}
?>