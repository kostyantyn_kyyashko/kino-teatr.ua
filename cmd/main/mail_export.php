<?
require_once('../init.php');

// отправка письма
$subject = "Export mail.ru";
$message = ' Script started at  ';
$message .= date("H:i:s  d.m.Y");
$headers  = "Content-type: text/html; charset=windows-1251 \r\n";
$headers .= "From: Export mailer.ru <admin@kino-teatr.ua>\r\n";

$to  = "o.boiko@kino-teatr.ua" ;
mail($to, $subject, $message, $headers);

if(!$task['id'])
	die('error');
	
// Вывод времени срабатывания
$fp = fopen("/var/www/html/multiplex/multiplex.in.ua/public/test/logger.txt", "a"); // Открываем файл в режиме записи
fwrite($fp, date("H:i:s  d.m.Y \r\n")); // Запись в файл
fclose($fp); //Закрытие файла

main_mail_export();	

function main_mail_export()
{
/*
	$ips=file(_SECURE_DIR.'export.txt');
	foreach ($ips as $ip)
	{
		$ip=explode('#',$ip);
		$ips2[]=trim($ip[0]);
	}
*/	
//	if(!in_array($_SERVER['REMOTE_ADDR'],$ips2))
//		return 404;
	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('main::persons');
	sys::useLib('main::reviews');
	sys::useLib('main::countries');
	sys::useLib('sys::form');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	require_once('../../mods/main/include/_cfg.php');
	
	$_cfg['sys::lang_id'] = 1;
	
	$result=array();

	$xml = '<?xml version="1.0" encoding="UTF-8"?>
	<billboard>';

	//Жанры----------------------------------------------------------------
	$r=$_db->query("
		SELECT
			gnr.id,
			lng.title
		FROM
			`#__main_genres` AS `gnr`
		LEFT JOIN
			`#__main_genres_lng` AS `lng`
		ON
			lng.record_id=gnr.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			gnr.order_number
	");
	$result['genres']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['genres'][]=$obj;
	}

	//Профессии----------------------------------------
	$r=$_db->query("
		SELECT
			prf.id,
			lng.title
		FROM
			`#__main_professions` AS `prf`
		LEFT JOIN
			`#__main_professions_lng` AS `lng`
		ON
			lng.record_id=prf.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			prf.order_number
	");
	$result['professions']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['professions'][]=$obj;
	}
	//===============================================================================


	//Страны------------------------------------------------------------------------
	$r=$_db->query("
		SELECT
			cnt.id,
			lng.title
		FROM
			`#__main_countries` AS `cnt`
		LEFT JOIN
			`#__main_countries_lng` AS `lng`
		ON
			lng.record_id=cnt.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			cnt.order_number
	");
	$result['countries']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['countries'][]=$obj;
	}

	$r=$_db->query("
		SELECT
			cit.id,
			cit.country_id,
			lng.title
		FROM
			`#__main_countries_cities` AS `cit`
		LEFT JOIN
			`#__main_countries_cities_lng` AS `lng`
		ON
			lng.record_id=cit.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			cit.order_number
	");
	$result['cities']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['cities'][]=$obj;
	}
	//===============================================================================


	//Показы-----------------------------------------------------------------
	$r=$_db->query("
			SELECT
			shw.begin,
			shw.end,
			shw.film_id,
			shw.hall_id,
			shw.id,
			hls.cinema_id,
			hls.`3d` as `hall_3d`,
			flm.`3d` as `film_3d`
		FROM
			`#__main_shows` AS `shw`
		LEFT JOIN
			`#__main_cinemas_halls` AS `hls`
		ON
			hls.id=shw.hall_id
		LEFT JOIN
			`#__main_films` AS `flm`
		ON
			flm.id=shw.film_id

		WHERE
			shw.end>='".mysql_real_escape_string(date('Y-m-d'))."'
		ORDER BY
			shw.end ASC
	");

	$result['shows']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$films[$obj['film_id']]=$obj['film_id'];
		$cinemas[$obj['cinema_id']]=$obj['cinema_id'];
		$halls[$obj['hall_id']]=$obj['hall_id'];
		$shows[$obj['id']]=$obj['id'];

  		if ($obj['hall_3d'] && $obj['film_3d'])
  		{
  			$obj['3d'] = 1;
  		} else {
  			$obj['3d'] = 0;
  		}

		$obj['times']=array();
		$result['shows'][$obj['id']]=$obj;
	}

	//=======================================================================

	//Премьеры

	$r=$_db->query("
		SELECT `id` FROM `#__main_films` WHERE `ukraine_premiere`>'".date('Y-m-d')."'
	");
	while (list($film_id)=$_db->fetchArray($r))
	{
		$films[$film_id]=$film_id;
	}



	//Часы показов--------------------------------------------------------

	$r=$_db->query("
		SELECT
			tms.time,
			tms.prices,
			tms_lng.note,
			tms.show_id
		FROM
			`#__main_shows_times` AS `tms`
		LEFT JOIN
			`#__main_shows_times_lng` AS `tms_lng`
		ON
			tms_lng.record_id=tms.id
		AND
			tms_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			tms.show_id IN(".implode(',',$shows).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['prices']=str_replace(';',',',$obj['prices']);
		$result['shows'][$obj['show_id']]['times'][]=$obj;
	}
	//=========================================================================

	//Фильмы------------------------------------------------------------------
	$r=$_db->query("
		SELECT
			flm.id,
			lng.title,
			flm.title_orig,
			flm.duration,
			flm.year,
			flm.ukraine_premiere,
			flm.world_premiere,
			flm.age_limit,
			flm.budget,
			flm.budget_currency,
			lng.intro,
			lng.text,
			flm.rating,
			flm.votes,
			flm.`3d` as `film_3d`,
			flm.pro_rating,
			flm.children,
			flm.pro_votes
		FROM
			`#__main_films` AS `flm`
		LEFT JOIN
			`#__main_films_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".$_cfg['sys::lang_id']."
		WHERE
			flm.id IN(".implode(',',$films).")

	");
	$result['films']=array();
	{
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['title_orig']=htmlspecialchars($obj['title_orig']);
			$obj['photos']=array();
			$obj['trailers']=array();
			$obj['posters']=array();
			$obj['persons']=array();
			$obj['genres']=array();
			$obj['countries']=array();
			$obj['studios']=array();
			$obj['reviews']=array();
			$obj['links']=array();
  			$poster='';
			$poster=main_films::getFilmFirstPoster($obj['id']);
			$img = str_replace(_ROOT_URL,'',$_cfg['main::films_url'].$poster['image']);
			$obj['poster']=_ROOT_URL.'public/main/rescrop3.php?f='.$img;

			$result['films'][$obj['id']]=$obj;
		}
	}
	//============================================================================

	//Фотки фильмов-------------------------------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`image`
		FROM
			`#__main_films_photos`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['src']=$_cfg['main::films_url'].$obj['image'];
		$result['films'][$obj['film_id']]['photos'][]=$obj;
	}
	//==========================================================

	//Трейлеры фильмов-------------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`file`,
			`url`
		FROM
			`#__main_films_trailers`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		if($obj['file'])
			$obj['url']=$_cfg['main::films_url'].$obj['file'];
		$obj['url']=htmlspecialchars($obj['url']);
		$result['films'][$obj['film_id']]['trailers'][]=$obj;
	}
	//=======================================================================


	//Перcоны фильмов------------------------------------------------------
	$r=$_db->query("
		SELECT
			fp.film_id,
			fp.person_id,
			fp.profession_id,
			lng.role
		FROM
			`#__main_films_persons` AS `fp`
		LEFT JOIN
			`#__main_films_persons_lng` AS `lng`
		ON
			lng.record_id=fp.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			fp.film_id IN(".implode(',',$films).")
	");
	$persons=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$persons[$obj['person_id']]=$obj['person_id'];
		$obj['role']=htmlspecialchars($obj['role']);
		$result['films'][$obj['film_id']]['persons'][]=$obj;
	}
	//==============================================================

	//Жанры фильмов--------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`genre_id`
		FROM
			`#__main_films_genres`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$result['films'][$obj['film_id']]['genres'][]=$obj['genre_id'];
	}
	//================================================

	//Страны фильмов--------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`country_id`
		FROM
			`#__main_films_countries`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$result['films'][$obj['film_id']]['countries'][]=$obj['country_id'];
	}
	//================================================

	//Студии фильмов--------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`studio_id`
		FROM
			`#__main_films_studios`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$result['films'][$obj['film_id']]['studios'][]=$obj['studio_id'];
	}
	//================================================

	//Рецензии на фильмы--------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`id`,
			`title`
		FROM
			`#__main_reviews`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_reviews::getReviewUrl($obj['id']);
		$result['films'][$obj['film_id']]['reviews'][]=$obj;
	}
	//================================================

	//ссылки на фильмы--------------------------------------
	$r=$_db->query("
		SELECT
			lnk.film_id,
			lnk.url,
			lng.title
		FROM
			`#__main_films_links` AS `lnk`
		LEFT JOIN
			`#__main_films_links_lng` AS `lng`
		ON
			lng.record_id=lnk.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			lnk.film_id IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['films'][$obj['film_id']]['links'][]=$obj;
	}
	//================================================



	//Кинотеатры-----------------------------------------------------
	$r=$_db->query("
		SELECT
			cnm.id,
			cnm.city_id,
			lng.title,
			cnm.site,
			cnm.phone,
			lng.address,
			lng.text
		FROM
			`#__main_cinemas` AS `cnm`
		LEFT JOIN
			`#__main_cinemas_lng` AS `lng`
		ON
			lng.record_id=cnm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
");
//		WHERE cnm.id IN(".implode(',',$cinemas).")
//	");
	$result['cinemas']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['address']=htmlspecialchars($obj['address']);
		$obj['phone']=htmlspecialchars($obj['phone']);
		$obj['site']=htmlspecialchars($obj['site']);
		$obj['halls']=array();
		$obj['photos']=array();
		$result['cinemas'][$obj['id']]=$obj;
	}


	//============================================================

	//Фотки кинотеатра------------------------------------------------
	$r=$_db->query("
		SELECT
			`cinema_id`,
			`image`
		FROM
			`#__main_cinemas_photos`
		WHERE
			`cinema_id` IN(".implode(',',$cinemas).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{


		$img = str_replace(_ROOT_URL,'',$_cfg['main::cinemas_url'].$obj['image']);
		$result['cinemas'][$obj['cinema_id']]['photo']=_ROOT_URL.'public/main/rescrop3.php?f='.$img;
	}
	//=============================================================

	//Залы кинотеатра--------------------------------------------
	$r=$_db->query("
		SELECT
			hls.id,
			hls.cinema_id,
			hls.`3d`,
			lng.title,
			hls.scheme
		FROM
			`#__main_cinemas_halls` AS `hls`
		LEFT JOIN
			`#__main_cinemas_halls_lng` AS `lng`
		ON
			lng.record_id=hls.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."

	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		if($obj['scheme'])
			$obj['scheme']=$_cfg['main::cinemas_url'].$obj['scheme'];
		$result['cinemas'][$obj['cinema_id']]['halls'][]=$obj;
	}
	//=============================================================

	//Персоны-------------------------------------------------------
	$result['persons']=array();
	if($persons)
	{
		$r=$_db->query("
			SELECT
				prs.id,
				prs.birthdate,
				prs.lastname_orig,
				prs.firstname_orig,
				lng.lastname,
				lng.firstname,
				lng.biography
			FROM
				`#__main_persons` AS `prs`
			LEFT JOIN
				`#__main_persons_lng` AS `lng`
			ON
				lng.record_id=prs.id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE
				prs.id IN(".implode(',',$persons).")
		");

		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['lastname']=htmlspecialchars($obj['lastname']);
			$obj['firstname']=htmlspecialchars($obj['firstname']);
			$obj['lastname_orig']=htmlspecialchars($obj['lastname_orig']);
			$obj['firstname_orig']=htmlspecialchars($obj['firstname_orig']);
			$obj['url']=main_persons::getPersonUrl($obj['id']);
			$photo=main_persons::getPersonFirstPhoto($obj['id']);
			$img = str_replace(_ROOT_URL,'',$_cfg['main::persons_url'].$photo['image']);
			$obj['poster']=_ROOT_URL.'public/main/rescrop3.php?f='.$img;
			$result['persons'][]=$obj;
		}
	}
	//===============================================================

	$xml .= '
	<genres>';
		foreach ($result['genres'] as $obj)
		{
			$xml .= '
			<genre id="'.$obj['id'].'">'.$obj['title'].'</genre>';
		}
	$xml .= '
	</genres>';


	$xml .= '
	<professions>';
		foreach ($result['professions'] as $obj)
		{
			$xml .= '
			<profession id="'.$obj['id'].'">'.$obj['title'].'</profession>';
		}
	$xml .= '
	</professions>';

	$xml .= '
	<countries>';
		foreach ($result['countries'] as $obj)
		{
			$xml .= '
			<country id="'.$obj['id'].'">'.$obj['title'].'</country>';
		}
	$xml .= '
	</countries>
	<cities>';
		foreach ($result['cities'] as $obj)
		{
			$xml .= '
			<city id="'.$obj['id'].'" country_id="'.$obj['country_id'].'">'.$obj['title'].'</city>';
		}
	$xml .= '
	</cities>';

	$xml .= '
	<cinemas>';
		foreach ($result['cinemas'] as $cinema)
		{
			$xml .= '
			<cinema id="'.$cinema['id'].'" city_id="'.$cinema['city_id'].'">
				<city_id>'.$cinema['city_id'].'</city_id>
				<type>Кинотеатр</type>
				<title>'.$cinema['title'].'</title>
				<id>'.$cinema['id'].'</id>
				<address>'.$cinema['address'].'</address>
				<phone>'.$cinema['phone'].'</phone>';
				if($cinema['site'])
				{
					$xml .= '
					<site>'.$cinema['site'].'</site>';
				}
				$xml .= '
				<text><![CDATA['.$cinema['text'].']]></text>';
					if($cinema['photo'])
					{
						$xml .= '
						<photo src="'.$cinema['photo'].'"/>';
					}

				$xml .= '
				<halls>';
					foreach ($cinema['halls'] as $hall)
					{
						$xml .= '
						<hall id="'.$hall['id'].'">
							<title>'.$hall['title'].'</title>
							<id>'.$hall['id'].'</id>
						</hall>';
					}
				$xml .= '
				</halls>
			</cinema>';
		}
	$xml .= '
	</cinemas>';

	$xml .= '
	<films>';
		foreach ($result['films'] as $film)
		{
			$xml .= '
			<film id="'.$film['id'].'">
				<id>'.$film['id'].'</id>
				<type>Фильм</type>
				<genres>'.implode(',',$film['genres']).'</genres>
				<title>'.$film['title'].'</title>
				<title_orig>'.$film['title_orig'].'</title_orig>
				<intro><![CDATA['.$film['intro'].']]></intro>
				<text></text>
				<rating votes="'.$film['votes'].'">'.$film['rating'].'</rating>
				<poster src="'.$film['poster'].'"/>
				<countries>'.implode(',',$film['countries']).'</countries>
				<ukraine_premiere>'.$film['ukraine_premiere'].'</ukraine_premiere>
				<world_premiere>'.$film['world_premiere'].'</world_premiere>
				<kids>'.$film['children'].'</kids>
				<persons>';

				foreach ($film['persons'] as $person)
				{
					$xml .= '
					<person id="'.$person['person_id'].'" profession_id="'.$person['profession_id'].'"  role="'.$person['role'].'"/>';
				}
				$xml .= '
				</persons>
			</film>';
		}
	$xml .= '
	</films>';

	$xml .= '
	<persons>';
		foreach ($result['persons'] as $person)
		{
			$xml .= '
			<person id="'.$person['id'].'">
				<id>'.$person['id'].'</id>
				<name>'.$person['firstname'].' '.$person['lastname'].'</name>
				<name_orig>'.$person['firstname_orig'].' '.$person['lastname_orig'].'</name_orig>
				<birthdate>'.$person['birthdate'].'</birthdate>
				<photo src="'.$person['photo'].'"/>
				<biography><![CDATA['.$person['biography'].']]></biography>
			</person>';
		}
	$xml .= '
	</persons>';


	$xml .= '
	<shows>';
		foreach ($result['shows'] as $show)
		{
		 	$night = 0;

			$xml .= '
			<show id="'.$show['id'].'" film_id="'.$show['film_id'].'" cinema_id="'.$show['cinema_id'].'" hall_id="'.$show['hall_id'].'">
				<film_id>'.$show['film_id'].'</film_id>
				<cinema_id>'.$show['cinema_id'].'</cinema_id>
				<hall_id>'.$show['hall_id'].'</hall_id>
				<begin>'.$show['begin'].'</begin>
				<ddd>'.$show['3d'].'</ddd>
				<end>'.$show['end'].'</end>
				<times>';
					foreach ($show['times'] as $time)
					{

						if ($time['time'] < '05:00:00')
						{
							$night = 1;
						} else {
							$xml .= '
							<time time="'.$time['time'].'">
								<prices>'.$time['prices'].'</prices>
							</time>';
						}

					}
				$xml .= '
				</times>
			</show>';

			if ($night)
			{
				$xml .= '
				<show id="'.$show['id'].'" film_id="'.$show['film_id'].'" cinema_id="'.$show['cinema_id'].'" hall_id="'.$show['hall_id'].'">
					<film_id>'.$show['film_id'].'</film_id>
					<cinema_id>'.$show['cinema_id'].'</cinema_id>
					<hall_id>'.$show['hall_id'].'</hall_id>
					<begin>'.(date('Y-m-d', strtotime($show['begin'].' +1 day'))).'</begin>
					<ddd>'.$show['3d'].'</ddd>
					<end>'.(date('Y-m-d', strtotime($show['end'].' +1 day'))).'</end>
					<times>';
						foreach ($show['times'] as $time)
						{
							if ($time['time'] < '05:00:00')
							{
								$xml .= '
									<time time="'.$time['time'].'">
										<prices>'.$time['prices'].'</prices>
									</time>';
							}
						}
					$xml .= '
					</times>
				</show>';
			}

		}
	$xml .= '
	</shows>';






	$xml .= '
	</billboard>';


	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/mailru.xml";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $xml);
	fclose($fh);
}
	?>
