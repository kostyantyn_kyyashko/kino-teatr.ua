<?php

$ch = curl_init('https://planetakino.ua/showtimes/xml/');
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_VERBOSE,true);

if(curl_exec($ch) === false)
{
   echo 'Ошибка curl: ' . curl_error($ch).'<br>';
}
else
{
   echo 'Операция завершена без каких-либо ошибок<br>';
}

curl_exec($ch);
curl_close($ch);

?>
