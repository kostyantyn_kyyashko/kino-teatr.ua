<?
require_once('../init.php');
if(!$task['id'])
	die('error');

//////////////////////////////////////////////////////////////////////////////
/// Configuration
//////////////////////////////////////////////////////////////////////////////

$email = "noreply@kino-teatr.ua,cm@kino-teatr.ua";
//$email = "master@obelchenko.ru";
$headers ="MIME-Version: 1.0" . "\r\n";
$headers.="Content-Type: text/html; charset=utf-8" . "\r\n";
$headers.="Content-Transfer-Encoding: 8bit" . "\r\n";
$headers.="From: =?utf-8?b?".base64_encode("Отчет push")."?=<admin@kino-teatr.ua>"  . "\r\n";
$subject="=?utf-8?b?".base64_encode("Отчет push")."?=";

//////////////////////////////////////////////////////////////////////////////
/// Functions
//////////////////////////////////////////////////////////////////////////////
function PutLog($s)
{
	file_put_contents ("../../logs/push.log", $s."\r\n", FILE_APPEND);
}

function push_message($title, $body, $link="https://kino-teatr.ua/spec_themes/WEEKEND-90.phtml") 
{
	// https://login.sendpulse.com/manual/
    $query = "grant_type=client_credentials&client_id=4381666c93a13d9e0de0412dd89b2ffc&client_secret=b8ab2689a913f1149bc2f2c063f945ba";
    $x = curl_init("https://api.sendpulse.com/oauth/access_token");
    curl_setopt($x, CURLOPT_HEADER, 0);
    curl_setopt($x, CURLOPT_POST, 1);
    curl_setopt($x, CURLOPT_POSTFIELDS, $query);
    curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($x, CURLOPT_REFERER, "//kino-teatr.ua");
    curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($x);
    curl_close($x);
    $data = json_decode($data);    
    $access_token = $data->access_token?$data->access_token:0;
    unset($data);
 /*   
    $query = "access_token=$access_token&limit=10&offset=0";
	$url = "https://api.sendpulse.com/push/websites/?$query";
    print "$url<p>";
	$x = curl_init($url);
    curl_setopt($x, CURLOPT_HEADER, 0);
    curl_setopt($x, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($x, CURLOPT_REFERER, "//kino-teatr.ua");
    curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($x);
    curl_close($x);
    var_dump($data);    
    print "<hr>";
 // string(166) "[{"id":26694,"url":"kino-teatr.ua","add_date":"2016-12-19 16:16:45","status":1}]" 
*/    
 
 	$params[] = "access_token=$access_token";
 	$params[] = "website_id=26694";
 	$params[] = "ttl=" . (60 * 60 * 5);
 	$params[] = "title=$title";
 	$params[] = "body=$body";
 	$params[] = "link=$link";

 	$query = implode("&", $params);
    $x = curl_init("https://api.sendpulse.com/push/tasks");
    curl_setopt($x, CURLOPT_HEADER, 0);
    curl_setopt($x, CURLOPT_POST, 1);
    curl_setopt($x, CURLOPT_POSTFIELDS, $query);
    curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($x, CURLOPT_REFERER, "//kino-teatr.ua");
    curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);
    
    curl_setopt($x, CURLOPT_ENCODING, "");
    curl_setopt($x, CURLOPT_MAXREDIRS, 10);
    curl_setopt($x, CURLOPT_TIMEOUT, 30);
    curl_setopt($x, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($x, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($x, CURLOPT_HTTPHEADER, array(
	    "authorization: Bearer $access_token",
	    "cache-control: no-cache",
	    "content-type: application/x-www-form-urlencoded",
  	));

  	$data = curl_exec($x);
    curl_close($x);
//    $data = json_decode($data);

	PutLog(date("d.m.Y H:i") . "\r\n$title\r\n$body\r\n$link\r\n");
	
	global $email,$subject,$headers;
	mail($email,$subject,date("d.m.Y H:i") . "\r\n$title\r\n$body\r\n$link\r\n",$headers);	
}
// Рассылка премьер недели	
 $q="SELECT spec.article_id, art.name
 		FROM 
 			`#__main_articles_articles_spec_themes` as spec
 		
 		LEFT JOIN
 			`#__main_articles_articles` as art
 		ON art.id=spec.article_id
 			
		WHERE 
			spec.`spec_theme_id`=90 AND `art`.`public` AND DATE(`art`.`date`)<=CURDATE() AND NOT spec.`pushed`
		LIMIT 1	
	";
  $articles=$_db->query($q);
  if ($row=$_db->fetchAssoc($articles)) 
  {
  	 $id = $row["article_id"];
	 $q="UPDATE 
	 			`#__main_articles_articles_spec_themes`
			SET
				`pushed` = 1
	 		WHERE 
				`spec_theme_id`=90 AND `article_id`=$id
		";  	
	 $_db->query($q);
	 push_message("Новости kino-teatr.ua", $row["name"], "https://kino-teatr.ua/articles/WEEKEND-premeryi-nedeli-$id.phtml");
  }
	
// Рассылка новостей с тагом "трейлер"  
 $q="	SELECT * 
 		FROM `#__main_news_articles` 
 		WHERE NOT `pushed` AND `tags` LIKE \"%трейлер%\" AND `public` AND DATE(`date`)<=CURDATE()
 		ORDER BY id DESC 
		LIMIT 1	
	";
  $articles=$_db->query($q);
  if ($row=$_db->fetchAssoc($articles)) 
  {
  	 $id = $row["id"];
	 $q="UPDATE 
	 			`#__main_news_articles`
			SET
				`pushed` = 1
	 		WHERE 
				`id`=$id
		";  	
	 $_db->query($q);
	 push_message("Новости kino-teatr.ua", $row["name"], "https://kino-teatr.ua/news/news-with-trailer-$id.phtml");
  }
	
// Рассылка сериалов с тагом "трейлер"  
 $q="	SELECT * 
 		FROM `#__main_serials_articles` 
 		WHERE NOT `pushed` AND `tags` LIKE \"%трейлер%\" AND `public` AND DATE(`date`)<=CURDATE()
 		ORDER BY id DESC 
		LIMIT 1	
	";
  $articles=$_db->query($q);
  if ($row=$_db->fetchAssoc($articles)) 
  {
  	 $id = $row["id"];
	 $q="UPDATE 
	 			`#__main_serials_articles`
			SET
				`pushed` = 1
	 		WHERE 
				`id`=$id
		";  	
	 $_db->query($q);
	 push_message("Новости kino-teatr.ua", $row["name"], "https://kino-teatr.ua/serials/serial-with-trailer-$id.phtml");
  }
	
// Рассылка новых конкурсов
 $q="	SELECT * 
 		FROM `#__main_contest_articles` 
 		WHERE NOT `pushed` AND `public` AND DATE(`date`)<=CURDATE() 
 		ORDER BY id DESC 
		LIMIT 1	
	";
  $articles=$_db->query($q);
  if ($row=$_db->fetchAssoc($articles)) 
  {
  	 $id = $row["id"];
	 $q="UPDATE 
	 			`#__main_contest_articles`
			SET
				`pushed` = 1
	 		WHERE 
				`id`=$id
		";  	
	 $_db->query($q);
	 push_message("Новый конкурс на kino-teatr.ua", "Прими участие в конкурсе: ".$row["name"], "https://kino-teatr.ua/ru/main/contest-$id.phtml");
  }
?>