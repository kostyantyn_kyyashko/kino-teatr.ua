<?
require_once('../init.php');
if(!$task['id'])
die('error');

sys::useLib('main::shows');

///////////////////////////////////////////////////////
function getCinemaBillBoard($cinema_id, $user)
{
	sys::useLib('main::cinemas');
	sys::useLib('main::films');
	sys::useLib('main::cards');
	global $_db, $_cfg;

	$date=sys::date2Db(date("d.m.Y"),false,'%d.%m.%Y');
	$lang = $_cfg['sys::lang_id']?intval($_cfg['sys::lang_id']):1;

	$q="
			SELECT
				shw.begin,
				shw.end,
				shw.film_id,
				shw.partner_id,
				flm.title_orig,
				flm.rating,
				flm.pre_rating,
				flm.votes,
				flm.pre_votes,
				flm.year,
				flm.`3d` as `film_3d`,
				shw.id AS `show_id`,
				hls_lng.title AS `hall`,
				cnm_lng.title AS `cinema`,
				cnm.site,
				cnm.notice_begin,
				cnm.notice_end,
				cnm_lng.notice,
				cnm.ticket_url,
				cnm.phone,
				hls.cinema_id,
				hls.id AS `hall_id`,
				hls.scheme,
				hls.`3d`,
				flm_lng.title AS `film`,
				shw.hall_id
				
			FROM `#__main_shows` AS `shw`

			LEFT JOIN `#__main_cinemas_halls` AS `hls`
			ON hls.id=shw.hall_id

			LEFT JOIN `#__main_films` AS `flm`
			ON flm.id=shw.film_id			

			LEFT JOIN `#__main_cinemas` AS `cnm`
			ON cnm.id=hls.cinema_id

			LEFT JOIN `#__main_cinemas_lng` AS `cnm_lng`
			ON cnm_lng.record_id=cnm.id
			AND cnm_lng.lang_id=".$lang."

			LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng`
			ON hls_lng.record_id=hls.id
			AND hls_lng.lang_id=".$lang."

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=shw.film_id
			AND flm_lng.lang_id=".$lang."

			WHERE 
			cnm.id=".intval($cinema_id)."
			AND shw.end>='".mysql_real_escape_string($date)."'
			AND shw.begin<='".mysql_real_escape_string($date)."'			

			ORDER BY cnm.order_number, shw.hall_id, hls.order_number, shw.begin
			";

	$r=$_db->query($q);

	$result=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$q = "
				SELECT tms.time, tms.prices, tms_lng.note, tms.3d as seans_3d, tms.sale_id, tms.sale_status, tms.id as time_id
				FROM `#__main_shows_times` AS `tms`
				
				LEFT JOIN `#__main_shows_times_lng` AS `tms_lng`
				ON tms_lng.record_id=tms.id
				AND tms_lng.lang_id=".$lang."
				
				WHERE 
					tms.show_id=".$obj['show_id']." AND tms.time >= '00:00' AND tms.time <= '23:59'
				ORDER BY tms.time
			";			

		$r2=$_db->query($q);

		$obj['prices']=array();
		while ($time=$_db->fetchAssoc($r2))
		{
			$time['past']=false;
			if($date==date('Y-m-d').' 00:00:00')
			{
				$timestamp=sys::date2Timestamp(date('Y-m-d').' '.$time['time'],'%Y-%m-%d %H:%M:%S');
				if($timestamp<time())
				$time['past']=true;
				else
				$time['past']=false;
			}


			$time['time']=sys::cutStrRight($time['time'],3);
			//				$time['url']=main_cards::getCardUrl($time['time'],$_GET['date'],$obj['film_id'],$obj['hall_id']);
			$prices=explode(',',$time['prices']);
			$obj['prices']=array_merge($obj['prices'],$prices);
			$obj['times'][]=$time;
		}

		$obj['prices']=array_unique($obj['prices']);
		sort($obj['prices']);
		$obj['price_range']=trim($obj['prices'][0]);
		if(count($obj['prices'])>1)
		{
			$obj['price_range'].='-'.trim($obj['prices'][count($obj['prices'])-1]);
		}

		//			if($obj['scheme'])
		//				$obj['scheme_url']=main_cinemas::getHallSchemeUrl($obj['hall_id']);
		//			else
		//				$obj['scheme_url']=false;

		$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
		$obj['cinema_url']=main_cinemas::getCinemaUrl($obj['cinema_id']);

		$obj['begin']=sys::db2Date($obj['begin'],false, $_cfg['sys::date_format']);
		$obj['end']=sys::db2Date($obj['end'],false, $_cfg['sys::date_format']);


		$result[$obj['cinema_id']]['title']=$obj['cinema'];
		if(strtotime($obj['notice_begin'])<time() && time()<strtotime($obj['notice_end']))
		{
			$result[$obj['cinema_id']]['notice']=$obj['notice'];
		}
		$result[$obj['cinema_id']]['phone']=$obj['phone'];
		$result[$obj['cinema_id']]['id']=$obj['cinema_id'];
		$result[$obj['cinema_id']]['site']=$obj['site'];

		/*if($obj['ticket_url'] && $_user['id']==2)
		$obj['ticket_url']=sys::rewriteUrl('?mod=main&act=register');*/
		//			$result[$obj['cinema_id']]['ticket_url']=$obj['ticket_url'];
		$result[$obj['cinema_id']]['shows_url']=main_cinemas::getCinemaShowsUrl($obj['cinema_id']);
		$result[$obj['cinema_id']]['url']=main_cinemas::getCinemaUrl($obj['cinema_id']);

		$result[$obj['cinema_id']]['halls'][$obj['hall_id']]['title']=$obj['hall'];
		$result[$obj['cinema_id']]['halls'][$obj['hall_id']]['3d']=$obj['3d'];
		$result[$obj['cinema_id']]['halls'][$obj['hall_id']]['scheme_url']=$obj['scheme_url'];
		$result[$obj['cinema_id']]['halls'][$obj['hall_id']]['url']=main_cinemas::getHallUrl($obj['cinema_id'],$obj['hall_id']);
		$result[$obj['cinema_id']]['halls'][$obj['hall_id']]['shows'][]=$obj;

		if (count($obj['times'])!=0)
		{
			$result[$obj['cinema_id']]['shows'][$obj['show_id']]=$obj;
			$result[$obj['cinema_id']]['countfilms']++;
		}
	}

	return $result;
}

function tplBillBoard($billboard)
{
	global $_db, $_cfg;
	$countfilms = 0;

	//		$_db->printR($billboard);

	ob_start();
?>
<table width="100%" cellpadding="3" cellspacing="3" border="0">
<?
foreach ($billboard as $obj) {

	// массив с индексами для *
	$notes = array();
	foreach ($obj['shows'] as $show)
	foreach ($show['times'] as $time)
	{
		$trim_note = $time["note"];
		if($trim_note && !in_array($trim_note, $notes)) $notes[] = $trim_note;
	}

	if($obj['countfilms']>0):
	$countfilms += $obj['countfilms'];
			?>
				<tr>
					<th colspan=5>
						<b><a href='<?=$obj['url']?>' title='<?=sys::translate('main::cinema')?> <?=$obj['title']?>'><?=sys::translate('main::cinema')?> <?=$obj['title']?></a></b>
					</th>
				</tr>

				<tr>
					<th><?=sys::translate('main::film')?></th>
					<th><?=sys::translate('main::hall')?></th>
					<th><?=sys::translate('main::shows')?></th>
					<th>&nbsp;</th>
					<th><?=sys::translate('main::prices')?></th>
				</tr>

				<?
				$arrshows = $obj['shows'];
				usort($arrshows, "cmp");

				foreach ($arrshows as $show) { ?>
				<tr>
					<td><a href='<?=$show['film_url']?>' title="<?=sys::translate('main::film')?> <?=$show['film']?> <?=$show['year']?>"><?=$show['film']?></a></td>
					<td>
					<?if($show['hall']):?>
							<?=$show['hall']?>
					<?else:?>
					&mdash;
					<?endif;?>
					</td>
					<td>

				<?
				$is_2d = 0;
				$has_3d = 0;
				$timesa = array();
				//if(sys::isDebugIP()) sys::printR($show['times']);
				foreach ($show['times'] as $time)
				{
					$has_3d += $time['seans_3d'];
					$star_class = $time['past']?"mark_star_time_past":"mark_star_time";

					$note = array_search($time['note'],$notes,true);
					$note = ($note===FALSE)?"":'<sup class="'.$star_class.'"><a href=# onclick=\'return ShowNote('.$obj['id'].')\'>'.str_repeat("*",$note+1)."</a></sup>";

					$is_2d = ($show['film_3d'] && $show['3d'] && !$time['seans_3d'])?"<sup class='mark3d'><b>2D</b></sup>":"";
					if($time['past'])
					{
						$timesa[] = "<span class='timepast'>".strip_tags($time['time'])."$is_2d</span>$note";
					} else{
						$timesa[] = "<span class='time'>".$time['time']."<b>$is_2d</b></span>$note";
					}
				}

				$timesar = implode('<span class="delimiter">, </span>', $timesa);
							?>

							<?=$timesar?>

					</td>
					<td>
						<?if($show['film_3d'] && $show['3d'] && $has_3d):?>
							<b>3D</b>
						<?else:?>
							&nbsp;
						<?endif;?>
					</td>
					<td><?=$show['price_range']?> <?=sys::translate('main::grn')?></td>
					
				</tr>

			    <? } //endforeach; // of foreach ($obj['shows'] as $show)?>

		<? endif; // if($obj['countfilms']>0):?>
	<? } //endforeach; ?>
	</table>
	<?		
	$result = ob_get_clean();
	
	return  ($countfilms?$result:"");
}

function loadAdminMessage($lang_id)
{
    global $_db;

	$info=$_db->getRecord('delivery_message', "date_from<=curdate() and date_to>=curdate() and trim(concat(title_ru,title_uk))<>'' AND trim(concat(message_ru,message_uk))<>''");

    if($info) 
    {
		if($info[title_uk]=="") $info[title_uk] = $info[title_ru];
		if($info[title_ru]=="") $info[title_ru] = $info[title_uk];
		if($info[title_ru].$info[title_uk] == "") return "";
		
		if($info[message_uk]=="") $info[message_uk] = $info[message_ru];
		if($info[message_ru]=="") $info[message_ru] = $info[message_uk];
		if($info[message_ru].$info[message_uk] == "") return "";

		$lng = ($lang_id==3)?"uk":"ru";
		
		$subject = trim($info["title_$lng"]);
    	$text = trim($info["message_$lng"]);
    	if(trim(strip_tags($subject.$text)) == "") return "";
    	$message = <<<message
    	<table width="98%" cellpadding=10>
    	<!--tr><td bgcolor="#e31e24" style="font-size:14px; padding:10px 20px; color:#fff;"><b>Вам сообщение:</b></td></tr-->
    	
    	<tr><td>
    	<div style="margin: 0pt 0pt 20px;">
			<h4 style="font-size: 14px;">$subject</h4>
			<div style="font-size: 12px;text-align:justify;">$text</div>
		</div>
		</td></tr></table><hr>
message;
    	return $message;
    }
	return "";
}

/////////////////////////  MAIN //////////////////////////////

$_db->setValue('sys_mods_tasks','last_run',gmdate('Y-m-d H:i:s'),intval($task['id']));

$r=$_db->query("SELECT * FROM `#__main_users_cinema_subscribe`");

$users = array();
$otpiska = "<hr>Вы подписаны на рассылку расписания выбранных Вами кинотеатров, для изменения или отписки посетите свой профиль на портале: <a href=https://kino-teatr.ua/ru/main/user_profile.phtml>kino-teatr.ua</a><hr>";

while ($subscribe=$_db->fetchAssoc($r))
{
	$users[$subscribe['user_id']][]=$subscribe['cinema_id'];
}
//$_db->printR($users);
foreach ($users as $user_id=>$cinema)
{
	sys::loadEnvVars($user_id);
	sys::useLang("main"); //Подключить языковой файл модуля
	$email['billboard'] = "";
	foreach ($cinema as $cinema_id)
	{
		$billboard = getCinemaBillBoard($cinema_id);
		$email['billboard'] .= tplBillBoard($billboard);
	}	
	$admin_message = loadAdminMessage($_cfg['sys::lang_id']); // взять из таблицы delivery_message 
	if($email['billboard'])
	{		
		$email['billboard'] = $admin_message.$email['billboard'].$otpiska;
		//sys::printR($email['billboard']);
		sys::addMailTpl($user_id,'main::user_cinema_subscriber',$email); // user_cinema_subscriber - шаблон в админке
	}
}

?>