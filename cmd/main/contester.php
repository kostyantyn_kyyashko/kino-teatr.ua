<?
require_once('../init.php');
if(!$task['id'])
	die('error');
	
sys::useLib('main::contest');

$_db->query("
	LOCK TABLES 
		`#__sys_mods_tasks` WRITE
");
$_db->setValue('sys_mods_tasks','last_run',gmdate('Y-m-d H:i:s'),intval($task['id']));
$_db->query("UNLOCK TABLES");

// выбрать конкурсы к закрытию
$q="SELECT 
		art.id,  
		art.winners,
		art.image,
		art.letter,
		art_lng_ru.title as title_ru,
		art_lng_uk.title as title_uk
		
	FROM `#__main_contest_articles` as art

	LEFT JOIN `#__main_contest_articles_lng` AS `art_lng_ru` ON art_lng_ru.record_id=art.id AND art_lng_ru.lang_id=1
	LEFT JOIN `#__main_contest_articles_lng` AS `art_lng_uk` ON art_lng_uk.record_id=art.id AND art_lng_uk.lang_id=3

	WHERE NOT finished AND public AND `dateend`<Now()";

$contests=$_db->query($q);

while($contest=$_db->fetchAssoc($contests))
{
	$id 	= $contest['id'];
	$winners = $contest['winners']; // Нужно будет выбрать столько победителей
	$image = $contest['image'];
	$letter = $contest['letter'];
	$title_ru = $contest['title_ru'];
	$title_uk = $contest['title_uk'];
	
	// удаляем мусор из списка победителей, если он есть
	$_db->query("DELETE FROM `#__main_contest_winners` WHERE `contest_id`=$id");
	
	// Определяем и фиксируем победителей	
	$qu="SELECT
			art.user_id,
			usr.email,
			usr.lang_id,
			usr_dat.main_first_name as first_name,
			usr_dat.main_last_name as last_name,
			usr_dat.main_nick_name as username
		
		FROM `#__main_contest_participants` AS `art`
		
		LEFT JOIN `#__sys_users` as `usr`
		ON art.user_id = usr.id
		
		LEFT JOIN `#__sys_users_data` as `usr_dat`
		ON art.user_id = usr_dat.user_id
		
		WHERE art.contest_id = $id
		ORDER BY art.rating desc, RAND() LIMIT $winners";

		$ru=$_db->query($qu);
		while($obju=$_db->fetchAssoc($ru))
		{
			$qp="INSERT INTO `#__main_contest_winners` (`contest_id`, `user_id`) VALUES ($id, ".$obju['user_id'].")";
			$rp=$_db->query($qp);
			//sys::sendContestMail('info@kino-teatr.ua','Kino-Teatr.ua',$obju['first_name'].' '.$obju['last_name'].' <'.$obju['email'].'>',$obju['username'],'Kino-Teatr.ua','',$letter,false,'UTF8');

			$email['letter']=$letter;
			$email['title']=($obju['lang_id']==3)?$title_uk:$title_ru;
			$email['url']= ($obju['lang_id']==3)?main_contest::getArticleUkUrl($id):main_contest::getArticleRuUrl($id);			
			sys::addMailTpl($obju['user_id'],'main::contest_winner',$email);
		}
	// закрываем конкурс и отмечаем разосланность
	$_db->query("UPDATE `#__main_contest_articles` SET finished=1, sent=1 WHERE `id`=$id");	
}
?>