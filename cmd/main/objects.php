<?php

$root = dirname(dirname(dirname(__file__)));

sys::useLib('main::films');
sys::useLib('main::persons');
sys::useLib('main::gossip');
sys::useLib('main::interview');
sys::useLib('main::articles');
sys::useLib('main::contest');
sys::useLib('main::reviews');
sys::useLib('main::news');
sys::useLib('main::serials');
//mail("master@obelchenko.ru", "test-2", "message-1-".rand());
$users = array();
$objects = new SObject;

$debug_users = array();
if ($_cfg['sys::delivery_debug']) {
    $debug_users = explode(',', $_cfg['sys::delivery_debug_users']);    
    $debug_users = array_unique($debug_users);
    $debug_users = array_diff($debug_users, array(''));
    $tmp = array();
    reset($debug_users);
    foreach ($debug_users as $val)
        $tmp[] = (int)$val;
    $tmp = array_unique($tmp);
    $debug_users = $tmp;
}
$q = sprintf("
    SELECT
        `object`.*,
        `user`.`email` AS `email`,
        `user`.`login` AS `login`,
        `user`.`lang_id` AS `lang_id`,
        `list`.`name` AS `object_name`
    FROM
        `#__main_users_%s_subscribe` AS `object`
    LEFT JOIN `#__sys_users` AS `user` ON
        `object`.`user_id` = `user`.`id`
    LEFT JOIN `#__main_users_objects_list` AS `list` ON
        `object`.`object_id` = `list`.`id`
    WHERE
        `object`.`w%d` = 1
        %s
",
    $task['name'],
    $wday,
    count($debug_users)
        ? 'AND user_id IN ('.implode(',', $debug_users).')'
        : ''
);
//$_db->printR($q);       
$r = $_db->query($q);

while ($info = $_db->fetchAssoc($r)) 
{
    $oname = $info['object_name']; // films_trailers persons films gossip_articles interview_articles articles_articles reviews news_articles contest_articles serials_articles
    if ($oname) {
        $uid = $info['user_id'];
        if (!isset($users[$uid])) {
            $users[$uid] = array(
                'info' => array(
                    'email' => $info['email'],
                    'login' => $info['login'],
                    'lang_id' => $info['lang_id']
                ),
                'objects' => array()
            );
        }
         
		$datetime = new DateTime($info['last_run']);				
        $unixtime = $datetime->getTimestamp();
        $users[$uid]['objects'][$oname] = $unixtime;
       	if ($objects->last_run[$oname] > $unixtime) 
        			$objects->last_run[$oname] = $unixtime;
    }
}
$objects->build_data();

reset($users);
$cdatetime = new DateTime();
$cdatetime->setTimestamp($objects->time);
$cdatetime = $cdatetime->format('Y-m-d H:i:s');
foreach ($users as $uid=>$val) {
    reset($val['objects']);
    $mailBody = '';
    $files = array();
    foreach ($val['objects'] as $key=>$date) {
        $method = 'get_'.$key;
        if (method_exists($objects, $method)) {
            $letter = $objects->$method($key, $date, $val['info']['lang_id']);
            if (is_array($letter)) {
                $mailBody .= sprintf('<!--debug info: key='.$key.', method='.$method.'-->'.'<table width="790" cellpadding="10"><tr><td bgcolor="#e31e24" style="font-size:14px; padding:10px 20px; color:#fff;"><b>%s</b></td></tr><tr><td>%s</td></tr></table><br>', $letter['subject'], $letter['body']);
                $files = array_merge($files, $letter['images']);
                if ($letter) {
                    $r = $_db->query(sprintf("
                        UPDATE
                            `#__main_users_objects_subscribe` AS `obj`
                        SET
                            `obj`.`last_run` = '%s'
                        WHERE
                            `obj`.`user_id` = %d AND
                            `obj`.`object_id` = %d
                            
                    ",
                        $cdatetime,
                        $uid,
                        $objects->rel[$key]
                    ));
                }
            }
        }
    }
    if ($mailBody) 
    {
		$admin_message = $objects->loadAdminMessage($val['info']['lang_id']); // взять из таблицы delivery_message 
	    $otpiska = "<hr>Отказаться от рассылки Вы можете в своем профиле: <a href=https://kino-teatr.ua/ru/main/user_profile.phtml>перейти</a><hr>";
    	sys::addMail('noreply@kino-teatr.ua', false, $val['info']['email'], $val['info']['login'], 'Рассылка Kino-teatr.ua', false, $admin_message.$mailBody.$otpiska, serialize($files), mb_detect_encoding($mailBody), false, 'sendMailObject');
// debug to Mike
//		$admin_message = $objects->loadAdminMessage($val['info']['lang_id']); // взять из таблицы delivery_message 
		//sys::addMail('noreply@kino-teatr.ua', false, "master@obelchenko.ru", $val['info']['login'], 'Рассылка Kino-teatr.ua', false, $admin_message.$mailBody.$otpiska, serialize($files), mb_detect_encoding($mailBody), false, 'sendMailObject');
//		sys::addMail('noreply@kino-teatr.ua', false, 'noreply@kino-teatr.ua', $val['info']['login'], 'Рассылка Kino-teatr.ua', false, $admin_message.$mailBody.$otpiska, serialize($files), mb_detect_encoding($mailBody), false, 'sendMailObject');
    }
    
}


class SObject {
    
    
    public $rel = array();
    public $time = 0;
    public $allowed = array();
    public $last_run = array();
    
    protected $tpl = array();
    protected $html = array();
    
    
    function __construct() {
        
        global $_db, $task;
        
        $this->time = time();
        $r = $_db->query("SELECT `id`, `name` FROM `#__main_users_objects_list`");
        while ($info = $_db->fetchAssoc($r)) {
            $this->allowed[] = $info['name'];
            $this->last_run[$info['name']] = $this->time;
            $this->rel[$info['name']] = $info['id'];
        }
        
        reset($this->allowed);
        foreach ($this->allowed as $val) {
            $mod_tpl = $task['mod'].'::delivery_'.$val;
            $tpl_1 = sys::parseModTpl($mod_tpl, 'email', false, 1);
            if (!$tpl_1) $tpl_1 = array(
                'title' => '',
                'subject' => '',
                'meta_title' => '',
                'meta_description' => '',
                'meta_keywords' => '',
                'meta_other' => '',
                'text' => '%text%'
            );
            $tpl_3 = sys::parseModTpl($mod_tpl, 'email', false, 3);
            if (!$tpl_3) $tpl_3 = array(
                'title' => '',
                'subject' => '',
                'meta_title' => '',
                'meta_description' => '',
                'meta_keywords' => '',
                'meta_other' => '',
                'text' => '%text%'
            );
            $this->tpl[$val] = array(
                1 => $tpl_1,
                3 => $tpl_3
            );
            $this->html[$val] = array(); 
        }
        
    }
    
    
    public function build_data() {
        reset($this->allowed);
               
        foreach ($this->last_run as $key=>$val) {
            $method = '__load_'.$key;
            if (method_exists($this, $method)) {
                $datetime = new DateTime();
                $datetime->setTimestamp($val);
                $this->$method($key, $datetime->format('Y-m-d H:i:s'));
            }
        }
        
    }
    
    static function loadAdminMessage($lang_id)
    {
        global $_db;

		$info=$_db->getRecord('delivery_message', "date_from<=curdate() and date_to>=curdate() and trim(concat(title_ru,title_uk))<>'' AND trim(concat(message_ru,message_uk))<>''");

        if($info) 
        {
			if($info[title_uk]=="") $info[title_uk] = $info[title_ru];
			if($info[title_ru]=="") $info[title_ru] = $info[title_uk];
			if($info[title_ru].$info[title_uk] == "") return "";
			
			if($info[message_uk]=="") $info[message_uk] = $info[message_ru];
			if($info[message_ru]=="") $info[message_ru] = $info[message_uk];
			if($info[message_ru].$info[message_uk] == "") return "";

			$lng = ($lang_id==3)?"uk":"ru";
			
			$subject = trim($info["title_$lng"]);
        	$text = trim($info["message_$lng"]);
        	if(trim(strip_tags($subject.$text)) == "") return "";
        	$message = <<<message
        	<table width="790" cellpadding=10>
        	<!--tr><td bgcolor="#e31e24" style="font-size:14px; padding:10px 20px; color:#fff;"><b>Вам сообщение:</b></td></tr-->
        	
        	<tr><td><div style="margin: 0pt 0pt 20px;">
			<h4 style="font-size: 14px;">$subject</h4>
			<div style="font-size: 12px;">$text</div>
			<div style="font-size: 11px;">&nbsp;</div>
			</div>
			</td></tr></table>
message;
        	return $message;
        }
    	return "";
    }
    
    private function __load_films_trailers($name, $date) {
        
        global $_db, $task;
        
        $list = array();
        
        $r = $_db->query(sprintf("
            SELECT
                `obj`.*,
                `lng_1`.`title` AS `title_1`,
                `lng_3`.`title` AS `title_3`
            FROM
                `#__%s_%s` AS `obj`
            LEFT JOIN `#__%s_%s_lng` AS `lng_1` ON
                 (`lng_1`.`lang_id` = 1 AND `lng_1`.`record_id` = `obj`.`id`)
            LEFT JOIN `#__%s_%s_lng` AS `lng_3` ON
                 (`lng_3`.`lang_id` = 3 AND `lng_3`.`record_id` = `obj`.`id`)
            WHERE
                `obj`.`public` = 1 AND
                `obj`.`date` > '%s'
            ORDER BY
                `obj`.`date`
        ", $task['mod'], $name, $task['mod'], $name, $task['mod'], $name, $date));
        
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) {
            $id = $info['id'];
            $method = '__render_'.$name;
            if (method_exists($this, $method)) {
                $this->html[$name][$id] = $this->$method($name, $info);
            };
        }
        
    }
    
    
    private function __load_persons($name, $date) {
        
        global $_db, $task;
        
        $list = array();
        $r = $_db->query(sprintf("
            SELECT
                `obj`.*,
                `lng_1`.`lastname` AS `lastname_1`,
                `lng_1`.`firstname` AS `firstname_1`,
                `lng_3`.`lastname` AS `lastname_3`,
                `lng_3`.`firstname` AS `firstname_3`
            FROM
                `#__%s_%s` AS `obj`
            LEFT JOIN `#__%s_%s_lng` AS `lng_1` ON
                 (`lng_1`.`lang_id` = 1 AND `lng_1`.`record_id` = `obj`.`id`)
            LEFT JOIN `#__%s_%s_lng` AS `lng_3` ON
                 (`lng_3`.`lang_id` = 3 AND `lng_3`.`record_id` = `obj`.`id`)
            WHERE
                `obj`.`date` > '%s'
            ORDER BY
                `obj`.`date`
        ", $task['mod'], $name, $task['mod'], $name, $task['mod'], $name, $date));
       
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) {
            $id = $info['id'];
            $method = '__render_'.$name;
            if (method_exists($this, $method)) {
                $this->html[$name][$id] = $this->$method($name, $info);
            };
        }
        
    }
    
    
    private function __load_films($name, $date) {// $name == "films"
        
        global $_db, $task;       
		
// mod_main_films      

		$q = sprintf("
            SELECT
                `obj`.*,
                `lng_1`.`title` AS `title_1`,
                `lng_3`.`title` AS `title_3`
            FROM
                `#__%s_%s` AS `obj`
            LEFT JOIN `#__%s_%s_lng` AS `lng_1` ON
                 (`lng_1`.`lang_id` = 1 AND `lng_1`.`record_id` = `obj`.`id`)
            LEFT JOIN `#__%s_%s_lng` AS `lng_3` ON
                 (`lng_3`.`lang_id` = 3 AND `lng_3`.`record_id` = `obj`.`id`)
            WHERE
                `obj`.`date` > '%s' 
            ORDER BY
                `obj`.`date`
        ", $task['mod'], $name, $task['mod'], $name, $task['mod'], $name, $date);
//	$_db->printR($q);	
        $r = $_db->query($q);
        
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) 
        {
            $id = $info['id'];
            $method = '__render_'.$name;
            if (method_exists($this, $method)) 
                $this->html[$name][$id] = $this->$method($name, $info);
        }        
    }
    
    
    private function __load_serials_articles($name, $date) {
        
        global $_db, $task;
        
        $list = array();
        $q=sprintf("
            SELECT
                `obj`.*,
                `lng_1`.`title` AS `title_1`,
                `lng_3`.`title` AS `title_3`,
                `lng_1`.`intro` AS `intro_1`,
                `lng_3`.`intro` AS `intro_3`
            FROM
                `#__%s_%s` AS `obj`
            LEFT JOIN `#__%s_%s_lng` AS `lng_1` ON
                 (`lng_1`.`lang_id` = 1 AND `lng_1`.`record_id` = `obj`.`id`)
            LEFT JOIN `#__%s_%s_lng` AS `lng_3` ON
                 (`lng_3`.`lang_id` = 3 AND `lng_3`.`record_id` = `obj`.`id`)
            WHERE
                `obj`.`public` = 1 AND
                `obj`.`date` > '%s'
            ORDER BY
                `obj`.`date`
        ", $task['mod'], $name, $task['mod'], $name, $task['mod'], $name, $date);
// $_db->printR($q);       
        $r = $_db->query($q);
   
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) {
            $id = $info['id'];
            $method = '__render_'.$name;
            if (method_exists($this, $method)) {
                $this->html[$name][$id] = $this->$method($name, $info);
            };
        }
        
    }
    
    private function __load_gossip_articles($name, $date) {
        
        global $_db, $task;
        
        $list = array();
        $r = $_db->query(sprintf("
            SELECT
                `obj`.*,
                `lng_1`.`title` AS `title_1`,
                `lng_3`.`title` AS `title_3`,
                `lng_1`.`intro` AS `intro_1`,
                `lng_3`.`intro` AS `intro_3`
            FROM
                `#__%s_%s` AS `obj`
            LEFT JOIN `#__%s_%s_lng` AS `lng_1` ON
                 (`lng_1`.`lang_id` = 1 AND `lng_1`.`record_id` = `obj`.`id`)
            LEFT JOIN `#__%s_%s_lng` AS `lng_3` ON
                 (`lng_3`.`lang_id` = 3 AND `lng_3`.`record_id` = `obj`.`id`)
            WHERE
                `obj`.`public` = 1 AND
                `obj`.`date` > '%s'
            ORDER BY
                `obj`.`date`
        ", $task['mod'], $name, $task['mod'], $name, $task['mod'], $name, $date));
        
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) {
            $id = $info['id'];
            $method = '__render_'.$name;
            if (method_exists($this, $method)) {
                $this->html[$name][$id] = $this->$method($name, $info);
            };
        }
        
    }
    
    
    private function __load_interview_articles($name, $date) {
        
        global $_db, $task;
        
        $list = array();
        $r = $_db->query(sprintf("
            SELECT
                `obj`.*,
                `lng_1`.`title` AS `title_1`,
                `lng_3`.`title` AS `title_3`,
                `lng_1`.`intro` AS `intro_1`,
                `lng_3`.`intro` AS `intro_3`
            FROM
                `#__%s_%s` AS `obj`
            LEFT JOIN `#__%s_%s_lng` AS `lng_1` ON
                 (`lng_1`.`lang_id` = 1 AND `lng_1`.`record_id` = `obj`.`id`)
            LEFT JOIN `#__%s_%s_lng` AS `lng_3` ON
                 (`lng_3`.`lang_id` = 3 AND `lng_3`.`record_id` = `obj`.`id`)
            WHERE
                `obj`.`public` = 1 AND
                `obj`.`date` > '%s'
            ORDER BY
                `obj`.`date`
        ", $task['mod'], $name, $task['mod'], $name, $task['mod'], $name, $date));
        
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) {
            $id = $info['id'];
            $method = '__render_'.$name;
            if (method_exists($this, $method)) {
                $this->html[$name][$id] = $this->$method($name, $info);
            };
        }
        
    }
    
    
    private function __load_articles_articles($name, $date) {
        
        global $_db, $task;
        
        $list = array();
        $r = $_db->query(sprintf("
            SELECT
                `obj`.*,
                `lng_1`.`title` AS `title_1`,
                `lng_3`.`title` AS `title_3`,
                `lng_1`.`intro` AS `intro_1`,
                `lng_3`.`intro` AS `intro_3`
            FROM
                `#__%s_%s` AS `obj`
            LEFT JOIN `#__%s_%s_lng` AS `lng_1` ON
                 (`lng_1`.`lang_id` = 1 AND `lng_1`.`record_id` = `obj`.`id`)
            LEFT JOIN `#__%s_%s_lng` AS `lng_3` ON
                 (`lng_3`.`lang_id` = 3 AND `lng_3`.`record_id` = `obj`.`id`)
            WHERE
                `obj`.`public` = 1 AND
                `obj`.`date` > '%s'
            ORDER BY
                `obj`.`date`
        ", $task['mod'], $name, $task['mod'], $name, $task['mod'], $name, $date)); 
        
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) {
            $id = $info['id'];
            $method = '__render_'.$name;
            if (method_exists($this, $method)) {
                $this->html[$name][$id] = $this->$method($name, $info);
            };
        }
        
    }
    
    
    private function __load_reviews($name, $date) {
        
        global $_db, $task;
        
        $list = array();
		$q = sprintf("
            SELECT
                `obj`.*
            FROM
                `#__%s_%s` AS `obj`
            WHERE
                `obj`.`public` = 1 AND
                `obj`.`date` > '%s'
            ORDER BY
                `obj`.`date`
        ", $task['mod'], $name, $date);
        $r = $_db->query($q);
 //$_db->printR($q);       
       
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) {
            $id = $info['id'];
            $method = '__render_'.$name;
            if (method_exists($this, $method)) {
                $this->html[$name][$id] = $this->$method($name, $info);
            };
        }
        
    }
    
    
    private function __load_news_articles($name, $date) {
        
        global $_db, $task;
        
        $list = array();
        $r = $_db->query(sprintf("
            SELECT
                `obj`.*,
                `lng_1`.`title` AS `title_1`,
                `lng_3`.`title` AS `title_3`,
                `lng_1`.`intro` AS `intro_1`,
                `lng_3`.`intro` AS `intro_3`,
                `heading_1`.`title` AS `heading_1`,
                `heading_3`.`title` AS `heading_3`
            FROM
                `#__%s_%s` AS `obj`
            LEFT JOIN `#__%s_%s_lng` AS `lng_1` ON
                 (`lng_1`.`lang_id` = 1 AND `lng_1`.`record_id` = `obj`.`id`)
            LEFT JOIN `#__%s_%s_lng` AS `lng_3` ON
                 (`lng_3`.`lang_id` = 3 AND `lng_3`.`record_id` = `obj`.`id`)
            LEFT JOIN `#__%s_news_lng` AS `heading_1` ON
                 (`heading_1`.`lang_id` = 1 AND `heading_1`.`record_id` = `obj`.`news_id`)
            LEFT JOIN `#__%s_news_lng` AS `heading_3` ON
                 (`heading_3`.`lang_id` = 3 AND `heading_3`.`record_id` = `obj`.`news_id`)
            WHERE
                `obj`.`public` = 1 AND
                `obj`.`date` > '%s'
            ORDER BY
                `obj`.`date`
        ", $task['mod'], $name, $task['mod'], $name, $task['mod'], $name, $task['mod'], $task['mod'], $date));
        
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) {
            $id = $info['id'];
            $method = '__render_'.$name;
            if (method_exists($this, $method)) {
                $this->html[$name][$id] = $this->$method($name, $info);
            };
        }
        
    }
    
      private function __load_contest_articles($name, $date) {        
        global $_db, $task;
        
        $list = array();
        $r = $_db->query(sprintf("
            SELECT
                `obj`.*,
                `lng_1`.`title` AS `title_1`,
                `lng_3`.`title` AS `title_3`,
                `lng_1`.`intro` AS `intro_1`,
                `lng_3`.`intro` AS `intro_3`
            FROM
                `#__%s_%s` AS `obj`
            LEFT JOIN `#__%s_%s_lng` AS `lng_1` ON
                 (`lng_1`.`lang_id` = 1 AND `lng_1`.`record_id` = `obj`.`id`)
            LEFT JOIN `#__%s_%s_lng` AS `lng_3` ON
                 (`lng_3`.`lang_id` = 3 AND `lng_3`.`record_id` = `obj`.`id`)
            WHERE
                `obj`.`public` = 1 AND
                `obj`.`date` > '%s'
            ORDER BY
                `obj`.`date`
        ", $task['mod'], $name, $task['mod'], $name, $task['mod'], $name, $date)); 
		$num = 0; 
        while ($info = $_db->fetchAssoc($r)) {
            $id = $info['id'];
            $method = '__render_'.$name;
            if (method_exists($this, $method)) {
                $this->html[$name][$id] = $this->$method($name, $info); 
            };
        }
        
    }
  
    private function __render_films_trailers($name, $info) {
        
        global $_cfg, $root;
        
        $value = array();
        
        reset($this->tpl[$name]);
        foreach ($this->tpl[$name] as $key=>$val) {
            $subject = $val['subject'];
            $trailer_name = $info['title_'.$key]
                ? $info['title_'.$key]
                : 'unknown';
            $lng = 'ru';
            if ($key == 3) $lng = 'uk';
            //$image = _ROOT_URL.'public/main/rescrop-bin.php?f=public/main/films/'.$info['image'].'&t=8'; // comment by Mike
            $image = _ROOT_URL.'public/main/rescrop.php?f=public/main/films/'.$info['image'].'&t=8'; // inserted by Mike
            $image = str_replace('test.kino-teatr.ua', 'kino-teatr.ua', $image);
            
            resizecrop($image, $root.'/public/main/films/'.$info['image'], '', 155, 109, 0, 0);
//mail("happyinvestor@mail.ru", "Links to trailers", "$image, $root/public/main/films/".$info['image'].", $root/mods/sys/phpmailer/attachments/".md5($image)); // by Mike
            $image = is_file($root.'/mods/sys/phpmailer/attachments/'.md5($image))
                ? sprintf('<img src="cid:%s" />', md5($image))
                : '';
            
            $trailer_url = main_films::getTrailersUrl($info['id'], $lng);
            $film_url = main_films::getFilmUrl($info['film_id'], $lng);
            $film_name = main_films::getFilmTitleByTrailerID($info['id'], $key);
            $body = $val['text'];
            $body = str_replace('%trailer_name%', $trailer_name, $body);
            $body = str_replace('%image%', $image, $body);
            $body = str_replace('%trailer_url%', $trailer_url, $body);
            $body = str_replace('%film_url%', $film_url, $body);
            $body = str_replace('%film_name%', $film_name, $body);
            $value[$key] = array(
                'subject' => $subject,
                'body' => $body,
                'images' => array(md5($image)=>$image)
            );
        }
        
        return $value;
        
    }
    
    
    private function __render_persons($name, $info) {
        
        global $_db, $_cfg, $task, $root;
        
        $value = array();
        
        reset($this->tpl[$name]);
        foreach ($this->tpl[$name] as $key=>$val) {
            $subject = $val['subject'];
            $lng = 'ru';
            if ($key == 3) $lng = 'uk';
            $r = $_db->query(sprintf("
                SELECT
                    `pp`.`image`
                FROM
                    `#__%s_persons_photos` AS `pp`
                WHERE
                    `pp`.`person_id`=%d AND
                    `pp`.`public`=1
                ORDER BY
                    `pp`.`order_number`
                LIMIT 1
            ",
                $task['mod'],
                $info['id']
            ));
            $photo = $_db->fetchAssoc($r);
// Comment By Mike            
//            $image = $photo
//                ? _ROOT_URL.'public/main/rescrop.php?f=public/main/persons/'.$photo['image'].'&t=5'
//                : '';
            $image = _ROOT_URL.'public/main/rescrop.php?f=public/main/persons/'.$photo['image'].'&t=5'; // inserted by Mike
            $image = str_replace('test.kino-teatr.ua', 'kino-teatr.ua', $image);
            
            resizecrop($image, $root.'/public/main/persons/'.$photo['image'], '', 126, 0, 0, 0);
//mail("happyinvestor@mail.ru", "Links to persons", "$image, $root/public/main/persons/".$photo['image'].", $root/mods/sys/phpmailer/attachments/".md5($image)); // by Mike
            $image = is_file($root.'/mods/sys/phpmailer/attachments/'.md5($image))
                ? sprintf('<img src="cid:%s" />', md5($image))  
                : '';
            
            $name = $info['firstname_'.$key].' '.$info['lastname_'.$key];
            $url = '';
            if ($key == 1) $url = main_persons::getPersonUrl($info['id'], $lng);
            else if ($key == 3) $url = main_persons::getPersonUkUrl($info['id']);
            $body = $val['text'];
            $body = str_replace('%image%', $image, $body);
            $body = str_replace('%url%', $url, $body);
            $body = str_replace('%name%', $name, $body);
            $value[$key] = array(
                'subject' => $subject,
                'body' => $body,
                'images' => array(md5($image)=>$image)
            );
        }
        
        return $value;
        
    }
    
    
    private function __render_films($name, $info) {
        
        global $_db, $_cfg, $task, $root;
        $value = array();
        reset($this->tpl[$name]);
        foreach ($this->tpl[$name] as $key=>$val) {  // every language
            $subject = $val['subject'];
            $lng = 'ru';
            if ($key == 3) $lng = 'uk';
            $r = $_db->query(sprintf("
                SELECT
                    `fp`.`image`
                FROM
                    `#__%s_films_posters` AS `fp`
                WHERE
                    `fp`.`film_id`=%d AND
                    `fp`.`public`=1
                ORDER BY
                    `fp`.`order_number`
                LIMIT 1
            ",
                $task['mod'],
                $info['id']
            ));
            $photo = $_db->fetchAssoc($r);
// Comment By Mike            
//            $image = $photo
//                ? _ROOT_URL.'public/main/rescrop.php?f=public/main/films/'.$photo['image'].'&t=5'
//                : '';
            $image = _ROOT_URL.'public/main/rescrop.php?f=public/main/films/'.$photo['image'].'&t=5'; // inserted by Mike
            $image = str_replace('test.kino-teatr.ua', 'kino-teatr.ua', $image);
            
            resizecrop($image, $root.'/public/main/films/'.$photo['image'], '', 126, 0, 0, 0);
//mail("happyinvestor@mail.ru", "Links to posters", "$image, $root/public/main/films/".$photo['image'].", $root/mods/sys/phpmailer/attachments/".md5($image)); // by Mike
// SELECT image, md5( image ) FROM `grifix_main_films_posters` ORDER BY `id` DESC 
            $image = is_file($root.'/mods/sys/phpmailer/attachments/'.md5($image))
                //? sprintf('<img src="cid:%s" />', md5($image))  // by Mike
                ? sprintf('cid:%s', md5($image))
                : '';

            $name = $info['title_'.$key];
            $url = main_films::getFilmUrl($info['id'], $lng);
            $body = $val['text'];

            $body = str_replace('%image%', $image, $body);
            $body = str_replace('%url%', $url, $body);
            $body = str_replace('%name%', $name, $body);
                
            $value[$key] = array(
                'subject' => $subject,
                'body' => $body,
                'images' => array(md5($image)=>$image)
            );
        }
        
        return $value;
    }
    
    
    private function __render_serials_articles($name, $info) {
    	global $_db, $_cfg, $task, $root;
        
        $value = array();
        reset($this->tpl[$name]);
        foreach ($this->tpl[$name] as $key=>$val) {       	
        	$subject = $val['subject'];
            $lng = 'ru';
            if ($key == 3) $lng = 'uk';
            $name = $info['title_'.$key];
            $url = '';
            if ($key == 1) $url = main_serials::getArticleUrl($info['id']);
            else if ($key == 3) $url = main_serials::getArticleUkUrl($info['id']);

            $image = _ROOT_URL.'public/main/rescrop.php?f=public/main/serials/'.$info['image'].'&t=8'; // inserted by Mike            
            $image = str_replace('test.kino-teatr.ua', 'kino-teatr.ua', $image);
            resizecrop($image, $root.'/public/main/serials/'.$info['image'], '', 155, 109, 0, 0);
            $image = is_file($root.'/mods/sys/phpmailer/attachments/'.md5($image))
                ? sprintf('<img src="cid:%s" />', md5($image))
                : '';
                            
            $body = $val['text'];
            $body = str_replace('%url%', $url, $body);
            $body = str_replace('%name%', $name, $body);
            $body = str_replace('%image%', $image, $body);
            $value[$key] = array(
                'subject' => $subject,
                'body' => $body,
                'images' => array(md5($image)=>$image)
            );            
        }
        return $value;     
    }
    
    
    private function __render_gossip_articles($name, $info) {
        
        global $_db, $_cfg, $task;
        
        $value = array();
        reset($this->tpl[$name]);
        foreach ($this->tpl[$name] as $key=>$val) {
            $subject = $val['subject'];
            $lng = 'ru';
            if ($key == 3) $lng = 'uk';
            $name = $info['title_'.$key];
            $intro = $info['intro_'.$key];
            $url = '';
            if ($key == 1) $url = main_gossip::getArticleUrl($info['id'], $lng);
            else if ($key == 3) $url = main_gossip::getArticleUkUrl($info['id']);
            $body = $val['text'];
            $body = str_replace('%intro%', strip_tags($intro), $body);
            $body = str_replace('%url%', $url, $body);
            $body = str_replace('%name%', $name, $body);
            $value[$key] = array(
                'subject' => $subject,
                'body' => $body,
                'images' => array()
            );
        }
        
        return $value;
        
    }
    
    
    private function __render_interview_articles($name, $info) {
        
        global $_db, $_cfg, $task;
        
        $value = array();
        reset($this->tpl[$name]);
        foreach ($this->tpl[$name] as $key=>$val) {
            $subject = $val['subject'];
            $lng = 'ru';
            if ($key == 3) $lng = 'uk';
            $name = $info['title_'.$key];
            $intro = $info['intro_'.$key];
            $url = '';
            if ($key == 1) $url = main_interview::getArticleUrl($info['id'], $lng);
            else if ($key == 3) $url = main_interview::getArticleUkUrl($info['id']);
            $body = $val['text'];
            $body = str_replace('%intro%', strip_tags($intro), $body);
            $body = str_replace('%url%', $url, $body);
            $body = str_replace('%name%', $name, $body);
            $value[$key] = array(
                'subject' => $subject,
                'body' => $body,
                'images' => array()
            );
        }
        
        return $value;
        
    }
    
    
    private function __render_articles_articles($name, $info) {
        
        global $_db, $_cfg, $task;
        
        $value = array();
        reset($this->tpl[$name]);
        foreach ($this->tpl[$name] as $key=>$val) {
            $subject = $val['subject'];
            $lng = 'ru';
            if ($key == 3) $lng = 'uk';
            $name = $info['title_'.$key];
            $intro = $info['intro_'.$key];
            $url = '';
            if ($key == 1) $url = main_articles::getArticleRuUrl($info['id']);
            else if ($key == 3) $url = main_articles::getArticleUkUrl($info['id']);
            $body = $val['text'];
            $body = str_replace('%intro%', strip_tags($intro), $body);
            $body = str_replace('%url%', $url, $body);
            $body = str_replace('%name%', $name, $body);
            $value[$key] = array(
                'subject' => $subject,
                'body' => $body,
                'images' => array()
            );
        }        
        return $value;        
    }
    
    
    private function __render_reviews($name, $info) {
        
        global $_db, $_cfg, $task;
        
        $value = array();
        reset($this->tpl[$name]);
        foreach ($this->tpl[$name] as $key=>$val) {
            $subject = $val['subject'];
            $lng = 'ru';
            if ($key == 3) $lng = 'uk';
            $name = $info['title'];
            $intro = $info['intro'];
            $url = '';
            if ($key == 1) $url = main_reviews::getReviewUrl($info['id'], $lng);
            else if ($key == 3) $url = main_reviews::getReviewUkUrl($info['id']);
            $film_url = main_films::getFilmUrl($info['film_id'], $lng);
            $film_name = main_films::getFilmByReviewID($info['id'], $key);
            $body = $val['text'];
            $body = str_replace('%intro%', strip_tags($intro), $body);
            $body = str_replace('%url%', $url, $body);
            $body = str_replace('%name%', $name, $body);
            $body = str_replace('%film_name%', $film_name, $body);
            $body = str_replace('%film_url%', $film_url, $body);
            $value[$key] = array(
                'subject' => $subject,
                'body' => $body,
                'images' => array()
            );
        }
        
        return $value;
        
    }
    
    
    private function __render_news_articles($name, $info) {
        
        global $_db, $_cfg, $task;
        
        $value = array();
        reset($this->tpl[$name]);
        foreach ($this->tpl[$name] as $key=>$val) {
            $subject = $val['subject'];
            $lng = 'ru';
            if ($key == 3) $lng = 'uk';
            $name = $info['title_'.$key];
            $intro = $info['intro_'.$key];
            $heading = $info['heading_'.$key];
            $body = $val['text'];
            $url = '';
            if ($key == 1) $url = main_news::getArticleRuUrl($info['id']);
            else if ($key == 3) $url = main_news::getArticleUkUrl($info['id']);
            $body = str_replace('%intro%', strip_tags($intro), $body);
            $body = str_replace('%heading%', $heading, $body);
            $body = str_replace('%url%', $url, $body);
            $body = str_replace('%name%', $name, $body);
            $value[$key] = array(
                'subject' => $subject,
                'body' => $body,
                'images' => array()
            );
        }
        
        return $value;
        
    }
    
    private function __render_contest_articles($name, $info) {
        
        global $_db, $_cfg, $task;
        $value = array();
        reset($this->tpl[$name]);
        foreach ($this->tpl[$name] as $key=>$val) {
            $subject = $val['subject'];
            $lng = 'ru';
            if ($key == 3) $lng = 'uk';
            $name = $info['title_'.$key];
            $intro = $info['intro_'.$key];
            $url = '';
            if ($key == 1) $url = main_contest::getArticleRuUrl($info['id']);
            else if ($key == 3) $url = main_contest::getArticleUkUrl($info['id']);
            $body = $val['text'];
            $body = str_replace('%intro%', strip_tags($intro), $body);
            $body = str_replace('%url%', $url, $body);
            $body = str_replace('%name%', $name, $body);
            $value[$key] = array(
                'subject' => $subject,
                'body' => $body,
                'images' => array()
            );
        }     
        return $value;        
    }
    
    public function get_films_trailers($name, $date, $lang_id) {
        
        global $_cfg, $task, $_db;
        
        $return = false;
        $datetime = new DateTime();
        $datetime->setTimestamp($date);
        
        $r = $_db->query(sprintf("
            SELECT
                `obj`.`id`
            FROM
                `#__%s_%s` AS `obj`
            WHERE
                `obj`.`public` = 1 AND
                `obj`.`date` > '%s'
        ",
            $task['mod'],
            $name,
            $datetime->format('Y-m-d H:i:s')
        ));
        $subject = '';
        $value = '';
        $images = array();
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) {
            $val = $this->html[$name][$info['id']][$lang_id];
            if(!$val) continue;
            $subject = $val['subject'];
            $clear = $num%4 == 4
                ? '<div style="clear:both;"></div>'
                : '';
            $value .= $val['body'].$clear."\n";
            $images = array_merge($images, $val['images']);
            $num++;
        }
        if (trim($value)) {
            $return = array(
                'subject' => $subject,
                'body' => $value,
                'images' => $images
            );
        }
        
        return $return;
        
    }
    
    
    public function get_persons($name, $date, $lang_id) {
        
        global $_cfg, $task, $_db;
        
        $return = false;
        $datetime = new DateTime();
        $datetime->setTimestamp($date);
        
        $r = $_db->query(sprintf("
            SELECT
                `obj`.`id`
            FROM
                `#__%s_%s` AS `obj`
            WHERE
                `obj`.`date` > '%s'
        ",
            $task['mod'],
            $name,
            $datetime->format('Y-m-d H:i:s')
        ));
        $subject = '';
        $value = '';
        $images = array();
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) {
            $val = $this->html[$name][$info['id']][$lang_id];
            if(!$val) continue;
            $subject = $val['subject'];
            $clear = $num%4 == 3
                ? '<div style="clear:both;"></div>'
                : '';
            $value .= $val['body'].$clear."\n";
            $images = array_merge($images, $val['images']);
            $num++;
        }
        if (trim($value)) {
            $return = array(
                'subject' => $subject,
                'body' => $value,
                'images' => $images
            );
        }
        
        return $return;
        
    }
    
    
    public function get_films($name, $date, $lang_id) {
        
        global $_cfg, $task, $_db;
        
        $return = false;
        $datetime = new DateTime();
        $datetime->setTimestamp($date);
        
        $r = $_db->query(sprintf("
            SELECT
                `obj`.`id`
            FROM
                `#__%s_%s` AS `obj`
            WHERE
                `obj`.`date` > '%s'
        ",
            $task['mod'],
            $name,
            $datetime->format('Y-m-d H:i:s')
        ));
        $subject = '';
        $value = '';
        $images = array();
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) {
            $val = $this->html[$name][$info['id']][$lang_id];
            if(!$val) continue;
            $subject = $val['subject'];
            $clear = $num%4 == 3
                ? '</td></tr><tr><td>'
                : '';
            $value .= $val['body'].$clear."\n";
            $images = array_merge($images, $val['images']);
            $num++;
        }
        if (trim($value)) {
            $return = array(
                'subject' => $subject,
                'body' => $value,
                'images' => $images
            );
        }
        
        return $return;
        
    }
    
    
    public function get_serials_articles($name, $date, $lang_id) {
        
        global $_cfg, $task, $_db;
        
        $return = false;
        $datetime = new DateTime();
        $datetime->setTimestamp($date);
        
        $q = sprintf("
            SELECT
                `obj`.`id`
            FROM
                `#__%s_%s` AS `obj`
            WHERE
                `obj`.`public` = 1 
                AND `obj`.`date` > '%s'
        ",
            $task['mod'],
            $name,
            $datetime->format('Y-m-d H:i:s')
        );
        $r = $_db->query($q);
// $_db->printR($q);       
        $subject = '';
        $value = '';
        $images = array();
        $num = 0;
        while ($info = $_db->fetchAssoc($r)) {
            $val = $this->html[$name][$info['id']][$lang_id];
            if(!$val) continue;
            $subject = $val['subject'];
            $clear = $num%4 == 4
                ? '<div style="clear:both;"></div>'
                : '';
            $value .= $val['body'].$clear."\n";
            $images = array_merge($images, $val['images']);
            $num++;
        }
        if (trim($value)) {
            $return = array(
                'subject' => $subject,
                'body' => $value,
                'images' => $images
            );
        }
        
        return $return;
    }
    
    public function get_gossip_articles($name, $date, $lang_id) {
        
        global $_cfg, $task, $_db;
        
        $return = false;
        $datetime = new DateTime();
        $datetime->setTimestamp($date);
        
        $r = $_db->query(sprintf("
            SELECT
                `obj`.`id`
            FROM
                `#__%s_%s` AS `obj`
            WHERE
                `obj`.`date` > '%s'
        ",
            $task['mod'],
            $name,
            $datetime->format('Y-m-d H:i:s')
        ));
        $subject = '';
        $value = '';
        $images = array();
        while ($info = $_db->fetchAssoc($r)) {
            $val = $this->html[$name][$info['id']][$lang_id];
            if(!$val) continue;
            $subject = $val['subject'];
            $value .= $val['body']."\n";
            $images = array_merge($images, $val['images']);
        }
        if (trim($value)) {
            $return = array(
                'subject' => $subject,
                'body' => $value,
                'images' => $images
            );
        }
        
        return $return;
        
    }
    
    
    public function get_interview_articles($name, $date, $lang_id) {
        
        global $_cfg, $task, $_db;
        
        $return = false;
        $datetime = new DateTime();
        $datetime->setTimestamp($date);
        
        $r = $_db->query(sprintf("
            SELECT
                `obj`.`id`
            FROM
                `#__%s_%s` AS `obj`
            WHERE
                `obj`.`date` > '%s'
        ",
            $task['mod'],
            $name,
            $datetime->format('Y-m-d H:i:s')
        ));
        $subject = '';
        $value = '';
        $images = array();
        while ($info = $_db->fetchAssoc($r)) {
            $val = $this->html[$name][$info['id']][$lang_id];
            if(!$val) continue;
            $subject = $val['subject'];
            $value .= $val['body']."\n";
            $images = array_merge($images, $val['images']);
        }
        if (trim($value)) {
            $return = array(
                'subject' => $subject,
                'body' => $value,
                'images' => $images
            );
        }
        
        return $return;
        
    }
    
    
    public function get_articles_articles($name, $date, $lang_id) {
        
        global $_cfg, $task, $_db;
        
        $return = false;
        $datetime = new DateTime();
        $datetime->setTimestamp($date);
        
        $r = $_db->query(sprintf("
            SELECT
                `obj`.`id`
            FROM
                `#__%s_%s` AS `obj`
            WHERE
                `obj`.`date` > '%s'
        ",
            $task['mod'],
            $name,
            $datetime->format('Y-m-d H:i:s')
        ));
        $subject = '';
        $value = '';
        $images = array();
        while ($info = $_db->fetchAssoc($r)) {
            $val = $this->html[$name][$info['id']][$lang_id];
            if(!$val) continue;
            $subject = $val['subject'];
            $value .= $val['body']."\n";
            $images = array_merge($images, $val['images']);
        }
        if (trim($value)) {
            $return = array(
                'subject' => $subject,
                'body' => $value,
                'images' => $images
            );
        }
        
        return $return;
        
    }
    
    public function get_contest_articles($name, $date, $lang_id) {
        
        global $_cfg, $task, $_db;
        
        $return = false;
        $datetime = new DateTime();
        $datetime->setTimestamp($date);
        
        $r = $_db->query(sprintf("
            SELECT
                `obj`.`id`
            FROM
                `#__%s_%s` AS `obj`
            WHERE
                `obj`.`date` > '%s'
        ",
            $task['mod'],
            $name,
            $datetime->format('Y-m-d H:i:s')
        ));
        $subject = '';
        $value = '';
        $images = array();
        while ($info = $_db->fetchAssoc($r)) {
            $val = $this->html[$name][$info['id']][$lang_id];
            if(!$val) continue;
            $subject = $val['subject'];
            $value .= $val['body']."\n";
            $images = array_merge($images, $val['images']);
        }
        if (trim($value)) {
            $return = array(
                'subject' => $subject,
                'body' => $value,
                'images' => $images
            );
        }
        
        return $return;
        
    }
    
    
    public function get_reviews($name, $date, $lang_id) {
        
        global $_cfg, $task, $_db;
        
        $return = false;
        $datetime = new DateTime();
        $datetime->setTimestamp($date);
        
		$q = sprintf("
            SELECT
                `obj`.`id`
            FROM
                `#__%s_%s` AS `obj`
            WHERE
                `obj`.`date` > '%s'
        ",
            $task['mod'],
            $name,
            $datetime->format('Y-m-d H:i:s')
        );
        $r = $_db->query($q);
 //$_db->printR($q);       
// var_dump($this);
        $subject = '';
        $value = '';
        $images = array();
        while ($info = $_db->fetchAssoc($r)) {
            $val = $this->html[$name][$info['id']][$lang_id];
            if(!$val) continue;
            $subject = $val['subject'];
            $value .= $val['body']."\n";
        }
        
        if (trim($value)) {
            $return = array(
                'subject' => $subject, // Это отсутствует в заголовке!!??????
                'body' => $value,
                'images' => $images
            );
        }
        return $return;
        
    }
    
    
    public function get_news_articles($name, $date, $lang_id) {
        
        global $_cfg, $task, $_db;
        
        $return = false;
        $datetime = new DateTime();
        $datetime->setTimestamp($date);
        
        $r = $_db->query(sprintf("
            SELECT
                `obj`.`id`
            FROM
                `#__%s_%s` AS `obj`
            WHERE
                `obj`.`date` > '%s'
        ",
            $task['mod'],
            $name,
            $datetime->format('Y-m-d H:i:s')
        ));
        $subject = '';
        $value = '';
        $images = array();
        while ($info = $_db->fetchAssoc($r)) {
            $val = $this->html[$name][$info['id']][$lang_id];
            if(!$val) continue;
            $subject = $val['subject'];
            $value .= $val['body']."\n";
            $images = array_merge($images, $val['images']);
        }
        if (trim($value)) {
            $return = array(
                'subject' => $subject,
                'body' => $value,
                'images' => $images
            );
        }
        
        return $return;
        
    }
    
    
}

function resizecrop($url, $file_input, $file_output, $w_o, $h_o, $percent = false,$realsize) {
	global $root;

	list($w_i, $h_i, $type) = getimagesize($file_input);
//mail("happyinvestor@mail.ru","resizecrop", "url=$url,\n file_input=$file_input,\n file_output=$file_output,\n w_o=$w_o,\n h_o=$h_o,\n percent=$percent,\n realsize=$realsize,\n w_i=$w_i,\n h_i=$h_i,\n type=$type");
	if ($realsize)
	{
		if ($w_i<$w_o)
		{
			$w_o = $w_i;
			$h_o = $h_i;
		}
	}

	if ($w_i<$h_i || $h_o == 0)
	{
		$h_n = round($w_o/($w_i/$h_i));
		$w_n = $w_o;
	} else if ($w_i>$h_i){
		$w_n = round($h_o/($h_i/$w_i));
		$h_n = $h_o;

		if($w_n < $w_o)
		{
			$h_n = round($w_o/($w_i/$h_i));
			$w_n = $w_o;
		}

	} else if ($w_i==$h_i) {
		$w_n = $w_o;
		$h_n = $w_o;
	}
        $types = array('','gif','jpeg','png');
        $ext = $types[$type];
        if ($ext) {
    	        $func = 'imagecreatefrom'.$ext;
    	        $img = $func($file_input);
        } else {
    	        echo 'Некорректный формат файла (bad file format)';
		return;
        }

	$img_o = imagecreatetruecolor($w_n, $h_n);
	imagecopyresampled($img_o, $img, 0, 0, 0, 0, $w_n, $h_n, $w_i, $h_i);

	if ($h_o!=0)
	{
		if ($h_n>$h_o)
		{
			$y_o = round(($h_n-$h_o)*0.25);
			$x_o = 0;
		}

		if ($w_n>$w_o)
		{
			$x_o = round(($w_n-$w_o)/2);
			$y_o = 0;
		}

		if ($w_n==$h_n)
		{
			$x_o = 0;
			$y_o = 0;
		}
	}  else {
		$w_o = $w_n;
		$h_o = $h_n;
	}

//	print_R ($x_o.'|'.$y_o.'|'.$w_o.'|'.$h_o.'|'.$w_n.'|'.$h_n);
//	exit;

	$img_n = imagecreatetruecolor($w_o, $h_o);
	imagecopy($img_n, $img_o, 0, 0, $x_o, $y_o, $w_o, $h_o);

    $fname = md5($url);
    $fpath = $root.'/mods/sys/phpmailer/attachments/'.$fname;
	if ($type == 2) {
		imagejpeg($img_n,$fpath,100);
	} else {
		$func = 'image'.$ext;
		$func($img_n,$fpath);
	}   
}
?>