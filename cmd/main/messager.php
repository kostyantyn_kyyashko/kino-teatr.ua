<?
require_once('../init.php');

if(!$task['id'])
	die('error');

sys::useLib('main::cinemas');
sys::useLib('main::news');
sys::useLib('main::persons');
sys::useLib('main::films');
sys::useLib('main::reviews');
sys::useLib('main::contest');
sys::useLib('main::interview');
sys::useLib('main::articles');
sys::useLib('main::gossip');

$_db->query("LOCK TABLES `#__main_discuss_messages` AS `msg` WRITE, `#__sys_mods_tasks` WRITE, `#__sys_users` AS `usr` WRITE");

$r=$_db->query("
	SELECT
		msg.object_id,
		msg.object_type, 
		msg.date,
		msg.text,
		usr.login AS `user`
	FROM `#__main_discuss_messages`	AS `msg`
	LEFT JOIN `#__sys_users` AS `usr`
	ON usr.id=msg.user_id
	WHERE msg.date>'".$task['last_run']."' 
	ORDER BY msg.date
");
$_db->setValue('sys_mods_tasks','last_run',gmdate('Y-m-d H:i:s'),intval($task['id']));
$_db->query("UNLOCK TABLES");

sys::loadEnvVars(7);
sys::useLang('main');

$email['text']=false;
while ($msg=$_db->fetchAssoc($r))
{
	switch ($msg['object_type'])
	{
		case 'news_article':
            $url=main_news::getArticleDiscussUrl($msg['object_id']);
		break;
		
		case 'film':
			$url=main_films::getFilmDiscussUrl($msg['object_id']);
		break;
		
		case 'person';
			$url=main_persons::getPersonDiscussUrl($msg['object_id']);
		break;
		
		case 'review';
			$url=main_reviews::getReviewDiscussUrl($msg['object_id']);
		break;
		
		case 'cinema';
			$url=main_cinemas::getCinemaDiscussUrl($msg['object_id']);
		break;
        
		case 'trailer':
//			$url=main_films::getTrailersUrl($msg['object_id']);
			$url=main_films::getTrailerUrl_new($msg['object_id'], "ru", "film-trailers");
		break;

		case 'articles_article':
			$url=main_articles::getArticleDiscussUrl($msg['object_id']);
		break;

		case 'interview_article':
			$url=main_interview::getArticleDiscussUrl($msg['object_id']);
		break;
		
		case 'gossip_article':
			$url=main_gossip::getArticleDiscussUrl($msg['object_id']);
		break;

		case 'contest_article':
			$url=main_contest::getContestArticleDiscussUrl($msg['object_id']);
		break;
		
	}
	ob_start()
	?>
		<div style="margin:0 0 15px 0; font-family:Arial, sans-serif; border:1px solid #e31e24; background:#fff; color:#000; font-size:12px; border-top:0; padding:10px 20px;">
			<div style="font-weight:bold; padding-bottom:5px; border-bottom:1px solid #e2e2e2; margin-bottom:10px;"> <?=$msg['user']?> | <?=sys::db2Date($msg['date'],true)?> | <a href="<?=$url?>" style="color:#e31e24;"><?=sys::translate('main::link')?></a></div>
			<div style=""> <?=$msg['text']?></div>
		</div>
	<?
	$email['text'].=ob_get_clean();
}
if($email['text'])
	sys::addMailTpl(7,'main::messager',$email);

?>