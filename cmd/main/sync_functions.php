<?php

function get_cinemas(){
	$sql = "SELECT mc.id, mc.name AS name, mcl.title AS name_ukr FROM grifix_main_cinemas mc LEFT JOIN grifix_main_cinemas_lng mcl ON (mc.id = mcl.record_id AND mcl.lang_id = 3)";
	$res = mysql_query($sql);
	$arr = array();
	while($row = mysql_fetch_assoc($res)){
		$arr[$row['id']] = $row;
	}
	return $arr;
}

function get_cinemas_halls(){
	$sql = "SELECT mch.id AS hall_id, mch.cinema_id, mch.name, mchl.title AS name_ukr FROM grifix_main_cinemas_halls mch LEFT JOIN grifix_main_cinemas_halls_lng mchl ON (mch.id = mchl.record_id AND mchl.lang_id = 3)";
	$res = mysql_query($sql);
	$arr = array();
	while($row = mysql_fetch_assoc($res)){
		$arr[$row['hall_id']] = $row;
	}
	return $arr;
}

function get_cinema_hall_by_name($params,$halls){
	if(strpos($params['hallName'],'.'))
		list($cinema,$hall_name) = explode('.',$params['hallName']);
	else
		$hall_name = $params['hallName'];
	$hall_name = trim($hall_name);
	$hall_id = 0;
	$cinema_id = $params['cinema_id'];
	if(!$hall_name)
		return $hall_id;
//	echo 'cinema_id '.$cinema_id.' hall_name '.$hall_name.'<br>';
	foreach($halls as $key=>$value){
		
			if($value['cinema_id'] == $cinema_id){
//				echo 'name '.$value['name'].' name_ukr '.$value['name_ukr'].'<br>';

				if(mb_strtolower($value['name']) == mb_strtolower($hall_name) || mb_strtolower($value['name_ukr']) == mb_strtolower($hall_name)){
					$hall_id = $key;
					break;
				}
				if(replace_hall_names($hall_name,$value['name_ukr'])){
					$hall_id = $key;
					break;
				}
			}
		
	}
	return $hall_id;
}

function megakino_connect(){
	$author_url = 'http://server.megakino.com.ua/gate/login';
	$author_params = 'username=kino-teatr.ua&password=vdfv78464rwcs5g34';
	if( $curl = curl_init() ) {
    	curl_setopt($curl, CURLOPT_URL, $author_url);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    	curl_setopt($curl, CURLOPT_POST, true);
   	 	curl_setopt($curl, CURLOPT_POSTFIELDS, $author_params);
    	$out = curl_exec($curl);
		curl_close($curl);
		$session_id = trim($out);
		return $session_id;
	}
	else
		return '';
}

function replace_hall_names($hall_name,$enter_name){
	$hall_name = mb_strtolower($hall_name);
	$enter_name = mb_strtolower($enter_name);
	$hall_name = str_ireplace('синий зал','синя зала',$hall_name);
	$hall_name = str_ireplace('червоний зал','червона зала',$hall_name);
	$hall_name = str_ireplace('зелений зал','зелена зала',$hall_name);
	$hall_name = str_ireplace('малий зал','мала зала',$hall_name);
	if($hall_name == $enter_name)
		return true;
	return false;
}

function get_cinema_by_name($params,$cinemas){
	$name = str_replace('Кінотеатр','',$params['name']);
	$name = str_replace('Кинотеатр','',$name);
	if(strpos($name,'(')){
		list($name,$name2) = explode('(',$name);
	}
	$name = trim($name);
	$cinema_id = 0;
	foreach($cinemas as $key=>$value){
		if($value['name'] == $name || $value['name_ukr'] == $name){
			$cinema_id = $key;
			break;
		}
	}
	return $cinema_id;
}

function get_films(){
	$sql = "SELECT gmf.title_orig, gmf.name AS title, gmf.year, gmf.id, gmfl.title AS title_ukr, gmfr.title AS title_rus FROM grifix_main_films gmf LEFT JOIN grifix_main_films_lng gmfl ON (gmf.id = gmfl.record_id AND gmfl.lang_id = 3) LEFT JOIN grifix_main_films_lng gmfr ON (gmf.id = gmfr.record_id AND gmfr.lang_id = 1)";
	$res = mysql_query($sql);
	$arr = array();
	while($row = mysql_fetch_assoc($res)){
		$arr[$row['id']] = $row;
	}
	return $arr;
}

function get_film_by_name($params,$films){
	$orig_name = $params['originName'];
	$name = $params['name'];
	$premiere = $params['premiere'];
	list($year,$month,$day) = explode('-',$premiere);
	$film_id = 0;
//	echo 'origin_name '.$orig_name.'<br>';
//	echo 'name '.$name.'<br>';
	foreach($films as $key=>$value){
		if($value['title_orig'] == $orig_name || $value['title_ukr'] == $name || $value['title_rus'] == $name || $value['title'] == $orig_name || $value['title'] == $name){
			if($value['year'] != $year){
				
			}
			else{
				$film_id = $key;
				break;
			}
			
		}
	}
	return $film_id;
}

function check_url($response){
	echo 'response '.$response.'<br>';
	if(!$response || strpos($response,'ERROR 403')){
		echo 'Denied !! <br>';
		return false;
	}
	return true;
}

  function set_events($session_id){
	if(!$session_id)
		return false;
	$sites_url = 'http://server.megakino.com.ua/gate/sites';
	mysql_query("TRUNCATE temp_auto_films_cmd");
	$sites_url .= '?sessionid='.$session_id;
	$films_url = 'http://server.megakino.com.ua/gate/shows';
	$buffer = file_get_contents($sites_url);
	$response = json_decode($buffer,true);
	if(!is_array($response))
		return false;
	
	foreach($response as $item){
		$site_id = $item['siteId'];
		$site_name = $item['name'];
		$site_index = $item['index'];
		$site_address = $item['address'];
		$filmes_buffer = file_get_contents($films_url.'?sessionid='.$session_id.'&siteId='.$site_id);
//		echo $films_url.'?sessionid='.$session_id.'&siteId='.$site_id.'<br>';
		$response_films = json_decode($filmes_buffer,true);
		foreach($response_films as $subitem){
//			var_dump($subitem);
			$id_film_export = $subitem['showId'];
			$date_premiere = $subitem['premiereLocal'];

			foreach($subitem['events'] as $event){
				$technology = $event['technology'];
				$hall_id = $event['hallId'];
				$event_id = $event['eventId'];
//				$map_url = 'http://server.megakino.com.ua/gate/eventmap?sessionid='.$session_id.'&amp;eventId='.$event_id.'&amp;siteId='.$site_id;
				$map_url = 'http://server.megakino.com.ua/gate/eventmap?sessionid='.$session_id.'&eventId='.$event_id.'&siteId='.$site_id;
				$map_response = file_get_contents($map_url);
				if(!check_url($map_response)){
					$buffer_session_id = megakino_connect();
					if($buffer_session_id){
						$session_id = $buffer_session_id;
						$map_url = 'http://server.megakino.com.ua/gate/eventmap?sessionid='.$session_id.'&eventId='.$event_id.'&siteId='.$site_id;
						$map_response = file_get_contents($map_url);
					}
						
				}
				$map_response = mysql_real_escape_string($map_response);
//				var_dump($map_response);
//				if($map_response && isset($buffer_map['code']) && $buffer_map['code'] == 0){
								
//					$map_response = mysql_real_escape_string($map_response);
//					$sql = "INSERT INTO `grifix_main_shows_eventmap`(`show_id`, `event_id`, `site_id`, `response`) VALUES ($show_id,$event_id,'$site_id','$map_response')";
//					mysql_query($sql);
//				}
//				var_dump($buffer);
//				echo 'origin '.$event['origin'].'<br>';
//				echo mb_substr($event['origin'],8,4).'<br>';
				$shows_time = mb_substr($event['origin'],8,4);
				$shows_time = mb_substr($shows_time,0,2).':'.mb_substr($shows_time,2,2).':00';
				$year = mb_substr($event['origin'],0,4);
				$month = mb_substr($event['origin'],4,2);
				$day = mb_substr($event['origin'],6,2);
				$date = "$year-$month-$day";
				$shows_min_price = $event['minPrice'];
				$shows_max_price = $event['maxPrice'];
				$prices = $shows_min_price.', '.$shows_max_price;
//				$date = date('Y-m-d H:i:s');
//				$sql = "INSERT INTO `temp_auto_films_cmd`(`id`, `film_id`, `site_id`, `site_index`, `event_id`, `hall_id`, `date`, `time`, `prices`, `technology`, `eventmap`) VALUES (null,$id_film_export,'$site_id',$site_index,$event_id,$hall_id,'$date_premiere','$shows_time','$prices','$technology', '$map_response')";
				$sql = "INSERT INTO `temp_auto_films_cmd`(`id`, `film_id`, `site_id`, `site_index`, `event_id`, `hall_id`, `date`, `time`, `prices`, `technology`, `eventmap`) VALUES (null,$id_film_export,'$site_id',$site_index,$event_id,$hall_id,'$date','$shows_time','$prices','$technology', '$map_response')";
//				echo $sql.'<br><br>';
    			$result = mysql_query($sql);
    			
				
			}

		}
	}
}

function get_eventmap($session_id,$site_id,$event_id){
//	$map_url = 'http://server.megakino.com.ua/gate/eventmap?sessionid='.$session_id.'&amp;siteId='.$site_id.'&amp;eventId='.$event_id;
	$site_id = trim($site_id);
	$event_id = trim($event_id);
	$map_url = 'http://server.megakino.com.ua/gate/eventmap?sessionid='.$session_id.'&eventId='.$event_id.'&siteId='.$site_id;
	echo 'map_url '.$map_url.'<br>';
	$buffer = file_get_contents($map_url);
	return $buffer;
//	$response = json_decode($buffer,true);
}


function set_shows(){
	mysql_query("TRUNCATE `grifix_main_shows2`");
	$sql = "SELECT id_film_export,id_film FROM grifix_main_auto_films WHERE id_export = 8 AND id_film != 0";
	$res = mysql_query($sql);
	if(mysql_num_rows($res)){
		while($arr = mysql_fetch_assoc($res)):
		$id_film_export = $arr['id_film_export'];
		$film_id = (int)$arr['id_film'];
		if(!$film_id)
			continue;
		$sql = "SELECT site_id, site_index, event_id, hall_id, date, time, prices, technology, eventmap FROM temp_auto_films_cmd WHERE film_id = $id_film_export";
		
		$res_shows = mysql_query($sql);
		if($res_shows && mysql_num_rows($res_shows)){
			while($arr = mysql_fetch_assoc($res_shows)){
				$site_id = trim($arr['site_id']);
				$site_index = $arr['site_index'];
				$event_id = trim($arr['event_id']);
				$hall_id = $arr['hall_id'];
				$date = $arr['date'];
				$time = $arr['time'];
				$prices = $arr['prices'];
				$technology = $arr['technology'];
				$sql_temp = "SELECT id_hall FROM grifix_main_auto_cinema WHERE id_hall_export = $hall_id AND id_cinema_export = '$site_id' AND id_export = 8 LIMIT 1";
//				echo $sql_temp.'<br>';
				$res_temp = mysql_query($sql_temp);
				$hall_id_self = (int)mysql_result($res_temp,0,'id_hall');
				$sql = "INSERT INTO `grifix_main_shows2`(`id`, `hall_id`, `film_id`, `partner_id`, `begin`, `end`,`event_id`) VALUES (null,$hall_id_self,$film_id,8,'$date','$date',$event_id)";
				mysql_query($sql);
				if($hall_id_self){
//					$sql_count = "SELECT COUNT(*) AS num FROM grifix_main_shows WHERE begin = '$date' AND hall_id = $hall_id AND film_id = $film_id";
//					$sql_count = "SELECT COUNT(*) AS num FROM grifix_main_shows WHERE hall_id = $hall_id_self AND film_id = $film_id AND partner_id = 8 AND begin = '$date' AND end = '$date'";
					$sql_count = "SELECT COUNT(*) AS num FROM grifix_main_shows WHERE event_id = $event_id";
					echo $sql_count.'<br>';
					$res_count = mysql_query($sql_count);
					if($res_count && mysql_result($res_count,0,'num')){
						
					}
					else{
						$sql = "INSERT INTO `grifix_main_shows`(`id`, `hall_id`, `film_id`, `partner_id`, `begin`, `end`, `event_id`) VALUES (null,$hall_id_self,$film_id,8,'$date','$date',$event_id)";
						mysql_query($sql);
						$show_id = mysql_insert_id();
						if($show_id){
							if($technology == '3D' || $technology == '3d')
								$_3d = 1;
							else
								$_3d = 0;
							$sql = "INSERT INTO `grifix_main_shows_times`(`id`, `show_id`, `time`, `prices`, `3D`, `may3D`, `sale_id`, `sale_status`) VALUES (null,$show_id,'$time','$prices',$_3d,$_3d,0,'')";
							mysql_query($sql);
							$eventmap = trim($arr['eventmap']);
							if($eventmap){
								$eventmap = mysql_real_escape_string($eventmap);
								$sql = "INSERT INTO `grifix_main_shows_eventmap`(`show_id`, `event_id`, `site_id`, `response`) VALUES ($show_id,$event_id,'$site_id','$eventmap')";
								echo $sql.'<br>';
								mysql_query($sql);
							}
							
							
							
						}
					}
					
				}
			}
		}
		endwhile;
	}
	else{
		return false;
	}
}


function get_show($film_id,$session_id){
	$sql = "SELECT id_film_export FROM grifix_main_auto_films WHERE id_film = $film_id";
	$res = mysql_query($sql);
	if(mysql_num_rows($res)){
		$id_film_export = mysql_result($res,0,'id_film_export');
		$sql = "SELECT site_id, event_id, hall_id, date, time, prices FROM temp_auto_films_cmd WHERE film_id = $id_film_export";
//		echo $sql.'<br>';
		$res_shows = mysql_query($sql);
		if(mysql_num_rows($res_shows)){
			while($arr = mysql_fetch_assoc($res_shows)){
				$site_id = $arr['site_id'];
				$event_id = $arr['event_id'];
				$hall_id = $arr['hall_id'];
				$date = $arr['date'];
				$time = $arr['time'];
				$prices = $arr['prices'];
				$res_temp = mysql_query("SELECT id_hall FROM grifix_main_auto_cinema WHERE id_hall_export = $hall_id AND id_cinema_export = '$site_id' LIMIT 1");
				$hall_id_self = mysql_result($res_temp,0,'id_hall');
				if($hall_id_self){
					$sql = "INSERT INTO `grifix_main_shows`(`id`, `hall_id`, `film_id`, `partner_id`, `begin`, `end`) VALUES (null,$hall_id_self,$film_id,8,'$date','$date')";
//					echo $sql.'<br>';
					mysql_query($sql);
					$show_id = mysql_insert_id();
					if($show_id){
						$sql = "INSERT INTO `grifix_main_shows_times`(`id`, `show_id`, `time`, `prices`, `3D`, `may3D`, `sale_id`, `sale_status`) VALUES (null,$show_id,'$time','$prices',0,0,0,'')";
//						echo $sql.'<br>';
						mysql_query($sql);
						$record_id = mysql_insert_id();
						if($record_id){
							mysql_query("INSERT INTO `grifix_main_shows_times_lng`(`record_id`, `lang_id`, `note`) VALUES ($record_id,1,'')");
							mysql_query("INSERT INTO `grifix_main_shows_times_lng`(`record_id`, `lang_id`, `note`) VALUES ($record_id,1,'')");
						}
					}
				}
			}
		}
		
	}
	else{
		return false;
	}
}

?>