<?php
require_once('../init.php');

$ch = curl_init("http://kino-teatr:Qxsoh~DI@api.vkino.ua/repertoire");
$fp = fopen(_ROOT_DIR."/export_m/vkino.xml", "w");
curl_setopt($ch, CURLOPT_FILE, $fp);
curl_exec($ch);
curl_close($ch);
fclose($fp);

$xml_imax=array( 135=>'https://planetakino.ua/showtimes/xml/',//ТРЦ «Блокбастер» IMAX
				 296=>'https://planetakino.ua/odessa2/showtimes/xml/',//ТРЦ «City Center»
				 297=>'https://planetakino.ua/odessa/showtimes/xml/',//ТРЦ «City Center Котовський»
				 207=>'https://planetakino.ua/lvov/showtimes/xml/',//Львов ТРЦ «King Cross Leopolis»
				 262=>'https://planetakino.ua/kharkov/showtimes/xml/',//ТРЦ «Французький Бульвар»
				 279=>'https://planetakino.ua/yalta/showtimes/xml/',//Ялта
				 285=>'https://planetakino.ua/sumy/showtimes/xml/',//ТРЦ «Мануфактура»
				 315=>'https://planetakino.ua/lvov2/showtimes/xml/'//Львов ТРЦ «Форум»
);

foreach ($xml_imax as $key => $url_xml)
{
	$ch = curl_init($url_xml);
	$fp = fopen(_ROOT_DIR."/export_m/".$key.".xml", "w");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
	//curl_setopt($ch, CURLOPT_HEADER, 1);
	//curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'ecdhe_rsa_aes_128_gcm_sha_256');
	//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_VERBOSE,true);
	curl_setopt($ch, CURLOPT_FILE, $fp);
	curl_exec($ch);
	curl_close($ch);
	fclose($fp);
}
// Вывод времени срабатывания
$fp = fopen("../../logs/logger.txt", "a"); // Открываем файл в режиме записи
fwrite($fp, date("H:i:s  d.m.Y")." vkino_file \r\n"); // Запись в файл
fclose($fp); //Закрытие файла
?>