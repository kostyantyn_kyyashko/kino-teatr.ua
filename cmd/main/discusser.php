<?
require_once('../init.php');
if(!$task['id'])
	die('error');
	
sys::useLib('main::discuss');
sys::useLib('main::cinemas');
sys::useLib('main::news');
sys::useLib('main::persons');
sys::useLib('main::films');
sys::useLib('main::reviews');
sys::useLib('main::contest');
sys::useLib('main::serials');

$_db->query("
	LOCK TABLES 
		`#__main_discuss_messages` WRITE,
		`#__sys_mods_tasks` WRITE
");

$r=$_db->query("
	SELECT
		`object_id`,
		`object_type`,
		`user_id`
	FROM
		`#__main_discuss_messages`
	WHERE
		`date`>'".$task['last_run']."'
");

$_db->setValue('sys_mods_tasks','last_run',gmdate('Y-m-d H:i:s'),intval($task['id']));
$_db->query("UNLOCK TABLES");
$messages=array();
$objects=array();
while ($obj=$_db->fetchAssoc($r))
{
	$messages[$obj['object_type'].'::'.$obj['object_id']]=$obj['user_id'];
}

$r=$_db->query("
	SELECT 
		`user_id`,
		`object_id`,
		`object_type`
	FROM
		`#__main_users_discuss_subscribe`
	WHERE
		`sent`=0
");
sys::useLang('main');

while ($subscribe=$_db->fetchAssoc($r))
{
	$object_id=$subscribe['object_type'].'::'.$subscribe['object_id'];
	sys::loadEnvVars($subscribe['user_id']);

	//if(isset($messages[$object_id]) && $messages[$object_id]!=$subscribe['user_id'])
	if(isset($messages[$object_id]))
	{
		if(!isset($objects[$object_id]))
		{
			switch ($subscribe['object_type'])
			{
				case 'film':
					$objects[$object_id]['title']=$_db->getValue('main_films','title',$subscribe['object_id'],true);
					$objects[$object_id]['url']=main_films::getFilmDiscussUrl($subscribe['object_id']);
				break;
				
				case 'news_article':
					$objects[$object_id]['title']=$_db->getValue('main_news_articles','title',$subscribe['object_id'],true);
					$objects[$object_id]['url']=main_news::getArticleDiscussUrl($subscribe['object_id']);
				break;
				
				case 'person':
					$r2=$_db->query("
						SELECT CONCAT(`firstname`,' ',`lastname`) AS `fio`
						FROM `#__main_persons_lng`
						WHERE `record_id`='".$subscribe['object_id']."'
						AND `lang_id`=".intval($_cfg['sys::lang_id'])."
					");
					list($objects[$object_id]['title'])=$_db->fetchArray($r2);
					$objects[$object_id]['url']=main_persons::getPersonDiscussUrl($subscribe['object_id']);
				break;
				
				case 'cinema':
					$objects[$object_id]['title']=$_db->getValue('main_cinemas','title',$subscribe['object_id'],true);
					$objects[$object_id]['url']=main_cinemas::getCinemaDiscussUrl($subscribe['object_id']);
				break;
				
				case 'review':
					$objects[$object_id]['title']=$_db->getValue('main_reviews','title',$subscribe['object_id']);
					$objects[$object_id]['url']=main_reviews::getreviewDiscussUrl($subscribe['object_id']);
				break;
 // inserted by Mike				
				case 'contest_article':
					$objects[$object_id]['title']=$_db->getValue('main_contest_articles','title',$subscribe['object_id'],true);
					$objects[$object_id]['url']=main_contest::getContestArticleDiscussUrl($subscribe['object_id']);	
				break;
 // inserted by Mike				
				case 'serials_article':
					$objects[$object_id]['title']=$_db->getValue('main_serials_articles','title',$subscribe['object_id'],true);
					$objects[$object_id]['url']=main_serials::getArticleDiscussUrl($subscribe['object_id']);	
				break;
			}
		}
		$email['object_type']=sys::translate('main::'.$subscribe['object_type']);
		$email['object_name']=$objects[$object_id]['title'];
		$email['url']=$objects[$object_id]['url'];
	
		sys::addMailTpl($subscribe['user_id'],'main::discusser',$email);
		$_db->query("
			UPDATE `#__main_users_discuss_subscribe` 
			SET `sent`='1'
			WHERE `object_id`='".$subscribe['object_id']."'
			AND `object_type`='".$subscribe['object_type']."'
			AND `user_id`='".$subscribe['user_id']."'
		");
	}

}







?>