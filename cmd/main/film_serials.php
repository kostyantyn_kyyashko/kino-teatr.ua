<?
$force_links = array(
//"https://epscape.com/show/400/twin-peaks",
//"https://epscape.com/show/24/sherlock",
//"https://epscape.com/show/23/the-walking-dead"  // !!! в последней не д.б. запятой !!!
);

require_once('../init.php');
require_once('../../third_pary/simple_html_dom.php');

if(!$task['id']) die('error');

//////////////////////////////////////////////////////////////////////////////
/// Configuration
//////////////////////////////////////////////////////////////////////////////

$email = "noreply@kino-teatr.ua,cm@kino-teatr.ua";
//$email = "master@obelchenko.ru";
$headers ="MIME-Version: 1.0" . "\r\n";
$headers.="Content-Type: text/html; charset=utf-8" . "\r\n";
$headers.="Content-Transfer-Encoding: 8bit" . "\r\n";
$headers.="From: =?utf-8?b?".base64_encode("Отчет парсера сериалов")."?=<admin@kino-teatr.ua>"  . "\r\n";
$subjectSerial="=?utf-8?b?".base64_encode("Отчет парсера - новый сериал.")."?=";
$subjectSeria="=?utf-8?b?".base64_encode("Отчет парсера - новая серия")."?=";

//////////////////////////////////////////////////////////////////////////////
/// Functions
//////////////////////////////////////////////////////////////////////////////

function my_var_dump($s)
{
	print "<pre>";
	var_dump($s);
	print "</pre>";
}

function PutLog($s)
{
//	print htmlspecialchars($s)."<br>";
	file_put_contents ("../../logs/serials.log", $s."\r\n", FILE_APPEND);
}

function GetUrl($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, "https://www.google.ru/?gfe_rd=cr&ei=NK2gWPzzK-XAuAHi3pG4Bg&gws_rd=ssl#newwindow=1&q=+EPSCAPE+");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function parseMonth($name)
{
	switch($name) {
		case "января": return "01";
		case "февраля": return "02";
		case "марта": return "03";
		case "апреля": return "04";
		case "мая": return "05";
		case "июня": return "06";
		case "июля": return "07";
		case "августа": return "08";
		case "сентября": return "09";
		case "октября": return "10";
		case "ноября": return "11";
		case "декабря": return "12";
		default: return false;
	}
}

function parseDate($d)
{
	if(strlen($d) < 5) return "01.01.1900";
	if(mb_strpos($d,"Сегодня") === 0) return date("d.m.Y");
	
	$d = explode(" ", $d);
	$res[] = $d[0];
	$res[] = parseMonth($d[1]);
	$res[] = ( (isset($d[2]) && (intval($d[2])>0)) ?$d[2]:date("Y"));
	return implode(".",$res);
}

function IsInArray($dbarr, $seria)
{
	foreach ($dbarr as $dbrec)
	{
		if( !($dbrec["date"] == $seria["date"]) ) continue;				
		if( $dbrec["season"] != $seria["season"] ) continue;
		if( $dbrec["seria"] != $seria["seria"] ) continue;
		return true;				
//		if(strcmp(mb_strtoupper($dbrec["title"]), mb_strtoupper($seria["title"])) == 0) return true;
	}
	return false;
}

function getSeriaFromDom($dom)
{
    	$seria = array();
    	$episode = $dom;
    	   	
    	$seria["title"] = $episode->find('td.episodes-table__episode-title', 0)->plaintext;
    	$seria["date"] = parseDate($episode->find('td.episodes-table__episode-date', 0)->plaintext);
//    	$seria["original_date"] = $episode->find('span.episodes-table__episode-date', 0)->plaintext;
    	
    	$ss = trim($episode->find('td.episodes-table__episode-number', 0)->plaintext); 
    	if($ss == "special")
    	{
    		$seria["season"] = 0;
    		$seria["seria"] = 0;    		
    	}
    	else 
    	{
		    $seasonXseria = explode("x",$ss);
    		$seria["season"] = trim($seasonXseria[0]);
    		$seria["seria"] = trim($seasonXseria[1]);
    	}
    	
    	$seria["link"] = $episode->find('a', 0)->href;    
    	return $seria;
}


//////////////////////////////////////////////////////////////////////////////
/// Programm
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
PutLog("\r\n============================\r\nStarted at ".date("d.m.Y H:i"));
//////////////////////////////////////////////////////////////////////////////

$host = "https://epscape.com/ru";
$old_host = "http://epscape.com";
$main_page = "$host/calendar/upcoming";


$episodes=array();

if(sizeof($force_links)>0)
 PutLog("\r\nПринудительная загрузка (".sizeof($force_links)." ссылки)!\r\n");

// принудительная выгрузка по массиву $force_links
foreach ($force_links as $link)
{
    $var = array();
    // в БД есть сериалы со старыми ссылками без https и ru. Включим их в проверку.
    $http_link = str_replace("https:", "http:", $link);

	$q="SELECT count(*) 
		FROM `#__main_films` 
		WHERE serial=1 AND (serial_link='$link' OR serial_link='$http_link')";
	
	$result = $_db->query($q);
	$row=$_db->fetchArray($result);	
	if($row[0] == 0) // если сериала в БД нет, то отправить уведомление и страницу не обрабатывать.
	{
		mail($email,$subjectSerial,"Появился новый сериал (принудительный): $link \r\n<br>Сохраните эту ссылку в админке фильма в поле 'Ссылка - донор'",$headers);
		PutLog("Новый сериал (принудительный): $link");
		continue;
	}    
	
	PutLog("Сериал уже есть в БД (принудительный): $link");
	
	// продолжим обработку страницы со списком сезонов и серий текущего сериала
	// 
	
	$q="SELECT id 
		FROM `#__main_films` 
		WHERE serial=1 AND (serial_link='$link' OR serial_link='$http_link')";
	
	$result = $_db->query($q);
	$row=$_db->fetchArray($result);	
	$var["film_id"] = $row["id"];

	// Выберем из БД список зарегистрированных серий в массив
	$dbarr = array();
	$q="SELECT 
			films.id,
			films.name,
			series.season,
			series.seria,
			series.date,
			series.title
		FROM `#__main_films` as films	
		LEFT JOIN `#__main_films_serials_series` AS `series` ON series.film_id=films.id		
		WHERE films.serial=1 AND (films.serial_link='$link' OR films.serial_link='$http_link')
	";
	
	$result = $_db->query($q);
	while($row=$_db->fetchAssoc($result))	$dbarr[] = $row;	

    // read serial page with all seasons and series
    $series_html = str_get_html(GetUrl($link));
    foreach ($series_html->find('tr.episodes-table__episode') as $episode)
    {
		$seria = getSeriaFromDom($episode);    	
    	if(!IsInArray($dbarr, $seria)) $var["series"][] = $seria;
    	//else PutLog("Серия уже есть в базе: " . $seria["season"] . " / " . $seria["seria"] . " / " . $seria["title"]);
    }
    
    $episodes[] = $var;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Принудительная закончена
////////////////////////////////////////////////////////////////////////////////////////////



if(sizeof($force_links)>0) PutLog("\r\nОсновной алгоритм.\r\n");

// read root anonses page
$html = str_get_html(GetUrl($main_page));
$ret = $html->find('div.main', 0); 
PutLog($ret?"Главная страница загружена успешно!":"Главная страница не загружена!");

// выгрузка с главной страницы донора
foreach ($ret->find('div.calendar-card__episode') as $serial)
{
    $var = array();
    
    $info = $serial->find('div.calendar-card__episode-info', 0);
    $link = $var["href"] = $info->find('a', 0)->href;
    // в БД есть сериалы со старыми ссылками без https и ru. Включим их в проверку.
    $old_link = str_replace($host, $old_host, $link);
    $http_link = str_replace("https:", "http:", $link);

	$q="SELECT count(*) 
		FROM `#__main_films` 
		WHERE serial=1 AND (serial_link='$link' OR serial_link='$old_link' OR serial_link='$http_link')";
	
	$result = $_db->query($q);
	$row=$_db->fetchArray($result);	
	if($row[0] == 0) // если сериала в БД нет, то отправить уведомление и страницу не обрабатывать.
	{
		mail($email,$subjectSerial,"Появился новый сериал: $link \r\n<br>Сохраните эту ссылку в админке фильма в поле 'Ссылка - донор'",$headers);
		PutLog("Новый сериал: $link");
		continue;
	}    
	
	//PutLog("Сериал уже есть в БД: $link");
	// продолжим обработку страницы со списком сезонов и серий текущего сериала
	// 
	
	$q="SELECT id 
		FROM `#__main_films` 
		WHERE serial=1 AND (serial_link='$link' OR serial_link='$old_link' OR serial_link='$http_link')";
	
	$result = $_db->query($q);
	$row=$_db->fetchArray($result);	
	$var["film_id"] = $row["id"];

	// Выберем из БД список зарегистрированных серий в массив
	$dbarr = array();
	$q="SELECT 
			films.id,
			films.name,
			series.season,
			series.seria,
			series.date,
			series.title
		FROM `#__main_films` as films	
		LEFT JOIN `#__main_films_serials_series` AS `series` ON series.film_id=films.id		
		WHERE films.serial=1 AND (films.serial_link='$link' OR films.serial_link='$old_link' OR films.serial_link='$http_link')
	";
	
	$result = $_db->query($q);
	while($row=$_db->fetchAssoc($result))	$dbarr[] = $row;

    $var["name_ru"] = trim($info->find('b', 0)->plaintext);
	$var["name_en"] = trim($info->find('i', 0)->plaintext);
		
	$seasonXseria = explode("x",$info->find('span', 0)->plaintext);
	$var["season"] = trim($seasonXseria[0]);
	$var["seria"] = trim($seasonXseria[1]);
	$var["date"] = trim($info->find('span', 1)->plaintext);

	// read serial page with all seasons and series
    $series_html = str_get_html(GetUrl($link));
    foreach ($series_html->find('tr.episodes-table__episode') as $episode)
    {
		$seria = getSeriaFromDom($episode);    	
    	if(!IsInArray($dbarr, $seria)) 
    	{
    		$var["series"][] = $seria;
    		//PutLog("Серия будет добавлена: " . $seria["season"] . " / " . $seria["seria"] . " / " . $seria["title"]);
    	}
    	//else PutLog("Серия уже есть в базе: " . $seria["season"] . " / " . $seria["seria"] . " / " . $seria["title"]);
    }
    $episodes[] = $var;
}

////////////////////////////////////////////////////////////////
/// Бывают случаи дублирования записей со страниц. Фильтруем ///
////////////////////////////////////////////////////////////////
	
	$filtered = array();
	foreach ($episodes as $ep_index=>$ep)
	if(isset($ep["series"]))
	{
		while ($s = array_pop($ep["series"]))
		{
			$exists = false;
			foreach($filtered as $f)
			{
			 	if(
			 		(+$f["season"] == +$s["season"]) &&
			 		(+$f["seria"] == +$s["seria"]) &&
			 		($f["date"] == $s["date"]) 
			 	)
			 	{
			 		$exists = true;
			 		break;
			 	}
			}
			if(!$exists)	
			{	
				$s["film_id"] = $ep["film_id"];
				$s["film_name"] = $ep["name_ru"] ." / ". $ep["name_en"]; 		
				$filtered[] = $s;
			}
		} // of while
	} // of if(isset($ep["series"]))

//	my_var_dump($episodes);
//	my_var_dump($filtered); 
////////////////////////////////////////////////////////////////
/// Те записи, которые остались в $episodes вставляем в БД /////
////////////////////////////////////////////////////////////////

	while ($s = array_pop($filtered))
	{
 		$film_name = $s["film_name"];
 		$link = $s["link"]; 		
		$film_id = $s["film_id"];

		$title = mysql_real_escape_string(trim($s["title"]));
		$date = $s["date"];
		$season = $s["season"];
		$seria = $s["seria"];
		
		$q = "INSERT INTO `#__main_films_serials_series` 
			(film_id, date, season, seria, title) 
				values 
			($film_id, '$date', '$season', '$seria', '$title')";
//		$_db->printR($q);
		$_db->query($q);
		PutLog("Новая серия: <id=".$_db->last_id."> <$date> <season=$season> <seria=$seria> <$title> <$link> <https://kino-teatr.ua/film-series/x-$film_id.phtml>");
		mail($email,$subjectSeria,"Сохранена новая серия: $title\r\n<br>Сериал: <a href=http://kino-teatr.ua/film-series/x-$film_id.phtml>$film_name</a>\r\n<br>\r\n<br>Источник: <a href=$link>$link</a>",$headers);	
	}
	
//////////////////////////////////////////////////////////////////////////////
PutLog("\r\nFinished at ".date("d.m.Y H:i"));	
//////////////////////////////////////////////////////////////////////////////

?>