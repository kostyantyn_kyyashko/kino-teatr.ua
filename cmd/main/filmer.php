<?
require_once('../init.php');
if(!$task['id'])
	die('error');
	
sys::useLib('main::films');


$_db->query("
	LOCK TABLES 
		`#__main_films_photos` WRITE,
		`#__main_films_posters` WRITE,
		`#__main_films_soundtracks` WRITE,
		`#__main_films_trailers` WRITE,
		`#__main_films_wallpapers` WRITE,
		`#__main_reviews` WRITE,
		`#__main_films_serials_series` WRITE,
		`#__main_news_articles` AS `art` WRITE,
		`#__main_news_articles_films` AS `art_flm` WRITE,
		`#__sys_mods_tasks` WRITE
");

//Фотки
$r=$_db->query("
	SELECT `film_id` 
	FROM `#__main_films_photos`
	WHERE `date`>'".$task['last_run']."'
");
while(list($film_id)=$_db->fetchArray($r))
{
	$films[$film_id]['photos']=true;
}

//Постеры
$r=$_db->query("
	SELECT `film_id` 
	FROM `#__main_films_posters`
	WHERE `date`>'".$task['last_run']."'
");
while(list($film_id)=$_db->fetchArray($r))
{
	$films[$film_id]['posters']=true;
}

//Саундтреки
$r=$_db->query("
	SELECT `film_id` 
	FROM `#__main_films_soundtracks`
	WHERE `date`>'".$task['last_run']."'
");
while(list($film_id)=$_db->fetchArray($r))
{
	$films[$film_id]['soundtracks']=true;
}

//Трейлеры
$r=$_db->query("
	SELECT `film_id` 
	FROM `#__main_films_trailers`
	WHERE `date`>'".$task['last_run']."'
");
while(list($film_id)=$_db->fetchArray($r))
{
	$films[$film_id]['trailers']=true;
}

//Обои
$r=$_db->query("
	SELECT `film_id` 
	FROM `#__main_films_wallpapers`
	WHERE `date`>'".$task['last_run']."'
");
while(list($film_id)=$_db->fetchArray($r))
{
	$films[$film_id]['wallpapers']=true;
}

//Рецензии
$r=$_db->query("
	SELECT `film_id` 
	FROM `#__main_reviews`
	WHERE `date`>'".$task['last_run']."'
");
while(list($film_id)=$_db->fetchArray($r))
{
	$films[$film_id]['reviews']=true;
}
//Серии сериалов 
$r=$_db->query("
	SELECT `film_id` 
	FROM `#__main_films_serials_series`
	WHERE `date_reg`>'".$task['last_run']."'
");
while(list($film_id)=$_db->fetchArray($r))
{
	$films[$film_id]['series']=true;
}

//Новости
$r=$_db->query("
	SELECT art_flm.film_id
	FROM `#__main_news_articles_films` AS `art_flm`
	LEFT JOIN `#__main_news_articles` AS `art`
	ON art_flm.article_id=art.id
	WHERE art.date>'".$task['last_run']."'
	
");
while(list($film_id)=$_db->fetchArray($r))
{
	$films[$film_id]['news']=true;
}
$_db->setValue('sys_mods_tasks','last_run',gmdate('Y-m-d H:i:s'),intval($task['id']));
$_db->query("UNLOCK TABLES");

$r=$_db->query("
	SELECT 
		`film_id`, 
		`user_id`
	FROM
		`#__main_users_films_subscribe`
");
sys::useLang('main');


while ($subscribe=$_db->fetchAssoc($r))
{
	if(isset($films[$subscribe['film_id']]))
	{
		sys::loadEnvVars($subscribe['user_id']);
		$email['updates']='<ul>';
		$email['film']=$_db->getValue('main_films','title',intval($subscribe['film_id']),true);
		
		if(isset($films[$subscribe['film_id']]['photos']))
			$email['updates'].='<li><a href="'.main_films::getFilmPhotosUrl($subscribe['film_id']).'">'.sys::translate('main::frames').'</a></li>';
		if(isset($films[$subscribe['film_id']]['posters']))
			$email['updates'].='<li><a href="'.main_films::getFilmPostersUrl($subscribe['film_id']).'">'.sys::translate('main::posters').'</a></li>';	
		if(isset($films[$subscribe['film_id']]['trailers']))
			$email['updates'].='<li><a href="'.main_films::getFilmTrailersUrl($subscribe['film_id']).'">'.sys::translate('main::trailers').'</a></li>';	
		if(isset($films[$subscribe['film_id']]['soundtracks']))
			$email['updates'].='<li><a href="'.main_films::getFilmSoundtracksUrl($subscribe['film_id']).'">'.sys::translate('main::soundtracks').'</a></li>';	
		if(isset($films[$subscribe['film_id']]['wallpapers']))
			$email['updates'].='<li><a href="'.main_films::getFilmWallpapersUrl($subscribe['film_id']).'">'.sys::translate('main::wallpapers').'</a></li>';	
		if(isset($films[$subscribe['film_id']]['news']))
			$email['updates'].='<li><a href="'.main_films::getFilmNewsUrl($subscribe['film_id']).'">'.sys::translate('main::news').'</a></li>';	
		if(isset($films[$subscribe['film_id']]['reviews']))
			$email['updates'].='<li><a href="'.main_films::getFilmReviewsUrl($subscribe['film_id']).'">'.sys::translate('main::reviews').'</a></li>';	
		if(isset($films[$subscribe['film_id']]['series']))
			$email['updates'].='<li><a href="'.main_films::getFilmSeriesUrl($subscribe['film_id']).'">'.sys::translate('main::series').'</a></li>';	
			
		$email['updates'].='</ul>';
		$email['url']=main_films::getFilmUrl($subscribe['film_id']);
		sys::addMailTpl($subscribe['user_id'],'main::filmer',$email);
	}
}


?>