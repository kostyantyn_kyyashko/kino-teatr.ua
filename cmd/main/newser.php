<?
require_once('../init.php');
if(!$task['id'])
	die('error');
	
sys::useLib('main::news');


$_db->query("
	LOCK TABLES 
		`#__main_news_articles` AS `art` WRITE,
		`#__main_news_articles_lng` AS `art_ru` WRITE,
		`#__main_news_articles_lng` AS `art_uk` WRITE,
		`#__sys_mods_tasks` WRITE
");

$r=$_db->query("
	SELECT 
		art.news_id,
		art.id,
		art.date,
		art_ru.title AS `title_ru`,
		art_ru.intro AS `intro_ru`,
		art_uk.title AS `title_uk`,
		art_uk.intro AS `intro_uk`
	FROM
		`#__main_news_articles` AS `art`
	LEFT JOIN
		`#__main_news_articles_lng` AS `art_ru`
	ON
		art_ru.record_id=art.id
	AND
		art_ru.lang_id=1
		
	LEFT JOIN
		`#__main_news_articles_lng` AS `art_uk`
	ON
		art_uk.record_id=art.id
	AND
		art_uk.lang_id=3
	WHERE
		AND art.date<'".gmdate('Y-m-d H:i:s')."'
	ORDER BY art.date
");
$_db->setValue('sys_mods_tasks','last_run',gmdate('Y-m-d H:i:s'),intval($task['id']));
$_db->query("UNLOCK TABLES");
$articles=array();
$users=array();
while ($article=$_db->fetchAssoc($r))
{
	$article['url']=main_news::getArticleUrl($article['id']);
	$articles[]=$article;
}
if($articles)
{
	$r=$_db->query("
		SELECT
			`news_id`,
			`user_id`
		FROM
			`#__main_users_news_subscribe`
	");
	while ($subscribe=$_db->fetchAssoc($r))
	{
		$users[$subscribe['user_id']][]=$subscribe['news_id'];
	}
	
	if($users)
	{
		foreach ($users as $user_id=>$user_news)
		{
			sys::loadEnvVars($user_id);
			$email['news']='';
			foreach ($articles as $article)
			{
				if(in_array($article['news_id'],$user_news))
				{
					ob_start();
					?>
						<h2 style="font-size:14px; margin:0; padding:0 0 3px 0;"><a href="<?=$article['url']?>"><?=$article['title_'.$_cfg['sys::lang']]?></a></h2>
						<?=$article['intro_'.$_cfg['sys::lang']];?>
					<?
					$email['news'].=ob_get_clean();
				}
			}
			if($email['news'])
				sys::addMailTpl($user_id,'main::newser',$email);
		}
	}
}




?>