<?
require_once('../init.php');
$r=$_db->query("SELECT * FROM `#__sys_mods_metatemplates` WHERE `mod_id`=2");
while ($tpl=$_db->fetchAssoc($r))
{
	$_db->query("
		INSERT INTO `#__sys_mods_templates`
		(
			`id`,
			`mod_id`,
			`type`,
			`name`,
			`info`
		)
		VALUES
		(
			0,
			2,
			'page',
			'".mysql_real_escape_string($tpl['name'])."',
			'".mysql_real_escape_string($tpl['info'])."'
		)
	");
	$new_id=$_db->last_id;
	$r2=$_db->query("
		SELECT * FROM `#__sys_mods_metatemplates_lng`
		WHERE `record_id`=".intval($tpl['id'])."
		AND `lang_id`=1
	");
	while ($tpl_lng=$_db->fetchAssoc($r2))
	{
		$_db->query("
			INSERT INTO `#__sys_mods_templates_lng`
			(
				`record_id`,
				`lang_id`,
				`title`,
				`meta_title`,
				`meta_description`,
				`meta_keywords`,
				`meta_other`
			)	
			VALUES
			(
				'".$new_id."',
				'1',
				'".$tpl_lng['title']."',
				'".$tpl_lng['title']."',
				'".$tpl_lng['description']."',
				'".$tpl_lng['keywords']."',
				'".$tpl_lng['other']."'
			)
		");
	}
	
	$r2=$_db->query("
		SELECT * FROM `#__sys_mods_metatemplates_lng`
		WHERE `record_id`=".intval($tpl['id'])."
		AND `lang_id`=3
	");
	while ($tpl_lng=$_db->fetchAssoc($r2))
	{
		$_db->query("
			INSERT INTO `#__sys_mods_templates_lng`
			(
				`record_id`,
				`lang_id`,
				`title`,
				`meta_title`,
				`meta_description`,
				`meta_keywords`,
				`meta_other`
			)	
			VALUES
			(
				'".$new_id."',
				'3',
				'".$tpl_lng['title']."',
				'".$tpl_lng['title']."',
				'".$tpl_lng['description']."',
				'".$tpl_lng['keywords']."',
				'".$tpl_lng['other']."'
			)
		");
	}
}

?>