<?sys::useLib('sys::gui');?>
<div style="position:relative;">
	<input alt="<?=$var['type']?>" <?if($var['req'] && $var['parse_req']):?> required="true" <?endif;?> title="<?=$var['title']?>" type="text" name="<?=$var['name']?>" class="regTextField" size="<?=(mb_strlen(strftime($var['format']))+2)?>" maxlength="<?=mb_strlen(strftime($var['format']))?>" value="<?=$var['value']?>" <?=$var['html_params']?>>
	<input value="<? echo isset($var['icon']) ? $var['icon'] : sys::translate('sys::calendar'); ?>" type="button" id="calendarBtn" onMouseUp="showFrameBox(this,'?mod=sys&act=calendar&field=<?=$var['name']?>&format=<?=base64_encode($var['format'])?>'); return false;" title="<?=sys::translate('sys::calendar')?>">
	<div id='kalBox' title="<?=sys::translate('sys::calendar');?>" style="display:none; width:276px; height:211px; position:absolute; z-index:1; left:0; top:0;" class="panel">
		<div style="text-align:right;" class="panel title"><img class="hand" style="cursor:pointer;" onclick="this.parentNode.parentNode.style.display='none';" src="<?=sys_gui::getIconSrc('sys::btn.close.gif')?>" alt="<?=sys::translate('sys::close')?>" title="<?=sys::translate('sys::close')?>"></div>
		<iframe frameborder="0" width="100%" height="<?=(200-18)?>"></iframe>
	</div>
</div>

