<?$day=1;?>

<style>
table.x  td
{
	text-align: center;
}
</style>

<table style="height:100%;" class="x table links2">
	<tr>
		<th style="text-align:center;" colspan="7">
			<a href="<?=$var['prev_year_url']?>">&laquo;&laquo;</a>&nbsp;&nbsp;<a class="nav" href="<?=$var['prev_month_url']?>">&laquo;</a>&nbsp;&nbsp;<?=$var['monthbox']?> <?=$var['yearbox']?>&nbsp;&nbsp;<a class="nav" href="<?=$var['next_month_url']?>">&raquo;</a>&nbsp;&nbsp;<a class="nav" href="<?=$var['next_year_url']?>">&raquo;&raquo;</a>
		</th>
	</tr>
	<tr>
	<?foreach($var['week_days'] as $week_day):?>
		<th><?=$week_day?></th>
	<?endforeach;?>
	</tr>
	<?for($i=0; $i<6; $i++):?>
	<tr>
		<?for($j=0; $j<7; $j++):?>
			<?if($i==0):?>
				<?if($j>=$var['first_day']):?>
					<?if($day==$var['cur_day']) $suff='_active';else $suff=false;?>
						<td><a class="date<?=$suff?>" href="<?=$var['url'][$day]?>" onclick="<?=$var['onclick'][$day]?>; return false;"><?=$day?></a></td>
					<?$day++?>
				<?else:?>
					<td></td>
				<?endif?>
			<?elseif($day<=$var['num_of_days']):?>
				<?if($day==$var['cur_day']) $suff='_active';else $suff=false;?>
				<td><a class="date<?=$suff?>" href="<?=$var['url'][$day]?>" onclick="<?=$var['onclick'][$day]?>;return false;"><?=$day?></a></td>
				<?$day++?>
			<?else:?>
				<td class="x">&nbsp;</td>
			<?endif?>
		<?endfor?>
	</tr>
	<?endfor;?>
</table>

