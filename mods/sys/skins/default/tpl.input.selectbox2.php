<?sys::jsInclude('sys::selectbox')?>
<?sys::cssInclude('sys::selectbox')?>
<div class="selectbox-<?=$var['name']?>">
    <select name="<?=$var['name']?><?if($var['multi']):?>[]<?endif?>" <?if($var['multi']):?>multiple<?endif?> alt="<?=$var['type']?>" <?if($var['req'] && $var['parse_req']):?> required="true" <?endif;?> title="<?=$var['title']?>"  <?=$var['html_params']?>>
    <?if(isset($var['empty'])):?>
    	<option value="<?=$var['empty::value']?>"><?=$var['empty']?></option>
    <?endif?>
    <?if(is_array($var['values'])):?>
    	<?foreach ($var['values'] as $key=>$val):?>
    		<?if(!is_array($var['value'])):?>
    			<?if($var['value'] && (string)$var['value']==(string)$key)$sel='selected';else $sel=false;?>
    		<?else:?>
    			<?if(in_array($key, $var['value']))$sel='selected';else $sel=false;?>
    		<?endif?>
    			<option value="<?=$key?>" <?=$sel?> ><?=$val?></option>
    	<?endforeach?>
    <?else:?>
    	<option></option>
    <?endif;?>
    </select>
</div>
<script>
$('div.selectbox-<?=$var["name"]?> > select').selectbox({
    onOpen: function(inst) {
        $('#sbOptions_' + inst.uid).css('z-index', '20 !important');
    },
    onClose: function(inst) {
        $('#sbOptions_' + inst.uid).css('z-index', '10 !important');
    }
});
</script>