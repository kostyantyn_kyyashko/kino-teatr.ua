<?php


require_once 'class.phpmailer.php';

reset($_POST);
foreach ($_POST as $key=>$val)
    $$key = $val;

$output = '{
    "status":%s,
    "msg":"%s"
}';
$status = 'true';
$msg = '';

if ($from_email && $from_name && $to_email && $to_name && $subject && $body) {
    $mail = new PHPMailer();
    $mail->From = $from_email;
    $mail->FromName = $from_name;
    $mail->AddAddress($to_email, $to_name);
    $mail->Subject = $subject;
    $mail->MsgHTML($body);
    $mail->CharSet = 'UTF-8';
    if (!$mail->Send()) {
        $status = 'false';
        $msg = $mail->ErrorInfo;
    }
} else {
    $status = 'false';
    $msg = 'Incorrect input parameters';
}

header('Content-Type: text/plain; charset=utf-8');
echo sprintf($output, $status, $msg);
exit;


/**
 * Array fix
 */
function _arrayFix($value) {
  reset($value);
  while (list($key, $val) = each($value)) {
    if (is_array($val)) $value[$key] = _arrayFix($val);
    elseif (!is_object($val)) $value[$key] = htmlspecialchars($val);
  }
  return $value;
}

/**
 * Print variables
 */
function sprint($val) {
  $__pre_b = '<pre style="font-family:\'Courier New\', sans-serif; font-size:12px; background:#eee; color:#000; text-align:left; padding:1px 5px 2px 5px; border-bottom:1px dotted #bbb; border-top:1px dotted #bbb; margin:0 0 5px 0;">';
  $__pre_e = '</pre>';
  print($__pre_b);
  if (is_array($val)) {
    $val = _arrayFix($val);
    print_r($val);
    reset($val);
  } else if (is_object($val)) {
    print_r($val);
  } else {
    print(htmlspecialchars($val));
  }
  print($__pre_e);
}

?>