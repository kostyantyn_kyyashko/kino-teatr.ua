<?
function sys_template_edit()
{
	if(!sys::checkAccess('sys::template_edit'))
		return 403;
		
	global $_db, $_err;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	
	sys::filterGet('id','int');
	sys::filterGet('type','text','text');
	
	if(!$_GET['id'] && !sys::checkAccess('sys::template_add'))
		return 403;
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='sys_mods_templates';
	$default=array();
	$default['mod_id']=1;
	$default['type']=$_GET['type'];
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id'],true);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('sys::new_template');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	if(sys::checkAccess('sys::template_add'))
	{
		//type
	
		$fields['type']['input']='selectbox';
		$fields['type']['type']='text';
		$fields['type']['req']=true;
		$fields['type']['table']=$tbl;
		$fields['type']['values']=array(
			'text'=>sys::translate('sys::text'),
			'html'=>sys::translate('sys::html'),
			'page'=>sys::translate('sys::page'),
			'email'=>sys::translate('sys::email')
		);
		if(!$_GET['id'])
			$fields['type']['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\');window.location=\'?mod=sys&act=template_edit&type=\'+this.value"';
		
		
		//mod_id
		$fields['mod_id']['input']='selectbox';
		$fields['mod_id']['type']='int';
		$fields['mod_id']['req']=true;
		$fields['mod_id']['table']=$tbl;
		$fields['mod_id']['values']=$_db->getValues('sys_mods','name',false,"ORDER BY `order_number`");	
		$fields['mod_id']['title']=sys::translate('sys::module');		

		//name
		$fields['name']['input']='textbox';
		$fields['name']['type']='text';
		$fields['name']['req']=true;
		$fields['name']['table']=$tbl;
		$fields['name']['max_chars']=55;
		$fields['name']['unique']=true;
		$fields['name']['unique::where']="`id`!=".intval($_GET['id'])." AND `mod_id`=".intval($values['mod_id'])." AND `type`=".intval($values['type'])."";
		$fields['name']['html_params']='style="width:100%" maxlength="55"';
		
		//description
		$fields['description']['input']='textbox';
		$fields['description']['type']='text';
		$fields['description']['req']=true;
		$fields['description']['table']=$tbl;
		$fields['description']['max_chars']=255;
		$fields['description']['html_params']='style="width:100%" maxlength="255"';
	}
	
	//info
	$fields['info']['input']='fck';
	$fields['info']['type']='html';
	$fields['info']['table']=$tbl;
	$fields['info']['height']='300px';
	$fields['info']['css']='_sys';
	if(!sys::checkAccess('sys::template_add'))
	{
		$fields['info']['input']="info";
		$fields['info']['table']=false;
		$fields['info']['html_params']=false;
	}
	
	if($values['type']=='email')
	{
		//subject
		$fields['subject']['input']='textbox';
		$fields['subject']['type']='text';
		$fields['subject']['table']=$tbl;
		$fields['subject']['max_chars']=255;
		$fields['subject']['html_params']='style="width:100%" maxlength="255"';
		$fields['subject']['multi_lang']=true;
	}
	
	if($values['type']=='page')
	{
		//title
		$fields['title']['input']='textbox';
		$fields['title']['type']='text';
		$fields['title']['table']=$tbl;
		$fields['title']['max_chars']=255;
		$fields['title']['html_params']='style="width:100%" maxlength="255"';
		$fields['title']['multi_lang']=true;
		$fields['title']['title']=sys::translate('sys::header');
		
		//meta_title
		$fields['meta_title']['input']='textbox';
		$fields['meta_title']['type']='text';
		$fields['meta_title']['table']=$tbl;
		$fields['meta_title']['max_chars']=255;
		$fields['meta_title']['html_params']='style="width:100%" maxlength="255"';
		$fields['meta_title']['multi_lang']=true;
		$fields['meta_title']['title']=sys::translate('sys::meta_title');
		
		//meta_description
		$fields['meta_description']['input']='textarea';
		$fields['meta_description']['type']='text';
		$fields['meta_description']['table']=$tbl;
		$fields['meta_description']['multi_lang']=true;
		$fields['meta_description']['html_params']='style="width:100%; height:50px"';
		
		//meta_keywords
		$fields['meta_keywords']['input']='textarea';
		$fields['meta_keywords']['type']='text';
		$fields['meta_keywords']['table']=$tbl;
		$fields['meta_keywords']['multi_lang']=true;
		$fields['meta_keywords']['html_params']='style="width:100%; height:50px"';
		
		//meta_other
		$fields['meta_other']['input']='textarea';
		$fields['meta_other']['type']='html';
		$fields['meta_other']['table']=$tbl;
		$fields['meta_other']['multi_lang']=true;
		$fields['meta_other']['html_params']='style="width:100%; height:50px"';
	}
	
	if($values['type']!='text')
	{
		//text
		$fields['text']['input']='fck';
		$fields['text']['type']='html';
		$fields['text']['table']=$tbl;
		$fields['text']['multi_lang']=true;
		$fields['text']['html_params']='style="width:100%; height:50px"';
		$fields['text']['height']='300px';
		if($values['type']=='email')
			$fields['text']['css']='email';
	}
	else 
	{
		$fields['text']['input']='textarea';
		$fields['text']['type']='text';
		$fields['text']['table']=$tbl;
		$fields['text']['multi_lang']=true;
		$fields['text']['html_params']='style="width:100%; height:150px"';
		
	}
	//=========================================================================================================//

	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>