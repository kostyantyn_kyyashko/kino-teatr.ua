<?
function sys_fileman()
{
	if(!sys::checkAccess('sys::fileman'))
		return 403;
		
	sys::useLib('sys::form');
	sys::useLib('sys::fileman');
	
	global $_err;
	
	
	$result=array();
	
	sys::filterGet('mode','text','public');
	if($_GET['mode']=='private')
		$root_dir=_PRIVATE_DIR;
	else 
		$root_dir=_PUBLIC_DIR;
	
	sys::filterGet('dir','text',base64_encode($root_dir));
	$dir=base64_decode($_GET['dir']);
	$title=str_replace($root_dir,'/',$dir);
	$win_params=sys::setWinParams('500','80');
	$add_title=sys::translate('sys::new_folder');
	
	$parent_dirs=array('.','..',$dir,$root_dir);
	
	if(!sys::checkPublicPath($dir))
		return 401;
	
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_FILES['file']))
	{
		
		if(!sys::uploadFile($_FILES['file'],$dir,false,false))
			$_err=sys::translate('sys::upload_error');
	}
	
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete' && !in_array($task[0],$parent_dirs))
			sys_fileman::deleteFile($dir.$task[1]);
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					if($key!=$dir && !in_array($key,$parent_dirs))
						sys_fileman::deleteFile($dir.$key);
				}
			}
					
		}	
	}
	//================================================================================================//
		
	//Таблица--------------------------------------------------------------------------
	$table['checkbox']=true;
	$files=scandir($dir);
	foreach ($files as $file)
	{
		$obj['name']=$file;
		if(is_dir($dir.$file))
		{
			$obj['type']='dir';
			$obj['size']=0;
		}
		else 
		{
			$obj['type']='file';
			$obj['size']=sys::convertFileSize(filesize($dir.$file));
		}
		if($obj['type']=='dir')
		{
			$obj['name']=' '.$obj['name'].'';
			if($file=='.')
			{
				if($dir==$root_dir)
					continue;
				$obj['url']='?mod=sys&act=fileman&mode='.$_GET['mode'].'&dir='.base64_encode($root_dir);
			}
			elseif($file=='..')
			{
				if($dir==$root_dir)
					continue;
				$arr_dir=explode('/',$dir);
				array_pop($arr_dir);
				array_pop($arr_dir);
				$path='';
				for($i=0; $i<count($arr_dir); $i++)
				{
					$path.=''.$arr_dir[$i].'/';
				}
				$obj['url']='?mod=sys&mode='.$_GET['mode'].'&act=fileman&dir='.base64_encode($path);
			}
			else
				$obj['url']='?mod=sys&act=fileman&mode='.$_GET['mode'].'&dir='.base64_encode($dir.$file.'/');
		}
		else 
		{	
			if($_GET['mode']=='private')
				$obj['url']='?mod=sys&act=download&file='.base64_encode($dir.$file);
			else 	
				$obj['url']=str_replace(_PUBLIC_DIR,_PUBLIC_URL,$dir).$file;
		}
			
		$table['records'][$file]=$obj;
		
		if($obj['type']=='dir')
		{
			$table['params'][$file]['name']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='?mod=sys&mode=".$_GET['mode']."&act=fileman&dir=".$obj['url']."'; return false;";
			$table['params'][$file]['icon']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='?mod=sys&act=fileman&mode=".$_GET['mode']."&dir=".$obj['url']."'; return false;";
			$table['params'][$file]['icon']['image']='sys::btn.folder.gif';
		}
		else 
		{
			$table['params'][$file]['name']['url']=$obj['url'];
			$table['params'][$file]['icon']['url']=$obj['url'];
		}
		$table['params'][$file]['icon']['title']=$obj['name'];
	
	}
	
	asort($table['records']);
	
	$table['fields']['icon']['type']='button';
	$table['fields']['icon']['image']='sys::btn.file.gif';
	$table['fields']['icon']['title']='';
	
	$table['fields']['name']['type']='text';
	
	$table['fields']['size']['type']='text';
	$table['fields']['size']['width']=80;
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	if(sys::checkAccess('sys::make_folders'))
	{
		$buttons['new']['onclick']="window.open('?mod=sys&act=fileman_dir_add&dir=".base64_encode($dir)."','','".$win_params."');";
		$buttons['new']['title']=$add_title;
		$buttons['new']['image']='sys::btn.new_folder.gif';
	}
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	//=======================================================================================================//
	
	//Селектор режима------------------------------------------------------------------------------------
	$mode['input']='selectbox';
	$mode['values']['public']=sys::translate('sys::public_section');
	$mode['values']['private']=sys::translate('sys::private_section');
	$mode['value']=$_GET['mode'];
	$mode['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\');window.location=\'?mod=sys&act=fileman&mode=\'+this.value"';
	$mode['title']=sys::translate('sys::section');
	//==============================================================================
	
	//Форма загрузки---------------------------------------------
	$upload['action']=$_SERVER['REQUEST_URI'];
	//==============================================================
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle(sys::translate('sys::fileman'));
	
	$result['panels'][0]['text']=sys_gui::showNavPath('sys::fileman');
	
	$result['panels'][1]['text']=$title;
	$result['panels'][1]['class']='title';
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSpacer().sys_form::parseField('mode',$mode).sys_gui::showSpacer().sys::parseTpl('sys::form.upload',$upload);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>