<?
function sys_mod_lang_edit()
{
	if(!sys::checkAccess('sys::mod_edit'))
		return 403;
		
	global $_db, $_err;

	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::mods');
	
	sys::filterGet('file');
	sys::filterGet('mod_id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	if(!$_GET['file'])
		return 404;
		
	$mod_name=$_db->getValue('sys_mods','name',intval($_GET['mod_id']));
	$filename=_MODS_DIR.$mod_name.'/lang/'.basename($_GET['file']);
	$file=file_get_contents($filename);
	if(!$file)
		return 404;
	$title=$_GET['file'];
	$record['text']=$file;
	
	
	$values=sys::setValues($record);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//text
	$fields['text']['input']='textarea';
	$fields['text']['type']='text';
	$fields['text']['html_params']='style="width:100%; height:400px"';
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			if(is_writable($filename))
				file_put_contents($filename,$values['text']);
			else 
				$_err=sys::translate('sys::file_not_writable');
			
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>