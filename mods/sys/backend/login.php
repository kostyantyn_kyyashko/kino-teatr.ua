<?
function sys_login()
{	
	global $_langs, $_err;
	sys::useLib('sys::form');	
	sys::registerUser(2);
	
	if(isset($_POST['cmd_enter']))
	{
		sys::setCookie('sys::lang',$_langs[$_POST['lang_id']]['code']);
		$err=sys::authorizeUser($_POST['login'],$_POST['password']);
		if($err===true)
		{
			if($_SERVER['QUERY_STRING']=='mod=sys&act=login' || !$_SERVER['QUERY_STRING'])
				sys::redirect('?mod=sys&act=default', false);
			else 
				sys::redirect('?'.$_SERVER['QUERY_STRING'], false);
		}
		else 
			$_err=sys::translate('sys::'.$err);
	}
	
	
	//login
	$fields['login']['input']='textbox';
	$fields['login']['type']='text';
	$fields['login']['req']=true;
	$fields['login']['html_params']='size="22" maxlength="32"';
	//======================================
	
	//password
	$fields['password']['input']='password';
	$fields['password']['type']='text';
	$fields['password']['req']=true;
	$fields['password']['html_params']='size="22" maxlength="32"';
	//======================================
	
	//lang_id
	$fields['lang_id']['input']="selectbox";
	$fields['lang_id']['type']='int';
	$fields['lang_id']['req']=true;
	$fields['lang_id']['title']=sys::translate('sys::language');
	
	if(isset($_COOKIE['sys::lang']))
	{
		foreach ($_langs as $key=>$val)
		{
			if($val['code']==$_COOKIE['sys::lang'])
			{
				$fields['lang_id']['value']=$val['id'];
				break;
			}
		}
	}
	
	if(isset($_POST['lang_id']))
		$fields['lang_id']['value']=$_POST['lang_id'];
	 
	foreach ($_langs as $lang)
	{
		$fields['lang_id']['values'][$lang['id']]=$lang['title'];
	}
	//=========================
	
	
	$form['fields']=sys_form::parseFields($fields);
	
	$result['main']=sys::parseTpl('sys::form.login',$form);
	return $result;
}
?>