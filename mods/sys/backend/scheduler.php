<?
function sys_scheduler()
{
	if(!sys::checkAccess('sys::scheduler'))
		return 403;
		
	global $_db, $_err;
	
	sys::useLib('sys::pages');
	sys::useLib('sys::mods');
	sys::useLib('sys::form');
		
	$tbl='sys_mods_tasks';
	$edit_url='?mod=sys&act=schedule_edit';
	$win_params=sys::setWinParams('600','400');
	
	$order_where=$where=false;
	
	$nav_point='sys::scheduler';
	
	$q="SELECT 
			tsk.id, 
			CONCAT(mods.name,'::',tsk.name) AS `name`,
			tsk.name as task,
			tsk.min,
			tsk.hour,
			tsk.mday,
			tsk.month,
			tsk.wday,
			tsk.on,
			tsk.last_run
		FROM `#__".$tbl."` AS `tsk`
		LEFT JOIN `#__sys_mods` AS `mods`
		ON mods.id=tsk.mod_id
		ORDER BY tsk.mod_id, tsk.order_number";
		
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		
			
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$r=$_db->query($q.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['name']=sys::translate($obj['name']);
		$obj['task']=$obj['task'].".php";
		$obj['last_run']=sys::db2Date($obj['last_run'],true);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=false;
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['width']=150;

	$table['fields']['task']['type']='text';
	$table['fields']['task']['width']=150;
	
	$table['fields']['min']['type']='text';
	$table['fields']['min']['title']=sys::translate('sys::minute');
	$table['fields']['min']['width']=100;
	
	$table['fields']['hour']['type']='text';
	$table['fields']['hour']['width']=100;
	
	$table['fields']['mday']['type']='text';
	$table['fields']['mday']['title']=sys::translate('sys::month_day');
	$table['fields']['mday']['width']=100;
	
	$table['fields']['month']['type']='text';
	$table['fields']['month']['width']=100;
	
	$table['fields']['wday']['type']='text';
	$table['fields']['wday']['title']=sys::translate('sys::week_day');
	$table['fields']['wday']['width']=100;
	
	$table['fields']['last_run']['type']='text';
	
	$table['fields']['on']['type']='checkbox';
	$table['fields']['on']['title']=sys::translate('sys::is_on');
	$table['fields']['on']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~on~%id%~%on%\')"';
	$table['fields']['on']['width']=40;
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
