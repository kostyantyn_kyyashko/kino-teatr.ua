<?
function sys_users_and_groups()
{
	if(!sys::checkAccess('sys::users_and_groups'))
		return 403;
		
	sys::setTitle(sys::translate('sys::users_and_groups'));
	$result['panels'][0]['text']=sys_gui::showNavPath('sys::users_and_groups');
	$result['main']=sys_gui::showIcons('sys::users_and_groups');
	return $result;
}
?>