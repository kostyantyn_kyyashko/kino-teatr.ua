<?
function sys_group_rights_tree()
{
	if(!sys::checkAccess('sys::group_edit'))
		return 403;
		
	sys::useLib('sys::users');
	sys::useLib('sys::form');
	
	sys::filterGet('parent_id','int',0);
	sys::filterGet('group_id','int');
	sys::filterGet('mode');
	
	$tree['objects']=sys_users::getRightsTreeBranch($_GET['parent_id']);
	
	if($_GET['parent_id'])
		sys_gui::mergeTree($_GET['parent_id'],$tree['objects']);
	else 
	{
		//Кнпки
		$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
		
		//Дерево
		$tree['root']['title']=sys::translate('sys::root');
	
		$tree['root']['onclick']="setMsg('".sys::translate('sys::loading')."','window.parent.fra_right'); window.parent.fra_right.location='?mod=sys&act=group_rights_table&parent_id=0&group_id=".$_GET['group_id']."&mode=".$_GET['mode']."';";
		$tree['root']['url']='?mod=sys&act=group_rights&group_id='.$_GET['group_id'];
		
		$tree['parent_id']=0;
		
		$mode['input']='selectbox';
		$mode['values']['frontend']=sys::translate('sys::frontend');
		$mode['values']['backend']=sys::translate('sys::backend');
		$mode['value']=$_GET['mode'];
		$mode['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\',\'window.parent\');window.parent.location=\'?mod=sys&act=group_rights&group_id='.$_GET['group_id'].'&mode=\'+this.value"';
		$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_form::parseField('mode',$mode);
		
		$result['main']=sys::parseTpl('sys::tree',$tree);
	}
	if(isset($result))
		return $result;
}
?>