<?
function sys_mod_entries_table()
{
	if(!sys::checkAccess('sys::mod_edit'))
		return 403;
		
	global $_db, $_err;
	
	sys::useLib('sys::pages');
	sys::useLib('sys::mods');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.asc');
	sys::filterGet('mod_id','int');
		
	if(!$_GET['mod_id'])
		return 404;
		
	$tbl='sys_mods_entries';
	$edit_url='?mod=sys&act=mod_entry_edit';
	$win_params=sys::setWinParams('600','400');
	$add_title=sys::translate('sys::add_entrypoint');
	
	$order_where=$where=" `mod_id`=".intval($_GET['mod_id'])."";
	$mod=$_db->getRecord('sys_mods',$_GET['mod_id']);
	
	if(!$mod)
		return 404;
	
	$title=$mod['name'].' :: '.sys::translate('sys::entries');
	
	$q="SELECT 
			`id`, `name`
		FROM `#__".$tbl."` 
		WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			sys_mods::deleteEntry($task[1]);
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					sys_mods::deleteEntry($key);
				}
			}
					
		}
			
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=sys::translate($mod['name'].'::'.$obj['name']);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;
	
	$table['fields']['title']['type']='text';
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&mode=add&mod_id=".$_GET['mod_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle(sys::translate($title));
	
	$result['panels'][0]['text']=sys::translate($title);
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=sys_mods::showTabs($_GET['mod_id'],'entries');
	$result['panels'][]=$panel;	
	unset($panel);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
