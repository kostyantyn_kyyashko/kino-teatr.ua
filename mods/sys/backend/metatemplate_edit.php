<?
function sys_metatemplate_edit()
{
	if(!sys::checkAccess('sys::metatemplate_edit'))
		return 403;
		
	global $_db, $_err;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	
	sys::filterGet('id','int');
	
	if(!$_GET['id'] && !sys::checkAccess('sys::metatemplate_add'))
		return 403;
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='sys_mods_metatemplates';
	$default=array();
	$default['mod_id']=1;
	$default['type']='text';
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id'],true);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('sys::new_metatemplate');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	if(sys::checkAccess('sys::metatemplate_add'))
	{
		//mod_id
		$fields['mod_id']['input']='selectbox';
		$fields['mod_id']['type']='int';
		$fields['mod_id']['req']=true;
		$fields['mod_id']['table']=$tbl;
		$fields['mod_id']['values']=$_db->getValues('sys_mods','name',false,"ORDER BY `order_number`");	
		$fields['mod_id']['title']=sys::translate('sys::module');		

		//name
		$fields['name']['input']='textbox';
		$fields['name']['type']='text';
		$fields['name']['req']=true;
		$fields['name']['table']=$tbl;
		$fields['name']['max_chars']=55;
		$fields['name']['unique']=true;
		$fields['name']['unique::where']="`id`!=".intval($_GET['id'])." AND `mod_id`=".intval($values['mod_id'])."";
		$fields['name']['html_params']='style="width:100%" maxlength="55"';
		
	}
	
	//info
	$fields['info']['input']='textarea';
	$fields['info']['type']='text';
	$fields['info']['table']=$tbl;
	$fields['info']['html_params']='style="width:100%; height:150px"';
	if(!sys::checkAccess('sys::metatemplate_add'))
	{
		$fields['info']['html_params'].=" readonly ";
		$fields['info']['table']=false;
	}
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['table']=$tbl;
	$fields['title']['multi_lang']=true;
	$fields['title']['html_params']='style="width:100%" maxlength="255"';
	$fields['title']['title']=sys::translate('sys::meta_title');
	
	//description
	$fields['description']['input']='textarea';
	$fields['description']['type']='text';
	$fields['description']['table']=$tbl;
	$fields['description']['multi_lang']=true;
	$fields['description']['html_params']='style="width:100%; height:50px"';
	$fields['description']['title']=sys::translate('sys::meta_description');
	
	//keywords
	$fields['keywords']['input']='textarea';
	$fields['keywords']['type']='text';
	$fields['keywords']['table']=$tbl;
	$fields['keywords']['multi_lang']=true;
	$fields['keywords']['html_params']='style="width:100%; height:50px"';
	$fields['keywords']['title']=sys::translate('sys::meta_keywords');
	
	//other
	$fields['other']['input']='textarea';
	$fields['other']['type']='text';
	$fields['other']['table']=$tbl;
	$fields['other']['multi_lang']=true;
	$fields['other']['html_params']='style="width:100%; height:150px"';
	$fields['other']['title']=sys::translate('sys::meta_other');
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>