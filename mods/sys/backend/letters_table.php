<?
function sys_letters_table()
{
	if(!sys::checkAccess('sys::letters'))
		return 403;
		
	global $_db, $_err;
	sys::useLib('sys::pages');
	sys::useLib('sys::letters');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','date.desc');
		
	$tbl='sys_letters';
	$edit_url='?mod=sys&act=letter_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('sys::add_letter');
	$order_where=$where=false;
	$nav_point='sys::letters';
	
	$q="SELECT 
			`id`, 
			`to_email`,
			`subject`,
			`date`
		FROM `#__".$tbl."`";
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			sys_letters::deleteLetter($task[1]);
			
		//Отправка
		if($task[0]=='send')
		{
			if(!sys_letters::sendLetter($task[1]))
				$_err=sys::translate('sys::sending_error');
		}
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					sys_letters::deleteletter($key);
				}
			}
					
		}	
		
		//Отправка отмеченных
		if($_POST['_task']=='sendChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					sys_letters::sendLetter($key);
				}
			}
		}
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['send']['type']='button';
	$table['fields']['send']['onclick']="if(confirm('".sys::translate('sys::send')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::sending')."'); document.forms['table'].elements._task.value='send.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['to_email']['type']='text';
	
	$table['fields']['subject']['type']='text';
	
	$table['fields']['date']['type']='text';
	$table['fields']['date']['width']=150;
	$table['fields']['date']['order']=true;
	
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['send_checked']['onclick']="if(confirm('".sys::translate('sys::send_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::sending')."'); document.forms['table'].elements._task.value='sendChecked'; document.forms['table'].submit();} return false;";
	$buttons['send_checked']['image']='sys::btn.send.gif';
	
	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
