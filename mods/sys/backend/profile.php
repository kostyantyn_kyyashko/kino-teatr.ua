<?
function sys_profile()
{
	global $_db, $_cfg, $_err, $_user, $_langs, $_charsets;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::users');
	
	$_GET['id']=$_user['id'];
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$nav_point='sys::my_profile';
	
	$record=array_merge($_db->getRecord('sys_users',$_user['id']), sys_users::getCfg($_user['id']), sys_users::getData($_user['id']));
	
	if(!$record)
		return 404;
	
	if($record['only_from_ip'])
		$record['only_from_ip']=long2ip($record['only_from_ip']);
	
	$values=sys::setValues($record);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//login
	$fields['login']['input']='textbox';
	$fields['login']['type']='text';
	$fields['login']['html_params']='size="16" maxlength="16"';
	$fields['login']['html_params'].=' readonly ';
	
	//email
	$fields['email']['input']='textbox';
	$fields['email']['type']='email';
	$fields['email']['req']=true;
	$fields['email']['table']='sys_users';
	$fields['email']['unique']=true;
	$fields['email']['unique::where']=$_user['id'];
	$fields['email']['html_params']='style="width:100%" maxlength="255"';
	
	//lang_id
	$fields['lang_id']['input']="selectbox";
	$fields['lang_id']['type']='int';
	$fields['lang_id']['req']=true;
	$fields['lang_id']['title']=sys::translate('sys::language');
	foreach ($_langs as $lang)
	{
		$fields['lang_id']['values'][$lang['id']]=$lang['title'];
	}
	
	//only_from_ip
	$fields['only_from_ip']['input']='textbox';
	$fields['only_from_ip']['type']='text';
	$fields['only_from_ip']['table']='sys_users';
	$fields['only_from_ip']['html_params']='size="15"';
	$fields['only_from_ip']['info']=sys::getHlp('sys::only_from_ip');
	
	$fields=array_merge($fields,sys_users::getDataFields(),sys_users::getCfgFields());
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			if($values['only_from_ip'])
				$values['only_from_ip']=ip2long($values['only_from_ip']);
			foreach ($values as $key=>$val)
			{
				if(strpos($key,'date')!==false)
					$values[$key]=sys::date2Db($values[$key],false);
			}
			
			//Вставка данных
			if(!$_db->updateArray('sys_users',$values,$fields,$_user['id']))
				$_err=sys::translate('sys::db_error').$_db->err;
				
			if(!$_err)
			{
				if(!$_db->updateArray('sys_users_data',$values,$fields,$_user['id'],'user_id'))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			
			if(!$_err)
			{	
				if(!$_db->updateArray('sys_users_cfg',$values,$fields,$_user['id'],'user_id'))
					$_err=sys::translate('sys::db_error').$_db->err;
					
			}
			
			foreach ($values as $key=>$val)
			{
				if(strpos($key,'date')!==false)
					$values[$key]=sys::db2Date($values[$key],false,$_cfg['sys::date_format']);
			}
			
			if($values['only_from_ip'])
				$values['only_from_ip']=long2ip($values['only_from_ip']);
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$panel['class']='blank';
	$panel['text']=sys_users::showProfileTabs('info');
	$result['panels'][]=$panel;
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>