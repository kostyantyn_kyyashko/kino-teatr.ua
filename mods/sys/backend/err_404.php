<?
function sys_err_404()
{
	sys::setTitle(sys::translate('sys::err_404'));
	$result['panels'][0]['text']=sys_gui::showNavPath('sys::err_404');
	$result['msg']=sys::translate('sys::err_404_text');	
	sys::jsAlert(sys::translate('sys::err_404_text'));
	return $result;
}
?>