<?
function sys_mod_action_edit()
{
	if(!sys::checkAccess('sys::mod_edit'))
		return 403;
		
	global $_db, $_err;
		
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::mods');
	
	sys::filterGet('id','int');
	sys::filterGet('mod_id','int');
	sys::filterGet('parent_id','int',0);
	sys::filterGet('mode','text','frontend');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='sys_mods_actions';
	$default['mod_id']=$_GET['mod_id'];
	$default['mode']=$_GET['mode'];
	$default['parent_id']=$_GET['parent_id'];
	$default['order_number']=$_db->getMaxOrderNumber($tbl,"`mod_id`=".intval($_GET['mod_id'])." AND `parent_id`=".intval($_GET['parent_id'])." AND `mode`='".mysql_real_escape_string($_GET['mode'])."'");
	
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		if(!$_GET['mode'] || !$_GET['mod_id'])
			return 404;
		$title=sys::translate('sys::new_action');
	}
	
	$values=sys::setValues($record, $default);
	$mod_name=$_db->getValue('sys_mods','name',intval($values['mod_id']));
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//mod_id
	$fields['mod_id']['input']='hidden';
	$fields['mod_id']['type']='int';
	$fields['mod_id']['req']=true;
	$fields['mod_id']['table']=$tbl;
	
	//mode
	$fields['mode']['input']='hidden';
	$fields['mode']['type']='name';
	$fields['mode']['req']=true;
	$fields['mode']['table']=$tbl;
	
	//parent_id
	$fields['parent_id']['input']='framebox';
	$fields['parent_id']['type']='int';
	if(!$fields['parent_id']['text']=$_db->getValue('sys_mods_actions','name',$values['parent_id']))
		$fields['parent_id']['text']=sys::translate('sys::root');
	else 
		$fields['parent_id']['text']=$mod_name.'::'.$fields['parent_id']['text'];
	$fields['parent_id']['frame']='?mod=sys&act=mod_actions_tree&task=setValue&mod_id='.$values['mod_id'].'&mode='.$values['mode'];
	$fields['parent_id']['frame::width']=200;
	$fields['parent_id']['frame::height']=200;
	$fields['parent_id']['table']=$tbl;
	$fields['parent_id']['title']=sys::translate('sys::parent');
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='name';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['max_chars']=255;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';
	$fields['name']['unique']=true;
	$fields['name']['unique::where']="`id`!=".intval($_GET['id'])." AND `mode`='".mysql_real_escape_string($values['mode'])."' AND `mod_id`=".intval($values['mod_id'])."";
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!sys_mods::checkActionParent($_GET['id'],$values['parent_id']))
					$_err=sys::translate('sys::invalid_parent_node');
				if(!$_err)
				{
					if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
						$_err=sys::translate('sys::db_error').$_db->err;
				}
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>