<?
function sys_profile_pass()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::users');
	
	$_GET['id']=$_user['id'];
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$nav_point='sys::my_profile';
	$values=sys::setValues();

	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//old_password
	$fields['old_password']['input']='password';
	$fields['old_password']['type']='text';
	$fields['old_password']['req']=true;
	$fields['old_password']['html_params']='size="16" maxlength="16"';
	
	//new_password
	$fields['new_password']['input']='password';
	$fields['new_password']['type']='text';
	$fields['new_password']['req']=true;
	$fields['new_password']['min_chars']=6;
	$fields['new_password']['max_chars']=16;
	$fields['new_password']['html_params']='size="16" maxlength="16"';
	
	//new_password2
	$fields['new_password2']['input']='password';
	$fields['new_password2']['type']='text';
	$fields['new_password2']['req']=true;
	$fields['new_password2']['min_chars']=6;
	$fields['new_password2']['max_chars']=16;
	$fields['new_password2']['html_params']='size="16" maxlength="16"';
	$fields['new_password2']['title']=sys::translate('sys::new_pasword_again');
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			if(md5($values['old_password'])!=$_user['password'])
				$_err=sys::translate('sys::incorrect_old_password');
			if(!$_err)
			{
				if($values['new_password']!=$values['new_password2'])
					$_err=sys::translate('sys::passwords_mismatch');
				else 
				{
					$_db->setValue('sys_users','password',md5($values['new_password']),$_user['id']);
					$_SESSION['sys::user_password']=md5($values['new_password']);
				}
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$panel['class']='blank';
	$panel['text']=sys_users::showProfileTabs('pass');
	$result['panels'][]=$panel;
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>