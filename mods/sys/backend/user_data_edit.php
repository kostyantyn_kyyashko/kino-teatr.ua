<?
function sys_user_data_edit()
{
	if(!sys::checkAccess('sys::user_edit'))
		return 403;
		
	global $_db, $_cfg, $_err;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::users');
	
	sys::filterGet('id','int');
	
	if(!$_GET['id'])
		return 404;
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='sys_users_data';
	$default=array();
	
	
	$record=sys_users::getData($_GET['id']);
	
	$title=$_db->getValue('sys_users','login',$_GET['id']).' :: '.sys::translate('sys::data');
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	
	//Поля----------------------------------------------------------------------------------------------
	$fields=sys_users::getDataFields();
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			foreach ($values as $key=>$val)
			{
				if(strpos($key,'date')!==false)
					$values[$key]=sys::date2Db($values[$key],false,$_cfg['sys::date_format']);
			}
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields,true,'user_id'))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id'],false,'user_id'))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			foreach ($values as $key=>$val)
			{
				if(strpos($key,'date')!==false)
					$values[$key]=sys::db2Date($values[$key],false,$_cfg['sys::date_format']);
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=sys_users::showUserTabs($_GET['id'],'data');
	$result['panels'][]=$panel;
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>