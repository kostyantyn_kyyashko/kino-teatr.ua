<?
function sys_fileman_dir_add()
{
	if(!sys::checkAccess('sys::make_folders'))
		return 403;
		
	global $_db, $_err;
		
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::fileman');
	
	if(!isset($_GET['dir']))
		return 404;
	$dir=base64_decode($_GET['dir']);
	$title=sys::translate('sys::new_folder');
	
	if(!sys::checkPublicPath($dir))
		return 401;
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
	
	$values=sys::setValues();
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='name';
	$fields['name']['req']=true;
	$fields['name']['max_chars']=255;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			if(!mkdir($dir.$values['name']))
				$_err=sys::translate('sys::error');	
			else 
				chmod($dir.$values['name'], 0755);
		
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>