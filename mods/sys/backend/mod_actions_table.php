<?
function sys_mod_actions_table()
{
	if(!sys::checkAccess('sys::mod_edit'))
		return 403;
		
	global $_db, $_err;
	
	sys::useLib('sys::pages');
	sys::useLib('sys::mods');
	
	sys::filterGet('order_by','text','order_number.asc');
	sys::filterGet('parent_id','int',0);
	sys::filterGet('mode');
	sys::filterGet('mod_id','int');
		
	if(!$_GET['mode'] || ($_GET['mode']!='frontend') && $_GET['mode']!='backend')
		$_GET['mode']='frontend';
		
	if(!$_GET['mod_id'])
		return 404;
		
	$tbl='sys_mods_actions';
	$edit_url='?mod=sys&act=mod_action_edit';
	$win_params=sys::setWinParams('700','500');
	$mod_name=$_db->getValue('sys_mods','name',$_GET['mod_id']);
	$add_title=sys::translate('sys::add_action');
	$order_where=$where="`parent_id`=".$_GET['parent_id']."
		AND `mod_id`=".intval($_GET['mod_id'])." 
		AND `mode`='".$_GET['mode']."' ";
	$new_parent_id=false;
	if(!$_GET['parent_id'])
	{
		$parent=false;
		$title=sys::translate('sys::actions');
	}
	else
	{ 
		$parent=$_db->getRecord($tbl,$_GET['parent_id']);
		$title=$parent['name'].' :: '.sys::translate('test::actions');
	}
	
	$q="SELECT 
			`id`, 
			`name`, 
			`mod_id`,
			`mode`,
			`order_number`  
		FROM `#__".$tbl."`WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
		{
			if($task[1]==$_GET['parent_id'])
				$new_parent_id=$_db->getValue($tbl,'parent_id',$task[1]);
			sys_mods::deleteAction($task[1]);
		}
		
		if($task[0]=='move')
		{
			sys_mods::moveAction($task[1],$task[2]);
			sys_gui::reloadTreeBranch('window.parent.parent.fra_left',$task[2],sys_mods::getActionsTreeBranch($task[2]),$_GET['parent_id']);
			if($task[1]==$_GET['parent_id'])
				$new_parent_id=$_db->getValue($tbl,'parent_id',$task[1]);
		}
			
		//Сдвиг вверх
		if($task[0]=='moveUp')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveDown($tbl,$task[1],$order_where);
			else
				$_db->moveUp($tbl,$task[1],$order_where);
		}
		
		//Сдвиг вниз
		if($task[0]=='moveDown')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveUp($tbl,$task[1],$order_where);
			else
				$_db->moveDown($tbl,$task[1],$order_where);
		}
		
		if($task[0]=='moveChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					if($key==$_GET['parent_id'])
						$new_parent_id=$_db->getValue($tbl,'parent_id',$key);
					sys_mods::moveAction($key,$task[1]);
					sys_gui::reloadTreeBranch('window.parent.parent.fra_left',$task[1],sys_mods::getActionsTreeBranch($task[1]));
				}
			}
		}
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					if($key==$_GET['parent_id'])
						$new_parent_id=$_db->getValue($tbl,'parent_id',$key);
					sys_mods::deleteAction($key);
				}
			}
					
		}
		
		//Упорядочение нумерации
		if($_POST['_task']=='orderNumbers')
			$_db->orderNumbers($tbl, $order_where);
		
		//Если удаляетя родительский раздел
		if($new_parent_id)
		{
			sys_gui::reloadWindow('window','?mod=sys&act=mod_actions_table&parent_id='.$new_parent_id);
			sys_gui::reloadTreeBranch('window.parent.parent.fra_left',$new_parent_id,sys_mods::getActionsTreeBranch($new_parent_id),$new_parent_id);
		}
			
	}
	//================================================================================================//
	
	if(!$new_parent_id)
	{
		//Обновить дерево для выделения текущего узла
		sys_gui::reloadTreeBranch('window.parent.parent.fra_left',$_GET['parent_id'],sys_mods::getActionsTreeBranch($_GET['parent_id']),$_GET['parent_id']);
	}
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	if($parent)
	{
		$table['records'][$parent['id']]=$parent;
		$table['records'][$parent['id']]['name']='..'.$parent['name'];
		$table['params'][$parent['id']]['name']['bold']=true;
		$table['params'][$parent['id']]['name']['onclick']="setMsg('".sys::translate('sys::loading')."'); window.location='?mod=sys&act=mod_actions_table&mode=".$parent['mode']."&mod_id=".$parent['mod_id']."&parent_id=".$parent['parent_id']."';return false;";
		$table['params'][$parent['id']]['name']['url']='?mod=sys&act=mod_actions_table&mode='.$parent['mode'].'&mod_id='.$parent['mod_id'].'&parent_id='.$parent['parent_id'];
		$table['params'][$parent['id']]['order_number']['html_params']='size="8" disabled ';
		$table['params'][$parent['id']]['move_up']['type']='text';
		$table['params'][$parent['id']]['move_down']['type']='text';
		$table['records'][$parent['id']]['title']=sys::translate($mod_name.'::'.$parent['name']);
	}
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['name']=$obj['name'];
		$obj['title']=sys::translate($mod_name.'::'.$obj['name']);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['move_up']['type']='button';
	$table['fields']['move_up']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveUp.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_up']['image']='sys::btn.desc.gif';
	
	$table['fields']['move_down']['type']='button';
	$table['fields']['move_down']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveDown.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_down']['image']='sys::btn.asc.gif';
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['move']['type']='button';
	$table['fields']['move']['frame']='?mod=sys&act=mod_actions_tree&task=move&action_id=%id%&mode='.$_GET['mode'].'&mod_id='.$_GET['mod_id'];
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;
	$table['fields']['name']['onclick']="setMsg('".sys::translate('sys::loading')."'); window.location='?mod=sys&act=mod_actions_table&parent_id=%id%&mode=%mode%&mod_id=%mod_id%';return false";
	$table['fields']['name']['url']='?mod=sys&act=actions_table&parent_id=%id%&mode='.$_GET['mode'].'&mod_id='.$_GET['mod_id'].'';
	
	$table['fields']['title']['type']='text';
	
	$table['fields']['order_number']['type']='textbox';
	$table['fields']['order_number']['order']=true;
	$table['fields']['order_number']['html_params']='size="8" onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=set~'.$tbl.'~order_number~%id%~\'+this.value+\'~int\')"';
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&mod_id=".$_GET['mod_id']."&mode=".$_GET['mode']."&parent_id=".$_GET['parent_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';
	
	$buttons['move']['frame']='?mod=sys&act=mod_actions_tree&task=moveChecked&mode='.$_GET['mode'].'&mod_id='.$_GET['mod_id'].'&field=_task';
	$buttons['move']['title']=sys::translate('sys::move_checked');
	
	//=======================================================================================================//
	
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle($title);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}

?>
