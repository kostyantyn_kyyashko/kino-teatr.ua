<?
function sys_config_edit()
{
	if(!sys::checkAccess('sys::configuration'))
		return 401;
	global $_err;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	
	$result=array();
	clearstatcache();
	if(!is_writable(_ROOT_DIR.'cfg.ini'))
	{
		$is_writable=false;
		$_err=sys::translate('sys::config_file_is_not_writable');
	}
	else
		$is_writable=true;
		
	$ini=parse_ini_file(_ROOT_DIR.'cfg.ini');
	$values=sys::setValues($ini);
	
	//project_id
	$fields['project_id']['input']='textbox';
	$fields['project_id']['type']='name';
	$fields['project_id']['req']=true;
	$fields['project_id']['min_chars']=3;
	$fields['project_id']['html_params']='style="width:100%" maxlength="55"';
	
	//root_dir
	$fields['root_dir']['input']='textbox';
	$fields['root_dir']['type']='text';
	$fields['root_dir']['req']=true;
	$fields['root_dir']['html_params']='style="width:100%" maxlength="55"';
	
	//root_url
	$fields['root_url']['input']='textbox';
	$fields['root_url']['type']='text';
	$fields['root_url']['req']=true;
	$fields['root_url']['html_params']='style="width:100%" maxlength="55"';
	
	//secure_dir
	$fields['secure_dir']['input']='textbox';
	$fields['secure_dir']['type']='text';
	$fields['secure_dir']['req']=true;
	$fields['secure_dir']['html_params']='size="55" maxlength="255"';
	$fields['secure_dir']['info']=sys::getHlp('sys::secure_dir');
	
	//db_host
	$fields['db_host']['input']='textbox';
	$fields['db_host']['type']='name';
	$fields['db_host']['req']=true;
	$fields['db_host']['html_params']='style="width:100%" maxlength="55"';
	
	//db_user
	$fields['db_user']['input']='textbox';
	$fields['db_user']['type']='name';
	$fields['db_user']['req']=true;
	$fields['db_user']['html_params']='style="width:100%" maxlength="55"';
	
	//db_name
	$fields['db_name']['input']='textbox';
	$fields['db_name']['type']='name';
	$fields['db_name']['req']=true;
	$fields['db_name']['html_params']='style="width:100%" maxlength="55"';
	
	//db_pass
	$fields['db_pass']['input']='textbox';
	$fields['db_pass']['type']='text';
	$fields['db_pass']['req']=true;
	$fields['db_pass']['html_params']='style="width:100%" maxlength="55"';
	
	//db_prefix
	$fields['db_prefix']['input']='textbox';
	$fields['db_prefix']['type']='name';
	$fields['db_prefix']['req']=true;
	$fields['db_prefix']['html_params']='style="width:100%" maxlength="55"';
	
	//entry_limit
	$fields['entry_limit']['input']='textbox';
	$fields['entry_limit']['type']='int';
	$fields['entry_limit']['req']=true;
	$fields['entry_limit']['html_params']='size="2" maxlength="2"';
	$fields['entry_limit']['info']=sys::getHlp('sys::entry_limit');
	
	//max_off_time
	$fields['max_off_time']['input']='textbox';
	$fields['max_off_time']['type']='int';
	$fields['max_off_time']['req']=true;
	$fields['max_off_time']['html_params']='size="8" maxlength="8"';
	$fields['max_off_time']['info']=sys::getHlp('sys::max_off_time');
	$fields['max_off_time']['extra']=sys::translate('sys::seconds');

	//from_email
	$fields['from_email']['input']='textbox';
	$fields['from_email']['type']='email';
	$fields['from_email']['req']=true;
	$fields['from_email']['html_params']='size="55" maxlength="255"';
	$fields['from_email']['info']=sys::getHlp('sys::from_email');
	
	//cache_seconds
	$fields['cache_seconds']['input']='textbox';
	$fields['cache_seconds']['type']='int';
	$fields['cache_seconds']['req']=true;
	$fields['cache_seconds']['html_params']='size="32" maxlength="32"';
	$fields['cache_seconds']['info']=sys::getHlp('sys::cache_seconds');
	$fields['cache_seconds']['extra']=sys::translate('sys::seconds');
	
	//download_speed
	$fields['download_speed']['input']='textbox';
	$fields['download_speed']['type']='int';
	$fields['download_speed']['req']=true;
	$fields['download_speed']['html_params']='size="32" maxlength="32"';
	$fields['download_speed']['info']=sys::getHlp('sys::download_speed');
	
	//images_quality
	$fields['images_quality']['input']='textbox';
	$fields['images_quality']['type']='int';
	$fields['images_quality']['req']=true;
	$fields['images_quality']['max_value']=100;
	$fields['images_quality']['min_value']=1;
	$fields['images_quality']['html_params']='size="3" maxlength="3"';
	$fields['images_quality']['info']=sys::getHlp('sys::images_quality');
	
	//session_minutes
	$fields['session_minutes']['input']='textbox';
	$fields['session_minutes']['type']='int';
	$fields['session_minutes']['req']=true;
	$fields['session_minutes']['html_params']='size="3" maxlength="3"';
	$fields['session_minutes']['info']=sys::getHlp('sys::session_minutes');
	$fields['session_minutes']['extra']=sys::translate('sys::minutes');
	
	//cookie_days
	$fields['cookie_days']['input']='textbox';
	$fields['cookie_days']['type']='int';
	$fields['cookie_days']['req']=true;
	$fields['cookie_days']['html_params']='size="3" maxlength="3"';
	$fields['cookie_days']['extra']=sys::translate('sys::days');
	
	//num_of_pages
	$fields['num_of_pages']['input']='textbox';
	$fields['num_of_pages']['type']='int';
	$fields['num_of_pages']['req']=true;
	$fields['num_of_pages']['html_params']='size="2" maxlength="2"';
	$fields['num_of_pages']['info']=sys::getHlp('sys::num_of_pages');
	
	//on_page
	$fields['on_page']['input']='selectbox';
	$fields['on_page']['type']='int';	
	$fields['on_page']['values'][5]=5;
	$fields['on_page']['values'][10]=10;
	$fields['on_page']['values'][20]=20;
	$fields['on_page']['values'][30]=30;
	$fields['on_page']['values'][40]=40;	
	$fields['on_page']['values'][50]=50;
	$fields['on_page']['values'][60]=60;
	$fields['on_page']['values'][70]=70;
	$fields['on_page']['values'][80]=80;	
	$fields['on_page']['values'][90]=90;	
	$fields['on_page']['values'][100]=100;
	$fields['on_page']['title']=sys::translate('sys::records_on_page');
	$fields['on_page']['info']=sys::getHlp('sys::on_page');
	
	//timezone
	$fields['timezone']['input']='selectbox';
	$fields['timezone']['type']='text';
	$timezones=timezone_identifiers_list();
	foreach ($timezones as $zone)
	{
		$fields['timezone']['values'][$zone]=$zone;
	}
	$fields['timezone']['info']=sys::getHlp('sys::timezone');
	
	//frontend_skin
	$fields['frontend_skin']['input']='selectbox';
	$fields['frontend_skin']['type']='text';
	$dir=_MODS_DIR.'sys/skins/';
	$handle=opendir($dir);
	while (false !== ($file = readdir($handle))) 
	{ 
		if(is_dir($dir.$file) && $file!='.' && $file!='..')
        	$fields['frontend_skin']['values'][$file]=$file;
    }
	closedir($handle);
	
	//frontend_skin
	$fields['backend_skin']['input']='selectbox';
	$fields['backend_skin']['type']='text';
	$dir=_MODS_DIR.'sys/backend/skins/';
	$handle=opendir($dir);
	while (false !== ($file = readdir($handle))) 
	{ 
		if(is_dir($dir.$file) && $file!='.' && $file!='..')
        	$fields['backend_skin']['values'][$file]=$file;
    }
	closedir($handle);

	//error_reporting
	$fields['error_reporting']['input']='selectbox';
	$fields['error_reporting']['type']='text';
	$fields['error_reporting']['values'][0]='0';
	$fields['error_reporting']['values'][7]='E_ERROR | E_WARNING | E_PARSE';
	$fields['error_reporting']['values'][15]='E_ERROR | E_WARNING | E_PARSE | E_NOTICE';
	$fields['error_reporting']['values']['E_ALL ^ E_NOTICE']='E_ALL ^ E_NOTICE';
	$fields['error_reporting']['values'][6143]='E_ALL';
	
	//log_sql
	$fields['log_sql']['input']='selectbox';
	$fields['log_sql']['type']='text';
	$fields['log_sql']['values']['off']=sys::translate('sys::is_off');
	$fields['log_sql']['values']['changes']=sys::translate('sys::changes');
	$fields['log_sql']['values']['all']=sys::translate('sys::all');
	$fields['log_sql']['info']=sys::getHlp('sys::log_sql');
	
	//session_in_db
	$fields['session_in_db']['input']='checkbox';
	$fields['session_in_db']['type']='bool';
	
	//debugger
	$fields['debugger']['input']='checkbox';
	$fields['debugger']['type']='bool';
	$fields['debugger']['info']=sys::getHlp('sys::debugger');
	
	//hurl
	$fields['hurl']['input']='checkbox';
	$fields['hurl']['type']='bool';
	
	//debug_mode
	$fields['debug_mode']['input']='checkbox';
	$fields['debug_mode']['type']='bool';
	$fields['debug_mode']['info']=sys::getHlp('sys::debug_mode');
	
	//timer
	$fields['timer']['input']='checkbox';
	$fields['timer']['type']='bool';
	$fields['timer']['info']=sys::getHlp('sys::timer');
	
	//delivery debug
	$fields['delivery_debug']['input']='checkbox';
	$fields['delivery_debug']['type']='bool';
	$fields['delivery_debug']['title']=sys::translate('sys::delivery_debug');
    
	//delivery debug users
	$fields['delivery_debug_users']['input']='textbox';
	$fields['delivery_debug_users']['type']='text';
	
	//memory_usage
	$fields['memory_usage']['input']='checkbox';
	$fields['memory_usage']['type']='bool';
	$fields['memory_usage']['title']=sys::translate('sys::memory_counter');
	
	//log_session
	$fields['log_session']['input']='checkbox';
	$fields['log_session']['type']='bool';
	
	//log_entry
	$fields['log_entry']['input']='checkbox';
	$fields['log_entry']['type']='bool';
	
	//log_attack
	$fields['log_attack']['input']='checkbox';
	$fields['log_attack']['type']='bool';
	
	//log_sql_error
	$fields['log_sql_error']['input']='checkbox';
	$fields['log_sql_error']['type']='bool';
	
	//closed
	$fields['closed']['input']='checkbox';
	$fields['closed']['type']='bool';
	$fields['closed']['title']=sys::translate('sys::site_closed');
	//======================================================================
	
	//Проверка и сохранение данных---------------------------------------------
	if(isset($_POST['_task']))
	{
		if($_POST['_task']=='save')
		{	
			if(!$_err=sys_check::checkValues($fields,$values))
			{
				$text='';
				foreach($ini as $key=>$val)
				{
					if(!isset($values[$key]))
						$values[$key]='off';
					$text.=$key.'='.$values[$key]."\n";		
				}
				if(!sys::writeToFile(_ROOT_DIR.'cfg.ini',$text,'w'))
					$_err=sys::translate('sys::file_write_error');
			}
		}	
		if(!$_err)
			sys::redirect('?mod=sys&act=config_edit',false);
	}
	//============================================================================
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирвание результата----------------------------------------
	sys::setTitle(sys::translate('sys::configuration'));
	$result['panels'][]['text']=sys_gui::showNavPath('sys::configuration');
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//==============================================================
	return $result;
}
?>
