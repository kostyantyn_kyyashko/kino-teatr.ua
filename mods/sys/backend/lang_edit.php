<?
function sys_lang_edit()
{
	if(!sys::checkAccess('sys::lang_edit'))
		return 403;
		
	global $_db, $_err, $_charsets;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::langs');
	
	sys::filterGet('id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='sys_langs';
	$default=array();
	$default['order_number']=$_db->getMaxOrderNumber($tbl);
	$default['on']=true;
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$title=$record['title'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('sys::new_lang');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//name
	$fields['code']['input']='textbox';
	$fields['code']['type']='text';
	$fields['code']['req']=true;
	$fields['code']['table']=$tbl;
	$fields['code']['max_chars']=10;
	$fields['code']['unique']=true;
	$fields['code']['unique::where']=$_GET['id'];
	$fields['code']['html_params']='style="width:100%" maxlength="10"';
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="55"';
	
	//short_title
	$fields['short_title']['input']='textbox';
	$fields['short_title']['type']='text';
	$fields['short_title']['req']=true;
	$fields['short_title']['table']=$tbl;
	$fields['short_title']['html_params']='style="width:100%" maxlength="5"';
	
	//locale
	$fields['locale']['input']='textbox';
	$fields['locale']['type']='text';
	$fields['locale']['req']=true;
	$fields['locale']['table']=$tbl;
	$fields['locale']['html_params']='style="width:100%" maxlength="255"';
	
	//mail_charset
	$fields['mail_charset']['input']="selectbox";
	$fields['mail_charset']['type']='text';
	$fields['mail_charset']['req']=true;
	$fields['mail_charset']['table']=$tbl;
	$fields['mail_charset']['values']=$_charsets;
	
	//project_title
	$fields['project_title']['input']='textbox';
	$fields['project_title']['type']='text';
	$fields['project_title']['req']=true;
	$fields['project_title']['table']=$tbl;
	$fields['project_title']['html_params']='style="width:100%" maxlength="55"';
	
	//robot_title
	$fields['robot_title']['input']='textbox';
	$fields['robot_title']['type']='text';
	$fields['robot_title']['req']=true;
	$fields['robot_title']['table']=$tbl;
	$fields['robot_title']['html_params']='style="width:100%" maxlength="55"';
	
	//meta_title
	$fields['meta_title']['input']='textbox';
	$fields['meta_title']['type']='text';
	$fields['meta_title']['table']=$tbl;
	$fields['meta_title']['html_params']='style="width:100%" maxlength="255"';
	
	//meta_keywords
	$fields['meta_keywords']['input']='textarea';
	$fields['meta_keywords']['type']='text';
	$fields['meta_keywords']['table']=$tbl;
	$fields['meta_keywords']['html_params']='style="width:100%; height:50px"';
	
	//meta_description
	$fields['meta_description']['input']='textarea';
	$fields['meta_description']['type']='text';
	$fields['meta_description']['table']=$tbl;
	$fields['meta_description']['html_params']='style="width:100%; height:50px"';
	
	//meta_other
	$fields['meta_other']['input']='textarea';
	$fields['meta_other']['type']='html';
	$fields['meta_other']['table']=$tbl;
	$fields['meta_other']['html_params']='style="width:100%; height:50px"';
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	//on
	$fields['on']['input']='checkbox';
	$fields['on']['type']='bool';
	$fields['on']['title']=sys::translate('sys::is_on');
	$fields['on']['table']=$tbl;
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>