<?
function sys_navigator()
{

	sys::filterGet('parent_id','int',0);

	$tree['objects']=sys_gui::getNavigatorBranch($_GET['parent_id']);

//global $_rights;
//if(sys::isDebugIP()) sys::printR($_rights);

	if($_GET['parent_id'])
		sys_gui::mergeTree($_GET['parent_id'],$tree['objects']);
	else
	{
		//Заголовок
		$result['panels'][0]['text']=sys::translate('sys::navigator');
		$result['panels'][0]['class']='title';

		//Кнпки
		$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";

		$result['panels'][1]['text']=sys_gui::showButtons($buttons);

		//Дерево
		$tree['root']['title']=sys::translate('sys::desktop');
		$tree['root']['onclick']="setMsg('".sys::translate('sys::loading')."','window.parent.fra_right'); window.parent.fra_right.location='?mod=sys&act=desktop'; window.parent.fra_right.focus();";
		$tree['root']['url']='?mod=sys&act=desktop';
		$tree['parent_id']=0;
		$result['main']=sys::parseTpl('sys::tree',$tree);
	}
	if(isset($result))
		return $result;
}

?>