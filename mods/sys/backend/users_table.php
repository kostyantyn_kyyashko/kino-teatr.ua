<?
function sys_users_table()
{
	if(!sys::checkAccess('sys::users'))
		return 403;
		
	global $_db, $_cfg, $_err;
	
	sys::useLib('sys::pages');
	sys::useLib('sys::users');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.desc');
	sys::filterGet('group_id','int');
	sys::filterGet('keywords');
	
	$tbl='sys_users';
	$edit_url='?mod=sys&act=user_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('sys::add_user');
	$where=" 1=1 ";
	if($_GET['keywords'])
		$where="usr.login LIKE '".$_GET['keywords']."%'";
	if($_GET['group_id'])
		$where="grp.group_id=".intval($_GET['group_id'])."";
	$nav_point='sys::users';
	$q="SELECT 
			usr.id, 
			usr.login,
			usr.date_reg,
			usr.email,
			usr.on,
			usr.date_last_visit,
			usr.date_on
		FROM `#__".$tbl."` AS `usr` 
		";
	if($_GET['group_id'])
	{
		$q.="
			LEFT JOIN `#__sys_users_groups` AS `grp`
			ON usr.id=grp.user_id
			AND grp.group_id=".intval($_GET['group_id'])."
		";
	}
	$q.="
		WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			sys_users::deleteUser($task[1]);
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					sys_users::deleteUser($key);
				}
			}
					
		}	
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	
	$table['params'][7]['on']['html_params']='disabled';
	$table['params'][7]['date_on']['html_params']='size="22" disabled';
	$table['params'][7]['delete']['type']='text';
	$table['params'][2]['on']['html_params']='disabled';
	$table['params'][2]['date_on']['html_params']='size="22" disabled';
	$table['params'][2]['delete']['type']='text';
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date_reg']=sys::db2Date($obj['date_reg'],true,$_cfg['sys::date_format']);
		$obj['date_on']=sys::db2Date($obj['date_on'],true);
		$obj['date_last_visit']=sys::db2Date($obj['date_last_visit'],true,$_cfg['sys::date_format'].' %H:%M');
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['data']['type']='button';
	$table['fields']['data']['onclick']="window.open('?mod=sys&act=user_data_edit&id=%id%','','".$win_params."');return false";
	$table['fields']['data']['url']='?mod=sys&act=user_data&user_id=%id%';
	$table['fields']['data']['image']='sys::btn.user_data.gif';
	
	$table['fields']['cfg']['type']='button';
	$table['fields']['cfg']['onclick']="window.open('?mod=sys&act=user_cfg_edit&id=%id%','','".$win_params."');return false";
	$table['fields']['cfg']['url']='?mod=sys&act=user_cfg_edit&user_id=%id%';
	$table['fields']['cfg']['image']='sys::btn.user_set.gif';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['login']['type']='text';
	$table['fields']['login']['order']=true;
	
	$table['fields']['date_reg']['type']='text';
	$table['fields']['date_reg']['order']=true;
	
	$table['fields']['date_last_visit']['type']='text';
	$table['fields']['date_last_visit']['order']=true;
	$table['fields']['date_last_visit']['title']=sys::translate('sys::last_visit');
	$table['fields']['date_last_visit']['width']=120;
	
	$table['fields']['email']['type']='text';
	$table['fields']['email']['url']='mailto:%email%';
	
	
	$table['fields']['on']['type']='checkbox';
	$table['fields']['on']['title']=sys::translate('sys::is_on');
	$table['fields']['on']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~on~%id%~%on%\')"';
	$table['fields']['on']['width']=30;
	$table['fields']['on']['order']=true;
	
	$table['fields']['date_on']['type']='textbox';
	$table['fields']['date_on']['html_params']='size="22" onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=set~'.$tbl.'~date_on~%id%~\'+this.value+\'~date\')"';
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Селектор групп------------------------------------------------
	$group['input']='selectbox';
	$group['values']=$_db->getValues('sys_groups','name',false, "ORDER BY `name` DESC");
	$group['values'][0]=sys::translate('sys::all_groups');
	$group['values']=array_reverse($group['values'],true);
	$group['value']=$_GET['group_id'];
	$group['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\');window.location=\'?mod=sys&act=users_table&group_id=\'+this.value"';
	$group['title']=sys::translate('sys::group');
	//==============================================================
	
	//Поиск------------------------------------------------
	$search['action']='?mod=sys&act=users_table';
	$search['html_params']='style="width:150px" maxlenght="255"';
	//======================================================
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle(sys::translate($nav_point));
	
	$result['panels'][]['text']=sys_gui::showNavPath('sys::users');
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSpacer().sys_gui::showSearchForm($search).sys_gui::showSpacer().sys_form::parseField('group_id',$group);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
