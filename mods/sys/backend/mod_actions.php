<?
function sys_mod_actions()
{
	if(!sys::checkAccess('sys::mod_edit'))
		return 403;
		
	global $_db, $_err;
	sys::useLib('sys::mods');
	
	sys::filterGet('mode');
	
	if(!$_GET['mode'] || ($_GET['mode']!='frontend') && $_GET['mode']!='backend')
		$_GET['mode']='frontend';
	
	$title=$_db->getValue('sys_mods','name',intval($_GET['mod_id']));
	if(!$title)
		return 404;
	$title.=' :: '.sys::translate('sys::actions');
		
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=sys_mods::showTabs($_GET['mod_id'],'actions');
	$result['panels'][]=$panel;	
	unset($panel);
	
	$result['left']['url']='?mod=sys&act=mod_actions_tree&mode='.$_GET['mode'].'&mod_id='.$_GET['mod_id'];
	$result['right']['url']='?mod=sys&act=mod_actions_table&mode='.$_GET['mode'].'&parent_id=0&mod_id='.$_GET['mod_id'];
	return $result;
}
?>