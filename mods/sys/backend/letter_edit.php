<?
function sys_letter_edit()
{
	if(!sys::checkAccess('sys::letter_edit'))
		return 403;
		
	global $_db, $_err, $_charsets, $_cfg;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::letters');
	
	sys::filterGet('id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='sys_letters';
	$default=array();
	$default['date']=strftime($_cfg['sys::date_time_format']);
	
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$title=$record['subject'];
		$record['date']=sys::db2Date($record['date'],true);
	}
	else 
	{
		$record=false;
		$title=sys::translate('sys::new_letter');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	//Поля и форма---------------------------------------------------------------------------------------------
	
	//from_email
	$fields['from_email']['input']='textbox';
	$fields['from_email']['type']='email';
	$fields['from_email']['table']=$tbl;
	$fields['from_email']['html_params']='style="width:100%" maxlength="255"';
	
	//from_name
	$fields['from_name']['input']='textbox';
	$fields['from_name']['type']='text';
	$fields['from_name']['table']=$tbl;
	$fields['from_name']['html_params']='style="width:100%" maxlength="255"';
	
	//to_email
	$fields['to_email']['input']='textbox';
	$fields['to_email']['type']='email';
	$fields['to_email']['table']=$tbl;
	$fields['to_email']['req']=true;
	$fields['to_email']['html_params']='style="width:100%" maxlength="255"';
	
	//to_name
	$fields['to_name']['input']='textbox';
	$fields['to_name']['type']='text';
	$fields['to_name']['table']=$tbl;
	$fields['to_name']['req']=true;
	$fields['to_name']['html_params']='style="width:100%" maxlength="255"';
	
	//subject
	$fields['subject']['input']='textbox';
	$fields['subject']['type']='text';
	$fields['subject']['req']=true;
	$fields['subject']['table']=$tbl;
	$fields['subject']['html_params']='style="width:100%" maxlength="55"';
	
	//text
	$fields['text']['input']='textarea';
	$fields['text']['type']='text';
	$fields['text']['table']=$tbl;
	$fields['text']['html_params']='style="width:100%; height:150px"';
	
	//html
	$fields['html']['input']='textarea';
	$fields['html']['type']='text';
	$fields['html']['table']=$tbl;
	$fields['html']['html_params']='style="width:100%; height:150px"';
	
	//att_files
	$fields['att_files']['input']='textarea';
	$fields['att_files']['type']='text';
	$fields['att_files']['table']=$tbl;
	$fields['att_files']['html_params']='style="width:100%; height:50px"';
	
	//charset
	$fields['charset']['input']="selectbox";
	$fields['charset']['type']='text';
	$fields['charset']['req']=true;
	$fields['charset']['table']=$tbl;
	$fields['charset']['values']=$_charsets;
	
	//date
	$fields['date']['input']='datebox';
	$fields['date']['type']='date';
	$fields['date']['req']=true;
	$fields['date']['table']=$tbl;
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['date']=sys::date2Db($values['date'],true);
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=sys::addMail($values['from_email'],$values['from_name'],$values['to_email'],$values['to_name'],$values['subject'],$values['text'],$values['html'],$values['att_files'],$values['charset'],$values['date']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['date']=sys::db2Date($values['date'],true);
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	
	if($_POST['_task']=='send')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			if(!sys::sendMail($values['from_email'],$values['from_name'],$values['to_email'],$values['to_name'],$values['subject'],$values['text'],$values['html'],$values['att_files'],$values['charset']))
				$_err=sys::translate('sys::sending_error');
			elseif($_GET['id'])
			{
				sys_letters::deleteLetter($_GET['id']);
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
			sys_gui::windowClose();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	$buttons['send']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::sending')."');document.forms['form'].elements['_task'].value='send'; document.forms['form'].submit();} return false;";	
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>