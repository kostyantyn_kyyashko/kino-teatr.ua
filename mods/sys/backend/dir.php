<?
function sys_dir()
{
	sys::useLib('sys::form');
	sys::useLib('sys::fileman');
	
	$result=array();
	
	sys::filterGet('keywords');
	
	$keywords=str_replace("\\",'',$_GET['keywords']);
	$_GET['keywords']=str_replace("/",'',$_GET['keywords']);
	$_GET['keywords']=str_replace(".",'',$_GET['keywords']);
	
	if($_GET['keywords'] && strpos($_GET['keywords'],'*')===false)
		$_GET['keywords']=$_GET['keywords'].'*';
	
	
	if(!isset($_GET['dir']))
		return 404;
		
	$dir=base64_decode($_GET['dir']);
	
	if(strpos($dir,_PRIVATE_DIR)===0)
	{
		$root_dir=_PRIVATE_DIR;
		$mode='private';
	}
	elseif(strpos($dir,_PUBLIC_DIR)===0) 
	{
		$root_dir=_PUBLIC_DIR;
		$mode='public';
	}
	else 
		return 404;
	
	$title=str_replace($root_dir,'/',$dir);
	
	if(!sys::checkPublicPath($dir))
		return 401;
	
		
	//Таблица--------------------------------------------------------------------------
	$table['records']=array();
	$table['checkbox']=true;
	if($_GET['keywords'])
		$files=glob($dir.$_GET['keywords']);
	else 
	{
		$table['msg']=' ';
		$files=array();
	}
	
	if($files)
	{
		foreach ($files as $file)
		{
			$file=str_replace($dir,'',$file);
				
			$obj['name']=$file;
			if(is_dir($dir.$file))
			{
				$obj['type']='dir';
				$obj['size']=0;
			}
			else 
			{
				$obj['type']='file';
				$obj['size']=sys::convertFileSize(filesize($dir.$file));
			}
			if($obj['type']=='dir')
				continue;		
			if($mode=='private')
				$obj['url']='?mod=sys&act=download&file='.base64_encode($dir.$file);
			else 	
				$obj['url']=str_replace(_PUBLIC_DIR,_PUBLIC_URL,$dir).$file;
			
			$table['records'][$file]=$obj;
			$table['params'][$file]['name']['url']=$obj['url'];
		
		}
	}
	
	asort($table['records']);
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['onclick']="window.parent.document.forms['form'].elements['_files[".$_GET['field']."]'].value='%name%';getChildByTagName(window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode,'A').innerHTML='%name%';return false;";
	
	$table['fields']['size']['type']='text';
	$table['fields']['size']['width']=50;
	//======================================================================================================//
	
	
	
	//Поиск------------------------------------------------
	$search['action']='?mod=sys&act=dir&dir='.base64_encode($dir).'&field='.$_GET['field'];
	$search['html_params']='style="width:150px" maxlenght="255"';
	//======================================================
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSpacer().sys_gui::showSearchForm($search);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>