<?
function sys_mod_actions_tree()
{
	if(!sys::checkAccess('sys::mod_edit'))
		return 403;
		
	sys::useLib('sys::mods');
	sys::useLib('sys::form');
	
	sys::filterGet('parent_id','int',0);
	sys::filterGet('mod_id','int');
	sys::filterGet('action_id','int');
	sys::filterGet('mode');
	sys::filterGet('field');
	sys::filterGet('task');
	
	$tree['objects']=sys_mods::getActionsTreeBranch($_GET['parent_id']);
	
	if($_GET['parent_id'])
		sys_gui::mergeTree($_GET['parent_id'],$tree['objects']);
	else 
	{
		//Кнпки
		$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
		
		//Дерево
		$tree['root']['title']=sys::translate('sys::root');
		switch($_GET['task'])
		{
			case 'setValue':
				$tree['root']['onclick']="window.parent.document.forms['form'].elements['".$_GET['field']."'].value=0;";
				$tree['root']['onclick'].="window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode.firstChild.innerHTML='".htmlspecialchars(sys::translate('sys::root'))."';";
				$tree['root']['onclick'].="window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode.lastChild.style.display='none';return false;";
			break;
			
			case 'move':
				$tree['root']['onclick']="window.parent.document.forms['table'].elements['_task'].value='move.".$_GET['action_id'].".0';";
				$tree['root']['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
					
			break;	
			
			case 'moveChecked':	
				$tree['root']['onclick']="window.parent.document.forms['table'].elements['_task'].value='moveChecked.0';";
				$tree['root']['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
			break;
		
			default:
				$tree['root']['onclick']="setMsg('".sys::translate('sys::loading')."','window.parent.fra_right'); window.parent.fra_right.location='?mod=sys&act=mod_actions_table&parent_id=0&mod_id=".$_GET['mod_id']."&mode=".$_GET['mode']."';";
				$tree['root']['url']='?mod=sys&act=mod_actions&mod_id='.$_GET['mod_id'];
		}
		$tree['parent_id']=0;
		
		if(!$_GET['task'])
		{
			$mode['input']='selectbox';
			$mode['values']['frontend']=sys::translate('sys::frontend');
			$mode['values']['backend']=sys::translate('sys::backend');
			$mode['value']=$_GET['mode'];
			$mode['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\',\'window.parent\');window.parent.location=\'?mod=sys&act=mod_actions&mod_id='.$_GET['mod_id'].'&mode=\'+this.value"';
			$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_form::parseField('mode',$mode);
		}
		$result['main']=sys::parseTpl('sys::tree',$tree);
	}
	if(isset($result))
		return $result;
}
?>