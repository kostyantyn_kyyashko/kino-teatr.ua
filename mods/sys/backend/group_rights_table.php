<?
function sys_group_rights_table()
{
	if(!sys::checkAccess('sys::group_edit'))
		return 403;
	
	global $_db, $_err;
	
	sys::useLib('sys::pages');
	sys::useLib('sys::users');
	
	sys::filterGet('order_by','text','order_number.asc');
	sys::filterGet('parent_id','int',0);
	sys::filterGet('mode');
	sys::filterGet('group_id','int');
		
	if(!$_GET['mode'] || ($_GET['mode']!='frontend') && $_GET['mode']!='backend')
		$_GET['mode']='frontend';
		
	if(!$_GET['group_id'])
		return 404;
	
	$tbl='sys_mods_actions';
	$group_name=$_db->getValue('sys_groups','name',$_GET['group_id']);
	$where="acts.parent_id=".$_GET['parent_id']." 
		AND acts.mode='".$_GET['mode']."' ";
	$new_parent_id=false;
	if(!$_GET['parent_id'])
	{
		$parent=false;
		$title=sys::translate('sys::rights');
	}
	else
	{ 
		$q = "SELECT acts.*, acts.order_number AS `order_number`, CONCAT(mods.name,'::',acts.name) AS `title`,
			rights.action_id AS `allowed`  
			FROM `#__sys_mods_actions` AS `acts`
			LEFT JOIN `#__sys_mods` AS `mods`
				ON mods.id=acts.mod_id
			LEFT JOIN `#__sys_groups_rights` AS `rights`
				ON rights.action_id=acts.id 
				AND rights.group_id=".intval($_GET['group_id'])."
			WHERE acts.id=".intval($_GET['parent_id'])."";
		$r=$_db->query($q);
		$parent=$_db->fetchAssoc($r);
		$title=sys::translate($parent['title']).' :: '.sys::translate('sys::rights');
	}
	
	$q="SELECT 
			acts.id, 
			acts.order_number AS `order_number`,
			CONCAT(mods.name,'::',acts.name) AS `title`,
			rights.action_id AS `allowed`  
		FROM `#__".$tbl."` AS `acts`
		LEFT JOIN `#__sys_groups_rights` AS `rights`
			ON rights.action_id=acts.id 
			AND rights.group_id=".intval($_GET['group_id'])."
		LEFT JOIN `#__sys_mods` AS `mods`
			ON mods.id=acts.mod_id
		
		WHERE ".$where;
	//===============================================================================================//
	
	sys_gui::reloadTreeBranch('window.parent.parent.fra_left',$_GET['parent_id'],sys_users::getRightsTreeBranch($_GET['parent_id']),$_GET['parent_id']);
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	if($parent)
	{
		$table['records'][$parent['id']]=$parent;
		$table['params'][$parent['id']]['title']['bold']=true;
		$table['params'][$parent['id']]['title']['onclick']="setMsg('".sys::translate('sys::loading')."'); window.location='?mod=sys&act=group_rights_table&mode=".$_GET['mode']."&group_id=".$_GET['group_id']."&parent_id=".$parent['parent_id']."';return false;";
		$table['params'][$parent['id']]['title']['url']='?mod=sys&act=group_rights_table&mode='.$_GET['mode'].'&group_id='.$_GET['group_id'].'&parent_id='.$parent['parent_id'];
		$table['records'][$parent['id']]['title']='..'.sys::translate($parent['title']);
		$table['params'][$parent['id']]['allowed']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_group_rights_change&group_id='.$_GET['group_id'].'&action_id='.$parent['id'].'\')"';
	}
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=sys::translate($obj['title']);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['title']['type']='text';
	$table['fields']['title']['onclick']="setMsg('".sys::translate('sys::loading')."'); window.location='?mod=sys&act=group_rights_table&parent_id=%id%&mode=".$_GET['mode']."&group_id=".$_GET['group_id']."';return false";
	$table['fields']['title']['url']='?mod=sys&act=group_rights_table&parent_id=%id%&mode='.$_GET['mode'].'&group_id='.$_GET['group_id'].'';
	
	$table['fields']['allowed']['type']='checkbox';
	$table['fields']['allowed']['title']=sys::translate('sys::alwd');
	$table['fields']['allowed']['alt']=sys::translate('sys::allowed');
	$table['fields']['allowed']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_group_rights_change&group_id='.$_GET['group_id'].'&action_id=%id%&recursive=1\')"';
	$table['fields']['allowed']['width']=40;
	
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------/
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	//=======================================================================================================//
	
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle($title);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>