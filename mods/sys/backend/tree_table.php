<?
function sys_tree_table()
{
	if(!sys::checkAccess('sys::system_tree'))
		return 403;
		
	global $_db, $_err;
	
	sys::useLib('sys::pages');
	sys::useLib('sys::mods');
	
	sys::filterGet('order_by','text','order_number.asc');
	sys::filterGet('parent_id','int',0);
	sys::filterGet('mod_id','int');
		
	$tbl='sys_tree';
	$edit_url='?mod=sys&act=tree_node_edit';
	$win_params=sys::setWinParams('700','500');
	$add_title=sys::translate('sys::add_node');
	$order_where="`parent_id`=".$_GET['parent_id']."";
	$where="tree.parent_id=".$_GET['parent_id']."";
	$new_parent_id=false;
	$old_parent_id=false;
	if(!$_GET['parent_id'])
	{
		$parent=false;
		$title=sys::translate('sys::actions');
	}
	else
	{ 
		$parent_parent_id=$_db->getValue('sys_tree','parent_id',intval($_GET['parent_id']));
		if($parent_parent_id)
		{
			$r=$_db->query("SELECT CONCAT(mods.name,'::',tree.name) AS `title`, tree.*
				FROM `#__sys_tree` AS `tree`
				LEFT JOIN `#__sys_mods` AS `mods`
				ON mods.id=tree.mod_id
				WHERE tree.id=".intval($parent_parent_id)."");
	
			$parent=$_db->fetchAssoc($r);
		}
		else 
		{
			$parent['title']=sys::translate('sys::root');
			$parent['order_number']=false;
			$parent['id']=0;
			$parent['parent_id']=0;
		}
		$title=sys::translate($parent['title']);
	}
	
	$q="SELECT 
			tree.id,
			CONCAT(mods.name,'::',tree.name) AS `name`, 
			tree.desktop,
			tree.navigator,
			tree.order_number  
		FROM `#__".$tbl."` AS `tree`
		LEFT JOIN `#__sys_mods` AS `mods`
		ON mods.id=tree.mod_id
		WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			sys_mods::deleteTreeNode($task[1]);
		
		if($task[0]=='move')
		{
			sys_mods::moveTreeNode($task[1],$task[2]);
			$tree_id=$task[2];
		}
			
			
		//Сдвиг вверх
		if($task[0]=='moveUp')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveDown($tbl,$task[1],$order_where);
			else
				$_db->moveUp($tbl,$task[1],$order_where);
		}
		
		//Сдвиг вниз
		if($task[0]=='moveDown')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveUp($tbl,$task[1],$order_where);
			else
				$_db->moveDown($tbl,$task[1],$order_where);
		}
		
		if($task[0]=='moveChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					if($key==$_GET['parent_id'])
						$new_parent_id=$_db->getValue($tbl,'parent_id',$key);
					sys_mods::moveTreeNode($key,$task[1]);
				}
				$tree_id=$task[1];
			}
		}
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					if($key!=$_GET['parent_id'])
						sys_mods::deleteTreeNode($key);
				}
			}		
		}
		
		//Упорядочение нумерации
		if($_POST['_task']=='orderNumbers')
			$_db->orderNumbers($tbl, $order_where);
			
	}
	//================================================================================================//
	if(isset($tree_id))
		sys_gui::reloadTreeBranch('window.parent.fra_left',$tree_id,sys_mods::getTreeBranch($tree_id),$_GET['parent_id']);
		
	sys_gui::reloadTreeBranch('window.parent.fra_left',$_GET['parent_id'],sys_mods::getTreeBranch($_GET['parent_id']),$_GET['parent_id']);
	
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	if($parent)
	{
		$table['records'][0]=$parent;
		$table['records'][0]['name']='..'.$title;
		$table['params'][0]['name']['bold']=true;
		$table['params'][0]['name']['onclick']="setMsg('".sys::translate('sys::loading')."'); window.location='?mod=sys&act=tree_table&parent_id=".$parent['id']."';return false;";
		$table['params'][0]['name']['url']='?mod=sys&act=tree_table&parent_id='.$parent['parent_id'];
		$table['params'][0]['order_number']['html_params']='size="8" disabled ';
		$table['params'][0]['move_up']['type']='text';
		$table['params'][0]['move_down']['type']='text';
		$table['params'][0]['delete']['type']='text';
		$table['params'][0]['move']['type']='text';
		$table['params'][0]['edit']['type']='text';
		$table['params'][0]['desktop']['html_params']='disabled';
		$table['params'][0]['navigator']['html_params']='disabled';
	}
	
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['name']=sys::translate($obj['name']);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['move_up']['type']='button';
	$table['fields']['move_up']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveUp.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_up']['image']='sys::btn.desc.gif';
	
	$table['fields']['move_down']['type']='button';
	$table['fields']['move_down']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveDown.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_down']['image']='sys::btn.asc.gif';
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['move']['type']='button';
	$table['fields']['move']['frame']='?mod=sys&act=tree_tree&task=move&node_id=%id%';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;
	$table['fields']['name']['onclick']="setMsg('".sys::translate('sys::loading')."'); window.location='?mod=sys&act=tree_table&parent_id=%id%';return false";
	$table['fields']['name']['url']='?mod=sys&act=tree_table&parent_id=%id%';
	
	$table['fields']['desktop']['type']='checkbox';
	$table['fields']['desktop']['title']=sys::translate('sys::dsk');
	$table['fields']['desktop']['alt']=sys::translate('sys::desktop');
	$table['fields']['desktop']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~desktop~%id%~%desktop%\')"';
	$table['fields']['desktop']['width']=40;
	
	$table['fields']['navigator']['type']='checkbox';
	$table['fields']['navigator']['title']=sys::translate('sys::nvg');
	$table['fields']['navigator']['alt']=sys::translate('sys::navigator');
	$table['fields']['navigator']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~navigator~%id%~%navigator%\')"';
	$table['fields']['navigator']['width']=40;
	
	$table['fields']['order_number']['type']='textbox';
	$table['fields']['order_number']['order']=true;
	$table['fields']['order_number']['html_params']='size="8" onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=set~'.$tbl.'~order_number~%id%~\'+this.value+\'~int\')"';
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&parent_id=".$_GET['parent_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';
	
	$buttons['move']['frame']='?mod=sys&act=tree_tree&task=moveChecked&field=_task';
	$buttons['move']['title']=sys::translate('sys::move_checked');
	
	//=======================================================================================================//
	
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle($title);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
