<?
function sys_tree_tree()
{
	if(!sys::checkAccess('sys::system_tree'))
		return 403;
		
	global $_db, $_err;

	sys::useLib('sys::mods');
	sys::useLib('sys::form');
	
	sys::filterGet('parent_id','int',0);
	sys::filterGet('field');
	sys::filterGet('task');
	
	$tree['objects']=sys_mods::getTreeBranch($_GET['parent_id']);
	
	if($_GET['parent_id'])
		sys_gui::mergeTree($_GET['parent_id'],$tree['objects']);
	else 
	{
		//Кнпки
		$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
		
		//Дерево
		$tree['root']['title']=sys::translate('sys::root');
		switch($_GET['task'])
		{
			case 'setValue':
				$tree['root']['onclick']="window.parent.document.forms['form'].elements['".$_GET['field']."'].value=0;";
				$tree['root']['onclick'].="getChildByTagName(window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode,'A').innerHTML='".htmlspecialchars(sys::translate('sys::root'))."';";
				$tree['root']['onclick'].="getChildByTagName(window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode,'DIV').style.display='none';return false;";
			break;
			
			case 'move':
				$tree['root']['onclick']="window.parent.document.forms['table'].elements['_task'].value='move.".$_GET['node_id'].".0';";
				$tree['root']['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
					
			break;	
			
			case 'moveChecked':	
				$tree['root']['onclick']="window.parent.document.forms['table'].elements['_task'].value='moveChecked.0';";
				$tree['root']['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
			break;
		
			default:
				$tree['root']['onclick']="setMsg('".sys::translate('sys::loading')."','window.parent.fra_right'); window.parent.fra_right.location='?mod=sys&act=tree_table&parent_id=0';";
				$tree['root']['url']='?mod=sys&act=tree_table';
		}
		$tree['parent_id']=0;
		
		if(!$_GET['task'])
		{
			$result['panels'][]['text']=sys_gui::showButtons($buttons);
		}
		$result['main']=sys::parseTpl('sys::tree',$tree);
	}
	if(isset($result))
		return $result;
}
?>