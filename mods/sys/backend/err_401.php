<?
function sys_err_401()
{
	sys::setTitle(sys::translate('sys::err_401'));
	$result['panels'][0]['text']=sys_gui::showNavPath('sys::err_401');
	$result['msg']=sys::translate('sys::err_401_text');
	sys::jsAlert(sys::translate('sys::err_401_text'));
	return $result;
}
?>