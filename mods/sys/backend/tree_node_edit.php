<?
function sys_tree_node_edit()
{
	if(!sys::checkAccess('sys::tree_node_edit'))
		return 403;
		
	global $_db, $_err;
		
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::mods');
	
	sys::filterGet('id','int');
	sys::filterGet('parent_id','int',0);
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='sys_tree';
	if($_GET['parent_id'])
		$default['mod_id']=$_db->getValue('sys_tree','mod_id',$_GET['parent_id']);
	$default['parent_id']=$_GET['parent_id'];
	$default['order_number']=$_db->getMaxOrderNumber($tbl,"`parent_id`=".intval($_GET['parent_id'])."");
	$default['action_id']=0;
	
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('sys::new_node');
	}
	
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//mod_id
	$fields['mod_id']['input']='selectbox';
	$fields['mod_id']['type']='int';
	$fields['mod_id']['req']=true;
	$fields['mod_id']['table']=$tbl;
	$fields['mod_id']['title']=sys::translate('sys::module');
	$r=$_db->query("SELECT `id`,`name` FROM `#__sys_mods`");
	$fields['mod_id']['values']['']='';
	while($mod=$_db->fetchAssoc($r))
	{
		$fields['mod_id']['values'][$mod['id']]=$mod['name'];
	}
	
	//parent_id
	$fields['parent_id']['input']='framebox';
	$fields['parent_id']['type']='int';
	$r=$_db->query("SELECT CONCAT(mods.name,'::',tree.name)
			FROM `#__sys_tree` AS `tree`
			LEFT JOIN `#__sys_mods` AS `mods`
			ON mods.id=tree.mod_id
			WHERE tree.id=".intval($values['parent_id'])."	
		");
	list($parent_name)=$_db->fetchArray($r);
	if($parent_name)
		$parent_name=sys::translate($parent_name);
	if(!$fields['parent_id']['text']=$parent_name)
		$fields['parent_id']['text']=sys::translate('sys::root');
	$fields['parent_id']['frame']='?mod=sys&act=tree_tree&task=setValue';
	$fields['parent_id']['frame::width']=200;
	$fields['parent_id']['frame::height']=200;
	$fields['parent_id']['table']=$tbl;
	$fields['parent_id']['title']=sys::translate('sys::parent');
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='name';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['max_chars']=255;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';
	
	//action_id
	$fields['action_id']['input']='framebox';
	$fields['action_id']['type']='int';
	$r=$_db->query("SELECT CONCAT(mods.name,'::',acts.name)
			FROM `#__sys_mods_actions` AS `acts`
			LEFT JOIN `#__sys_mods` AS `mods`
			ON mods.id=acts.mod_id
			WHERE acts.id=".intval($values['action_id'])."	
		");
	list($action_name)=$_db->fetchArray($r);
	if($action_name)
		$parent_name=sys::translate($action_name);
	if(!$fields['action_id']['text']=$action_name)
		$fields['action_id']['text']=sys::translate('sys::not_set');
	$fields['action_id']['frame']='?mod=sys&act=mod_actions_tree&task=setValue&mode=backend';
	$fields['action_id']['frame::width']=200;
	$fields['action_id']['frame::height']=200;
	$fields['action_id']['table']=$tbl;
	$fields['action_id']['title']=sys::translate('sys::action');
	
	//url
	$fields['url']['input']='textbox';
	$fields['url']['type']='text';
	$fields['url']['req']=true;
	$fields['url']['table']=$tbl;
	$fields['url']['max_chars']=255;
	$fields['url']['html_params']='style="width:100%" maxlength="255"';
	
	//icon
	$fields['icon']['input']='textbox';
	$fields['icon']['type']='text';
	$fields['icon']['table']=$tbl;
	$fields['icon']['max_chars']=255;
	$fields['icon']['html_params']='style="width:100%" maxlength="255"';
	
	//desktop
	$fields['desktop']['input']='checkbox';
	$fields['desktop']['type']='bool';
	$fields['desktop']['table']=$tbl;
	
	//navigator
	$fields['navigator']['input']='checkbox';
	$fields['navigator']['type']='bool';
	$fields['navigator']['table']=$tbl;
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!sys_mods::checkTreeNodeParent($_GET['id'],$values['parent_id']))
					$_err=sys::translate('sys::invalid_parent_node');
				if(!$_err)
				{
					if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
						$_err=sys::translate('sys::db_error').$_db->err;
				}
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>