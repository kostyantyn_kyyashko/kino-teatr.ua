<?
function sys_mod_udata_edit()
{
	if(!sys::checkAccess('sys::mod_edit'))
		return 403;
		
	global $_db, $_err;

	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::mods');
		
	sys::filterGet('id','int');
	sys::filterGet('mod_id','int',0);
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='sys_mods_udata';
	$default['mod_id']=$_GET['mod_id'];
	$default['order_number']=$_db->getMaxOrderNumber($tbl,"`mod_id`=".$_GET['mod_id']."");
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('sys::new_field');
	}
	
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//mod_id
	$fields['mod_id']['input']='hidden';
	$fields['mod_id']['type']='int';
	$fields['mod_id']['req']=true;
	$fields['mod_id']['table']=$tbl;
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='name';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['min_chars']=3;
	$fields['name']['max_chars']=16;
	$fields['name']['html_params']='style="width:100%" maxlength="16"';
	$fields['name']['unique']=true;
	$fields['name']['unique::where']="`id`!=".intval($_GET['id'])." AND `mod_id`=".intval($values['mod_id'])."";
	if($_GET['id'])
		$fields['name']['html_params'].='readonly';
	
	//field
	$fields['field']['input']='selectbox';
	$fields['field']['values']['']=false;
	$fields['field']['req']=true;
	$fields['field']['type']='text';
	$fields['field']['table']=$tbl;
	$fields['field']['values']=sys_mods::getFields();
	
	
	//required
	$fields['required']['input']='checkbox';
	$fields['required']['type']='bool';
	$fields['required']['table']=$tbl;
	
	if(!$_GET['id'])
	{
		//multi_lang
		$fields['multi_lang']['input']='checkbox';
		$fields['multi_lang']['type']='bool';
		$fields['multi_lang']['table']=$tbl;
	}
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				$mod_name=$_db->getValue('sys_mods','name',intval($_GET['mod_id']));
				if($values['multi_lang'])
				{
					if(!$_db->query("ALTER TABLE `#__sys_users_data_lng` ADD `".$mod_name."_".$values['name']."` VARCHAR( 55 );"))
						$_err=sys::translate('sys::db_error').$_db->err;
				}
				else 
				{
					if(!$_db->query("ALTER TABLE `#__sys_users_data` ADD `".$mod_name."_".$values['name']."` VARCHAR( 55 );"))
						$_err=sys::translate('sys::db_error').$_db->err;
				}
				if(!$_err)
				{
					if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
						$_err=sys::translate('sys::db_error').$_db->err;
				}
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>