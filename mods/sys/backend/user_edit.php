<?
function sys_user_edit()
{
	if(!sys::checkAccess('sys::user_edit'))
		return 403;
		
	global $_db, $_cfg, $_err, $_langs, $_charsets;

	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::users');
	
	sys::filterGet('id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='sys_users';
	$default=array();
	$default['date_reg']=strftime($_cfg['sys::date_time_format']);
	$default['password']=false;
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$record['groups']=sys_users::getUserGroupsIds($_GET['id']);
		$record['password']=false;
		$record['last_ip']=long2ip($record['last_ip']);
		$record['date_reg']=sys::db2Date($record['date_reg'],true);
		if($record['only_from_ip'])
			$record['only_from_ip']=long2ip($record['only_from_ip']);
		$title=$record['login'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('sys::new_user');
	}
	
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//login
	$fields['login']['input']='textbox';
	$fields['login']['type']='text';
	$fields['login']['req']=true;
	$fields['login']['table']=$tbl;
	$fields['login']['unique']=true;
	$fields['login']['min_chars']=3;
	$fields['login']['max_chars']=16;
	$fields['login']['unique::where']=$_GET['id'];
	$fields['login']['html_params']='size="16" maxlength="16"';
	if($_GET['id']==7 || $_GET['id']==2)
		$fields['login']['html_params'].=' readonly ';
		
	//password
	$fields['password']['input']='textbox';
	$fields['password']['type']='text';
	if($values['password'])
		$fields['password']['table']=$tbl;
	$fields['password']['min_chars']=6;
	$fields['password']['max_chars']=16;
	$fields['password']['html_params']='size="16" maxlength="16"';
	if($_GET['id']==2)
		$fields['password']['html_params'].=' readonly ';
	$fields['password']['extra']=sys_gui::showButton(sys::translate('sys::generate'),"this.form.elements['password'].value=generatePass()");
	
	//email
	$fields['email']['input']='textbox';
	$fields['email']['type']='email';
	$fields['email']['req']=true;
	$fields['email']['table']=$tbl;
	$fields['email']['unique']=true;
	$fields['email']['unique::where']=$_GET['id'];
	$fields['email']['html_params']='style="width:100%" maxlength="255"';
	if($_GET['id']==2)
		$fields['email']['html_params'].=' readonly ';
	
	if($_GET['id']!=2 && $_GET['id']!=7)
	{
		//groups
		$fields['groups']['input']='selectbox';
		$fields['groups']['type']='int';
		$fields['groups']['req']=true;
		$fields['groups']['multi']=true;
		$fields['groups']['values']=$_db->getValues('sys_groups','name',"`id`!=2","ORDER BY `name`");
	}
	
	//lang_id
	$fields['lang_id']['input']="selectbox";
	$fields['lang_id']['type']='int';
	$fields['lang_id']['req']=true;
	$fields['lang_id']['title']=sys::translate('sys::language');
	foreach ($_langs as $lang)
	{
		$fields['lang_id']['values'][$lang['id']]=$lang['title'];
	}
	
	//date_reg
	$fields['date_reg']['input']='datebox';
	$fields['date_reg']['type']='date';
	$fields['date_reg']['req']=true;
	$fields['date_reg']['table']=$tbl;
	$fields['date_reg']['title']=sys::translate('sys::registration_date');
	
	//only_from_ip
	$fields['only_from_ip']['input']='textbox';
	$fields['only_from_ip']['type']='text';
	$fields['only_from_ip']['table']='sys_users';
	$fields['only_from_ip']['html_params']='size="15"';
	
	//last_ip
	$fields['last_ip']['input']='textbox';
	$fields['last_ip']['type']='text';
	$fields['last_ip']['html_params']='size="15" readonly';
	
	//on
	$fields['on']['input']='checkbox';
	$fields['on']['type']='bool';
	$fields['on']['table']=$tbl;
	$fields['on']['title']=sys::translate('sys::is_on');
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['password']=md5($values['password']);
			$values['date_reg']=sys::date2Db($values['date_reg'],true);
			if($values['only_from_ip'])
				$values['only_from_ip']=ip2long($values['only_from_ip']);
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
				$_db->query("INSERT INTO `#__sys_users_data` (`user_id`) VALUES (".$_GET['id'].")");
				$_db->query("INSERT INTO `#__sys_users_cfg` (`user_id`) VALUES (".$_GET['id'].")");
				$_db->query("INSERT INTO `#__sys_users_data_lng` (`record_id`) VALUES (".$_GET['id'].")");
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['date_reg']=sys::db2Date($values['date_reg'],true);
			$values['password']=false;
			if($values['only_from_ip'])
				$values['only_from_ip']=long2ip($values['only_from_ip']);
		}
		if(!$_err)
		{	
			//Записать группы
			if($_GET['id']!=2 && $_GET['id']!=7)
				sys_users::saveGroups($_GET['id'],$_POST['groups']);
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	if($_GET['id'])
	{
		$panel['class']='blank';
		$panel['text']=sys_users::showUserTabs($_GET['id'],'info');
		$result['panels'][]=$panel;
	}
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>