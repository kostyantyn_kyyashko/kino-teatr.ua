<?
function sys_ajx_group_rights_change()
{	
	if(!sys::checkAccess('sys::group_edit'))
		return 403;
	
	global $_db;
	sys::useLib('sys::users');
	sys::setTpl();
	
	sys::filterGet('group_id','int');
	sys::filterGet('action_id','int');
	sys::filterGet('allowed','int');
	sys::filterGet('recursive','int');
	
	if(!$_GET['group_id'] || !$_GET['action_id'])
		return 404;
	
	sys::sendHeaders();
	$allowed=$_db->getValue('sys_groups_rights','group_id',"`group_id`=".intval($_GET['group_id'])." AND `action_id`=".intval($_GET['action_id'])."");
	if($allowed)
		sys_users::disallowAction($_GET['group_id'],$_GET['action_id'],$_GET['recursive']);
	else 
		sys_users::allowAction($_GET['group_id'],$_GET['action_id'],$_GET['recursive']);
	?>hideMsg();<?
}
?>