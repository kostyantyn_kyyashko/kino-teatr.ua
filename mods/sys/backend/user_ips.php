<?
function sys_user_ips()
{
	if(!sys::checkAccess('sys::user_edit'))
		return 403;
		
	global $_db, $_cfg, $_err;

	sys::useLib('sys::pages');
	sys::useLib('sys::users');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','ip.asc');
	sys::filterGet('user_id','int');
	
	$tbl='sys_users_ips';
	$where=" `user_id`=".intval($_GET['user_id'])." ";
	$login=$_db->getValue('sys_users','login',$_GET['user_id']);
	if(!$login)
		return 404;
	$title=$login.' :: '.sys::translate('sys::ips');
	$q="SELECT `ip` 
		FROM `#__".$tbl."` 
		WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
	
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['ip']=long2ip($obj['ip']);
		$table['records'][$obj['ip']]=$obj;
	}
	
	$table['fields']['ip']['type']='text';
	$table['fields']['ip']['order']=true;
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle(sys::translate($title));
	
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';

	$panel['class']='blank';
	$panel['text']=sys_users::showUserTabs($_GET['user_id'],'ips');
	$result['panels'][]=$panel;
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
