<?
function sys_metatemplates_table()
{
	if(!sys::checkAccess('sys::metatemplates'))
		return 403;
		
	global $_db , $_err;
	sys::useLib('sys::pages');
	sys::useLib('sys::mods');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.asc');
		
	$tbl='sys_mods_metatemplates';
	$edit_url='?mod=sys&act=metatemplate_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('test::add_metatemplate');
	$order_where=$where=false;
	$nav_point='sys::metatemplates';
	
	$q="SELECT 
			tpl.id, 
			tpl.name,
			concat(mods.name,'::',tpl.name) AS `title`,
			mods.name AS `mod`
		FROM `#__".$tbl."` AS `tpl`
		LEFT JOIN `#__sys_mods` AS `mods`
		ON mods.id=tpl.mod_id";
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			sys_mods::deleteMetatemplate($task[1]);
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					sys_mods::deletemetatemplate($key);
				}
			}
					
		}	
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['mod']=sys::translate($obj['mod'].'::'.$obj['mod']);
		$obj['title']=''.sys::translate($obj['title']);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	if(sys::checkAccess('sys::metatemplate_delete'))
	{
		$table['fields']['delete']['type']='button';
		$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	}
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	if(sys::checkAccess('sys::metatemplate_add'))
	{
		$table['fields']['name']['type']='text';
		$table['fields']['name']['order']=true;
	}
	
	$table['fields']['title']['type']='text';
	
	$table['fields']['mod']['type']='text';
	$table['fields']['mod']['order']=true;
	$table['fields']['mod']['title']=sys::translate('sys::module');
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	if(sys::checkAccess('sys::metatemplate_add'))
	{
		$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
		$buttons['new']['title']=$add_title;
	}
	
	if(sys::checkAccess('sys::metatemplate_delete'))
	{
		$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
		$buttons['delete_checked']['image']='sys::btn.delete.gif';
	}
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
