<?
function sys_mod_edit()
{
	if(!sys::checkAccess('sys::mod_edit'))
		return 403;
		
	global $_db, $_err;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::mods');
	
	sys::filterGet('id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='sys_mods';
	
	$default['on']=1;
	$default['order_number']=$_db->getMaxOrderNumber($tbl);
	
	
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('sys::new_module');
	}
	
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='name';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['min_chars']=3;
	$fields['name']['html_params']='style="width:100%" maxlength="55"';
	$fields['name']['unique']=true;
	$fields['name']['unique::where']=$_GET['id'];
	if($_GET['id']==1 || $_GET['id']==2)
		$fields['name']['html_params'].=' readonly';
	
	//info
	$fields['info']['input']='textarea';
	$fields['info']['type']='text';
	$fields['info']['table']=$tbl;
	$fields['info']['html_params']='style="width:100%; height:50px"';
	if($_GET['id']==1 || $_GET['id']==2)
	{
		$fields['info']['html_params'].=' readonly';
		$fields['info']['table']=false;
	}
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	//on
	$fields['on']['input']='checkbox';
	$fields['on']['type']='bool';
	$fields['on']['table']=$tbl;
	$fields['on']['title']=sys::translate('sys::is_on');
	if($_GET['id']==1 || $_GET['id']==2)
	{
		$fields['on']['html_params']='disabled';
		$fields['on']['table']=false;
		$fields['on']['value']=1;
	}
		
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	if($_GET['id'])
	{
		$panel['class']='blank';
		$panel['text']=sys_mods::showTabs($_GET['id'],'info');
		$result['panels'][]=$panel;
	}
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>