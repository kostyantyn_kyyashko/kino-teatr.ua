<style type="text/css">
	table.debug{
		width:auto;
		background:#D4D0C8;
		font:12px Verdana;
		margin:4px;
		border-collapse:collapse;
	}

	table.debug th, table.debug td{
		padding:4px;
		border:1px solid black;
		text-align:left;
		vertical-align:top;
	}
	
	table.debug pre{
		font:12px Verdana;
	}
	
	table.debug th{
		background:gray;
		color:#ffffff;
	}
	
	div.debug{
		width:100%;
		background:#ffffff;
	}
</style>
<div class="debug">
	<?foreach($var['sql::explain'] as $obj):?>
		<table class="debug">
			<tr>
				<th>time</th>
				<th>query</th>
			</tr>
			<tr>
				<td><?=number_format($obj['time'],4)?></td>
				<td><pre><?=$obj['query']?></pre></td>
			</tr>
			<?if(count($obj['explain'])):?>
				<tr>
					<th colspan="2">explain</th>
				</tr>
				<tr>
					<td colspan="2">
						<?foreach($obj['explain'] as $obj2):?>
							<table class="debug">
								<?foreach($obj2 as $key=>$val):?>
									<?if($val):?>
										<tr>
											<th><?=$key?></th>
											<td><?=$val?></td>
										</tr>
									<?endif;?>
								<?endforeach;?>
							</table>
						<?endforeach;?>
					</td>
				</tr>
			<?endif?>
		</table>
	<?endforeach?>
	<table class="debug">
		<tr>
			<th><?=sys::translate('sys::total_sql_time')?></th>
			<td><?=number_format($var['sql::time'],4)?></td>
		</tr>
		<tr>
			<th><?=sys::translate('sys::total_sql_number')?></th>
			<td><?=$var['sql::number']?></td>
		</tr>
	</table>
</div>