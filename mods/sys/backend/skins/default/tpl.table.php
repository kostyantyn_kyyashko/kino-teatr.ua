<form id="table" class="table" action="<?=$_SERVER['REQUEST_URI']?>" method="POST">	
	<input type="hidden" name="_task">
	<?if(!$var['objects']):?>
		<div class="msg"><?=$var['msg']?></div>
	<?else:?>
		<table id="thead1" class="table">
			<tr>
				<?if($var['checkbox']):?>
					<th width="1">
						<input onclick="checkAll(this,'_chk')" class="checkbox" type="checkbox">
					</th>
				<?endif?>
				<?foreach ($var['fields'] as $key=>$field):?>
					<th title="<?=$field['alt']?>">
					<?if($field['order_url']):?>
						<a onclick="setMsg('<?=sys::translate('sys::loading')?>'); window.location='<?=$field['order_url']?>'; return false;" class="<?=$field['class']?>" href="<?=$field['order_url']?>"><?=$field['title']?></a>
					<?else:?>
						<nobr><?=$field['title']?></nobr>
					<?endif?>
					</th>
				<?endforeach;?>
				<th style="text-align:center;" width="20">
					<a onclick="setMsg('<?=sys::translate('sys::loading')?>'); window.location='<?=$var['order_url']?>'; return false;" href="<?=$var['order_url']?>"><img src="<?=$var['order_image']?>" title="<?=$var['order_title']?>" alt="<?=$var['order_title']?>"></a>
				</th>
			</tr>
		</table>
		<table id="tbody" class="table">
			<?$i=1;?>
			<?$z_index=count($var['objects'])+10?>
			<?foreach ($var['objects'] as $id=>$obj):?>
				<?if($i%2==0) $class='even'; else $class='odd';?>
				<tr class="<?=$class?>">
					<?if($var['checkbox']):?>
						<td width="1"><input class="checkbox" name="_chk[<?=$id?>]" type="checkbox"></td>
					<?endif?>
					<?foreach ($obj['fields'] as $key=>$field):?>
						<td title="<?=$field['alt']?>" class="<?=$field['type']?>" <?if($field['width']):?>width="<?=$field['width']?>"<?endif?>>
							<?if($field['onclick'] || $field['url']):?>
								<a target="_blank" href="<?=$field['url']?>" onclick="<?=$field['onclick']?>">
							<?endif;?>
							<?if($field['bold']):?>
								<b>
							<?endif;?>
							<?if($field['italic']):?>
								<i>
							<?endif;?>
								<?switch ($field['type']):
									
									case 'button':?>
										<?if($field['frame']):?>
											<span style="position:relative; z-index:<?=$z_index?>;">
												<img class="hand" src="<?=$field['src']?>" alt="<?=$field['title']?>" title="<?=$field['title']?>" onclick="showFrameBox(this,'<?=$field['frame']?>');window.focus(); return false;">
												<div style="display:none; width:<?=$field['frame::width']?>px; height:<?=$field['frame::height']?>px; position:absolute; z-index:1; left:10px; top:10px;" class="panel">
													<div style="text-align:right;" class="panel title">
														<img class="hand" onclick="this.parentNode.parentNode.style.display='none';" src="<?=sys_gui::getIconSrc('sys::btn.close.gif')?>" alt="<?=sys::translate('sys""close')?>" title="<?=sys::translate('sys::close')?>">
													</div>
													<iframe frameborder="0" width="100%" height="<?=($field['frame::height']-22)?>"></iframe>
												</div>
											</span>
										<?else:?>
											<img src="<?=$field['src']?>" alt="<?=$field['alt']?>" title="<?=$field['alt']?>">
										<?endif?>
									<?break;?>
									
									<?case 'image':?>
										<img  src="<?=$field['src']?>?time=<?=time()?>" alt="<?=$obj[$key]?>" title="<?=$obj[$key]?>">
									<?break;?>
									
									<?default:?>
										<?=$obj[$key]?>
									
								<?endswitch;?>
							<?if($field['italic']):?>
								</i>
							<?endif;?>
							<?if($field['bold']):?>
								</b>
							<?endif;?>
							<?if($field['onclick'] || $field['url']):?>
								</a>
							<?endif;?>
						</td>
						
					<?endforeach;?>
					<td width="20"></td>
				</tr>
				<?$i++?>
				<?$z_index--;?>
			<?endforeach;?>
		</table>
	
	<?endif;?>
</form>