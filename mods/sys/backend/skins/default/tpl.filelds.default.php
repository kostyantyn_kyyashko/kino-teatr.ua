<?$areas=array('textarea','editor','fck','bbarea','info')?>
<?$z_index=count($var['fields'])+10?>
<table class="form">
	<?foreach ($var['fields'] as $name=>$field):?>
		<?if(in_array($field['input'],$areas)):?>
			<tr>
				<th colspan="2">
					<nobr>
						<?=$field['title']?><?if(isset($field['req']) && $field['req']):?>*<?endif?>:
					</nobr>
				</th>
			</tr>
			<tr>
				<td colspan="2">
					<div style="position:relative; z-index:<?=$z_index?>">
						<?=$field['html']?>
						<?if(isset($field['extra'])):?>
							<?=$field['extra']?>
						<?endif?>
						<?if(isset($field['info'])):?>
							<div class="info"><img onclick="if(this.parentNode.lastChild.style.display=='block')this.parentNode.lastChild.style.display='none'; else this.parentNode.lastChild.style.display='block'" align="absmiddle" src="<?=_IMG_URL?>btn.info.gif" alt="<?=sys::translate('sys::information')?>" title="<?=sys::translate('sys::information')?>"><div ondblclick="this.style.display='none'" class="info_text"><div style="text-align:right;" class="panel title"><img class="hand" onclick="this.parentNode.parentNode.style.display='none';" src="<?=sys_gui::getIconSrc('sys::btn.close.gif')?>" alt="<?=sys::translate('sys""close')?>" title="<?=sys::translate('sys::close')?>"></div><p class="panel"><?=$field['info']?></p><iframe frameborder="0"></iframe></div></div>
						<?endif?>
					</div>
				</td>
			</tr>
		<?elseif($field['input']!='hidden'):?>
			<tr>
				<th>
					<nobr>
						<?=$field['title']?><?if(isset($field['req']) && $field['req']):?>*<?endif?>:
					</nobr>
				</th>
				<td>
					<div style="position:relative; z-index:<?=$z_index?>">
						<?=$field['html']?>
						<?if(isset($field['extra'])):?>
							<?=$field['extra']?>
						<?endif?>
						<?if(isset($field['info'])):?>
							<div class="info"><img onclick="if(this.parentNode.lastChild.style.display=='block')this.parentNode.lastChild.style.display='none'; else this.parentNode.lastChild.style.display='block'" align="absmiddle" src="<?=_IMG_URL?>btn.info.gif" alt="<?=sys::translate('sys::information')?>" title="<?=sys::translate('sys::information')?>"><div ondblclick="this.style.display='none'" class="info_text"><div style="text-align:right; width:250px;" class="panel title"><img class="hand" onclick="this.parentNode.parentNode.style.display='none';" src="<?=sys_gui::getIconSrc('sys::btn.close.gif')?>" alt="<?=sys::translate('sys""close')?>" title="<?=sys::translate('sys::close')?>"></div><div class="panel info_text2"><?=$field['info']?></div><iframe frameborder="0"></iframe></div></div>
						<?endif?>
					</div>
				</td>
			</tr>
		<?else:?>
			<?=$field['html']?>
		<?endif?>
		<?$z_index--;?>
	<?endforeach;?>
</table>


