<input type="file" size="75" title="<?=$var['title']?>" <?=$var['html_params']?> name="<?=$var['name']?>"> 
<input type="hidden" name="_files[<?=$var['name']?>]" value="<?=$var['value']?>">
<span style="position:relative;">
	<input onclick="showFrameBox(this,'?mod=sys&act=dir&field=<?=$var['name']?>&dir=<?=base64_encode($var['file::dir'])?>'); return false;" type="button" class="button" value="<?=sys::translate('sys::server')?>..."> 
	<div title="<?=sys::translate('sys::files');?>" style="display:none; width:370px; height:300px; position:absolute; z-index:1; left:0px; top:18px;" class="panel">
		<div style="text-align:right;" class="panel title">
			<img class="hand" onclick="this.parentNode.parentNode.style.display='none';" src="<?=sys_gui::getIconSrc('sys::btn.close.gif')?>" alt="<?=sys::translate('sys::close')?>" title="<?=sys::translate('sys::close')?>">
		</div>
		<iframe frameborder="0" width="100%" height="280"></iframe>
	</div>
</span>
<a target="_blank" href="<?=$var['file::url']?>"><?=$var['value']?></a>
<?if($var['value']):?>
	<input style="border:0px;" title="<?=sys::translate('sys::delete_file')?>" onclick="if(confirm('<?=sys::translate('sys::are_you_sure')?>')){setMsg('<?=sys::translate('sys::deleting')?>');this.form.elements['_task'].value='delete_file.<?=$var['name']?>';}" align="absmiddle" src="<?=sys_gui::getIconSrc('sys::btn.delete.gif')?>" type="image">
<?endif;?>
<?if($var['preview'] && $var['value']):?>
	<a class="preview" target="_blank" href="<?=$var['preview::url']?>"><img src="<?=$var['preview::src']?>?<?=time();?>" alt="" <?=$var['preview::html_params']?>></a>
<?endif?>
