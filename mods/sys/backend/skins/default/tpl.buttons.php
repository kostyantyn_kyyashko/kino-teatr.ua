<span class="buttons">
	<?foreach($var['objects'] as $key=>$obj):?>
		<?if($key=='spacer') $class=' spacer';else $class=false;?>
		<?if($obj['frame']):?>
			<span style="position:relative;"><img class="button" align="absmiddle" src="<?=$obj['src']?>" alt="<?=$obj['title']?>" title="<?=$obj['title']?>" onclick="showFrameBox(this,'<?=$obj['frame']?>'); return false;">
			<div title="_frame" style="display:none; width:<?=$obj['frame::width']?>px; height:<?=$obj['frame::height']?>px; position:absolute; z-index:900; left:20px; top:18px;" class="panel"><div style="text-align:right;" class="panel title"><img class="hand" onclick="this.parentNode.parentNode.style.display='none';" src="<?=sys_gui::getIconSrc('sys::btn.close.gif')?>" alt="<?=sys::translate('sys""close')?>" title="<?=sys::translate('sys::close')?>"></div><iframe frameborder="0" width="100%" height="<?=($obj['frame::height']-22)?>"></iframe></div></span>
		<?elseif($obj['url']):?>
			<a href="<?=$obj['url']?>" onclick="<?=$obj['onclick']?>"><img class="button" src="<?=$obj['src']?>" alt="<?=$obj['title']?>" title="<?=$obj['title']?>"></a>
		<?else:?>
			<img align="absmiddle" onclick="<?=$obj['onclick']?>" class="button<?=$class?>" src="<?=$obj['src']?>" alt="<?=$obj['title']?>" title="<?=$obj['title']?>">
		<?endif?>
	<?endforeach;?>
</span>
