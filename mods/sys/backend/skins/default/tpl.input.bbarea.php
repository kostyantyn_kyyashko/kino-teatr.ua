<?=sys::jsInclude('sys::bb');?>
<div class="xBB"> 
	<img alt="<?=sys::translate('sys::bold')?>" title="<?=sys::translate('sys::bold')?>" onclick="insertTag('bold',getChildByTagName(this.parentNode,'TEXTAREA').form,'<?=$var['name']?>');" src="<?=_IMG_URL?>btn.bold.gif">
	<img alt="<?=sys::translate('sys::italic')?>" title="<?=sys::translate('sys::italic')?>" onclick="insertTag('italic',getChildByTagName(this.parentNode,'TEXTAREA').form,'<?=$var['name']?>');" src="<?=_IMG_URL?>btn.italic.gif">
	<img alt="<?=sys::translate('sys::underline')?>" title="<?=sys::translate('sys::underline')?>" onclick="insertTag('underline',getChildByTagName(this.parentNode,'TEXTAREA').form,'<?=$var['name']?>');" src="<?=_IMG_URL?>btn.underline.gif">
	<img alt="<?=sys::translate('sys::strike')?>" title="<?=sys::translate('sys::strike')?>" onclick="insertTag('strike',getChildByTagName(this.parentNode,'TEXTAREA').form,'<?=$var['name']?>');" src="<?=_IMG_URL?>btn.strike.gif">
	<img alt="<?=sys::translate('sys::quote')?>" title="<?=sys::translate('sys::quote')?>" onclick="insertTag('quote',getChildByTagName(this.parentNode,'TEXTAREA').form,'<?=$var['name']?>');" src="<?=_IMG_URL?>btn.quote.gif">
	<img alt="<?=sys::translate('sys::align_left')?>" title="<?=sys::translate('sys::align_left')?>" onclick="insertTag('left',getChildByTagName(this.parentNode,'TEXTAREA').form,'<?=$var['name']?>');" src="<?=_IMG_URL?>btn.align_left.gif">
	<img alt="<?=sys::translate('sys::align_center')?>" title="<?=sys::translate('sys::align_center')?>" onclick="insertTag('center',getChildByTagName(this.parentNode,'TEXTAREA').form,'<?=$var['name']?>');" src="<?=_IMG_URL?>btn.align_center.gif">
	<img alt="<?=sys::translate('sys::align_right')?>" title="<?=sys::translate('sys::align_right')?>" onclick="insertTag('right',getChildByTagName(this.parentNode,'TEXTAREA').form,'<?=$var['name']?>');" src="<?=_IMG_URL?>btn.align_right.gif">
	<img alt="<?=sys::translate('sys::link2')?>" title="<?=sys::translate('sys::link2')?>" onclick="insertTag('link',getChildByTagName(this.parentNode,'TEXTAREA').form,'<?=$var['name']?>');" src="<?=_IMG_URL?>btn.link.gif">
	<img alt="<?=sys::translate('sys::image')?>" title="<?=sys::translate('sys::image')?>" onclick="insertTag('image',getChildByTagName(this.parentNode,'TEXTAREA').form,'<?=$var['name']?>');" src="<?=_IMG_URL?>btn.image.gif">
	<img alt="<?=sys::translate('sys::flash')?>" title="<?=sys::translate('sys::flash')?>" onclick="insertTag('flash',getChildByTagName(this.parentNode,'TEXTAREA').form,'<?=$var['name']?>');" src="<?=_IMG_URL?>btn.flash.gif">
	<img alt="<?=sys::translate('sys::preview')?>" onclick="previewBB(getChildByTagName(this.parentNode,'TEXTAREA'),'<?=_ROOT_URL?>')" title="<?=sys::translate('sys::preview')?>" src="<?=_IMG_URL?>btn.preview.gif">
	<textarea name="<?=$var['name']?>" title="<?=$var['title']?>" alt="<?=$var['type']?>" <?=$var['html_params']?> <?if($var['req']):?> required="true" <?endif;?>><?=$var['value']?></textarea>
</div>


