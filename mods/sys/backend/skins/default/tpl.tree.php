<?=sys::useJs('sys::tree');?>
<?if(isset($var['root'])):?>
	<?if(!isset($var['root']['url']))$var['root']['url']=false;?>
	<nobr><a class="active" href="<?=$var['root']['url']?>" id="tree_link_0" onclick="<?=$var['root']['onclick']?>; selectNode(0); return false;"><?=$var['root']['title']?></a></nobr>
<?endif;?>
<ul id="tree_list_<?=$var['parent_id']?>" class="tree">
	<?if(isset($var['objects'])):?>

	<?foreach ($var['objects'] as $obj):?>



		<?
		if($obj['children'])
		{
			$img='plus.gif';
			$img_onclick="changeIcon(".$obj['id'].",'".$obj['children_url']."',true);";
		}
		else
		{
			$img='pixel.gif';
			$img_onclick=false;
		}
		if(!isset($obj['url']))$obj['url']=false;
		?>
		<li id="tree_node_<?=$obj['id']?>">
			<nobr><img onclick="<?=$img_onclick?>" id="tree_img_<?=$obj['id']?>" width="9" height="9" alt="" title="" src="<?=$skin_url?>img/<?=$img?>"> <a href="<?=$obj['url']?>" id="tree_link_<?=$obj['id']?>" onclick="<?=$obj['onclick']?>; selectNode(<?=$obj['id']?>); return false;"><?=$obj['title']?></a></nobr>
		</li>
	<?endforeach;?>
	<?endif;?>
</ul>