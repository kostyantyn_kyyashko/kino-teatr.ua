<style type="text/css">
	table.err{
		width:auto;
		background:#D4D0C8;
		font:12px Verdana;
		margin:4px 4px 4px 0;
		border-collapse:collapse;
	}

	table.err th, table.err td{
		padding:4px;
		border:1px solid black;
		text-align:left;
		vertical-align:top;
		color:red;
	}
	
	table.err th{
		background:gray;
		color:#ffffff;
	}
	
	#err{
		position:absolute;
		top:0px;
		left:0px;
		z-index:1000;
		background:#ffffff;
		width:100%;
	}
</style>
<div id="err" ondblclick="this.style.display='none'">
	<?foreach ($var as $obj):?>
		<table style="margin-left:<?=($obj['level']*20)?>" class="err">
			<?foreach($obj as $key=>$val):?>
				<?if(!is_array($val) && $key!='level'):?>
					<tr>
						<th><?=$key?></th>
						<td><pre><?=$val?></pre></td>
					</tr>
				<?endif;?>
			<?endforeach;?>
		</table>
	<?endforeach;?>
</div>

