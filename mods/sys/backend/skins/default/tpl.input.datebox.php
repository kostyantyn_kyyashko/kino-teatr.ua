<?sys::useLib('sys::gui');?>
<span style="position:relative;">
	<input alt="<?=$var['type']?>" <? if($var['req'] && $var['parse_req']):?> required="true" <?endif;?> title="<?=$var['title']?>" type="text" name="<?=$var['name']?>" size="<?=(mb_strlen(strftime($var['format']))+2)?>" maxlength="<?=mb_strlen(strftime($var['format']))?>" value="<?=$var['value']?>" <?=$var['html_params']?>> 
	<button onclick="showFrameBox(this,'?mod=sys&act=calendar&field=<?=$var['name']?>&format=<?=base64_encode($var['format'])?>'); return false;" title="<?=sys::translate('sys::calendar')?>"><img class="image" alt="<?=sys::translate('sys::calendar')?>" title="<?=sys::translate('sys::calendar')?>" align="absmiddle" src="<?=sys_gui::getIconSrc('sys::btn.calendar.gif')?>"></button>
	<div title="<?=sys::translate('sys::calendar');?>" style="display:none; width:270px; height:200px; position:absolute; z-index:1; left:0px; top:18px;" class="panel">
		<div style="text-align:right;" class="panel title"><img class="hand" onclick="this.parentNode.parentNode.style.display='none';" src="<?=sys_gui::getIconSrc('sys::btn.close.gif')?>" alt="<?=sys::translate('sys::close')?>" title="<?=sys::translate('sys::close')?>"></div>
		<iframe frameborder="0" width="100%" height="<?=(200-18)?>"></iframe>
	</div>
</span>

