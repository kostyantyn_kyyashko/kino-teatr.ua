<select name="<?=$var['name']?><?if($var['multi']):?>[]<?endif?>" <?if($var['multi']):?>multiple<?endif?> alt="<?=$var['type']?>" <?if($var['req'] && $var['parse_req']):?> required="true" <?endif;?> title="<?=$var['title']?>"  <?=$var['html_params']?>>
<?if(isset($var['empty'])):?>
	<option value="<?=$var['empty::value']?>"><?=$var['empty']?></option>
<?endif?>
<?if(is_array($var['values'])):?>
	<?foreach ($var['values'] as $key=>$val):?>
		<?if(!is_array($var['value'])):?>
			<?if($var['value'] && (string)$var['value']==(string)$key)$sel='selected';else $sel=false;?>
		<?else:?>
			<?if(in_array($key, $var['value']))$sel='selected';else $sel=false;?>
		<?endif?>
			<option value="<?=$key?>" <?=$sel?> ><?=$val?></option>
	<?endforeach?>
<?else:?>
	<option></option>
<?endif;?>
</select>

