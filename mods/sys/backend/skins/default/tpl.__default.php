<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title><?=htmlspecialchars($_title)?></title>
	<meta name="keywords" content="<?=htmlspecialchars($_meta_keywords)?>">
	<meta name="description" content="<?=htmlspecialchars($_meta_description)?>">	
	<link rel="icon" href="<?=_IMG_URL?>favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="<?=_IMG_URL?>favicon.ico" type="image/x-icon"> 
	<?=$_meta_other?>
	<style type="text/css">
		@import url(<?=$skin_url?>css.default.css);
	</style>
	<!--[if IE]>
		<style type="text/css">
			@import url(<?=$skin_url?>css.ie.css);
		</style>
	<![endif]-->
	<script type="text/javascript" src="<?=_ROOT_URL?>mods/sys/include/js.sys.js"></script>
	<script type="text/javascript" src="<?=_ROOT_URL?>mods/sys/include/js.gui.js"></script>
	<script type="text/javascript" src="<?=_ROOT_URL?>mods/sys/include/js.form.js"></script>
	<?=sys::useJs('sys::sys');?>
	<?=sys::useJs('sys::gui');?>
    <?if(count($_js_include)):?>
        <?foreach($_js_include as $src):?>
            <script type="text/javascript" src="<?=$src?>"></script>
        <?endforeach;?>
    <?endif?>
	<?if($_js_header):?>
    	<script type="text/javascript">
    	<?=$_js_header?>
    	</script>
    <?endif;?>
</head>
<body class="" onresize="preparePanels(); prepareTable();" onload="prepareDocument(); preparePanels(); prepareTable();">
	<div id="msg" style="display:none"></div>
	<?if(isset($var['panels']) && count($var['panels'])):?>
		<div id="panels">
			<?for($i=0; $i<count($var['panels']); $i++):?>
				<?$panel=$var['panels'][$i];?>
				<?if(!isset($panel['class'])) $panel['class']=false;?>
				<?if(!isset($panel['html_params'])) $panel['html_params']=false;?>
				<?if(!isset($panel['text'])) $panel['text']='&nbsp;';?>
				<?if($i==count($var['panels'])-1) $panel['class'].=' last'?>
				<div class="panel <?=$panel['class']?>" <?=$panel['html_params']?>><?=$panel['text']?></div>
			<?endfor;?>
		</div>
	<?endif;?>
	<table id="framework">
		<?if(isset($var['panels']) && count($var['panels'])):?>
			<tr>
				<td id="panels_back">&nbsp;</td>
			</tr>
		<?endif?>
		<tr>
			<td>
				<table style="height:100%">
					<tr>
						<?if(isset($var['left']) && $var['left']):?>
							<?if(!isset($var['left']['width']))$var['left']['width']='220px';?>
							<?if(!isset($var['left_iframe']['width']))$var['left_iframe']['width']=$var['left']['width'];?>
							<td id="left" class="panel" style="width:<?=$var['left']['width']?>;">
								<iframe width="<?=$var['left_iframe']['width']?>" frameborder="0" name="fra_left" id="fra_left"></iframe>
							</td>
							<td class="hider_v">
								<img alt="<?=sys::translate('sys::hide')?>" title="<?=sys::translate('sys::hide')?>" class="button" src="<?=sys_gui::getIconSrc('sys::btn.left.gif')?>" onclick="hideFrame('left',this,'<?=sys_gui::getIconSrc('sys::btn.left.gif')?>','<?=sys_gui::getIconSrc('sys::btn.right.gif')?>', '<?=sys::translate('sys::hide')?>','<?=sys::translate('sys::show')?>')">
							</td>
							<script type="text/javascript">
								window.frames['fra_left'].document.write('<?=sys::translate('sys::loading')?>');
								document.getElementById('fra_left').src='<?=$var['left']['url']?>';
							</script>
						<?endif;?>
						<td id="wks" style="width:100%;">
							<table style="height:100%;">
								<?if(isset($var['main']) || isset($var['msg'])):?>
									<tr>
										<td id="main">
											<?if(isset($var['msg'])):?>
												<div class="msg"><?=$var['msg']?></div>
											<?endif;?>
											<?if(isset($var['main'])):?>
												<?=$var['main']?>
											<?endif;?>
										</td>
									</tr>
								<?endif?>
								<?if(isset($var['right']) && $var['right']):?>
									<?if(!isset($var['right']['height'])) $var['right']['height']='100%'; else $var['bottom']['height']='100%'; ?>
									<tr>
										<td class="panel" id="right" style="height:<?=$var['right']['height']?>;">
											<iframe height="100%" width="100%" frameborder="0" name="fra_right" id="fra_right"></iframe>
										</td>
									</tr>
									<script type="text/javascript">
										window.frames['fra_right'].document.write('<?=sys::translate('sys::loading')?>');
										document.getElementById('fra_right').src='<?=$var['right']['url']?>';
									</script>
									<?if(isset($var['right']['hider'])):?>
										<tr>
											<td class="hider_h">
												<img alt="<?=sys::translate('sys::hide')?>" title="<?=sys::translate('sys::hide')?>" class="button" src="<?=sys_gui::getIconSrc('sys::btn.up.gif')?>" onclick="hideFrame('right',this,'<?=sys_gui::getIconSrc('sys::btn.up.gif')?>','<?=sys_gui::getIconSrc('sys::btn.down.gif')?>', '<?=sys::translate('sys::hide')?>','<?=sys::translate('sys::show')?>')">
											</td>
										</tr>
									<?endif?>
								<?endif;?>
								<?if(isset($var['bottom'])):?>
									<?if(!isset($var['bottom']['height']))$var['bottom']['height']='300px';?>
									<?if(isset($var['bottom']['hider'])):?>
										<tr>
											<td class="hider_h">
												<img alt="<?=sys::translate('sys::hide')?>" title="<?=sys::translate('sys::hide')?>" class="button" src="<?=sys_gui::getIconSrc('sys::btn.down.gif')?>" onclick="hideFrame('bottom',this,'<?=sys_gui::getIconSrc('sys::btn.down.gif')?>','<?=sys_gui::getIconSrc('sys::btn.up.gif')?>', '<?=sys::translate('sys::hide')?>','<?=sys::translate('sys::show')?>','<?=$var['bottom']['height']?>','<?=$var['right']['height']?>');">
											</td>
										</tr>
									<?endif?>
									<tr>
										<td class="panel" id="bottom" style="height:<?=$var['bottom']['height']?>">
											<iframe height="100%" width="100%" frameborder="0" name="fra_bottom" id="fra_bottom"></iframe>
										</td>
									</tr>
									<script type="text/javascript">
										window.frames['fra_bottom'].document.write('<?=sys::translate('sys::loading')?>');
										document.getElementById('fra_bottom').src='<?=$var['bottom']['url']?>';
									</script>
								<?endif?>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
<script type="text/javascript">
	prepareTable();
	preparePanels();
	//prepareDocument();
	/*
	<?if(isset($var['left'])):?>
		window.frames['fra_left'].document.write('<?=sys::translate('sys::loading')?>');
		document.getElementById('fra_left').src='<?=$var['left']['url']?>';
	<?endif?>
	
	<?if(isset($var['right'])):?>
		window.frames['fra_right'].document.write('<?=sys::translate('sys::loading')?>');
		document.getElementById('fra_right').src='<?=$var['right']['url']?>';
	<?endif?>
	
	<?if(isset($var['bottom'])):?>
		window.frames['fra_bottom'].document.write('<?=sys::translate('sys::loading')?>');
		document.getElementById('fra_bottom').src='<?=$var['bottom']['url']?>';
	<?endif?>
	*/
	<?if($_err):?>
		alert("<?=sys::parseErr($_err)?>");
	<?endif;?>
	
	<?if($_js_footer):?>
		<?=$_js_footer?>
	<?endif;?>
	
	<?if(count($_hotkeys)):?>
		document.onkeydown=function(e)
		{
			if (e) 
				event=e;
			<?foreach($_hotkeys as $key=>$action):?>
				if ((event.ctrlKey) && event.keyCode==<?=ord($key)?>)
				{
					if(e)
					{
						event.preventDefault();
						event.stopPropagation();	
					}
					event.returnValue=false;
					event.cancelBubble=true;	
					
			     	<?=$action?>
			     	return false;
				}
			<?endforeach?>
		};
		
		document.onkeypress=function(e)
		{
			<?foreach($_hotkeys as $key=>$action):?>
				if ((event.ctrlKey) && event.keyCode==<?=ord($key)?>)
				{
					if (e) 
					{
						e.preventDefault();
						e.stopPropagation();
					}
					event.returnValue=false;
					event.cancelBubble=true;
				}
			<?endforeach?>
		};
	<?endif?>
</script>



