<script type="text/javascript" src="/mods/main/skins/default/js/jquery.js"></script>
<style>
div.form {
    position:relative;
}
div.form > div.locked {
    display:none;
    position:absolute;
    top:0;
    left:0;
    width:600px;
    height:330px;
    background:#fff;
    filter:progid:DXImageTransform.Microsoft.Alpha(opacity=70);
    -moz-opacity: 0.7;
    -khtml-opacity: 0.7;
    opacity: 0.7;
}
table.form {
    width:auto;
}
table.form th,
table.form td {
    width:auto;
    padding:5px 10px;
}
table.form th {
    text-align:right;
    padding-right:0;
}
table.form td > input.text {
    width:400px;
}
table.form td > textarea {
    width:400px;
    height:200px;
    resize:none;
}
table.form td > input.submit {
    width:400px;
    background:#9FABC1;
    color:#fff;
    padding:10px;
    text-transform:uppercase;
    cursor:pointer;
    outline:none;
}
table.form tr.response {
    display:none;
}
table.form tr.response div.inner {
    width:400px;
    height:200px;
    border:1px solid #9FABC1;
    background:#F6F6FB;
    overflow-y:auto;
}
table.form tr.response div.inner > div.item {
    padding:2px 10px;
}
table.form tr.response div.inner > div.item > b.sended {
    color:#0bb603;
    font-size:11px;
}
table.form tr.response div.inner > div.item > b.sended.complete {
    background:#0bb603;
    color:#fff;
    padding:3px 10px;
}
table.form tr.response div.inner > div.item > b.sended.error {
    color:#f00;
}
table.form tr.response.error div.inner {
    border:1px solid #f00;
    background:#f9e3e3;
}
table.form tr.response.error div.inner > div.item.error {
    color:#f00;
}
</style><br />
<div class="form">
    <table class="form">
        <tr>
            <th>E-mail</th>
            <td><input type="text" class="text" name="from_email" value="o.boiko@kino-teatr.ua" /></td>
        </tr>
        <tr>
            <th>Имя</th>
            <td><input type="text" class="text" name="from_name" value="Kino-teatr.ua" /></td>
        </tr>
        <tr>
            <th>Тема письма</th>
            <td><input type="text" class="text" name="subject" value="Тестовое письмо" /></td>
        </tr>
        <tr>
            <th>Содержание письма</th>
            <td><textarea name="body">Тестовое содержание</textarea></td>
        </tr>
        <tr>
            <th></th>
            <td><input type="submit" class="submit" name="send" value="начать рассылку" style="width:300px; font-weight:bold;" />&nbsp;<input type="submit" class="submit" name="send_test" value="тест" style="width:96px;" /></td>
        </tr>
        <tr class="response">
            <th>Ответ</th>
            <td><div class="inner"></div></td>
        </tr>
    </table>
    <div class="locked"></div>
</div>


<script>
$(function(){
    
    
    var users = [<?php $users = getUserList();
        reset($users);
        foreach ($users as $val) {
            echo sprintf('
            {id:%d,name:"%s",email:"%s"},', $val['id'], $val['name'], $val['email']);
        }
        ?>];
    var users_test = [<?php $users = getTestUserList();
        reset($users);
        foreach ($users as $val) {
            echo sprintf('
            {id:%d,name:"%s",email:"%s"},', $val['id'], $val['name'], $val['email']);
        }
        ?>];
    
    
    $('input[name=send]').click(function(e){
        return window.confirm('Подтверждение рассылки') ? __submitDelivery(false) : false;
    });
    $('input[name=send_test]').click(function(e){
        __submitDelivery(true);
    });
    
    
    var __submitDelivery = function(test) {
        
        $('.locked').css('display', 'block');
        $('.response').css('display', 'none');
        $('.response').removeClass('error');
        $('.inner').html('');
        
        var mail = __prepareMail();
        
        if (mail.status) {
            __prepareToSendMail(mail, test);
        } else {
            $('.locked').css('display', 'none');
            $('.response').css('display', 'table-row');
            $('.response').addClass('error');
            $('.inner').html(mail.msg);
        };
        
    };
    
    
    var __prepareToSendMail = function(mail, test) {
        
        $('.locked').css('display', 'block');
        $('.response').css('display', 'table-row');
        $('.response').removeClass('error');
        
        var _users = test ? users_test.slice(0) : users.slice(0);
        __tryToSendMail(_users, mail)
        
    };
    
    
    var __tryToSendMail = function(_users, mail) {
        
        if (_users.length) {
            var u = _users.shift();
            $.post(window.location.origin + '/mods/sys/phpmailer/post-mail.php', {
                from_email: mail.email,
                from_name: mail.name,
                to_email: u.email,
                to_name: u.name,
                subject: mail.subject,
                body: mail.body
            }, function(data, status){
                if (status == 'success') {
                    data = eval('('+data+')');
                    if (data.status) {
                        $('.inner').prepend(__parseResponseItem('<b class="sended">sended</b> ' + u.name + ' [' + u.email + ']'));
                    } else {
                        $('.inner').prepend(__parseResponseItem('<b class="sended error">error</b> ' + u.name + ' [' + u.email + ']<br /><b class="sended error" style="font-size:9px;">'+data.msg+'</b>'));
                    };
                };
            }).error(function(data, status){
                $('.inner').prepend(__parseResponseItem('<b class="sended error">error</b> ' + u.name + ' [' + u.email + ']<br /><b class="sended error" style="font-size:9px;">See server logs for detailed information</b>'));
            }).complete(function(data, status){
                setTimeout(function(){
                    __tryToSendMail(_users, mail);
                }, 200);
            });
        } else {
            $('.inner').prepend(__parseResponseItem('<b class="sended complete">РАССЫЛКА ЗАВЕРШЕНА!</b>'));
            $('.locked').css('display', 'none');
        };
        
    };
    
    
    var __parseResponseItem = function(msg, error) {
        
        var div = $('<div class="item"></div>');
        div.html(msg);
        if (error) div.addClass('error');
        return div;
        
    };
    
    
    var __prepareMail = function() {
        
        var mail = {
            status: true,
            msg: ''
        };
        
        var email = $('input[name=from_email]').val();
        var name = $('input[name=from_name]').val();
        var subject = $('input[name=subject]').val();
        var body = $('textarea[name=body]').val();
        
        if (!email) {
            var msg = __parseResponseItem('Введите E-mail', true);
            mail.msg += msg[0].outerHTML;
            mail.status = false;
        };
        
        if (!name) {
            var msg = __parseResponseItem('Введите Имя', true);
            mail.msg += msg[0].outerHTML;
            mail.status = false;
        };
        
        if (!subject) {
            var msg = __parseResponseItem('Введите тему письма', true);
            mail.msg += msg[0].outerHTML;
            mail.status = false;
        };
        
        if (!body) {
            var msg = __parseResponseItem('Введите содержание письма', true);
            mail.msg += msg[0].outerHTML;
            mail.status = false;
        };
        
        if (mail.status) {
            mail = {
                status: true,
                email: email,
                name: name,
                subject: subject,
                body: body
            };
        };
        
        return mail;
        
    };
    
    
});
</script>


<?php

function getUserList() {
    
    global $_db;
    
	$r = $_db->query("
		SELECT
            usr.*,
			usr_dat.main_first_name AS `firstname`,
			usr_dat.main_last_name AS `lastname`
		FROM `#__sys_users` AS `usr`
		LEFT JOIN `#__sys_users_data` AS `usr_dat`
		ON usr_dat.user_id=usr.id
	");
    
    $users = array();

	while ($user = $_db->fetchAssoc($r)) {
        $info = prepareUser($user);
        if ($info) $users[] = $info;
	}
    
    return $users;
    
}

function getTestUserList() {
    
    global $_db;
    
    $r = $_db->query("
        SELECT
            `r`.`group_id` AS gip
        FROM
            `#__sys_groups_rights` AS `r`
        WHERE
            `r`.`action_id`=148
    ");
    $ids = '';
	while ($group = $_db->fetchAssoc($r))
        $ids .= $group['gip'].',';
	if ($ids) $ids = substr($ids, 0, strlen($ids) - 1);
    
	$r = $_db->query("
		SELECT
            usr.*,
			usr_dat.main_first_name AS `firstname`,
			usr_dat.main_last_name AS `lastname`
		FROM
            `#__sys_users` AS `usr`
        
		LEFT JOIN `#__sys_users_data` AS `usr_dat`
		ON usr_dat.user_id=usr.id
        
        WHERE usr.id IN (SELECT 
			usr.id AS uid
		FROM `#__sys_users` AS `usr` 
		
			LEFT JOIN `#__sys_users_groups` AS `grp`
			ON usr.id=grp.user_id
			AND grp.group_id IN ($ids)
		
		WHERE grp.group_id IN ($ids))
		
    ");
    
    $users = array();

	while ($user = $_db->fetchAssoc($r)) {
        $info = prepareUser($user);
        if ($info) $users[] = $info;
	}
    
//if(sys::isDebugIP())
//{	
//	$users = array();
//	$users[] = array("id"=>23551, "name"=>"Mike", "email"=>"happyinvestor@mail.ru");
//}	
    return $users;
    
}


function prepareUser($user) {
    $id = (int)$user['id'];
    $email = addslashes(strip_tags(trim((string)$user['email'])));
    $lname = addslashes(strip_tags((string)$user['lastname']));
    $fname = addslashes(strip_tags((string)$user['firstname']));
    $lname = $lname && $fname
        ? $lname.' '
        : $lname;
    $name = $lname.$fname;
    if (!$name) $name = $email;
    $info = false;
    if ($email && $id) {
        $info = array(
            'id'=>$id,
            'email'=>$email,
            'name'=>$name
        );
    }
    return $info;
}

?>