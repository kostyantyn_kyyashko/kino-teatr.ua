<?sys::useJs('sys::form');?>
<center>
<form id="login" onsubmit="if(checkForm(this)){setMsg('<?=sys::translate('sys::loading')?>');return true;} else return false;" method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
	<center><a target="_blank" href="http://grifix.net"><img style="margin:0 0 20px 0" alt="Grifix" title="Grifix" src="<?=_IMG_URL?>grifix.gif"></a></center>
	<table align="center" class="form">
		<?foreach ($var['fields'] as $field):?>
			<tr>
				<th><?=$field['title']?><?if($field['req']):?>*<?endif;?></th>
				<td><?=$field['html']?></td>
			</tr>
		<?endforeach;?>
		<tr>
			<td colspan="2" style="text-align:center; padding-top:20px">
				<input name="cmd_enter" class="button" type="submit" value="<?=sys::translate('sys::enter')?>">
				<br>
				<small>
					<br>
					<?=sys::translate('sys::optimozed_by_browser')?><br><a target="_blank" href="http://www.mozilla.com/firefox/">Mozilla Firefox</a>
					<br>
					<br>
					&copy; 2008 <a target="_blank" href="http://smike.net.ua">Michael Shapovalov</a>
				</small>
			</td>
		</tr>
	</table>
</form>
</center>

