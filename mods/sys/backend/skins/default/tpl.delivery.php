<table class="table">
	<tr>
		<th>&nbsp;</th>
		<th><?=sys::translate('main::week1')?></th>
		<th><?=sys::translate('main::week2')?></th>
		<th><?=sys::translate('main::week3')?></th>
		<th><?=sys::translate('main::week4')?></th>
		<th><?=sys::translate('main::week5')?></th>
		<th><?=sys::translate('main::week6')?></th>
		<th><?=sys::translate('main::week7')?></th>
		<th class="last">Total</th>
		<th>Пользователи</th>
	</tr>
    <?php
        $total = 0;
        $total1 = 0;
        $total2 = 0;
        $total3 = 0;
        $total4 = 0;
        $total5 = 0;
        $total6 = 0;
        $total7 = 0;
        reset($var);
        foreach ($var['objects'] as $val):
    ?>
    <tr>
        <td class="name"><?=$val['title']?></td>
        <td><?=$val['w1']?></td>
        <td><?=$val['w2']?></td>
        <td><?=$val['w3']?></td>
        <td><?=$val['w4']?></td>
        <td><?=$val['w5']?></td>
        <td><?=$val['w6']?></td>
        <td><?=$val['w7']?></td>
        <td class="last"><?php
            $total1 += $val['w1'];
            $total2 += $val['w2'];
            $total3 += $val['w3'];
            $total4 += $val['w4'];
            $total5 += $val['w5'];
            $total6 += $val['w6'];
            $total7 += $val['w7'];
            $tt = $val['w1']+$val['w2']+$val['w3']+$val['w4']+$val['w5']+$val['w6']+$val['w7'];
            $total += $tt;
        ?><?=$tt?></td>
        <td><?=$val['users']?></td>
    </tr>
    <? endforeach; ?>
    <tr class="last">
        <td class="name">Total</td>
        <td><?=$total1?></td>
        <td><?=$total2?></td>
        <td><?=$total3?></td>
        <td><?=$total4?></td>
        <td><?=$total5?></td>
        <td><?=$total6?></td>
        <td><?=$total7?></td>
        <td class="last"><?=$total?></td>
        <td></td>
    </tr>
    <tr>
        <td class="name">Пользователи</td>
        <td><?=$var['u1']?></td>
        <td><?=$var['u2']?></td>
        <td><?=$var['u3']?></td>
        <td><?=$var['u4']?></td>
        <td><?=$var['u5']?></td>
        <td><?=$var['u6']?></td>
        <td><?=$var['u7']?></td>
        <td class="last"></td>
        <td><?=$var['users']?></td>
    </tr>
    <tr>
        <td class="name" colspan="9"><?=sys::translate('main::cinema_subscribers')?></td>        
        <td><?=$var['cinema_subscribers']?></td>
    </tr>
</table>
<style>
table.table {
    width:auto;
}
table.table th,
table.table td {
    text-align:center;
    padding:10px 20px;
}
table.table td.name {
    font-weight:bold;
    text-align:left;
    white-space:nowrap;
}
table.table tr.last td {
    border-top:3px solid #404E6A;
}
table.table tr td.last {
    border-left:3px solid #404E6A;
}
</style>