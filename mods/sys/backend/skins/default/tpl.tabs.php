<div class="tabs">
	<nobr>
	<?foreach($var['tabs'] as $key=>$tab):?>
		<?if($key==$var['active']) $class='tab_active';else $class='tab';?>
		<a href="<?=$tab['url']?>" onclick="setMsg('<?=sys::translate('sys::loading')?>');window.location='<?=$tab['url']?>';return false;" class="<?=$class?>">
			<?=$tab['title']?>
		</a>
	<?endforeach;?>
	</nobr>
</div>
