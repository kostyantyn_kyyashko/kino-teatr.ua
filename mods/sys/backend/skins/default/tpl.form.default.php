<?sys::useJs('sys::form');?>
<form <?if($var['id']):?> id="<?=$var['id']?>"<?endif?> onsubmit="return checkForm(this)" method="<?=$var['method']?>" action="<?=$var['action']?>" <?=$var['html_params']?>>
	<input type="hidden" name="_task">
	<?=sys::parseTpl('sys::filelds.default',$var)?>
</form>

