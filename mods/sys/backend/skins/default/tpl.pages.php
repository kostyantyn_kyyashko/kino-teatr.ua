<div class="pages">
	<?if($var['prev_page']):?>
		<a title="<?=sys::translate('sys::prev_page')?>" onclick="setMsg('<?=sys::translate('sys::loading')?>');window.location='<?=$var['prev_page']?>'; return false;" href="<?=$var['prev_page']?>"><b>&laquo;</b></a>
	<?endif?>
	<?if($var['first_page']):?>
		<a onclick="setMsg('<?=sys::translate('sys::loading')?>');window.location='<?=$var['first_page']?>'; return false;" href="<?=$var['first_page']?>">1</a> ...
	<?endif;?>
	<?if(count($var['pages'])>1):?>
		<?foreach ($var['pages'] as $number=>$url):?>
			<?if($number==$var['current_page']):?>
				<strong><?=$number?></strong>
			<?else:?>
				<a onclick="setMsg('<?=sys::translate('sys::loading')?>');window.location='<?=$url?>'; return false;" href="<?=$url?>"><?=$number?></a>
			<?endif?>
		<?endforeach;?>
	<?endif?>
	<?if($var['last_page']):?>
		... <a onclick="setMsg('<?=sys::translate('sys::loading')?>');window.location='<?=$var['last_page']?>'; return false;" href="<?=$var['last_page']?>"><?=$var['last_page_number']?></a>
	<?endif?>
	<?if($var['next_page']):?>
		<a title="<?=sys::translate('sys::next_page')?>" onclick="setMsg('<?=sys::translate('sys::loading')?>');window.location='<?=$var['next_page']?>'; return false;" href="<?=$var['next_page']?>"><b>&raquo;</b></a>
	<?endif?>
	<?=sys_form::parseField('on_page',$var['on_page'])?> <?=sys::translate('sys::on_page')?>.
	<?=sys::translate('sys::total')?>:<?=$var['num_rows']?>
</div>