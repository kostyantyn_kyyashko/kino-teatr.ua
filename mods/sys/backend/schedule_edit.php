<?
function sys_schedule_edit()
{
	if(!sys::checkAccess('sys::scheduler'))
		return 403;
		
	global $_db, $_err, $_cfg;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::mods');
	
	sys::filterGet('id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
	
	if(!$_GET['id'])
		return 404;
		
	$tbl='sys_mods_tasks';
	
	$default=array();
	$record=$_db->getRecord($tbl,$_GET['id']);
	if(!$record)
		return 404;
	$mod_name=$_db->getValue('sys_mods','name',$record['mod_id']);
	$title=sys::translate($mod_name.'::'.$record['name']);

	$values=sys::setValues($record, $default);
	if(!isset($_POST['info']))
		$values['info'].=sys::getHlp('sys::schedule');
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//info
	
	$fields['info']['input']="info";
	$fields['info']['type']="html";
	$fields['info']['table']=false;
	$fields['info']['html_params']=false;
	
	
	//min
	$fields['min']['input']='textbox';
	$fields['min']['type']='text';
	$fields['min']['req']=true;
	$fields['min']['table']=$tbl;
	$fields['min']['title']=sys::translate('sys::minute');
	
	//hour
	$fields['hour']['input']='textbox';
	$fields['hour']['type']='text';
	$fields['hour']['req']=true;
	$fields['hour']['table']=$tbl;
	$fields['hour']['title']=sys::translate('sys::hour');
	
	//mday
	$fields['mday']['input']='textbox';
	$fields['mday']['type']='text';
	$fields['mday']['req']=true;
	$fields['mday']['table']=$tbl;
	$fields['mday']['title']=sys::translate('sys::month_day');
	
	//month
	$fields['month']['input']='textbox';
	$fields['month']['type']='text';
	$fields['month']['req']=true;
	$fields['month']['table']=$tbl;
	$fields['month']['title']=sys::translate('sys::month');
	
	//wday
	$fields['wday']['input']='textbox';
	$fields['wday']['type']='text';
	$fields['wday']['req']=true;
	$fields['wday']['table']=$tbl;
	$fields['wday']['title']=sys::translate('sys::week_day');
	
	//on
	$fields['on']['input']='checkbox';
	$fields['on']['type']='bool';
	$fields['on']['title']=sys::translate('sys::is_on');
	$fields['on']['table']=$tbl;
	
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
				$_err=sys::translate('sys::db_error').$_db->err;
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>