<?
function sys_mod_tasks_table()
{
	if(!sys::checkAccess('sys::mod_edit'))
		return 403;
		
	global $_db, $_err;
	
	sys::useLib('sys::pages');
	sys::useLib('sys::mods');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.asc');
	sys::filterGet('mod_id','int');
		
	if(!$_GET['mod_id'])
		return 404;
		
	$tbl='sys_mods_tasks';
	$edit_url='?mod=sys&act=mod_task_edit';
	$win_params=sys::setWinParams('600','400');
	$add_title=sys::translate('sys::add_task');
	
	$order_where=$where=" `mod_id`=".intval($_GET['mod_id'])."";
	$mod=$_db->getRecord('sys_mods',$_GET['mod_id']);
	
	if(!$mod)
		return 404;
	
	$title=$mod['name'].' :: '.sys::translate('sys::tasks');
	
	$q="SELECT 
			`id`, 
			`name`,
			`min`,
			`hour`,
			`mday`,
			`month`,
			`wday`,
			`order_number`,
			`on`,
			`last_run`
		FROM `#__".$tbl."` 
		WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			sys_mods::deleteTask($task[1]);
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					sys_mods::deleteTask($key);
				}
			}
					
		}
		
		//Сдвиг вверх
		if($task[0]=='moveUp')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveDown($tbl,$task[1],$order_where);
			else
				$_db->moveUp($tbl,$task[1],$order_where);
		}
		
		//Сдвиг вниз
		if($task[0]=='moveDown')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveUp($tbl,$task[1],$order_where);
			else
				$_db->moveDown($tbl,$task[1],$order_where);
		}
		
		//Упорядочение нумерации
		if($_POST['_task']=='orderNumbers')
			$_db->orderNumbers($tbl, $order_where);
			
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=sys::translate($mod['name'].'::'.$obj['name']);
		$obj['last_run']=sys::db2Date($obj['last_run'],true);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['width']=150;
	
	$table['fields']['title']['type']='text';
	$table['fields']['title']['width']=150;
	
	
	$table['fields']['order_number']['type']='textbox';
	$table['fields']['order_number']['order']=true;
	$table['fields']['order_number']['html_params']='size="8" onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=set~'.$tbl.'~order_number~%id%~\'+this.value+\'~int\')"';
	
	$table['fields']['on']['type']='checkbox';
	$table['fields']['on']['title']=sys::translate('sys::is_on');
	$table['fields']['on']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~on~%id%~%on%\')"';
	$table['fields']['on']['width']=40;
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&mode=add&mod_id=".$_GET['mod_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle(sys::translate($title));
	
	$result['panels'][0]['text']=sys::translate($title);
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=sys_mods::showTabs($_GET['mod_id'],'tasks');
	$result['panels'][]=$panel;	
	unset($panel);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
