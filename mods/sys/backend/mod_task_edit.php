<?
function sys_mod_task_edit()
{
	if(!sys::checkAccess('sys::mod_edit'))
		return 403;
		
	global $_db, $_err, $_cfg;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::mods');
	
	sys::filterGet('id','int');
	sys::filterGet('mod_id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='sys_mods_tasks';
	$default['mod_id']=$_GET['mod_id'];
	$default['order_number']=$_db->getMaxOrderNumber($tbl);
	$default['min']='*';
	$default['hour']='*';
	$default['mday']='*';
	$default['month']='*';
	$default['wday']='*';
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=array();
		$title=sys::translate('sys::new_task');
	}
	
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//mod_id
	$fields['mod_id']['input']='hidden';
	$fields['mod_id']['type']='int';
	$fields['mod_id']['req']=true;
	$fields['mod_id']['table']=$tbl;
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='name';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['max_chars']=54;
	$fields['name']['html_params']='style="width:100%" maxlength="16"';
	$fields['name']['unique']=true;
	$fields['name']['unique::where']="`id`!=".intval($_GET['id'])." AND `mod_id`=".intval($values['mod_id'])."";
	
	//min
	$fields['min']['input']='textbox';
	$fields['min']['type']='text';
	$fields['min']['table']=$tbl;
	$fields['min']['title']=sys::translate('sys::minute');
	
	//hour
	$fields['hour']['input']='textbox';
	$fields['hour']['type']='text';
	$fields['hour']['table']=$tbl;
	$fields['hour']['title']=sys::translate('sys::hour');
	
	//mday
	$fields['mday']['input']='textbox';
	$fields['mday']['type']='text';
	$fields['mday']['table']=$tbl;
	$fields['mday']['title']=sys::translate('sys::month_day');
	
	//month
	$fields['month']['input']='textbox';
	$fields['month']['type']='text';
	$fields['month']['table']=$tbl;
	$fields['month']['title']=sys::translate('sys::month');
	
	//wday
	$fields['wday']['input']='textbox';
	$fields['wday']['type']='text';
	$fields['wday']['table']=$tbl;
	$fields['wday']['title']=sys::translate('sys::week_day');
	
	//info
	$fields['info']['input']='fck';
	$fields['info']['type']='html';
	$fields['info']['table']=$tbl;
	$fields['info']['height']='300px';
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	//on
	$fields['on']['input']='checkbox';
	$fields['on']['type']='bool';
	$fields['on']['title']=sys::translate('sys::is_on');
	$fields['on']['table']=$tbl;
	
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>