<?
function sys_err_403()
{
	sys::setTitle(sys::translate('sys::err_403'));
	$result['panels'][0]['text']=sys_gui::showNavPath('sys::err_403');
	$result['msg']=sys::translate('sys::err_403_text');
	sys::jsAlert(sys::translate('sys::err_403_text'));
	return $result;
}
?>