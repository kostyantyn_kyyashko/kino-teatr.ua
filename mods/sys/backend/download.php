<?
function sys_download()
{
	if(!isset($_GET['file']))
		return 404;
	$file=base64_decode($_GET['file']);
	
	if(!sys::checkPublicPath($file))
		return 401;
	
	sys::downloadFile($file);
}
?>
