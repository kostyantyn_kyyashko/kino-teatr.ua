<?
function sys_group_rights()
{
	if(!sys::checkAccess('sys::group_edit'))
		return 403;
		
	global $_db, $_err;
	sys::useLib('sys::users');
	
	sys::filterGet('mode');
	sys::filterGet('group_id','int');
	
	if(!$_GET['group_id'])
		return 404;
	
	if(!$_GET['mode'] || ($_GET['mode']!='frontend') && $_GET['mode']!='backend')
		$_GET['mode']='frontend';
	
	$title=$_db->getValue('sys_groups','name',intval($_GET['group_id']));
	if(!$title)
		return 404;
	$title.=' :: '.sys::translate('sys::rights');
		
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=sys_users::showGroupTabs($_GET['group_id'],'rights');
	$result['panels'][]=$panel;	
	unset($panel);
	
	$result['left']['url']='?mod=sys&act=group_rights_tree&mode='.$_GET['mode'].'&group_id='.$_GET['group_id'];
	$result['right']['url']='?mod=sys&act=group_rights_table&mode='.$_GET['mode'].'&parent_id=0&group_id='.$_GET['group_id'];
	return $result;
}
?>