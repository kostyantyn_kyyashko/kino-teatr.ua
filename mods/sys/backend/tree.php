<?
function sys_tree()
{
	
	if(!sys::checkAccess('sys::system_tree'))
		return 403;
		
	sys::useLib('sys::mods');
	
	$title=sys::translate('sys::system_tree');
		
	sys::setTitle($title);
	$result['panels'][]['text']=sys_gui::showNavPath('sys::system_tree');
	
	$result['left']['url']='?mod=sys&act=tree_tree';
	$result['right']['url']='?mod=sys&act=tree_table';
	return $result;
}
?>