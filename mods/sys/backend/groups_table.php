<?
function sys_groups_table()
{
	if(!sys::checkAccess('sys::groups'))
		return 403;
		
	global $_db, $_err;
	sys::useLib('sys::pages');
	sys::useLib('sys::users');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','order_number.asc');
		
	$tbl='sys_groups';
	$edit_url='?mod=sys&act=group_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('sys::add_group');
	$order_where=false;
	
	$nav_point='sys::groups';
	
	$q="SELECT 
			`id`, 
			`name`,
			`backend`,
			`order_number`
		FROM `#__".$tbl."`";
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			sys_users::deleteGroup($task[1]);
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					sys_users::deleteGroup($key);
				}
			}
					
		}	
		
		//Сдвиг вверх
		if($task[0]=='moveUp')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveDown($tbl,$task[1],$order_where);
			else
				$_db->moveUp($tbl,$task[1],$order_where);
		}
		
		//Сдвиг вниз
		if($task[0]=='moveDown')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveUp($tbl,$task[1],$order_where);
			else
				$_db->moveDown($tbl,$task[1],$order_where);
		}
		
		//Упорядочение нумерации
		if($_POST['_task']=='orderNumbers')
			$_db->orderNumbers($tbl, $order_where);
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	$table['params'][7]['backend']['html_params']='disabled';
	$table['params'][7]['delete']['type']='text';
	$table['params'][8]['backend']['html_params']='disabled';
	$table['params'][8]['delete']['type']='text';
	$table['params'][2]['backend']['html_params']='disabled';
	$table['params'][2]['delete']['type']='text';
	
	while($obj=$_db->fetchAssoc($r))
	{
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['rights']['type']='button';
	$table['fields']['rights']['onclick']="window.open('?mod=sys&act=group_rights&group_id=%id%','','".$win_params."');return false";
	$table['fields']['rights']['url']='?mod=sys&act=group_rights&group_id=%id%';
	
	$table['fields']['move_up']['type']='button';
	$table['fields']['move_up']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveUp.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_up']['image']='sys::btn.desc.gif';
	
	$table['fields']['move_down']['type']='button';
	$table['fields']['move_down']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveDown.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_down']['image']='sys::btn.asc.gif';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;
	
	$table['fields']['backend']['type']='checkbox';
	$table['fields']['backend']['title']=sys::translate('sys::bcknd');
	$table['fields']['backend']['alt']=sys::translate('sys::backend');
	$table['fields']['backend']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~backend~%id%~%backend%\')"';
	$table['fields']['backend']['width']=40;
	
	$table['fields']['order_number']['type']='textbox';
	$table['fields']['order_number']['order']=true;
	$table['fields']['order_number']['html_params']='size="8" onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=set~'.$tbl.'~order_number~%id%~\'+this.value+\'~int\')"';
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
