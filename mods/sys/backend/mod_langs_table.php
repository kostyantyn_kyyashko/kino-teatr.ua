<?
function sys_mod_langs_table()
{
	if(!sys::checkAccess('sys::mod_edit'))
		return 403;
		
	global $_db, $_err;
	
	sys::useLib('sys::mods');
	sys::useLib('sys::form');
	
	sys::filterGet('mod_id','int');
		
	if(!$_GET['mod_id'])
		return 404;
		
	$edit_url='?mod=sys&act=mod_lang_edit';
	$win_params=sys::setWinParams('700','500');
	
	$mod=$_db->getRecord('sys_mods',$_GET['mod_id']);
	
	if(!$mod)
		return 404;
	
	$title=$mod['name'].' :: '.sys::translate('sys::langs');
	//===============================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$files=sys::scanDir(_MODS_DIR.$mod['name'].'/lang/');
	foreach ($files as $file)
	{
		$obj['name']=$file;
		$table['records'][$file]=$obj;
	}
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&file=%name%&mod_id=".$_GET['mod_id']."','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&file=%name%';
	
	$table['fields']['name']['type']='text';
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle(sys::translate($title));
	
	$result['panels'][0]['text']=sys::translate($title);
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=sys_mods::showTabs($_GET['mod_id'],'langs');
	$result['panels'][]=$panel;	
	unset($panel);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
