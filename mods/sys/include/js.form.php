function checkForm(form)
{
	err="";
	for(i=0; i<form.elements.length; i++)
	{
		if(form.elements[i].alt=='fck')
			continue;
		field_err="";
		if(form.elements[i].required || form.elements[i].attributes.required)
		{
			if(!form.elements[i].value)
			{
				field_err="<?=sys::translate('sys::not_value')?>";
				err+=form.elements[i].title+": "+field_err+"\n";	
			}
		}
		
		if(form.elements[i].alt)
		{
			if(form.elements[i].alt=='int' && isNaN(form.elements[i].value))
			{
				field_err="<?=sys::translate('sys::must_be_numeric')?>";
				err+=form.elements[i].title+": "+field_err+"\n";
			}
		}
		
		if(field_err)
		{
			if(form.elements[i])
				form.elements[i].className+=' err';
		}	
		else
		{
			if(form.elements[i] && form.elements[i].type!='button' && form.elements[i].type!='submit' && form.elements[i].type!='checkbox')
				form.elements[i].className=form.elements[i].className.replace(' err','');
		}
		
	}
	
	if(!err)
		return true;
	alert(err);
	return false;
}

function searchInList(obj,num_of_chars,field,ajx_url)
{
	list=document.getElementById('_ajx_'+field+'_list');
	box=list.parentNode;
	if(obj.value.length>num_of_chars || obj.value=='*')
	{
		box.style.display='block';
		list.innerHTML='<?=sys::translate('sys::searching')?>';
		ajaxRequest(ajx_url+"&field="+field+"&keywords="+obj.value);	
	}
}
