<?php
class sys_letters
{		
	static function deleteLetter($letter_id)
	{
		global $_db;
		$_db->delete('sys_letters',"`id`='".$letter_id."'");
	}
	
	static function sendLetter($letter_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT * FROM `#__sys_letters`
			WHERE `id`='".mysql_real_escape_string($letter_id)."'
		");
		$letter=$_db->fetchAssoc($r);
		if(sys::sendMail($letter['from_email'],$letter['from_name'],$letter['to_email'],$letter['to_name'],$letter['subject'],$letter['text'],$letter['html'],$letter['att_files'],$letter['charset']))
		{
			$_db->query("DELETE FROM `#__sys_letters` WHERE `id`='".mysql_real_escape_string($letter_id)."'");
			return true;
		}
		else 
			return false;
	}
	
	static function sendLettersPack($pack_size=100)
	{
		global $_db, $_cfg;
		$date=gmdate('Y-m-d H:i:s');
		$_db->query("LOCK TABLES `#__sys_letters` WRITE");
		$r=$_db->query("
			SELECT * FROM `#__sys_letters`
			WHERE `date`<='".$date."'
			ORDER BY `date`
			LIMIT 0,".intval($pack_size)."
		");
		$letters_id='';
		while ($letter=$_db->fetchAssoc($r))
		{
			$letters[]=$letter;
			$letters_id.="'".$letter['id']."',";
		}
		$letters_id=sys::cutStrRight($letters_id,1);
		if($letters_id)
		{
			$_db->query("
				DELETE FROM `#__sys_letters`
				WHERE `id` IN(".$letters_id.")
			");
		}
		
		$_db->query("UNLOCK TABLES");
		
		foreach ($letters as $letter)
		{
            $method = $letter['method'];
            $response = sys::$method($letter['from_email'],$letter['from_name'],$letter['to_email'],$letter['to_name'],$letter['subject'],$letter['text'],$letter['html'],$letter['att_files'],$letter['charset']);
			if(!$response)
			{
				sys::addMail($letter['from_email'],$letter['from_name'],$letter['to_email'],$letter['to_name'],$letter['subject'],$letter['text'],$letter['html'],$letter['att_files'],$letter['charset'],false,$method);
			}
		}
		
	}
}
?>