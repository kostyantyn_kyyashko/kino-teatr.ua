<?
register_shutdown_function('session_write_close');

function on_session_open($path, $name)
{
	global $_db, $_cfg;
	$_db->query("DELETE FROM `#__sys_sess` WHERE `date_updated` < '".gmdate('Y-m-d H:i:s',time()-$_cfg['sys::session_minutes']*30)."'");
}

function on_session_close()
{
}

function on_session_read($id)
{
	global $_db;
	$query = "SELECT `data` FROM `#__sys_sess` WHERE `id` = '".mysql_real_escape_string($id)."'";
	$r=$_db->query($query);
	list($result)=$_db->fetchArray($r);
	return $result;
}

function on_session_write($id, $data)
{
	global $_db, $_cfg;
	$r=$_db->query("SELECT * FROM `#__sys_sess` WHERE `id`='".mysql_real_escape_string($id)."'");
	$session=$_db->fetchAssoc($r);
	if($session['id'])
	{
		$query = "UPDATE `#__sys_sess`
				SET `data` = '".mysql_real_escape_string($data)."',
				`date_updated` = '".gmdate('Y-m-d H:i:s')."',
				`user_id`='".$_SESSION['sys::user_id']."',
				`location`='".mysql_real_escape_string($_SERVER['REQUEST_URI'])."'
				WHERE `id` = '".$session['id']."'";
	}
	else
	{
		$query = "INSERT INTO `#__sys_sess`
				(`date_started`,`date_updated`, `id`, `data`,`user_id`,`location`)
				VALUES(
				'".gmdate('Y-m-d H:i:s')."', 
				'".gmdate('Y-m-d H:i:s')."', 
				'".$id."',
				'".mysql_real_escape_string($data)."',
				'".$_SESSION['sys::user_id']."',
				'".mysql_real_escape_string($_SERVER['REQUEST_URI'])."'
				)";
	}
	$_db->query($query);
}

function on_session_destroy($id)
{
	global $_db;
	$_db->query("DELETE FROM `#__sys_sess` WHERE `id` = '".mysql_real_escape_string($id)."'");
 	return true;
}

function on_session_gc($maxlifetime)
{
	global $_db, $_cfg;
	$_db->query("DELETE FROM `#__sys_sess` WHERE `date_updated` < '".gmdate('Y-m-d H:i:s',time()-$_cfg['sys::session_minutes']*30)."'");
	return true;
}

session_set_save_handler
(
	"on_session_open",
	"on_session_close",
	"on_session_read",
	"on_session_write",
	"on_session_destroy",
 	"on_session_gc"
 );
?>