<?
class cls_sys_tree
{
	public $max_level; //Максимальная глубина дерева
	public $nodes; //Узлы дерева
	public $tree_modes; //Упорядоченые узлы дерева
	function sys_tree()
	{
		$this->max_level=0;
		return true;
	}
	/*
	$nodes['redhat']['parent_id']='lin';
		$nodes['redhat']['order_number']=2;
		$nodes['redhat']['name']='Red Hat Linux';
		
	$nodes['win']['parent_id']=0;
	$nodes['win']['order_number']=1;
	$nodes['win']['name']='Windows';
	
		$nodes['win98']['parent_id']='win';
		$nodes['win98']['order_number']=1;
		$nodes['win98']['name']='Windows 98';
		
		$nodes['win2k']['parent_id']='win';
		$nodes['win2k']['order_number']='2';
		$nodes['win2k']['name']='Windows 2000';
	
		$nodes['obsd']['parent_id']='bsd';
		$nodes['obsd']['order_number']=2;
		$nodes['obsd']['name']='OpenBSD';
		
		
	$nodes['lin']['parent_id']=0;
	$nodes['lin']['order_number']=2;
	$nodes['lin']['name']='Linux';
	
		$nodes['mandrake']['parent_id']='lin';
		$nodes['mandrake']['order_number']=1;
		$nodes['mandrake']['name']='Mandrake Linux';
		
		$nodes['mediaPL']['parent_id']='winXP';
		$nodes['mediaPL']['order_number']=3;
		$nodes['mediaPL']['name']='Wimdows Media Player';
		
		$nodes['winXP']['parent_id']='win';
		$nodes['winXP']['order_number']='3';
		$nodes['winXP']['name']='Windows XP';
		
	$nodes['bsd']['parent_id']=0;
	$nodes['bsd']['order_number']=2;
	$nodes['bsd']['name']='BSD';
	
		$nodes['fbsd']['parent_id']='bsd';
		$nodes['fbsd']['order_number']=1;
		$nodes['fbsd']['name']='FreeBSD';
	
	*/
	//=============================================================
	function make($nodes, $root_node=false)
	{
		
		$this->max_level=0;
		unset($this->tree_modes, $this->nodes);
		$this->nodes=$nodes;
		
		if($root_node)
			$this->nodes[$root_node['id']]=$root_node;
		foreach($this->nodes as $id=>$node)
		{
			
			//Получить уровень узла в дереве
			$this->nodes[$id]['level']=$this->getNodeLevel($id);
			if(!$this->nodes[$id]['level'])
				$this->nodes[$id]['level']=0;
				
			//Получить дочерние узлы узла
			$this->getNodeChildren($id);
			
			//Получить родительские узлы узла
			$this->getNodeParents($id);
			
		}
		
		
		
		//Если явно указан корневой узел
		if($root_node)
			$this->orderTree($root_node['id']);
		else//Считать корневыми все узлы нулевого уровня
		{
			foreach ($this->nodes as $id=>$node)
			{
				if(!$node['level'])
					$parents[$id]=$node['order_number'];
			}
			asort($parents);
			foreach($parents as $id=>$order_number)
				$this->orderTree($id);
					
			
			
		}
		return $this->tree_nodes;
		
		
	}
	//=======================================================================
	
	
	function getNodeLevel($node_id, $level=0)
	{
		$parent_id=$this->nodes[$node_id]['parent_id'];
		//Если в массиве узлов есть узел родительского объекта
		
		if(array_key_exists($parent_id,$this->nodes))
		{
			$level=$level+$this->getNodeLevel($parent_id,$level);
			$level++;
			if($level>$this->max_level);
				$this->max_level=$level;
			
			return $level;
		}
		
	}
	
	
	function getNodeChildren($parent_id)
	{
		foreach($this->nodes as $id=>$node)
		{
			if($node['parent_id']==$parent_id)
				$this->nodes[$parent_id]['children'][$id]=$node['order_number'];
		}
		if(isset($this->nodes[$parent_id]['children']) && is_array($this->nodes[$parent_id]['children']))
			asort($this->nodes[$parent_id]['children']);
	}
	
	
	
	function getNodeParents($node_id,$next_node_id=false)
	{
		if(!$next_node_id)
			$next_node_id=$node_id;
		if($parent_id=$this->nodes[$next_node_id]['parent_id'])
		{
			if(array_key_exists($parent_id,$this->nodes))
			{
				$this->nodes[$node_id]['parents'][$parent_id]=$this->nodes[$parent_id]['order_number'];
				$this->getNodeParents($node_id,$parent_id);
			}
		}
	}
	
	
	function orderTree($node_id)
	{
		$this->tree_nodes[$node_id]=$this->nodes[$node_id];
		if(isset($this->nodes[$node_id]['children']) && is_array($this->nodes[$node_id]['children']))
		{
			foreach($this->nodes[$node_id]['children'] as $child_id=>$val)
				$this->orderTree($child_id);
		}
	}

}
?>