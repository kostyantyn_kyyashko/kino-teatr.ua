function changeIcon(obj_id, children_url, use_cache)
{
	var img_plus=new Image();
	var img_minus=new Image();
	var img_cur=document.getElementById('tree_img_'+obj_id);
	img_plus.src="<?=_SKIN_URL?>"+"img/plus.gif";
	img_minus.src="<?=_SKIN_URL?>"+"img/minus.gif";

	if(img_cur.src==img_plus.src)
	{
		img_cur.src=img_minus.src;
		//Если дочерние элементы дерева уже загружены
		if(document.getElementById('tree_list_'+obj_id))
		{
			//Перезагрузить их
			document.getElementById('tree_list_'+obj_id).style.display='';
			if(!use_cache)
			{
				document.getElementById('tree_node_'+obj_id).innerHTML='<ul class="tree" id="tree_list_'+obj_id+'"><li>'+"<?=sys::translate('sys::loading')?>"+'</li></ul>';
				ajaxRequest(children_url);
			}
		}
		else
		{
			//Загрузить их
			document.getElementById('tree_node_'+obj_id).innerHTML+='<ul class="tree" id="tree_list_'+obj_id+'"><li>'+"<?=sys::translate('sys::loading')?>"+'</li></ul>';
			ajaxRequest(children_url);

		}
	}
	else
	{
		img_cur.src=img_plus.src;
		document.getElementById('tree_list_'+obj_id).style.display='none';
	}
}

function selectNode(obj_id,win)
{
	for(i=0; i<document.links.length; i++)
	{
		document.links[i].className='';
	}

	document.getElementById('tree_link_'+obj_id).className='active';
}
