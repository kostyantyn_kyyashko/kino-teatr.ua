function showFrameBox(obj,location)
{
	box=getChildByTagName(obj.parentNode,'DIV');
	frame=getChildByTagName(box,'IFRAME');
	if(box.style.display=='')
		box.style.display='none';
	else
	{
		box.style.display='';
		if(frame.contentWindow && !frame.src)
			frame.contentWindow.document.write('<?=sys::translate('sys::loading')?>');
		if(!frame.src)
			frame.src=location;
	}
}

