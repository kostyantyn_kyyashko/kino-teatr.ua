<?php
class sys_fileman
{		
	static function deleteFile($path)
	{
		if(is_file($path))
		{
			if(!sys::checkAccess('sys::delete_files'))
				return false;
			else 
				unlink($path);
		}
		elseif(is_dir($path))
		{
			if(!sys::checkAccess('sys::delete_folders'))
				return false;
			else 
				sys::deleteDir($path.'/');
		}
	}
}
?>