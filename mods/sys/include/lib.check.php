<?php
class sys_check
{
	static function checkArray($fields, $values)
	{
		$err=false;
		foreach ($fields as $name=>$params)
		{
			if(!isset($values[$name]))
				$values[$name]=false;
				
			$check=false;
			
			if(isset($params['req']) && $params['req'] && $params['type']!='file')
			{
				if(is_array($values[$name]))
				{
					if(!count($values[$name]))
					{
						$check['msg']='not_value';
						$check['var']=false;
					}
				}
				else
				{
					$values[$name]=trim($values[$name]);
					if(!mb_strlen($values[$name]))
					{
						$check['msg']='not_value';
						$check['var']=false;
					}
				}
			}
			if(!$check)
			{
				if(is_array($values[$name]))
				{
					for ($i=0; $i<count($values[$name]); $i++)
					{
						if(!isset($values[$name][$i]))
							$values[$name][$i]=false;
						if($check=self::checkArrayItem($name, $values[$name][$i],$params))
							break;
					}
				}
				else
					$check=self::checkArrayItem($name, $values[$name],$params);
			}
			
			if($check)
			{
				$err[$name]['title']=$params['title'];
				if(isset($params['err::'.$check['msg']]))
					$err[$name]['text']=$params['err::'.$check['msg']];
				else
					$err[$name]['text']=sys::translate('sys::'.$check['msg'],$check['var']);
			}
		}
		return $err;
		
	}
	//================================================================//
	
	static function checkArrayItem($name, $value, $params)
	{
		global $_cfg;
		$var=false;
		$msg=false;
		
		if($value || $params['type']=='file')
		{
			//Проверка на уникальность
			if(!$msg && isset($params['unique']))
			{
				if(!isset($params['unique::where']))
					$params['unique::where']=false;
				$msg=self::checkUnique($value,$name,$params['table'],$params['unique::where'],true);		
			}
			
			//Проверка на максимальное кол-во символов
			if(!$msg && isset($params['max_chars']))
			{
				$msg=self::checkMaxChars($value,$params['max_chars'],true);
				if($msg)
					$var['max_chars']=$params['max_chars'];
				
			}
			
			//Проверка на минимально кол-во символов
			if(!$msg && isset($params['min_chars']))
			{
				$msg=self::checkMinChars($value,$params['min_chars'],true);
				if($msg)
					$var['min_chars']=$params['min_chars'];
				
			}
			
			//Проверка на максимальное значение
			if(!$msg && isset($params['max_value']))
			{
				$msg=self::checkMax($value,$params['max_value'],true);
				if($msg)
					$var['max']=$params['max_value'];
				
			}
			
			//Проверка на минимальное значение
			if(!$msg && isset($params['min_value']) && $value)
			{
				$msg=self::checkMin($value,$params['min_value'],true);
				if($msg)
					$var['min']=$params['min_value'];
			}
			
			if(!$msg && isset($params['type']))
			{
				switch ($params['type'])
				{
					case 'int':
						$msg=self::checkInt($value,true);
					break;	
					
					case 'number':
						$msg=self::checkNumber($value,true);
					break;	
					
					case 'float':
						$msg=self::checkFloat($value,true);
					break;
					
					case 'text':
						$msg=self::checkText($value,true);
					break;
					
					case 'time':
						$msg=self::checkTime($value,true);
					break;
					
					case 'name':
						$msg=self::checkName($value,true);
					break;
					
					case 'date':
						if(!isset($params['format']))
							$params['format']=$_cfg['sys::date_time_format'];
						$msg=self::checkDate($value,$params['format'],true);
						if($msg)
							$var['format']=$params['format'];
					break;
					
					case 'email':
						$msg=self::checkEmail($value,true);
					break;
					
					case 'phone':
					
						$msg=self::checkPhone($value,true);
					break;
					
					case 'ip':
						$msg=self::checkIp($value,true);
					break;
					
					case 'file':
						if(!isset($params['file::dir']))
							$params['file::dir']=false;
							
						if(!isset($params['file::max_size']))
							$params['file::max_size']=false;
							
						if(!isset($params['file::formats']))
							$params['file::formats']=false;
							
						if(!isset($params['file::max_width']))
							$params['file::max_width']=false;
							
						if(!isset($params['file::max_height']))
							$params['file::max_height']=false;
						
						$file=$_FILES[$name];
						$msg=self::checkPostFile($file,$params['file::dir'],$params['file::max_size'],$params['file::formats'],$params['file::max_width'],$params['file::max_height'],true);
						if($msg)
						{
							$var['formats']=implode(', ',$params['file::formats']);
							$var['max_size']=sys::convertFileSize($params['file::max_size']);
							$var['max_width']=$params['file::max_width'];
							$var['max_height']=$params['file::max_height'];
						}
						if(isset($params['req']) && $params['req'] && !$_FILES[$name]['name'] && !$_POST['_files'][$name])
							$msg='not_value';
					break;
				}
			}
		}
		
		if($msg)
		{
			$result['msg']=$msg;
			$result['var']=$var;
			return $result;
		}
		else 
			return false;
	}
	
	
	//------------------------------------------------------------------
	static function checkValues($fields, $values)
	{
		global $_langs;
		foreach($fields as $name=>$params)
		{
			if(!isset($params['title']))
				$params['title']=sys::translate('sys::'.$name);
				
			//Если мултиязчное поле
			if(isset($params['multi_lang']) && $params['multi_lang'])
			{
				if(isset($title))
					unset($title);
				
				foreach($_langs as $lang_id=>$lang) 
				{
					$m_name=$name.'_'.$lang['code'];
					if(!isset($title))
						$title=$params['title'];
					$params['title']=$title.' ('.$lang['title'].')';
					$arr_params[$m_name]=$params;
				}
			}
			else 
				$arr_params[$name]=$params;
		}
		return self::checkArray($arr_params,$values);
	}
	//==================================================================
	
	
	static function checkName($val, $msg=false)
	{	
		$result=preg_match("/^[a-zA-Z0-9_]+$/",$val);
		if($msg)
		{
			if($result)
				return false;
			else 
				return 'not_valid_name';
		}
		else 
			return $result;
		
	}
	//===============================================================================
	
	static function checkEmail($email, $msg=false)
	{
		$result=preg_match("/^\S+@\S+$/",$email);
		if($msg)
		{
			if($result)
				return false;
			else 
				return 'not_valid_email';
		}
		else 
			return $result;
	}
	
	static function checkPhone($phone, $msg=false){
		if($phone=='')
			return false;
		 $phone = str_replace('+','',$phone);
		 $pattern = "/^380\d{3}\d{2}\d{2}\d{2}$/";
		 $result = preg_match($pattern, $phone);
		if($msg)
		{
			if($result)
				return false;
			else 
//				return 'Неправильный формат телефона. В начале строки не указан код страны или присутствуют нецифровые символы или телефон слишком короткий!';
				return 'not_valid_phone';
		}
		else 
			return $result;
	}
	
	//===============================================================================
	
	static function checkDate($date, $format=false, $msg=false)
    {
		global $_cfg;
		if(!$format)
			$format=$_cfg['sys::date_time_format'];
		
        $date=strptime($date, $format);
        if($msg)
        {
        	if($date)
        		return false;
        	else 
        		return 'not_valid_date';
        }
        else 
        {
        	if($date)
        		return true;
        	else 
        		return false;
        }
    }
	//========================================================================
	
	static function checkIp($ip, $msg)
	{
		$result=long2ip(ip2long($ip))==$ip;
		if($msg)
		{
			if($result)
				return false;
			else 
				return 'not_valid_ip';
		}
		else 
			return $result;
	}
	//=================================================================//
	
	static function checkMax($value, $max_value, $msg=false)
	{	
		$result=$value<=$max_value;
		
		if($msg)
		{
			if($result)
				return false;
			else 
				return 'too_big_value';
		}
		else 
			return $result;
		
	}
	//=================================
	
	static function checkMin($value, $min_value, $msg=false)
	{
		$result=$value>=$min_value;
		
		if($msg)
		{
			if($result)
				return false;
			else 
				return 'too_small_value';
		}
		else 
			return $result;
		
	}
	//=================================
	
	static function checkTime($value, $msg=false)
	{
		$value=explode(':',$value);
		if($value[0]>23 || $value[0]<0 || $value[1]>59 || $value[1]<0)
			$result=false;
		else 
			$result=true;
		
		if($msg)
		{
			if($result)
				return false;
			else 
				return 'invalid_time_format';
		}
		else 
			return $result;
		
	}
	//=================================
	
	
	//Проверка на макс кол-во симвлов
	static function checkMaxChars($string, $max_chars, $msg=false)
	{
		
		$result=mb_strlen($string)<=$max_chars;	
		
		if($msg)
		{
			if($result)
				return false;
			else 
				return 'too_many_chars';
		}
		else 
			return $result;
	}
	//===================================================================//
	
	
	//Проверка на мин. кол-во симвлов
	static function checkMinChars($string, $min_chars, $msg=false)
	{
		
		$result=mb_strlen($string)>=$min_chars;	
		
		if($msg)
		{
			if($result)
				return false;
			else 
				return 'not_enough_chars';
		}
		else 
			return $result;
	}
	//===================================================================//
	
	static function checkUnique($value, $field_name, $table, $where=false, $msg=false)
	{
		global $_db;
		if(is_numeric($where))
			$where=" `id`!=".intval($where)."";
		
		if(!$where)
			$where=" 1=1 ";
	
		$r=$_db->query("SELECT `".mysql_real_escape_string($field_name)."` FROM `#__".mysql_real_escape_string($table)."` 
						WHERE `".mysql_real_escape_string($field_name)."`='".mysql_real_escape_string($value)."' AND ".$where);
      
		list($result)=$_db->fetchArray($r);
		if($msg)
		{
			if($result)
				return 'not_unique';
			else 
				return false;	
		}
		else 
		{
			if($result)
				return false;
			else 
				return true;
		}
	}
	//==================================================================
	
	static function checkNumber($value, $msg=false)
	{
		$result=true;
			
		if($value && !is_numeric($value))
			$result=false;
			
		if($msg)
		{
			if($result)
				return false;
			else 
				return 'not_number';
		}
		else 
			return $result;	
		
	}
	//===================================================================
	
	static function checkInt($value, $msg=false)
	{
		$result=true;
		
		if($value && !is_numeric($value))
			$result=false;
			
		if($value && !intval($value))
			$result=false;
			
		if($msg)
		{
			if($result)
				return false;
			else 
				return 'not_int';
		}
		else 
			return $result;		
	}
	//=============================================================
	
	static function checkFloat($value, $msg=false)
	{
		$result=true;
		
		if($value && !is_numeric($value))
			$result=false;
			
		if($value && !floatval($value))
			$result=false;
			
		if($msg)
		{
			if($result)
				return false;
			else 
				return 'not_float';
		}
		else 
			return $result;
	}
	//=========================================================
	
	static function checkText($value, $msg=false)
	{
		$result=true;
		
		if($msg)
		{
			if($result)
				return false;
			else 
				return 'not_text';
		}
		else 
			return $result;
	}
	//===========================================================
	
	static function checkPostFile($file,$upload_dir=false,$max_file_size=false,$allowed_formats=false,$max_width=false, $max_height=false, $msg=false)
	{
		
		clearstatcache();
		if(!isset($file['name']) || !$file['name'])
		{
			if($msg)
				return false; 
			else 
				return true;
		}
		
		//Если массив файлов
		if(is_array($file['name']))
		{
			for($i=0; $i<count($file['name']);$i++)
			{
				$arr_file=array();
				$arr_file['name']=$file['name'][$i];
				$arr_file['type']=$file['type'][$i];
				$arr_file['tmp_name']=$file['tmp_name'][$i];
				$arr_file['size']=$file['size'][$i];
				if($msg)
				{
					if($err=self::checkPostFile($arr_file,$upload_dir,$max_file_size,$allowed_formats,$max_width, $max_height, $msg))
						return $err;
				}
				else 
				{
					if(!self::checkPostFile($arr_file,$upload_dir,$max_file_size,$allowed_formats,$max_width, $max_height, $msg))
						return false;
				}
			}
			if($msg)
				return false; 
			else 
				return true;
		}
		
	
		if($file['size']<=0)
		{
			if($msg)
				return 'no_file';
			else 
				return false;
		}
			
		if($upload_dir && !is_writable($upload_dir))
		{
			if($msg)
				return 'upload_dir_is_not_writable';
			else 
				return false;
		}
		
		if($max_file_size && $file['size']>$max_file_size)
		{
			if($msg)
				return 'too_big_file';
			else 
				return false;
		}
			
		$path_info = pathinfo($file['name']);
		$path_info['extension']=mb_strtolower($path_info['extension']);
		
		if($allowed_formats && !in_array($path_info['extension'],$allowed_formats))
		{
			if($msg)
				return 'invalid_file_format';
			else
				return false;
		}
		
		$image_formats[]='gif';
		$image_formats[]='jpg';
		$image_formats[]='jpeg';
		$image_formats[]='png';
			
		if(in_array($path_info['extension'],$image_formats))
		{
			$image_info = getimagesize($file['tmp_name']); 
			if($max_width && ($max_width<$image_info[0]))
			{
				if($msg)
					return 'too_wide_image';
				else 
					return false;
			}
			if($max_height && ($max_height<$image_info[1]))
			{
				if($msg)
					return 'too_high_image';
				else 
					return false;
			}
		}
		
		if(!is_uploaded_file($file['tmp_name']))
		{
			if($msg)
				return 'upload_error';
			else 
				return false;
		}
		
		if($msg)	
			return false;
		else 
			return true;
		
	}
	

}
?>