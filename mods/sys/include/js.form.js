function addOption(field, id, value, selected)
{
	list=document.getElementById(field);
	var len=list.options.length;
	var doc = list.ownerDocument;
	if (!doc)
		doc = list.document;
	var opt = doc.createElement('OPTION');
	opt.value = id;
	opt.text = value;
	list.options.add(opt, len);
	if(selected)
		list.options[len].selected=true;
}

function setValue(field, id, value)
{
	hid=document.getElementById(field);
	text=getChildByTagName(hid.parentNode,'NOBR');
	text.innerHTML=value;
	hid.value=id;
}

function userFunctionCall()
{
	var str = 'theUserCallBackFunction(';
	for(var i=0;i<arguments.length;i++) {
		if(i>0) str += ',';
		str += '\''+arguments[i]+'\'';
	}
	str +=')';
	try{eval(str);}catch(e){}  //by Mike
}


// Это пример функции, которая может быть дописана в конкретном шаблоне.
// Функция с этим именем будет вызываться автоматически при выборе одного из элементов списка автозаполнения.
/*
function theUserCallBackFunction()
{
	var arr = new Array();
	for(var i=0;i<arguments.length;i++) {
		if(arguments[i]=='') continue;
		var a = arguments[i].split("=");
		arr[a[0]] = a[1];
	}
	 alert(arr['caller_identification']); // по этому полю можно понять вызывающий список
	 alert(arr['film_3d']);	// все поля из SQL-запроса находятся в именованном массиве
}
*/