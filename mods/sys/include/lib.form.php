<?
class sys_form
{
	//Подготвка параметров поля
	static function prepareFieldParams($name, $params)
	{

		global $_cfg;
		//Если поле уже подговлено
		/*if(isset($params['prepared']) && $params['prepared'])
			return $params;*/

		//html-параметры
		if(!isset($params['html_params']))
			$params['html_params']=false;

		//типа инпута
		if(!isset($params['input']))
			$params['input']='textbox';

		//типа данных
		if(!isset($params['type']))
			$params['type']=false;

		//значение поля
		if(!isset($params['value']))
			$params['value']=false;
		elseif($params['input']!='fck' && !is_array($params['value']))
			$params['value']=htmlspecialchars($params['value']);

		//дополнительный текст поля
		if(!isset($params['extra']))
			$params['extra']=false;

		//шаблон для вывода поля
		if(!isset($params['tpl']))
			$params['tpl']='sys::input.'.$params['input'];

		//признак обязательного поля
		if(!isset($params['req']))
			$params['req']=false;

		if(!isset($params['multi']))
			$params['multi']=false;

		if(!isset($params['onclick']))
			$params['onclick']=false;

		if(!isset($params['text']))
			$params['text']=false;

		if(!isset($params['empty::value']))
			$params['empty::value']=false;

		//название поля
		if(!isset($params['title']))
			$params['title']=sys::translate('sys::'.$name);

		if(!isset($params['values']))
			$params['values']=false;

		//мультиязычность
		if(!isset($params['multi_lang']))
			$params['multi_lang']=false;

		switch ($params['input'])
		{
			case 'filebox':
				if(!isset($params['file::dir']))
					$params['file::dir']=false;

				if(!isset($params['preview']))
					$params['preview']=false;

				if(!isset($params['preview::html_params']))
					$params['preview::html_params']=false;

				if(!isset($params['file::url']))
					$params['file::url']=str_replace(_ROOT_DIR,_ROOT_URL,$params['file::dir']).$params['value'];

				if(!isset($params['preview::prefix']))
					$params['preview::prefix']=false;

				if(!isset($params['preview::src']))
					$params['preview::src']=str_replace(_ROOT_DIR,_ROOT_URL,$params['file::dir']).$params['preview::prefix'].$params['value'];


				if(!isset($params['preview::url']))
					$params['preview::url']=$params['file::url'];
			break;

			case 'multifilebox':
				if(!$params['value'])
				{
					$params['value']=array();
					$params['value'][0]=false;
				}
			break;

			case 'framebox':
				if(!isset($params['frame']))
					$params['frame']=false;

				if(!isset($params['frame::width']))
					$params['frame::width']=100;

				if(!isset($params['frame::height']))
					$params['frame::height']=100;
			break;

			case 'datebox':
				if(!isset($params['format']))
					$params['format']=$_cfg['sys::date_time_format'];
			break;

		}


		//имя поля
		$params['name']=$name;

		//флаг сигнализирующий о том что поле подготовлено
		$params['prepared']=true;

		return $params;
	}
	//=============================================================//

	//Пропарсить шаблон поля
	static function parseField($name, $params, $html_params=false, $tpl=false)
	{
		$params=self::prepareFieldParams($name, $params);
		if($html_params)
			$params['html_params']=$html_params;
		if($tpl)
			$params['tpl']=$tpl;

		if($params['input']=='fck')
			return self::showFCK($name, $params);

		return sys::parseTpl($params['tpl'],$params);
	}
	//==================================================================//

	//Пропарсить набор полей для формы
	static function parseFields($fields, $values=array(), $with_html=true)
	{

		global $_cfg, $_langs;
		if(!$fields || !is_array($fields) || !count($fields))
			return false;
		foreach($fields as $name=>$params)
		{
			if(!isset($params['title']))
				$params['title']=sys::translate('sys::'.$name);
			//Если мултиязчное поле
			if(isset($params['multi_lang']) && $params['multi_lang'])
			{
				$title=false;

				//Cформировать поля для каждого языка
				foreach($_langs as $lang)
				{
					$name_lang=$name.'_'.$lang['code'];
					if(!$title)
						$title=$params['title'];
					$params['title']=$title.' ('.$lang['title'].')';
					if(isset($values[$name_lang]))
						$params['value']=$values[$name_lang];
					$result[$name_lang]=$params;
					if($with_html)
						$result[$name_lang]['html']=self::parseField($name_lang,$params);
				}
			}
			else
			{
				if(isset($values[$name]))
					$params['value']=$values[$name];
				$result[$name]=$params;
				if($with_html)
					$result[$name]['html']=self::parseField($name,$params);
			}

		}
		return $result;
	}
	//=======================================================//

	//----------------------------------------------------------------------------
	static function parseForm($params, $tpl=false, $html_params=false)
	{
		if(!$tpl)
			$tpl='sys::form.default';
		if(!isset($params['html_params']))
			$params['html_params']=false;
		if(!isset($params['values']))
			$params['values']=array();
		if($html_params)
			$params['html_params']=$html_params;
		if(!isset($params['action']))
			$params['action']=$_SERVER['REQUEST_URI'];
		if(!isset($params['method']))
			$params['method']='POST';
		if(!isset($params['id']))
			$params['id']='form';
		if(isset($params['fields']))
			$params['fields']=self::parseFields($params['fields'],$params['values']);
		else
			$params['fields']=false;

		return sys::parseTpl($tpl, $params);
	}
	//=======================================================================

	static function showFCK($name,$params)
	{
		global $_cfg;
		require_once(_3PARY_DIR."fckeditor/fckeditor.php");

		if(!isset($params['css']) || !$params['css'])
			$params['css']='default';

		$oFCKeditor = new FCKeditor($name);
		if(!$oFCKeditor->IsCompatible())
		{
			$params['type']='textarea';
			return self::parseField($name_lang,$params);
		}

		if(!isset($params['fck::ToolbarSets']))
			$params['fck::ToolbarSets']='grifix';

		$oFCKeditor->ToolbarSet=$params['fck::ToolbarSets'];


		$oFCKeditor->Config['AutoDetectLanguage']=false ;
		$oFCKeditor->Config['DefaultLanguage']=$_cfg['sys::lang'];


		if(is_dir(_ROOT_DIR.'mods/main/skins/'.$_cfg['sys::frontend_skin'].'/smiley/'))
		{
			$smiley_dir=_ROOT_DIR.'mods/main/skins/'.$_cfg['sys::frontend_skin'].'/smiley/';
			$smiley_url=_ROOT_URL.'mods/main/skins/'.$_cfg['sys::frontend_skin'].'/smiley/';
		}
		else
		{
			$smiley_dir=_ROOT_DIR.'mods/main/skins/default/smiley/';
			$smiley_url=_ROOT_URL.'mods/main/skins/default/smiley/';
		}

		$oFCKeditor->Config['SmileyPath']=$smiley_url;
		$images=sys::scanDir($smiley_dir);

		$oFCKeditor->Config['SmileyImages']="['";
		$oFCKeditor->Config['SmileyImages']=implode("','",$images);
		$oFCKeditor->Config['SmileyImages'].="'];";
		$oFCKeditor->Config['SmileyImages']="['regular_smile.gif','test.gif'];";


		if(is_file(_ROOT_DIR.'mods/main/skins/'.$_cfg['sys::frontend_skin'].'/cfg/fck_styles_'.$params['css'].'.xml'))
			$oFCKeditor->Config['StylesXmlPath']=_ROOT_URL.'mods/main/skins/'.$_cfg['sys::frontend_skin'].'/cfg/fck_styles_'.$params['css'].'.xml';
		else
			$oFCKeditor->Config['StylesXmlPath']=_ROOT_URL.'mods/main/skins/default/cfg/fck_styles_default.xml';

		if(is_file(_ROOT_DIR.'mods/main/skins/'.$_cfg['sys::frontend_skin'].'/cfg/fck_templates.xml'))
			$oFCKeditor->Config['TemplatesXmlPath']=_ROOT_URL.'mods/main/skins/'.$_cfg['sys::frontend_skin'].'/cfg/fck_templates.xml';
		else
			$oFCKeditor->Config['TemplatesXmlPath']=_ROOT_URL.'mods/main/skins/default/cfg/fck_templates.xml';

		$oFCKeditor->Config['SkinPath']=_ROOT_URL.'third_pary/fckeditor/editor/skins/silver/';
		$oFCKeditor->Config['FontSizes']='10px;11px;12px;13px;14px;15px;16px;17px;18px;19px;20px;21px;22px;23px;24px';

		if($params['css']!='_sys')
			$oFCKeditor->Config['EditorAreaCSS']=_ROOT_URL.'mods/main/skins/'.$_cfg['sys::frontend_skin'].'/css.'.$params['css'].'.css';
		else
			$oFCKeditor->Config['EditorAreaCSS']=_ROOT_URL.'mods/sys/backend/skins/'.$_cfg['sys::backend_skin'].'/css.default.css';
		$oFCKeditor->Config['BodyClass']='xContent';

		$oFCKeditor->BasePath=_ROOT_URL.'third_pary/fckeditor/';
		$oFCKeditor->Value = $params['value'];
		$oFCKeditor->Width ='100%';



		if($params['req'])
			$oFCKeditor->Params['req']='required="1"';
		else
			$oFCKeditor->Params['req']=false;

		$oFCKeditor->Params['alt']='fck';
		$oFCKeditor->Params['title']=$params['title'];

		if(isset($params['height']))
			$oFCKeditor->Height=$params['height'];
		return $oFCKeditor->CreateHtml();
	}
}
?>