<?php
class sys_calendar
{
	//Получить путь к картинке
	static function getCalendar($url,$date_url=false, $date_onclick=false,$year=false, $month=false, $day=false, $format=false, $from_year=1970, $to_year=2050)
	{
		global $_cfg;
		if(!$year)
			$result['year']=date('Y');
		else
			$result['year']=$year;

		if(!$month)
			$result['month']=date('n');
		else
			$result['month']=$month;

		if(!$day)
			$result['day']=date('j');
		else
			$result['day']=$day;

		if(!$format)
			$format=$_cfg['sys::date_format'];

		if($result['year']==date('Y') && $result['month']==date('n'))
			$result['cur_day']=date('j');
		else
			$result['cur_day']=false;

		$result['num_of_days']=date('t',mktime(0,0,0,$result['month'],$result['day'],$result['year']));
		$result['first_day']=mktime(0,0,0,$result['month'],1,$result['year']);
		if (date('w',$result['first_day'])>0)
		{			$result['first_day']=date('w',$result['first_day'])-1;
		} else {			$result['first_day']=6;
		}

		for($i=1; $i<$result['num_of_days']+1; $i++)
		{
			$result['dates'][$i]=mktime(0,0,0,$result['month'],$i,$result['year']);
			$result['dates'][$i]=strftime($format,$result['dates'][$i]);
			$result['onclick'][$i]=str_replace('%date%',$result['dates'][$i],$date_onclick);
			$result['url'][$i]=str_replace('%date%',$result['dates'][$i],$date_url);
		}


		$format=base64_encode($format);

		$result['next_year_url']=$url.'&year='.($result['year']+1).'&month='.$result['month'].'&format='.$format;
		$result['prev_year_url']=$url.'&year='.($result['year']-1).'&month='.$result['month'].'&format='.$format;
		if($result['month']==12)
		 	$result['next_month_url']=$url.'&year='.($result['year']+1).'&month=1&format='.$format;
		else
			$result['next_month_url']=$url.'&year='.($result['year']).'&month='.($result['month']+1).'&format='.$format;

		if($result['month']==1)
		 	$result['prev_month_url']=$url.'&year='.($result['year']-1).'&month=12&format='.$format;
		else
			$result['prev_month_url']=$url.'&year='.($result['year']).'&month='.($result['month']-1).'&format='.$format;

		$result['months']=array();
		$result['months'][1]=sys::translate('sys::january');
		$result['months'][2]=sys::translate('sys::february');
		$result['months'][3]=sys::translate('sys::march');
		$result['months'][4]=sys::translate('sys::april');
		$result['months'][5]=sys::translate('sys::may');
		$result['months'][6]=sys::translate('sys::june');
		$result['months'][7]=sys::translate('sys::july');
		$result['months'][8]=sys::translate('sys::august');
		$result['months'][9]=sys::translate('sys::september');
		$result['months'][10]=sys::translate('sys::october');
		$result['months'][11]=sys::translate('sys::november');
		$result['months'][12]=sys::translate('sys::december');

		$result['week_days']=array();
		$result['week_days'][0]=sys::translate('sys::mon');
		$result['week_days'][1]=sys::translate('sys::tue');
		$result['week_days'][2]=sys::translate('sys::wed');
		$result['week_days'][3]=sys::translate('sys::thu');
		$result['week_days'][4]=sys::translate('sys::fri');
		$result['week_days'][5]=sys::translate('sys::sat');
		$result['week_days'][6]=sys::translate('sys::sun');

		$result['from_year']=$from_year;
		$result['to_year']=$to_year;
		return $result;
	}

	static function showCalendar($url,$date_url=false,$date_onclick=false,$year=false, $month=false, $day=false, $format=false, $from_year=1900, $to_year=2050)
	{
		sys::useLib('sys::form');

		$var=self::getCalendar($url,$date_url,$date_onclick,$year,$month,$day, $format, $from_year, $to_year);
		$yearbox['input']='selectbox';
		$yearbox['value']=$var['year'];
		$yearbox['html_params']='onchange="window.location=\''.sys::cutGetParams('year').'&year=\'+this.value"';
		for($i=$from_year;$i<$to_year+1;$i++)
		{
			$yearbox['values'][$i]=$i;
		}
		$var['yearbox']=sys_form::parseField('year',$yearbox);

		$monthbox['input']='selectbox';
		$monthbox['value']=$var['month'];
		$monthbox['values']=$var['months'];
		$monthbox['html_params']='onchange="window.location=\''.sys::cutGetParams('month').'&month=\'+this.value"';
		$var['monthbox']=sys_form::parseField('month',$monthbox);
		return sys::parseTpl('sys::calendar',$var);
	}


}
?>