function clearTags(text) 
{
	text = text.replace('[QUOTE]', '');
	text = text.replace('[/QUOTE]', '');
	text = text.replace('[B]', '');
	text = text.replace('[/B]', '');
	text = text.replace('[I]', '');
	text = text.replace('[/I]', '');
	return text;
}

function previewBB(obj, RootUrl)
{
	Action=obj.form.action;
	Target=obj.form.target;
	window.open("","preview","width=800,height=600,directories=no, toolbar=no, location=no, resizable=yes, scrollbars=yes, menubar=no, dependent=yes, left=100, top=100");
	obj.form.action=RootUrl+'?mod=sys&act=preview&field='+obj.name;
	obj.form.target="preview";
	obj.form.submit();
	obj.form.action=Action;
	obj.form.target=Target;
	
}

function insertTag(type, _form, _field)
{
	var url_ = /^http:\/\/.*/i;
	var email = /^([0-9a-z][0-9a-z-_]*\.)*([0-9a-z][0-9a-z-_]*)@([0-9a-z][0-9a-z-_]*\.)+[a-z]+$/;
	
	theText = _form.elements[_field];
	theText.focus();
	
	if (document.selection)
	{
		SelectedText = document.selection.createRange();
		
		switch (type) {
			case "bold" :
				SelectedText.text = '[B]' + SelectedText.text + '[/B]';
				break;
			case "italic" :
				SelectedText.text = '[I]' + SelectedText.text + '[/I]';
				break;
			case "underline" :
				SelectedText.text = '[U]' + SelectedText.text + '[/U]';
				break;
			case "left" :
				SelectedText.text = '[LEFT]' + SelectedText.text + '[/LEFT]';
				break;
			case "center" :
				SelectedText.text = '[CENTER]' + SelectedText.text + '[/CENTER]';
				break;
			case "right" :
				SelectedText.text = '[RIGHT]' + SelectedText.text + '[/RIGHT]';
				break;
			case "strike" :
				SelectedText.text = '[STRIKE]' + SelectedText.text + '[/STRIKE]';
				break;
			case "quote" :
				SelectedText.text = '[QUOTE]' + SelectedText.text + '[/QUOTE]';
				break;
			case "link" :
				temp_text = '';
				theURL = clear_tags(prompt('URL', 'http://'));
				if (email.test(theURL))	{
					if (SelectedText.text == '') temp_text = theURL;
					theURL = 'mailto:' + theURL;
				}
				if (theURL) SelectedText.text = '[URL=' + theURL + ']' + clear_tags(SelectedText.text) + temp_text + '[/URL]';
				break;
			case "image" :
				temp_text = '';
				theURL = clear_tags(prompt('URL', 'http://'));
				if (theURL) SelectedText.text = '[IMAGE]' + theURL + '[/IMAGE]';
				break;
			
			case "flash" :
				temp_text = '';
				theURL = clear_tags(prompt('FLASH', 'http://'));
				if (theURL) SelectedText.text = '[FLASH]' + theURL + '[/FLASH]';
				break;
			default :
		}
	}
	else
	{
		var textLength = theText.textLength;
		var selStart = theText.selectionStart;
		var selEnd = theText.selectionEnd;
	
		var s1 = (theText.value).substring(0,selStart);
		var s2 = (theText.value).substring(selStart, selEnd)
		var s3 = (theText.value).substring(selEnd, textLength);
		
		
		switch (type) {
			case "bold" :
				theText.value = s1 + '[B]' + s2 + '[/B]' + s3;
				break;
			case "italic" :
				theText.value = s1 + '[I]' + s2 + '[/I]' + s3;
				break;
			case "left" :
				theText.value = s1 + '[LEFT]' + s2 + '[/LEFT]' + s3;
				break;
			case "right" :
				theText.value = s1 + '[RIGHT]' + s2 + '[/RIGHT]' + s3;
				break;
			case "center" :
				theText.value = s1 + '[CENTER]' + s2 + '[/CENTER]' + s3;
				break;
			case "underline" :
				theText.value = s1 + '[U]' + s2 + '[/U]' + s3;
				break;
			case "strike" :
				theText.value = s1 + '[STRIKE]' + s2 + '[/STRIKE]' + s3;
				break;
			case "quote" :
				theText.value = s1 + '[QUOTE]' + s2 + '[/QUOTE]' + s3;
				break;
			case "link" :
				temp_text = '';
				theURL = clearTags(prompt('URL', 'http://'));
				if (email.test(theURL))	{
					if (s2 == '') temp_text = theURL;
					theURL = 'mailto:' + theURL;
				}
				if (theURL) theText.value = s1 + '[URL=' + theURL + ']' + clearTags(s2) + temp_text + '[/URL]' + s3;
				break;
			case "image" :
				temp_text = '';
				theURL = clearTags(prompt('URL', 'http://'));
				if (theURL) theText.value = s1 + '[IMAGE]' + theURL + '[/IMAGE]' + s3;
				break;
			
			case "flash" :
				temp_text = '';
				theURL = clearTags(prompt('URL', 'http://'));
				if (theURL) theText.value = s1 + '[FLASH]' + theURL + '[/FLASH]' + s3;
				break;
			default :
		}
	}

	theText.focus();	
}