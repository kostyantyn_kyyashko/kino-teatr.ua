<?php
class sys_pages
{
	static function get($query, $current_page=false, $on_page=false, $num_of_pages=false, $strip=true)
	{
/*		
print "<!-- mikesdata ";
var_dump($current_page);
var_dump($on_page);
var_dump($num_of_pages);
var_dump($query);
print " -->";
*/
		global $_db, $_cfg;
		$result=array();
		$result['limit']=false;
		$result['first_page']=false;
		$result['prev_page']=false;
		$result['next_page']=false;
		$result['last_page']=false;
		$result['pages']=array();
		$result['num_rows']=false;

		if(!$on_page)
		{
			if(isset($_COOKIE['sys::on_page']))
				$on_page=intval($_COOKIE['sys::on_page']);

			if(isset($_GET['on_page']))
				$on_page=intval($_GET['on_page']);

			if(!$on_page)
				$on_page=$_cfg['sys::on_page'];
		}

		if(!$num_of_pages)
			$num_of_pages=$_cfg['sys::num_of_pages'];

		if($num_of_pages%2==0)
			$num_of_pages++;

		if(!$current_page && isset($_GET['page']))
			$current_page=$_GET['page'];

		if(!$current_page)
			$current_page=1;

		$result['current_page']=$current_page;
		$half_of_pages=round($num_of_pages/2)-1;

		if($strip)
		{
			$arr_q=explode('FROM',$query);
			$arr_query[1] = $arr_q[1];

			if (count($arr_q)>2)
			{
				preg_match("/.*pst.image
			FROM(.*)/is", $query, $tit);
				$arr_query[1] = $tit[1];
			}

			$arr_query=explode('ORDER',$arr_query[1]);

			if (!strpos($arr_query[0], 'shw.begin'))
			{
			$arr_query[0] = str_replace('GROUP BY flm.id', '', $arr_query[0]);

			if(!isset($arr_query[0]) || ($arr_query[0]=="")) return $result;
			$r=$_db->query("SELECT COUNT(*) FROM ".$arr_query[0]);

			} else {
				$r=$_db->query("SELECT COUNT(*) FROM ( SELECT flm.* FROM ".$arr_query[0]." ) AS tbl");
			}

			list($result['num_rows'])=$_db->fetchArray($r);
		}
		else
		{
			$r=$_db->query($query);
			$result['num_rows']=$_db->numRows($r);
		}

		$last_page=ceil($result['num_rows']/$on_page);
		$result['last_page_number']=$last_page;

//		if($current_page>$last_page || $current_page<1)
//			return $result;
		if($current_page>$last_page) $current_page=$last_page;
		if($current_page<1) $current_page=1;

		//Страницы перед текущей
		$prev_pages=array();
		for($i=1; $i<$half_of_pages+1; $i++)
		{
			if($current_page-$i>0)
				$prev_pages[]=$current_page-$i;
		}
		//недостача страниц перед текущей
		$prev_pages_diff=$half_of_pages-count($prev_pages);
		//===================================

		//Страницы после текущей
		$next_pages=array();
		for($i=1; $i<$half_of_pages+1; $i++)
		{
			if($current_page+$i<=$last_page)
				$next_pages[]=$current_page+$i;
		}
		////недостача страниц полсе текущей
		$next_pages_diff=$half_of_pages-count($next_pages);
		//==================================

		if($prev_pages_diff)
		{
			for($i=0;$i<$prev_pages_diff; $i++)
			{
				if(isset($next_pages[$half_of_pages-1]) && $next_pages[$half_of_pages-1]+$i+1<=$last_page)
					$next_pages[]=$next_pages[$half_of_pages-1]+$i+1;
			}
		}

		if($next_pages_diff)
		{
			for($i=0;$i<$next_pages_diff; $i++)
			{
				if(isset($prev_pages[$half_of_pages-1]) && $prev_pages[$half_of_pages-1]-$i-1>0 && $i!=$last_page)
					$prev_pages[]=$prev_pages[$half_of_pages-1]-$i-1;
			}
		}

		if($current_page==1)
			$first_record = 0;
		else
			$first_record = $on_page * ($current_page-1);

		//==================================

		$result['pages']=array();
		if($prev_pages);
			$result['pages']=array_merge($result['pages'],array_reverse($prev_pages));

		$result['pages'][]=$current_page;

		if($next_pages)
			$result['pages']=array_merge($result['pages'],$next_pages);

		$result['limit'] = " LIMIT ".$first_record.", ".($on_page);

		//Первая  страницы
		if(in_array(1,$result['pages']))
			$result['first_page']=false;
		else
			$result['first_page']=1;

		//Предыдущая страница
		if($current_page==1)
			$result['prev_page']=false;
		else
			$result['prev_page']=$current_page-1;

		//Следующая страница
		if($current_page==$last_page)
			$result['next_page']=false;
		else
			$result['next_page']=$current_page+1;

		//Последняя страница
		if(in_array($last_page,$result['pages']))
			$result['last_page']=false;
		else
			$result['last_page']=$last_page;

		return $result;
	}

	static function setUrls($pages, $template=false, $hurl=true, $var_name='pages')
	{			
		global $_cfg;
		if(!$template)
			$template=sys::cutGetParams('page').'&page=%page%'; // "?mod=main&lang=ru&act=last_trailers&trailer_id=5745&page=%page%" 

		$result=$pages;

		if($pages['first_page'])
		{
			$result['first_page']=str_replace('&'.$var_name.'=%page%','',$template);
			if($hurl && _MODE=='frontend')
				$result['first_page']=sys::rewriteUrl($result['first_page']);
		}
		else
			$result['first_page']=false;

		if($pages['prev_page'])
		{
			if($pages['prev_page']==1)
				$result['prev_page']=str_replace('&'.$var_name.'=%page%','',$template);
			else
				$result['prev_page']=str_replace('%page%',$pages['prev_page'],$template);
			if($hurl && _MODE=='frontend')
				$result['prev_page']=sys::rewriteUrl($result['prev_page']);
		}
		else
			$result['prev_page']=false;

		if($pages['next_page'])
		{
			$result['next_page']=str_replace('%page%',$pages['next_page'],$template);
			if($hurl && _MODE=='frontend')
				$result['next_page']=sys::rewriteUrl($result['next_page']);
		}
		else
			$result['next_page']=false;

		if($pages['last_page'])
		{
			$result['last_page']=str_replace('%page%',$pages['last_page'],$template);
			if($hurl && _MODE=='frontend')
				$result['last_page']=sys::rewriteUrl($result['last_page']);
		}
		else
			$result['last_page']=false;

		$urls=array();
		if(isset($pages['pages']))
		{
			foreach ($pages['pages'] as $page)
			{
				if($page==1)
					$urls[$page]=str_replace('&'.$var_name.'=%page%','',$template);
				else
					$urls[$page]=str_replace('%page%',$page,$template);
				if($hurl && _MODE=='frontend')
					$urls[$page]=sys::rewriteUrl($urls[$page]);
			}
		}

		$result['pages']=$urls;
		return $result;
	}

	static function pocess($query, $on_page=false, $num_of_pages=false, $template=false, $current_page=false, $hurl=true, $var_name='page',$strip=true)
	{
		//if(sys::isDebugIP()) sys::printR (self::get($query, $current_page, $on_page, 9,$strip));

		return self::setUrls(self::get($query, $current_page, $on_page, 9,$strip),$template,$hurl,$var_name);
	}
	
// Используется для преобразования старого длинного адреса через слэши
// Извлекается /page/2 и подставляется в конец ЧПУ как параметр ?page=2	
	static function setPageUrl($old_url, $new_url)
	{
		$old_url = explode("/",$old_url);
		$page = array_search("page",$old_url);
		$page = ($page===false)?"":($page+1);
		if($page && isset($old_url[$page])) $page = intval($old_url[$page]);
		if($page) return $new_url.((strpos($new_url,"?")===false)?"?":"&")."page=$page";
		return $new_url;
	}
}
?>