<?php
class sys_gui
{
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $mod_image
	 * @return unknown
	 */
	static function getIconSrc($mod_image)
	{
		$mod_image=explode('::',$mod_image);
		$mod=$mod_image[0];
		$image=$mod_image[1];

		//Текущий модуль, текущий скин
		if(is_file(_MODS_DIR.$mod.'/backend/skins/'._SKIN.'/img/'.$image))
			return _MODS_URL.$mod.'/backend/skins/'._SKIN.'/img/'.$image;

		//Текущий модуль, дефолтный скин
		if(is_file(_MODS_DIR.$mod.'/backend/skins/default/img/'.$image))
			return _MODS_URL.$mod.'/backend/skins/default/img/'.$image;

		//Системный модуль текущий скин
		if(is_file(_MODS_DIR.'sys/backend/skins/'._SKIN.'/img/'.$image))
			return _MODS_URL.'sys/backend/skins/'._SKIN.'/img/'.$image;

		//Системный модуль дефолтный скин
		if(is_file(_MODS_DIR.'sys/backend/skins/default/img/'.$image))
			return _MODS_URL.'sys/backend/skins/default/img/'.$image;

		return false;
	}
	//=========================================================//

	//-----------------------------------------------------------
	static function showNavPath($mod_name=false)
	{
		global $_db;
		$var=array();

		$obj=array();
		$obj['id']=0;
		if($mod_name)
		{
			$mod_name=explode('::',$mod_name);
			$mod=$mod_name[0];
			$name=$mod_name[1];

			//Получить текущий объект
			$r=$_db->query("SELECT tree.id, tree.parent_id,
				CONCAT(mods.name,'::',tree.name) AS `name`
				FROM `#__sys_tree` AS `tree`
				LEFT JOIN `#__sys_mods` AS `mods`
				ON tree.mod_id=mods.id
				WHERE mods.name='".mysql_real_escape_string($mod)."'
				AND tree.name='".mysql_real_escape_string($name)."'");
			$obj=$_db->fetchAssoc($r);
			$obj['name']=sys::translate($obj['name']);
			$parent_id=$obj['parent_id'];
			$var['objects'][]=$obj;

			//Получить родительские объекты
			while($parent_id)
			{
				$r=$_db->query("SELECT tree.id, tree.parent_id, tree.url,
					CONCAT(mods.name,'::',tree.name) AS `name`
					FROM `#__sys_tree` AS `tree`
					LEFT JOIN `#__sys_mods` AS `mods`
					ON tree.mod_id=mods.id
					WHERE tree.id='".mysql_real_escape_string($parent_id)."'");
				$obj2=$_db->fetchAssoc($r);
				$obj2['name']=sys::translate($obj2['name']);
				$parent_id=$obj2['parent_id'];
				$var['objects'][]=$obj2;
				unset ($obj2);
			}
		}

		$window='window.parent.fra_left';
		self::reloadTreeBranch('window.parent.fra_left',$obj['id'],self::getNavigatorBranch($obj['id']),$obj['id']);


		if(isset($var['objects']))
			$var['objects']=array_reverse($var['objects']);
		return sys::parseTpl('sys::nav_path',$var);
	}
	//===============================================================

	static function showButton($title, $onclick)
	{
		ob_start();
		?>
			<input class="button" type="button" onclick="<?=$onclick?>" value="<?=htmlspecialchars($title);?>">
		<?
		return ob_get_clean();
	}

	//Показать инонки текущего модуля
	static function showIcons($mod_name=false)
	{
		global $_db;
		if(!$mod_name)
		{
			$r=$_db->query("SELECT tree.*,
				CONCAT(mods.name,'::',tree.name) AS `name`,
				mods.name AS `mod_name`,
				CONCAT(mods.name,'::',actions.name) AS `action`
				FROM `#__sys_tree` AS `tree`
				LEFT JOIN `#__sys_mods` AS `mods`
				ON tree.mod_id = mods.id
				LEFT JOIN `#__sys_mods_actions` AS `actions`
				ON tree.action_id = actions.id
				WHERE tree.parent_id=0
				AND tree.desktop!=0
				ORDER BY tree.order_number");
		}
		else
		{
			$mod_name=explode('::',$mod_name);
			$mod=$mod_name[0];
			$name=$mod_name[1];
			$r=$_db->query("SELECT tree.id FROM `#__sys_tree` AS `tree`
				LEFT JOIN `#__sys_mods` AS `mods`
				ON tree.mod_id = mods.id
				WHERE tree.name='".$name."'
				AND mods.name='".$mod."'");
			list($parent_id)=$_db->fetchArray($r);
			$r=$_db->query("SELECT tree.*,
				CONCAT(mods.name,'::',tree.name) AS `name`,
				mods.name AS `mod_name`,
				CONCAT(mods.name,'::',actions.name) AS `action`
				FROM `#__sys_tree` AS `tree`
				LEFT JOIN `#__sys_mods` AS `mods`
				ON tree.mod_id = mods.id
				LEFT JOIN `#__sys_mods_actions` AS `actions`
				ON tree.action_id = actions.id
				WHERE tree.parent_id=".intval($parent_id)."
				AND tree.desktop!=0
				ORDER BY tree.order_number");
		}
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['name']=sys::translate($obj['name']);

			if(!$obj['icon']=self::getIconSrc($obj['mod_name'].'::'.$obj['icon']))
				$obj['icon']=_MODS_URL.'sys/backend/skins/default/img/ico.default.gif';

			if(sys::checkAccess($obj['action']))
				$var['objects'][]=$obj;
		}
		return sys::parseTpl('sys::icons',$var);
	}
	//=========================================================



	//Подговтовить кнопки
	static function prepareButtons(&$buttons)
	{
		foreach($buttons as $name=>$button)
		{
			$result[$name]=self::prepareButton($name,$button);
		}
		return $result;
	}
	//=====================================================================

	static function prepareButton($name,$button)
	{
		global $_hotkeys;
		if($name=='spacer')
			$button['title']=false;

		if(!isset($button['title']))
			$button['title']=sys::translate('sys::'.$name);

		if(!isset($button['mod']))
			$button['mod']=$_GET['mod'];

		if(!isset($button['image']))
			$button['image']='sys::btn.'.$name.'.gif';

		if(!isset($button['onclick']))
			$button['onclick']=false;

		if(!isset($button['frame']))
			$button['frame']=false;

		if(!isset($button['frame::height']))
			$button['frame::height']=200;

		if(!isset($button['frame::width']))
			$button['frame::width']=200;

		if(!isset($button['url']))
			$button['url']=false;

		if(isset($button['hotkey']))
		{
			$_hotkeys[$button['hotkey']]=$button['onclick'];
			$button['title'].=' CTRL+'.$button['hotkey'];
		}

		if(!$button['src']=self::getIconSrc($button['image']))
			$button['src']=_MODS_URL.'sys/backend/skins/default/img/btn.default.gif';
		return $button;
	}

	//Показать кнопки
	static function showButtons($buttons)
	{
		$var['objects']=self::prepareButtons($buttons);
		return sys::parseTpl('sys::buttons',$var);
	}
	//=========================================================

	static function showPages($pages)
	{
		global $_cfg;
		sys::useLib('sys::form');

		$on_page['input']='selectbox';
		$on_page['type']='int';

		if(isset($_COOKIE['sys::on_page']))
			$on_page['value']=intval($_COOKIE['sys::on_page']);
		if(isset($_GET['on_page']))
		{
			$on_page['value']=intval($_GET['on_page']);
			sys::setCookie('sys::on_page',$_GET['on_page']);
		}
		if(!isset($on_page['value']))
			$on_page['value']=$_cfg['sys::on_page'];

		$id_city = (isset($_GET['id_city']))?"&id_city=".$_GET['id_city']:"";	
			
		$on_page['values'][5]=5;
		$on_page['values'][10]=10;
		$on_page['values'][20]=20;
		$on_page['values'][30]=30;
		$on_page['values'][40]=40;
		$on_page['values'][50]=50;
		$on_page['values'][60]=60;
		$on_page['values'][70]=70;
		$on_page['values'][80]=80;
		$on_page['values'][90]=90;
		$on_page['values'][100]=100;
		$on_page['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\');window.location=\''.sys::cutGetParams(array('on_page','page')).'&page=1&on_page=\'+this.value"'.$id_city;
		$pages['on_page']=$on_page;
		return sys::parseTpl('sys::pages',$pages);
	}


	//Показать стандартную таблицу
	static function showTable($params, $tpl=false)
	{
		sys::useLib('sys::form');
		sys::filterGet('order_by');
		if(!$tpl)
			$tpl='sys::table';

		if(!isset($params['checkbox']))
			$params['checkbox']=false;

		if(!isset($params['msg']))
			$params['msg']=sys::translate('sys::nothing_found');

		if($_GET['order_by'])
		{
			$order_by=explode('.',$_GET['order_by']);

			if($order_by[1]=='asc')
			{
				$params['order_image']=self::getIconSrc('sys::btn.asc.gif');
				$params['order_url']=sys::cutGetParams('order_by').'&order_by='.$order_by[0].'.desc';
				$params['order_title']=sys::translate('sys::asc');
				$order='desc';
			}
			else
			{
				$params['order_image']=self::getIconSrc('sys::btn.desc.gif');
				$params['order_url']=sys::cutGetParams('order_by').'&order_by='.$order_by[0].'.asc';
				$params['order_title']=sys::translate('sys::desc');
				$order='asc';
			}
		}
		else
		{
			$order_by=false;
			$params['order_image']=self::getIconSrc('sys::btn.asc.gif');
			$params['order_url']=false;
			$params['order_title']=sys::translate('sys::asc');
			$order='desc';
		}

		//Обход полей
		foreach ($params['fields'] as $key=>$val)
		{
			if(!isset($val['title']))
				$val['title']=htmlspecialchars(sys::translate('sys::'.$key));
			else
				$val['title']=htmlspecialchars($val['title']);

			if(!isset($val['alt']))
				$val['alt']=$val['title'];

			if(!isset($val['width']))
				$val['width']=false;

			if(!isset($val['onclick']))
				$val['onclick']=false;

			if(!isset($val['frame']))
				$val['frame']=false;

			if(!isset($val['html_params']))
				$val['html_params']=false;

			if(!isset($val['url']))
				$val['url']=false;

			if(!isset($val['path']))
				$val['path']=false;

			if(!isset($val['src']))
				$val['src']=false;

			if(!isset($val['bold']))
				$val['bold']=false;

			if(!isset($val['italic']))
				$val['italic']=false;

			if(!isset($val['values']))
				$val['values']=false;

			if($key=='id' && !$val['width'])
				$val['width']=20;

			if(isset($val['order']))
			{
				if($key==$order_by[0])
				{
					$val['class']='active';
					$val['order_url']=sys::cutGetParams('order_by').'&order_by='.$key.'.'.$order;
				}
				else
				{
					$val['class']=false;
					$val['order_url']=sys::cutGetParams('order_by').'&order_by='.$key.'.'.$order_by[1];
				}
			}
			else
				$val['order_url']=false;

			if($val['type']=='button')
				$val['title']=false;

			if(($val['type']=='textbox' || $val['type']=='button' || $val['type']=='checkbox' || $val['type']=='selectbox') && !$val['width'])
				$val['width']=1;

			$params['fields'][$key]=$val;
		}

		$params['objects']=array();
		//Обход записей
		if(isset($params['records']))
		{
			foreach ($params['records'] as $id=>$record)
			{
				foreach ($params['fields'] as $key=>$field)
				{
					if(!isset($record[$key]))
						$record[$key]=false;

					//Обход собственных параметров запписи
					foreach ($field as $param_key=>$param)
					{
						if(isset($params['params'][$id][$key][$param_key]))
							$field[$param_key]=$params['params'][$id][$key][$param_key];
						if(!isset($field[$param_key]))
							$field[$param_key]=false;
					}

					foreach ($record as $r_key=>$r_val)
					{
						if($field['html_params'])
							$field['html_params']=str_replace('%'.$r_key.'%',htmlspecialchars($r_val),$field['html_params']);
						if($field['onclick'])
							$field['onclick']=str_replace('%'.$r_key.'%',htmlspecialchars($r_val),$field['onclick']);
						if($field['url'])
							$field['url']=str_replace('%'.$r_key.'%',htmlspecialchars($r_val),$field['url']);
						if($field['src'])
							$field['src']=str_replace('%'.$r_key.'%',htmlspecialchars($r_val),$field['src']);
						if($field['frame'])
							$field['frame']=str_replace('%'.$r_key.'%',htmlspecialchars($r_val),$field['frame']);
					}

					switch ($field['type'])
					{
						case 'button':
							$field=self::prepareButton($key,$field);
							/*
							if(!isset($field['image']))
								$field['image']='sys::btn.'.$key.'.gif';
							$field['image']=self::getIconSrc($field['image']);
							*/
						break;

						case 'checkbox':
							$checkbox['input']='checkbox';
							$checkbox['title']=$field['alt'];
							$checkbox['value']=$record[$key];
							$checkbox['html_params']=$field['html_params'];
							$obj[$key]=sys_form::parseField($key.'['.$id.']',$checkbox);
						break;

						case 'selectbox':
							$selectbox['input']='selectbox';
							$selectbox['title']=$field['alt'];
							$selectbox['value']=$record[$key];
							$selectbox['values']=$field['values'];
							$selectbox['html_params']=$field['html_params'];
							$obj[$key]=sys_form::parseField($key.'['.$id.']',$selectbox);
						break;

						case 'textbox':
							$textbox['input']='textbox';
							$textbox['title']=$field['alt'];
							$textbox['value']=$record[$key];
							$textbox['html_params']=$field['html_params'];
							$obj[$key]=sys_form::parseField($key.'['.$id.']',$textbox);
						break;

						/*case 'image':
							$title=$record[$key];
							$obj[$key]['src']=$field['src'];
							$obj[$key]['url']=$field['url'];
							$obj[$key]['title']=$record[$key];
						break;
						*/

						case 'custom':
							$obj[$key]=$record[$key];
						break;

						default:
							if(!isset($record[$key]))
								$record[$key]=' ';
							$obj[$key]=htmlspecialchars($record[$key]);
					}
					$obj['fields'][$key]=$field;

				}
				$params['objects'][$id]=$obj;
			}
		}

		return sys::parseTpl($tpl,$params);
	}
	//================================================================

	//Обработать аякс-запрос action~table~field~record_id~value~type~title
	static function processAjaxRequest($task)
	{
		global $_db, $_cfg;
		sys::sendHeaders();
		sys::setTpl();
		$err=false;
		$var=array();
		$task=explode('~',$task);
		for($i=0; $i<8; $i++)
		{
			if(!isset($task[$i]))
				$task[$i]=false;
		}

		$actions=array('switch','set');

		$action=$task[0];
		$table=$task[1];
		$field=$task[2];
		$record_id=$task[3];
		$value=$task[4];
		$type=$task[5];
		$title=$task[6];
		$key_field=$task[7];

		if(!$key_field)
			$key_field='id';

		if(!$title)
			$title=sys::translate('sys::'.$field);

		if(in_array($action,$actions))
		{
			if($type)
			{
				sys::useLib('sys::check');
			if($type=='date')
			{
				eval("\$err=sys_check::check".$type."(\$value,false,true);");
				$var['format']=$_cfg['sys::date_time_format'];
			}
			else
			{
				eval("\$err=sys_check::check".$type."(\$value,true);");
			}
			if($err)
					$err=$title.': '.sys::translate('sys::'.$err,$var);
			}
			if($err)
			{
				?>alert("<?=$err?>");<?
				?>hideMsg();<?
				return true;
			}
			/*
			if($action=='switch')
			{
				if($value)
					$value=0;
				else
					$value=1;

			}
			*/
			if(!$err)
			{
				if($type=='date')
					$value=sys::date2Db($value,true);
				if($action=='switch')
				{
					$value=$_db->getValue($table, $field, $record_id,false, $key_field);
					if($value)
						$value=false;
					else
						$value=true;
				}

				if(!$_db->setValue($table, $field, $value, $record_id, $type, $key_field))
				{
					?>alert('<?=sys::translate('sys::db_error').$_db->err?>');<?
				}
			}
		}

		?>hideMsg();<?
		return true;
	}
	//=====================================================================

	/**
	 * Enter description here...
	 *
	 */
	static function afterSave()
	{
		global $_js_header;
		sys::filterGet('id','int');
		ob_start();
		?>
			if(window.opener)
			{
				setMsg('<?=sys::translate('sys::loading')?>','window.opener');
				window.opener.location=window.opener.location;
				<?if($_GET['id'] && $_POST['_task']=='save' && strpos($_SERVER['REQUEST_URI'],'&id=')===false):?>
					document.write('<?=sys::translate('sys::saving')?>');
					window.location='<?=sys::cutGetParams('id').'&id='.$_GET['id']?>';
				<?endif;?>
				<?if($_POST['_task']=='save_and_close'):?>
					window.close()
				<?endif;?>
			}
			else
			{
				<?if($_POST['_task']=='save_and_close'):?>
					window.location='?mod=sys&act=default';
				<?endif;?>
			}
		<?
		$_js_header.=ob_get_clean();
	}

	//Подгрузить узлы в дерево
	static function mergeTree($parent_id,$nodes)
	{
		$var['objects']=$nodes;
		$var['parent_id']=$parent_id;
		ob_start();
		sys::sendHeaders();
		?>
		<?if(count($nodes)):?>
			if(document.getElementById('tree_list_<?=$parent_id?>'))
			{
				document.getElementById('tree_list_<?=$parent_id?>').parentNode.removeChild(document.getElementById('tree_list_<?=$parent_id?>'));
			}
			document.getElementById('tree_node_<?=$parent_id?>').innerHTML+="<?=addslashes(sys::stripNewLine(sys::parseTpl('sys::tree',$var)))?>";
		<?else:?>
			document.getElementById('tree_img_<?=$parent_id?>').src="<?=self::getIconSrc('sys::pixel.gif')?>";
		<?endif;?>
		<?
		echo ob_get_clean();
		sys::setTpl();
	}
	//============================================

	static function mergeExternalTree($parent_id,$nodes,$window,$active_node_id=false)
	{
		global $_js_header;
		$var['objects']=$nodes;
		$var['parent_id']=$parent_id;
		ob_start();
		?>
		if(<?=$window?>)
		{
			if(<?=$window?>.document.getElementById('tree_node_<?=$parent_id?>'))
			{
				if(<?=$window?>.document.getElementById('tree_list_<?=$parent_id?>'))
				{
					<?=$window?>.document.getElementById('tree_list_<?=$parent_id?>').parentNode.removeChild(<?=$window?>.document.getElementById('tree_list_<?=$parent_id?>'));
				}
				<?if(count($nodes)):?>
					<?=$window?>.document.getElementById('tree_node_<?=$parent_id?>').innerHTML+="<?=addslashes(sys::stripNewLine(sys::parseTpl('sys::tree',$var)))?>";
					<?=$window?>.document.getElementById('tree_img_<?=$parent_id?>').src="<?=self::getIconSrc('sys::minus.gif')?>";
				<?else:?>
					<?=$window?>.document.getElementById('tree_img_<?=$parent_id?>').src="<?=self::getIconSrc('sys::pixel.gif')?>";
				<?endif;?>
				<?if($active_node_id):?>
					for(i=0; i<<?=$window?>.document.links.length; i++)
					{
						<?=$window?>.document.links[i].className='';
					}
					<?=$window?>.document.getElementById('tree_link_'+<?=$active_node_id?>).className='active';
				<?endif;?>
			}
		}
		<?
		$_js_header.=ob_get_clean();
	}

	static function setMsg($text,$window=false)
	{
		global $_js_header;
		$_js_header.="setMsg('".$text."','".addslashes($window)."');";
	}
	//===============================================================================

	static function reloadWindow($window, $extra=false)
	{
		global $_js_header;
		$_js_header.='if('.$window.' && '.$window.'.document.body.innerHTML!="'.sys::translate('sys::loading').'"){setMsg("'.sys::translate('sys::loading').'","'.$window.'");'.$window.'.location.href='.$window.'.location.href+"'.$extra.'";}';
	}
	//===========================================================================

	static function windowLocation($window, $location)
	{
		global $_js_header;
		$_js_header.='if('.$window.'){setMsg("'.sys::translate('sys::loading').'","'.$window.'");'.$window.'.location.href="'.$location.'";}';
	}

	static function getNavigatorBranch($parent_id)
	{
		global $_db;
		$result=array();

		$q = "SELECT 
				tree.id,
				CONCAT(mods.name,'::',tree.name) AS `title`,
				tree.url, 
				CONCAT(mods.name,'::',actions.name) AS `action`
			
			FROM `#__sys_tree` AS `tree`
			
			LEFT JOIN `#__sys_mods_actions` AS `actions`
				ON tree.action_id = actions.id
				
			LEFT JOIN `#__sys_mods` AS `mods`
				ON tree.mod_id = mods.id
				
			WHERE tree.parent_id='".intval($parent_id)."'
				AND tree.navigator=1
				
			ORDER BY tree.order_number
			";
		$r=$_db->query($q);

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=sys::translate($obj['title']);

			if($obj['url']=='?mod=sys&act=login')
				$obj['onclick']="window.parent.location='".$obj['url']."'";
			else
				$obj['onclick']="setMsg('".sys::translate('sys::loading')."','window.parent.fra_right'); window.parent.fra_right.location='".$obj['url']."';window.parent.fra_right.focus();";

			if(!sys::checkAccess($obj['action'])) continue;
			
			//Проверить есть ли у узла потомки
			$r2=$_db->query("SELECT  CONCAT(mods.name,'::',actions.name) AS `action`
				FROM `#__sys_tree` AS `tree`
				LEFT JOIN `#__sys_mods_actions` AS `actions`
					ON actions.id=tree.action_id
				LEFT JOIN `#__sys_mods` AS `mods`
					ON tree.mod_id = mods.id
				WHERE tree.parent_id='".mysql_real_escape_string($obj['id'])."'
				AND tree.navigator='1'");

			$obj['children']=false;
			while(list($action)=$_db->fetchArray($r2))
			{
				if(sys::checkAccess($action))
				{
					$obj['children']=true;
					break;
				}
			}

			$obj['children_url']='?mod=sys&act=navigator&parent_id='.$obj['id'];
			$result[]=$obj;
		}

//if(sys::isDebugIP()) 
//{
//	sys::printR($result);
//}
		return $result;
	}
	
	

	static function showSearchForm($params)
	{
		return sys::parseTpl('sys::form.search',$params);
	}
	
	static function showSearchFormWithDatepickers($params)
	{
		return sys::parseTpl('sys::form.search_with_datepickers',$params);
	}

	static function windowClose($window='window')
	{
		global $_js_header;
		$_js_header.=$window.'.close();';
	}

	static function showTabs($tabs, $active_tab)
	{
		foreach ($tabs as $key=>$val)
		{
			if(!isset($val['title']) || !$val['title'])
				$tabs[$key]['title']=sys::translate('sys::'.$key);
		}
		$var['tabs']=$tabs;
		$var['active']=$active_tab;
		return sys::parseTpl('sys::tabs',$var);
	}
	
	static function showDataPickers(){
		$html = '<div class="container_dates">';
		$html .= '<span><input type="text" id="date-start" size="8" /></span>';
		$html .= '<span><input type="text" id="date-end" size="8" /></span>';
		$html .= '</div>';
		return $html;
	}

	static function showSpacer()
	{
		return '&nbsp;<img align="absmiddle" class="button spacer" alt="" src="'._IMG_URL.'spacer.gif">&nbsp;';
	}

	static function reloadTreeBranch($window,$node_id=false,$children, $active_node=false)
	{
		if($node_id)
			self::mergeExternalTree($node_id,$children,$window,$active_node);
		else
			self::reloadWindow($window);
	}

	static function showOptionsList($objects, $field)
	{
		ob_start();
		?>document.getElementById("_ajx_<?=$field?>_list").innerHTML='';<?
		if($objects)
		{
			foreach ($objects as $obj)
			{
				?>document.getElementById("_ajx_<?=$field?>_list").innerHTML+='<a class="list" href="#" onclick="addOption(\'_ajx_<?=$field?>_values\',\'<?=$obj['id']?>\',\'<?=htmlspecialchars(addslashes($obj['name']))?>\',true);this.style.display=\'none\';return false"><?=$obj['name']?></div>';<?
			}
		}
		else
		{
			?>document.getElementById("_ajx_<?=$field?>_list").innerHTML='<?=sys::translate('sys::nothing_found')?>';<?
		}
		return ob_get_clean();
	}

	static function showOptionsLst($objects, $field)
	{
		ob_start();
		?>document.getElementById("_ajx_<?=$field?>_list").innerHTML='';<?
		if($objects)
		{
			foreach ($objects as $obj)
			{
				?>document.getElementById("_ajx_<?=$field?>_list").innerHTML+='<a class="list" href="#" onclick="document.getElementById(\'film_id\').value=\'<?=$obj['id']?>\';document.getElementById(\'film_name\').value=\'<?=$obj['name']?>\';this.style.display=\'none\';this.parentNode.parentNode.style.display=\'none\';return false"><?=$obj['name']?></div>';<?
			}
		}
		else
		{
			?>document.getElementById("_ajx_<?=$field?>_list").innerHTML='<?=sys::translate('sys::nothing_found')?>';<?
		}
		return ob_get_clean();
	}


	static function showValuesList($objects, $field)
	{
		ob_start();
		?>document.getElementById("_ajx_<?=$field?>_list").innerHTML='1111';

		<?

		if($objects)
		{
			foreach ($objects as $obj)
			{			
				$args = "";	
				foreach ($obj as $k=>$fld)
					$args .= (htmlspecialchars(addslashes("'$k=$fld'")).",");
				$args .= "\'caller_identification=$field\'"	
						
				?>document.getElementById("_ajx_<?=$field?>_list").innerHTML+='<a class="list" href="#" onclick="setValue(\'_ajx_<?=$field?>_value\',\'<?=$obj['id']?>\',\'<?=htmlspecialchars(addslashes($obj['name']))?>\');this.parentNode.parentNode.style.display=\'none\';userFunctionCall(<?=$args?>);return false"><?=$obj['name']?></div>';<?
			}
		}
		else
		{
			?>document.getElementById("_ajx_<?=$field?>_list").innerHTML='<?=sys::translate('sys::nothing_found')?>';<?
		}

		return ob_get_clean();
	}
}
?>