function hideFrame(frame_id, obj, hide_image, show_image, hide_text, show_text, bottom_height, top_height)
{
	var img_hide=new Image();
	var img_show=new Image();
	img_hide.src=hide_image;
	img_show.src=show_image;
	//Скрыть
	if(obj.src==img_hide.src)
	{
		document.getElementById(frame_id).style.display='none';
		obj.src=img_show.src
		obj.alt=show_text;
		obj.title=show_text;
		if(top_height)
			document.getElementById('right').style.height='100%';
	}
	else //Показать
	{
		document.getElementById(frame_id).style.display='';
		obj.src=img_hide.src
		obj.alt=hide_text;
		obj.title=hide_text;
		if(top_height)
			document.getElementById('right').style.height=top_height;
		if(bottom_height)
			document.getElementById('bottom').style.height=bottom_height;
	}
}

function prepareTable()
{
	if(document.getElementById('thead1'))
	{
		document.getElementById('thead1').style.width=document.getElementById('tbody').offsetWidth;
		
		var rows=document.getElementById('tbody').tBodies[0].firstChild.childNodes;
		var heads=document.getElementById('thead1').tBodies[0].firstChild.childNodes;
		
		for(i=0; i<rows.length; i++)
		{
			if(rows[i].offsetWidth)
				heads[i].style.width=rows[i].offsetWidth-5;
		}
		
	}
}

function setMsg(text,iframe)
{
	
	if(!iframe)
	{
		if(document.getElementById('msg'))
		{
			document.getElementById('msg').style.width=document.getElementById('framework').offsetWidth;
			document.getElementById('msg').style.height=document.getElementById('framework').offsetHeight;
			document.getElementById('msg').style.display='';	
			document.getElementById('msg').innerHTML=text;
			
		}
	}
	else
	{
		eval("if("+iframe+".document.getElementById('msg'))"+iframe+".document.getElementById('msg').style.display='';");
		eval("if("+iframe+".document.getElementById('msg'))"+iframe+".document.getElementById('msg').innerHTML=text;");	
	}
}

function hideMsg(iframe)
{
	if(!iframe)
		document.getElementById('msg').style.display='none';	
	else
		eval(iframe+".document.getElementById('msg').style.display='none';");
}

function prepareDocument()
{	
	
		
	if(window.name!='fra_left')
	{
		if(blank)
		{
			if(window.parent.location!=window.location)
			{
				if(window.parent.document.getElementById('right'))
					window.parent.document.getElementById('right').className='';
				document.body.className='blank';
			}	
		}
		else
		{
			if(window.parent.location!=window.location)
			{
				if(window.parent.document.getElementById('right'))
					window.parent.document.getElementById('right').className='panel';
			}
			document.body.className='';
		
		}
		
		
		if(window.location==top.location)
		{
			document.body.className='';
		}
	}	
}

function preparePanels()
{
	if(document.getElementById('left') || document.getElementById('right') || document.getElementById('bottom'))
		blank=true;
	else
		blank=false;
		
	if(document.getElementById('panels'))
	{
		document.getElementById('panels').style.width=document.getElementById('framework').offsetWidth;
		document.getElementById('panels_back').style.height=document.getElementById('panels').offsetHeight-2;
		if(blank)
		{
			if(window.top.location!=window.location)
			{
				document.getElementById('panels').style.left=0;
				document.getElementById('panels_back').style.height=document.getElementById('panels').offsetHeight;
			}
			document.getElementById('panels').style.top=0;	
		}
	}
}

function displayErr(err)
{
	if(err)
		alert(err);
}