<?php
class sys_mods
{		
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $mod_id
	 * @return unknown
	 */
	static function deleteModule($mod_id)
	{
		if($mod_id==1 || $mod_id==2)
			return false;
		global $_db;
		$r=$_db->query("SELECT `id` 
			FROM `#__sys_mods_actions` 
			WHERE `mod_id`=".intval($mod_id)."");
		while(list($action_id)=$_db->fetchArray($r))
		{
			self::deleteAction($action_id);
		}
		
		$r=$_db->query("SELECT `id` 
			FROM `#__sys_mods_cfg` 
			WHERE `mod_id`=".intval($mod_id)."");
		while(list($cfg_id)=$_db->fetchArray($r))
		{
			self::deleteCfg($cfg_id);
		}
		
		$r=$_db->query("SELECT `id` 
			FROM `#__sys_tree` 
			WHERE `mod_id`=".intval($mod_id)."");
		while(list($node_id)=$_db->fetchArray($r))
		{
			self::deleteTreeNode($node_id);
		}
		
		$r=$_db->query("SELECT `id` 
			FROM `#__sys_mods_entries` 
			WHERE `mod_id`=".intval($mod_id)."");
		while(list($entry_id)=$_db->fetchArray($r))
		{
			self::deleteEntry($entry_id);
		}
		
		$r=$_db->query("SELECT `id` 
			FROM `#__sys_mods_ucfg` 
			WHERE `mod_id`=".intval($mod_id)."");
		while(list($cfg_id)=$_db->fetchArray($r))
		{
			self::deleteUserCfg($cfg_id);
		}
		
		$r=$_db->query("SELECT `id` 
			FROM `#__sys_mods_udata` 
			WHERE `mod_id`=".intval($mod_id)."");
		while(list($data_id)=$_db->fetchArray($r))
		{
			self::deleteUserDataField($data_id);
		}
		
		$r=$_db->query("SELECT `id` 
			FROM `#__sys_mods_templates` 
			WHERE `mod_id`=".intval($mod_id)."");
		while(list($tpl_id)=$_db->fetchArray($r))
		{
			self::deleteTemplate($tpl_id);
		}
		
		$_db->delete('sys_mods',intval($mod_id));
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $action_id
	 */
	static function deleteAction($action_id)
	{
		global $_db;
		$_db->delete('sys_groups_rights',"`action_id`=".intval($action_id)."");
		$r=$_db->query("SELECT `id`
			FROM `#__sys_mods_actions`
			WHERE `parent_id`=".intval($action_id)."");
		while(list($child_id)=$_db->fetchArray($r))
		{
			self::deleteAction($child_id);
		}
		$_db->delete('sys_mods_actions',intval($action_id));
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $node_id
	 */
	static function deleteTreeNode($node_id)
	{
		global $_db;
		$r=$_db->query("SELECT `id`
			FROM `#__sys_tree`
			WHERE `parent_id`=".intval($node_id)."");
		while(list($child_id)=$_db->fetchArray($r))
		{
			self::deleteTreeNode($child_id);
		}
		$_db->delete('sys_tree',intval($node_id));
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $entry_id
	 */
	static function deleteEntry($entry_id)
	{
		global $_db;
		$_db->delete('sys_mods_entries',intval($entry_id));
	}
	
	static function deleteTask($task_id)
	{
		global $_db;
		$_db->delete('sys_mods_tasks',intval($task_id));
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $cfg_id
	 */
	static function deleteCfg($cfg_id)
	{
		global $_db;
		$_db->delete('sys_mods_cfg',intval($cfg_id));
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $mod_id
	 * @param unknown_type $active_tab
	 * @return unknown
	 */
	static function showTabs($mod_id, $active_tab)
	{
		$tabs['info']['url']='?mod=sys&act=mod_edit&id='.$mod_id;
		$tabs['cfg']['url']='?mod=sys&act=mod_cfg_table&mod_id='.$mod_id;
		$tabs['user_cfg']['url']='?mod=sys&act=mod_ucfg_table&mod_id='.$mod_id;
		$tabs['user_data']['url']='?mod=sys&act=mod_udata_table&mod_id='.$mod_id;
		$tabs['actions']['url']='?mod=sys&act=mod_actions&mod_id='.$mod_id;	
		$tabs['tasks']['url']='?mod=sys&act=mod_tasks_table&mod_id='.$mod_id;	
		$tabs['entries']['url']='?mod=sys&act=mod_entries_table&mod_id='.$mod_id;		
		$tabs['langs']['url']='?mod=sys&act=mod_langs_table&mod_id='.$mod_id;	
		return sys_gui::showTabs($tabs, $active_tab);
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $cfg_id
	 */
	static function deleteUserCfg($cfg_id)
	{
		global $_db;
		if($cfg_id==1)
			return false;
		$r=$_db->query("SELECT CONCAT(mods.name,'_',cfg.name) AS `cfg_name` 
			FROM `#__sys_mods_ucfg` AS `cfg`
			LEFT JOIN `#__sys_mods` AS `mods` 
				ON mods.id=cfg.mod_id
			WHERE cfg.id=".intval($cfg_id)."");
		list($cfg_name)=$_db->fetchArray($r);
		$_db->query("ALTER TABLE `#__sys_users_cfg` DROP `".$cfg_name."`");
		$_db->delete('sys_mods_ucfg',intval($cfg_id));
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $field_id
	 */
	static function deleteUserDataField($field_id)
	{
		global $_db;
		$r=$_db->query("SELECT CONCAT(mods.name,'_',data.name) AS `name`, data.multi_lang 
			FROM `#__sys_mods_udata` AS `data`
			LEFT JOIN `#__sys_mods` AS `mods` 
				ON mods.id=data.mod_id
			WHERE data.id=".intval($field_id)."");
		$field=$_db->fetchAssoc($r);
		if($field['multi_lang'])
			$_db->query("ALTER TABLE `#__sys_users_data_lng` DROP `".$field['name']."`");
		else 
			$_db->query("ALTER TABLE `#__sys_users_data` DROP `".$field['name']."`");
		$_db->delete('sys_mods_udata',intval($field_id));
	}
	
	/**
	 * Enter description here...
	 *
	 * @return unknown
	 */
	static function getFields()
	{
		$fields=sys::scanDir(_COMMON_DIR.'cfg.fields');
		foreach ($fields as $fld)
		{
			$result[$fld]=$fld;
		}
		return $result;
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $parent_id
	 * @param unknown_type $field
	 * @return unknown
	 */
	static function getActionsTreeBranch($parent_id)
	{
		global $_db;
		
		sys::filterGet('task');
		sys::filterGet('field');
		sys::filterGet('action_id','int');
		$result=array();
		$q="SELECT acts.id, acts.mod_id, acts.name AS `title`, CONCAT(mods.name,'::',acts.name) AS `full_title`, acts.mode 
			FROM `#__sys_mods_actions` AS `acts`
			LEFT JOIN `#__sys_mods` AS `mods`
			ON mods.id=acts.mod_id
			WHERE acts.parent_id=".intval($parent_id)." 
		";
		if($_GET['mod_id'])
			$q.="AND acts.mod_id=".intval($_GET['mod_id'])."";
		
		$q.="
			AND acts.mode='".mysql_real_escape_string($_GET['mode'])."'
			ORDER BY acts.order_number
			";
		
		$r=$_db->query($q);
		while($obj=$_db->fetchAssoc($r))
		{	
			switch($_GET['task'])
			{
				case 'setValue':	
					$obj['onclick']="window.parent.document.forms['form'].elements['".$_GET['field']."'].value=".$obj['id'].";";
					$obj['onclick'].="window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode.firstChild.innerHTML='".htmlspecialchars($obj['full_title'])."';";
					$obj['onclick'].="window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode.lastChild.style.display='none';return false;";
				break;
				
				case 'move':	
					$obj['onclick']="window.parent.document.forms['table'].elements['_task'].value='move.".$_GET['action_id'].".".$obj['id']."';";
					$obj['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
				break;
				
				case 'moveChecked':	
					$obj['onclick']="window.parent.document.forms['table'].elements['_task'].value='moveChecked.".$obj['id']."';";
					$obj['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
				break;
			
				default:
					$obj['onclick']="setMsg('".sys::translate('sys::loading')."','window.parent.fra_right'); window.parent.fra_right.location='?mod=sys&mod_id=".$obj['mod_id']."&mode=".$obj['mode']."&act=mod_actions_table&parent_id=".$obj['id']."';";
					$obj['url']="?mod=sys&mod_id=".$obj['mod_id']."&mode=".$obj['mode']."&act=mod_actions_table&parent_id=".$obj['id'];
			}
			//Проверить есть ли у узла потомки
			$r2=$_db->query("SELECT `id`
				FROM `#__sys_mods_actions`
				WHERE `parent_id`='".$obj['id']."'
				LIMIT 0,1
			");
			
			list($obj['children'])=$_db->fetchArray($r2);
			
			$obj['children_url']='?mod=sys&act=mod_actions_tree&mod_id='.$obj['mod_id'].'&mode='.$obj['mode'].'&task='.$_GET['task'].'&field='.$_GET['field'].'&parent_id='.$obj['id'].'&action_id='.$_GET['action_id'];	
			$result[]=$obj;
		}
		return $result;
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $action_id
	 * @param unknown_type $to_parent_id
	 * @return unknown
	 */
	static function moveAction($action_id, $to_parent_id)
	{
		global $_db;
		if(!self::checkActionParent($action_id,$to_parent_id))
			return false;
		$_db->setValue('sys_mods_actions','parent_id',$to_parent_id,$action_id,'int');
		
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $node_id
	 * @param unknown_type $to_parent_id
	 * @return unknown
	 */
	static function moveTreeNode($node_id, $to_parent_id)
	{
		global $_db;
		if(!self::checkTreeNodeParent($node_id,$to_parent_id))
			return false;
		$_db->setValue('sys_tree','parent_id',$to_parent_id,$node_id,'int');
		
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $action_id
	 * @return unknown
	 */
	static function getActionChildren($action_id)
	{
		global $_db;
		$result=array();
		$r=$_db->query("SELECT `id` FROM `#__sys_mods_actions` WHERE `parent_id`=".intval($action_id)."");
		while(list($child_id)=$_db->fetchArray($r))
		{
			$result[]=$child_id;
			$result=array_merge($result,self::getActionChildren($child_id));
		}
		return $result;
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $node_id
	 * @return unknown
	 */
	static function getTreeNodeChildren($node_id)
	{
		global $_db;
		$result=array();
		$r=$_db->query("SELECT `id` FROM `#__sys_tree` WHERE `parent_id`=".intval($node_id)."");
		while(list($child_id)=$_db->fetchArray($r))
		{
			$result[]=$child_id;
			$result=array_merge($result,self::getTreeNodeChildren($child_id));
		}
		return $result;
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $action_id
	 * @param unknown_type $parent_id
	 * @return unknown
	 */
	static function checkActionParent($action_id, $parent_id)
	{
		if($action_id==$parent_id)
			return false;
			
		if(in_array($parent_id,self::getActionChildren($action_id)))
			return false;
		
		return true;
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $node_id
	 * @param unknown_type $parent_id
	 * @return unknown
	 */
	static function checkTreeNodeParent($node_id, $parent_id)
	{
		if($node_id==$parent_id)
			return false;
			
		if(in_array($parent_id,self::getTreeNodeChildren($node_id)))
			return false;
		
		return true;
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $parent_id
	 * @return unknown
	 */
	static function getTreeBranch($parent_id)
	{
		global $_db;
		
		sys::filterGet('task');
		sys::filterGet('field');
		sys::filterGet('node_id','int');
		$result=array();
		
		$q = "SELECT tree.id, CONCAT(mods.name,'::',tree.name) AS `title` 
			FROM `#__sys_tree` AS `tree`
			LEFT JOIN `#__sys_mods` AS `mods`
			ON tree.mod_id=mods.id
			WHERE tree.parent_id=".intval($parent_id)." 
			ORDER BY tree.order_number
			";
		$r=$_db->query($q);
		
		while($obj=$_db->fetchAssoc($r))
		{	
			$obj['title']=sys::translate($obj['title']);
			switch($_GET['task'])
			{
				case 'setValue':	
					$obj['onclick']="window.parent.document.forms['form'].elements['".$_GET['field']."'].value=".$obj['id'].";";
					$obj['onclick'].="getChildByTagName(window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode,'A').innerHTML='".htmlspecialchars($obj['title'])."';";
					$obj['onclick'].="getChildByTagName(window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode,'DIV').style.display='none';return false;";
				break;
				
				case 'move':	
					$obj['onclick']="window.parent.document.forms['table'].elements['_task'].value='move.".$_GET['node_id'].".".$obj['id']."';";
					$obj['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
				break;
				
				case 'moveChecked':	
					$obj['onclick']="window.parent.document.forms['table'].elements['_task'].value='moveChecked.".$obj['id']."';";
					$obj['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
				break;
			
				default:
					$obj['onclick']="setMsg('".sys::translate('sys::loading')."','window.parent.fra_right'); window.parent.fra_right.location='?mod=sys&act=tree_table&parent_id=".$obj['id']."';";
					$obj['url']="?mod=sys&act=tree_table&parent_id=".$obj['id'];
			}
			//Проверить есть ли у узла потомки
			$r2=$_db->query("SELECT `id`
				FROM `#__sys_tree`
				WHERE `parent_id`='".$obj['id']."'
				LIMIT 0,1
			");
			
			list($obj['children'])=$_db->fetchArray($r2);
			
			$obj['children_url']='?mod=sys&act=tree_tree&task='.$_GET['task'].'&field='.$_GET['field'].'&parent_id='.$obj['id'].'&node_id='.$_GET['node_id'];	
			$result[]=$obj;
		}
		return $result;
	}
	
	static function deleteTemplate($tpl_id)
	{
		global $_db;
		if(!sys::checkAccess('sys::template_delete'))
			return false;
		$_db->delete('sys_mods_templates',$tpl_id, true);
	}
	
	static function deleteMetaTemplate($tpl_id)
	{
		global $_db;
		if(!sys::checkAccess('sys::template_delete'))
			return false;
		$_db->delete('sys_mods_metatemplates',$tpl_id, true);
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $mod_name
	 * @param unknown_type $group
	 * @return unknown
	 */
	static function getConfigFields($mod_name, $group=false)
	{
		global $_db;
		$result=array();
		if(!$mod_id=self::getModId($mod_name))
			return false;
		
		$q="SELECT `name`, `field`, `required`, `value` FROM `#__sys_mods_cfg` 
							WHERE `mod_id`='".$mod_id."'";
		if($group)
			$q.=" AND `group`='".mysql_real_escape_string($group)."'";
		
		$q.=" ORDER BY `order_number`";

		$r=$_db->query($q);
		while($field=$_db->fetchAssoc($r))
		{
			$field_name=$mod_name.'_'.$field['name'];
			$params['req']=$field['required'];
	
			if(is_file(_COMMON_DIR.'/cfg.fields/'.$field['field']))	
			{
				require(_COMMON_DIR.'/cfg.fields/'.$field['field']);
				$result[$field_name]=$params;
				unset($params);
			}
			else 
			{
				$result[$field_name]['input']='textbox';
				$result[$field_name]['type']='text';
			}
			
			$result[$field_name]['table']='sys_mods_cfg';
			$result[$field_name]['title']=sys::translate($mod_name.'::'.$field['name']);
		}
		return $result;
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $mod_name
	 * @param unknown_type $group
	 * @return unknown
	 */
	static function getConfigValues($mod_name, $group=false)
	{
		global $_db;
		$result=array();
		if(!$mod_id=self::getModId($mod_name))
			return false;
		
		$q="SELECT `name`, `value` FROM `#__sys_mods_cfg` 
				WHERE `mod_id`='".$mod_id."'";
		if($group)
			$q.=" AND `group`='".mysql_real_escape_string($group)."'";
		
		$r=$_db->query($q);
		while($values=$_db->fetchAssoc($r))
		{
			$result[$mod_name.'_'.$values['name']]=$values['value'];
		}
		return $result;
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $mod_name
	 * @return unknown
	 */
	static function getModId($mod_name)
	{
		global $_mods;
		$mod_id=false;
		foreach ($_mods as $id=>$name)
		{
			if($name==$mod_name)
			{
				$mod_id=$id;
				break;
			}
		}
		return $mod_id;
	}

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $mod_name
	 * @param unknown_type $values
	 * @param unknown_type $fields
	 * @return unknown
	 */
	static function updateConfigValues($mod_name, $values, $fields)
	{
		global $_db;
		$len=mb_strlen($mod_name)+1;
		if(!$mod_id=self::getModId($mod_name))
			return false;
		
		foreach($fields as $name=>$params)
		{
			if($params['table'] && $params['table']=='sys_mods_cfg')
			{
				$cfg_name=sys::cutStrLeft($name,$len);
				$value=$_db->processValue($values[$name],$params['type']);
				
				$_db->query("UPDATE `#__sys_mods_cfg` SET `value`=".$value."
							WHERE `mod_id`='".$mod_id."' AND `name`='".$cfg_name."'");	
			}
		}
	}
	
}
?>