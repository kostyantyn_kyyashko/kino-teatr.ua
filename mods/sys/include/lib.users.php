<?php
class sys_users
{		
	static function deleteGroup($group_id)
	{
		global $_db, $_cfg;
		
		if($group_id==2 || $group_id==7 || $group_id==8)
			return false;
		
		$_db->delete('sys_users_groups',"`group_id`=".intval($group_id)."");
		$_db->delete('sys_groups',$group_id,true);
	}
	
	static function showGroupTabs($group_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('sys::info');
		$tabs['info']['url']='?mod=sys&act=group_edit&id='.$group_id;
		
		$tabs['rights']['title']=sys::translate('sys::rights');
		$tabs['rights']['url']='?mod=sys&act=group_rights&group_id='.$group_id;
		
		return sys_gui::showTabs($tabs, $active_tab);
	}
	
	static function showUserTabs($user_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('sys::info');
		$tabs['info']['url']='?mod=sys&act=user_edit&id='.$user_id;
		
		$tabs['data']['title']=sys::translate('sys::data');
		$tabs['data']['url']='?mod=sys&act=user_data_edit&id='.$user_id;
		
		$tabs['cfg']['title']=sys::translate('sys::cfg');
		$tabs['cfg']['url']='?mod=sys&act=user_cfg_edit&id='.$user_id;
		
		if($user_id!=2)
		{
			$tabs['ips']['title']=sys::translate('sys::ips');
			$tabs['ips']['url']='?mod=sys&act=user_ips&user_id='.$user_id;
		}
		
		return sys_gui::showTabs($tabs, $active_tab);
	}
	
	static function getRightsTreeBranch($parent_id)
	{
		global $_db;
		sys::filterGet('group_id','int');
		
		$result=array();
		$q="SELECT acts.id, CONCAT(mods.name,'::',acts.name) AS `title` 
			FROM `#__sys_mods_actions` AS `acts`
			LEFT JOIN `#__sys_mods` AS `mods`
			ON mods.id=acts.mod_id
			WHERE acts.parent_id=".intval($parent_id)." 
			AND acts.mode='".mysql_real_escape_string($_GET['mode'])."'
			ORDER BY acts.order_number
			";
		$r=$_db->query($q);
		while($obj=$_db->fetchAssoc($r))
		{	
		
			$obj['title']=sys::translate($obj['title']);
			$obj['onclick']="setMsg('".sys::translate('sys::loading')."','window.parent.fra_right'); window.parent.fra_right.location='?mod=sys&group_id=".$_GET['group_id']."&mode=".$_GET['mode']."&act=group_rights_table&parent_id=".$obj['id']."';";
			$obj['url']="?mod=sys&group_id=".$_GET['group_id']."&mode=".$_GET['mode']."&act=group_rights_table&parent_id=".$obj['id'];
			
			//Проверить есть ли у узла потомки
			$r2=$_db->query("SELECT `id`
				FROM `#__sys_mods_actions`
				WHERE `parent_id`='".$obj['id']."'
				LIMIT 0,1
			");
			
			list($obj['children'])=$_db->fetchArray($r2);
			
			$obj['children_url']='?mod=sys&act=group_rights_tree&group_id='.$_GET['group_id'].'&mode='.$_GET['mode'].'&parent_id='.$obj['id'];	
			$result[]=$obj;
		}
		return $result;
	}
	
	static function disallowAction($group_id,$action_id, $recursive=false)
	{
		global $_db;
		if($recursive)
		{
			$r=$_db->query("SELECT `id` FROM `#__sys_mods_actions` WHERE `parent_id`=".intval($action_id)."");
			while(list($child_id)=$_db->fetchArray($r))
			{
				self::disallowAction($group_id,$child_id,true);
			}
		}
		$_db->delete('sys_groups_rights',"`group_id`=".intval($group_id)." AND `action_id`=".intval($action_id)."");
		
	}
	
	static function allowAction($group_id,$action_id,$recursive=false)
	{
		global $_db;
		if($recursive)
		{
			$r=$_db->query("SELECT `id` FROM `#__sys_mods_actions` WHERE `parent_id`=".intval($action_id)."");
			while(list($child_id)=$_db->fetchArray($r))
			{
				self::allowAction($group_id,$child_id,true);
			}
		}
		$_db->query("INSERT INTO `#__sys_groups_rights` VALUES ('".intval($group_id)."','".intval($action_id)."')");
	}
	
	static function deleteUser($user_id)
	{
		global $_db, $_mods;
		
		if($user_id==2 || $user_id==7)
			return false;
		$_db->delete('sys_users_social_network_info',"`user_id`=".intval($user_id)."");
		$_db->delete('sys_users_cfg',"`user_id`=".intval($user_id)."");
		$_db->delete('sys_users_data',intval($user_id),true,'user_id');
		$_db->delete('sys_users_groups',"`user_id`=".intval($user_id)."");
		$_db->delete('sys_users',intval($user_id));
		
		foreach ($_mods as $id=>$name)
		{
			if($name!='sys' && is_file(_MODS_DIR.$name.'/include/lib.users.php'))
			{
				sys::useLib($name.'::users');
				eval($name."_users::deleteUser(\$user_id);");
			}
		}
		
	}
	
	static function getUserGroupsIds($user_id)
	{
		global $_db;
		$result=array();
		$r=$_db->query("SELECT `group_id` FROM `#__sys_users_groups` WHERE `user_id`=".intval($user_id)."");
		while(list($group_id)=$_db->fetchArray($r))
		{
			$result[]=$group_id;
		}
		return $result;
	}
	
	static function saveGroups($user_id, $groups)
	{
		global $_db;
		$_db->delete('sys_users_groups',"`user_id`=".intval($user_id)."");
		foreach ($groups as $group_id)
		{
			$_db->query("INSERT INTO `#__sys_users_groups` VALUES (".intval($user_id).", ".intval($group_id).")");
		}
	}
	
	static function getCfg($user_id)
	{
		global $_db, $_cfg;
		$result=$_db->getRecord('sys_users_cfg',$user_id,false,'user_id');
		foreach ($result as $key=>$val)
		{
			if(strpos($key,'date')!==false)
				$result[$key]=sys::db2Date($result[$key],false,$_cfg['sys::date_format']);
		}
		return $result;
	}
	
	static function getData($user_id)
	{
		global $_db, $_cfg;
		$result=$_db->getRecord('sys_users_data',$user_id,true,'user_id');
		foreach ($result as $key=>$val)
		{
			if(strpos($key,'date')!==false)
				$result[$key]=sys::db2Date($result[$key],false,$_cfg['sys::date_format']);
		}
		return $result;
	}
	
	static function getCfgFields()
	{
		global $_db;
		$result=array();
		$r=$_db->query("SELECT 
				CONCAT(mods.name,'_',cfg.name) AS `name`,
				CONCAT(mods.name,'::',cfg.name) AS `title`,
				cfg.field,
				cfg.required
			FROM `#__sys_mods_ucfg` AS `cfg`
			LEFT JOIN `#__sys_mods` AS `mods`
				ON mods.id=cfg.mod_id
			ORDER BY mods.order_number, cfg.order_number 
		");
		
		while($params=$_db->fetchAssoc($r))
		{
			$params['title']=sys::translate($params['title']);
			$params['table']='sys_users_cfg';
			$params['req']=$params['required'];
			
			include(_COMMON_DIR.'cfg.fields/'.$params['field']);
			$result[$params['name']]=$params;
			unset($params);
			
		}
		return $result;
	}
	
	static function getDataFields()
	{
		global $_db;
		$r=$_db->query("SELECT 
				CONCAT(mods.name,'_',data.name) AS `name`,
				CONCAT(mods.name,'::',data.name) AS `title`,
				data.field,
				data.required,
				data.multi_lang
			FROM `#__sys_mods_udata` AS `data`
			LEFT JOIN `#__sys_mods` AS `mods`
				ON mods.id=data.mod_id
			ORDER BY mods.order_number, data.order_number
		");
		
		while($params=$_db->fetchAssoc($r))
		{
			$params['title']=sys::translate($params['title']);
			$params['table']='sys_users_data';
			$params['req']=$params['required'];
			
			include(_COMMON_DIR.'cfg.fields/'.$params['field']);
			$result[$params['name']]=$params;
			unset($params);
		}
		return $result;
	}
	
	static function showProfileTabs($active_tab)
	{
		$tabs['info']['title']=sys::translate('sys::info');
		$tabs['info']['url']='?mod=sys&act=profile';
		
		$tabs['pass']['title']=sys::translate('sys::change_password');
		$tabs['pass']['url']='?mod=sys&act=profile_pass';
		
		return sys_gui::showTabs($tabs, $active_tab);
	}
	
	static function getGroupRights($group_id)
	{
		$r=$_db->query("
			SELECT 
			acts.parent_id,
			CONCAT(mods.name,'::',parents.name) AS `parent`,
			CONCAT(mods.name,'::',acts.name) AS `name`
			FROM `#__sys_groups_rights` AS `rights`
			       	
	    	LEFT JOIN `#__sys_mods_actions` AS `acts`
	    	ON acts.id=rights.action_id
	    	
	    	LEFT JOIN `#__sys_mods_actions` AS `parents`
	    	ON parents.id=acts.parent_id
	    	
	    	LEFT JOIN `#__sys_mods` AS `mods`
	       	ON mods.id=acts.mod_id
	       
			WHERE rights.group_id=".$group_id." 
			AND acts.mode='"._MODE."'
			       	
		");
		while($right=$_db->fetchAssoc($r))
		{
			if($right['parent_id'])
				$result['parents'][$right['name']]=$right['parent'];
			$result['names'][]=$right['name'];
		}
		$result['names']=array_unique($result['names']);
		return $result;
	}
	
	static function checkGroupAcess($action, $group_id, $rights=false)
	{
		if(!$rights)
			$rights=self::getGroupRights($group_id);
		if(!$action)
			return true;
		if(in_array($action, $rights['names']))
		{
			if(isset($rights['parents'][$action]))
				return self::checkGroupAcess($_rights['parents'][$action],$group_id,$rights);
			else 	
				return true;	
		}
		return false;
			
	}
	
	
}
?>