<?php
class cls_sys_db
{
	/**
	 * Enter description here...
	 *
	 * @var unknown_type
	 */
	public $err;
	/**
	 * Enter description here...
	 *
	 * @var unknown_type
	 */
	public $last_id;
	/**
	 * Enter description here...
	 *
	 * @var unknown_type
	 */
	public $query;

	//Конструктор
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $db_host
	 * @param unknown_type $db_user
	 * @param unknown_type $db_pass
	 * @param unknown_type $db_name
	 * @return cls_sys_db
	 */
	function cls_sys_db($db_host=false, $db_user=false,$db_pass=false,$db_name=false)
	{
		//Определить параметры соединения
		if(!$db_host)
			$db_host=_DB_HOST;
		if(!$db_user)
			$db_user=_DB_USER;
		if(!$db_pass)
			$db_pass=_DB_PASS;
		if(!$db_name)
			$db_name=_DB_NAME;

		return $this->connect($db_host, $db_user, $db_pass, $db_name);

	}
	//==========================================================================//

	//Подключение к БД
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $db_host
	 * @param unknown_type $db_user
	 * @param unknown_type $db_pass
	 * @param unknown_type $db_name
	 * @return unknown
	 */
	function connect($db_host=false,$db_user=false,$db_pass=false,$db_name=false)
	{
		if(!$db_host)
			$db_host=_DB_HOST;
		if(!$db_user)
			$db_user=_DB_USER;
		if(!$db_pass)
			$db_pass=_DB_PASS;
		if(!$db_name)
			$db_name=_DB_NAME;

		mysql_connect($db_host, $db_user, $db_pass) or die(mysql_error());
		mysql_select_db($db_name) or die(mysql_error());
		return true;
	}
	//========================================================================//


	function close()
	{
		mysql_close();
	}

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $query
	 */
	function printR($query)
	{
		sys::printR(str_replace('#__',_DB_PREFIX,$query));
	}

	//Запрос к БД
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $query
	 * @return unknown
	 */
	function query($query)
	{
		global $_cfg, $_debug , $_user, $_errors;

		//Определить тип запроса
		$query = trim($query);
		$type = explode(' ', $query);
		$type = mb_strtoupper($type[0]);
		//===========================//

		$query=$this->query = str_replace('#__',_DB_PREFIX,$query);//Вставить префикс в запросы

		//Получить результат запроса и сделать замер времени
		$start=getmicrotime();
		$result = mysql_query($query);
		$time = getMicrotime()-$start;
		//===============================//

		//Обработка ошибок
		if(!$result)
		{
			$this->err=mysql_error();
			if($_cfg['sys::error_reporting'])
			{
				//Добавить ошибку в массив ошибок
				$err['type']='SQL';
				$err['query']=$query;
				$err['error']=$this->err;
				$_errors[]=$err;
				//============================//
			}
			if($_cfg['sys::log_sql_error'])
			{		
				$get = "";
				foreach ($_GET as $key=>$value) 
				{
							$get .= "$key=$value, ";
				}		
				if($this->err != "MySQL server has gone away")
					sys::writeToFile(_LOGS_DIR.'err.sql',"/* Time: ".date("d.m.Y H:i:s")." */\n/* Uri: ".$_SERVER['REQUEST_URI']." */\n/* Referer: ".$_SERVER['HTTP_REFERER']." */\nQuery:\n ".$query."\n/* Error: ".$this->err." */\n/* GET: $get */\n\n\n\n");
				else	
					sys::writeToFile(_LOGS_DIR.'err.sql',"/* ".date("d.m.Y H:i:s")."\t".$this->err." */\n");
			}
			return false;
		}
		//===================================//


		//Дебаг
		if($_cfg['sys::debug_mode'])
		{
			$_debug['sql::time']+=$time;//Добавить время запросоа к общему времени выполнения запросов

			$_debug['sql::number']++;//Обновить счетчик запросов

			//Показать время выполнения запроса и експлейн
			$explain['query']=str_replace("\t",'',$query);
			$explain['time']=$time;
			$explain['explain']=$this->explain($query);
			$_debug['sql::explain'][]=$explain;
			//===================================//
		}
		//==================================//

		//Запись в логи
		if($_cfg['sys::log_sql'])
		{
			if($_cfg['sys::log_sql']=='changes')
			{
				if($type!='SELECT' && strpos($query,"SET NAMES 'UTF8'")!==0 && !strpos($query,"sys_sess`"))
					$this->logQuery($query,$time);
			}
			elseif($_cfg['sys::log_sql']=='all')
				$this->logQuery($query,$time);
		}
		//=====================================//

		//Получение ид последней записи
		if($type=='INSERT')
		{
			$li=mysql_query('SELECT LAST_INSERT_ID()');
			list($this->last_id)=mysql_fetch_array($li);
		}
		//==================================//
		return $result;
	}
	//==========================================================================

	//Запись запроса в лог
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $query
	 * @param unknown_type $time
	 */
	function logQuery($query, $time)
	{
		global $_user;
		if(isset($_user['id']))
			$user_id=$_user['id'];
		else
			$user_id=0;

		$query = str_replace("\n"," ",$query);
		$query = str_replace("\r"," ",$query);
		$query = str_replace("\t"," ",$query);
		sys::writeToFile(_LOGS_DIR.'log.sql',"/*\t".$_SERVER['REMOTE_ADDR']."\t".$user_id."\t".gmdate('Y-m-d H:i:s',time())."\t".$_SERVER['REQUEST_URI']."\t".$time."\t*/\n".$query."\n\n");
	}
	//===========================================================//

	//
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $r
	 * @return unknown
	 */
	function fetchArray($r)
	{
		return mysql_fetch_array($r);
	}
	//==============================================================//

	//
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $r
	 * @return unknown
	 */
	function numRows($r)
	{
		return mysql_num_rows($r);
	}
	//======================================================//

	//
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $r
	 * @return unknown
	 */
	function fetchAssoc($r)
	{
		return mysql_fetch_assoc($r);
	}
	//==========================================================================

	//Экслэйн зароса
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $query
	 * @return unknown
	 */
	function explain($query)
	{
		$explain=array();
		$r=mysql_query('EXPLAIN '.$query);
		if($r)
		{
			while($row=mysql_fetch_assoc($r))
				$explain[]=$row;
		}
		return $explain;
	}
	//=================================================//

	//Записать значение в базу
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $table
	 * @param unknown_type $field
	 * @param unknown_type $value
	 * @param unknown_type $record_id
	 * @param unknown_type $type
	 * @return unknown
	 */
	function setValue($table, $field, $value, $record_id, $type=false, $key_field='id')
	{
		return $this->query("UPDATE `#__".mysql_real_escape_string($table)."`
			SET `".mysql_real_escape_string($field)."` = ".$this->processValue($value, $type)."
			WHERE `".$key_field."`='".intval($record_id)."'");
	}
	//=========================================================================

	//Обработка значения перед записью в БД
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $value
	 * @param unknown_type $type
	 * @return unknown
	 */
	function processValue($value, $type=false)
	{
		switch ($type)
		{
			case 'bool':
				if($value)
					return 1;
				else
					return 0;
			case 'int':
				if(mb_strlen($value))
					return intval($value);
				else
					return '(NULL)';

			case 'float':
				if(mb_strlen($value))
					return "'".floatval($value)."'";
				else
					return '(NULL)';
			case 'ip':
				if($value)
					return ip2long($value);
				else
					return '(NULL)';
			break;

			default:
				if(!mb_strlen($value))
					return '(NULL)';
				else
					return "'".mysql_real_escape_string($value)."'";
		}
	}


		//-------------------------------------------------------------------
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $table
	 * @param unknown_type $record_id
	 * @param unknown_type $where
	 */
	function moveUp($table, $record_id, $where='')
	{

		if($where)
			$where = ' AND '.$where;

		$r=$this->query("SELECT `id`, `order_number` FROM `#__".$table."`
							WHERE `id`=".intval($record_id));
		$current=$this->fetchAssoc($r);

		$r=$this->Query("SELECT `id` FROM `#__".$table."`
							WHERE `order_number` < '".$current['order_number']."'
							".$where."
							ORDER BY `order_number` DESC LIMIT 0,1");
		list($prev_id) = $this->fetchArray($r);
		if($prev_id)
		{
			$this->Query("UPDATE `#__".$table."`
							SET `order_number` = ".($current['order_number']-1)."
							WHERE `id` = ".$current['id']." ");

			$this->Query("UPDATE `#__".$table."`
							SET `order_number` = '".$current['order_number']."'
							WHERE `id` = ".$prev_id." ");
		}
	}
	//=========================================================================


	//-----------------------------------------------------------------------
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $table
	 * @param unknown_type $record_id
	 * @param unknown_type $where
	 */
	function moveDown($table, $record_id, $where='')
	{

		if($where)
			$where = ' AND '.$where;

		$r=$this->query("SELECT `id`, `order_number` FROM #__".$table."
							WHERE `id`=".intval($record_id));
		$current=$this->fetchAssoc($r);

		$r=$this->Query("SELECT `id` FROM #__".$table."
							WHERE `order_number` > '".$current['order_number']."' ".$where."
							ORDER BY `order_number` ASC LIMIT 0,1");
		list($next_id) = $this->fetchArray($r);

		if($next_id)
		{
			$this->Query("UPDATE #__".$table."
							SET `order_number` = '".($current['order_number']+1)."'
							WHERE `id` = ".$current['id']."");

			$this->Query("UPDATE #__".$table."
					SET `order_number` = ".$current['order_number']." WHERE `id` = ".$next_id."");
		}
	}
	//==========================================================================


	/**
	 * Enter description here...
	 *
	 * @param unknown_type $table
	 * @param unknown_type $where
	 */
	function orderNumbers($table, $where="")
	{
		if($where)
			$where = " WHERE ".$where;
		$r=$this->Query("SELECT `id` FROM `#__".$table."` ".$where."
							ORDER BY `order_number`");
		$i=1;
		while(list($record_id)=$this->fetchArray($r))
		{
			$this->Query("UPDATE `#__".$table."` SET `order_number` = ".$i."
							WHERE `id`=".$record_id."");
			$i++;
		}
	}
	//===================================================================

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $table
	 * @param unknown_type $where
	 * @param unknown_type $multi_lang
	 * @param unknown_type $key_field
	 * @return unknown
	 */
	function getRecord($table, $where, $multi_lang=false, $key_field='id')
	{
		global $_cfg;
		if(is_numeric($where))
		{
			$record_id=$where;
			$where="`".$key_field."`=".intval($record_id)."";
		}
		else
			$record_id=false;
//	if(sys::isDebugIP()) 
//	{
//		self::printR("SELECT * FROM `#__".$table."`	WHERE ".$where);
//	}
		$r=$this->query("SELECT * FROM `#__".$table."`	WHERE ".$where);
		$record=$this->fetchAssoc($r);

		if($multi_lang && $record_id)
			$record=array_merge($record,$this->getRecordLng($table, $record_id));

		return $record;
	}
	//==============================================================

	function copyRecord($table, $record_id, $key_field='id', $values=false)
	{
		global $_cfg;
		if(!$key_field)
			$key_field='id';
		$record=$this->getRecord($table,$record_id,false,$key_field);
		if(!$record)
			return false;
		$record=array_merge($record,$values);
		$keys='';
		$vals='';
		foreach ($record as $key=>$val)
		{
			$keys.="`".$key."`,";
			$vals.="'".$val."',";
		}
		$keys=sys::cutStrRight($keys,1);
		$vals=sys::cutStrRight($vals,1);

		return $this->query("INSERT INTO `#__".$table."` (".$keys.") VALUES (".$vals.")");

	}

	//-----------------------------------------------------------------
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $table
	 * @param unknown_type $record_id
	 * @return unknown
	 */
	function getRecordLng($table, $record_id)
	{
		global $_langs;
		$record=array();
		$r=$this->query("SELECT * FROM `#__".mysql_real_escape_string($table)."_lng`
				WHERE `record_id`=".intval($record_id)."");
		while($record_lng=$this->fetchAssoc($r))
		{
			if(count($record_lng))
			{
				foreach($record_lng as $key=>$val)
				{
					if($key!='lang_id' && $key!='record_id' && $record_lng['lang_id'] && isset($_langs[$record_lng['lang_id']]))
						$record[$key.'_'.$_langs[$record_lng['lang_id']]['code']]=$val;
				}
			}
		}

		return $record;
	}
	//====================================================================

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $table
	 * @param unknown_type $field
	 * @param unknown_type $record_id
	 * @param unknown_type $multi_lang
	 * @return unknown
	 */
	function getValue($table, $field, $where, $multi_lang=false, $key_field='id')
	{
		global $_cfg;
		if(is_numeric($where))
		{
			$record_id=$where;
			$where="`".$key_field."`=".intval($record_id)."";
		}
		if(!$multi_lang)
		{
			$r=$this->query("SELECT `".mysql_real_escape_string($field)."`
				FROM `#__".mysql_real_escape_string($table)."`
				WHERE ".$where."");
			list($value)=$this->fetchArray($r);
			return $value;
		}
		else
		{
			$r=$this->Query("SELECT `".mysql_real_escape_string($field)."`
			FROM `#__".mysql_real_escape_string($table)."_lng`
			WHERE `record_id`=".intval($record_id)."
			AND `lang_id`=".intval($_cfg['sys::lang_id'])."");
			list($result)=$this->fetchArray($r);
			return $result;
		}
	}


	/**
	 * Enter description here...
	 *
	 * @param unknown_type $table
	 * @param unknown_type $where
	 * @return unknown
	 */
	function getMaxOrderNumber($table,$where="")
	{
		if($where)
			$where=' WHERE '.$where;

		$r=$this->Query("SELECT MAX(`order_number`) FROM #__".mysql_real_escape_string($table)." ".$where);
		list($max_order_number)=$this->fetchArray($r);
		return $max_order_number+1;
	}
	//==============================================================

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $table
	 * @param unknown_type $arr_data
	 * @param unknown_type $arr_params
	 * @param unknown_type $auto_increment
	 * @param unknown_type $key_field
	 * @return unknown
	 */
	function insertArray($table, $arr_data, $arr_params, $arr_extra=false, $auto_increment=true, $key_field='id')
	{
		//Формируем запрос на вставку для немультиязычных полей
		$this->begin();
		$q="INSERT INTO #__".$table." ";
		$field_names=false;
		$values=false;

		foreach($arr_params as $name=>$params)
		{
			if(isset($params['table']) && $params['table']==$table)
			{
				if(!isset($arr_data[$name]))
					$arr_data[$name]=false;
				if(!isset($params['multi_lang']) || !$params['multi_lang'])
				{
					$field_names[] .= '`'.$name.'`';
					$values[] .= "".$this->processValue($arr_data[$name],$params['type'])."";
				}
				else //формируем параметров для мультиязычных данных
					$lng_params[$name]=$params;
			}
		}

		if($arr_extra)
		{
			foreach($arr_extra as $key=>$val)
			{
				$field_names[] .= '`'.$key.'`';
				$values[] .= "'".$val."'";
			}
		}

		$field_names = implode(',',$field_names);
		$values = implode(',',$values);

		if($auto_increment)
			$q .= "(`".$key_field."`, ".$field_names.") VALUES ('0', ".$values.")";
		else
			$q .= "(".$field_names.") VALUES (".$values.")";
//if(sys::isDebugIP(die(self::printR($q))));
		if($this->query($q))
		{
			if(!$auto_increment)
				$new_id=$arr_data[$key_field];
			else
				$new_id=$this->last_id;


			//Если есть мультиязычные поля
			if(isset($lng_params) && count($lng_params))
				$this->insertLng($table,$new_id,$arr_data,$lng_params);

			$this->commit();
			return $new_id;
		}
		$this->rollback();
		return false;

	}
	//=========================================================================

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $table
	 * @param unknown_type $record_id
	 * @param unknown_type $arr_data
	 * @param unknown_type $arr_params
	 * @return unknown
	 */
	function insertLng($table, $record_id, $arr_data, $arr_params)
	{
		global $_langs;
		$this->query("DELETE FROM `#__".mysql_real_escape_string($table)."_lng`
			WHERE `record_id`=".intval($record_id)."");

		foreach($_langs as $lang_id=>$lang)
		{
			$field_names=false;
			$values=false;
			$q="INSERT INTO `#__".$table."_lng`";
			$field_names[] = "`lang_id`";
			$values[] = "'".$lang_id."'";
			$field_names[] = "`record_id`";
			$values[] = "'".$record_id."'";
			foreach($arr_params as $name=>$params)
			{
				if(isset($params['table']) && $params['table']==$table && isset($params['multi_lang']))
				{
					$field_names[] = '`'.$name.'`';
					$values[] = "".$this->processValue($arr_data[$name.'_'.$lang['code']],$params['type'])."";
				}
			}
			$field_names = implode(',',$field_names);
			$values = implode(',',$values);
			$q .= "(".$field_names.") VALUES (".$values.")";
			$result=$this->query($q);
		}
		return $result;
	}
	//====================================================================================

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $table
	 * @param unknown_type $arr_data
	 * @param unknown_type $arr_params
	 * @param unknown_type $where
	 * @param unknown_type $key_field
	 * @return unknown
	 */
	function updateArray($table, $arr_data, $arr_params, $where, $arr_extra=false, $key_field='id')
	{
		$this->begin();
		if(is_numeric($where))
		{
			$record_id=$where;
			$where=" `".$key_field."`=".intval($where)."";
		}

		$q="UPDATE `#__".mysql_real_escape_string($table)."` SET ";
		$fields_number=0;
		foreach($arr_params as $name=>$params)
		{
			if(!isset($arr_data[$name]))
				$arr_data[$name]=false;
			if(isset($params['table']) && $params['table']==$table)
			{
				if(!isset($params['multi_lang']) || !$params['multi_lang'])
				{
					if(isset($params['table']) && $params['table']==$table)
						$q.="`".$name."`= ".$this->processValue($arr_data[$name],$params['type']).", ";
					$fields_number++;
				}
				else
					$lng_params[$name]=$params;
			}
		}

		if($arr_extra)
		{
			foreach ($arr_extra as $key=>$val)
			{
				$q.="`".$key."`= '".$val."', ";
				$fields_number++;
			}
		}
		$q=sys::cutStrRight($q,2);
		$q.=" WHERE ".$where;

		if($fields_number)
			$result=$this->query($q);
		else
			$result=true;

		if($result && isset($lng_params) && is_array($lng_params) && $record_id)
			$result=$this->insertLng($table,$record_id,$arr_data,$lng_params);
		if($result)
		{
			$this->commit();
			return $result;
		}
		$this->rollback();
		return $result;

	}
	//===============================================================================

	/**
	 * Enter description here...
	 *
	 * @param str $table
	 * @param str,int $where
	 * @param bool $multi_lang
	 * @param str $key_field
	 */
	function delete($table, $where, $multi_lang=false, $key_field='id')
	{
		if(is_numeric($where))
		{
			$record_id=$where;
			$where=" `".$key_field."`=".intval($where)." ";
		}

		if($multi_lang && $record_id)
		{
			$this->printR("DELETE FROM `#__".mysql_real_escape_string($table)."_lng`
				WHERE `record_id`=".intval($record_id)."");
			
			$this->query("DELETE FROM `#__".mysql_real_escape_string($table)."_lng`
				WHERE `record_id`=".intval($record_id)."");
		}
		
		$this->query("DELETE FROM `#__".mysql_real_escape_string($table)."`
			WHERE ".$where."");
		
	}

	/**
	 * Enter description here...
	 *
	 * @param string $table
	 * @param string $field
	 * @param string int $where
	 * @param string $extra
	 * @return bool
	 */
	function getValues($table, $field, $where=false, $extra=false)
	{
		if(!$where)
			$where=' 1=1 ';
		$r=$this->query("SELECT `id`, `".$field."` FROM `#__".$table."` WHERE ".$where." ".$extra);
		while($record=$this->fetchAssoc($r))
		{
			$result[$record['id']]=$record[$field];
		}
		return $result;
	}

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $table
	 * @param unknown_type $fields
	 * @param unknown_type $where
	 * @param unknown_type $extra
	 * @return unknown
	 */
	function getRecords($table, $fields, $where=false, $extra=false)
	{
		if(!$where)
			$where=' 1=1 ';
		$r=$this->query("SELECT  ".$fields." FROM `#__".$table."` WHERE ".$where." ".$extra);
		while($record=$this->fetchAssoc($r))
		{
			$result[]=$record;
		}
		return $result;
	}

	function commit()
	{
		mysql_query("COMMIT");
		return true;
	}

	function begin()
	{
		mysql_query("BEGIN");
	}

	function rollback()
	{
		mysql_query("ROLLBACK");
		return false;
	}
}
?>