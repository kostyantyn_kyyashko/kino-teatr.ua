<?php
class sys
{
	// Проверка наличия параметра с заданным значением в конфигурации
	static function isParamInCfgArray($param_name, $param_value)
	{
//		print "<!--";
//		var_dump($param_name);
//		var_dump($param_value);
//		print "-->";
		global $_cfg;
		if(!isset($_cfg[$param_name])) return false;
		if(trim($_cfg[$param_name])=="") return false;
		return in_array($param_value, explode(",",$_cfg[$param_name]));		
	}

	// Функция для отладки кода на рабочем сайте
	static function isDebugIP()
	{
		return self::isParamInCfgArray('sys::debug_ip',$_SERVER['REMOTE_ADDR']);
		global $_cfg;
		if(!isset($_cfg['sys::debug_ip'])) return false;
		if(trim($_cfg['sys::debug_ip'])=="") return false;
		return in_array($_SERVER['REMOTE_ADDR'], explode(",",$_cfg['sys::debug_ip']));
	}
	
	//Отправить заголовки
	static function sendHeaders()
	{
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header("Content-type: text/html; charset=utf-8");
	}
	
	//===========================================================//

	static function parseUri($uri)
	{
		global $_langs;
		$uri=str_replace('.phtml','',$uri);
		$arr_params=explode('?',$uri);
		$query_string=$arr_params[1];
		$arr_params=$arr_params[0];
		$arr_params=explode('/',$arr_params);
		$arr_params=array_reverse($arr_params);
		array_pop($arr_params); //убрать пустой
		if(count($_langs)>1)
			$result['lang']=array_pop($arr_params);
		$result['mod']=array_pop($arr_params);
		$result['act']=array_pop($arr_params);


		$arr_params=array_reverse($arr_params);

		for($i=0; $i<count($arr_params); $i++)
		{
			if($i%2==0)
			{
				$result[$arr_params[$i]]=$arr_params[$i+1];
			}
		}
		if($query_string)
		{
			parse_str($query_string,$query);
			$result=array_merge($result, $query);
		}
		return $result;
	}

	static function strToArr($str)
	{
		return preg_split('//u', $name, -1, PREG_SPLIT_NO_EMPTY);
	}

	//Подключить библиотеку
	static function useLib($mod_lib)
	{
		$mod_lib=explode('::',$mod_lib);
		$mod=$mod_lib[0];
 		if(isset($mod_lib[1]))
		{
			$lib=$mod_lib[1];
			$path=_MODS_DIR.$mod.'/include/lib.'.$lib.'.php';
		}
		else
			$path=_MODS_DIR.$mod.'/include/lib.'.$mod.'.php';
 		require_once($path);
	}
	//=================================================================//

	static function isLang($lang_code)
	{
		global  $_langs;
		foreach ($_langs as $lang)
		{
			if($lang['code']==$lang_code)
				return true;
		}
		return false;
	}

	static function useAct($mod_act)
	{
		global $_db, $_mods;
		$result=404;
		$mod_act=explode('::',$mod_act);
		$mod=basename($mod_act[0]);
		$act=basename($mod_act[1]);

 		if(!in_array($mod,$_mods))
			return 404;
		
		$path=_MODS_DIR.$mod.'/backend/'.$act.'.php';
		if(_MODE=='frontend')
			$path=_MODS_DIR.$mod.'/'.$act.'.php';
			
		if(is_file($path))
		{
//			if(self::isDebugIP()) self::printR($path); 			
			require_once($path);
//			if(self::isDebugIP()) self::printR("\$result=".$mod."_".$act."();"); 			
			eval("\$result=".$mod."_".$act."();");
		}

		return $result;
	}

	static function getHlp($mod_hlp)
	{
		global $_cfg;
		$mod_hlp=explode('::',$mod_hlp);
		$mod=$mod_hlp[0];
		$hlp=$mod_hlp[1];

		$path=_MODS_DIR.$mod.'/hlp/'.$hlp.'.'.$_cfg['sys::lang'].'.html';
		if(is_file($path))
			return file_get_contents($path);
	}

	static function getCache($filename, $period)
	{
		if(self::isDebugIP()) 
		{
			var_dump($filename);
			return false;
		}
		global $_cfg;
		if(!$period)
			return false;
		if(!is_file(_CACHE_DIR.$filename) || $_SERVER['REQUEST_METHOD']=='POST')
		{
			if(is_file(_CACHE_DIR.$filename))
				unlink(_CACHE_DIR.$filename);
			return false;
		}
		$time=filemtime(_CACHE_DIR.$filename);
		if(time()-$time>$period)
			return false;
		if($_cfg['sys::debugger'])
			sys::printR('cache');
		return file_get_contents(_CACHE_DIR.$filename);
	}

	static function setCache($filename,$data)
	{
		//return false;
		file_put_contents(_CACHE_DIR.$filename, $data);
	}

	//Подключить класс
	static function useClass($mod_class)
	{
		$mod_class=explode('::',$mod_class);
		$mod=$mod_class[0];
		if(isset($mod_class[1]))
		{
			$class=$mod_class[1];
			$path=_MODS_DIR.$mod.'/include/cls.'.$class.'.php';
		}
		else
			$path=_MODS_DIR.$mod.'/include/cls.'.$mod.'.php';
		require_once($path);
	}
	//=================================================================//

	static function jsInclude($mod_file)
	{
		global $_js_include;
		$mod_file=explode('::',$mod_file);
		$mod=basename($mod_file[0]);
		$file=basename($mod_file[1]);
		$newJsFile = _MODS_URL.$mod.'/include/js.'.$file.'.js';
		if(is_array($_js_include) && in_array($newJsFile, $_js_include)) return;
		$_js_include[]=$newJsFile;
	}

	static function cssInclude($mod_file)
	{
		global $_css_include;
		$mod_file=explode('::',$mod_file);
		$mod=basename($mod_file[0]);
		$file=basename($mod_file[1]);
		$newCssFile = _MODS_URL.$mod.'/include/css.'.$file.'.css';
		if(is_array($_css_include) && in_array($newCssFile, $_css_include)) return;
		$_css_include[]=$newCssFile;
	}

	//Включить в шаблон страницы java script файл
	static function useJs($mod_file)
	{
		global $_js_header;
		$mod_file=explode('::',$mod_file);
		$mod=basename($mod_file[0]);
		$file=basename($mod_file[1]);

//if(self::isDebugIP()) self::printR(_MODS_DIR.$mod.'/include/js.'.$file.'.php'); // By Mike

		ob_start();
		require_once(_MODS_DIR.$mod.'/include/js.'.$file.'.php');
		$_js_header.=ob_get_clean();
	}

	//Подключить языковой файл
	static function useLang($mod_file)
	{
		global $_cfg, $_lang;
		$mod_file=explode('::',$mod_file);
		$mod=basename($mod_file[0]);
		if(isset($mod_file[1]))
		{
			$file=basename($mod_file[2]);
			$path=_MODS_DIR.$mod.'/lang/lng.'.$_cfg['sys::lang'].'.'.$file.'.ini';
			$path2=_MODS_DIR.$mod.'/lang/lng.'.$_cfg['sys::lang'].'.'.$file.'.'._MODE.'.ini';
		}
		else
		{
			$path=_MODS_DIR.$mod.'/lang/lng.'.$_cfg['sys::lang'].'.ini';
			$path2=_MODS_DIR.$mod.'/lang/lng.'.$_cfg['sys::lang'].'.'._MODE.'.ini';
		}

		if(is_file($path))
		{
			$ini=parse_ini_file($path);
			if(count($ini))
			{
				foreach ($ini as $key=>$val)
				{
					$_lang[$mod.'::'.$key]=$val;
				}
			}
		}

		if(is_file($path2))
		{
			$ini=parse_ini_file($path2);
			if(count($ini))
			{
				foreach ($ini as $key=>$val)
				{
					$_lang[$mod.'::'.$key]=$val;
				}
			}
		}

	}
	//=================================================================//

	//Подклчить контроллер шаблонов
	static function useController($mod_ctr, $var=false)
	{
		global $_cfg, $_user, $_langs, $_err, $_msg, $_cookie, $_first_visit;;
		global $_meta_keywords, $_meta_description, $_meta_other, $_meta_title, $_title;

		if(!$var)
			$var=array();

		$result=false;
		$mod_ctr=explode('::',$mod_ctr);
		$mod=basename($mod_ctr[0]);
		$ctr=basename($mod_ctr[1]);

		if(_MODE=='backend')
			$prefix='/backend';
		else
			$prefix=false;

		$skin_dir=_MODS_DIR.$mod.$prefix.'/skins/'._SKIN.'/';
		$skin_url=_MODS_URL.$mod.$prefix.'/skins/'._SKIN.'/';
		$path=$skin_dir.'ctr.'.$ctr.'.php';

		if(!is_file($path))
			$path=$skin_dir.'ctr.default.php';//Дефолтный контроллер текущего модуля текущего скина

		if(!is_file($path))
		{
			$skin_dir=_MODS_DIR.$mod.$prefix.'/skins/default/';
			$skin_url=_MODS_URL.$mod.$prefix.'/skins/default/';
			$path=$skin_dir.'ctr.'.$ctr.'.php';//Контроллер дефолтного скина текущего модуля
		}

		if(!is_file($path))
			$path=$skin_dir.'ctr.default.php';//Дефолтный контроллер дефолтного скина текущего модуля

		if(!is_file($path))
		{
			if(_MODE=='frontend')
			{
				$skin_dir=_MODS_DIR.'main/skins/'._SKIN.'/';
				$skin_url=_MODS_URL.'main/skins/'._SKIN.'/';
			}
			else
			{
				$skin_dir=_MODS_DIR.'sys/backend/skins/'._SKIN.'/';
				$skin_url=_MODS_URL.'sys/backend/skins/'._SKIN.'/';
			}
			$path=$skin_dir.'ctr.default.php';//Дефолтный системный контроллер текущего скина
		}

		if(!is_file($path))
		{
			if(_MODE=='frontend')
			{
				$skin_dir=_MODS_DIR.'main/skins/default/';
				$skin_url=_MODS_URL.'main/skins/default/';
			}
			else
			{
				$skin_dir=_MODS_DIR.'sys/backend/skins/default/';
				$skin_url=_MODS_URL.'sys/backend/skins/default/';
			}
			$path=$skin_dir.'ctr.default.php';//Дефолтный системный контроллер дефолтного скина
		}

// MikeDump($path, __LINE__, __FILE__,__FUNCTION__); // By Mike
		require($path);
		return $result;
	}
	//=========================================================================//

	static function cutLines($string)
	{
		$string=str_replace("\n",'',$string);
		$string=str_replace("\t",'',$string);
		$string=str_replace("\r",'',$string);
		return $string;
	}

	//Пропарсить шаблон
	static function parseTpl($mod_tpl, $var=false, $cut_lines=false)
	{
		global $_cfg, $_user, $_langs, $_err, $_msg, $_js_include, $_js_header, $_js_footer;
		global $_meta_keywords, $_meta_description, $_meta_other, $_meta_title, $_title;
		global $_cookie, $_first_visit, $_hotkeys, $_root_title, $_var;

		if(!$var)
			$var=array();
		$mod_tpl=explode('::',$mod_tpl);
		$mod=basename($mod_tpl[0]);
		$tpl=basename($mod_tpl[1]);
		
		$act_tpl=(strpos($tpl,'_')===0 && strpos($tpl,'_')!==1);
		
		if(_MODE=='backend')
			$prefix='/backend';
		else
			$prefix=false;

		$skin_dir=_MODS_DIR.$mod.$prefix.'/skins/'._SKIN.'/';
		$skin_url=_MODS_URL.$mod.$prefix.'/skins/'._SKIN.'/';
		$path=$skin_dir.'tpl.'.$tpl.'.php';

		if(!is_file($path) && $act_tpl)
			$path=$skin_dir.'tpl._default.php';

		if(!is_file($path))
		{
			$skin_dir=_MODS_DIR.$mod.$prefix.'/skins/default/';
			$skin_url=_MODS_URL.$mod.$prefix.'/skins/default/';
			$path=$skin_dir.'tpl.'.$tpl.'.php';
		}

		if(!is_file($path) && $act_tpl)
			$path=$skin_dir.'tpl._default.php';

		//Подключить шаблон из бэкэнда (для шаблонов стандартных полей форм, ошибок и т.д.)
		if(!is_file($path))
		{
			$skin_dir=_MODS_DIR.'sys/backend/skins/default/';
			$skin_url=_MODS_URL.'sys/backend/skins/default/';
			$path=$skin_dir.'tpl.'.$tpl.'.php';
		}

		if(!is_file($path) && $act_tpl)
			$path=$skin_dir.'tpl._default.php';
			
//if(self::isDebugIP()) self::printR($path); 
			
		ob_start();
		require($path);
		return ob_get_clean();
	}
	//========================================//

	//Дамп переменной
	static function printR($var)
	{
		ob_start();
		?><pre style="position:relative; text-align:left; color:#000000; font-size:12px; font-family:arial; background:#FFFFDF; z-index:400;" ondblclick="this.style.display='none';" class="printR"><?print_r($var)?></pre><?
		echo ob_get_clean();
	}
	//==========================================================//

	//Запись в файл
	static function writeToFile($file, $string, $mode='a')
	{
		if(empty($file)) return false;
		$fp=fopen($file,$mode);
		flock($fp, 2);
		$result=fwrite($fp,$string);
		flock($fp, 3);
		fclose($fp);
		return $result;
	}
	//======================================================//

	//Перевод
	static function translate($name, $var=false)
	{
		global $_lang;
		if($var && isset($_lang[$name]))
		{
			$result=$_lang[$name];
			foreach($var as $key=>$val)
			{
				$result=str_replace('%'.$key.'%', $val, $result);
			}
		}
		elseif(isset($_lang[$name]))
		{
			$result=$_lang[$name];
		}
		else
			$result=$name;
		return $result;
	}
	//============================================================================//

	//обрезка текста
	static function cutString($string, $maxlen)
	{
	    $len = (mb_strlen($string) > $maxlen)
	        ? mb_strripos(mb_substr($string, 0, $maxlen), ' ')
	        : $maxlen
	    ;
	    $cutStr = mb_substr($string, 0, $len);
	    return (mb_strlen($string) > $maxlen)
	        ? $cutStr . '...'
	        : $cutStr
	    ;
	}
	//==========================================================//

	//руссифицированная дата
	static function russianDateTime($date)
	{
		global $_langs;

		$day = date('d',strtotime($date));
		$month = date('m',strtotime($date));
		$year = date('Y',strtotime($date));
		$time = date('H:i',strtotime($date));

		$mon=self::translate('main::month_'.intval($month).'_a');

		return $day.' '.$mon.' '.$year.' '.$time;
	}

	static function russianDate($date)
	{
		global $_langs;

		$day = date('d',strtotime($date));
		$month = date('m',strtotime($date));
		$year = date('Y',strtotime($date));

		$mon=self::translate('main::month_'.intval($month).'_a');

		return $day.' '.$mon.' '.$year;
	}


	static function russianDate2($date)
	{
		global $_langs;

		$day = date('d',strtotime($date));
		$month = date('m',strtotime($date));
		$year = date('Y',strtotime($date));

		$mon=self::translate('main::month_'.intval($month).'_a');

		return $day.' '.$mon;
	}
	//==========================================================//


	//Дефолтный разборщик _REQUEST_URI
	static function parseRequestUri()
	{

		global $_langs;
		$uri=self::getRootUri();
		$arr_params=explode('/',$uri);
		$arr_params=array_reverse($arr_params);
		if(count($_langs)>1)
			array_pop($arr_params); //убрать язык
		array_pop($arr_params); //убрать модуль
		array_pop($arr_params); //убрать екшен
		array_pop($arr_params); //убрать пустой

		$arr_params=array_reverse($arr_params);

		for($i=0; $i<count($arr_params); $i++)
		{
			if($i%2==0 && !isset($_GET[$arr_params[$i]]))
			{
				$_GET[$arr_params[$i]]=$arr_params[$i+1];
			}
		}



	}
	//==========================================================//

	//Обрезать строку справа
	static function cutStrRight($string, $number)
	{
		return substr($string,0,strlen($string)-$number);
	}
	//========================================================================//

	//Обрежать стрку слева
	static function cutStrLeft($string, $number)
	{
		return substr($string,$number,strlen($string));
	}
	//======================================


	/*Получить _REQUEST_URI в котором корнем будет директория в котром лежит
	скрипт и отсечь суффикс .phtml
	для случая если скрипт лежит не в корневой папке домена.*/
	static function getRootUri()
	{
		$arr_url=parse_url(_ROOT_URL);
		$uri=$_SERVER['REQUEST_URI'];
		if($arr_url['path'] && $arr_url['path']!='/')
			$uri='/'.str_replace($arr_url['path'],'',$uri);
		$uri=explode('.phtml',$uri);
		$uri=$uri[0];
		return $uri;
	}
	//=========================================================//

	//Авторизация пользователя
	static function authorizeUser($login, $password, $encrypted=false)
	{
		global $_db, $_cfg, $_cookie;

		$log_password=$password;//Незашифрованный пароль для логов

		if(!$_cookie)//Если не включены куки
			return 'cookie_is_off';

		if(!$login)
			return 'no_login';

		if(!$password)
			return 'no_password';

		if(!$encrypted)
			$password=md5($password);


		$user=self::getUser(false,$login);//Получить пользователя по логину

		if(!$user)
			return 'user_not_exist';

		//Если не совпал пароль
		if($password!=$user['password'])
		{
			if($_cfg['sys::log_attack'])
				self::writeToFile(_LOGS_DIR.'attack.log',gmdate('Y-m-d H:i:s')."\t".$_SERVER['REMOTE_ADDR']."\t".$login."\t".$log_password."\n");

			if($user['enter_attempts']>=$_cfg['sys::entry_limit'] && $user['on'])//Если счетчик попыток входа превышает максимально допустимое значение и аккаунт юзера не заблокирован
			{
				//Приостановить аккунт пользователя
				$_db->query("UPDATE `#__sys_users`
					SET `on`=0,
					`date_on`='".gmdate('Y-m-d H:i:s',time()+mt_rand(10,$_cfg['sys::max_off_time']))."'
					WHERE `id`=".$user['id']."");
				//==================================//

			}
			else
			{
				//Увеличить счетчик попыток взлома
				$_db->query("UPDATE `#__sys_users`
					SET `enter_attempts`=(`enter_attempts`+1)
					WHERE `id`=".$user['id']."");
				//=======================//
			}
			return 'user_not_exist';
		}

		//Если время блокировки истекло
		if($user['date_on'] && $user['date_on']<gmdate('U') && !$user['on'])
		{
			//разблокировать юзера
			$_db->query("UPDATE `#__sys_users`
				SET `on`=1,
				`date_on`='0000-00-00 00:00:00',
				`enter_attempts`=0
				WHERE `id`='".$user['id']."'");
			//========================================//
			$user['on']=1;
			$user['date_on']=false;;
			$user['enter_attempts']=0;
		}


		//Если учетная временно заблокирована
		if(!$user['on'] && $user['date_on']>gmdate('U'))
			return 'account_temporary_off';


		if(!$user['on'] && $user['confirm_code'])
			return 'account_not_activated';

		if(!$user['on'])
			return 'account_off';

		if($user['only_from_ip'] && ip2long($user['only_from_ip'])!=ip2long($_SERVER['REMOTE_ADDR']))//Если включена блокировка по ip и ip не совпадает
			return 'invalid_user_ip';

		if(_MODE=='backend' && !$user['backend'])
			return 'access_denied';

		self::registerUser($user);

		return true;
	}


	static function authorizeUserSoc($login)
	{
		global $_db, $_cfg, $_cookie;

		if(!$_cookie)//Если не включены куки
			return 'cookie_is_off';

		if(!$login)
			return 'no_login';

		$user=self::getUser(false,$login);//Получить пользователя по логину

		if(!$user)
			return 'user_not_exist';

		//Если время блокировки истекло
		if($user['date_on'] && $user['date_on']<gmdate('U') && !$user['on'])
		{
			//разблокировать юзера
			$_db->query("UPDATE `#__sys_users`
				SET `on`=1,
				`date_on`='0000-00-00 00:00:00',
				`enter_attempts`=0
				WHERE `id`='".$user['id']."'");
			//========================================//
			$user['on']=1;
			$user['date_on']=false;;
			$user['enter_attempts']=0;
		}

		//Если учетная временно заблокирована
		if(!$user['on'] && $user['date_on']>gmdate('U'))
			return 'account_temporary_off';

		if(!$user['on'] && $user['confirm_code'])
			return 'account_not_activated';

		if(!$user['on'])
			return 'account_off';

		if($user['only_from_ip'] && ip2long($user['only_from_ip'])!=ip2long($_SERVER['REMOTE_ADDR']))//Если включена блокировка по ip и ip не совпадает
			return 'invalid_user_ip';

		if(_MODE=='backend' && !$user['backend'])
			return 'access_denied';

		self::registerUser($user);

		return true;
	}
	//======================================//

	//Получить базовую инфу по юзеру
	static function getUser($user_id=false, $user_login=false)
	{
		global $_db, $_cfg;
		if($user_id)
		{
			$where=" `id`=".intval($user_id)." ";
		}
		elseif($user_login)
		{
			$where=" `login`='".mysql_real_escape_string($user_login)."' ";
		}

		//Получить базовую информациию по пользователю
		$r=$_db->query("SELECT * FROM `#__sys_users`
				WHERE ".$where);
		$user=$_db->fetchAssoc($r);

		if($user['date_on'])
			$user['date_on']=self::date2Timestamp($user['date_on'],"%Y-%m-%d %H:%M:%S");

		if($user['date_last_visit'])
			$user['date_last_visit']=self::date2Timestamp($user['date_last_visit'],"%Y-%m-%d %H:%M:%S");

		if($user['date_reg'])
			$user['date_reg']=self::date2Timestamp($user['date_reg'],"%Y-%m-%d %H:%M:%S");
		//=====================================//

		if(!$user)
			return false;

		//Получить список групп в которые входит юзер
		$r=$_db->query("SELECT grps.id, grps.backend
				FROM `#__sys_users_groups` AS `usrs_grps`

				LEFT JOIN `#__sys_groups` AS `grps`
				ON grps.id=usrs_grps.group_id

				WHERE usrs_grps.user_id=".$user['id']."
				ORDER BY grps.order_number
				");

		$user['backend']=false; //Флаг доступа в админ панель
		while($grp=$_db->fetchAssoc($r))
		{
			$user['groups'][]=$grp['id'];
			if($grp['backend'])
				$user['backend']=true;
		}
		//=====================================//

		return $user;
	}
	//===========================================//

	/*Регистрация пользователя в системе
	$user - массив с данными пользователя или id-пользователя*/
	static function registerUser($user)
	{
		global $_cfg, $_db, $_user, $_rights;

		if(!is_array($user))
			$user=self::getUser($user);

		$_SESSION=array(); //очистить сессию
		$_user=false; //очистить данные по пользователю

		//записать данные пользователя в сесссию
		$_SESSION['sys::user_id']=$user['id'];
		$_SESSION['sys::user_password']=$user['password'];
		$_SESSION['sys::user_backend']=$user['backend'];
		$_SESSION['sys::user_ip']=$_SERVER['REMOTE_ADDR'];
		//=========================//

		//Загрузить данные пользователя
		$_user=$user;
		$_user=array_merge($_user,self::getUserData($user['id']));

		//=======================//
		self::loadUserCfg($user['id']);//Загрузить настройки пользователя
		$_rights=self::getUserRights($user['id']);//Загрузить права групп пользователя

		//Записать идентификатор пользователя в таблицу сессий
		$_db->query("UPDATE `#__sys_sess` SET `user_id`=".$user['id']."
						WHERE `id`='".session_id()."'");
		//==========================//

		if($user['id']!=2)
		{
			//Обновить дату последнего захода пользователя и ip-адрес
			$_db->query("UPDATE `#__sys_users` SET `date_last_visit`='".gmdate('Y-m-d H:i:s')."',
						`last_ip`='".ip2long($_SERVER['REMOTE_ADDR'])."'
						WHERE `id`='".$user['id']."'");

			//Записать ip-пользователя в таблицу ip-адресов
			$exists = false;
			$q = "SELECT `user_id` FROM `#__sys_users_ips` WHERE `user_id`='".$user['id']."_".ip2long($_SERVER['REMOTE_ADDR'])."'";  // filter by qnique key !!!
			$r=$_db->query($q);
			if($row=$_db->fetchAssoc($r))
			{
				if($row['user_id'] == $user['id']) $exists = true;
			}
			
			if(!$exists) 
				$_db->query("INSERT INTO `#__sys_users_ips` VALUES (".$user['id'].",".ip2long($_SERVER['REMOTE_ADDR']).")");

			//Записать в журнал регистрацию пользователя
			if($_user['id']!=2 && $_cfg['sys::log_entry'])
				self::writeToFile(_LOGS_DIR.'entry.log',gmdate('Y-m-d H:i:s',time())."\t".$_SERVER['REMOTE_ADDR']."\t".$user['id']."\n");
		}
		//=============================//
	}
	//=========================================================================//


	/*Получить массив с правами пользователя в перменной $user передается либо массив с данными
	о пользователе, либо id пользователя*/
	static function getUserRights($user)
	{
		global $_db, $_cfg;
		$result['parents']=$result['names']=array();

		if(!is_array($user))
			$user=self::getUser($user);
		$groups=implode(',',$user['groups']);
        
		if($groups=="") $groups = "-1"; 
		 
		$q = "SELECT
					acts.parent_id,
					CONCAT(mods.name,'::',parents.name) AS `parent`,
					CONCAT(mods.name,'::',acts.name) AS `name`
			       	FROM `#__sys_groups_rights` AS `rights`

			    	LEFT JOIN `#__sys_mods_actions` AS `acts`
			    	ON acts.id=rights.action_id

			    	LEFT JOIN `#__sys_mods_actions` AS `parents`
			    	ON parents.id=acts.parent_id

			    	LEFT JOIN `#__sys_mods` AS `mods`
			       	ON mods.id=acts.mod_id

					WHERE rights.group_id IN (".$groups.")
					AND acts.mode='"._MODE."'
			       ";
		$r=$_db->query($q);

		while($right=$_db->fetchAssoc($r))
		{
			if($right['parent_id'])
				$result['parents'][$right['name']]=$right['parent'];
			$result['names'][]=$right['name'];
		}
		$result['names']=array_unique($result['names']);
		return $result;
	}
	//==========================================================//

	//Получить дополнительную инфу по юзеру
	static function getUserData($user_id)
	{
		global $_db, $_cfg;
		$result=array();
		$r=$_db->query("SELECT data.*, lng.*
			FROM `#__sys_users_data` AS `data`
			LEFT JOIN `#__sys_users_data_lng` AS `lng`
			ON  lng.record_id=".intval($user_id)."
			AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE data.user_id=".intval($user_id)."");
		$user=$_db->fetchAssoc($r);
		if(is_array($user))
		{
			foreach($user as $key=>$val)
			{
				if($key!='user_id' && $key!='lang_id' && $key!='onj_id')
					$result[$key]=$val;
			}
		}
		unset ($user);
		return $result;
	}
	//=========================================================================//

	//Загрузить настройки пользователя
	static function loadUserCfg($user_id)
	{
		global $_cfg;
		$cfg=self::getUserCfg($user_id);
		if($cfg)
		{
			foreach($cfg as $key=>$val)
			{
				$pos=strpos($key,'_');
				$mod=substr($key,0,$pos);
				$set=substr($key,$pos+1);
				if(mb_strlen($val))
					$_cfg[$mod.'::'.$set]=$val;

			}
			unset($cfg, $pos, $mod, $set);
		}

	}
	//================================================//

	//Получить настойки юзера
	static function getUserCfg($user_id)
	{
		global $_db, $_cfg;
		$result=false;
		$r=$_db->query("SELECT * FROM `#__sys_users_cfg`
			WHERE `user_id`=".intval($user_id)."");
		$cfg=$_db->fetchAssoc($r);
		if(is_array($cfg))
		{
			foreach($cfg as $key=>$val)
			{
				if($key!='user_id' && $key!='id')
					$result[$key]=$val;
			}
		}

		unset($cfg);
		return $result;
	}
	//=========================================================//

	//Проверить текущего юзера
	static function checkSessionUser()
	{
		global $_db, $_user, $_rights;
		if(!isset($_SESSION['sys::user_id']))
			return false;

		$user=self::getUser($_SESSION['sys::user_id']);

		if($user['only_from_ip'] && $user['only_from_ip']!=ip2long($_SERVER['REMOTE_ADDR']))
			return false;

		if(!$user['id'] || !$user['on'] || $user['password']!=$_SESSION['sys::user_password'])
			return false;

		//Загрузить данные пользователя
		$_user=$user;
		$_user=array_merge($_user,self::getUserData($user['id']));
		//==================//

		self::loadUserCfg($user['id']);//Загрузить настройки пользователя

		$_rights=self::getUserRights($user);//Загрузить права группы пользователя

		return true;
	}
	//=============================================//


	//Убрать символы новой строки и табы
	static function stripNewLine($text)
	{
		return str_replace(array("\n", "\r", "\t"), "", $text);
	}
	//===========================================//

	//Задать параметри окна
	static function setWinParams($width, $height,  $left=200, $top=200, $directories='no', $toolbar='no', $location='no', $resizable='yes', $scrollbars='yes', $menubar='no', $dependent='yes')
	{
		return 'width='.$width.',height='.$height.',directories='.$directories.', toolbar='.$toolbar.', location='.$location.', resizable='.$resizable.', scrollbars='.$scrollbars.', menubar='.$menubar.', dependent='.$dependent.', left='.$left.', top='.$top.' ';
	}
	//==========================================================================//

	//Страница перемещена
	static function moved($url, $code="310 Moved Permanently", $pattern="", $replacement="")
	{
		header("HTTP/1.1 $code");
		if($pattern) header("Location: ".preg_replace($pattern,$replacement,$url));
		 else header("Location: ".$url);
		exit();
	}
	//=============================================
	//Перенаправление
	static function redirect($url, $rewrite=true)
	{
		if($rewrite)
			$url=self::rewriteUrl($url);
		header('Location: '.$url);
		exit();
	}
	//=============================================

	//Преобразовать дату в метку времени
	static function date2Timestamp($date, $format=false)
	{
		global $_cfg;
		if(!$format)
			$format=$_cfg['sys::date_time_format'];

		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			$date=date_parse_from_format($date, $format);
		} 
		else {
			$date=strptime($date, $format);
		}
//		$date=strptime($date, $format);


		return mktime($date['tm_hour'],$date['tm_min'],$date['tm_sec'],($date['tm_mon']+1),$date['tm_mday'],($date['tm_year']+1900));
	}
	//========================================================

	//----------------------------------------------------------
	static function date2Db($date, $gmt=false, $format=false)
	{
		if(!$date)
			return false;
		$date=self::date2Timestamp($date,$format);
		if($gmt)
			return gmdate('Y-m-d H:i:s', $date);
		else
			return date('Y-m-d H:i:s', $date);
	}
	//=========================================================

	static function db2Timestamp($date, $format='%Y-%m-%d %H:%M:%S')
	{
		return self::date2Timestamp($date,$format);
	}

	//----------------------------------------------------------
	static function db2Date($date, $gmt=false, $format=false)
	{
		global $_cfg;
		if(!$date || $date=='0000-00-00' || $date=='0000-00-00 00:00:00')
			return false;
		if(!$format)
			$format=$_cfg['sys::date_time_format'];

		$timestamp=self::db2Timestamp($date);

		if($gmt)
			return strftime($format,(strtotime($date)+date('Z',$timestamp)));
		else
			return strftime($format,(strtotime($date)));
	}
	//=========================================================

	//Задать заголовок страницы
	static function setTitle($title)
	{
		global $_title, $_meta_title;
		if($title)
			$_meta_title=$_title=$title.' - '.$_title;
	}
	//====================================

	//Проверить доступ
	static function checkAccess($action=false)
	{
		global $_user;
		global $_rights;

		if($_user['id']==7)
			return true;
		if(!$action)
			return true;

		if(in_array($action, $_rights['names']))
		{
			if(isset($_rights['parents'][$action]))
				return self::checkAccess($_rights['parents'][$action]);
			else
				return true;
		}
		return false;
	}
	//======================================

	//-----------------------------------------------
	static function setTpl($mod_tpl=false)
	{
		global $_tpl;
		$_tpl=$mod_tpl;
	}
	//=======================================================================

	//JavaScript алерт
	static function jsAlert($msg)
	{
		global $_js_footer;
		$msg=str_replace('<br>','\n',$msg);
		$msg=strip_tags($msg);
		$_js_footer.='alert("'.self::stripNewLine($msg).'");';
	}
	//===========================================================================//

	//Установить куку
	static function setCookie($name, $value=false)
	{
		global $_cfg;
		setcookie($name,$value,time()+$_cfg['sys::cookie_days']*_DAY,'/');
	}
	//==========================================


	/*Отдает _QUERY_STRIN построенную из $_GET-параметром, вырезая при этом парамнтры
	указанные в массиве $params или в переменной $params*/
	static function cutGetParams($params)
	{
		$result='?';
		foreach ($_GET as $key=>$val)
		{
			if ($key!='order_by' && $key!='city_id')
			{
				if(is_array($params))
				{
					if(!in_array($key,$params) && $val)
						$result.=$key.'='.$val.'&';
				}
				else
				{
					if($key!=$params && $val)
						$result.=$key.'='.$val.'&';
				}
			}
		}
		$result=self::cutStrRight($result,1);
		return $result;
	}

	static function changeLang($params)
	{


		if (substr($_SERVER['REQUEST_URI'],0,3)=='/uk')
		{
			if (substr($_SERVER['REQUEST_URI'],0,9)=='/uk/main/')
			{
			$url = _ROOT_URL.str_replace('/uk/', 'ru/', $_SERVER['REQUEST_URI']);
			} else {
			$url = _ROOT_URL.substr($_SERVER['REQUEST_URI'],4);
			}
		} else if (substr($_SERVER['REQUEST_URI'],0,3)=='/ru') {
			$url = _ROOT_URL.str_replace('/ru/', 'uk/', $_SERVER['REQUEST_URI']);
		} else {
			$url = _ROOT_URL.'uk'.$_SERVER['REQUEST_URI'];
		}

		return $url;
	}
	//===============================================================

	//на основе $order_by выдать ORDER BY для mysql-запроса
	static function parseOrderBy($order_by=false)
	{
		if(!$order_by)
			return " ORDER BY `id` ASC";
		$order_by=explode('.',$order_by);
		if(!isset($order_by[1]) || ($order_by[1]!='asc' && $order_by[1]!='desc'))
			$order_by[1]='asc';
		return " ORDER BY `".mysql_real_escape_string($order_by[0])."` ".(strtoupper(mysql_real_escape_string($order_by[1])));
	}

	//Перезапись урла
	static function rewriteUrl($url, $lang=false)
	{
		global $_cfg, $_langs;
		if(!$lang)
			$lang=$_cfg['sys::lang'];

		if($_cfg['sys::hurl'])
		{
			$url=self::cutStrLeft($url,1);
			parse_str($url,$arr);

			$query_string='';
			$mod=false;
			$act=false;
			foreach($arr as $key=>$val)
			{
				if(!$val)
					$val='';
				if($key=='mod')
					$mod=$val;
				elseif($key=='act')
					$act=$val;
				elseif($key!='lang')
					$query_string.='/'.$key.'/'.$val;
			}

			if(count($_langs)>1)
				$result_url=_ROOT_URL.$lang.'/'.$mod.'/'.$act.$query_string.'.phtml';
			else
				$result_url=_ROOT_URL.$mod.'/'.$act.$query_string.'.phtml';

			return $result_url;
		}
		else
			return $url;
	}

	static function jsAlertErr($err)
	{
		global $_err;
		$_err=$err;
	}
	//======================================================//

	static function convertFileSize($file_size)
	{
		if($file_size < 1024)
			$result = $file_size."b";
		if($file_size > 1024)
			$result = round($file_size/1024)."Kb";
		if($file_size > 1024*1024)
			$result = round($file_size/1024/1024)."Mb";
		return $result;
	}
	//===================================================================

	static function parseErr($err, $html=false)
	{
		global $_js_footer;
		if(!is_array($err))
			return $err;
		else
		{
			$result='';
			foreach ($err as $key=>$val)
			{
				if($html)
					$result.=$val['title'].': '.$val['text']."<br>";
				else
					$result.=htmlspecialchars($val['title'].': '.$val['text']."\\n");
				$_js_footer.="
					if(document.getElementById('form').elements['".$key."'])
						document.getElementById('form').elements['".$key."'].className='err';
				";
			}
		}
		return $result;
	}
	//======================================================================//

	static function uploadFile($file,$upload_dir,$upload_name=false,$rewrite=true)
	{
		if(!isset($file['name']))
			return false;

		$path_info=pathInfo($file['name']);

		if($upload_name && isset($path_info['extension']) && $path_info['extension'])
			$upload_name=$upload_name.'.'.$path_info['extension'];
		elseif(!$upload_name)
			$upload_name=$path_info['basename'];
		$upload_name = strtolower($upload_name);	// Inserted By Mike 
		if(!$rewrite)
		{
			$i=0;
			$path_info=pathinfo($upload_name);
			while(is_file($upload_dir.$upload_name))
			{
				$i++;
				$upload_name=str_replace('.'.$path_info['extension'],'',$path_info['basename']).'_'.$i.'.'.$path_info['extension'];
			}
		}

		$i=0;

		if(move_uploaded_file($file['tmp_name'],$upload_dir.$upload_name))
		{
			chmod($upload_dir.$upload_name, 0664);
			return $upload_name;
		}

		return false;
	}
	//===============================================================================

	static function copyFile($from_path,$to_path=false, $rewrite=true)
	{
		$path_info=pathinfo($from_path);
		$to_name=$path_info['basename'];
		if(!$to_path)
			$to_path=$from_path;
		if(!$rewrite)
		{
			$i=0;
			while(is_file($path_info['dirname'].'/'.$to_name))
			{
				$i++;
				$to_name=str_replace('.'.$path_info['extension'],'',$path_info['basename']).'_'.$i.'.'.$path_info['extension'];
			}
		}

		$to_path=$path_info['dirname'].'/'.$to_name;
		if(copy($from_path,$to_path))
		{
			chmod($to_path, 0664);
			return $to_name;
		}

		return false;
	}
	//===============================================================================
// by Mike inserted cutting param
	static function resizeImage($from_path, $to_path=false, $to_width=100, $to_height=100, $cutting=false)
	{
		global $_cfg;
		if(!is_file($from_path))
			return false;
			
		$image_info = getImageSize($from_path);
		$from_width = $image_info[0];
		$from_height = $image_info[1];
		$is_vertical = $from_height>$from_width;

		$format=exif_imagetype($from_path);
		if($format==IMAGETYPE_GIF)
			$format='gif';
		elseif($format==IMAGETYPE_JPEG)
			$format='jpeg';
		elseif($format==IMAGETYPE_PNG)
			$format='png';
		else
			return false;

		if(!$to_path)
			$to_path=$from_path;

		if($from_width<=$to_width && $from_height<=$to_height)
		{
			if($from_path!=$to_path)
			{
				copy($from_path, $to_path);
			}
			chmod($to_path, 0664);
			return true;
		}


		switch($format)
		{
			case 'jpeg':
				$from_image=imagecreatefromjpeg($from_path);
			break;

			case 'gif':
				$from_image=imagecreatefromgif($from_path);
			break;

			case 'png':
				$from_image=imagecreatefrompng($from_path);
			break;

			default:
				return false;
		}

// Inserted by Mike	... 
		if($cutting)
		{
			if('png'==$format)
				$to_image = imageCreate($to_width, $to_height);
			else
				$to_image = imageCreateTrueColor($to_width, $to_height);
							
			$ratio = intval(min($from_height/$to_height, $from_width/$to_width));
			$src_w = $to_width * $ratio;						
			$src_h = $to_height * $ratio;						
			$src_x = intval(($from_width-$src_w)/2);
			$src_y = $is_vertical?0:intval(($from_height-$src_h)/2);

			imageCopyResampled($to_image, $from_image, 0, 0, $src_x, $src_y, $to_width, $to_height, $src_w, $src_h);
		}	// inserted by Mike end	
		else 
		{
			$ratio = max($from_height/$to_height, $from_width/$to_width);
			$to_width = $from_width/$ratio; 
			$to_height = $from_height/$ratio;
	
			if('png'==$format)
				$to_image = imageCreate($to_width, $to_height);
			else
				$to_image = imageCreateTrueColor($to_width, $to_height);
			//$white = imageColorAllocate ($to_image, 255, 255, 255);
			//imageFill($to_image, 0, 0, $white);
			//imageColorTransparent ($to_image ,$white);
			imageCopyResampled($to_image, $from_image, 0, 0, 0, 0, $to_width, $to_height, $from_width, $from_height);
		}
		
		switch($format)
		{
			case 'jpeg':
				imagejpeg($to_image, $to_path,$_cfg['sys::images_quality']);
			break;

			case 'gif':
				imagegif($to_image, $to_path);
			break;

			case 'png':
				imagepng($to_image, $to_path);
			break;

			default:
				return false;
		}
		chmod($to_path, 0664);
		unset($from_image, $to_image);
		return true;
	}


	static function setValues($record=false, $default=false)
	{
		if($record)
			$values=$record;
		if($_SERVER['REQUEST_METHOD']=='POST')
		{
			if(isset($_POST['_files']) && is_array($_POST['_files']))
			{
				foreach ($_POST['_files'] as $key=>$val)
				{
					$_POST[$key]=$val;
				}
			}
			$values=$_POST;
		}
		if(!isset($values))
			$values=$default;

		return $values;
	}

	static function scanDir($dir)
	{
		$files=scandir($dir);
		$result=array();
		foreach ($files as $file)
		{
			if($file!='.' && $file!='..')
				$result[]=$file;
		}
		return $result;
	}

	static function filterGet($name, $type='text', $default=false)
	{

		switch ($type)
		{
			case 'int':
				if(!isset($_GET[$name]) || !mb_strlen($_GET[$name]))
					$_GET[$name]=intval($default);
				else
					$_GET[$name]=intval($_GET[$name]);
			break;

			default:
				if(!isset($_GET[$name]) || !mb_strlen($_GET[$name]))
					$_GET[$name]=$default;
			break;
		}
	}

	static function downloadFile($file_path, $speed=false, $send_errors=false)
	{
		global $_cfg;
		if(!$speed)
			$speed=$_cfg['sys::download_speed'];

		ob_end_clean();
		if(file_exists($file_path) && $fp=fopen($file_path,'rb'))
		{
			$running_time = 0;
			$begin_time = time();

			$file_size=filesize($file_path);
			$file_date = date("D, d M Y H:i:s T",filemtime($file_path));

			if(preg_match("/bytes=(\d+)-/", $_SERVER["HTTP_RANGE"], $range))
			{
				header('HTTP/1.1 206 Partial Content');
				$data_start = intval($range[1]);
			}
			else
			{
				header("HTTP/1.1 200 OK");
				$data_start = 0;
			}

			$data_end = $file_size - 1;
			$etag = md5($file_path.$file_size.$file_date);
			fseek($fp, $data_start);
			header('Content-Disposition: attachment; filename='.basename($file_path));
			header('Last-Modified: '.$file_date);
			header("ETag: \"".$etag."\"");
			header('Content-Length: '.($file_size-$data_start));
			header('Content-Range: bytes '.$data_start.'-'.$data_end.'/'.$file_size);
			header('Content-type: application/octet-stream');

			while(!feof($fp) && (connection_status()==0))
	 		{
	 			print fread($fp,$speed);
				flush();
	 			sleep(1);
	 			$running_time = time() - $begin_time;
				if($running_time>240)
	 			{
	 				set_time_limit(300);
					$begin_time = time();
				}

			}

			fclose ($fp);
			return true;
		}
		else
		{
			if($send_errors)
				header('HTTP/1.0 404 Not Found');
			return false;
		}
	}
	//==============================================================

	static function checkPublicPath($path)
	{
		if((strpos($path,_PUBLIC_DIR)!==0 && strpos($path,_PRIVATE_DIR)!==0) || strpos($path,'/..')!==false || strpos($path,'/.')!==false)
			return false;
		return true;
	}

	static function deleteDir($dir)
	{
		$dh=opendir($dir);
		while($item=readdir($dh))
		{
			if($item!='..' && $item!='.')
			{
				if(is_dir($dir.$item))
					self::deleteDir($dir.$item.'/');
				else
					unlink($dir.$item);
			}
		}
		closedir($dh);
		rmdir($dir);
	}


	static function parseModTpl($mod_tpl, $type='text', $var=false, $lang_id=false)
	{
		global $_db, $_cfg;

		if(!$lang_id)
			$lang_id=$_cfg['sys::lang_id'];

		$mod_tpl=explode('::',$mod_tpl);
		$result=array();
		$q = "SELECT
				lng.title,
				lng.subject,
				lng.meta_title,
				lng.meta_description,
				lng.meta_keywords,
				lng.meta_other,
				lng.text
			FROM
				`#__sys_mods_templates` AS `tpl`

			LEFT JOIN
				`#__sys_mods` AS `mods`
			ON
				mods.id=tpl.mod_id

			LEFT JOIN
				`#__sys_mods_templates_lng` AS `lng`
			ON
				lng.record_id=tpl.id AND lng.lang_id=".intval($lang_id)."
			WHERE
				mods.name='".mysql_real_escape_string($mod_tpl[0])."'
			AND
				tpl.name='".mysql_real_escape_string($mod_tpl[1])."'
			AND
				tpl.type='".mysql_real_escape_string($type)."'
			";
		$r=$_db->query($q);

		//if(sys::isDebugIP()) $_db->printR($q);

		$result=$_db->fetchAssoc($r);
		if($var && is_array($var))
		{
			foreach($var as $key=>$val)
			{
				$result['subject']=str_replace('%'.$key.'%',$val,$result['subject']);
				$result['title']=str_replace('%'.$key.'%',$val,$result['title']);
				$result['meta_title']=str_replace('%'.$key.'%',$val,$result['meta_title']);
				$result['meta_keywords']=str_replace('%'.$key.'%',$val,$result['meta_keywords']);
				$result['meta_description']=str_replace('%'.$key.'%',$val,$result['meta_description']);
				$result['meta_other']=str_replace('%'.$key.'%',$val,$result['meta_other']);
				$result['text']=str_replace('%'.$key.'%',$val,$result['text']);
			}
		}


		if($type=='text' || $type=='html')
			return $result['text'];
		else
			return $result;
	}

	static function addMail($from_email=false, $from_name=false, $to_email, $to_name, $subject, $text=false, $html=false, $att_files=false, $charset=false, $date=false, $method='sendMail')
	{
		global $_db, $_cfg;
		if(!$charset)
			$charset=$_cfg['sys::mail_charset'];
		if(!$from_email)
			$from_email=$_cfg['sys::from_email'];
		if(!$from_name)
			$from_name=$_cfg['sys::from'];
		if(!$to_email)
			return false;
		if(!$to_name)
			return false;
		if(!$date)
			$date=gmdate('Y-m-d H:i:s');
		
		$id=uniqid();
		$q = "
			INSERT INTO `#__sys_letters`
			(
				`id`,
				`from_email`,
				`from_name`,
				`to_email`,
				`to_name`,
				`subject`,
				`text`,
				`html`,
				`att_files`,
				`charset`,
                `method`,
				`date`
			)
			VALUES
			(
				'".$id."',
				'".mysql_real_escape_string($from_email)."',
				'".mysql_real_escape_string($from_name)."',
				'".mysql_real_escape_string($to_email)."',
				'".mysql_real_escape_string($to_name)."',
				'".mysql_real_escape_string($subject)."',
				'".mysql_real_escape_string($text)."',
				'".mysql_real_escape_string($html)."',
				'".mysql_real_escape_string($att_files)."',
				'".mysql_real_escape_string($charset)."',
				'".mysql_real_escape_string($method)."',
				'".mysql_real_escape_string($date)."'
			)
		";
//		if(self::isDebugIP()) $_db->printR($q);
		while(!$_db->query($q))
		{
			$id=uniqid(false, true);
		}
		return $id;
	}

	static function sendCommentMail($from_email=false, $from_name=false, $to_email, $to_name, $subject, $text=false, $html=false, $att_files=false, $charset=false)
	{
		global $_cfg;
		if(!$charset)
			$charset=$_cfg['sys::mail_charset'];
		if(!$from_email)
			$from_email=$_cfg['sys::from_email'];
		if(!$from_name)
			$from_name=$_cfg['sys::from'];
		if(!$to_email)
			return false;
		if(!$to_name)
			return false;


		if($html)
		{
			$var['main']=$html;
			$html=sys::parseTpl('main::__email',$var);
		}

		$from_name=mb_convert_encoding($from_name,$charset);
		$to_name=mb_convert_encoding($to_name,$charset);
		$subject=mb_convert_encoding($subject,$charset);

		if($html)
			$html=mb_convert_encoding($html,$charset);

		$boundary='------=_NextPart_000_0000_01C692DD.A0BC23B0';

		$headers  = "MIME-Version: 1.0\n";
		$headers .= "From: ".$from_name." <".$from_email.">\n";
		$headers .= "To: ".$to_name." <".$to_email.">\n";
		$headers .= "Reply-To: ".$from_name." <".$from_email.">\n";
		$headers .= "X-Priority: 3\n";
		$headers .= "X-MSMail-Priority: Normal\n";
		$headers .= "X-Mailer: PHP\n";
		$headers .= "Content-Type: multipart/mixed;\n\tboundary=\"".$boundary."\"\n\n";

		if($html)
		{
			$msg.= "\n\n--".$boundary."\n";
			$msg.="Content-Type: text/html; charset=".$charset."\n";
			$msg.="Content-Transfer-Encoding: base64\n\n";
			$msg.=chunk_split(base64_encode($html));
		}


		$msg.='--'.$boundary.'--';


		return mail($to_email, $subject,$msg, $headers);
	}

	static function sendContestMail($from_email=false, $from_name=false, $to_email, $to_name, $subject, $text=false, $html=false, $att_files=false, $charset=false)
	{
		global $_cfg;
		if(!$charset)
			$charset=$_cfg['sys::mail_charset'];
		if(!$from_email)
			$from_email=$_cfg['sys::from_email'];
		if(!$from_name)
			$from_name=$_cfg['sys::from'];
		if(!$to_email)
			return false;
		if(!$to_name)
			return false;


		if($html)
		{
			$var['main']=$html;
			$html=sys::parseTpl('main::__email',$var);
		}

		$from_name=mb_convert_encoding($from_name,$charset);
		$to_name=mb_convert_encoding($to_name,$charset);
		$subject=mb_convert_encoding($subject,$charset);

		if($html)
			$html=mb_convert_encoding($html,$charset);

		$boundary='------=_NextPart_000_0000_01C692DD.A0BC23B0';

		$headers  = "MIME-Version: 1.0\n";
		$headers .= "From: ".$from_name." <".$from_email.">\n";
		$headers .= "To: ".$to_name." <".$to_email.">\n";
		$headers .= "Cc: ".$from_name." <".$_cfg['sys::from_email'].">\n";
		$headers .= "Reply-To: ".$from_name." <".$from_email.">\n";
		$headers .= "X-Priority: 3\n";
		$headers .= "X-MSMail-Priority: Normal\n";
		$headers .= "X-Mailer: PHP\n";
		$headers .= "Content-Type: multipart/mixed;\n\tboundary=\"".$boundary."\"\n\n";

		if($html)
		{
			$msg.= "\n\n--".$boundary."\n";
			$msg.="Content-Type: text/html; charset=".$charset."\n";
			$msg.="Content-Transfer-Encoding: base64\n\n";
			$msg.=chunk_split(base64_encode($html));
		}

		$msg.='--'.$boundary.'--'; 
		return mail($to_email, $subject,$msg, $headers);
	}


	static function sendMail($from_email=false, $from_name=false, $to_email, $to_name, $subject, $text=false, $html=false, $att_files=false, $charset=false)
	{
		global $_cfg;
		if(!$charset)
			$charset=$_cfg['sys::mail_charset'];
		if(!$from_email)
			$from_email=$_cfg['sys::from_email'];
		if(!$from_name)
			$from_name=$_cfg['sys::from'];
		if(!$to_email)
			return false;
		if(!$to_name)
			return false;

		if($html)
		{
			$var['main']=$html;
			$html=sys::parseTpl('main::__email',$var);
		}

		$from_name=mb_convert_encoding($from_name,$charset);
		$to_name=mb_convert_encoding($to_name,$charset);
		$subject=mb_convert_encoding($subject,$charset);

		if($text)
			$text=mb_convert_encoding($text,$charset);
		if($html)
			$html=mb_convert_encoding($html,$charset);

		$boundary='------=_NextPart_000_0000_01C692DD.A0BC23B0';

		$headers  = "MIME-Version: 1.0\n";
		$headers .= "From: ".$from_name." <".$from_email.">\n";
		$headers .= "To: ".$to_name." <".$to_email.">\n";
		$headers .= "Reply-To: ".$from_name." <".$from_email.">\n";
		$headers .= "X-Priority: 3\n";
		$headers .= "X-MSMail-Priority: Normal\n";
		$headers .= "X-Mailer: PHP\n";
		$headers .= "Content-Type: multipart/mixed;\n\tboundary=\"".$boundary."\"\n\n";

		$msg="This message is in MIME format. Since your mail reader does not understand this format, some or all of this message may not be legible.";

		if($html)
		{
			$msg.= "\n\n--".$boundary."\n";
			$msg.="Content-Type: text/html; charset=".$charset."\n";
			$msg.="Content-Transfer-Encoding: base64\n\n";
			$msg.=chunk_split(base64_encode($html));
		}

		if($text)
		{
			$msg.= "\n\n--".$boundary."\n";
			$msg.="Content-Type: text/plain; charset=".$charset."\n";
			$msg.="Content-Transfer-Encoding: base64\n\n";
			$msg.=chunk_split(base64_encode($text));
		}

		if($att_files)
		{
			foreach($att_files as $file)
			{
				$file_name=basename($file);
				$msg .= "\n\n--".$boundary."\n";
				$msg .= "Content-Type: application/octetstream;\n\tname=\"image:".$file_name."\"\n";
				$msg .= "Content-Transfer-Encoding: base64\n";
				$msg .= "Content-Disposition: attachment;\n\tfilename=\"".$file_name."\"\n";
				$msg .= "\n".chunk_split(base64_encode(file_get_contents($file)));
				$msg .= "\n\n";
			}
		}

		$msg.='--'.$boundary.'--';

		return mail($to_email, $subject,$msg, $headers);
	}
    
    static function sendMailObject($from_email=false, $from_name=false, $to_email, $to_name, $subject, $text=false, $html=false, $att_files=false, $charset=false) {
        
        $html = '<a href="'._ROOT_URL.'"><img src="cid:'.md5('logo').'" /></a><br /><br />'.$html;
        
        $mail = new PHPMailer();
        $mail->From = $from_email;
        $mail->FromName = $from_name;
        $mail->AddAddress($to_email, $to_name);
//  $mail->AddAddress("happyinvestor@mail.ru", "Dbg"); // by Mike 
        $mail->Subject = $subject;
        $mail->MsgHTML($html);
        $mail->CharSet = $charset ? $charset : 'UTF-8';
        
        $att_files = unserialize($att_files);
        reset($att_files);
        $i=1;
        foreach ($att_files as $file) {
            //$mail->AddEmbeddedImageUrl($file);  // comment by Mike

        	// cut name from ($file = <img src="cid:e2fed397e403debbabbafa4727b142d9" />)
			$s = $file;
			$s = split(":",$s); 
			$s = $s[1]?split("\"",$s[1]):"";
			$s = $s[0]?$s[0]:"";
        	// now $s is "e2fed397e403debbabbafa4727b142d9";
            // inserted by Mike
		    $dir = dirname(dirname(__file__)).DIRECTORY_SEPARATOR.'phpmailer'.DIRECTORY_SEPARATOR.'attachments';
		    $f = $dir.DIRECTORY_SEPARATOR.$s;
		    $mail->AddEmbeddedImage($f, $s, ($i++).".jpg");
        }
        $mail->AddEmbeddedImage(dirname(dirname(__file__)).DIRECTORY_SEPARATOR.'phpmailer'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'logo.png', md5('logo'), 'logo.png');
        
        return $mail->Send();
        
    }

	static function sendSystemMail($subject, $text, $html=false, $att_files, $charset)
	{
		global $_db;
		$r=$_db->query("SELECT
			grp.id FROM `#__sys_groups` AS `grp`
			LEFT JOIN `#__sys_groups_rights` AS `rights`
			ON rights.group_id=grp.id
			WHERE rights.action_id=39
		");
		while(list($group_id)=$_db->fetchArray($r))
		{
			$groups[]=$group_id;
		}

		$r=$_db->query("SELECT usr.login, usr.email FROM `#__sys_users` AS `usr`
			LEFT JOIN `#__sys_users_groups` AS `grp`
			ON grp.user_id=usr.id
			WHERE grp.group_id IN(".implode(',',$groups).")");
		while ($user=$_db->fetchAssoc($r))
		{
			self::sendMail(false,false,$user['email'],$user['login'],$subject,$text,$html,$att_files,$charset);
		}
	}

	static function sendMailTpl($user_id, $mod_tpl, $var=false)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT
				`login`,
				`email`,
				`lang_id`
			FROM
				`#__sys_users`
			WHERE `id`=".intval($user_id)."
		");
		$user=$_db->fetchAssoc($r);
		$mail=self::parseModTpl($mod_tpl,'email',$var,$user['lang_id']);
		self::sendMail(false, false,$user['email'],$user['login'],$mail['subject'],false,$mail['text']);
	}

	static function addMailTpl($user_id, $mod_tpl, $var=false)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT
				`login`,
				`email`,
				`lang_id`
			FROM
				`#__sys_users`
			WHERE `id`=".intval($user_id)."
		");
		$user=$_db->fetchAssoc($r);
//if(self::isDebugIP()) self::printR($user);		
		$mail=self::parseModTpl($mod_tpl,'email',$var,$user['lang_id']);
//if(self::isDebugIP()) self::printR($mail);		
		$result = self::addMail(false, false,$user['email'],$user['login'],$mail['subject'],false,$mail['text'],false,'utf-8',false,'sendMailObject');
//if(self::isDebugIP()) self::printR($result);		
	}

	static function setMeta($meta)
	{
		global $_meta_description,$_meta_keywords, $_meta_other, $_meta_title, $_title;
		if($meta['meta_title'])
			$_meta_title=$meta['meta_title'].' - '.$_title ;
		if($meta['meta_keywords'])
			$_meta_keywords=$meta['meta_keywords'];
		if($meta['meta_description'])
			$_meta_description=$meta['meta_description'];
		if($meta['meta_other'])
			$_meta_other=$meta['meta_other'];

	}

	static function setDescription($meta_description)
	{
		global $_meta_description;
		if($meta_description)
			$_meta_description=$meta_description;
	}

	static function setKeywords($meta_keywords)
	{
		global $_meta_keywords;
		if(is_array($meta_keywords))
			$meta_keywords=implode(',',$meta_keywords);
		if($meta_keywords)
			$_meta_keywords=$meta_keywords;

	}

	static function watermark($img_path, $wtm_path, $trans=100, $position='center', $percent=70)
	{
		$format=pathinfo($img_path);
		$format=$format['extension'];
		if(strtolower($format)=='jpg')
			$format='jpeg';
		eval("\$img=imagecreatefrom".$format."(\$img_path);");

		$wtm=imagecreatefrompng($wtm_path);

		$img_width=imagesx($img);
		$img_height=imagesy($img);

		$wtm_width=imagesx($wtm);
		$wtm_height=imagesy($wtm);

		$tmp_height=intval($img_height/100*$percent);
		$tmp_width=intval($img_width/100*$percent);

		$ratio = max($wtm_height/$tmp_height, $wtm_width/$tmp_width);

		$tmp_width = $wtm_width/$ratio;
		$tmp_height = $wtm_height/$ratio;

		$tmp = imagecreatetruecolor($tmp_width, $tmp_height);

	    $white = imageColorAllocate ($tmp, 255, 255, 255);
		imageFill($tmp, 0, 0, $white);
		imageColorTransparent($tmp ,$white);

		if($wtm_width>$tmp_width || $wtm_height>$tmp_height)
			imagecopyresized($tmp,$wtm,0,0,0,0,$tmp_width,$tmp_height,$wtm_width,$wtm_height);
		else
		{
			$tmp_width=$wtm_width;
			$tmp_height=$wtm_height;
			$tmp=$wtm;
		}

		switch ($position)
		{
			case 'center':
				$x=($img_width-$tmp_width)/2;
				$y=($img_height-$tmp_height)/2;
			break;

			case 'left_top':
				$x=10;
				$y=10;
			break;

			case 'right_top':
				$x=$img_width-$tmp_width-10;
				$y=10;
			break;

			case 'left_bottom':
				$x=10;
				$y=$img_height-$tmp_height-10;
			break;

			case 'right_bottom':
				$x=$img_width-$tmp_width-10;
				$y=$img_height-$tmp_height-10;
			break;
			default:
				$x=10;
				$y=10;
		}

		//Наложить временную картинку на оригинальную
		imagecopymerge($img,$tmp,$x,$y,0,0,$tmp_width, $tmp_height,$trans);
		if($format=='jpeg')
			imagejpeg($img,$img_path,100);
		else
			eval("image".$format."(\$img, \$img_path);");
	}

	static function setJsFooter($code)
	{
		global $_js_footer;
		$_js_footer.=$code;
	}

	static function parseBBCode($text)
	{
		require_once(_3PARY_DIR.'xbb/bbcode.lib.php');
		$bb = new bbcode($text);
		return $bb->get_html();
	}

	static function stripBBCode($text)
	{
		$text=str_replace('[B]','',$text);
		$text=str_replace('[/B]','',$text);
		$text=str_replace('[U]','',$text);
		$text=str_replace('[/U]','',$text);
		$text=str_replace('[I]','',$text);
		$text=str_replace('[/I]','',$text);
		$text=str_replace('[LEFT]','',$text);
		$text=str_replace('[/LEFT]','',$text);
		$text=str_replace('[CENTER]','',$text);
		$text=str_replace('[/CENTER]','',$text);
		$text=str_replace('[RIGHT]','',$text);
		$text=str_replace('[/RIGHT]','',$text);
		$text=str_replace('[QUOTE]','',$text);
		$text=str_replace('[/QUOTE]','',$text);
		$text=preg_replace( "#\[URL=(.+?)\](.+?)\[\/URL\]#is", "", $text);
		$text=preg_replace( "#\[URL=(.+?)\](.+?)\[\/URL\]#is", "", $text);
		$text=preg_replace( "#\[IMAGE ALIGN=(.+?)\](.+?)\[\/IMAGE]#is", "", $text);
		$text=preg_replace( "#\[IMAGE\](.+?)\[\/IMAGE]#is", "", $text);
		return $text;
	}

	static function generatePass($chars=8)
	{
		$digit = array(0,1,2,3,4,5,6,7,8,9);
		$halp = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','Y','Z');
		$lalp = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','y','z');
		$pasw ='';

		for ($i= 0; $i<$chars; $i++)
		{
			$r = mt_rand(0,9);
			if($r<3)
				$pasw .= $digit[mt_rand(0,9)];
			elseif($r>=3 && $r<=6)
				$pasw .= $halp[mt_rand(0,24)];
			else
				$pasw .= $lalp[mt_rand(0,24)];
		}
		return $pasw;
	}

	static function markKeyword($text, $keyword, $num_of_chars=255)
	{
		$pos=mb_strpos(mb_strtolower($text),mb_strtolower($keyword));
		$begin=$pos-$num_of_chars;
		if($begin<=0)
			$begin=0;
		$len=$num_of_chars*2+mb_strlen($keyword);
		return preg_replace("/(\s".$keyword."\s)/iu",'<b>\1</b>',mb_substr($text,$begin,$len));

	}

	static function markKeywords($text, $keywords, $num_of_chars=255)
	{
		$arr_keywords=explode(' ',$keywords);
		if(mb_strpos($text,$keywords))
			$keyword=$keywords;
		else
			$keyword=$arr_keywords[0];

		$pos=mb_strpos(mb_strtolower($text),mb_strtolower($keyword));
		$begin=$pos-$num_of_chars;
		if($begin<=0)
			$begin=0;
		$len=$num_of_chars*2+mb_strlen($keyword);
		$result=preg_replace("/(".$keyword.")/iu",'<b>\1</b>',mb_substr($text,$begin,$len));

		foreach ($arr_keywords as $word)
		{
			$result=preg_replace("/(".$word.")/iu",'<b>\1</b>',$result);
		}
		return $result;
	}

	static function loadLangVars($lang_id)
	{
		global $_db, $_cfg;
		//Загрузить переменные языка
		$r=$_db->query("SELECT * FROM `#__sys_langs`
							WHERE `id`='".intval($lang_id)."'");
		$lang=$_db->fetchAssoc($r);
		$_cfg['sys::lang_id']=$lang['id'];
		$_cfg['sys::from']=$lang['robot_title'];
		$_cfg['sys::mail_charset']=$lang['mail_charset'];

		eval("setlocale (LC_TIME, ".$lang['locale'].");");
		eval("setlocale (LC_CTYPE, ".$lang['locale'].");");
		eval("setlocale (LC_MONETARY, ".$lang['locale'].");");
		eval("setlocale (LC_TIME, ".$lang['locale'].");");

		setDateFormat();
		unset($lang);
		//============================================================================//
	}

	static function loadEnvVars($user_id)
	{
		global $_db, $_cfg, $_user, $_rights;
		$_user=self::getUser($user_id);
		$_user=array_merge($_user,self::getUserData($user_id));
		self::loadUserCfg($user_id);
		$_rights=self::getUserRights($user_id);//Загрузить права групп пользователя

		//Загрузить переменные языка
		$r=$_db->query("SELECT * FROM `#__sys_langs`
							WHERE `id`='".$_user['lang_id']."'");
		$lang=$_db->fetchAssoc($r);
		$_cfg['sys::lang_id']=$lang['id'];
		$_cfg['sys::lang']=$lang['code'];
		$_cfg['sys::from']=$lang['robot_title'];
		$_cfg['sys::mail_charset']=$lang['mail_charset'];

		eval("setlocale (LC_TIME, ".$lang['locale'].");");
		eval("setlocale (LC_CTYPE, ".$lang['locale'].");");
		eval("setlocale (LC_MONETARY, ".$lang['locale'].");");
		eval("setlocale (LC_TIME, ".$lang['locale'].");");

		setDateFormat();
		date_default_timezone_set($_cfg['sys::timezone']);//Задать часовой пояс
	}
    
    
    static function isDelivery() {
        
        global $_cfg, $_user;
        
        $status = false;
        if ($_cfg['sys::delivery_debug']) {
            if (in_array($_user['id'], explode(',', $_cfg['sys::delivery_debug_users']))) {
                $status = true;
            }
        } else {
            $status = true;
        }
        
        return $status;
        
    }

    static function attentionUserInfo() {
        global $_user;
        $output = '';
        $status = false;
        if ($_user['id'] != 2) {
            if (!isset($_COOKIE['sys::attention_user_info'])) {
                $status = true;
                setcookie('sys::attention_user_info', 1, null, '/');
            }
        }
        if ($status) {
            if (!@$_user['main_nick_name'] || !@$_user['main_first_name'] || !@$_user['main_last_name'] || !@$_user['main_sex'] || !@$_user['main_birth_date']) {
                $output = "<script>$(function(){
                    if (confirm('Настоятельно рекомендуем заполнить ваши данные на странице профиля!')) {
                        window.location.href = '/ru/main/user_profile.phtml';
                    };
                })</script>";
            }
        }
        return $output;
    }

	static function translitIt($str)
	{
	    $tr = array(
	        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
	        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
	        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
	        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
	        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
	        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
	        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	        "the"=>"","the "=>""," the"=>""," the "=>"-",
	        "The"=>"","The "=>""," The"=>""," The "=>"-",
	        "... "=> "", " - "=> "-"," "=> "-", "."=> "", "/"=> "-"
	    );
	    return strtolower(strtr($str,$tr));
//	    return strtr($str,$tr);
	}
	
	static function getHumanUrl($id=0, $str="", $folder="", $lang=false)
	{
		global $_cfg;

		if($id) $id = '-'.$id;
		if(!$lang) $lang=$_cfg['sys::lang'];
		
		if ($lang=='ru') 	$lang="";
		else 				$lang="$lang/";

	    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', self::translitIt($str));

		return _ROOT_URL.$lang.$folder.'/'.$urlstr.$id.'.phtml';
	}
	
}

require_once dirname(dirname(__file__)).DIRECTORY_SEPARATOR.'phpmailer'.DIRECTORY_SEPARATOR.'class.phpmailer.php';

?>