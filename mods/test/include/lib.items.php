<?
class test_items
{
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $item_id
	 */
	static function deleteItem($item_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("SELECT `id` FROM `#__test_items_photos` 
			WHERE `item_id`='".intval($item_id)."'");
		while(list($photo_id)=$_db->fetchArray($r))
		{
			self::deletePhoto($photo_id);
		}
		
		$r=$_db->query("SELECT `id` FROM `#__test_items_files` 
			WHERE `item_id`='".intval($item_id)."'");
		while(list($file_id)=$_db->fetchArray($r))
		{
			self::deleteFile($file_id);
		}
		
		$_db->query("DELETE FROM `#__test_items_sections` 
			WHERE `item_id`='".intval($item_id)."'");
		
		$_db->delete('test_items',intval($item_id),true);
	}
	
	static function deleteFile($file_id)
	{
		global $_db, $_cfg;
		$file=$_db->getValue('test_items_files','file',$file_id);
		if(is_file($_cfg['test::files_dir'].$file))
			unlink($_cfg['test::files_dir'].$file);
		
		$_db->delete('test_items_files',intval($file_id));
	}
	
	static function deleteFileFile($file_id, $file)
	{
		global $_db, $_cfg;
		if(is_file($_cfg['test::files_dir'].$file))
			unlink($_cfg['test::files_dir'].$file);
		
		$_db->setValue('test_items_files','file',false,$file_id);
	}
	
	static function deletePhoto($photo_id)
	{
		global $_db, $_cfg;
		$image=$_db->getValue('test_items_photos','image',$photo_id);
		if(is_file($_cfg['test::images_dir'].$image))
			unlink($_cfg['test::images_dir'].$image);
		if(is_file($_cfg['test::images_dir'].'pre_'.$image))
			unlink($_cfg['test::images_dir'].'pre_'.$image);
		
		$_db->delete('test_items_photos',intval($photo_id));
	}
	
	static function deletePhotoImage($photo_id, $image)
	{
		global $_db, $_cfg;
		if(is_file($_cfg['test::images_dir'].$image))
			unlink($_cfg['test::images_dir'].$image);
		if(is_file($_cfg['test::images_dir'].'pre_'.$image))
			unlink($_cfg['test::images_dir'].'pre_'.$image);
		$_db->setValue('test_items_photos','image',false,$photo_id);
	}
	

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $item_id
	 */
	static function uploadPhotos($item_id)
	{
		global $_db, $_cfg;
		for($i=0; $i<count($_FILES['photos']['name']); $i++)
		{
			$file=array();
			$file['name']=$_FILES['photos']['name'][$i];
			$file['type']=$_FILES['photos']['type'][$i];
			$file['tmp_name']=$_FILES['photos']['tmp_name'][$i];
			$file['error']=$_FILES['photos']['error'][$i];
			$file['size']=$_FILES['photos']['size'][$i];
			if($filename=sys::uploadFile($file,$_cfg['test::images_dir'],false, false))
			{
				sys::resizeImage($_cfg['test::images_dir'].$filename,$_cfg['test::images_dir'].'pre_'.$filename,$_cfg['test::image_width'],$_cfg['test::image_height']);
				$order_number=$_db->getMaxOrderNumber('test_items_photos',"`item_id`=".intval($item_id)."");
				$_db->query("INSERT INTO `#__test_items_photos` 
					(`id`,`item_id`,`image`,`order_number`)
					VALUES
					(0,".intval($item_id).",'".$filename."',".$order_number.")");
			}
		}
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $file
	 * @param unknown_type $photo_id
	 * @param unknown_type $current_image
	 * @return unknown
	 */
	static function uploadPhoto($file,$photo_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
		{
			if(is_file($_cfg['test::images_dir'].$current_image))
				unlink($_cfg['test::images_dir'].$current_image);
			if(is_file($_cfg['test::images_dir'].'pre_'.$current_image))
				unlink($_cfg['test::images_dir'].'pre_'.$current_image);
		}
		if($image=sys::uploadFile($file,$_cfg['test::images_dir'],false, false))
		{
			$_db->setValue('test_items_photos','image',$image,$photo_id);
			sys::resizeImage($_cfg['test::images_dir'].$image,$_cfg['test::images_dir'].'pre_'.$image,$_cfg['test::image_width'],$_cfg['test::image_height']);
		}
		return $image;
	}
	
	
	static function uploadFile($file,$file_id,$current_file=false)
	{
		global $_db, $_cfg;
		if($current_file)
		{
			if(is_file($_cfg['test::files_dir'].$current_file))
				unlink($_cfg['test::files_dir'].$current_file);
		}
		if($file=sys::uploadFile($file,$_cfg['test::files_dir'],false, false))
			$_db->setValue('test_items_files','file',$file,$file_id);
		return $file;
	}
	
	static function uploadFiles($fields, $values)
	{
		global $_db, $_cfg, $_langs;;
		for($i=0; $i<count($_FILES['file']['name']); $i++)
		{
			foreach ($fields as $key=>$params)
			{
				if(isset($params['table']) && $params['table']=='test_items_files')
				{
					if(isset($params['multi_lang']))
					{
						foreach ($_langs as $lang)
						{
							$values[$key.'_'.$lang['code']]=$_POST[$key.'_'.$lang['code']][$i];
						}
					}
					elseif(isset($_POST[$key][$i]))
						$values[$key]=$_POST[$key][$i];
				}
			}
			$file=array();
			$file['name']=$_FILES['file']['name'][$i];
			$file['type']=$_FILES['file']['type'][$i];
			$file['tmp_name']=$_FILES['file']['tmp_name'][$i];
			$file['error']=$_FILES['file']['error'][$i];
			$file['size']=$_FILES['file']['size'][$i];
			
			if($values['file']=sys::uploadFile($file,$_cfg['test::files_dir'],false, false))
			{
				$fields['order_number']['type']='int';
				$fields['order_number']['table']='test_items_files';
				$values['order_number']=$_db->getMaxOrderNumber('test_items_files',"`item_id`=".intval($values['item_id'])."");
				$result=$_db->insertArray('test_items_files',$values,$fields);
			}
		}
		return $result;
	}
	
	
	/**
	 * Enter description here...
	 *
	 * @param int $item_id
	 * @return array
	 */
	static function getItemSectionsIds($item_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `section_id`
			FROM `#__test_items_sections`
			WHERE `item_id`=".intval($item_id)."");
		while(list($section_id)=$_db->fetchArray($r))
		{
			$result[]=$section_id;
		}
		return $result;
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $item_id
	 * @param unknown_type $sections
	 */
	static function saveItemSections($item_id, $sections)
	{
		global $_db;
		$_db->query("DELETE FROM `#__test_items_sections` 
			WHERE `item_id`='".$item_id."'");
		if(count($sections))
		{
			$sections=array_unique($sections);
			foreach ($sections as $section_id=>$name)
			{
				if($item_id)
				{
					$_db->query("INSERT INTO `#__test_items_sections`
						(
							`item_id`, 
							`section_id`, 
							`order_number`
						)
						VALUES
						(
							".intval($item_id).",
							".intval($section_id).",
							".self::getMaxOrderNumber($section_id)."
						)
					");
				}
			}
		}
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $section_id
	 * @return unknown
	 */
	static function getMaxOrderNumber($section_id)
	{
		global $_db;
		$result=0;
		$r=$_db->query("SELECT MAX(`order_number`)
			FROM `#__test_items_sections`
			WHERE `section_id`=".intval($section_id)."
			");
		list($result)=$_db->fetchArray($r);
		$result++;
		return $result;
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $item_id
	 * @param unknown_type $section_id
	 */
	static function moveUp($item_id, $section_id)
	{
		global $_db;
		$r=$_db->query("SELECT `item_id`, `section_id`, `order_number` 
			FROM `#__test_items_sections` 
			WHERE `item_id`=".intval($item_id)." AND `section_id`=".intval($section_id)."");
		$current=$_db->fetchAssoc($r);
		
		$r=$_db->query("SELECT `item_id`, `section_id` 
			FROM `#__test_items_sections` 
			WHERE `order_number` < ".$current['order_number']." 
			ORDER BY `order_number` DESC");
		
		$prev=$_db->fetchAssoc($r);
		
		if($prev)
		{
			$_db->Query("UPDATE `#__test_items_sections` 
							SET `order_number`=".($current['order_number']-1)." 
							WHERE `item_id`=".$current['item_id']." AND `section_id`=".$current['section_id']."");
		
			$_db->Query("UPDATE `#__test_items_sections` 
							SET `order_number`='".$current['order_number']."' 
							WHERE `item_id`=".$prev['item_id']." AND `section_id`=".$prev['section_id']." ");
		}
	}
	
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $item_id
	 * @param unknown_type $section_id
	 */
	static function moveDown($item_id, $section_id)
	{
		global $_db;
		$r=$_db->query("SELECT `item_id`, `section_id`, `order_number` 
			FROM `#__test_items_sections` 
			WHERE `item_id`=".intval($item_id)." AND `section_id`=".intval($section_id)."");
		$current=$_db->fetchAssoc($r);
		
		$r=$_db->query("SELECT `item_id`, `section_id` 
			FROM `#__test_items_sections` 
			WHERE `order_number` > ".$current['order_number']." 
			ORDER BY `order_number` DESC");
		
		$next=$_db->fetchAssoc($r);
		
		if($next)
		{
			$_db->Query("UPDATE `#__test_items_sections` 
				SET `order_number`=".($current['order_number']+1)." 
				WHERE `item_id`=".$current['item_id']." AND `section_id`=".$current['section_id']."");
		
			$_db->Query("UPDATE `#__test_items_sections` 
				SET `order_number`='".$current['order_number']."' 
				WHERE `item_id`=".$next['item_id']." AND `section_id`=".$next['section_id']." ");
		}
	}
	
	static function orderNumbers($section_id)
	{
		global $_db;
		$r=$_db->Query("SELECT `item_id` FROM `#__test_items_sections`
			WHERE `section_id`=".intval($section_id)."
			ORDER BY `order_number`");
		$i=1;
		while(list($item_id)=$_db->fetchArray($r))
		{
			$_db->Query("UPDATE `#__test_items_sections` SET `order_number` = ".$i." 
				WHERE `item_id`=".$item_id." AND `section_id`=".intval($section_id)."");
			$i++;
		}
	} 
	
	static function showItemTabs($item_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('test::info');
		$tabs['info']['url']='?mod=test&act=item_edit&id='.$item_id;
		
		$tabs['photos']['title']=sys::translate('test::photos');
		$tabs['photos']['url']='?mod=test&act=item_photos_table&item_id='.$item_id;
		
		$tabs['files']['title']=sys::translate('test::files');
		$tabs['files']['url']='?mod=test&act=item_files_table&item_id='.$item_id;
		
		return sys_gui::showTabs($tabs, $active_tab);
	}
	
}
?>