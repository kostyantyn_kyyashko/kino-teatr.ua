<?
class test_sections
{
	static function deleteSection($section_id)
	{
		global $_db, $_cfg, $_err;
		$image=$_db->getValue('test_sections','image',$section_id);
		if(is_file($_cfg['test::images_dir'].$image))
			unlink($_cfg['test::images_dir'].$image);
		if(is_file($_cfg['test::images_dir'].'pre_'.$image))
			unlink($_cfg['test::images_dir'].'pre_'.$image);
		
		$r=$_db->query("SELECT `id` FROM `#__test_sections` 
			WHERE `parent_id`=".intval($section_id)."");
		while(list($child_id)=$_db->fetchArray($r))
		{
			self::deleteSection($child_id);
		}
		
		$_db->delete('test_sections',intval($section_id),true);
	}
	
	static function getTreeBranch($parent_id, $field=false)
	{
		global $_db;
		$result=array();
		$r=$_db->query("SELECT `id`, `name` AS `title` 
			FROM `#__test_sections`
			WHERE `parent_id`='".intval($parent_id)."'
			ORDER BY `order_number`
			");
		while($obj=$_db->fetchAssoc($r))
		{	
			if($field)
			{
				$obj['onclick']="window.parent.document.forms['form'].elements['".$_GET['field']."'].value=".$obj['id'].";";
				$obj['onclick'].="window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode.firstChild.innerHTML='".htmlspecialchars($obj['title'])."';";
				$obj['onclick'].="window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode.lastChild.style.display='none';return false;";
			}
			else 
			{
				$obj['onclick']="setMsg('".sys::translate('sys::loading')."','window.parent.fra_right.fra_right'); window.parent.fra_right.fra_right.location='?mod=test&act=sections_table&parent_id=".$obj['id']."';";
				$obj['url']='?mod=test&act=sections';
			}
			//Проверить есть ли у узла потомки
			$r2=$_db->query("SELECT `id`
				FROM `#__test_sections`
				WHERE `parent_id`='".$obj['id']."'
				LIMIT 0,1
			");
			
			list($obj['children'])=$_db->fetchArray($r2);
			
			$obj['children_url']='?mod=test&act=sections_tree&field='.$field.'&parent_id='.$obj['id'];	
			$result[]=$obj;
		}
		return $result;
	}
	
	static function uploadImage($file,$section_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
		{
			if(is_file($_cfg['test::images_dir'].$current_image))
				unlink($_cfg['test::images_dir'].$current_image);
			if(is_file($_cfg['test::images_dir'].'pre_'.$current_image))
				unlink($_cfg['test::images_dir'].'pre_'.$current_image);
		}
		if($image=sys::uploadFile($file,$_cfg['test::images_dir'],'section_'.$section_id))
		{
			$_db->setValue('test_sections','image',$image,$section_id);
			sys::resizeImage($_cfg['test::images_dir'].$image,$_cfg['test::images_dir'].'pre_'.$image,$_cfg['test::image_width'],$_cfg['test::image_height']);
		}
		return $image;
	}
	
	static function deleteImage($section_id, $image)
	{
		global $_db, $_cfg;
		if(is_file($_cfg['test::images_dir'].$image))
			unlink($_cfg['test::images_dir'].$image);
		if(is_file($_cfg['test::images_dir'].'pre_'.$image))
			unlink($_cfg['test::images_dir'].'pre_'.$image);
		$_db->setValue('test_sections','image',false,$section_id);
	}
	
	static function getSectionsNames($arr_ids)
	{
		global $_db;
		$result=array();
		if(!$arr_ids || !count($arr_ids))
			return false;
		for($i=0; $i<count($arr_ids); $i++)
		{
			$arr_ids[$i]=intval($arr_ids[$i]);
		}
		$str_ids=implode(',',$arr_ids);
		$r=$_db->query("SELECT `id`, `name` 
			FROM `#__test_sections` 
			WHERE `id` IN(".$str_ids.")");
		while($obj=$_db->fetchAssoc($r))
		{
			$result[$obj['id']]=$obj['name'];
		}
		return $result;
	}
}
?>