<?
class test_news
{
	static function deleteNews($news_id)
	{
		global $_db;
		$r=$_db->query("SELECT `id` FROM `#__test_news_comments` 
			WHERE `news_id`=".intval($news_id)."");
		while(list($comment_id)=$_db->fetchArray($r))
		{
			self::deleteComment($comment_id);
		}
		
		$_db->delete('test_news',intval($news_id));
	}
	
	static function showNewsTabs($news_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('test::info');
		$tabs['info']['url']='?mod=test&act=news_edit&id='.$news_id;
		
		$tabs['comments']['title']=sys::translate('test::comments');
		$tabs['comments']['url']='?mod=test&act=news_comments_table&news_id='.$news_id;
		
		return sys_gui::showTabs($tabs, $active_tab);
	}
	
	static function deleteComment($comment_id)
	{
		$_db->delete('test_news_comments',intval($comment_id));
	}
}


?>