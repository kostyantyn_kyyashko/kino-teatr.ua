<?
$_cfg['test::images_dir']=_PUBLIC_DIR.'test/images/';
$_cfg['test::images_url']=_PUBLIC_URL.'test/images/';
$_cfg['test::image_width']=150;
$_cfg['test::image_height']=150;
$_cfg['test::image_max_size']=10*_KB;
$_cfg['test::image_max_width']=1024;
$_cfg['test::image_max_height']=768;
$_cfg['test::files_dir']=_PUBLIC_DIR.'test/files/';
$_cfg['test::files_url']=_PUBLIC_URL.'test/files/';
$_cfg['test::file_max_size']=4*_MB;
$_cfg['test::file_formats']=array('zip','pdf','rtf','rar','txt');
$_cfg['test::item_stats']['in_sight']=sys::translate('test::in_sight');
$_cfg['test::item_stats']['not_in_sight']=sys::translate('test::not_in_sight');
$_cfg['test::item_stats']['waiting']=sys::translate('test::waiting');
?>