<?
function test_sections_tree()
{
	sys::setTitle(sys::translate('test::test'));
	sys::useLib('test::sections');
	
	
	sys::filterGet('parent_id','int',0);
	sys::filterGet('field','text');
	
	$tree['objects']=test_sections::getTreeBranch($_GET['parent_id'],$_GET['field']);
	
	if($_GET['parent_id'])
		sys_gui::mergeTree($_GET['parent_id'],$tree['objects']);
	else 
	{
		//Кнпки
		$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
		
		//Дерево
		$tree['root']['title']=sys::translate('sys::root');
		if($_GET['field'])
		{
			$tree['root']['onclick']="window.parent.document.forms['form'].elements['".$_GET['field']."'].value=0;";
			$tree['root']['onclick'].="window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode.firstChild.innerHTML='".htmlspecialchars(sys::translate('sys::root'))."';";
			$tree['root']['onclick'].="window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode.lastChild.style.display='none';return false;";
		}
		else 
		{
			$tree['root']['onclick']="setMsg('".sys::translate('sys::loading')."','window.parent.fra_right.fra_right'); window.parent.fra_right.fra_right.location='?mod=test&act=sections_table&parent_id=0';";
			$tree['root']['url']='?mod=test&act=sections';
		}
		$tree['parent_id']=0;
		
		if(!$_GET['field'])
		{
			$result['panels'][0]['text']=sys::translate('test::sections');
			$result['panels'][0]['class']='title';
			$result['panels'][]['text']=sys_gui::showButtons($buttons);
		}
		$result['main']=sys::parseTpl('sys::tree',$tree);
	}
	if(isset($result))
		return $result;
}
?>