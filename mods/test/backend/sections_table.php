<?
function test_sections_table()
{
	global $_db, $_err;

	sys::useLib('sys::pages');
	sys::useLib('test::sections');
		
	sys::filterGet('order_by','text','order_number.asc');
	sys::filterGet('parent_id','int',0);
		
	$tbl='test_sections';
	$edit_url='?mod=test&act=section_edit';
	$win_params=sys::setWinParams('700','500');
	$add_title=sys::translate('test::add_section');
	$order_where=$where="`parent_id`=".intval($_GET['parent_id'])."";
	$new_parent_id=false;
	if(!$_GET['parent_id'])
	{
		$parent=false;
		$title=sys::translate('test::sections');
	}
	else
	{ 
		$parent=$_db->getRecord($tbl,$_GET['parent_id']);
		$title=$parent['name'].' :: '.sys::translate('test::sections');
	}
	
	$q="SELECT 
			`id`, 
			`name`,
			`public`, 
			`order_number`, 
			`image`  
		FROM `#__".$tbl."`WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
		{
			if($task[1]==$_GET['parent_id'])
				$new_parent_id=$_db->getValue($tbl,'parent_id',$task[1]);
			test_sections::deleteSection($task[1]);
		}
			
		//Сдвиг вверх
		if($task[0]=='moveUp')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveDown($tbl,$task[1],$order_where);
			else
				$_db->moveUp($tbl,$task[1],$order_where);
		}
		
		//Сдвиг вниз
		if($task[0]=='moveDown')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveUp($tbl,$task[1],$order_where);
			else
				$_db->moveDown($tbl,$task[1],$order_where);
		}
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					if($key==$_GET['parent_id'])
						$new_parent_id=$_db->getValue($tbl,'parent_id',$key);
					test_sections::deleteSection($key);
				}
			}
					
		}
		
		//Упорядочение нумерации
		if($_POST['_task']=='orderNumbers')
			$_db->orderNumbers($tbl, $order_where);
		
		//Если удаляетя родительский раздел
		if($new_parent_id)
		{
			sys_gui::reloadWindow('window','?mod=test&act=sections_table&parent_id='.$new_parent_id);
			sys_gui::reloadTreeBranch('window.parent.parent.fra_left',$new_parent_id,test_sections::getTreeBranch($new_parent_id),$new_parent_id);
		}
			
	}
	//================================================================================================//
	
	if(!$new_parent_id)
	{
		//Обновить дерево для выделения текущего узла
		sys_gui::reloadTreeBranch('window.parent.parent.fra_left',$_GET['parent_id'],test_sections::getTreeBranch($_GET['parent_id']),$_GET['parent_id']);
		//Перегрузить фрейм с товарами
		sys_gui::windowLocation('window.parent.fra_bottom','?mod=test&act=items_table&section_id='.$_GET['parent_id']);
	}
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	if($parent)
	{
		$table['records'][0]=$parent;
		$table['records'][0]['name']='..'.$parent['name'];
		$table['params'][0]['name']['bold']=true;
		$table['params'][0]['name']['onclick']="setMsg('".sys::translate('sys::loading')."'); window.location='?mod=test&act=sections_table&parent_id=".$parent['parent_id']."';return false;";
		$table['params'][0]['name']['url']='?mod=test&act=sections_table&parent_id='.$parent['parent_id'];
		$table['params'][0]['order_number']['html_params']='size="8" disabled ';
		$table['params'][0]['move_up']['type']='text';
		$table['params'][0]['move_up']['url']=false;;
		$table['params'][0]['move_down']['type']='text';
	}
	
	while($obj=$_db->fetchAssoc($r))
	{
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['msg']=sys::translate('test::sections_not_found');
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['move_up']['type']='button';
	$table['fields']['move_up']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveUp.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_up']['image']='sys::btn.desc.gif';
	
	$table['fields']['move_down']['type']='button';
	$table['fields']['move_down']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveDown.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_down']['image']='sys::btn.asc.gif';
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;
	$table['fields']['name']['onclick']="setMsg('".sys::translate('sys::loading')."'); window.location='?mod=test&act=sections_table&parent_id=%id%';return false";
	$table['fields']['name']['url']='?mod=test&act=sections_table&parent_id=%id%';
	
	$table['fields']['public']['type']='checkbox';
	$table['fields']['public']['title']=sys::translate('test::pbl');
	$table['fields']['public']['alt']='public';
	$table['fields']['public']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~public~%id%~%public%~float\')"';
	$table['fields']['public']['width']=40;
	
	$table['fields']['order_number']['type']='textbox';
	$table['fields']['order_number']['order']=true;
	$table['fields']['order_number']['html_params']='size="8" onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=set~'.$tbl.'~order_number~%id%~\'+this.value+\'~int\')"';
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&parent_id=".$_GET['parent_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';
	//=======================================================================================================//
	
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle(sys::translate($title));
	
	$result['panels'][0]['text']=sys::translate($title);
	$result['panels'][0]['class']='title';
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;	
}
?>
