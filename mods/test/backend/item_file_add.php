<?
function test_item_file_add()
{
	global $_db, $_cfg, $_err;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('test::items');
	
	sys::filterGet('item_id','int');
	if(!$_GET['item_id'])
		return 404;
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='test_items_files';
	$title=sys::translate('test::upload_files');
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//item_id
	$fields['item_id']['input']='hidden';
	$fields['item_id']['type']='int';
	$fields['item_id']['req']=true;
	$fields['item_id']['table']=$tbl;
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="55"';
	$fields['name']['title']=sys::translate('sys::name');
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="55"';
	$fields['title']['title']=sys::translate('sys::title');
	
	//file
	$fields['file']['input']='filebox';
	$fields['file']['type']='file';
	$fields['file']['req']=true;
	$fields['file']['file::dir']=$_cfg['test::files_dir'];
	$fields['file']['file::formats']=$_cfg['test::file_formats'];
	$fields['file']['file::max_size']=$_cfg['test::file_max_size'];
	$fields['file']['title']=sys::translate('sys::file');
	$fields['file']['table']=$tbl;
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$_POST))
		{
			if(!test_items::uploadFiles($fields,$_POST))
				$_err=sys::translate('sys::db_error').$_db->err;
		}
		if(!$_err)
			sys_gui::afterSave();
		
	}
	//==========================================================================================================//
	
	//Форма-----------------------------------------
	$form['fields']=$fields;
	//============================================
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys::parseTpl('test::item_file_add',$form);
	//============================================================================================================
	return $result;
}
?>