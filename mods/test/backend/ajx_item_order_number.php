<?
function test_ajx_item_order_number()
{
	global $_db, $_err;
	sys::useLib('sys::check');
	if(!isset($_GET['section_id']) || !isset($_GET['item_id']) || !isset($_GET['order_number']) || !$_GET['order_number'])
		return false;
	$err=sys_check::checkInt($_GET['order_number'],true);
	if($err)
		$err=sys::translate('sys::order_number').': '.sys::translate('sys::'.$err);			
	if($err)
	{				
		?>alert("<?=$err?>");<?
		?>hideMsg();<?
		return true;
	}
	
	$_db->query("UPDATE `#__test_items_sections` 
		SET `order_number`=".intval($_GET['order_number'])."
		WHERE `item_id`=".intval($_GET['item_id'])." AND `section_id`=".intval($_GET['section_id'])."");
	?>hideMsg();<?
}
?>