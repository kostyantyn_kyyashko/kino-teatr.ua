<?
function test_item_file_edit()
{
	global $_db, $_err;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('test::items');
	
	sys::filterGet('id','int');
	sys::filterGet('item_id','int',0);
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='test_items_files';
	$default['item_id']=$_GET['item_id'];
	$default['order_number']=$_db->getMaxOrderNumber($tbl,"`item_id`=".intval($_GET['item_id'])."");
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id'],true);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('test::new_file');
	}
	
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//item_id
	$fields['item_id']['input']='hidden';
	$fields['item_id']['type']='int';
	$fields['item_id']['req']=true;
	$fields['item_id']['table']=$tbl;
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="55"';
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="55"';
	
	//file
	$fields['file']['input']='filebox';
	$fields['file']['type']='file';
	$fields['file']['req']=true;
	$fields['file']['file::dir']=$_cfg['test::files_dir'];
	$fields['file']['file::formats']=$_cfg['test::file_formats'];
	$fields['file']['file::max_size']=$_cfg['test::file_max_size'];
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{
			//Загрузить файл	
			if($_FILES['file']['name'])
				$values['file']=test_items::uploadFile($_FILES['file'],$_GET['id'],$values['file']);
			
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='file')
		{
			test_sections::deleteFileFile($_GET['id'],$values['file']);
			$values['file']=false;
		}
	}
	//==================================================================================================
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>