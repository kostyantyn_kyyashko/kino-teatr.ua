<?
function test_news_comments_table()
{
	global $_db, $_err;

	sys::useLib('sys::pages');
	sys::useLib('test::news');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','date.desc');
	sys::filterGet('news_id','int');
		
	if(!$_GET['news_id'])
		return 404;
		
	$tbl='test_news_comments';
	$edit_url='?mod=test&act=news_comment_edit';
	$win_params=sys::setWinParams('600','400');
	$add_title=sys::translate('test::add_comment');
	
	$order_where=$where=" `news_id`=".intval($_GET['news_id'])."";
	$news=$_db->getRecord('test_news',$_GET['news_id']);
	
	if(!$news)
		return 404;
	
	$title=$news['name'].' :: '.sys::translate('test::comments');
	
	$q="SELECT 
			`id`, `date`, `text` 
		FROM `#__".$tbl."` 
		WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			test_news::deleteComment($task[1]);
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					test_news::deleteComment($key);
				}
			}
					
		}
			
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['text']=mb_substr($obj['text'],0,55).'...';
		$obj['date']=sys::db2Date($obj['date'],true);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['text']['type']='text';
	
	$table['fields']['date']['type']='text';
	$table['fields']['date']['order']=true;
	
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	
	$buttons['new']['onclick']="window.open('".$edit_url."&mode=add&news_id=".$_GET['news_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle(sys::translate($title));
	
	$result['panels'][0]['text']=sys::translate($title);
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=test_news::showNewsAdminTabs($_GET['news_id'],'comments');
	$result['panels'][]=$panel;	
	
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;	
}
?>
