<?
function test_config_edit()
{
	global $_err;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::mods');
	
	$fields=sys_mods::getConfigFields('test');
	$values=sys::setValues(sys_mods::getConfigValues('test'));
	
	
	//Проверка и сохранение данных---------------------------------------------
	if(isset($_POST['_task']))
	{
		if($_POST['_task']=='save')
		{
			if(!$_err=sys_check::checkValues($fields,$values))
			{
				sys_mods::updateConfigValues('test',$values,$fields);
			}
		}	
	}
	//============================================================================
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирвание результата----------------------------------------
	sys::setTitle(sys::translate('test::config'));
	$result['panels'][]['text']=sys_gui::showNavPath('test::config');
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//==============================================================
	return $result;
}
?>