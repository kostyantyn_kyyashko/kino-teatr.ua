<?
function test_section_edit()
{
	global $_db, $_cfg, $_err;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('test::sections');
		
	sys::filterGet('id','int');
	sys::filterGet('parent_id','int',0);
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='test_sections';
	$default['public']=1;
	$default['parent_id']=$_GET['parent_id'];
	$default['order_number']=$_db->getMaxOrderNumber($tbl,"`parent_id`=".intval($_GET['parent_id'])."");
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id'],true);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('test::new_section');
	}
	
	$values=sys::setValues($record, $default);
	if($values['parent_id']==$record['id'])
		$values['parent_id']=$record['parent_id'];
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['min_chars']=4;
	$fields['name']['html_params']='style="width:100%" maxlength="55"';
	
	//path_name
	$fields['path_name']['input']='textbox';
	$fields['path_name']['type']='name';
	$fields['path_name']['max_chars']=16;
	$fields['path_name']['min_chars']=6;
	$fields['path_name']['unique']=true;
	$fields['path_name']['unique::where']=$_GET['id'];
	$fields['path_name']['err::not_unique']=sys::translate('test::name_exist');
	$fields['path_name']['table']=$tbl;
	$fields['path_name']['html_params']='style="width:100%" maxlength="55"';
	$fields['path_name']['title']=sys::translate('test::path_name');
	
	//parent_id
	$fields['parent_id']['input']='framebox';
	$fields['parent_id']['type']='int';
	if(!$fields['parent_id']['text']=$_db->getValue('test_sections','name',$values['parent_id']))
		$fields['parent_id']['text']=sys::translate('sys::root');
	$fields['parent_id']['frame']='?mod=test&act=sections_tree';
	$fields['parent_id']['frame::width']=200;
	$fields['parent_id']['frame::height']=200;
	$fields['parent_id']['table']=$tbl;
	$fields['parent_id']['title']=sys::translate('test::parent_section');
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="55"';
	
	//text
	$fields['text']['input']='textarea';
	$fields['text']['type']='html';
	$fields['text']['multi_lang']=true;
	$fields['text']['table']=$tbl;
	$fields['text']['html_params']='style="width:100%; height:50px"';
	$fields['text']['title']=sys::translate('test::text');
	
	//image
	$fields['image']['input']='filebox';
	$fields['image']['type']='file';
	$fields['image']['file::dir']=$_cfg['test::images_dir'];
	$fields['image']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image']['file::max_size']=$_cfg['test::image_max_size'];
	$fields['image']['file::max_width']=$_cfg['test::image_max_width'];
	$fields['image']['file::max_height']=$_cfg['test::image_max_height'];
	$fields['image']['title']=sys::translate('test::image');
	$fields['image']['preview']=true;
	$fields['image']['preview::prefix']='pre_';
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	//public
	$fields['public']['input']='checkbox';
	$fields['public']['type']='bool';
	$fields['public']['table']=$tbl;
	$fields['public']['title']=sys::translate('test::public');
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').' '.$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{
			//Загрузить и отресайзить картинку	
			if($_FILES['image']['name'])
				$values['image']=test_sections::uploadImage($_FILES['image'],$_GET['id'],$values['image']);
			
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			test_sections::deleteImage($_GET['id'],$values['image']);
			$values['image']=false;
		}
	}
	//==================================================================================================
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>