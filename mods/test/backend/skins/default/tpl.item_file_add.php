<?sys::useJs('sys::form');?>
<script type="text/javascript">
var clone_html=false;
function cloneTable()
{
	if(!clone_html)
		clone_html=document.getElementById('clone').innerHTML;
	div = document.createElement("DIV");
	div.innerHTML=clone_html;
	document.getElementById('form').appendChild(div);
}

function removeClone(obj)
{
	if(obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id!='clone')
	{
		div=obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
		div.parentNode.removeChild(div);
	}
}
</script>
<form id="form" enctype="multipart/form-data" onsubmit="return checkForm(this)" method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
	<input type="hidden" name="_task">
	<input type="hidden" name="item_id" value="<?=$_GET['item_id']?>">
	<div id="clone">
		<table class="form">
			<tr>
				<th><nobr><?=$var['fields']['name']['title']?>*:</nobr></th>
				<td><?=sys_form::parseField('name[]',$var['fields']['name'])?></td>
				<td width="20" rowspan="4">
					<nobr>
						<img onclick="cloneTable();" src="<?=sys_gui::getIconSrc('sys::btn.add.gif')?>" alt="<?=sys::translate('sys::add')?>" title="<?=sys::translate('sys::add')?>">
						<img onclick="removeClone(this);" src="<?=sys_gui::getIconSrc('sys::btn.delete.gif')?>" alt="<?=sys::translate('sys::delete')?>" title="<?=sys::translate('sys::add')?>">
					</nobr>
				</td>
			</tr>
			<?foreach ($_langs as $lang):?>
				<tr>
					<th><nobr><?=$var['fields']['title']['title']?> (<?=$lang['title']?>)</nobr></th>
					<td><?=sys_form::parseField('title_'.$lang['code'].'[]',$var['fields']['title'])?></td>
				</tr>
			<?endforeach;?>
			<tr>
				<th><nobr><?=$var['fields']['file']['title']?>*:</nobr></th>
				<td><?=sys_form::parseField('file[]',$var['fields']['file'])?></td>
			</tr>
		</table>
		<hr>
	</div>
</form>

