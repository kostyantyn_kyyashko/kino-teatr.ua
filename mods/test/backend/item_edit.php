<?
function test_item_edit()
{
	global $_db, $_cfg, $_err;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('test::items');
	sys::useLib('test::sections');
	
	sys::filterGet('id','int');
	sys::filterGet('section_id','int');
	sys::filterGet('mode');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='test_items';
	$default['sections'][]=$_GET['section_id'];
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id'],true);
		if(!$record)
			return 404;
		$record['sections']=test_items::getItemSectionsIds($_GET['id']);
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('test::new_item');
	}
	
	$values=sys::setValues($record, $default);
	$values['sections']=test_sections::getSectionsNames($values['sections']);
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="55"';
	
	//path_name
	$fields['path_name']['input']='textbox';
	$fields['path_name']['type']='name';
	$fields['path_name']['max_chars']=16;
	$fields['path_name']['min_chars']=3;
	$fields['path_name']['unique']=true;
	$fields['path_name']['unique::where']=$_GET['id'];
	$fields['path_name']['err::not_unique']=sys::translate('test::name_exist');
	$fields['path_name']['table']=$tbl;
	$fields['path_name']['html_params']='style="width:100%" maxlength="55"';
	$fields['path_name']['title']=sys::translate('test::path_name');
	
	//sections
	$fields['sections']['input']='multisearchbox';
	$fields['sections']['type']='text';
	$fields['sections']['req']=true;
	$fields['sections']['onkeyup']="if(this.value.length>2) ajaxRequest('?mod=test&act=ajx_sections_list&field=sections&keywords='+this.value);";
	$fields['sections']['title']=sys::translate('test::sections');
	
	//photos
	$fields['photos']['input']='multifilebox';
	$fields['photos']['type']='file';
	$fields['photos']['file::dir']=$_cfg['test::images_dir'];
	$fields['photos']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['photos']['file::max_size']=$_cfg['test::image_max_size'];
	$fields['photos']['file::max_width']=$_cfg['test::image_max_width'];
	$fields['photos']['file::max_height']=$_cfg['test::image_max_height'];
	$fields['photos']['title']=sys::translate('test::photos');
	
	//status
	$fields['status']['input']='selectbox';
	$fields['status']['type']='text';
	$fields['status']['values']=$_cfg['test::item_stats'];
	$fields['status']['table']=$tbl;
	$fields['status']['title']=sys::translate('test::status');
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="55"';
	
	//text
	$fields['text']['input']='textarea';
	$fields['text']['type']='html';
	$fields['text']['multi_lang']=true;
	$fields['text']['table']=$tbl;
	$fields['text']['html_params']='style="width:100%; height:50px"';
	$fields['text']['title']=sys::translate('test::text');
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close' || $_POST['_task']=='next')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			test_items::saveItemSections($_GET['id'],$values['sections']);
			if($_FILES['photos']['name'])
				test_items::uploadPhotos($_GET['id']);
			if($_POST['_task']=='next')
				sys_gui::windowLocation('window','?mod=test&act=item_photos_table&mode=add&item_id='.$_GET['id']);
			else
				sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Отмена добавления------------------------------------------------------------------------
	if($_POST['_task']=='cancel')
	{
		if($_GET['id'])
			test_items::deleteItem($_GET['id']);
		sys_gui::windowClose();
	}
	//================================================================================================
	
	//Кнопки---------------------------------------------------------------------------------------------------
	if($_GET['mode']=='add')
	{
		$buttons['next']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='next'; document.forms['form'].submit();} return false;";
		
		$buttons['cancel']['onclick']="setMsg('".sys::translate('sys::canceling')."');document.forms['form'].elements['_task'].value='cancel'; document.forms['form'].submit(); return false;";
	}
	else 
	{
		$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	}
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	
	if($_GET['mode']!='add')
	{
		$panel['class']='blank';
		$panel['text']=test_items::showItemTabs($_GET['id'],'info');
		$result['panels'][]=$panel;	
	}
		
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>