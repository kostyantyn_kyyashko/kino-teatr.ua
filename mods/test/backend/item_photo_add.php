<?
function test_item_photo_add()
{
	global $_db, $_cfg, $_err;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('test::items');
	
	sys::filterGet('item_id','int');
	
	if(!$_GET['item_id'])
		return 404;
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='test_items_photos';
	$title=sys::translate('test::upload_photos');
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//photos
	$fields['photos']['input']='multifilebox';
	$fields['photos']['type']='file';
	$fields['photos']['file::dir']=$_cfg['test::images_dir'];
	$fields['photos']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['photos']['file::max_size']=$_cfg['test::image_max_size'];
	$fields['photos']['file::max_width']=$_cfg['test::image_max_width'];
	$fields['photos']['file::max_height']=$_cfg['test::image_max_height'];
	$fields['photos']['title']=sys::translate('test::photos');
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$_POST))
		{
			if($_FILES['photos']['name'])
				test_items::uploadPhotos($_GET['item_id']);
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>