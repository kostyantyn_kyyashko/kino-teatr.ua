<?
class counters
{
	static function deleteCounter($counter_id)
	{
		global $_db;
		$_db->delete('counters',intval($counter_id));
	}
	
	static function showCounters($tpl="counters::counters")
	{
		global $_db;
		$var['objects']=array();
		$r=$_db->query("SELECT `code` FROM `#__counters` WHERE `on`=1 ORDER BY `order_number`");
		while ($obj=$_db->fetchAssoc($r))
		{
			$var['objects'][]=$obj;
		}
		return sys::parseTpl($tpl, $var);
	}
}
?>