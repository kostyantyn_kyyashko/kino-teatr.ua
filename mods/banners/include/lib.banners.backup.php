<?
class banners
{
	static function deleteBanner($banner_id)
	{
		global $_db;
		$_db->delete('banners',intval($banner_id));
	}

	static function showBanners($place)
	{
		global $_db;
		switch ($place)
		{
			case 'header':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='header' AND `on`=1
					ORDER BY RAND() LIMIT 0,1
				");
				list($result)=$_db->fetchArray($r);
				return $result;
			break;

			case 'left':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='left' AND `on`=1
					ORDER BY RAND() LIMIT 0,1
				");
				list($result)=$_db->fetchArray($r);
				return $result;
			break;

			case 'main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='main' AND `on`=1
					ORDER BY RAND() LIMIT 0,1
				");
				list($result)=$_db->fetchArray($r);
				return $result;
			break;

			case 'right':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='right' AND `on`=1
					ORDER BY RAND() LIMIT 0,1
				");
				list($result)=$_db->fetchArray($r);
				return $result;
			break;

			case 'hidden':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='hidden' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_hidden_1_sec':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_hidden_1_sec' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_hidden_2_sec':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_hidden_2_sec' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_hidden_1_main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_hidden_1_main' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_hidden_2_main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_hidden_2_main' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

				case 'v3_fullscreen_banner_main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_fullscreen_banner_main' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_premium':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_premium' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

						case 'v3_premium2':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_premium2' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

									case 'v3_premium_online':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_premium_online' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_secondbanner':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_secondbanner' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_premium_main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_premium_main' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

						case 'v3_premium_main2':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_premium_main2' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

						case 'branding_images':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='branding_images' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

						case 'branding_links':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='branding_links' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

						case 'v3_top_main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_top_main' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;
			case 'v3_top_secondary':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_top_secondary' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;
		}
	}
	
	static function filterContent($code)
	{
		// ��� ������� ���� �� ������ �� ���������, ������ ������� ���� ������ ���� ������ ����
		// <!--#DIR N1 N2 N3 -->
		// ��� �������� � ������������� ����������� HTML: <!--...--> 
		// DIR - ���������
		// Nx... - ������������ ��������
		// ������: <!--#SHOW OVER_16 ACTION--> - �������� ������ ��� ���������������� ��� ���� ������
		// ������: <!--#HIDE EROTIC MELODRAMA--> - �������� ������ ��� ����� ������� ��� ���������
		// ��� ������� ������������ � ���� ����� ������� ��� �������.
		if(sys::isDebugIP()) 
		{
			$lines = explode("\n", $code);
			$filter = trim($lines[0]);
			unset($lines[0]);
			
			if(strpos($filter,"<!--#")!==0) return $code; // ��� �������
			
			$filter = str_replace(array("<!--#", "-->"), array("",""),$filter); // ������ �����������
			$filter = explode(" ", $filter);  // �������� 
			$function = strtoupper($filter[0]);			  // �������	
			unset($filter[0]);				  // �������� ������ ��������� �������
			if($filter=="")  return $code; // ��� �������
			if(sizeof($filter)==0)  return $code; // ��� ����������
			
			$result = array();
			foreach ($filter as $method)
			{
				$method_name = "method_" . strtolower($method);
				if(method_exists($this, $method_name)) $result[] = $this->$method_name($function);
			}
			$result = array_sum($result);
			return ($result>0)?implode("\n",$lines):"";
		}
		
		return $code;
	}
}



?>