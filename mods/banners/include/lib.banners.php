<?
class banners
{
	static function deleteBanner($banner_id)
	{
		global $_db;
		$_db->delete('banners',intval($banner_id));
	}

	static function showBanners($place)
	{
		global $_db;
		switch ($place)
		{
			case 'header':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='header' AND `on`=1
					ORDER BY RAND() LIMIT 0,1
				");
				list($result)=$_db->fetchArray($r);				
				return self::filterContent($result);
			break;

			case 'left':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='left' AND `on`=1
					ORDER BY RAND() LIMIT 0,1
				");
				list($result)=$_db->fetchArray($r);
				return self::filterContent($result);
			break;

			case 'main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='main' AND `on`=1
					ORDER BY RAND() LIMIT 0,1
				");
				list($result)=$_db->fetchArray($r);
				return self::filterContent($result);
			break;

			case 'right':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='right' AND `on`=1
					ORDER BY RAND() LIMIT 0,1
				");
				list($result)=$_db->fetchArray($r);
				return self::filterContent($result);
			break;

			case 'hidden':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='hidden' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_hidden_1_sec':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_hidden_1_sec' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_hidden_2_sec':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_hidden_2_sec' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_hidden_1_main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_hidden_1_main' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_hidden_2_main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_hidden_2_main' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

				case 'v3_fullscreen_banner_main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_fullscreen_banner_main' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

							case 'v3_fullscreen_banner_secondary':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_fullscreen_banner_secondary' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;
			
			case 'v3_premium':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_premium' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_premium2':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_premium2' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_premium3':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_premium3' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_premium_online':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_premium_online' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_secondbanner':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_secondbanner' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_premium_main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_premium_main' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_premium_main2':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_premium_main2' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;
			
			case 'v3_premium_main3':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_premium_main3' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

						case 'branding_images':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='branding_images' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

						case 'branding_links':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='branding_links' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;
			
					case 'V3_page_down':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='V3_page_down' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

						case 'v3_top_main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_top_main' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

									case 'V3_todayBlock_interface_main':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='V3_todayBlock_interface_main' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;

			case 'v3_top_secondary':
				$r=$_db->query("
					SELECT `code` FROM `#__banners`
					WHERE `place`='v3_top_secondary' AND `on`=1
					ORDER BY `order_number`
				");
				$result='';
				while (list($code)=$_db->fetchArray($r))
				{
					$result.=self::filterContent($code);
				}
				return $result;
			break;
		}
	}
	

	static function filterContent($code)
	{
		global $_db;
		// Для вырезки кода по одному из признаков, первой строкой кода должна быть строка вида
		// <!--#DIR N1 N2 N3 -->
		// Код обрамлен в многострочный комментарии HTML: <!--...--> 
		// DIR - директива
		// Nx... - наименования фильтров
		// Пример: <!--#SHOW OVER_16 ACTION--> - Показать только для совершеннолетних или жанр боевик
		// Пример: <!--#HIDE EROTIC MELODRAMA--> - Спрятать только для жанра эротика или мелодрама
		// Для каждого наименования в этом файле пишется код фильтра.
		
		$lines = explode("\n", $code);
		$filter = trim($lines[0]);
		unset($lines[0]);
		
		if(strpos($filter,"<!--#")!==0) return $code; // нет фильтра
		
		$filter = str_replace(array("<!--#", "-->"), array("",""),$filter); // убрать комментарии
		$filter = explode(" ", $filter);  // Выделить 
		$function = strtolower($filter[0]);			  // Функция	
		unset($filter[0]);				  // Оставить только параметры функции
		if($filter=="")  return $code; // нет фильтра
		if(sizeof($filter)==0)  return $code; // нет параметров
		
		$result = array();
		foreach ($filter as $method)
		{
			$method_name = "__method_" . strtolower($method);				
			if(method_exists('banners', $method_name)) $result[] = self::$method_name();
		}
		$result = array_sum($result);

/*		if(sys::isDebugIP()) 
		{
			$_db->printR("BANNER");
			$_db->printR($_GET);
			$_db->printR("\$result=$result, \$function=$function");
			$_db->printR(" ");
			$_db->printR("\$code pre=");
			$_db->printR(htmlentities($code));
			$_db->printR(" ");
			$_db->printR("\$code after=");
			if(($function=="show") && ($result>0)) $_db->printR("show code: ".htmlentities(implode("\n",$lines)));
			else
			if(($function=="hide") && ($result>0)) $_db->printR("no code (cleared by filter)");
			else	
												   $_db->printR("show default code: ".htmlentities(implode("\n",$lines))); 
		} */
		
		switch ($function)
		{
			case "show":	if($result>0) return implode("\n",$lines);
			case "hide":	if($result>0) return "";
			default: return implode("\n",$lines);
		}	
		
		return $code;
	}
	
// Методы фильтрации	

// DETECT EROTIC
	private function __method_erotic()
	{
		global $_db, $_cfg;
		sys::useLib('main::genres');		
		
//		if(sys::isDebugIP()) $_db->printR($_GET);			
		
		if(isset($_GET['genre_id']) && $_GET['genre_id']) return (doubleval($_GET['genre_id'])==20)?1:0; // erotic genre 20			
/*
Array
(
    [title] => fff
    [genre_id] => 20
    [year] => 2016
    [show] => on
    [lang] => ru
    [mod] => main
    [act] => films
    [uap] => 
    [wop] => 
    [country] => 
    [letter] => %D0%9E
    [city_id] => 1
    [order_by] => title.asc
)
*/

// проверки по рубрикам
		switch($_GET["act"]) {
			case "films" : return self::filter_films_erotic();							
			
			case "gossip" : return self::filter_gossips_erotic("ORDER BY art.date DESC");
			case "popular_gossip" : return self::filter_gossips_erotic("ORDER BY art.today_shows DESC, art.total_shows DESC");
			case "discussed_gossip" : return self::filter_gossips_erotic("ORDER BY art.comments DESC");										
			case "gossip_article" : return (in_array(20, main_genres::getTextGenresIds($_GET["article_id"], "gossip")));
			
			case "news" : return self::filter_news_erotic("ORDER BY art.date DESC");										
			case "popular_news" : return self::filter_news_erotic("ORDER BY art.today_shows DESC, art.total_shows DESC");										
			case "discussed_news" : return self::filter_news_erotic("ORDER BY art.comments DESC");										
			case "news_article" : return (in_array(20, main_genres::getTextGenresIds($_GET["article_id"], "news")));
			
			case "articles" : return self::filter_articles_erotic("ORDER BY art.date DESC");										
			case "popular_articles" : return self::filter_articles_erotic("ORDER BY art.today_shows DESC, art.total_shows DESC");										
			case "discussed_articles" : return self::filter_articles_erotic("ORDER BY art.comments DESC");										
			case "articles_article" : return (in_array(20, main_genres::getTextGenresIds($_GET["article_id"], "articles")));
			
			case "interview" : return self::filter_interviews_erotic("ORDER BY art.date DESC");
			case "popular_interview" : return self::filter_interviews_erotic("ORDER BY art.today_shows DESC, art.total_shows DESC");
			case "discussed_interview" : return self::filter_interviews_erotic("ORDER BY art.comments DESC");
			case "interview_article" : return (in_array(20, main_genres::getTextGenresIds($_GET["article_id"], "interview")));
			
			case "reviews" : return self::filter_reviews_erotic("ORDER BY rev.date DESC");								
			case "popular_reviews" : return self::filter_reviews_erotic("ORDER BY rev.total_shows DESC");
			case "review" : return (in_array(20, main_genres::getTextGenresIds($_GET["review_id"], "reviews")));
			
			default: ;
		} // of switch($_GET["act"])
		
// проверки по ключам		
		if(isset($_GET['film_id'])) $_GET['film_id'] = doubleval(0+$_GET['film_id']);
		
		if(isset($_GET['film_id'])) 
		{
				$r=$_db->query("SELECT count(film_id) FROM `#__main_films_genres` WHERE `film_id`=".doubleval($_GET['film_id'])." AND `genre_id`=20"); // erotic genre 20
				$result=($_db->fetchArray($r));
				if($result[0]>0) return 1; 
		}
		
		if(isset($_GET['poster_id'])) 
		{
			$q = "
				SELECT count(g.film_id) 
					FROM `#__main_films_genres` g
							
				LEFT JOIN `#__main_films_posters` p
					ON p.film_id = g.film_id
							 
				WHERE p.`id`=".doubleval($_GET['poster_id'])." AND g.`genre_id`=20
				";
			$r=$_db->query($q); // erotic genre 20
			$result=($_db->fetchArray($r));
			if($result[0]>0) return 1; 
		}
		
		if(isset($_GET['photo_id'])) 
		{
			$q = "
				SELECT count(g.film_id) 
					FROM `#__main_films_genres` g
							
				LEFT JOIN `#__main_films_photos` p
					ON p.film_id = g.film_id
							 
				WHERE p.`id`=".doubleval($_GET['photo_id'])." AND g.`genre_id`=20
				";
			$r=$_db->query($q); // erotic genre 20
			$result=($_db->fetchArray($r));
			if($result[0]>0) return 1; 
		}
				
		
		return 0;
	}
	
// DETECT MELODRAMA
	private function __method_melodrama()
	{
		return 0;
	}	
	
	private function filter_films_erotic()
	{
		//  страницы фильмов и текстухи обрабатываются индивидуально: возврат 1, если хоть один из найденных - эротика
		//  запрос должен совпадать с запросом на films.php, иначе будет рассинхронизация
		global $_db, $_cfg;
		
		$q="
			SELECT
			 flm.id	
			FROM `#__main_films` AS `flm`
			LEFT JOIN `#__main_films_lng` AS `lng`
			ON lng.record_id=flm.id
			AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
			";		
		
		//Страна
		if($_GET['country'])
		{
			$q.="LEFT JOIN `#__main_films_countries` AS `cnt`
			ON cnt.film_id=flm.id AND cnt.country_id=".intval($_GET['country'])."";
		}
	
		//Город
		if($_GET['city_id'])
		{
			$q.="
			LEFT JOIN `#__main_shows` AS `shw`
			ON shw.film_id=flm.id
			LEFT JOIN `#__main_cinemas_halls` AS `hls`
			ON hls.id=shw.hall_id
			LEFT JOIN `#__main_cinemas` AS `cnm`
			ON cnm.id=hls.cinema_id AND cnm.city_id=".intval($_GET['city_id'])."
			";
		}

		//Условия
		$q.=" WHERE 1=1";
	
		if($_GET['title'])
		{
			$q.=" AND (lng.title LIKE '%".mysql_real_escape_string(urldecode($_GET['title']))."%'
				OR flm.title_orig LIKE '%".mysql_real_escape_string(urldecode($_GET['title']))."%')";
		}
	
		if($_GET['year'])
		{
			$q.=" AND flm.year='".intval($_GET['year'])."'";
		}
	
		if($_GET['uap'])
		{
			$q.=" AND flm.ukraine_premiere='".$uap."'";
		}
	
		if($_GET['wop'])
		{
			$q.=" AND flm.world_premiere='".$wop."'";
		}
	
		if($_GET['letter'])
		{
			$q.=" AND (lng.title LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%'
				OR flm.title_orig LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%')";
		}
	
		if($_GET['country'])
		{
			$q.=" AND cnt.country_id=".intval($_GET['country'])."";
		}
	
		if($_GET['show'])
		{
			$q.=" AND cnm.city_id=".intval($_GET['city_id'])." AND (shw.begin<='".date('Y-m-d')."' AND shw.end>='".date('Y-m-d')."')";
		}

		$q.="
				GROUP BY flm.id
				ORDER BY flm.total_shows DESC
			";
		
		$pages = sys_pages::pocess($q, $_cfg['main::films_on_page']);
		
		$q = "SELECT DISTINCT id FROM (".$q.$pages["limit"].") AS T";
		$result=$_db->query($q); 
		//if(sys::isDebugIP()) $_db->printR($q);
		while($row=$_db->fetchArray($result))
		{
		  $g = main_films::getFilmGenresTitlesIds($row['id']);
		//if(sys::isDebugIP()) { $_db->printR("=="); $_db->printR($row['id']); }

		  foreach ($g as $grec)  {
		    //if(sys::isDebugIP()) $_db->printR($grec);
		    if($grec["id"]==20) return 1;
		  }
		}
		//if(sys::isDebugIP()) $_db->printR($result);
		return 0; 
	}	
	
	private function filter_gossips_erotic($order_by="ORDER BY art.date DESC")
	{
		//  запрос должен совпадать с запросом на gossip.php, иначе будет рассинхронизация
		global $_db, $_cfg;
		
		$q="
			SELECT
				art.id
			FROM `#__main_gossip_articles` AS `art`
	
			LEFT JOIN
				`#__main_gossip_articles_lng` AS `art_lng`
			ON
				art_lng.record_id=art.id
			AND
				art_lng.lang_id=".intval($_cfg['sys::lang_id'])."
	
			LEFT JOIN `#__main_gossip` AS `nws`
			ON art.gossip_id=nws.id
	
			LEFT JOIN `#__main_gossip_articles_spec_themes` AS `spec`
			ON art.id=spec.article_id
			
			WHERE
				(art.city_id=".intval($_cfg['main::city_id'])." OR art.city_id=0)
				AND art.public=1
				AND art.date<'".gmdate('Y-m-d H:i:s')."'
				AND nws.showtab=1
		";
		if($_GET['gossip_id'])
		{
			$q.="
				AND art.gossip_id=".intval($_GET['gossip_id'])."
			";
		}
		if($_REQUEST['tag'] && $_cfg['sys::lang_id']=='1')
		{
			$q.="
				AND (
					art.tags LIKE '%".$_REQUEST['tag']."%'
				OR
					art.tags LIKE '%".$_REQUEST['tag']."'
				OR
					art.tags LIKE '".$_REQUEST['tag']."%'
				)
			";	
		}
	
		if($_REQUEST['tag'] && $_cfg['sys::lang_id']=='3') {
			$q.="
				AND (
					art.tags_ua LIKE '%".$_REQUEST['tag']."%'
				OR
					art.tags_ua LIKE '%".$_REQUEST['tag']."'
				OR
					art.tags_ua LIKE '".$_REQUEST['tag']."%'
				)
			";	
		}
	
		$q.="
			$order_by
		";
		
		$pages = sys_pages::pocess($q, $_cfg['main::gossip_on_page']);
		
		$q = "SELECT DISTINCT id FROM (".$q.$pages["limit"].") AS T";
		$result=$_db->query($q); 
	 //if(sys::isDebugIP()) $_db->printR($q);
		while($row=$_db->fetchArray($result))
		{
		  $genres = main_genres::getTextGenresIds($row['id'], "gossip");
		      //if(sys::isDebugIP()) { $_db->printR("=="); $_db->printR($row['id']); }

		  foreach ($genres as $g)  {
		    //if(sys::isDebugIP()) $_db->printR($g);
		    if($g==20) return 1;
		  }
		}
		//if(sys::isDebugIP()) $_db->printR($result);
		return 0; 
	}	
	

	private function filter_news_erotic($order_by="ORDER BY art.date DESC")
	{
		//  запрос должен совпадать с запросом на news.php, иначе будет рассинхронизация
		global $_db, $_cfg;
		$q="
			SELECT
				art.id
			FROM `#__main_news_articles` AS `art`
	
			LEFT JOIN
				`#__main_news_articles_lng` AS `art_lng`
			ON
				art_lng.record_id=art.id
			AND
				art_lng.lang_id=".intval($_cfg['sys::lang_id'])."
	
			LEFT JOIN `#__main_news` AS `nws`
			ON art.news_id=nws.id
	
			LEFT JOIN `#__main_news_articles_spec_themes` AS `spec`
			ON art.id=spec.article_id
			
			WHERE
				(art.city_id=".intval($_cfg['main::city_id'])." OR art.city_id=0)
				AND art.public=1
				AND art.date<'".gmdate('Y-m-d H:i:s')."'
				AND nws.showtab=1
		";
		if($_GET['news_id'])
		{
			$q.="
				AND art.news_id=".intval($_GET['news_id'])."
			";
		}
	
		if($_REQUEST['tag'] && $_cfg['sys::lang_id']=='1')
		{
			$q.="
				AND (
					art.tags LIKE '%".str_replace("'","",$_REQUEST['tag'])."%'
				OR
					art.tags LIKE '%".str_replace("'","",$_REQUEST['tag'])."'
				OR
					art.tags LIKE '".str_replace("'","",$_REQUEST['tag'])."%'
				)
			";
		}
	
		if($_REQUEST['tag'] && $_cfg['sys::lang_id']=='3') {
			$q.="
				AND (
					art.tags_ua LIKE '%".str_replace("'","",$_REQUEST['tag'])."%'
				OR
					art.tags_ua LIKE '%".str_replace("'","",$_REQUEST['tag'])."'
				OR
					art.tags_ua LIKE '".str_replace("'","",$_REQUEST['tag'])."%'
				)
			";
		}
	
		$q.="
			$order_by
		";
		
		$pages = sys_pages::pocess($q, $_cfg['main::news_on_page']);
		
		$q = "SELECT DISTINCT id FROM (".$q.$pages["limit"].") AS T";
		$result=$_db->query($q); 
	//if(sys::isDebugIP()) $_db->printR($q);
		while($row=$_db->fetchArray($result))
		{
		  $genres = main_genres::getTextGenresIds($row['id'], "news");
		      //if(sys::isDebugIP()) { $_db->printR("=="); $_db->printR($row['id']); }

		  foreach ($genres as $g)  {
		    //if(sys::isDebugIP()) $_db->printR($g);
		    if($g==20) return 1;
		  }
		}
		//if(sys::isDebugIP()) $_db->printR($result);
		return 0;
	}	
	
	private function filter_interviews_erotic($order_by="ORDER BY art.date DESC")
	{
		//  запрос должен совпадать с запросом на interview.php, иначе будет рассинхронизация
		global $_db, $_cfg;
		$q="
			SELECT
				art.id
			FROM `#__main_interview_articles` AS `art`
	
			LEFT JOIN
				`#__main_interview_articles_lng` AS `art_lng`
			ON
				art_lng.record_id=art.id
			AND
				art_lng.lang_id=".intval($_cfg['sys::lang_id'])."
	
			LEFT JOIN `#__main_interview` AS `nws`
			ON art.interview_id=nws.id
	
			LEFT JOIN `#__main_interview_articles_spec_themes` AS `spec`
			ON art.id=spec.article_id
			
			WHERE
				(art.city_id=".intval($_cfg['main::city_id'])." OR art.city_id=0)
				AND art.public=1
				AND art.date<'".gmdate('Y-m-d H:i:s')."'
				AND nws.showtab=1
		";
		if($_GET['interview_id'])
		{
			$q.="
				AND art.interview_id=".intval($_GET['interview_id'])."
			";
		}
	
		if($_REQUEST['tag'] && $_cfg['sys::lang_id']=='1')
		{
			$q.="
				AND (
					art.tags LIKE '%".$_REQUEST['tag']."%'
				OR
					art.tags LIKE '%".$_REQUEST['tag']."'
				OR
					art.tags LIKE '".$_REQUEST['tag']."%'
				)
			";
		}
	
		if($_REQUEST['tag'] && $_cfg['sys::lang_id']=='3') {
			$q.="
				AND (
					art.tags_ua LIKE '%".$_REQUEST['tag']."%'
				OR
					art.tags_ua LIKE '%".$_REQUEST['tag']."'
				OR
					art.tags_ua LIKE '".$_REQUEST['tag']."%'
				)
			";
		}
		$q.="
			$order_by
		";
		
		$pages = sys_pages::pocess($q, $_cfg['main::interview_on_page']);
		
		$q = "SELECT DISTINCT id FROM (".$q.$pages["limit"].") AS T";
		$result=$_db->query($q); 
	//if(sys::isDebugIP()) $_db->printR($q);
		while($row=$_db->fetchArray($result))
		{
		  $genres = main_genres::getTextGenresIds($row['id'], "interview");
		      //if(sys::isDebugIP()) { $_db->printR("=="); $_db->printR($row['id']); }

		  foreach ($genres as $g)  {
		    //if(sys::isDebugIP()) $_db->printR($g);
		    if($g==20) return 1;
		  }
		}
		//if(sys::isDebugIP()) $_db->printR($result);
		return 0;
	}	
	
	private function filter_articles_erotic($order_by="ORDER BY art.date DESC")
	{
		//  запрос должен совпадать с запросом на articles.php, иначе будет рассинхронизация
		global $_db, $_cfg;
		$q="
			SELECT
				art.id
			FROM `#__main_articles_articles` AS `art`
	
			LEFT JOIN
				`#__main_articles_articles_lng` AS `art_lng`
			ON
				art_lng.record_id=art.id
			AND
				art_lng.lang_id=".intval($_cfg['sys::lang_id'])."
	
			LEFT JOIN `#__main_articles` AS `nws`
			ON art.articles_id=nws.id
	
			LEFT JOIN `#__main_articles_articles_spec_themes` AS `spec`
			ON art.id=spec.article_id
			
			WHERE
				/*art.date BETWEEN '".$month_begin."' AND '".$month_end."'
				AND */(art.city_id=".intval($_cfg['main::city_id'])." OR art.city_id=0)
				AND art.public=1
				AND art.date<'".gmdate('Y-m-d H:i:s')."'
				AND nws.showtab=1
		";
		if($_GET['articles_id'])
		{
			$q.="
				AND art.articles_id=".intval($_GET['articles_id'])."
			";
		}
	
		if($_REQUEST['tag'] && $_cfg['sys::lang_id']=='1')
		{
			$q.="
				AND (
					art.tags LIKE '%".$_REQUEST['tag']."%'
				OR
					art.tags LIKE '%".$_REQUEST['tag']."'
				OR
					art.tags LIKE '".$_REQUEST['tag']."%'
				)
			";
		}
	
		if($_REQUEST['tag'] && $_cfg['sys::lang_id']=='3') {
			$q.="
				AND (
					art.tags_ua LIKE '%".$_REQUEST['tag']."%'
				OR
					art.tags_ua LIKE '%".$_REQUEST['tag']."'
				OR
					art.tags_ua LIKE '".$_REQUEST['tag']."%'
				)
			";
		}
	
		$q.="
			$order_by
		";
	
		$pages = sys_pages::pocess($q, $_cfg['main::articles_on_page']);
		
		$q = "SELECT DISTINCT id FROM (".$q.$pages["limit"].") AS T";
		$result=$_db->query($q); 
	//if(sys::isDebugIP()) $_db->printR($q);
		while($row=$_db->fetchArray($result))
		{
		  $genres = main_genres::getTextGenresIds($row['id'], "articles");
		      //if(sys::isDebugIP()) { $_db->printR("=="); $_db->printR($row['id']); }

		  foreach ($genres as $g)  {
		    //if(sys::isDebugIP()) $_db->printR($g);
		    if($g==20) return 1;
		  }
		}
		//if(sys::isDebugIP()) $_db->printR($result);
		return 0;
	}	
	
	private function filter_reviews_erotic($order_by="ORDER BY rev.date DESC")
	{
		//  запрос должен совпадать с запросом на reviews.php, иначе будет рассинхронизация
		global $_db, $_cfg;
		$q="
			SELECT
				rev.id
			FROM
				`#__main_reviews` AS `rev`
	
			LEFT JOIN
				`#__main_films_lng` AS `flm_lng`
			ON
				flm_lng.record_id=rev.film_id
			AND
				flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
	
			LEFT JOIN `#__main_films` AS `flm`
			ON flm.id=rev.film_id
	
			LEFT JOIN
				`#__sys_users` AS `usr`
			ON
				usr.id=rev.user_id
			LEFT JOIN
				`#__sys_users_groups` AS `users`
			ON
				usr.id=users.user_id
			LEFT JOIN `#__main_reviews_articles_spec_themes` AS `spec`
			ON rev.id=spec.article_id
	
			WHERE rev.public=1
			AND rev.date<'".gmdate('Y-m-d H:i:s')."'
			$order_by
		";
	
		$pages = sys_pages::pocess($q, $_cfg['main::reviews_on_page']);
		
		$q = "SELECT DISTINCT id FROM (".$q.$pages["limit"].") AS T";
		$result=$_db->query($q); 
//	if(sys::isDebugIP()) $_db->printR($q);
		while($row=$_db->fetchArray($result))
		{
		  $genres = main_genres::getTextGenresIds($row['id'], "reviews");
//	if(sys::isDebugIP()) { $_db->printR("=="); $_db->printR($row['id']); }

		  foreach ($genres as $g)  {
//	if(sys::isDebugIP()) $_db->printR($g);
		    if($g==20) return 1;
		  }
		}
		return 0;
	}			
	
}	
?>