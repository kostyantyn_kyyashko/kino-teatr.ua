<?
function content_types_table()
{
	if(!sys::checkAccess('content::types'))
		return 403;
		
	global $_db, $_err, $_cfg;
	
	sys::useLib('sys::pages');
	sys::useLib('content::types');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','order_number.asc');
		
	$tbl='content_types';
	$edit_url='?mod=content&act=type_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('content::add_type');
	$order_where=$where=false;
	$nav_point='content::types';
	
	$q="SELECT 
			`id`, 
			`name`,
			`tree`,
			`root`,
			`data`,
			`multi_lang`,
			`on`,
			`order_number`
		FROM `#__".$tbl."`";
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			content_types::deleteType($task[1]);
			
		//Сдвиг вверх
		if($task[0]=='moveUp')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveDown($tbl,$task[1],$order_where);
			else
				$_db->moveUp($tbl,$task[1],$order_where);
		}
		
		//Сдвиг вниз
		if($task[0]=='moveDown')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveUp($tbl,$task[1],$order_where);
			else
				$_db->moveDown($tbl,$task[1],$order_where);
		}
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					content_types::deleteType($key);
				}
			}
					
		}	
		
		//Упорядочение нумерации
		if($_POST['_task']=='orderNumbers')
			$_db->orderNumbers($tbl, $order_where);
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['move_up']['type']='button';
	$table['fields']['move_up']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveUp.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_up']['image']='sys::btn.desc.gif';
	
	$table['fields']['move_down']['type']='button';
	$table['fields']['move_down']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveDown.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_down']['image']='sys::btn.asc.gif';
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;
	
	$table['fields']['root']['type']='checkbox';
	$table['fields']['root']['title']=sys::translate('content::rt_nd');
	$table['fields']['root']['alt']=sys::translate('content::root_node');
	$table['fields']['root']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~root~%id%\')"';
	$table['fields']['root']['width']=40;
	
	$table['fields']['tree']['type']='checkbox';
	$table['fields']['tree']['title']=sys::translate('content::tre');
	$table['fields']['tree']['alt']=sys::translate('content::show_in_tree');
	$table['fields']['tree']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~tree~%id%\')"';
	$table['fields']['tree']['width']=40;
	
	$table['fields']['data']['type']='checkbox';
	$table['fields']['data']['title']=sys::translate('content::dta');
	$table['fields']['data']['alt']=sys::translate('content::data');
	$table['fields']['data']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~data~%id%\')"';
	$table['fields']['data']['width']=40;
	
	$table['fields']['multi_lang']['type']='checkbox';
	$table['fields']['multi_lang']['title']=sys::translate('content::mlng');
	$table['fields']['multi_lang']['alt']=sys::translate('content::multi_lang');
	$table['fields']['multi_lang']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~multi_lang~%id%\')"';
	$table['fields']['multi_lang']['width']=40;
	
	$table['fields']['on']['type']='checkbox';
	$table['fields']['on']['title']=sys::translate('sys::is_on');
	$table['fields']['on']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~on~%id%\')"';
	$table['fields']['on']['width']=40;
	
	$table['fields']['order_number']['type']='textbox';
	$table['fields']['order_number']['order']=true;
	$table['fields']['order_number']['html_params']='size="8" onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=set~'.$tbl.'~order_number~%id%~\'+this.value+\'~int\')"';
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&mode=add','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
