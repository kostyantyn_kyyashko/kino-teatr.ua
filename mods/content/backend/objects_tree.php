<?
function content_objects_tree()
{
	if(!sys::checkAccess('content::content'))
		return 403;
	
	sys::setTitle(sys::translate('content::tree'));
	sys::useLib('content::objects');
	
	
	sys::filterGet('parent_id','int',0);
	sys::filterGet('field','text');
	sys::filterGet('task','text');
	
	$tree['objects']=content_objects::getTreeBranch($_GET['parent_id']);
	
	if($_GET['parent_id'])
		sys_gui::mergeTree($_GET['parent_id'],$tree['objects']);
	else 
	{
		//Кнпки
		$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
		
		//Дерево
		$tree['root']['title']=sys::translate('sys::root');	
		switch($_GET['task'])
		{
			case 'setValue':
				$tree['root']['onclick']="window.parent.document.forms['form'].elements['".$_GET['field']."'].value=0;";
				$tree['root']['onclick'].="getChildByTagName(window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode,'A').innerHTML='".htmlspecialchars(sys::translate('sys::root'))."';";
				$tree['root']['onclick'].="getChildByTagName(window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode,'DIV').style.display='none';return false;";
			break;
			
			case 'move':
				$tree['root']['onclick']="window.parent.document.forms['table'].elements['_task'].value='move.".$_GET['object_id'].".0';";
				$tree['root']['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
					
			break;	
			
			case 'copy':
				$tree['root']['onclick']="window.parent.document.forms['table'].elements['_task'].value='copy.".$_GET['object_id'].".0';";
				$tree['root']['onclick'].="setMsg('".sys::translate('sys::copying')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
			break;
			
			case 'link':
				$tree['root']['onclick']="window.parent.document.forms['table'].elements['_task'].value='link.".$_GET['object_id'].".0';";
				$tree['root']['onclick'].="setMsg('".sys::translate('sys::linking')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
			break;
			
			case 'moveChecked':	
				$tree['root']['onclick']="window.parent.document.forms['table'].elements['_task'].value='moveChecked.0';";
				$tree['root']['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
			break;
			
			case 'copyChecked':	
				$tree['root']['onclick']="window.parent.document.forms['table'].elements['_task'].value='copyChecked.0';";
				$tree['root']['onclick'].="setMsg('".sys::translate('sys::copying')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
			break;
			
			case 'linkChecked':	
				$tree['root']['onclick']="window.parent.document.forms['table'].elements['_task'].value='linkChecked.0';";
				$tree['root']['onclick'].="setMsg('".sys::translate('sys::linking')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
			break;
		
			default:
				$tree['root']['onclick']="setMsg('".sys::translate('sys::loading')."','window.parent.fra_right'); window.parent.fra_right.location='?mod=content&act=objects_table&parent_id=0';";
				$tree['root']['url']='?mod=content&act=objects';
		}
		
		$tree['parent_id']=0;
		
		if(!$_GET['task'])
		{
			$result['panels'][]['text']=sys_gui::showButtons($buttons);
		}
		$result['main']=sys::parseTpl('sys::tree',$tree);
	}
	if(isset($result))
		return $result;
}
?>