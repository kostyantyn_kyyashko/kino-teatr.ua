<?
function content_objects()
{
	if(!sys::checkAccess('content::content'))
		return 403;
		
	$nav_point='content::content';
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	$result['left']['url']='?mod=content&act=objects_tree';
	$result['right']['url']='?mod=content&act=objects_table';
	return $result;
}
?>