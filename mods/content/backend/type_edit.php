<?
function content_type_edit()
{
	if(!sys::checkAccess('content::types'))
		return 403;
		
	global $_db, $_err, $_cfg;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('content::types');
	
	sys::filterGet('id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='content_types';
	$default=array();
	$default['order_number']=$_db->getMaxOrderNumber($tbl);
	$default['on']=true;
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('content::new_type');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='name';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['max_chars']=16;
	$fields['name']['unique']=true;
	$fields['name']['unique::where']=$_GET['id'];
	$fields['name']['html_params']='style="width:100%" maxlength="16"';
	
	//name_multi
	$fields['name_multi']['input']='textbox';
	$fields['name_multi']['type']='name';
	$fields['name_multi']['req']=true;
	$fields['name_multi']['table']=$tbl;
	$fields['name_multi']['max_chars']=16;
	$fields['name_multi']['unique']=true;
	$fields['name_multi']['unique::where']=$_GET['id'];
	$fields['name_multi']['html_params']='style="width:100%" maxlength="16"';
	$fields['name_multi']['title']=sys::translate('content::name_multi');
	
	//root
	$fields['root']['input']='checkbox';
	$fields['root']['type']='bool';
	$fields['root']['table']=$tbl;
	$fields['root']['title']=sys::translate('content::root_node');
	if(in_array($_GET['id'],$_cfg['content::sys_types']))
	{
		$fields['root']['table']=false;
		$fields['root']['html_params']='disabled';
		$fields['root']['value']=true;
	}
	
	//tree
	$fields['tree']['input']='checkbox';
	$fields['tree']['type']='bool';
	$fields['tree']['table']=$tbl;
	$fields['tree']['title']=sys::translate('content::show_in_tree');
	if(in_array($_GET['id'],$_cfg['content::sys_types']))
	{
		$fields['tree']['table']=false;
		$fields['tree']['html_params']='disabled';
		$fields['tree']['value']=true;
	}
	
	//data
	$fields['data']['input']='checkbox';
	$fields['data']['type']='bool';
	$fields['data']['table']=$tbl;
	$fields['data']['title']=sys::translate('content::data');
	if(in_array($_GET['id'],$_cfg['content::sys_types']))
	{
		$fields['data']['table']=false;
		$fields['data']['html_params']='disabled';
		$fields['data']['value']=false;
	}
	
	//multi_lang
	$fields['multi_lang']['input']='checkbox';
	$fields['multi_lang']['type']='bool';
	$fields['multi_lang']['table']=$tbl;
	$fields['multi_lang']['title']=sys::translate('content::multi_lang');
	if(in_array($_GET['id'],$_cfg['content::sys_types']))
	{
		$fields['multi_lang']['table']=false;
		$fields['multi_lang']['html_params']='disabled';
		$fields['multi_lang']['value']=true;
	}
	
	//parent_types
	$fields['parent_types']['input']='textarea';
	$fields['parent_types']['type']='text';
	$fields['parent_types']['table']=$tbl;
	$fields['parent_types']['html_params']='style="width:100%; height:150px"';
	$fields['parent_types']['title']=sys::translate('content::parent_types');
	
	//on
	$fields['on']['input']='checkbox';
	$fields['on']['type']='bool';
	$fields['on']['title']=sys::translate('sys::is_on');
	$fields['on']['table']=$tbl;
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>