<?
function content_object_edit()
{
	if(!sys::checkAccess('content::content'))
		return 403;
		
	global $_db, $_err;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::langs');
	
	sys::filterGet('id','int');
	sys::filterGet('parent_id','int');
	sys::filterGet('type_id','int');
	
	if($_GET['type_id'])
	{
		$type_name=$_db->getValue('content_types','name',intval($_GET['type_id']));
		if(!$type_name)
			return 404;
		else
		{
			sys::redirect('?mod=main&act='.$type_name.'_edit&id='.$_GET['id'].'&parent_id='.$_GET['parent_id']);
		}
	}
	
	$title=sys::translate('content::new_object');
	
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//type
	$fields['type']['input']="selectbox";
	$fields['type']['type']='text';
	$fields['type']['req']=true;
	$r=$_db->query("SELECT `name` FROM `#__content_types`
		WHERE `root`=1 AND `on`=1
		ORDER BY `order_number`");
	$fields['type']['values'][0]=' ';
	while(list($name)=$_db->fetchArray($r))
	{
		$fields['type']['values'][$name]=sys::translate('main::'.$name);
	}
	$fields['type']['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\'); window.location=\'?mod=main&act=\'+this.value+\'_edit&parent_id='.$_GET['parent_id'].'\';"';
	
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>