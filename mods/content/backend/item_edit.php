<?
function content_item_edit()
{
	if(!sys::checkAccess('content::content'))
		return 403;
		
	global $_db, $_err, $_user, $_cfg;
		
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('content::objects');
	sys::useLib('content::items');
	
	sys::filterGet('id','int');
	sys::filterGet('parent_id','int',0);
	sys::filterGet('no_close','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl_obj='content_objects';
	$tbl_data='content_items';
	$type_id=9;
	$default=content_objects::getDefaultValues($type_id,$_GET['parent_id']);
	$type=$_db->getRecord('content_types',$type_id);
	
	
	if($_GET['id'])
	{
		$record=content_objects::getRecord($_GET['id'],$type_id);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('content::new_'.$type['name']);
	}
	
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	if(!$_GET['id'])
	{
		$fields=content_objects::getHiddenFields();
	}
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl_obj;
	$fields['name']['max_chars']=255;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl_obj;
	$fields['title']['max_chars']=255;
	$fields['title']['html_params']='style="width:100%" maxlength="255"';
	
	//short_title
	$fields['short_title']['input']='textbox';
	$fields['short_title']['type']='text';
	$fields['short_title']['multi_lang']=true;
	$fields['short_title']['table']=$tbl_obj;
	$fields['short_title']['max_chars']=55;
	$fields['short_title']['html_params']='size="55" style="maxlength="55"';
	$fields['short_title']['title']=sys::translate('content::short_title');
	$fields['short_title']['info']=sys::getHlp('content::short_title');
	
	//image
	$fields['image']['input']='filebox';
	$fields['image']['type']='file';
	$fields['image']['file::dir']=$_cfg['content::files_dir'];
	$fields['image']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image']['title']=sys::translate('sys::image');
	$fields['image']['preview']=true;
	
	//intro
	$fields['intro']['input']='fck';
	$fields['intro']['type']='html';
	$fields['intro']['table']=$tbl_data;
	$fields['intro']['multi_lang']=true;
	$fields['intro']['html_params']='style="width:100%; height:50px"';
	$fields['intro']['height']='300px';
	$fields['intro']['title']=sys::translate('content::intro');
	
	//text
	$fields['text']['input']='fck';
	$fields['text']['type']='html';
	$fields['text']['table']=$tbl_obj;
	$fields['text']['multi_lang']=true;
	$fields['text']['html_params']='style="width:100%; height:50px"';
	$fields['text']['height']='300px';
	
	//meta_title
	$fields['meta_title']['input']='textbox';
	$fields['meta_title']['type']='text';
	$fields['meta_title']['multi_lang']=true;
	$fields['meta_title']['table']=$tbl_obj;
	$fields['meta_title']['max_chars']=255;
	$fields['meta_title']['html_params']='style="width:100%" maxlength="255"';
	
	//meta_description
	$fields['meta_description']['input']='textarea';
	$fields['meta_description']['type']='text';
	$fields['meta_description']['table']=$tbl_obj;
	$fields['meta_description']['multi_lang']=true;
	$fields['meta_description']['html_params']='style="width:100%; height:150px"';
	
	//meta_keywords
	$fields['meta_keywords']['input']='textarea';
	$fields['meta_keywords']['type']='text';
	$fields['meta_keywords']['table']=$tbl_obj;
	$fields['meta_keywords']['multi_lang']=true;
	$fields['meta_keywords']['html_params']='style="width:100%; height:150px"';
	
	//meta_other
	$fields['meta_other']['input']='textarea';
	$fields['meta_other']['type']='html';
	$fields['meta_other']['table']=$tbl_obj;
	$fields['meta_other']['multi_lang']=true;
	$fields['meta_other']['html_params']='style="width:100%; height:150px"';
	
	//public
	$fields['public']['input']='checkbox';
	$fields['public']['type']='bool';
	$fields['public']['table']=$tbl_obj;
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=content_objects::insertObject($type_id,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!content_objects::updateObject($type_id,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			//Загрузить и отресайзить картинку	
			$values['image']=content_items::uploadImage($_GET['id'],$record['image'],$values['image']);
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			content_items::deleteImage($_GET['id'],$values['image']);
			$values['image']=false;
		}
	}
	//==================================================================================================
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	if(!$_GET['no_close'])
		$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>