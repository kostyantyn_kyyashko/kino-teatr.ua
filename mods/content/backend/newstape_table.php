<?
function content_newstape_table()
{
	if(!sys::checkAccess('content::content'))
		return 403;
		
	global $_db, $_err;
		
	sys::useLib('sys::pages');
	sys::useLib('content::objects');
	
	sys::filterGet('order_by','text','date.desc');
	sys::filterGet('parent_id','int',0);
	
	$tbl='content_objects';
	$edit_url='?mod=content&act=news_edit';
	$win_params=sys::setWinParams('780','580');
	$add_title=sys::translate('content::add_news');
	$where="rels.parent_id=".$_GET['parent_id'];
	$parent=$_db->getRecord('content_objects',intval($_GET['parent_id']));
	$title=$parent['name'].' :: '.sys::translate('content::news');
	
	$q="SELECT 
			obj.id,
			obj.name, 
			obj.public,
			rels.order_number,
			news.date  
		FROM `#__content_objects_rels` AS `rels`
		LEFT JOIN `#__".$tbl."` AS `obj`
		ON rels.child_id=obj.id
		LEFT JOIN `#__content_news` AS `news`
		ON news.object_id=obj.id
		WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			content_objects::deleteObject($task[1]);
		
		if($task[0]=='move')
		{
			if(!content_objects::moveObject($task[1],$task[2],$_GET['parent_id']))
				$_err=sys::translate('content::invalid_parent');
			else 
				$tree_id=$task[2];
		}
		
		if($task[0]=='copy')
		{
			if(!content_objects::copyObject($task[1],$task[2]))
				$_err=sys::translate('content::invalid_parent');
			else 
				$tree_id=$task[2];
		}
		
		if($task[0]=='clone')
		{
			if(!content_objects::copyObject($task[1],$_GET['parent_id']))
				$_err=sys::translate('content::invalid_parent');
		}
		
		if($task[0]=='link' && ($task[1]!=$_GET['parent_id']))
		{
			if(!content_objects::linkObject($task[1],$task[2]))
				$_err=sys::translate('content::invalid_parent');
			else 
				$tree_id=$task[2];
		}
		
		if($task[0]=='unlink' && ($task[1]!=$_GET['parent_id']))
			content_objects::unlinkObject($task[1],$_GET['parent_id']);
			
		//Сдвиг вверх
		if($task[0]=='moveUp')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				content_objects::moveDown($task[1],$_GET['parent_id']);
			else
				content_objects::moveUp($task[1],$_GET['parent_id']);
		}
		
		//Сдвиг вниз
		if($task[0]=='moveDown')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				content_objects::moveUp($task[1],$_GET['parent_id']);
			else
				content_objects::moveDown($task[1],$_GET['parent_id']);
		}
		
		if($task[0]=='moveChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					content_objects::moveObject($key,$task[1],$_GET['parent_id']);
				}
				$tree_id=$task[1];
			}
		
				
		}
		
		if($task[0]=='copyChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					content_objects::copyObject($key,$task[1]);
				}
				$tree_id=$task[1];
			}
		}
		
		if($task[0]=='cloneChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					content_objects::copyObject($key,$_GET['parent_id']);
				}
			}
		}
		
		if($task[0]=='linkChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					content_objects::linkObject($key,$task[1]);
				}
				$tree_id=$task[1];
			}
		}
		
		if($task[0]=='unlinkChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					content_objects::unlinkObject($key,$_GET['parent_id']);
				}
			}
		}
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					content_objects::deleteObject($key);
				}
			}		
		}
		
		//Упорядочение нумерации
		if($_POST['_task']=='orderNumbers')
			content_objects::orderNumbers($_GET['parent_id']);
			
	}
	//================================================================================================//
	
	if(isset($tree_id))
		sys_gui::reloadTreeBranch('window.parent.fra_left',$tree_id,content_objects::getTreeBranch($tree_id),$_GET['parent_id']);
		
	sys_gui::reloadTreeBranch('window.parent.fra_left',$_GET['parent_id'],content_objects::getTreeBranch($_GET['parent_id']),$_GET['parent_id']);
	
	
	
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['move_up']['type']='button';
	$table['fields']['move_up']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveUp.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_up']['image']='sys::btn.desc.gif';
	
	$table['fields']['move_down']['type']='button';
	$table['fields']['move_down']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveDown.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_down']['image']='sys::btn.asc.gif';
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%&type_id=%type_id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%&type_id=%type_id%';
	
	$table['fields']['move']['type']='button';
	$table['fields']['move']['frame']='?mod=content&act=objects_tree&task=move&object_id=%id%';
	
	$table['fields']['copy']['type']='button';
	$table['fields']['copy']['frame']='?mod=content&act=objects_tree&task=copy&object_id=%id%';
	
	$table['fields']['clone']['type']='button';
	$table['fields']['clone']['onclick']="setMsg('".sys::translate('sys::cloning')."'); document.forms['table'].elements._task.value='clone.%id%'; document.forms['table'].submit(); return false;";
	
	$table['fields']['link']['type']='button';
	$table['fields']['link']['frame']='?mod=content&act=objects_tree&task=link&object_id=%id%';
	$table['fields']['link']['image']='sys::btn.rel.gif';
	
	$table['fields']['unlink']['type']='button';
	$table['fields']['unlink']['onclick']="if(confirm('".sys::translate('sys::unlink')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::unlinking')."'); document.forms['table'].elements._task.value='unlink.%id%'; document.forms['table'].submit();} return false;";
	$table['fields']['unlink']['image']='sys::btn.unrel.gif';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;
	$table['fields']['name']['onclick']="setMsg('".sys::translate('sys::loading')."'); window.location='?mod=content&act=objects_table&parent_id=%id%'; return false";
	$table['fields']['name']['url']='?mod=content&act=objects_table&parent_id=%id%';
	
	$table['fields']['date']['type']='text';
	$table['fields']['date']['order']=true;
	
	/*
	$table['fields']['menu']['type']='checkbox';
	$table['fields']['menu']['title']=sys::translate('content::mnu');
	$table['fields']['menu']['alt']=sys::translate('content::show_in_menu');
	$table['fields']['menu']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~menu~%id%\')"';
	$table['fields']['menu']['width']=40;
	*/
	
	$table['fields']['public']['type']='checkbox';
	$table['fields']['public']['title']=sys::translate('sys::pbl');
	$table['fields']['public']['alt']=sys::translate('sys::public');
	$table['fields']['public']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=content&act=ajx_update&task=public&id=%id%\')"';
	$table['fields']['public']['width']=40;
	
	/*
	$table['fields']['order_number']['type']='textbox';
	$table['fields']['order_number']['order']=true;
	$table['fields']['order_number']['html_params']='size="8" onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=content&act=ajx_update&task=order_number&parent_id='.$_GET['parent_id'].'&child_id=%id%&order_number=\'+this.value)"';
	*/
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&parent_id=".$_GET['parent_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';
	
	$buttons['move']['frame']='?mod=content&act=objects_tree&task=moveChecked&field=_task';
	$buttons['move']['title']=sys::translate('sys::move_checked');
	
	$buttons['copy']['frame']='?mod=content&act=objects_tree&task=copyChecked&field=_task';
	$buttons['copy']['title']=sys::translate('sys::copy_checked');
	
	$buttons['clone_checked']['title']=sys::translate('sys::clone_checked');
	$buttons['clone_checked']['image']='sys::btn.clone.gif';
	$buttons['clone_checked']['onclick']="setMsg('".sys::translate('sys::cloning')."'); document.forms['table'].elements._task.value='cloneChecked'; document.forms['table'].submit(); return false;";
	
	$buttons['link']['frame']='?mod=content&act=objects_tree&task=linkChecked&field=_task';
	$buttons['link']['title']=sys::translate('sys::link_checked');
	$buttons['link']['image']='sys::btn.rel.gif';
	
	$buttons['unlink_checked']['title']=sys::translate('sys::unlink_checked');
	$buttons['unlink_checked']['image']='sys::btn.unrel.gif';
	$buttons['unlink_checked']['onclick']="if(confirm('".sys::translate('sys::unlink_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::unlinking')."'); document.forms['table'].elements._task.value='unlinkChecked'; document.forms['table'].submit();} return false;";
	
	//=======================================================================================================//
	
	
	//Формирование результата------------------------------------------------------------------------------//
	sys::setTitle('content::content');
	
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>