<div class="content">
	<h1><?=sys::translate('content::map')?></h1>
</div>
<ul class="map">
<?foreach ($var['objects'] as $obj):?>
	<li style="padding-left:<?=$obj['level']?>0px">
		<a title="<?=$obj['alt']?>" href="<?=$obj['url']?>"><?=$obj['title']?></a>
	</li>
<?endforeach?>
</ul>
