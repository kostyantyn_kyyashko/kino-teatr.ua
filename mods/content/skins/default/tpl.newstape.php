<table class="newstape">
	<?foreach ($var['objects'] as $obj):?>
		<tr>
			<td class="content">
				<?if($obj['image']):?>
					<div class="image"><img src="<?=$obj['image']?>" alt="<?=$obj['title']?>" title="<?=$obj['title']?>"></div>
				<?endif?>
				<h2 id="news_<?=$obj['id']?>">
					<?if($obj['url']):?>
						<a href="<?=$obj['url']?>"><?=$obj['title']?></a>
					<?else:?>
						<?=$obj['title']?>
					<?endif;?>
				</h2>
				<?=$obj['intro']?>
				<div class="date"><?=$obj['date']?></div>
			</td>
		</tr>
	<?endforeach;?>
</table>
<?=sys::parseTpl('main::pages',$var['pages'])?>