<div class="content">
	<?foreach ($var['objects'] as $obj):?>
	
				<h2 id="article_<?=$obj['id']?>">
					<?if($obj['url']):?>
						<a href="<?=$obj['url']?>"><?=$obj['title']?></a>
					<?else:?>
						<?=$obj['title']?>
					<?endif;?>
				</h2>
				<?=$obj['intro']?>
	<?endforeach;?>
</div>
<?=sys::parseTpl('main::pages',$var['pages'])?>