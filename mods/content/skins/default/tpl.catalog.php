<table class="catalogs">
	<?foreach ($var['catalogs'] as $catalog):?>
		<tr>
			<td class="image">
				<div class="image">
					<a href="<?=$catalog['url']?>">
						<img src="<?=$catalog['image']?>" alt="<?=$catalog['title']?>" title="<?=$catalog['title']?>">
					</a>
				</div>
			</td>
			<td class="content">
				<h2><a href="<?=$catalog['url']?>"><?=$catalog['title']?></a></h2>
				<?=$catalog['text']?>
			</td>
		</tr>
	<?endforeach;?>
</table>
<?=sys::parseTpl('main::pages',$var['pages'])?>
<table class="items">
	<?foreach ($var['items'] as $item):?>
		<tr>
			<td class="image">
				<div class="image">
					<a href="<?=$item['url']?>">
						<img src="<?=$item['image']?>" alt="<?=$item['title']?>" title="<?=$item['title']?>">
					</a>
				</div>
			</td>
			<td class="content">
				<h3><a href="<?=$item['url']?>"><?=$item['title']?></a></h3>
				<?=$item['intro']?>
			</td>
		</tr>
	<?endforeach;?>
</table>
<?=sys::parseTpl('main::pages',$var['pages'])?>