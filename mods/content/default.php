<?
function content_default()
{
	global $_db, $_cfg;
	sys::filterGet('id','int');
	if(!$_GET['id'])
		return 404;
	
	$type_id=$_db->getValue('content_objects','type_id',intval($_GET['id']));
	if(!$type_id)
		return 404;
	$type_name=$_db->getValue('content_types','name',$type_id);
	if(!$type_name)
		return 404;
	
	$_GET['act']=$type_name;
	return sys::useAct('main::'.$type_name);
}
?>