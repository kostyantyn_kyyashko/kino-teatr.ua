<?
class content_objects
{
	static function deleteObject($object_id)
	{
		global $_db, $_cfg;
		//Получить данне по типу объекта
		$r=$_db->query("SELECT types.* 
			FROM `#__content_objects` AS `obj`
			LEFT JOIN `#__content_types` AS `types`
			ON types.id=obj.type_id
			WHERE obj.id=".intval($object_id)."");
		
		$type=$_db->fetchAssoc($r);
		
		//Удалить всех потомков объекта
		$r=$_db->query("SELECT `child_id` 
			FROM `#__content_objects_rels`
			WHERE `parent_id`=".intval($object_id)."");
		while(list($child_id)=$_db->fetchArray($r))
		{
			self::deleteObject($child_id);
		}
		
		//Выполнить функцию удаления объекта по типу
		if($type['data'])
		{
			eval("sys::useLib('main::".$type['name_multi']."');");
			eval("main_".$type['name_multi']."::delete".$type['name']."(\$object_id);");
		}
		
		//Удалить все отношения объекта
		$_db->delete('content_objects_rels',"`parent_id`=".intval($object_id)." OR `child_id`=".intval($object_id)."");
		
		
		//Удалить объект
		$_db->delete('content_objects',intval($object_id),true);
	}
	
	static function publicObject($obj_id)
	{
		global $_db;
		$public=$_db->getValue('content_objects','public',intval($obj_id));
		if($public)
			$public=0;
		else
			$public=1;
			
		$children=self::getAllChildren($obj_id);
		array_push($children, intval($obj_id));
		$_db->query("UPDATE `#__content_objects` SET `public`=".$public."
			WHERE `id` IN (".implode(',',$children).")"); 
	}
	
	static function getTreeBranch($parent_id)
	{
		global $_db;
		sys::filterGet('task','text');
		sys::filterGet('field','text');
		sys::filterGet('object_id','int');
		$result=array();
		$r=$_db->query("SELECT obj.id, obj.name AS `title` 
			FROM `#__content_objects` AS `obj`
			LEFT JOIN `#__content_objects_rels` AS `rels`
				ON rels.child_id=obj.id
			LEFT JOIN `#__content_types` AS `types`
				ON types.id=obj.type_id
			WHERE rels.parent_id=".intval($parent_id)." AND types.tree=1
			ORDER BY rels.order_number
			");
		while($obj=$_db->fetchAssoc($r))
		{	
			switch($_GET['task'])
			{
				case 'setValue':	
					$obj['onclick']="window.parent.document.forms['form'].elements['".$_GET['field']."'].value=".$obj['id'].";";
					$obj['onclick'].="getChildByTagName(window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode,'A').innerHTML='".htmlspecialchars($obj['title'])."';";
					$obj['onclick'].="getChildByTagName(window.parent.document.forms['form'].elements['".$_GET['field']."'].parentNode,'DIV').style.display='none';return false;";
				break;
				
				case 'move':	
					$obj['onclick']="window.parent.document.forms['table'].elements['_task'].value='move.".$_GET['object_id'].".".$obj['id']."';";
					$obj['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
				break;
				
				case 'copy':	
					$obj['onclick']="window.parent.document.forms['table'].elements['_task'].value='copy.".$_GET['object_id'].".".$obj['id']."';";
					$obj['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
				break;
				
				case 'link':	
					$obj['onclick']="window.parent.document.forms['table'].elements['_task'].value='link.".$_GET['object_id'].".".$obj['id']."';";
					$obj['onclick'].="setMsg('".sys::translate('sys::linking')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
				break;
				
				case 'moveChecked':	
					$obj['onclick']="window.parent.document.forms['table'].elements['_task'].value='moveChecked.".$obj['id']."';";
					$obj['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
				break;
				
				case 'copyChecked':	
					$obj['onclick']="window.parent.document.forms['table'].elements['_task'].value='copyChecked.".$obj['id']."';";
					$obj['onclick'].="setMsg('".sys::translate('sys::moving')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
				break;
				
				case 'linkChecked':	
					$obj['onclick']="window.parent.document.forms['table'].elements['_task'].value='linkChecked.".$obj['id']."';";
					$obj['onclick'].="setMsg('".sys::translate('sys::linking')."','window.parent'); window.parent.document.forms['table'].submit(); return false;";
				break;
			
				default:
					$obj['onclick']="setMsg('".sys::translate('sys::loading')."','window.parent.fra_right'); window.parent.fra_right.location='?mod=content&act=objects_table&parent_id=".$obj['id']."';";
					$obj['url']="?mod=content&act=objects_table&parent_id=".$obj['id'];
			}
			//Проверить есть ли у узла потомки
			$r2=$_db->query("SELECT rels.child_id
				FROM `#__content_objects_rels` AS `rels`
				LEFT JOIN `#__content_objects` AS `obj`
					ON obj.id=rels.child_id
				LEFT JOIN `#__content_types` AS `types`
					ON types.id=obj.type_id
				WHERE rels.parent_id='".$obj['id']."' AND types.tree=1
				LIMIT 0,1
			");
			
			list($obj['children'])=$_db->fetchArray($r2);
			
			$obj['children_url']='?mod=content&act=objects_tree&task='.$_GET['task'].'&field='.$_GET['field'].'&object_id='.$_GET['object_id'].'&parent_id='.$obj['id'];	
			$result[]=$obj;
		}
		return $result;
	}
	
	static function moveObject($obj_id, $to_id, $parent_id)
	{
		
		if(self::checkParentType($obj_id, $to_id))
		{
			self::unlinkObject($obj_id, $parent_id);
			self::linkObject($obj_id, $to_id);
			return true;
		}
		return false;
	}
	
	static function copyObject($obj_id, $to_id)
	{
		global $_db, $_user, $_cfg;
		
		$_db->begin();
		$obj=$_db->getRecord("content_objects", intval($obj_id));
		if(!$obj)
			return false;
			
		$type=$_db->getRecord('content_types',$obj['type_id']);
		$values['id']=0;
		$values['public_date']=gmdate('Y-m-d H:i:s');
		$values['name']=mysql_real_escape_string($obj['name']).' ('.mb_strtolower(sys::translate('content::copy')).')';
		$values['user_id']=$_user['id'];
		if(!$_db->copyRecord('content_objects',$obj_id,false,$values))
			return $_db->rollback();
		unset($values);
			
		$copy_id=$_db->last_id;
		
		$r=$_db->query("SELECT * FROM `#__content_objects_lng` WHERE `record_id`=".intval($obj['id'])."");
		while($lng=$_db->fetchAssoc($r))
		{
			if(!$_db->query("INSERT INTO `#__content_objects_lng`
				(
					`record_id`,
					`lang_id`,
					`title`,
					`text`,
					`short_title`,
					`meta_title`,
					`meta_description`,
					`meta_keywords`,
					`meta_other`
				)	
				VALUES
				(
					".$copy_id.",
					".$lng['lang_id'].",
					'".mysql_real_escape_string($lng['title'])."',
					'".mysql_real_escape_string($lng['text'])."',
					'".mysql_real_escape_string($lng['short_title'])."',
					'".mysql_real_escape_string($lng['meta_title'])."',
					'".mysql_real_escape_string($lng['meta_description'])."',
					'".mysql_real_escape_string($lng['meta_keywords'])."',
					'".mysql_real_escape_string($lng['meta_other'])."'
				)
				
			"))
				return $_db->rollback();
				
		}
	
		
		if($type['data'])
		{
			$result=false;
			eval("sys::useLib('main::".$type['name_multi']."');");
			eval("\$result=main_".$type['name_multi']."::copy".$type['name']."(\$obj_id,\$copy_id);");
			if(!$result)
				return $_db->rollback();
		}
		
		if(self::linkObject($copy_id, $to_id))
		{
			$_db->commit();
			$children=self::getChildren($obj_id);
			foreach ($children as $child_id)
			{
				self::copyObject($child_id,$copy_id);
			}
			return true;
		}
		
		
		return false;
	}
	
	static function linkObject($obj_id, $to_id)
	{
		global $_db;
		
		if(self::checkParentType($obj_id, $to_id))
		{
			$order_number=$_db->getMaxOrderNumber('content_objects_rels',"`parent_id`=".intval($to_id)."");
			$_db->query("INSERT INTO `#__content_objects_rels` VALUES (".intval($to_id).",".intval($obj_id).",".intval($order_number).")");
			return true;	
		}
		return false;
	}
	
	static function unlinkObject($obj_id, $parent_id)
	{
		global $_db;
		$_db->query("DELETE FROM `#__content_objects_rels` WHERE `parent_id`=".intval($parent_id)." AND `child_id`=".intval($obj_id)."");
	}
	
	static function checkParentType($obj_id, $parent_id)
	{
		global $_db, $_cfg;
			
		if(in_array($parent_id,self::getAllChildren($obj_id)))
			return false;
		
		$r=$_db->query("SELECT types.parent_types FROM `#__content_objects` AS `obj`
			LEFT JOIN `#__content_types` AS `types`
			ON types.id=obj.type_id
			WHERE obj.id=".intval($obj_id)."");
		
		list($parent_types)=$_db->fetchArray($r);
		$parent_types=str_replace(' ','',$parent_types);
		$parent_types=explode(',',$parent_types);
		if($parent_id==0)
			$parent_type='root';
		else 
		{
			$r=$_db->query("SELECT types.name FROM `#__content_objects` AS `obj`
				LEFT JOIN `#__content_types` AS `types`
				ON types.id=obj.type_id
				WHERE obj.id=".intval($parent_id)."");
			list($parent_type)=$_db->fetchArray($r);
		}
		if($obj_id==$parent_id)
			return false;
	
		return in_array($parent_type,$parent_types);
	}
	
	static function getParents($obj_id)
	{
		global $_db;
		$parent_id=$obj_id;
		while($parent_id)
		{
			$parent_id=$_db->getValue('content_objects_rels','parent_id',"`child_id`=".intval($parent_id)."");
			$result[]=$parent_id;
		}
		return $result;
	}
	
	static function getAllChildren($obj_id)
	{
		global $_db;
		$result=array();
		$r=$_db->query("SELECT `child_id` FROM `#__content_objects_rels` WHERE `parent_id`=".intval($obj_id)."");
		while(list($child_id)=$_db->fetchArray($r))
		{
			if($child_id)
			{
				$result[]=$child_id;
				$result=array_merge($result,self::getAllChildren($child_id));
			}
		}
		return $result;
	}
	
	static function getChildren($obj_id)
	{
		global $_db;
		$result=array();
		$r=$_db->query("SELECT `child_id` FROM `#__content_objects_rels` WHERE `parent_id`=".intval($obj_id)."");
		while(list($child_id)=$_db->fetchArray($r))
		{
			$result[]=$child_id;
		}
		return $result;
	}
	
	static function moveDown($obj_id, $parent_id)
	{
		global $_db;
		$r=$_db->query("SELECT `child_id`, `order_number` FROM `#__content_objects_rels` 
							WHERE `child_id`=".intval($obj_id)." AND `parent_id`=".intval($parent_id)."");
		$current=$_db->fetchAssoc($r);
		
		$r=$_db->Query("SELECT `child_id` FROM `#__content_objects_rels`  
							WHERE `order_number` > '".$current['order_number']."' 
							AND `parent_id`=".intval($parent_id)."
							ORDER BY `order_number` ASC LIMIT 0,1");
		list($next_id) = $_db->fetchArray($r);
		if($next_id)
		{
			$_db->Query("UPDATE `#__content_objects_rels` 
							SET `order_number` = ".($current['order_number']+1)." 
							WHERE `child_id` = ".$current['child_id']." AND `parent_id`=".intval($parent_id)."");
		
			$_db->Query("UPDATE `#__content_objects_rels` 
							SET `order_number` = '".$current['order_number']."' 
							WHERE `child_id` = ".$next_id." AND `parent_id`=".intval($parent_id)."");
		}
	}
	
	static function moveUp($obj_id, $parent_id)
	{		
		global $_db;
		$r=$_db->query("SELECT `child_id`, `order_number` FROM `#__content_objects_rels` 
							WHERE `child_id`=".intval($obj_id)." AND `parent_id`=".intval($parent_id)." ");
		$current=$_db->fetchAssoc($r);
		
		$r=$_db->Query("SELECT `child_id` FROM `#__content_objects_rels`  
							WHERE `order_number` < '".$current['order_number']."' 
							AND `parent_id`=".intval($parent_id)."
							ORDER BY `order_number` DESC LIMIT 0,1");
		list($prev_id) = $_db->fetchArray($r);
		if($prev_id)
		{
			$_db->Query("UPDATE `#__content_objects_rels` 
							SET `order_number` = ".($current['order_number']-1)." 
							WHERE `child_id` = ".$current['child_id']." AND `parent_id`=".intval($parent_id)."");
		
			$_db->Query("UPDATE `#__content_objects_rels` 
							SET `order_number` = '".$current['order_number']."' 
							WHERE `child_id` = ".$prev_id." AND `parent_id`=".intval($parent_id)." ");
		}
	}
	
	static function orderNumbers($parent_id)
	{
		global $_db;
		$r=$_db->Query("SELECT `child_id` FROM `#__content_objects_rels` 
							WHERE `parent_id`=".intval($parent_id)." 
							ORDER BY `order_number`");
		$i=1;
		while(list($child_id)=$_db->fetchArray($r))
		{
			$_db->query("UPDATE `#__content_objects_rels` SET `order_number` = ".$i." 
							WHERE `parent_id`=".intval($parent_id)." AND `child_id`=".$child_id."");
			$i++;
		}
	} 
	
	static function getDefaultValues($type_id, $parent_id)
	{
		global $_db, $_user;
		$result['order_number']=$_db->getMaxOrderNumber('content_objects_rels',"`parent_id`=".intval($_GET['parent_id'])."");
		$result['public']=1;
		$result['parent_id']=$parent_id;
		$result['user_id']=$_user['id'];
		$result['public_date']=gmdate('Y-m-d H:i:s');
		$result['type_id']=$type_id;
		
		return $result;
	}
	
	static function getHiddenFields()
	{
		//parent_id
		$result['parent_id']['input']='hidden';
		$result['parent_id']['type']='int';
		
		//type_id
		$result['type_id']['input']='hidden';
		$result['type_id']['type']='int';
		$result['type_id']['table']='content_objects';
		
		//order_number
		$result['order_number']['input']='hidden';
		$result['order_number']['type']='int';
		
		//user_id
		$result['user_id']['input']='hidden';
		$result['user_id']['type']='int';
		$result['user_id']['table']='content_objects';
		
		//public_date
		$result['public_date']['input']='hidden';
		$result['public_date']['type']='text';
		$result['public_date']['table']='content_objects';
		
		return $result;
	}
	
	static function getRecord($object_id, $type_id)
	{
		global $_db;
		$type=$_db->getRecord('content_types',intval($type_id));
		$result=$_db->getRecord('content_objects',$object_id,true);
		if($type['data'])
		{
			$data=$_db->getRecord('main_'.$type['name_multi'], $object_id, $type['multi_lang'], 'object_id');
			$result=array_merge($result, $data);
		}
		return $result;
	}
	
	static function insertObject($type_id, $arr_data, $arr_params)
	{
		global $_db;
		$_db->begin();
		
		if(!$id=$_db->insertArray('content_objects',$arr_data,$arr_params))
		{
			$_db->rollback();
			return false;
		}
		$_db->query("INSERT INTO `#__content_objects_rels` VALUES (".intval($arr_data['parent_id']).",".intval($id).",".intval($arr_data['order_number']).")");
		$type=$_db->getRecord('content_types',intval($type_id));
		if($type['data'])
		{
			$arr_data['object_id']=$id;
			$arr_params['object_id']['type']='int';
			$arr_params['object_id']['table']='main_'.$type['name_multi'];
			if(!$_db->insertArray('main_'.$type['name_multi'],$arr_data,$arr_params,false,'object_id'))
			{
				$_db->rollback();
				return false;
			}
		}
		$_db->commit();
		return $id;
	}
	
	static function updateObject($type_id, $arr_data, $arr_params, $object_id)
	{
		global $_db;
		$_db->begin();
		if(!$_db->updateArray('content_objects',$arr_data ,$arr_params, intval($object_id)))
			return $_db->rollback();
			
		$type=$_db->getRecord('content_types',intval($type_id));
		if($type['data'])
		{
			if(!$_db->updateArray('main_'.$type['name_multi'],$arr_data ,$arr_params, intval($object_id),'object_id'))
				return $_db->rollback();
		}
		$_db->commit();
		return true;
	}
}
?>