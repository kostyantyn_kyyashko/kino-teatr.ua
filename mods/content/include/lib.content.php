<?
class content
{
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $active_id
	 * @param unknown_type $tpl
	 * @return unknown
	 */
	static function showMenu($active_id, $tpl='main::menu')
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT obj.id, types.name AS `type`, obj.url, obj.entry_id, lng.title, lng.short_title
				FROM `#__content_objects_rels` AS `rels`
			LEFT JOIN `#__content_objects_lng` AS `lng`
				ON lng.record_id=rels.child_id
				AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
			LEFT JOIN `#__content_objects` AS `obj`
				ON rels.child_id=obj.id
			LEFT JOIN `#__content_types` AS `types`
				ON types.id=obj.type_id
			WHERE
				obj.menu=1 AND rels.parent_id=0 AND obj.public=1
			ORDER BY rels.order_number
		");

		$lang=$_cfg['sys::lang'];




		while($obj=$_db->fetchAssoc($r))
		{
			if($obj['entry_id'])
			{
				$mode = $_db->getValue('sys_mods_entries','name',$obj['entry_id']);

				if ($lang=='ru')
				{
				$obj['url'] = _ROOT_URL.$mode.'.phtml';
				} else {
				$obj['url'] = _ROOT_URL.$lang.'/'.$mode.'.phtml';
				}

			}
			elseif (!$obj['url'])
			{
				$obj['url']=sys::rewriteUrl('?mod=content&act='.$obj['type'].'&id='.$obj['id']);
			}
			$obj['alt']=htmlspecialchars($obj['title']);
			if($obj['short_title'])
				$obj['title']=$obj['short_title'];

			if($obj['id']==$active_id)
				$obj['active']=true;
			else
				$obj['active']=false;

			$obj['title']=htmlspecialchars($obj['title']);
			$var['objects'][]=$obj;
		}
		return sys::parseTpl($tpl,$var);
	}

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $object_id
	 * @param unknown_type $tpl
	 * @return unknown
	 */
	static function showNavPath($object_id, $tpl='main::navpath')
	{
		global $_db, $_cfg;
		$parent_id=$object_id;
		$var['objects']=array();
		while ($parent_id)
		{
			$r=$_db->query("
				SELECT
					rels.parent_id,
					lng.title,
					lng.short_title
				FROM `#__content_objects_rels` AS `rels`
				LEFT JOIN `#__content_objects_lng` AS `lng`
					ON lng.record_id=rels.child_id
					AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
				WHERE rels.child_id=".intval($parent_id)."
				LIMIT 0,1
			");
			$obj=$_db->fetchAssoc($r);
			if(!$obj['short_title'])
				$obj['short_title']=$obj['title'];
			$obj['alt']=$obj['title'];
			$obj['title']=$obj['short_title'];
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['alt']=htmlspecialchars($obj['alt']);
			if($parent_id!=$object_id)
				$obj['url']=sys::rewriteUrl('?mod=content&act=default&id='.$parent_id);
			else
				$obj['url']=false;
			$var['objects'][]=$obj;
			$parent_id=$obj['parent_id'];
		}
		if($var['objects'])
			$var['objects']=array_reverse($var['objects']);
		return sys::parseTpl($tpl, $var);
	}

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $object_id
	 * @return unknown
	 */
	static function getFirstParentId($object_id)
	{
		global $_db;
		$r=$_db->query("SELECT `parent_id`
				FROM `#__content_objects_rels`
				WHERE `child_id`=".intval($object_id)."
				LIMIT 0,1");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $obj_id
	 * @return unknown
	 */
	static function getRootId($obj_id)
	{
		global $_db;
		$root_id=$obj_id;
		$parent_id=$_db->getValue('content_objects_rels','parent_id',"`child_id`=".intval($obj_id)."");
		while ($parent_id)
		{
			if($parent_id)
				$root_id=$parent_id;
			else
				return $root_id;
			$parent_id=$_db->getValue('content_objects_rels','parent_id',"`child_id`=".intval($parent_id)."");
		}
		return $root_id;
	}


	static function getParentId($obj_id)
	{
		global $_db;
		$root_id=$obj_id;
		$result=$_db->getValue('content_objects_rels','parent_id',"`child_id`=".intval($obj_id)."");
		return $result;
	}

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $obj_id
	 * @return unknown
	 */
	static function getObject($obj_id)
	{
		global $_db, $_cfg;
		$type_id=$_db->getValue('content_objects','type_id',intval($obj_id));
		$type=$_db->getRecord('content_types',$type_id);

		$q="SELECT obj.*, lng.* ";
		if($type['data'])
			$q.=",data.*";
		if($type['multi_lang'])
			$q.=",data_lng.*";
		$q.=" FROM `#__content_objects` AS `obj`";
		$q.=" LEFT JOIN `#__content_objects_lng` AS `lng`
			ON lng.record_id=obj.id AND lng.lang_id=".intval($_cfg['sys::lang_id'])."";
		if($type['data'])
		{
			$q.=" LEFT JOIN `#__main_".$type['name_multi']."` AS `data`
				ON data.object_id=obj.id";
		}

		if($type['multi_lang'])
		{
			$q.=" LEFT JOIN `#__main_".$type['name_multi']."_lng` AS `data_lng`
			ON data_lng.record_id=obj.id AND data_lng.lang_id=".intval($_cfg['sys::lang_id'])."";
		}

		$q.=" WHERE obj.id=".intval($obj_id)."";

		$r=$_db->query($q);
		return $_db->fetchAssoc($r);
	}

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $obj
	 */
	static function prepareObject(&$obj, $auto_meta=true)
	{
		global $_meta_keywords,  $_meta_description, $_meta_other;
		if($obj['meta_title'])
			sys::setTitle($obj['meta_title']);
		else
			sys::setTitle($obj['title']);

		if($obj['meta_description'])
			$_meta_description=htmlspecialchars($obj['meta_description']);
		elseif($auto_meta)
			$_meta_description=htmlspecialchars(sys::cutLines(mb_substr(strip_tags($obj['text']),0,255)));

		if($obj['meta_keywords'])
			$_meta_keywords=htmlspecialchars($obj['meta_keywords']);
		elseif($auto_meta)
			$_meta_keywords=htmlspecialchars(sys::cutLines(mb_substr(strip_tags($obj['text']),0,255)));

		if($obj['meta_other'])
			$_meta_other=htmlspecialchars($obj['meta_other']);

		$obj['title']=htmlspecialchars($obj['title']);
	}

	static function getEntryNodeId($entry_id)
	{
		global $_db;
		return $_db->getValue('content_objects','id',"`entry_id`=".intval($entry_id)."");
	}
}
?>