<?
function main_cinema_photo()
{
	sys::useLib('main::cinemas');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('photo_id','int');
	
	if(!$_GET['photo_id'])
		return 404;

		$q="
			SELECT 
				pht.cinema_id,
				pht.image,
				cnm_lng.title AS `cinema`
			FROM `#__main_cinemas_photos` AS `pht`
			LEFT JOIN `#__main_cinemas_lng` AS `cnm_lng`
			ON cnm_lng.record_id=pht.cinema_id
			AND cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE pht.id=".intval($_GET['photo_id'])."
			AND pht.public=1
		";
	
	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);
	
	
	if(!$result)
		return 404;
		
	$meta['cinema']=$result['cinema'];	
	$result['meta']=sys::parseModTpl('main::cinema_photo','page',$meta);
	
	$result['image']=$_cfg['main::cinemas_url'].$result['image'];
	
	return $result;
}
?>