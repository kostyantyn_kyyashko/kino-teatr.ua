<?
const APP_ID = 314473142398393; //App ID/API Key
const APP_SECRET = '2a2815cf73905571ecf4eac0cc9c87b1'; //App Secret
const URL_GET_ME = 'https://graph.facebook.com/me';

function Transliterate($string)
{
  $cyr=array(
     "Щ", "Ш", "Ч","Ц", "Ю", "Я", "Ж","А","Б","В",
     "Г","Д","Е","Ё","З","И","Й","К","Л","М","Н",
     "О","П","Р","С","Т","У","Ф","Х","Ь","Ы","Ъ",
     "Э","Є", "Ї","І",
     "щ", "ш", "ч","ц", "ю", "я", "ж","а","б","в",
     "г","д","е","ё","з","и","й","к","л","м","н",
     "о","п","р","с","т","у","ф","х","ь","ы","ъ",
     "э","є", "ї","і"
  );
  $lat=array(
     "Shch","Sh","Ch","C","Yu","Ya","J","A","B","V",
     "G","D","e","e","Z","I","y","K","L","M","N",
     "O","P","R","S","T","U","F","H","",
     "Y","" ,"E","E","Yi","I",
     "shch","sh","ch","c","yu","ya","j","a","b","v",
     "g","d","e","e","z","i","y","k","l","m","n",
     "o","p","r","s","t","u","f","h",
     "", "y","" ,"e","e","yi","i"
  );
  for($i=0; $i<count($cyr); $i++)  {
     $c_cyr = $cyr[$i];
     $c_lat = $lat[$i];
     $string = str_replace($c_cyr, $c_lat, $string);
  }
  $string =
  	preg_replace(
  		"/([qwrtpsdfghklzxcvbnmQWRTPSDFGHKLZXCVBNM]+)[jJ]e/",
  		"\${1}e", $string);
  $string =
  	preg_replace(
  		"/([qwrtpsdfghklzxcvbnmQWRTPSDFGHKLZXCVBNM]+)[jJ]/",
  		"\${1}'", $string);
  $string = preg_replace("/([eyuioaEYUIOA]+)[Kk]h/", "\${1}h", $string);
  $string = preg_replace("/^kh/", "h", $string);
  $string = preg_replace("/^Kh/", "H", $string);
  return $string;
}

function getUser() 
{    
    $url = URL_GET_ME . '?fields=id,name,email,picture,first_name,last_name,gender&access_token=' . $_POST["token"];    

    if (!($user = @file_get_contents($url))) return false;
    $user = json_decode($user);   
    if (empty($user)) return false;
    return $user;
}

function badResult($msg)
{
	$result["success"] = false;
	$result["msg"] = $msg;
	echo json_encode($result);
}

function DoCheckExistsAccount($user)
{
	global $_db, $_cfg, $_err, $_user, $_cookie;
	$accRec = false;

	$socRec = $_db->getRecord("sys_users_social_network_info", "facebook_id='{$user->id}'");
	if($socRec)
		$accRec = $_db->getRecord("sys_users", $socRec["user_id"]);	
	if($accRec)	
		return $accRec;

	if(isset($user->email) && !empty($user->email))
	{	
		$accRec = $_db->getRecord("sys_users", "email='".$user->email."'");
		if($accRec) return $accRec;
	}		
		
	return $accRec?$accRec:false;	
}

function DoCreateAccount($user)
{
	global $_db, $_cfg, $_err, $_user, $_cookie;
	
	$fname = Transliterate(isset($user->first_name)?$user->first_name:"");
	$lname = Transliterate(isset($user->last_name)?$user->last_name:"");	

	$tfname = strtolower(Transliterate(isset($user->first_name)?$user->first_name:""));
	$tlname = strtolower(Transliterate(isset($user->last_name)?$user->last_name:""));	
	$login = substr($tfname, 0,1).$tlname;
	
	$email = isset($user->email)?$user->email:"";
	$gender = (isset($user->gender) && ($user->gender=="male"))?"man":"woman";
	$np = '';
	
	$count = 2;
	$testLogin = $login;
	
	while( $_db->getRecord("sys_users", "login='".$testLogin."'") )
		$testLogin = $login.($count++);
	$login = $testLogin;	

	$_db->query("INSERT INTO `#__sys_users` (`login`, `password`, `date_reg`, `email`, `on`, `date_last_visit`, `lang_id`) VALUES ('".$login."', '".md5($login)."','".date("Y-m-d H:i:s")."', '$email', '1','".date("Y-m-d H:i:s")."','1')");
	$newid = $_db->last_id;	
	
	if (isset($user->picture) && isset($user->picture->data) && isset($user->picture->data->url))
	{
		$content = file_get_contents($user->picture->data->url);
	
		$fp = fopen("/var/www/html/multiplex/multiplex.in.ua/public/main/users/user_".$newid.".jpg", "w");
		fwrite($fp, $content);
		fclose($fp);
		$np = "user_".$newid.".jpg";
		sys::resizeImage($_cfg['main::users_dir'].$np,false,65,65);
	}   		
	
	$_db->query("INSERT INTO `#__sys_users_cfg` (`user_id`, `sys_timezone`, `main_city_id`) VALUES ('".$newid."','Europe/Kiev','1')");
	$_db->query("INSERT INTO `#__sys_users_data` (`user_id`, `main_nick_name`, `main_first_name`, `main_last_name`, `main_sex`, `main_avatar`)	VALUES ('".$newid."','".$login."','".$fname."','".$lname."','".$gender."','".$np."')");
	$_db->query("INSERT INTO `#__sys_users_groups` (`user_id`, `group_id`) VALUES ('".$newid."','8')	");	
	$_db->query("INSERT INTO `#__sys_users_social_network_info` (user_id, facebook_id) VALUES (".$newid.", ".$user->id.")");
	
	return $_db->getRecord("sys_users", $newid, false);
}

function DoLogin()
{
	global $_db, $_cfg, $_err, $_user, $_cookie;
	$result["success"] = false;
	
    $user = getUser();
    
	if ( $user )
	{
		if( !($userRec = DoCheckExistsAccount($user)) )
			$userRec = DoCreateAccount($user);				
			
		$logged=sys::authorizeUserSoc($userRec["login"]);			

 		if($logged===true)
 		{
 			$_db->query("UPDATE `#__sys_users_social_network_info` SET facebook_id=".$user->id." WHERE user_id=".$userRec["id"]."");			
 			
			$result["success"] = true;				
			echo json_encode($result);
			return;	 		
 		}
		else
			return badResult(sys::translate('main::errAuthErrLogin')); 
	}	
	else 
		return badResult(sys::translate('main::errAuthFB')." ".sys::translate('main::errAuthErrLogin'));
	
	badResult(sys::translate('main::errAuthErrLogin'));	
}

function DoLink()
{
	global $_db, $_cfg, $_err, $_user, $_cookie;
	$result["success"] = false;
    $user = getUser();

	if ( $user )
	{
		$socRec = $_db->getRecord("sys_users_social_network_info", $_user["id"], false, 'user_id');
		
		if(!$socRec) 
			$_db->query("INSERT INTO `#__sys_users_social_network_info` (user_id, facebook_id) VALUES (".$_user["id"].", ".$user->id.")");			
		else 	
			$_db->query("UPDATE `#__sys_users_social_network_info` SET facebook_id=".$user->id." WHERE user_id=".$_user["id"]."");			
			
		$result["success"] = true;				
		$result["data"] = $user;				
		echo json_encode($result);
		return;
	}	
	else 
		return badResult(sys::translate('main::errAuthFB')." ".sys::translate('main::errAuthErrLink'));
	
	badResult(sys::translate('main::errAuthErrLink'));		
}

function DoUnlink()
{
	global $_db, $_cfg, $_err, $_user, $_cookie;
	$_db->query("UPDATE `#__sys_users_social_network_info` SET facebook_id=NULL WHERE user_id=".$_user["id"]."");			
	$result["success"] = false;	
}

function main_ajx_auth_fb()
{
	sys::setTpl();
	global $_db, $_cfg, $_err, $_user, $_cookie;
	$result["success"] = false;
	
	switch($_POST["_task"])
	{
		case "login": doLogin();
					  break;
					  
		case "link":  doLink();
					  break;
					  
		case "unlink": doUnlink();
					   break;
					  
		default:	$result["msg"] = sys::translate('main::errAuthCommon');
					echo json_encode($result);
	}
}

?>