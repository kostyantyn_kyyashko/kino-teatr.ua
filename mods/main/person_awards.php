<?
function main_person_awards()
{
	sys::useLib('main::persons');
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('person_id','int');

	define('_CANONICAL',main_persons::getPersonAwardsUrl($_GET['person_id']),true);

	if(!$_GET['person_id'])
		return 404;

	$result['person']=main_persons::getPersonFio($_GET['person_id']);
	
	if(!$result['person'])
		return 404;

	$meta['person']=$result['person'];
	$result['meta']=sys::parseModTpl('main::person_awards','page',$meta);

	if(!$result['person'])
		return 404;
	$q="
		SELECT
			flm_awd.id,
            flm_awd.film_id,
			flm_awd.year,
			awd_lng.title as nomination,
            flm_lng.title as `film`,
			awd_cat_lng.title AS `title`,
			flm_awd.result
		FROM `#__main_films_awards` AS `flm_awd`

		LEFT JOIN `#__main_awards_categories` AS `awd_cat`
		ON awd_cat.id=flm_awd.award_id

		LEFT JOIN `#__main_awards_categories_lng` AS `awd_cat_lng`
		ON awd_cat_lng.record_id=flm_awd.award_id
		AND awd_cat_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN `#__main_awards_lng` AS `awd_lng`
		ON awd_lng.record_id=awd_cat.award_id
		AND awd_lng.lang_id=".$_cfg['sys::lang_id']."


		LEFT JOIN `#__main_films_awards_persons` AS `awd_prs`
		ON awd_prs.film_award_id=flm_awd.id		

		LEFT JOIN `#__main_films_lng` AS `flm_lng`
		ON flm_lng.record_id=flm_awd.film_id 
        AND flm_lng.lang_id=".$_cfg['sys::lang_id']."

		WHERE awd_prs.person_id=".intval($_GET['person_id'])."
		ORDER BY 
        	flm_awd.year DESC, 
            awd_lng.title,
            flm_lng.title,
            awd_cat_lng.title
        ";
	
	$r=$_db->query($q);

	$result['objects']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
		$result['objects'][]=$obj;
	}
	if(!$result['objects'])
		return 404;

	main::countShow($_GET['person_id'],'person');

	return $result;
}
?>