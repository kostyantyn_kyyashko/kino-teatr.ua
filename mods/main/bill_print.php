<?
function main_bill_print()
{

	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('main::countries');
	sys::useLib('sys::form');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	define('_CANONICAL',$_cfg['sys::root_url'].$_cfg['sys::lang'].'/main/bill/order/cinemas/date/'.$_GET['date'].'.phtml');
	sys::filterGet('date','text',date('d.m.Y'));
	sys::filterGet('order','text','cinemas');
	sys::filterGet('city_id','int',$_cfg['main::city_id']);

	$cache_name='main_billboard_print_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::billboard_cache_period']*_HOUR))
		return unserialize($cache);

	$meta['city']=$result['city']=main_countries::getCityTitle($_GET['city_id'],'what');
	$meta['date']=$result['date']=strftime('%d %B',sys::date2Timestamp($_GET['date'],'%d.%m.%Y'));
	$result['meta']=sys::parseModTpl('main::billboard_print','page',$meta);
	if(!$result['city'])
		return 404;

	$result['objects']=main_shows::getBillBoard($_GET['city_id'],$_GET['date'], $_GET['order']);

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>