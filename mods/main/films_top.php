<?


function main_films_top()
{
	global $_db, $_cfg, $_err;
	sys::useLib('main::genres');
	sys::useLib('main::countries');
	sys::useLib('sys::form');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	sys::filterGet('title');
	sys::filterGet('genre_id');
	sys::filterGet('year');
	sys::filterGet('letter');
	sys::filterGet('city_id','int',$_cfg['main::city_id']);
	sys::filterGet('show');
	sys::filterGet('order_by','text','title.asc');

	if ($_GET['page']!='' || $_GET['title']!='' || $_GET['genre_id']!='' || $_GET['year']!='' || $_GET['city_id']!='')
	{
		define('_NOINDEX',1,true);
	}


		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		define('_CANONICAL',_ROOT_URL.'films.phtml',true);
		} else {
		define('_CANONICAL',_ROOT_URL.$lang.'/films.phtml',true);
		}


	$cache_name='main_films_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::films_page_cache_period']*60))
		return unserialize($cache);

	$result=array();

	if ($_GET['year'])
	{
		$meta['year']=$_GET['year'].' '.sys::translate('main::ofyear');
	} else {
		$meta['year']= '';
	}

	if ($_GET['letter'])
	{
		$meta['letter']=sys::translate('main::ofletter').' '.$_GET['letter'];
	} else {
		$meta['letter']= '';
	}

	if ($_GET['page'])
	{
		$meta['page'] = sys::translate('main::page').' '.$_GET['page'];
	} else {
		$meta['page'] = '';
	}

	if ($_GET['genre_id'])
	{
		$meta['genre']=sys::translate('main::ofgenre').' '.main_films::getGenreTitle($_GET['genre_id']);
	} else {
		$meta['genre']= '';
	}



	$result['meta']=sys::parseModTpl('main::filmsRating','page', $meta);


	//=====================================================

	//Поля поисковой формы-------------------------------------
	$result['fields']['title']['input']='textbox';
	$result['fields']['title']['value']=urldecode($_GET['title']);
	$result['fields']['title']['title']=sys::translate('main::title');

	$result['fields']['genre_id']['input']='selectbox';
	$result['fields']['genre_id']['value']=$_GET['genre_id'];
	$result['fields']['genre_id']['empty']=sys::translate('main::all_genres');
	$result['fields']['genre_id']['values']=main_genres::getGenresTitles();
	$result['fields']['genre_id']['title']=sys::translate('main::genre');

	$result['fields']['year']['input']='selectbox';
	$result['fields']['year']['value']=$_GET['year'];
	$result['fields']['year']['empty']=sys::translate('main::all_years');
	$result['fields']['year']['values']=main::getYears();
	$result['fields']['year']['title']=sys::translate('main::year');

	$result['fields']['city_id']['input']='selectbox';
	$result['fields']['city_id']['value']=$_GET['city_id'];
	$result['fields']['city_id']['empty']=sys::translate('main::all_cities');
	$result['fields']['city_id']['values']=main_countries::getCountryCitiesTitles(29);
	$result['fields']['city_id']['title']=sys::translate('main::city');

	$result['fields']['show']['input']='checkbox';
	$result['fields']['show']['value']=$_GET['show'];
	$result['fields']['show']['title']=sys::translate('main::only_showing');
	//===================================================================

	//Формирование запроса-----------------------------------------------
	$q="
		SELECT
			flm.id,
			flm.title_orig,
			lng.title,
			flm.year,
			flm.rating,
			flm.votes,
			flm.pro_rating,
			flm.pro_votes
	";

	//Жанр
	if($_GET['genre_id'])
		$q.=" ,gnr.genre_id ";


	//Город
	if($_GET['city_id'])
		$q.=" ,cnm.city_id ";


	$q.="
		FROM `#__main_films` AS `flm`
		LEFT JOIN `#__main_films_lng` AS `lng`
		ON lng.record_id=flm.id
		AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	//Жанр
	if($_GET['genre_id'])
	{
		$q.="LEFT JOIN `#__main_films_genres` AS `gnr`
		ON gnr.film_id=flm.id AND gnr.genre_id=".intval($_GET['genre_id'])."";
	}

	//Город
	if($_GET['city_id'])
	{
		$q.="
		LEFT JOIN `#__main_shows` AS `shw`
		ON shw.film_id=flm.id
		LEFT JOIN `#__main_cinemas_halls` AS `hls`
		ON hls.id=shw.hall_id
		LEFT JOIN `#__main_cinemas` AS `cnm`
		ON cnm.id=hls.cinema_id AND cnm.city_id=".intval($_GET['city_id'])."
		";
	}

	if($_GET['show'] && !$_GET['city_id'])
	{
		$q.="
		LEFT JOIN `#__main_shows` AS `shw`
		ON shw.film_id=flm.id
		";
	}

	//Только в прокате
	if($_GET['show'] && !$_GET['city_id'])
	{

	}

	//Условия
	$q.=" WHERE votes>=5";

	if($_GET['title'])
	{
		$q.=" AND (lng.title LIKE '%".mysql_real_escape_string(urldecode($_GET['title']))."%'
			OR flm.title_orig LIKE '%".mysql_real_escape_string(urldecode($_GET['title']))."%')";
	}

	if($_GET['year'])
	{
		$q.=" AND flm.year='".intval($_GET['year'])."'";
	}

	if($_GET['letter'])
	{
		$q.=" AND (lng.title LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%'
			OR flm.title_orig LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%')";
	}

	if($_GET['genre_id'])
	{
		$q.=" AND gnr.genre_id=".intval($_GET['genre_id'])."";
	}

	if($_GET['show'])
	{
		$q.=" AND cnm.city_id=".intval($_GET['city_id'])." AND (shw.begin<='".date('Y-m-d')."' AND shw.end>='".date('Y-m-d')."')";
	}

	$q.="
		GROUP BY flm.id
		ORDER BY flm.rating DESC, flm.votes DESC
		LIMIT 100
	";
	//===========================================================


	$r=$_db->query($q);
	$result['objects']=array();


	$i = 1;



	while($obj=$_db->fetchAssoc($r))
	{


		$qw="
			SELECT
				image
			FROM `#__main_films_posters`
			WHERE film_id='".intval($obj['id'])."'
			AND public=1
			AND order_number=1
			LIMIT 0,1

		";

		$image = '';

		$rw=$_db->query($qw);
		$res=$_db->fetchAssoc($rw);
		if($res) $image=$res['image'];
		if ($image)
		{
	  	    $poster="x2_".$image;	
			if(!file_exists($_cfg['main::films_dir'].$poster)) $poster = 'blank_news_img.jpg';
			 else $poster=$_cfg['main::films_url'].$poster;
			$obj['poster'] = $poster;
		} else {
			$obj['poster'] = $_cfg['sys::root_url'].'blank_news_img.jpg';
		}

		if ($obj['rating']>0)
			$obj['rate'] = round($obj['rating'], 2);
		else 
			$obj['rate'] = 0;

		$obj['url']=main_films::getFilmUrl($obj['id']);
		$obj['place']=$i;
		if(main_films::isFilmTrailers($obj['id']))
		{
			$obj['trailers']=main_films::getFilmTrailersUrl($obj['id']);
		}
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['rating']=main_films::formatFilmRating($obj['rating']);
		$result['objects'][]=$obj;
		$i++;
	}

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>