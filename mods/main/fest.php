<?
function main_fest()
{

	sys::useLib('main::fest');
	sys::useLib('main::films');
	sys::useLib('main::news');
	sys::useLib('main::users');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('fest_id','int');



	if(!$_GET['article_id'])
		return 404;



	$r=$_db->query("
		SELECT
			art.date_w_s,
			art.fest_id,
			art.comments,
			art.public,
			flm.title,
			fl.year,
			pst.image as `poster`,
			art.name as `film_id`,
			MONTH(art.date_w_s) AS `month`,
			fst.title as `festival`,
			fest.image as image
		FROM `#__main_fest_articles` AS `art`

		LEFT JOIN
			`#__main_films_lng` AS `flm`
		ON
			art.name = flm.record_id
		AND
			flm.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN
			`#__main_films_posters` AS `pst`
		ON
			art.name = pst.film_id

		LEFT JOIN
			`#__main_films` AS `fl`
		ON
			art.name = fl.id

		LEFT JOIN
			`#__main_fest_lng` AS `fst`
		ON
			art.fest_id = fst.record_id
		AND
			flm.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN
			`#__main_fest` AS `fest`
		ON
			art.fest_id = fest.id

		WHERE art.fest_id=".intval($_GET['article_id'])."
		 ORDER BY RAND()
	");



	while($obj=$_db->fetchAssoc($r))
	{
		$festival = $obj['festival'];
		$id='';		$id = date('dmY', strtotime($obj['date_w_s']));
		$obj['time'] = date('H:i', strtotime($obj['date_w_s']));

		$obj['film_url']=main_films::getFilmUrl($obj['film_id']);

		$poster=main_films::getFilmFirstPoster($obj['film_id']);


			if($poster['image'])
			{
				$image=$_cfg['main::films_url'].$poster['image'];
			} else {
	         	$image = 'fest_img.jpg';
			}

			$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=25';
			if (main_films::isFilmTrailers($obj['film_id']))
			$obj['trailers']=main_films::getFilmTrailersUrl($obj['film_id']);


				if (count($result['films'][$id])<9 && !is_array($result['films'][$id][$obj['film_id']]))
				{
					$result['films'][$id][$obj['film_id']]=$obj;
				}



				$result['films'][$id]['day']=date('d', strtotime($obj['date_w_s']));

				$result['films'][$id]['festid']=$obj['fest_id'];



				$result['films'][$id]['dayid']=date('d-m-Y', strtotime($obj['date_w_s']));


				$result['films'][$id]['month']=sys::translate('main::month_'.$obj['month'].'_a');

//				$result['films'][$id]['month']=date('m', strtotime($obj['date_w_s']));
	}


	$r=$_db->query("
		SELECT
			news_id
		FROM `#__main_fest_news`

		WHERE fest_id=".intval($_GET['article_id'])."
	");


	while($obj=$_db->fetchAssoc($r))
	{
		$news_id = $obj['news_id'];
	}



	$q=$_db->query("
		SELECT
			art.id,
			art.date,
			art.user_id,
			art.total_shows,
			art.comments,
			art.exclusive,
			art.small_image AS `image`,
			art.image AS `big_image`,
			art_lng.title,
			art_lng.intro
		FROM `#__main_news_articles` AS `art`

		LEFT JOIN
			`#__main_news_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_news` AS `nws`
		ON art.news_id=nws.id

		WHERE
			art.news_id = '".$news_id."'
			AND art.public=1

		ORDER BY art.date DESC

	");




	while($obj=$_db->fetchAssoc($q))
	{
		$id='';
		$id = date('d-m-Y', strtotime($obj['date']));

		$obj['date']=sys::russianDate($obj['date']);
		$obj['url']=main_news::getArticleUrl($obj['id']);
		$obj['shows']=$obj['total_shows'];

		if($obj['image'])
			$image=$_cfg['main::news_url'].$obj['image'];


		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));


		if ($wid<213 && $obj['big_image'])
		{
		 	$image = $_cfg['main::news_url'].$obj['big_image'];
		} else if ($wid>=213)
		{
		 	$image = $_cfg['main::news_url'].$obj['image'];
		} else {
         	$image = 'blank_news_img.jpg';
		}

		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';



		if (count($result['news'][$id])<5)
			{
				$result['news'][$id][]=$obj;
			}

		$result['news'][$id]['day']=date('d', strtotime($obj['date']));

		$result['news'][$id]['festid']=intval($_GET['article_id']);

		$result['news'][$id]['dayid']=date('d-m-Y', strtotime($obj['date']));

//				$result['films'][$id]['month']=date('m', strtotime($obj['date_w_s']));
	}


//print_r ($result);

ksort($result['films']);



	if(!$result)
		return 404;

	$result['title']='Фестиваль "'.$festival.'"';
	$meta['article']=$result['title'];

	$result['meta']=sys::parseModTpl('main::fest_article','page',$meta);
	$result['festival'] = $festival;
	if($result['image'])
	{
		$size=getimagesize($_cfg['main::fest_dir'].$result['image']);
		$result['width']=$size[0];
		$result['image']=$_cfg['main::fest_url'].$result['image'];
	}
	$result['alt']=htmlspecialchars($result['alt']);
	$result['source']=htmlspecialchars($result['source']);

	$date=sys::db2Timestamp($result['date']);
	$result['year']=date('Y',$date);
	$result['month']=date('m',$date);

	$result['date']=sys::db2Date($result['date'],false,$_cfg['sys::date_format']);
	$result['user_url']=main_users::getUserUrl($result['user_id']);



	return $result;


}
?>