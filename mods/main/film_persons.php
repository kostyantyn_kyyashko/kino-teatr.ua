<?
function main_film_persons()
{
	sys::useLib('main::films');
	sys::useLib('main::persons');
	sys::useLib('main::users');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');

	define('_CANONICAL',main_films::getFilmPersonsUrl($_GET['film_id']),true);

	if(!$_GET['film_id'])
		return 404;

	$result['film']=$_db->getValue('main_films','title',intval($_GET['film_id']),true);
	$result['serial']=$_db->getValue('main_films','serial',intval($_GET['film_id']),false);
	$meta['film']=$result['film'];
	$result['meta']=sys::parseModTpl('main::film_persons','page',$meta);
	if(!$result['film'])
		return 404;

	main::countShow($_GET['film_id'],'film');

	$r=$_db->query("
		SELECT
			CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`,
			prs_pht.image,
			flm_prs.person_id AS `id`,
			prf_lng.titles AS `profession`,
			flm_prs_lng.role,
			flm_prs.profession_id
		FROM `#__main_films_persons` AS `flm_prs`
		
		LEFT JOIN `#__main_persons_lng` AS `prs_lng`
		ON prs_lng.record_id=flm_prs.person_id
		AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_films_persons_lng` AS `flm_prs_lng`
		ON flm_prs_lng.record_id=flm_prs.id
		AND flm_prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_persons_photos` AS `prs_pht`
		ON prs_pht.person_id=flm_prs.person_id
		AND prs_pht.order_number=1

		LEFT JOIN `#__main_professions` AS `prf`
		ON prf.id=flm_prs.profession_id

		LEFT JOIN `#__main_professions_lng` AS `prf_lng`
		ON prf_lng.record_id=flm_prs.profession_id
		AND prf_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE flm_prs.film_id=".intval($_GET['film_id'])."
		ORDER BY prf.order_number, flm_prs.order_number
	");
	$result['profs']=array();
	$result['objects']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['url']=main_persons::getPersonUrl($obj['id']);


		// inserted by Mike begin
		if($obj['image'])
		{
    	$image="x2_".$obj['image']; 
		if(!file_exists('./public/main/persons/'.$image)) $image = $_cfg['sys::root_url'].'images/userpic.jpg';
		 else $image=$_cfg['main::persons_url'].$image;
		} else {
         	$image = $_cfg['sys::root_url'].'images/userpic.jpg';
		}
		$obj['image'] = $image;
		// inserted by Mike end

//				if($obj['image'])
//				{
//		         	$image = $_cfg['main::persons_url'].$obj['image'];
//					$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=25';
//		        } else {
//					$obj['image'] = '//kino-teatr.ua/images/userpic.jpg';
//		        }






		$result['profs'][$obj['profession_id']]=$obj['profession'];
		$result['objects'][]=$obj;
	}
	return $result;
}
?>