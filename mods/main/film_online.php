<?
function main_film_online()
{
	sys::useLib('main::films');
	sys::useLib('main::reviews');
	sys::useLib('main::users');
	sys::useLib('main::discuss');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('film_id','int');
	sys::filterGet('city_id','int',$_cfg['main::city_id']);

	define('_CANONICAL',main_films::getFilmUrl($_GET['film_id']),true);

	if(!$_GET['film_id'])
		return 404;





	$r=$_db->query("
		SELECT
			flm.title_orig,
			flm.megogo_id,
			flm.title_alt,
			flm_lng.title,
			flm.year,
			flm.budget,
			flm.budget_currency,
			flm.duration,
			flm.ukraine_premiere,
			flm.world_premiere,
			flm.yakaboo_url,
			flm.age_limit,
			flm_lng.intro,
			flm_lng.text,
			flm.pre_rating,
			flm.pre_votes,
			flm.rating,
			flm.votes,
			flm.pro_rating,
			flm.pro_votes,
			flm.comments AS `comments_num`,
			flm.banner,
			flm.serial,
			trk.film_id AS `tracking`
		FROM `#__main_films` AS `flm`

		LEFT JOIN `#__main_films_lng` AS `flm_lng`
		ON flm_lng.record_id=flm.id
		AND flm_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN `#__main_users_films_subscribe` AS `trk`
		ON trk.film_id=".intval($_GET['film_id'])."
		AND trk.user_id=".intval($_user['id'])."

		WHERE flm.id=".intval($_GET['film_id'])."
	");

	$result=$_db->fetchAssoc($r);

	if(!$result)
		return 404;

	main::countShow($_GET['film_id'],'film');

	$meta['film']=$result['title'];
	$meta['year']=$result['year'];
	$result['meta']=sys::parseModTpl('main::film_online','page',$meta);

	$result['title']=htmlspecialchars($result['title']);

	$titles = array();

	$result['title_orig']=htmlspecialchars($result['title_orig']);
	$result['title_alt']=htmlspecialchars($result['title_alt']);

	$poster=main_films::getFilmFirstPoster($_GET['film_id']);


		if($poster)
		{
			$image=$_cfg['main::films_url'].$poster['image'];
			$result['poster']['url']=main_films::getFilmPostersUrl($_GET['film_id']);
		} else {
         	$image = 'blank_news_img.jpg';
		}

		$result['poster']['image'] = _ROOT_URL.'public/main/rescrop_4.php?f='.str_replace(_ROOT_URL,'',$image).'&t=5';


			if(main_films::isFilmTrailers($_GET['film_id']))
					{

     		$q="
			SELECT
				trl.film_id,
				trl.id AS `trailer_id`,
				trl.image,
				trl.file,
				trl.url,
				trl.user_id,
				trl.shows,
				trl.width,
				trl.height,
				trl.order_number,
				trl.public,
				flm_lng.title AS `film`,
				flm_pst.image as `poster`
			FROM `#__main_films_trailers` AS `trl`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=trl.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			LEFT JOIN `#__main_films_photos` AS `flm_pst`
			ON flm_pst.film_id=trl.film_id
			WHERE trl.film_id='".intval($_GET['film_id'])."'
			AND trl.public=1
			ORDER BY trl.order_number DESC
			LIMIT 1
		";


	$t=$_db->query($q);
	$trails=$_db->fetchAssoc($t);

	$result['file'] = _ROOT_URL.'public/main/films/'.$trails['file'];
	$result['image'] = $trails['image'];

	$result['trailer']=true;
			}

	$genres = main_films::getFilmGenresTitlesIds($_GET['film_id']);

	$gnr = array();

	foreach ($genres as $genr)
	{
		$gnr[] = "<a href='".sys::rewriteUrl('?mod=main&act=films&genre_id='.$genr['id'])."'  title='".sys::translate('main::films')." - ".$genr['title']."'>".$genr['title']."</a>";
	}

	$countries = main_films::getFilmCountriesTitlesIds($_GET['film_id']);

	$cnt = array();

	foreach ($countries as $country)
	{
		$cnt[] = "<a href='".sys::rewriteUrl('?mod=main&act=films&country='.$country['id'])."' title='".sys::translate('main::films')." ".$country['title']."'>".$country['title']."</a>";
	}


	$result['genres'] = $gnr;
	$result['countries']=$cnt;
	$result['directors']=main_films::getFilmDirectors($_GET['film_id']);
	$result['actors']=main_films::getFilmActors($_GET['film_id']);
	$result['studios']=main_films::getFilmStudiosTitles($_GET['film_id']);
	$result['links']=main_films::getFilmLinks($_GET['film_id']);
	$result['rating']=main_films::formatFilmRating($result['rating']);
	$result['pre_rating']=main_films::formatFilmRating($result['pre_rating']);
	$result['pro_rating']=main_films::formatFilmRating($result['pro_rating']);
	$result['rating_url']=main_films::getFilmRatingUrl($_GET['film_id']);
	$result['discuss_url']=main_films::getFilmDiscussUrl($_GET['film_id']);
	$result['reviews_url']=main_films::getFilmReviewsUrl($_GET['film_id']);
	$result['imdb_url']=main_films::getFilmImdbUrl($result['title_orig']);
	$result['persons_url']=main_films::getFilmPersonsUrl($_GET['film_id']);


//	if ($_GET['city_id']=='1')
//	{
//		$ro=$_db->query("
//			SELECT
//				count(*) as count
//			FROM `#__main_shows`
//
//			WHERE
//				hall_id='215'
//			AND
//				film_id='".$_GET['film_id']."'
//			AND
//				begin<='".date('Y-m-d')."'
//			AND
//				end>='".date('Y-m-d')."'
//		");
//
//		$imax = $_db->fetchAssoc($ro);
//
//		if ($imax['count'])
//		{
//			$result['imax'] = 1;
//			$result['imaxcinema'] = 135;
//			$result['imaxlabel'] = 'imax';
//		}
//	}

//	if ($_GET['city_id']=='9')
//	{
//		$ro=$_db->query("
//			SELECT
//				count(*) as count,
//				hall_id
//			FROM `#__main_shows`
//
//			WHERE
//				(hall_id='356'
//			OR
//				hall_id='357'
//			OR
//				hall_id='358'
//			OR
//				hall_id='359'
//			OR
//				hall_id='360'
//			OR
//				hall_id='361'
//			OR
//				hall_id='362'
//				)
//			AND
//				begin<='".date('Y-m-d')."'
//			AND
//				end>='".date('Y-m-d')."'
//			AND
//				film_id='".$_GET['film_id']."'
//		");
//
//		$imax = $_db->fetchAssoc($ro);
//
//		if ($imax['count'])
//		{
//			$result['imax'] = 1;
//			$result['imaxcinema'] = 207;
//			$result['imaxlabel'] = 'planeta';
//		}
//	}

//	if ($_GET['city_id']=='4')
//	{
//		$ro=$_db->query("
//			SELECT
//				count(*) as count,
//				hall_id
//			FROM `#__main_shows`
//
//			WHERE
//				(hall_id='331'
//			OR
//				hall_id='332'
//			OR
//				hall_id='333'
//			OR
//				hall_id='334'
//			OR
//				hall_id='335'
//			OR
//				hall_id='336'
//			OR
//				hall_id='337'
//			OR
//				hall_id='338'
//			OR
//				hall_id='339'
//				)
//			AND
//				begin<='".date('Y-m-d')."'
//			AND
//				end>='".date('Y-m-d')."'
//			AND
//				film_id='".$_GET['film_id']."'
//		");
//
//		$imax = $_db->fetchAssoc($ro);
//
//		if ($imax['count'])
//		{
//			$result['imax'] = 1;
//			$result['imaxcinema'] = 197;
//			$result['imaxlabel'] = 'planeta';
//		}
//	}


	$result['intro'] = eregi_replace("<div[^>]*>", "<div>", $result['intro']);
	$result['intro'] = eregi_replace("<b[^>]*>", "<b>", $result['intro']);
	$result['intro'] = eregi_replace("<strong[^>]*>", "<strong>", $result['intro']);
	$result['intro'] = eregi_replace("<span[^>]*>", "", $result['intro']);
	$result['intro'] = eregi_replace("</span>", "", $result['intro']);
	$search = array ("'<span[^>]*?>.*?</span>'si");                    // интерпретировать как php-код
	$replace = array ("");
	$result['intro'] = eregi_replace($search, $replace, $result['intro']);
	$result['intro'] = str_replace("<div>&nbsp;</div>","",$result['intro']);
	$result['intro'] = str_replace('<div >&nbsp;</div>',"",$result['intro']);
	$result['intro'] = str_replace("<p>&nbsp;</p>","",$result['intro']);

	if($result['budget'])
		$result['budget']=$result['budget'].' '.sys::translate('main::mil').' '.sys::translate('main::'.$result['budget_currency']);
	if($result['ukraine_premiere']!='0000-00-00')
		$result['ukraine_premiere']=sys::db2Date($result['ukraine_premiere'],false,$_cfg['sys::date_format']);
	else
		$result['ukraine_premiere']=false;

	if($result['world_premiere']!='0000-00-00')
		$result['world_premiere']=sys::db2Date($result['world_premiere'],false,$_cfg['sys::date_format']);
	else
		$result['world_premiere']=false;

	if($result['text'])
		$result['text_url']=main_films::getFilmTextUrl($_GET['film_id']);
	else
		$result['text_url']=false;


	//Отслеживание--------------------------------------------
	if(isset($_POST['cmd_tracking']))
	{
		main_films::trackingFilm($_GET['film_id']);
		sys::redirect('?mod=main&act=film&film_id='.$_GET['film_id']);
	}
	if(isset($_POST['cmd_untracking']))
	{
		main_films::untrackingFilm($_GET['film_id']);
		sys::redirect('?mod=main&act=film&film_id='.$_GET['film_id']);
	}
	//============================================================


	//Рецензии------------------------------------
	$r=$_db->query("
		SELECT
			rev.id,
			rev.title,
			rev.intro,
			rev.mark,
			rev.date,
			rev.image,
			usr.login AS `user`,
			rev.user_id
		FROM
			`#__main_reviews` AS `rev`
		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=rev.user_id
		WHERE rev.film_id=".intval($_GET['film_id'])."
		AND rev.public=1
		AND rev.date<'".gmdate('Y-m-d H:i:s')."'
		ORDER BY rev.date DESC
		LIMIT 2
	");
	$result['reviews']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['date']=sys::russianDate($obj['date']);
		$obj['url']=main_reviews::getReviewUrl($obj['id']);
		$obj['user_url']=main_users::getUserReviewsUrl($obj['user_id']);

		if($obj['image'])
		{
			$image=$_cfg['main::reviews_url'].$obj['image'];
        } else {
         	$image = 'blank_news_img.jpg';
		}
			$obj['image'] = _ROOT_URL.'public/main/rescrop_4.php?f='.str_replace(_ROOT_URL,'',$image).'&t=4';


		$result['reviews'][]=$obj;
	}
	//================================================


	//Комментарии--------------------------------------------
	$q="
		SELECT
			msg.id,
			msg.date,
			msg.user_id,
			msg.user_name,
			msg.rating,
			usr.login AS `user`,
			msg.text,
			grp.group_id AS `star`,
			data.main_avatar AS `avatar`

		FROM `#__main_discuss_messages` AS `msg`

		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=msg.user_id

		LEFT JOIN `#__sys_users_data` AS `data`
		ON data.user_id=msg.user_id

		LEFT JOIN `#__sys_users_groups` as grp
		ON grp.user_id=msg.user_id
		AND grp.group_id=23

		WHERE msg.object_id='".intval($_GET['film_id'])."'
		AND msg.object_type='film'

		ORDER BY msg.date DESC

		LIMIT 0,5
	";

	$r=$_db->query($q);
	$result['comments']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true);
		if($obj['user_id']==2)
		{
			$obj['user']=$obj['user_name'];
			$obj['user_url']=false;
		}
		else
			$obj['user_url']=main_users::getUserUrl($obj['user_id']);
		$obj['bb_text']=sys::parseBBCode($obj['text']);
		$result['comments'][]=$obj;
	}

	//====================================================


	return $result;
}
?>