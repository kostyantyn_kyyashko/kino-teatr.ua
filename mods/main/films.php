<?
function main_films()
{
	global $_db, $_cfg, $_err;
	sys::useLib('main::genres');
	sys::useLib('main::countries');
	sys::useLib('sys::form');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	sys::filterGet('title');
	sys::filterGet('genre_id');
	sys::filterGet('uap');
	sys::filterGet('wop');
	sys::filterGet('year');
	sys::filterGet('country');
	sys::filterGet('letter');
	sys::filterGet('city_id','int',$_cfg['main::city_id']);
	sys::filterGet('show');
	sys::filterGet('order_by','text','title.asc');

	if ($_GET['uap'])
	{
		$uap = date('Y-m-d', strtotime($_GET['uap']));
	}

	if ($_GET['wop'])
	{
		$wop = date('Y-m-d', strtotime($_GET['wop']));
	}

	if ($_GET['page']!='' || $_GET['title']!='' || $_GET['genre_id']!='' || $_GET['year']!='' || $_GET['city_id']!='')
	{
		define('_NOINDEX',1,true);
	}


		$lang=$_cfg['sys::lang'];


	if ($_GET['page'] && !$_GET['genre_id'] && !$_GET['year'])
	{
		if ($lang=='ru')
		{
			define('_CANONIC', _ROOT_URL.'films.phtml');
		} else {
			define('_CANONIC', _ROOT_URL.'uk/films.phtml');
		}
	} else if ($_GET['genre_id'] && !$_GET['year'])
	{
		if ($lang=='ru')
		{
			define('_CANONIC', _ROOT_URL.'ru/main/films/genre_id/'.$_GET['genre_id'].'.phtml');
		} else {
			define('_CANONIC', _ROOT_URL.'uk/main/films/genre_id/'.$_GET['genre_id'].'.phtml');
		}
	} else if ($_GET['genre_id'] && $_GET['year'])
	{
		if ($lang=='ru')
		{
			define('_CANONIC', _ROOT_URL.'ru/main/films/genre_id/'.$_GET['genre_id'].'/year/'.$_GET['year'].'.phtml');
		} else {
			define('_CANONIC', _ROOT_URL.'uk/main/films/genre_id/'.$_GET['genre_id'].'/year/'.$_GET['year'].'.phtml');
		}
	}


	$cache_name='main_films_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::films_page_cache_period']*60))
		return unserialize($cache);

	$result=array();

	$meta['letter']=mysql_real_escape_string(urldecode($_GET['letter']));
	$meta['page']=$_GET['page'];
	$meta['year']=$_GET['year'];
	$meta['genre']=main_films::getGenreTitle($_GET['genre_id']);

	if ($_GET['genre_id']!=16)
	{

		if(!$_GET['letter'] && !$_GET['page'] && !$_GET['genre_id'] && !$_GET['year'])
		{
			$result['meta']=sys::parseModTpl('main::films','page',$meta);
		} else if ($_GET['letter'] && !$_GET['page'] && !$_GET['genre_id'] && !$_GET['year']) {
			$result['meta']=sys::parseModTpl('main::films_letter','page',$meta);
		} else if ($_GET['letter'] && $_GET['page'] && !$_GET['genre_id'] && !$_GET['year']) {
			$result['meta']=sys::parseModTpl('main::films_letter_page','page',$meta);
		} else if (!$_GET['letter'] && $_GET['page'] && !$_GET['genre_id'] && !$_GET['year']) {
			$result['meta']=sys::parseModTpl('main::films_page','page',$meta);
		}


		if ($_GET['year'] && !$_GET['page'] && !$_GET['genre_id'])
		{
			$result['meta']=sys::parseModTpl('main::films_year','page',$meta);
		} else if ($_GET['year'] && $_GET['page'] && !$_GET['genre_id'])
		{
			$result['meta']=sys::parseModTpl('main::films_year_page','page',$meta);
		} else if (!$_GET['year'] && !$_GET['page'] && $_GET['genre_id'])
		{
			$result['meta']=sys::parseModTpl('main::films_genre','page',$meta);
		} else if (!$_GET['year'] && $_GET['page'] && $_GET['genre_id'])
		{
			$result['meta']=sys::parseModTpl('main::films_genre_page','page',$meta);
		} else if ($_GET['year'] && !$_GET['page'] && $_GET['genre_id'])
		{
			$result['meta']=sys::parseModTpl('main::films_genre_year','page',$meta);
		} else if ($_GET['year'] && $_GET['page'] && $_GET['genre_id'])
		{
			$result['meta']=sys::parseModTpl('main::films_genre_year_page','page',$meta);
		}

	} else {

		if ($_GET['page'])
		{
			$meta['page'] = sys::translate('main::page').' '.$_GET['page'];
		} else {
			$meta['page'] = '';
		}

		if ($_GET['year'])
		{
			$meta['year']=$_GET['year'].' '.sys::translate('main::ofyear');
		} else {
			$meta['year']= '';
		}



			$result['meta']=sys::parseModTpl('main::mults','page',$meta);
	}



	//Определение направления сортировки
	$order_by=explode('.',$_GET['order_by']);
	$order_fields=array('title','year','rating','pro_rating');
	if($order_by[1]=='asc')
	{
		$result['order']='asc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.desc');
		}

	}
	else
	{
		$result['order']='desc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.asc');
		}
	}
	$result['order_field']=$order_by[0];

	$order_by=sys::parseOrderBy($_GET['order_by']);
	//=====================================================

	//Поля поисковой формы-------------------------------------
	$result['fields']['title']['input']='textbox';
	$result['fields']['title']['value']=urldecode($_GET['title']);
	$result['fields']['title']['title']=sys::translate('main::title');

	$result['fields']['genre_id']['input']='selectbox';
	$result['fields']['genre_id']['value']=$_GET['genre_id'];
	$result['fields']['genre_id']['empty']=sys::translate('main::all_genres');
	$result['fields']['genre_id']['values']=main_genres::getGenresTitles();
	$result['fields']['genre_id']['title']=sys::translate('main::genre');

	$result['fields']['year']['input']='selectbox';
	$result['fields']['year']['value']=$_GET['year'];
	$result['fields']['year']['empty']=sys::translate('main::all_years');
	$result['fields']['year']['values']=main::getYears();
	$result['fields']['year']['title']=sys::translate('main::year');

	$result['fields']['city_id']['input']='selectbox';
	$result['fields']['city_id']['value']=$_GET['city_id'];
	$result['fields']['city_id']['empty']=sys::translate('main::all_cities');
	$result['fields']['city_id']['values']=main_countries::getCountryCitiesTitles(29);
	$result['fields']['city_id']['title']=sys::translate('main::city');

	$result['fields']['show']['input']='checkbox';
	$result['fields']['show']['value']=$_GET['show'];
	$result['fields']['show']['title']=sys::translate('main::only_showing');
	//===================================================================

	//Формирование запроса-----------------------------------------------
	$q="
		SELECT
			flm.id,
			flm.title_orig,
			lng.title,
			flm.year,
			flm.rating,
			flm.votes,
			flm.pre_votes,
			flm.pro_rating,
			flm.pro_votes
	";

	//Жанр
	if($_GET['genre_id'])
		$q.=" ,gnr.genre_id ";


	//Город
	if($_GET['city_id'])
		$q.=" ,cnm.city_id ";


	$q.="
		FROM `#__main_films` AS `flm`
		LEFT JOIN `#__main_films_lng` AS `lng`
		ON lng.record_id=flm.id
		AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	//Жанр
	if($_GET['genre_id'])
	{
		$q.="LEFT JOIN `#__main_films_genres` AS `gnr`
		ON gnr.film_id=flm.id AND gnr.genre_id=".intval($_GET['genre_id'])."";
	}

	//Страна
	if($_GET['country'])
	{
		$q.="LEFT JOIN `#__main_films_countries` AS `cnt`
		ON cnt.film_id=flm.id AND cnt.country_id=".intval($_GET['country'])."";
	}

	//Город
	if($_GET['city_id'])
	{
		$q.="
		LEFT JOIN `#__main_shows` AS `shw`
		ON shw.film_id=flm.id
		LEFT JOIN `#__main_cinemas_halls` AS `hls`
		ON hls.id=shw.hall_id
		LEFT JOIN `#__main_cinemas` AS `cnm`
		ON cnm.id=hls.cinema_id AND cnm.city_id=".intval($_GET['city_id'])."
		";
	}

	//Только в прокате
	if($_GET['show'] && !$_GET['city_id'])
	{

	}

	//Условия
	$q.=" WHERE 1=1";

	if($_GET['title'])
	{
		$q.=" AND (lng.title LIKE '%".mysql_real_escape_string(urldecode($_GET['title']))."%'
			OR flm.title_orig LIKE '%".mysql_real_escape_string(urldecode($_GET['title']))."%')";
	}

	if($_GET['year'])
	{
		$q.=" AND flm.year='".intval($_GET['year'])."'";
	}

	if($_GET['uap'])
	{
		$q.=" AND flm.ukraine_premiere='".$uap."'";
	}

	if($_GET['wop'])
	{
		$q.=" AND flm.world_premiere='".$wop."'";
	}

	if($_GET['letter'])
	{
		$q.=" AND (lng.title LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%'
			OR flm.title_orig LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%')";
	}

	if($_GET['genre_id'])
	{
		$q.=" AND gnr.genre_id=".intval($_GET['genre_id'])."";
	}

	if($_GET['country'])
	{
		$q.=" AND cnt.country_id=".intval($_GET['country'])."";
	}

	if(isset($_GET['serial']))
	{
		if($_GET['serial'] != "") return 404;
		$q.=" AND flm.serial=1 ";
	}

	if($_GET['show'])
	{
		$q.=" AND cnm.city_id=".intval($_GET['city_id'])." AND (shw.begin<='".date('Y-m-d')."' AND shw.end>='".date('Y-m-d')."')";
	}

	$q.="
		GROUP BY flm.id
		ORDER BY flm.total_shows DESC
	";
	//===========================================================

	if (sys::translate('main::films_'.$_GET['year'])!='' && sys::translate('main::films_'.$_GET['year'])!='main::films_'.$_GET['year'])
	{
		$result['seo'] =  sys::translate('main::films_'.$_GET['year']);
	}


	if (sys::translate('main::films_'.$_GET['year'].'_genre_'.$_GET['genre_id'])!='' && sys::translate('main::films_'.$_GET['year'].'_genre_'.$_GET['genre_id'])!='main::films_'.$_GET['year'].'_genre_'.$_GET['genre_id'])
	{

		$result['seo'] =  sys::translate('main::films_'.$_GET['year'].'_genre_'.$_GET['genre_id']);
	}




	if (!$_GET['year'] && sys::translate('main::films_seo')!='' && sys::translate('main::films_seo')!='main::films_seo')
	{
		$result['seo'] =  sys::translate('main::films_seo');
	}


	$result['pages']=sys_pages::pocess($q,$_cfg['main::films_on_page']);
	
	if(isset($_GET['serial'])) 
	{
		foreach(array("first_page", "prev_page", "next_page", "last_page") as $key=>$keyval)
		{
			if(isset($result['pages'][$keyval]) && $result['pages'][$keyval])
				$result['pages'][$keyval] = $result['pages'][$keyval]."?serial";
		}
		
		for($page=1; $page<=$result['pages']['last_page_number']; $page++)
		 if(isset($result['pages']['pages'][$page]))
			$result['pages']['pages'][$page] = $result['pages']['pages'][$page]."?serial";
	}	 
	
/*if(sys::isDebugIP()) 
{
	sys::printR($result['pages']);
}*/

	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{

			$qw=$_db->query("
			SELECT
				pst.image as `poster`

			FROM `#__main_films_posters` AS `pst`

			WHERE pst.film_id=".intval($obj['id'])."
			AND pst.order_number=1
			LIMIT 1
		");

		$poster = '';

		while($object=$_db->fetchAssoc($qw))
		{
			$poster = $object['poster'];
		}

		if ($poster)
		{
//			$obj['poster'] = '//kino-teatr.ua/public/main/resize.php?f='.$poster.'&w=60';
		// inserted by Mike begin
	  	    $image="x2_".$poster;	
			if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'ims/kt_logo.jpg';
			 else $image=$_cfg['main::films_url'].$image;
			$obj['poster'] = $image;
			// inserted by Mike end
		} else {
			$obj['poster'] = $_cfg['sys::root_url'].'ims/kt_logo.jpg';
		}

		
		if ($obj['rating']>0)
		{
		$obj['votes'] = $obj['votes']+$obj['pre_votes'];
			$obj['rate'] = sys::translate('main::rating').': <strong>'.round($obj['rating'], 1).'</strong> / '.sys::translate('main::votes').': '.$obj['votes'].'<br>';
		}

		$obj['url']=main_films::getFilmUrl($obj['id']);
		$obj['title']=htmlspecialchars($obj['title']);

		$obj['round']=htmlspecialchars($obj['title_orig']);
		$obj['genres']=main_films::getFilmGenresTitles($obj['id']);
		$obj['countries']=main_films::getFilmCountriesTitles($obj['id']);
		$obj['rating_url']=main_films::getFilmRatingUrl($obj['id']);
		$obj['pro_rating_url']=main_films::getFilmReviewsUrl($obj['id']);
		$obj['rating']=main_films::formatFilmRating($obj['rating']);
		$obj['pro_rating']=main_films::formatFilmRating($obj['pro_rating']);

		if ($obj['year']>0)
		{
			$obj['year'] = $obj['year'];
		} else {
			$obj['year']='';
		}

		$obj['second'] = array();
		if ($obj['year'])
		$obj['second'][] = $obj['year'];
		if ($obj['countries'])
		$obj['second'][] = implode(', ', $obj['countries']);
		if ($obj['genres'])
		$obj['second'][] = implode(', ', $obj['genres']);

		if ($obj['year'] || $obj['countries'] || $obj['genres'])
		$obj['second_row'] = implode(' | ', $obj['second']).'<br><br>';


        $object['actors'] = array();
        $obje['actors'] = array();

        $pi = 0;
			$obje['actors']=main_films::getFilmActors($obj['id']);

            if (is_array($obje['actors'][0]))
            {

	            for ($i=0;$i<=1;$i++)
	            {
	            	if ($obje['actors'][$i]['url'])
	            	{
						$object['actors'][] = '<a href="'.$obje['actors'][$i]['url'].'" title="'.$obje['actors'][$i]['fio'].'">'.$obje['actors'][$i]['fio'].'</a>';
						$pi++;
					}
	            }

            }

		if ($pi>0)
		{
			$obj['actors'] = sys::translate('main::actors').': '.implode(', ', $object['actors']);
		}

        $object['directors'] = array();
        $obje['directors'] = array();

        $di = 0;
			$obje['directors']=main_films::getFilmDirectors($obj['id']);


            if (is_array($obje['directors'][0]))
            {

	            for ($i=0;$i<1;$i++)
	            {
						$object['directors'][] = '<a href="'.$obje['directors'][$i]['url'].'" title="'.$obje['directors'][$i]['fio'].'">'.$obje['directors'][$i]['fio'].'</a>';
						$di++;
	            }

            }

		if ($di>0)
		{
			$obj['directors'] = sys::translate('main::directors').': '.implode(', ', $object['directors']);
		}

		$obj['third'] = array();
		if ($obj['directors'])
		$obj['third'][] = $obj['directors'];
		if ($obj['actors'])
		$obj['third'][] = $obj['actors'];

		if ($obj['directors'] || $obj['actors'])
		$obj['third_row'] = implode(', ', $obj['third']);



		$result['objects'][]=$obj;
	}

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>