<?
function main_news_article_imax()
{
	sys::useLib('main::news');
	sys::useLib('main::users');
	sys::useLib('main::discuss');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('news_id','int');

	define('_CANONICAL',main_news::getArticleUrl($_GET['article_id']),true);

	if(!$_GET['article_id'])
		return 404;

	$r=$_db->query("
		SELECT
			art.date,
			art.image,
			art.news_id,
			art.user_id,
			art.comments,
			art.source_url,
			art.public,
			art_lng.title,
			art_lng.text,
			art_lng.source,
			art_lng.alt,
			usr.login AS `user`
		FROM `#__main_news_articles` AS `art`

		LEFT JOIN
			`#__main_news_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN
			`#__sys_users` AS `usr`
		ON
			usr.id=art.user_id

		WHERE art.id=".intval($_GET['article_id'])."
	");

	$result=$_db->fetchAssoc($r);

	if(!$result)
		return 404;

	if(!$result['public'] && !sys::checkAccess('main::news_confirm'))
		return 404;

	main::countShow($_GET['article_id'],'article');

	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::news_delete') || $result['user_id']==$_user['id'])
		{
			main_news::deleteArticle($_GET['article_id']);
			sys::redirect('?mod=main&act=news');
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::news_confirm'))
	{
		main_news::confirmArticle($_GET['article_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}


	$meta['article']=$result['title'];
	$result['meta']=sys::parseModTpl('main::news_article','page',$meta);
	if($result['image'])
	{
		$size=getimagesize($_cfg['main::news_dir'].$result['image']);
		$result['width']=$size[0];
		$result['image']=$_cfg['main::news_url'].$result['image'];
	}

	$result['text'] = ereg_replace("<([^>]*)(class|lang|style|size|face)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>","<\\1>",$result['text']);

	$result['text'] = str_replace("<div>&nbsp;</div>","",$result['text']);
	$result['text'] = str_replace('<div >&nbsp;</div>',"",$result['text']);

	$result['text'] = str_replace("<p>&nbsp;</p>","",$result['text']);


	$result['alt']=htmlspecialchars($result['alt']);
	$result['source']=htmlspecialchars($result['source']);

	$date=sys::db2Timestamp($result['date']);
	$result['year']=date('Y',$date);
	$result['month']=date('m',$date);

	$result['date']=sys::db2Date($result['date'],false,$_cfg['sys::date_format']);
	$result['user_url']=main_users::getUserUrl($result['user_id']);
	$result['edit_url']=false;
	$result['delete']=false;


	//�����������--------------------------------------------
	$q="
		SELECT
			msg.id,
			msg.date,
			msg.user_id,
			msg.user_name,
			msg.rating,
			usr.login AS `user`,
			msg.text,
			grp.group_id AS `star`,
			data.main_avatar AS `avatar`

		FROM `#__main_discuss_messages` AS `msg`

		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=msg.user_id

		LEFT JOIN `#__sys_users_data` AS `data`
		ON data.user_id=msg.user_id

		LEFT JOIN `#__sys_users_groups` as grp
		ON grp.user_id=msg.user_id
		AND grp.group_id=23

		WHERE msg.object_id='".intval($_GET['article_id'])."'
		AND msg.object_type='film'

		ORDER BY msg.date DESC

		LIMIT 0,5
	";


	$r=$_db->query($q);
	$result['comments']=array();


	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true);
		if($obj['user_id']==2)
		{
			$obj['user']=$obj['user_name'];
			$obj['user_url']=false;
		}
		else
			$obj['user_url']=main_users::getUserUrl($obj['user_id']);
		$obj['bb_text']=sys::parseBBCode($obj['text']);
		$result['comments'][]=$obj;
	}

	if(sys::checkAccess('main::news_edit') || $result['user_id']==$_user['id'])
		$result['edit_url']=main_news::getArticleEditUrl($_GET['article_id']);

	if(sys::checkAccess('main::news_delete') || $result['user_id']==$_user['id'])
		$result['delete']=true;
	return $result;
}
?>