<?

function main_cinema_shows()
{

	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('sys::form');
	sys::useLib('main::megakino');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('cinema_id','int');

	if(isset($_POST['_task']) && $_POST['_task'])
	{
		if($_POST['_task']=='user_cinema_subscribe')
		$id_cinema = isset($_POST["id_cinema"])?intval($_POST["id_cinema"]):0;
		$act = isset($_POST["act"])?intval($_POST["act"]):0;

		if($act==0)
			main_cinemas::unsubscribeCinema($_user["id"], $id_cinema);
		else
			main_cinemas::subscribeCinema($_user["id"], $id_cinema);
		sys::redirect(main_cinemas::getCinemaShowsUrl($id_cinema),false);
	}


	define('_CANONICAL',main_cinemas::getCinemaShowsUrl($_GET['cinema_id']),true);

	$cache_name='main_cinema_shows_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($_SERVER['HTTP_HOST'] == 'kino-teatr.ua'){
		if($cache=sys::getCache($cache_name,$_cfg['main::cinema_shows_page_cache_period']*_HOUR/6)) // раз в 10 минут
		return unserialize($cache);
	}


	if(!$_GET['cinema_id'])
		return 404;

	$r=$_db->query("
		SELECT
			cnm.user_id,
			cnm.city_id,
			cnm.notice_begin,
			cnm.ticket_url,
			cnm.notice_end,
			cnm_lng.title AS `cinema`,
			cnm_lng.notice
		FROM
			`#__main_cinemas` AS `cnm`

		LEFT JOIN
			`#__main_cinemas_lng` AS `cnm_lng`
		ON
			cnm_lng.record_id=cnm.id
		AND
			cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE
			cnm.id=".intval($_GET['cinema_id'])."
	");
	$result=$_db->fetchAssoc($r);

	if(!$result)
		return 404;

	$result['notice_begin']=strtotime($result['notice_begin']);
	$result['notice_end']=strtotime($result['notice_end']);
	if($result['notice_begin']<time() && time()<$result['notice_end'])
		$result['notice']=$result['notice'];
	else
		$result['notice']=false;

	$meta['cinema']=$result['cinema'];
	$result['title'] = sys::translate('main::shows').' - '.sys::translate('main::cinema').' '.$result['cinema'];
	$result['meta']=sys::parseModTpl('main::cinema_shows','page',$meta);
	$curr_data = date('Y-m-d');
		$partner = '';

	$q="
		SELECT
			shw.begin,
			shw.end,
			shw.film_id,
			shw.id AS `show_id`,
			shw.partner_id,
			hls_lng.title AS `hall`,
			hls.cinema_id,
			hls.id AS `hall_id`,
			hls.scheme,
			hls.`3d`,
			flm.year,
			flm.`3d` as `film_3d`,
			flm_lng.title AS `film`,
			shw.hall_id,
			ev.event_id,
			ev.site_id
		FROM `#__main_shows` AS `shw`

		LEFT JOIN `#__main_cinemas_halls` AS `hls`
		ON hls.id=shw.hall_id

		LEFT JOIN `#__main_films_lng` AS `flm_lng`
		ON flm_lng.record_id=shw.film_id
		AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_films` AS `flm`
		ON flm.id=shw.film_id
		
		LEFT JOIN `#__main_shows_eventmap` AS `ev`
		ON ev.show_id=shw.id

		LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng`
		ON hls_lng.record_id=hls.id
		AND hls_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE hls.cinema_id=".$_GET['cinema_id']." AND shw.end>='$curr_data'
		$partner
		ORDER BY hls.cinema_id, shw.begin, flm_lng.title, hls.order_number, shw.id
	";

//	if(sys::isDebugIP()) $_db->printR($q);
	$r=$_db->query($q);



	$result['dates']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		// Получить массив дат от begin до end
		$a_dt_begin = explode("-", $obj['begin']);
		$a_dt_end = explode("-", $obj['end']);

		//$dt_begin = mktime(0,0,0, month, 		  	day, 			year);
		$dt_begin   = mktime(0,0,0,	$a_dt_begin[1], $a_dt_begin[2], $a_dt_begin[0]);
		$dt_end     = mktime(0,0,0,	$a_dt_end[1], 	$a_dt_end[2], 	$a_dt_end[0]);
		$today 		= mktime(0,0,0, date("m"),		date("d"),		date("Y"));
		$now 		= mktime(date("H"), date("i"), 0, date("m"), date("d"), date("Y"));

		$qq = "
			SELECT tms.show_id, tms.time, tms.prices, tms_lng.note, tms.sale_id, tms.sale_status, tms.`3d` as seans_3d, 0 as past, tms.id as time_id
			FROM `#__main_shows_times` AS `tms`
			LEFT JOIN `#__main_shows_times_lng` AS `tms_lng`
			ON tms_lng.record_id=tms.id
			AND tms_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE tms.show_id=".$obj['show_id']." ORDER BY tms.time
		";

		$r2=$_db->query($qq);
		$a_prices=array();
		$a_times = array();

		while ($time=$_db->fetchAssoc($r2))
		{
			$time['time'] = sys::cutStrRight($time['time'],3);
			$a_times[] = $time;
			$prices = preg_split ("/[^\d]+/", $time['prices']);
			$a_prices=array_merge($a_prices, $prices);
		}

		$a_prices = array_unique($a_prices);
		sort($a_prices);
		if(!$a_prices[0]) unset($a_prices[0]);
		if(sizeof($a_prices)>2)
			$a_prices = array(0=>$a_prices[0],1=>$a_prices[sizeof($a_prices)-1]);

		if($obj['scheme'])
			$obj['scheme_url']=main_cinemas::getHallSchemeUrl($obj['hall_id']);
		else
			$obj['scheme_url']=false;

		$dates = array();
		$dtx=$dt_begin;
		for($dt=$a_dt_begin[2]; $dtx<=$dt_end; $dt++)
		{
		 	$dtx = mktime(0,0,0,$a_dt_begin[1], $dt, $a_dt_begin[0]);
			if($dtx<$today) continue; // если дата прошедшая, то нечего и отображать
		 	if($dtx<=$dt_end) // Для данного вида цикла нужна эта проверка. Иначе берется один лишний будущий день
				$dates[] = sys::db2Date(date("Y-m-d",$dtx),false, $_cfg['sys::date_format']);
		}

		foreach ($dates as $dt_key)
		{
			// Установить признак прошел или не прошел сеанс
			$a_dt_key = explode(".", $dt_key); // dd.mm.yyyy
			foreach ($a_times as $i=>$time)
			{
				$tm = explode(":",$time['time']);
				$tm = mktime($tm[0], $tm[1], 0, $a_dt_key[1], $a_dt_key[0], $a_dt_key[2]);
				$a_times[$i]['past'] = $tm<$now;
				$a_times[$i]['event_id'] = $obj['event_id'];
				$a_times[$i]['site_id'] = $obj['site_id'];

				if($a_times[$i]['sale_id']!=0 and $tm>=$now)
				{
					$a_times[$i]['time'] = '<a href="http://bilet.kino-teatr.ua/showtime?theater='.$a_times[$i]['sale_status'].'&showtime='.$a_times[$i]['sale_id'].'&agent=kino-teatr" class="vkino-link">'.$a_times[$i]['time'].'</a>';
				}
				elseif($a_times[$i]['sale_status']!='' and $a_times[$i]['sale_id']==0 and $tm>=$now)
				{
					$temp_st=explode('|',$a_times[$i]['sale_status']);
					if($temp_st[0]=='imax' and $temp_st[1]!='')
					{
						$a_times[$i]['time'] = '<a href="'.$temp_st[1].'" onclick="goPartner(this, 1);" class="mult-link">'.$a_times[$i]['time'].'</a>';
					}
					if($temp_st[0]=='mult' and $temp_st[1]!='')
					{
						$a_times[$i]['time'] = '<a href="'.$temp_st[1].'" onclick="goPartner(this, 2);" class="mult-link">'.$a_times[$i]['time'].'</a>';
					}
					if($temp_st[0]=='mega')
					{
						$a_times[$i]['time'] = ''.$a_times[$i]['time'].'';
					}
				}
                if ($obj['partner_id'] == 8) {
                    if ($_user['id'] == 2) {
                        $url = "/eventmap.php?lang_id=" . intval($_cfg['sys::lang_id']) . '&unauthorized=1';
                        $a_times[$i]['time'] = '<a href="' . $url . '" class="megakino-link" onclick="show_unauthorized(this);return false;" style="font-size:12px;">' . $a_times[$i]['time'] . '</a>';
                    } else {
                        $temp_st = explode('|', $a_times[$i]['sale_status']);
                        $site_id = '';
                        $event_id = '';
                        if ($temp_st[0] == 'megakino' and $temp_st[1] != '' and $temp_st[2] != '') {
                            $site_id = $temp_st[1];
                            $event_id = $temp_st[2];
                        } else if ($obj['event_id'] && $obj['site_id']) {
                            $site_id = $obj['site_id'];
                            $event_id = $obj['event_id'];
                        }

                        if ($site_id && $event_id) {
                            $url = "/eventmap.php?lang_id=" . intval($_cfg['sys::lang_id']) . '&cinema_id=' . $obj['cinema_id'] . '&site_id=' . $site_id . '&show_id=' . $obj['show_id'] . '&time_id=' . $a_times[$i]['time_id'];
                            $a_times[$i]['time'] = '<a href="' . $url . '" class="megakino-link" onclick="show_eventmap(this,' . $obj['show_id'] . ');return false;" style="font-size:12px;">' . $a_times[$i]['time'] . '</a>';
                        }
                    }

                }
			}
			// [дата][Фильм][Зал]
			$result['dates'][$dt_key][$obj['film_id']]['film'] = $obj['film'];
			$result['dates'][$dt_key][$obj['film_id']]['year'] = $obj['year'];
			$result['dates'][$dt_key][$obj['film_id']]['film_3d'] = $obj['film_3d'];
			$result['dates'][$dt_key][$obj['film_id']]['film_url']=main_films::getFilmUrl($obj['film_id']);
			$result['dates'][$dt_key][$obj['film_id']]['halls'][$obj['hall_id']]['title']=$obj['hall']?$obj['hall']:"&nbsp;";
			$result['dates'][$dt_key][$obj['film_id']]['halls'][$obj['hall_id']]['3d']=$obj['3d'];
			$result['dates'][$dt_key][$obj['film_id']]['halls'][$obj['hall_id']]['id']=$obj['hall_id'];
			$result['dates'][$dt_key][$obj['film_id']]['halls'][$obj['hall_id']]['scheme_url']=$obj['scheme_url'];
			$result['dates'][$dt_key][$obj['film_id']]['halls'][$obj['hall_id']]['times'][]=$a_times;
			$result['dates'][$dt_key][$obj['film_id']]['halls'][$obj['hall_id']]['price_range']=$a_prices;
		}
	}

	$result["user_cinema_subscribed"] = main_cinemas::getUserCinemaSubscribed($_user["id"], intval($_GET['cinema_id']));

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>