<?
function main_reviews_rssya()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::reviews');
	sys::setTpl();
	$r=$_db->query("
		SELECT 
			rev.id,
			rev.film_id,
			rev.user_id,
			rev.intro,
			rev.title,
			rev.text,
			rev.image,
			rev.date,
			flm_lng.title AS `film`
		FROM
			`#__main_reviews` AS `rev`
			
		LEFT JOIN 
			`#__main_films_lng` AS `flm_lng`
		ON
			flm_lng.record_id=rev.film_id
		AND
			flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			
		WHERE rev.public=1
		AND rev.date<'".gmdate('Y-m-d H:i:s')."'
		
		ORDER BY rev.date DESC
		LIMIT 0,6
	");
	
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=date('r',sys::db2Timestamp($obj['date']));	
		if($obj['image'] && is_file($_cfg['main::reviews_dir'].$obj['image']))
		{
			$obj['image_src']=$_cfg['main::reviews_url'].$obj['image'];
			$obj['image_mime']=getimagesize($_cfg['main::reviews_dir'].$obj['image']) ;
			$obj['image_mime']=$obj['image_mime']['mime'];
			$obj['image_size']=filesize($_cfg['main::reviews_dir'].$obj['image']);
		}
		else 
			$obj['image']=false;
		$obj['title']=sys::translate('main::review_on_film').' &quot;'.htmlspecialchars(strip_tags($obj['film'])).'&quot; - '.htmlspecialchars(strip_tags($obj['title']));;
		$obj['url']=main_reviews::getReviewUrl($obj['id']);
		$obj['intro']=htmlspecialchars(strip_tags($obj['intro']));
	
		$result['objects'][]=$obj;
	}
	return $result;
}
?>