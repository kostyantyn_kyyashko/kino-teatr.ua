<?
function main_films_export()
{

	$ips=file(_SECURE_DIR.'export.txt');
	foreach ($ips as $ip)
	{
		$ip=explode('#',$ip);
		$ips2[]=trim($ip[0]);
	}
//	if(!in_array($_SERVER['REMOTE_ADDR'],$ips2))
//		return 404;
	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('main::persons');
	sys::useLib('main::reviews');
	sys::useLib('main::countries');
	sys::useLib('sys::form');
	global $_db, $_cfg, $_err, $_user, $_cookie;


	sys::setTpl();

	$xml = '';

	$result=array();

	//Жанры----------------------------------------------------------------
	$r=$_db->query("
		SELECT
			gnr.id,
			lng.title
		FROM
			`#__main_genres` AS `gnr`
		LEFT JOIN
			`#__main_genres_lng` AS `lng`
		ON
			lng.record_id=gnr.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			gnr.order_number
	");
	$result['genres']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['genres'][$obj['id']]=$obj;
	}

	//Профессии----------------------------------------
	$r=$_db->query("
		SELECT
			prf.id,
			lng.title
		FROM
			`#__main_professions` AS `prf`
		LEFT JOIN
			`#__main_professions_lng` AS `lng`
		ON
			lng.record_id=prf.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			prf.order_number
	");
	$result['professions']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['professions'][$obj['id']]=$obj;
	}
	//===============================================================================


	//Страны------------------------------------------------------------------------
	$r=$_db->query("
		SELECT
			cnt.id,
			lng.title
		FROM
			`#__main_countries` AS `cnt`
		LEFT JOIN
			`#__main_countries_lng` AS `lng`
		ON
			lng.record_id=cnt.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			cnt.order_number
	");
	$result['countries']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['countries'][$obj['id']]=$obj;
	}

	$r=$_db->query("
		SELECT
			cit.id,
			cit.country_id,
			lng.title
		FROM
			`#__main_countries_cities` AS `cit`
		LEFT JOIN
			`#__main_countries_cities_lng` AS `lng`
		ON
			lng.record_id=cit.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			cit.order_number
	");
	$result['cities']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['cities'][]=$obj;
	}
	//===============================================================================


	//Показы-----------------------------------------------------------------
	$r=$_db->query("
			SELECT
			shw.begin,
			shw.end,
			shw.film_id,
			shw.hall_id,
			shw.id,
			hls.cinema_id,
			hls.`3d` as `hall_3d`,
			flm.`3d` as `film_3d`
		FROM
			`#__main_shows` AS `shw`
		LEFT JOIN
			`#__main_cinemas_halls` AS `hls`
		ON
			hls.id=shw.hall_id
		LEFT JOIN
			`#__main_films` AS `flm`
		ON
			flm.id=shw.film_id

		WHERE
			shw.end>='".mysql_real_escape_string(date('Y-m-d'))."'
		ORDER BY
			shw.end ASC
	");

	$result['shows']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$films[$obj['film_id']]=$obj['film_id'];
		$cinemas[$obj['cinema_id']]=$obj['cinema_id'];
		$halls[$obj['hall_id']]=$obj['hall_id'];
		$shows[$obj['id']]=$obj['id'];

  		if ($obj['hall_3d'] && $obj['film_3d'])
  		{  			$obj['3d'] = 1;
  		} else {
  			$obj['3d'] = 0;
  		}

		$obj['times']=array();
		$result['shows'][$obj['id']]=$obj;
	}

	//=======================================================================

	//Премьеры

	$r=$_db->query("
		SELECT `id` FROM `#__main_films` WHERE `ukraine_premiere`>'".date('Y-m-d')."'
	");
	while (list($film_id)=$_db->fetchArray($r))
	{
		$films[$film_id]=$film_id;
	}



	//Часы показов--------------------------------------------------------

	$r=$_db->query("
		SELECT
			tms.time,
			tms.prices,
			tms_lng.note,
			tms.show_id
		FROM
			`#__main_shows_times` AS `tms`
		LEFT JOIN
			`#__main_shows_times_lng` AS `tms_lng`
		ON
			tms_lng.record_id=tms.id
		AND
			tms_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			tms.show_id IN(".implode(',',$shows).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['prices']=str_replace(';',',',$obj['prices']);
		$result['shows'][$obj['show_id']]['times'][]=$obj;
	}
	//=========================================================================

	//Фильмы------------------------------------------------------------------
	$r=$_db->query("
		SELECT
			flm.id,
			lng.title,
			flm.title_orig,
			flm.duration,
			flm.year,
			flm.ukraine_premiere,
			flm.world_premiere,
			flm.age_limit,
			flm.budget,
			flm.budget_currency,
			lng.intro,
			lng.text,
			flm.rating,
			flm.votes,
			flm.`3d` as `film_3d`,
			flm.pro_rating,
			flm.children,
			flm.pro_votes
		FROM
			`#__main_films` AS `flm`
		LEFT JOIN
			`#__main_films_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".$_cfg['sys::lang_id']."
		WHERE
			flm.id IN(".implode(',',$films).")

	");
	$result['films']=array();
	{
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['title_orig']=htmlspecialchars($obj['title_orig']);
			$obj['photos']=array();
			$obj['trailers']=array();
			$obj['posters']=array();
			$obj['persons']=array();
			$obj['genres']=array();
			$obj['countries']=array();
			$obj['studios']=array();
			$obj['reviews']=array();
			$obj['links']=array();

				$obj['intro'] = eregi_replace("<div[^>]*>", "<div>", $obj['intro']);
				$obj['intro'] = eregi_replace("<b[^>]*>", "<b>", $obj['intro']);
				$obj['intro'] = eregi_replace("<strong[^>]*>", "<strong>", $obj['intro']);
				$obj['intro'] = eregi_replace("<span[^>]*>", "", $obj['intro']);
				$obj['intro'] = eregi_replace("</span>", "", $obj['intro']);
				$search = array ("'<span[^>]*?>.*?</span>'si");                    // интерпретировать как php-код
				$replace = array ("");
				$obj['intro'] = eregi_replace($search, $replace, $obj['intro']);
				$obj['intro'] = str_replace("<div>&nbsp;</div>","",$obj['intro']);
				$obj['intro'] = str_replace('<div >&nbsp;</div>',"",$obj['intro']);
				$obj['intro'] = str_replace("<p>&nbsp;</p>","",$obj['intro']);
				$obj['intro'] = str_replace("<p>","",$obj['intro']);
				$obj['intro'] = str_replace("</p>","",$obj['intro']);
				$obj['intro'] = str_replace("<b>","",$obj['intro']);
				$obj['intro'] = str_replace("</b>","",$obj['intro']);
				$obj['intro'] = str_replace("&nbsp;"," ",$obj['intro']);
				$obj['intro'] = str_replace("&mdash;","-",$obj['intro']);
				$obj['intro'] = str_replace("&ndash;","-",$obj['intro']);
				$obj['intro'] = str_replace("&quot;",'"',$obj['intro']);
				$obj['intro'] = str_replace("&ldquo;",'"',$obj['intro']);
				$obj['intro'] = str_replace("&rdquo;",'"',$obj['intro']);
				$obj['intro'] = strip_tags($obj['intro']);

  			$poster='';
			$poster=main_films::getFilmFirstPoster($obj['id']);
			$img = $_cfg['main::films_url'].$poster['image'];
			if ($poster['image'])
			$obj['poster']=$img;

			$result['films'][$obj['id']]=$obj;
		}
	}
	//============================================================================

	//Фотки фильмов-------------------------------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`image`
		FROM
			`#__main_films_photos`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['src']=$_cfg['main::films_url'].$obj['image'];
		$result['films'][$obj['film_id']]['photos'][]=$obj;
	}
	//==========================================================

	//Трейлеры фильмов-------------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`file`,
			`url`,
			`language`
		FROM
			`#__main_films_trailers`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		if($obj['file'])
			$obj['url']=$_cfg['main::films_url'].$obj['file'];
		$obj['url']=htmlspecialchars($obj['url']);
		$result['films'][$obj['film_id']]['trailers'][]=$obj;
	}
	//=======================================================================


	//Перcоны фильмов------------------------------------------------------
	$r=$_db->query("
		SELECT
			fp.film_id,
			fp.person_id,
			fp.profession_id,
			lng.role
		FROM
			`#__main_films_persons` AS `fp`
		LEFT JOIN
			`#__main_films_persons_lng` AS `lng`
		ON
			lng.record_id=fp.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			fp.film_id IN(".implode(',',$films).")
	");
	$persons=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$persons[$obj['person_id']]=$obj['person_id'];
		$obj['role']=htmlspecialchars($obj['role']);
		$result['films'][$obj['film_id']]['persons'][$obj['person_id']]=$obj;
	}
	//==============================================================

	//Жанры фильмов--------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`genre_id`
		FROM
			`#__main_films_genres`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$result['films'][$obj['film_id']]['genres'][]=$obj['genre_id'];
	}
	//================================================

	//Страны фильмов--------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`country_id`
		FROM
			`#__main_films_countries`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$result['films'][$obj['film_id']]['countries'][]=$obj['country_id'];
	}
	//================================================

	//Студии фильмов--------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`studio_id`
		FROM
			`#__main_films_studios`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$result['films'][$obj['film_id']]['studios'][]=$obj['studio_id'];
	}
	//================================================

	//Рецензии на фильмы--------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`id`,
			`title`
		FROM
			`#__main_reviews`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_reviews::getReviewUrl($obj['id']);
		$result['films'][$obj['film_id']]['reviews'][]=$obj;
	}
	//================================================

	//ссылки на фильмы--------------------------------------
	$r=$_db->query("
		SELECT
			lnk.film_id,
			lnk.url,
			lng.title
		FROM
			`#__main_films_links` AS `lnk`
		LEFT JOIN
			`#__main_films_links_lng` AS `lng`
		ON
			lng.record_id=lnk.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			lnk.film_id IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['films'][$obj['film_id']]['links'][]=$obj;
	}
	//================================================



	//Кинотеатры-----------------------------------------------------
	$r=$_db->query("
		SELECT
			cnm.id,
			cnm.city_id,
			lng.title,
			cnm.site,
			cnm.phone,
			lng.address,
			lng.text
		FROM
			`#__main_cinemas` AS `cnm`
		LEFT JOIN
			`#__main_cinemas_lng` AS `lng`
		ON
			lng.record_id=cnm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
");
//		WHERE cnm.id IN(".implode(',',$cinemas).")
//	");
	$result['cinemas']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['address']=htmlspecialchars($obj['address']);
		$obj['phone']=htmlspecialchars($obj['phone']);
		$obj['site']=htmlspecialchars($obj['site']);
		$obj['halls']=array();
		$obj['photos']=array();
		$result['cinemas'][$obj['id']]=$obj;
	}


	//============================================================

	//Фотки кинотеатра------------------------------------------------
	$r=$_db->query("
		SELECT
			`cinema_id`,
			`image`
		FROM
			`#__main_cinemas_photos`
		WHERE
			`cinema_id` IN(".implode(',',$cinemas).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{


		$img = str_replace(_ROOT_URL,'',$_cfg['main::cinemas_url'].$obj['image']);
		$result['cinemas'][$obj['cinema_id']]['photo']=_ROOT_URL.'public/main/rescrop3.php?f='.$img;
	}
	//=============================================================

	//Залы кинотеатра--------------------------------------------
	$r=$_db->query("
		SELECT
			hls.id,
			hls.cinema_id,
			hls.`3d`,
			lng.title,
			hls.scheme
		FROM
			`#__main_cinemas_halls` AS `hls`
		LEFT JOIN
			`#__main_cinemas_halls_lng` AS `lng`
		ON
			lng.record_id=hls.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."

	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		if($obj['scheme'])
			$obj['scheme']=$_cfg['main::cinemas_url'].$obj['scheme'];
		$result['cinemas'][$obj['cinema_id']]['halls'][]=$obj;
	}
	//=============================================================

	//Персоны-------------------------------------------------------
	$result['persons']=array();
	if($persons)
	{
		$r=$_db->query("
			SELECT
				prs.id,
				prs.birthdate,
				prs.lastname_orig,
				prs.firstname_orig,
				lng.lastname,
				lng.firstname,
				lng.biography
			FROM
				`#__main_persons` AS `prs`
			LEFT JOIN
				`#__main_persons_lng` AS `lng`
			ON
				lng.record_id=prs.id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE
				prs.id IN(".implode(',',$persons).")
		");

		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['lastname']=htmlspecialchars($obj['lastname']);
			$obj['firstname']=htmlspecialchars($obj['firstname']);
			$obj['lastname_orig']=htmlspecialchars($obj['lastname_orig']);
			$obj['firstname_orig']=htmlspecialchars($obj['firstname_orig']);
			$obj['url']=main_persons::getPersonUrl($obj['id']);
			$photo=main_persons::getPersonFirstPhoto($obj['id']);
			$img = str_replace(_ROOT_URL,'',$_cfg['main::persons_url'].$photo['image']);
			$obj['poster']=_ROOT_URL.'public/main/rescrop3.php?f='.$img;
			$result['persons'][]=$obj;
		}
	}
	//===============================================================


	$xml .= '
	<films>';
		foreach ($result['films'] as $film)
		{
			$xml .= '
			<film id="'.$film['id'].'">
				<id>'.$film['id'].'</id>
				<type>Фильм</type>
				<genres>';

				foreach ($film['genres'] as $person)
				{
					if (isset($result['genres'][$person]['title']))
					{
						$xml .= '
						<genre>
							<title>'.$result['genres'][$person]['title'].'</title>
						</genre>
						';
					}
				}


				$xml .= '
				</genres>
				<title>'.$film['title'].'</title>
				<title_orig>'.$film['title_orig'].'</title_orig>
				<intro><![CDATA['.$film['intro'].']]></intro>
				<text><![CDATA['.$film['text'].']]></text>
				<rating votes="'.$film['votes'].'">'.$film['rating'].'</rating>
				<poster><![CDATA['.$film['poster'].']]></poster>
				<countries>';

				foreach ($film['countries'] as $person)
				{
					if (isset($result['countries'][$person]['title']))
					{
						$xml .= '
						<country>
							<title>'.$result['countries'][$person]['title'].'</title>
						</country>
						';
					}
				}


				$xml .= '
				</countries>
				<ukraine_premiere>'.$film['ukraine_premiere'].'</ukraine_premiere>
				<world_premiere>'.$film['world_premiere'].'</world_premiere>
				<kids>'.$film['children'].'</kids>
				<persons>';

				foreach ($film['persons'] as $person)
				{
					if (isset($result['persons'][$person['person_id']]['firstname']) || isset($result['persons'][$person['person_id']]['last']))
					{
						$xml .= '
						<person>
							<id>'.$person['person_id'].'</id>
							<profession>'.$result['professions'][$person['profession_id']]['title'].'</profession>
							<name>'.$result['persons'][$person['person_id']]['firstname'].' '.$result['persons'][$person['person_id']]['lastname'].'</name>
						</person>';
					}
				}
				$xml .= '
				</persons>

				<trailers>';

					foreach ($film['trailers'] as $person)
					{
						$xml .= '
						<trailer url="'.$person['url'].'"/>';
					}
				$xml .= '
				</trailers>

			</film>';
		}
	$xml .= '
	</films>';

	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header("Content-type: text/xml; charset=cp-1251");
	echo'<?xml version="1.0"?> ';

	echo $xml;


}
?>