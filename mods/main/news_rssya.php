<?
function main_news_rssya()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::news');
	sys::setTpl();
	$r=$_db->query("
		SELECT 
			art.id,
			art.date,
			art.user_id,
			art.exclusive,
			art.small_image AS `image`,
			art_lng.title,
			art_lng.intro,
			art_lng.text,
			nws_lng.title AS `category`
		FROM `#__main_news_articles` AS `art`
		
		LEFT JOIN `#__main_news_articles_lng` AS `art_lng`
		ON art_lng.record_id=art.id
		AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		
		LEFT JOIN `#__main_news_lng` AS `nws_lng`
		ON nws_lng.record_id=art.news_id
		AND nws_lng.lang_id=".$_cfg['sys::lang_id']."
		
		WHERE art.public=1
		AND art.news_id!=9
		AND art.date<'".gmdate('Y-m-d H:i:s')."'
		
		ORDER BY art.date DESC
		
		LIMIT 0,10
	");
	
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=date('r',sys::db2Timestamp($obj['date']));	
		if($obj['image'] && is_file($_cfg['main::news_dir'].$obj['image']))
		{
			$obj['image_src']=$_cfg['main::news_url'].$obj['image'];
			$obj['image_mime']=getimagesize($_cfg['main::news_dir'].$obj['image']) ;
			$obj['image_mime']=$obj['image_mime']['mime'];
			$obj['image_size']=filesize($_cfg['main::news_dir'].$obj['image']);
		}
		else 
			$obj['image']=false;
		$obj['title']=htmlspecialchars(stripslashes(strip_tags($obj['title'])));
		$obj['url']=main_news::getArticleUrl($obj['id']);
		$obj['intro']=strip_tags($obj['intro']);
	
		$result['objects'][]=$obj;
	}
	return $result;
}
?>