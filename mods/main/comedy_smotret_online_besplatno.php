<?

function main_comedy_smotret_online_besplatno()
{
	global $_db, $_cfg, $_err;
	sys::useLib('main::genres');
	sys::useLib('main::countries');
	sys::useLib('sys::form');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	sys::filterGet('title');
	sys::filterGet('genre_id');
	sys::filterGet('uap');
	sys::filterGet('wop');
	sys::filterGet('year');
	sys::filterGet('country');
	sys::filterGet('letter');
	sys::filterGet('city_id','int',$_cfg['main::city_id']);
	sys::filterGet('show');
	sys::filterGet('order_by','text','title.asc');

	if ($_GET['page']!='' || $_GET['title']!='' || $_GET['genre_id']!='' || $_GET['year']!='' || $_GET['city_id']!='')
	{
		define('_NOINDEX',1,true);
	}


	$lang=$_cfg['sys::lang'];

	if ($_GET['page'])
	{
		if ($lang=='ru')
		{
			define('_CANONIC', _ROOT_URL.'ru/main/comedy_smotret_online_besplatno.phtml');
		} else {
			define('_CANONIC', _ROOT_URL.'uk/main/comedy_smotret_online_besplatno.phtml');
		}
	}

	$cache_name='main_films_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::films_page_cache_period']*60))
		return unserialize($cache);

	$result=array();


	$meta['page']=$_GET['page'];

	if(!$_GET['page'])
	{
		$result['meta']=sys::parseModTpl('main::watch_comedy','page',$meta);
	} else {
		$result['meta']=sys::parseModTpl('main::watch_comedy_page','page',$meta);
	}

	//Определение направления сортировки
	$order_by=explode('.',$_GET['order_by']);
	$order_fields=array('title','year','rating','pro_rating');
	if($order_by[1]=='asc')
	{
		$result['order']='asc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.desc');
		}

	}
	else
	{
		$result['order']='desc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.asc');
		}
	}
	$result['order_field']=$order_by[0];

	$order_by=sys::parseOrderBy($_GET['order_by']);
	//=====================================================

	//Поля поисковой формы-------------------------------------
	$result['fields']['title']['input']='textbox';
	$result['fields']['title']['value']=urldecode($_GET['title']);
	$result['fields']['title']['title']=sys::translate('main::title');

	$result['fields']['genre_id']['input']='selectbox';
	$result['fields']['genre_id']['value']=$_GET['genre_id'];
	$result['fields']['genre_id']['empty']=sys::translate('main::all_genres');
	$result['fields']['genre_id']['values']=main_genres::getGenresTitles();
	$result['fields']['genre_id']['title']=sys::translate('main::genre');

	$result['fields']['year']['input']='selectbox';
	$result['fields']['year']['value']=$_GET['year'];
	$result['fields']['year']['empty']=sys::translate('main::all_years');
	$result['fields']['year']['values']=main::getYears();
	$result['fields']['year']['title']=sys::translate('main::year');

	$result['fields']['city_id']['input']='selectbox';
	$result['fields']['city_id']['value']=$_GET['city_id'];
	$result['fields']['city_id']['empty']=sys::translate('main::all_cities');
	$result['fields']['city_id']['values']=main_countries::getCountryCitiesTitles(29);
	$result['fields']['city_id']['title']=sys::translate('main::city');

	$result['fields']['show']['input']='checkbox';
	$result['fields']['show']['value']=$_GET['show'];
	$result['fields']['show']['title']=sys::translate('main::only_showing');
	//===================================================================

	//Формирование запроса-----------------------------------------------
	$q="
		SELECT
			flm.id,
			flm.title_orig,
			lng.title,
			flm.year,
			flm.rating,
			flm.votes,
			flm.pro_rating,
			flm.pro_votes
		FROM `#__main_films` AS `flm`
		
		LEFT JOIN `#__main_films_lng` AS `lng`
		ON lng.record_id=flm.id
		AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
		
		LEFT JOIN `#__main_films_genres` AS `gnr`
		ON gnr.film_id=flm.id
		AND gnr.genre_id=6
		
		LEFT JOIN `partner_megogo_objects` AS `prt`
		ON prt.id=flm.megogo_id 

		WHERE flm.megogo_id!=''  AND prt.vod in ('advod','fvod') AND genre_id IS NOT NULL
		GROUP BY flm.id
		ORDER BY flm.ukraine_premiere DESC, flm.total_shows DESC
	";
	//===========================================================



	if (sys::translate('main::watch_comedy')!='main::watch_comedy')
	{
		$result['seo'] =  sys::translate('main::watch_comedy');
	}

  	$result['pages']=sys_pages::pocess($q,$_cfg['main::films_on_page']);


	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{

			$qw=$_db->query("
			SELECT
				pst.image as `poster`

			FROM `#__main_films_posters` AS `pst`

			WHERE pst.film_id=".intval($obj['id'])."
			AND pst.order_number=1
			LIMIT 1
		");

		$poster = '';

		while($object=$_db->fetchAssoc($qw))
		{
			$poster = $object['poster'];
		}

		if($poster)
		{
	  	    $image="x2_".$poster; 
			if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::films_url'].$image;
		} else {
         	$image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		}
		$obj['poster'] = $image;

		if ($obj['rating']>0)
		{
			$obj['rate'] = sys::translate('main::rating').': <strong>'.round($obj['rating'], 1).'</strong> / '.sys::translate('main::votes').': '.$obj['votes'].'<br>';
		}





		$obj['url']=main_films::getFilmUrl($obj['id']);
		$obj['title']=htmlspecialchars($obj['title']);




		$obj['round']=htmlspecialchars($obj['title_orig']);
		$obj['genres']=main_films::getFilmGenresTitles($obj['id']);
		$obj['countries']=main_films::getFilmCountriesTitles($obj['id']);
		$obj['rating_url']=main_films::getFilmRatingUrl($obj['id']);
		$obj['pro_rating_url']=main_films::getFilmReviewsUrl($obj['id']);
		$obj['rating']=main_films::formatFilmRating($obj['rating']);
		$obj['pro_rating']=main_films::formatFilmRating($obj['pro_rating']);




		if ($obj['year']>0)
		{
			$obj['year'] = $obj['year'];
		} else {
			$obj['year']='';
		}

		$obj['second'] = array();
		if ($obj['year'])
		$obj['second'][] = $obj['year'];
		if ($obj['countries'])
		$obj['second'][] = implode(', ', $obj['countries']);
		if ($obj['genres'])
		$obj['second'][] = implode(', ', $obj['genres']);

		if ($obj['year'] || $obj['countries'] || $obj['genres'])
		$obj['second_row'] = implode(' | ', $obj['second']).'<br><br>';


        $object['actors'] = array();
        $obje['actors'] = array();

        $pi = 0;
			$obje['actors']=main_films::getFilmActors($obj['id']);

            if (is_array($obje['actors'][0]))
            {

	            for ($i=0;$i<=1;$i++)
	            {
	            	if ($obje['actors'][$i]['url'])
	            	{
						$object['actors'][] = '<a href="'.$obje['actors'][$i]['url'].'" title="'.$obje['actors'][$i]['fio'].'">'.$obje['actors'][$i]['fio'].'</a>';
						$pi++;
					}
	            }

            }

		if ($pi>0)
		{
			$obj['actors'] = sys::translate('main::actors').': '.implode(', ', $object['actors']);
		}

        $object['directors'] = array();
        $obje['directors'] = array();

        $di = 0;
			$obje['directors']=main_films::getFilmDirectors($obj['id']);


            if (is_array($obje['directors'][0]))
            {

	            for ($i=0;$i<1;$i++)
	            {
						$object['directors'][] = '<a href="'.$obje['directors'][$i]['url'].'" title="'.$obje['directors'][$i]['fio'].'">'.$obje['directors'][$i]['fio'].'</a>';
						$di++;
	            }

            }

		if ($di>0)
		{
			$obj['directors'] = sys::translate('main::directors').': '.implode(', ', $object['directors']);
		}

		$obj['third'] = array();
		if ($obj['directors'])
		$obj['third'][] = $obj['directors'];
		if ($obj['actors'])
		$obj['third'][] = $obj['actors'];

		if ($obj['directors'] || $obj['actors'])
		$obj['third_row'] = implode(', ', $obj['third']);



		$result['objects'][]=$obj;
	}

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>