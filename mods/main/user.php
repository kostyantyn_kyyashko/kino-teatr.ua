<?
function main_user()
{
	sys::useLib('main::users');
	sys::useLib('main::films');
	sys::useLib('main::persons');
	sys::useLib('main::reviews');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('user_id','int');

	if(!$_GET['user_id']) return 404;
		
	$id_user = intval($_GET['user_id']);	
// выбор информации из профиля
	$r=$_db->query("
		SELECT
			usr.date_reg,
			usr.login,
			usr.date_last_visit,
			dat.main_sex AS `sex`,
			dat.main_avatar AS `avatar`,
			dat.main_birth_date AS `birth_date`,
			dat.main_info AS `info`
		FROM
			`#__sys_users` AS `usr`
		LEFT JOIN
			`#__sys_users_data` AS `dat`
		ON
			dat.user_id=usr.id
		WHERE
			usr.id=$id_user

	");

	$result=$_db->fetchAssoc($r);
	if(!$result)
		return 404;

	$meta['user']=$result['login'];
	$result['meta']=sys::parseModTpl('main::user','page',$meta);

			if($result['avatar'])
			{
				$result['avatar']=$_cfg['main::users_url'].$result['avatar'];
			} else {
				$result['avatar']=$_cfg['main::users_url'].'avatar.jpg';
			}


	$result['age']=floor((time()-sys::db2Timestamp($result['birth_date'],'%Y-%m-%d'))/(365*_DAY)); 
	$result['date_reg']=sys::db2Date($result['date_reg'],false,$_cfg['sys::date_format']);
	$result['date_last_visit']=sys::db2Date($result['date_last_visit'],true);
	$result['sex']='<span class="'.$result['sex'].'Span">&nbsp;</span>'.sys::translate('main::'.$result['sex']);
	$result['birth_date']=sys::db2Date($result['birth_date'],false,$_cfg['sys::date_format']);

// выбор информации о любимых жанрах
	
	$q = "
			SELECT 
				gnr.genre_id, 
				lng.title,
				lng.titles
			FROM `#__main_users_genres` as gnr
			
			LEFT JOIN `#__main_genres_lng` as lng 
			ON lng.record_id=gnr.genre_id and lng.lang_id=".intval($_cfg['sys::lang_id'])."
			
			WHERE gnr.user_id=$id_user
			ORDER BY lng.title
			";	
	$r=$_db->query($q);
	$result['genres'] = array();
	while ($genre = $_db->fetchAssoc($r))
	{
		if(!$genre['titles']) $genre['titles'] = $genre['title'];
		$genre['titles'] = trim($genre['titles']);
		$result['genres'][] = $genre;
	}
	
// выбор информации о понравившихся фильмах

	$q = "
			SELECT * FROM (
			SELECT 
				case when (rtn.mark=0) then rtn.pre_mark else rtn.mark end mark,
				rtn.film_id, 
				flm.title,
				f.year,
				CAST(world_premiere AS unsigned) as world_premiere,
				pst.image
				
			FROM `#__main_films_rating` AS rtn

			LEFT JOIN `#__main_films_lng` AS flm
			ON flm.record_id=rtn.film_id and lang_id=".intval($_cfg['sys::lang_id'])."
			
			LEFT JOIN `#__main_films` AS f
			ON f.id=rtn.film_id
			
			LEFT JOIN `#__main_films_posters` AS pst
			ON pst.film_id=rtn.film_id AND order_number =1
			
			WHERE rtn.user_id=$id_user
			) T
			WHERE mark >5
			ORDER BY mark DESC, world_premiere DESC 
		";
	$r=$_db->query($q);
	
	$result['liked_films'] = array();
	while ($film = $_db->fetchAssoc($r))
	{
		$image = $film['image'];
		if ($image)
		{
	  	    $image="x2_".$image;	
			if(!file_exists($_cfg['main::films_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::films_url'].$image;
			$film['image'] = $image;
		} else {
			$film['image'] = $_cfg['sys::root_url'].'blank_news_img.jpg';
		}

		$film['film_url']=main_films::getFilmUrl($film['film_id']);
		if(main_films::isFilmTrailers($film['film_id'])) $film['trailers']=main_films::getFilmTrailersUrl($film['film_id']);
		if(main_films::isFilmShows($film['film_id'])) $film['shows']=main_films::getFilmShowsUrl($film['film_id']);

		$result['liked_films'][] = $film;
	}	
// выбор информации о непонравившихся фильмах

	$q = "
			SELECT * FROM (
			SELECT 
				case when (rtn.mark=0) then rtn.pre_mark else rtn.mark end mark,
				rtn.film_id, 
				flm.title,
				f.year,
				CAST(world_premiere AS unsigned) as world_premiere,
				pst.image
				
			FROM `#__main_films_rating` AS rtn

			LEFT JOIN `#__main_films_lng` AS flm
			ON flm.record_id=rtn.film_id and lang_id=".intval($_cfg['sys::lang_id'])."
			
			LEFT JOIN `#__main_films` AS f
			ON f.id=rtn.film_id
			
			LEFT JOIN `#__main_films_posters` AS pst
			ON pst.film_id=rtn.film_id AND order_number =1
			
			WHERE rtn.user_id=$id_user
			) T
			WHERE mark <=5
			ORDER BY mark, world_premiere DESC 
		";
	$r=$_db->query($q);
	
	$result['not_liked_films'] = array();
	while ($film = $_db->fetchAssoc($r))
	{
		$image = $film['image'];
		if ($image)
		{
	  	    $image="x2_".$image;	
			if(!file_exists($_cfg['main::films_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::films_url'].$image;
			$film['image'] = $image;
		} else {
			$film['image'] = $_cfg['sys::root_url'].'blank_news_img.jpg';
		}

		$film['film_url']=main_films::getFilmUrl($film['film_id']);
		if(main_films::isFilmTrailers($film['film_id'])) $film['trailers']=main_films::getFilmTrailersUrl($film['film_id']);
		if(main_films::isFilmShows($film['film_id'])) $film['shows']=main_films::getFilmShowsUrl($film['film_id']);

		$result['not_liked_films'][] = $film;
	}	
	
	
// выбор информации о понравившихся персонах
	$q = "
			SELECT 
				rtn.mark, 
				rtn.person_id, 
				concat(flm.firstname, ' ', flm.lastname) as title,
				pst.image
				
			FROM `#__main_persons_rating` AS rtn

			LEFT JOIN `#__main_persons_lng` AS flm
			ON flm.record_id=rtn.person_id and lang_id=".intval($_cfg['sys::lang_id'])."
			
			LEFT JOIN `#__main_persons_photos` AS pst
			ON pst.person_id=rtn.person_id AND order_number =1
			
			WHERE rtn.user_id=$id_user AND rtn.mark>5
			ORDER BY rtn.mark DESC, flm.lastname 
		";
	$r=$_db->query($q);
	
	$result['liked_persons'] = array();
	while ($person = $_db->fetchAssoc($r))
	{
		$image = $person['image'];
		if ($image)
		{
	  	    $image="x2_".$image;	
			if(!file_exists($_cfg['main::persons_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::persons_url'].$image;
			$person['image'] = $image;
		} else {
			$person['image'] = $_cfg['sys::root_url'].'blank_news_img.jpg';
		}

		$person['person_url']=main_persons::getPersonUrl($person['person_id']);
		if(main_persons::isPersonTrailers($person['person_id'])) $person['trailers']=main_persons::getPersonTrailersUrl($person['person_id']);

		$result['liked_persons'][] = $person;
	}	

// выбор информации о непонравившихся персонах
	$q = "
			SELECT 
				rtn.mark, 
				rtn.person_id, 
				concat(flm.firstname, ' ', flm.lastname) as title,
				pst.image
				
			FROM `#__main_persons_rating` AS rtn

			LEFT JOIN `#__main_persons_lng` AS flm
			ON flm.record_id=rtn.person_id and lang_id=".intval($_cfg['sys::lang_id'])."
			
			LEFT JOIN `#__main_persons_photos` AS pst
			ON pst.person_id=rtn.person_id AND order_number =1
			
			WHERE rtn.user_id=$id_user AND rtn.mark<=5
			ORDER BY rtn.mark, flm.lastname 
		";
	$r=$_db->query($q);
	
	$result['not_liked_persons'] = array();
	while ($person = $_db->fetchAssoc($r))
	{
		$image = $person['image'];
		if ($image)
		{
	  	    $image="x2_".$image;	
			if(!file_exists($_cfg['main::persons_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::persons_url'].$image;
			$person['image'] = $image;
		} else {
			$person['image'] = $_cfg['sys::root_url'].'blank_news_img.jpg';
		}

		$person['person_url']=main_persons::getPersonUrl($person['person_id']);
		if(main_persons::isPersonTrailers($person['person_id'])) $person['trailers']=main_persons::getPersonTrailersUrl($person['person_id']);

		$result['not_liked_persons'][] = $person;		
	}	

	// выбор информации о рецензиях
/*	
	$q = "
			SELECT 
				rv.id as review_id, 
				rv.film_id,
				rv.date,
				rv.title, 
				rv.intro,
				rv.image,
				flm.year,
				pst.image as film_image,
				flmlng.title as film_title
				
			FROM `#__main_reviews` AS rv

			LEFT JOIN `#__main_films` AS flm
			ON flm.id=rv.film_id 

			LEFT JOIN `#__main_films_lng` AS flmlng
			ON flmlng.record_id=rv.film_id and flmlng.lang_id=".intval($_cfg['sys::lang_id'])."
				
			LEFT JOIN `#__main_films_posters` AS pst
			ON pst.film_id=rv.film_id AND pst.order_number =1

			WHERE rv.user_id=$id_user
			ORDER BY rv.date DESC
			LIMIT 0, 3
		";
	$r=$_db->query($q);
	
	$result['reviews'] = array();
	while ($review = $_db->fetchAssoc($r))
	{
		$image = $review['image'];
		if ($image)
		{
	  	    $image="x2_".$image;	
			if(!file_exists($_cfg['main::reviews_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::reviews_url'].$image;
			$review['image'] = $image;
		} else {
			$review['image'] = $_cfg['sys::root_url'].'blank_news_img.jpg';
		}
		
		$review['url']=main_reviews::getReviewUrl($review['review_id']);
		$review['date']=sys::db2Date($review['date'],false,'%d.%m.%Y');
		$review['intro']=strip_tags($review['intro']);

		$result['reviews'][] = $review;		
	}	
*/
	
	// выбор информации об оспасибленных рецензиях
	// Вывести названия рецензий, автора и название фильма, которым пользователь поставил "спасибо"
	$q = "
			SELECT 
				rv.id as review_id, 
				rv.film_id,
				rv.user_id,
				rv.date,
				rv.title, 
				rv.intro,
				rv.image,
				flm.year,
				usr.login as author,
				pst.image as film_image,
				flmlng.title as film_title
				
			FROM `#__main_reviews_thanks` AS thanks
			
			LEFT JOIN `#__main_reviews` AS rv			
			ON thanks.review_id=rv.id

			LEFT JOIN `#__main_films` AS flm
			ON flm.id=rv.film_id 

			LEFT JOIN `#__main_films_lng` AS flmlng
			ON flmlng.record_id=rv.film_id and flmlng.lang_id=".intval($_cfg['sys::lang_id'])."
				
			LEFT JOIN `#__main_films_posters` AS pst
			ON pst.film_id=rv.film_id AND pst.order_number =1

			LEFT JOIN `#__sys_users` AS `usr`
			ON usr.id=rv.user_id

			WHERE thanks.user_id=$id_user
			ORDER BY thanks.review_id DESC
		";
	$r=$_db->query($q);
	
	$result['thanks'] = array();
	while ($review = $_db->fetchAssoc($r))
	{
		if(!$review['author']) continue;
		$image = $review['image'];
		if ($image)
		{
	  	    $image="x2_".$image;	
			if(!file_exists($_cfg['main::reviews_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::reviews_url'].$image;
			$review['image'] = $image;
		} else {
			$review['image'] = $_cfg['sys::root_url'].'blank_news_img.jpg';
		}
		
		$review['url']=main_reviews::getReviewUrl($review['review_id']);
		$review['film_url']=main_films::getFilmUrl($review['film_id']);
		$review['author_url']=main_users::getUserUrl($review['user_id']);
		$review['date']=sys::db2Date($review['date'],false,'%d.%m.%Y');
		$review['intro']=strip_tags($review['intro']);

		$result['thanks'][] = $review;		
	}	
	
	// выбор информации о размещенных персонах
	// Вывести фотки персон, которых добавил этот user
	$q = "
			SELECT 
				prs.id as person_id, 
				concat(lng.firstname, ' ', lng.lastname) as title,
				pst.image
				
			FROM `#__main_persons` AS prs

			LEFT JOIN `#__main_persons_lng` AS lng
			ON lng.record_id=prs.id and lang_id=".intval($_cfg['sys::lang_id'])."
			
			LEFT JOIN `#__main_persons_photos` AS pst
			ON pst.person_id=prs.id AND pst.order_number =1
			
			WHERE prs.user_id=$id_user
			ORDER BY lng.lastname 
		";
	//if(sys::isDebugIP()) $_db->printR($q);
	$r=$_db->query($q);
	
	$result['added_persons'] = array();
	while ($person = $_db->fetchAssoc($r))
	{
		$image = $person['image'];
		if ($image)
		{
	  	    $image="x2_".$image;	
			if(!file_exists($_cfg['main::persons_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::persons_url'].$image;
			$person['image'] = $image;
		} else {
			$person['image'] = $_cfg['sys::root_url'].'blank_news_img.jpg';
		}

		$person['person_url']=main_persons::getPersonUrl($person['person_id']);
		if(main_persons::isPersonTrailers($person['person_id'])) $person['trailers']=main_persons::getPersonTrailersUrl($person['person_id']);

		$result['added_persons'][] = $person;		
	}	
		
	return $result;
}
?>