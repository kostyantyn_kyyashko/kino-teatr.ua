<?
function main_popular_reviews()
{
	global $_db, $_cfg, $_err;
	sys::useLib('main::reviews');
	sys::useLib('main::users');
	sys::useLib('main::spec_themes');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	sys::filterGet('order_by','text','date.desc');


	if ($_GET['page']!='')
	{
		define('_NOINDEX',1,true);
	}

	if ($_GET['order_by'])
	{
		define('_CANONICAL',$_cfg['sys::root_url'].$_cfg['sys::lang'].'/main/popular_reviews.phtml',true);
    }


	$cache_name='main_reviews_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::reviews_page_cache_period']*60))
		return unserialize($cache);

	$result=array();

	$result['pagename']=sys::translate('main::reviews');

	if ($_GET['page'])
	{
		$meta['page'] = sys::translate('main::page').' '.$_GET['page'];
	} else {
		$meta['page'] = '';
	}

	$result['meta']=sys::parseModTpl('main::reviews','page',$meta);

	//Определение направления сортировки
	$order_by=explode('.',$_GET['order_by']);
	$order_fields=array('date','thanks','title','user');
	if($order_by[1]=='asc')
	{
		$result['order']='asc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.desc');
		}

	}
	else
	{
		$result['order']='desc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.asc');
		}
	}
	$result['order_field']=$order_by[0];

	$order_by=sys::parseOrderBy($_GET['order_by']);
	//=====================================================


	//Формирование запроса-----------------------------------------------
	$q="
		SELECT
			rev.id,
			rev.film_id,
			rev.user_id,
			rev.total_shows,
			rev.comments,
			rev.title,
			rev.intro,
			flm.year,
			rev.date,
			rev.image,
			spec.spec_theme_id as spec_theme,
			flm_lng.title AS `film`,
			rev.thanks,
			users.group_id as `gid`,
			usr.login AS `user`
		FROM
			`#__main_reviews` AS `rev`

		LEFT JOIN
			`#__main_films_lng` AS `flm_lng`
		ON
			flm_lng.record_id=rev.film_id
		AND
			flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_films` AS `flm`
		ON flm.id=rev.film_id

		LEFT JOIN
			`#__sys_users` AS `usr`
		ON
			usr.id=rev.user_id
		LEFT JOIN
			`#__sys_users_groups` AS `users`
		ON
			usr.id=users.user_id

		LEFT JOIN `#__main_reviews_articles_spec_themes` AS `spec`
		ON rev.id=spec.article_id

		WHERE rev.public=1
		AND rev.date<'".gmdate('Y-m-d H:i:s')."'
		ORDER BY rev.total_shows DESC
	";


	//===========================================================

	$result['pages']=sys_pages::pocess($q,$_cfg['main::reviews_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		if($obj['image'])
		{
					$image=$_cfg['main::reviews_url'].$obj['image'];

                	list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));
 		}



		if ($wid>=200 && $obj['image'])
		{
		 	$image = $_cfg['main::reviews_url'].$obj['image'];
		} else {
           	$poster=main_films::getFilmFirstPoster($obj['film_id']);

           	if ($poster)
           	{
				$image=$_cfg['main::films_url'].$poster['image'];
           	} else {
         	$image = 'blank_news_img.jpg';
           	}
		}


		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';
		$obj['shows']=$obj['total_shows'];
		$obj['date'] = date('Y-m-d', strtotime($obj['date']));
   		$obj['date']=sys::russianDate($obj['date']);
		$obj['intro'] = strip_tags($obj['intro']);
		$obj['url']=main_reviews::getReviewUrl($obj['id']);
		$obj['spec_theme_url']=main_spec_themes::getArticleUrl($obj['spec_theme']);
		$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
		$obj['user_url']=main_users::getUserUrl($obj['user_id']);

        if ($obj['gid']==10 || $obj['gid']==20)
        {
			$obj['pro'] = 1;
        }

		$result['objects'][]=$obj;
	}

		$result['jour_revs']=sys::rewriteUrl('?mod=main&act=reviews');
		$result['user_revs']=sys::rewriteUrl('?mod=main&act=users_reviews');
		$result['popular_revs']=sys::rewriteUrl('?mod=main&act=popular_reviews');


	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>