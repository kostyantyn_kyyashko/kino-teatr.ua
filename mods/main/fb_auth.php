<?

function Transliterate($string){
  $cyr=array(
     "Щ", "Ш", "Ч","Ц", "Ю", "Я", "Ж","А","Б","В",
     "Г","Д","Е","Ё","З","И","Й","К","Л","М","Н",
     "О","П","Р","С","Т","У","Ф","Х","Ь","Ы","Ъ",
     "Э","Є", "Ї","І",
     "щ", "ш", "ч","ц", "ю", "я", "ж","а","б","в",
     "г","д","е","ё","з","и","й","к","л","м","н",
     "о","п","р","с","т","у","ф","х","ь","ы","ъ",
     "э","є", "ї","і"
  );
  $lat=array(
     "Shch","Sh","Ch","C","Yu","Ya","J","A","B","V",
     "G","D","e","e","Z","I","y","K","L","M","N",
     "O","P","R","S","T","U","F","H","",
     "Y","" ,"E","E","Yi","I",
     "shch","sh","ch","c","yu","ya","j","a","b","v",
     "g","d","e","e","z","i","y","k","l","m","n",
     "o","p","r","s","t","u","f","h",
     "", "y","" ,"e","e","yi","i"
  );
  for($i=0; $i<count($cyr); $i++)  {
     $c_cyr = $cyr[$i];
     $c_lat = $lat[$i];
     $string = str_replace($c_cyr, $c_lat, $string);
  }
  $string =
  	preg_replace(
  		"/([qwrtpsdfghklzxcvbnmQWRTPSDFGHKLZXCVBNM]+)[jJ]e/",
  		"\${1}e", $string);
  $string =
  	preg_replace(
  		"/([qwrtpsdfghklzxcvbnmQWRTPSDFGHKLZXCVBNM]+)[jJ]/",
  		"\${1}'", $string);
  $string = preg_replace("/([eyuioaEYUIOA]+)[Kk]h/", "\${1}h", $string);
  $string = preg_replace("/^kh/", "h", $string);
  $string = preg_replace("/^Kh/", "H", $string);
  return $string;
}


function main_fb_auth()
{
	sys::setTpl();
	global $_db, $_cfg, $_err, $_user, $_cookie;

$s = file_get_contents('http://ulogin.ru/token.php?token=' . $_REQUEST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
$user = json_decode($s, true);
//$user['network'] - соц. сеть, через которую авторизовался пользователь
//$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
//$user['first_name'] - имя пользователя
//$user['last_name'] - фамилия пользователя

$return = $_REQUEST['return'];

$uid = $user['uid'];
$fname = $user['first_name'];
$lname = $user['last_name'];


$dob = date('Y-m-d', strtotime($user['bdate']));
if ($user['sex']=='2')
{
	$gender = 'man';
} else {
	$gender = 'woman';
}
$email = $user['email'];
$photo = $user['photo_big'];

if ($email)
{
		$q="
			SELECT
				login
			FROM `#__sys_users`
			WHERE
				email = '".$email."'
		";

		$r=$_db->query($q);
		$result['objects']=array();

		while($obj=$_db->fetchAssoc($r))
		{
			$userlogin = $obj['login'];
		}

	if ($userlogin)
	{
		$err=sys::authorizeUserSoc($userlogin);
 		if($err===true)
 			header( 'Location: '.$return); 
		else
			echo $_err=sys::translate('sys::'.$err);
	}

} else {
$tfname = strtolower(Transliterate($fname));
$tlname = strtolower(Transliterate($lname));

$login = substr($tfname, 0,1).$tlname;

		$q="
			SELECT
				login
			FROM `#__sys_users`
			WHERE
				login = '".$login."'
		";

		$r=$_db->query($q);
		$result['objects']=array();


		while($obj=$_db->fetchAssoc($r))
		{
			$userlogin = $obj['login'];
		}


	if ($userlogin)
	{
		$err=sys::authorizeUserSoc($userlogin);

	 		if($err===true)
				header( 'Location: '.$return);
			else
				echo $_err=sys::translate('sys::'.$err);
	}
}

if(!$userlogin)
{
	$tfname = strtolower(Transliterate($fname));
	$tlname = strtolower(Transliterate($lname));
	$login = substr($tfname, 0,1).$tlname;

		$q="
			SELECT
				max(id) as id
			FROM `#__sys_users`
		";

		$r=$_db->query($q);

		while($obj=$_db->fetchAssoc($r))
		{
			$newid = $obj['id']+1;
		}

		if ($photo)
		{
			$content = file_get_contents($photo);
		
			$fp = fopen("/var/www/html/multiplex/multiplex.in.ua/public/main/users/user_".$newid.".jpg", "w");
			fwrite($fp, $content);
			fclose($fp);
			$np = "user_".$newid.".jpg";
			sys::resizeImage($_cfg['main::users_dir'].$np,false,65,65);
		}  else {
			$np = '';
		}
		
		
		if(!$email)
		{
			$email = '';
		}

		if($login != "") // patch by Mike
		{
			$q="INSERT INTO `#__sys_users` (`id`, `date_reg`, `login`, `password`, `email`, `on`, `date_last_visit`, `lang_id`)
			VALUES ('".$newid."','".date("Y-m-d H:i:s")."','".$login."','".md5($login)."','".$email."','1','".date("Y-m-d H:i:s")."','1')
			";
			$r=$_db->query($q);
	
			$q="INSERT INTO `#__sys_users_cfg` (`user_id`, `sys_timezone`, `main_city_id`)
			VALUES ('".$newid."','Europe/Kiev','1')
			";
			$r=$_db->query($q);
	
			$q="INSERT INTO `#__sys_users_data` (`user_id`, `main_first_name`, `main_last_name`, `main_sex`, `main_birth_date`, `main_avatar`)
			VALUES ('".$newid."','".$fname."','".$lname."','".$gender."','".$dob."','".$np."')
			";
			$r=$_db->query($q);
	
			$q="INSERT INTO `#__sys_users_groups` (`user_id`, `group_id`)
			VALUES ('".$newid."','8')
			";
			$r=$_db->query($q);
		}

		$err=sys::authorizeUserSoc($login);
					header( 'Location: '.$return);
}
}
?>