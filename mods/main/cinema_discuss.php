<?
function main_cinema_discuss()
{
	sys::useLib('main::cinemas');
	sys::useLib('main::discuss');
	sys::useLib('main::users');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('cinema_id','int');

	define('_CANONICAL',main_cinemas::getCinemaDiscussUrl($_GET['cinema_id']),true);

	if(!$_GET['cinema_id'])
		return 404;


	$result['cinema']=$_db->getValue('main_cinemas','title',intval($_GET['cinema_id']),true);
	$meta['cinema']=$result['cinema'];
	$result['meta']=sys::parseModTpl('main::cinema_discuss','page',$meta);
	if(!$result['cinema'])
		return 404;
	sys::jsInclude('sys::bb');
	return $result;
}
?>