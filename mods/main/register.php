<?
function main_register()
{
	define('_NOINDEX','1');
	sys::useLib('main::users');
	sys::useLib('main::questions');
	sys::useLib('main::captcha');
	sys::useLib('main::genres');;
	sys::useLib('sys::form');
	sys::useLib('sys::langs');
	sys::useLib('main::countries');
	sys::useLib('sys::check');
	sys::useJs('sys::gui');
	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;
    
	if($_user['id']!=2)
		sys::redirect(_ROOT_URL,false);

	$result['meta']=sys::parseModTpl('main::register','page');

	//login
	$fields['login']['input']='textbox';
	$fields['login']['unique']=true;
	$fields['login']['table']='sys_users';
	$fields['login']['type']='text';
	$fields['login']['req']=true;
	$fields['login']['max_chars']=16;
	$fields['login']['min_chars']=2;
	$fields['login']['title']=sys::translate('main::login');

	//password
	$fields['password']['input']='password';
	$fields['password']['type']='text';
	$fields['password']['req']=true;
	$fields['password']['showval']=true;
	$fields['password']['max_chars']=16;
	$fields['password']['min_chars']=6;
	$fields['password']['title']=sys::translate('main::password');

	//password2
	$fields['password2']['input']='password';
	$fields['password2']['type']='text';
	$fields['password2']['req']=true;
	$fields['password2']['max_chars']=16;
	$fields['password2']['min_chars']=6;
	$fields['password2']['title']=sys::translate('main::password_again');

	//email
	$fields['email']['input']='textbox';
	$fields['email']['unique']=true;
	$fields['email']['table']='sys_users';
	$fields['email']['type']='email';
	$fields['email']['req']=true;
	$fields['email']['title']=sys::translate('main::email');

	//email2
	$fields['email2']['input']='textbox';
	$fields['email2']['type']='email';
	$fields['email2']['req']=false; // true
	$fields['email2']['title']=sys::translate('main::email_again');

	//nickname
	$fields['nickname']['input']='textbox';
	$fields['nickname']['type']='text';
	$fields['nickname']['max_chars']=55;
	$fields['nickname']['req']=false; // true
	$fields['nickname']['title']=sys::parseModTpl('main::nickname');

	//firstname
	$fields['firstname']['input']='textbox';
	$fields['firstname']['type']='text';
	$fields['firstname']['max_chars']=55;
	$fields['firstname']['req']=false; // true
	$fields['firstname']['title']=sys::translate('main::firstname');

	//lastname
	$fields['lastname']['input']='textbox';
	$fields['lastname']['type']='text';
	$fields['lastname']['max_chars']=55;
	$fields['lastname']['req']=false; // true
	$fields['lastname']['title']=sys::translate('main::lastname');

	//phone
	$fields['phone']['input']='textbox';
	$fields['phone']['type']='phone';
	$fields['phone']['req']=false;	
	$fields['phone']['html_params']='tabindex="9" autocomplete="off" id="phone" placeholder="380651234567" maxlength=12'; // !!! 3 parameter in sys_form::parseField() overload this value !!!
	$fields['phone']['title']=sys::translate('main::phone');

	//birth_date
	$fields['birth_date']['input']='datebox';
	$fields['birth_date']['type']='date';
	$fields['birth_date']['icon']='';
	$fields['birth_date']['format']=$_cfg['sys::date_format'];
	$fields['birth_date']['req']=false; // true
	$fields['birth_date']['title']=sys::translate('main::birth_date');

	//sex
	$fields['sex']['input']='selectbox2';
	$fields['sex']['type']='text';
	$fields['sex']['values']['man']=sys::translate('main::man');
	$fields['sex']['values']['woman']=sys::translate('main::woman');
	$fields['sex']['req']=false; // true
	$fields['sex']['title']=sys::translate('main::sex');
	$fields['sex']['empty']='';

	//city_id
	$fields['city_id']['input']='selectbox2';
	$fields['city_id']['type']='int';
	$fields['city_id']['values']=main_countries::getCountryCitiesTitles(29);
	$fields['city_id']['req']=false; // true
	$fields['city_id']['title']=sys::translate('main::city');

	//lang_id
	$fields['lang_id']['input']='selectbox2';
	$fields['lang_id']['type']='int';
	foreach ($_langs as $lang_id=>$lang) $fields['lang_id']['values'][$lang_id]=$lang['title'];
	$fields['lang_id']['req']=false; // true
	$fields['lang_id']['title']=sys::translate('main::language');

	//timezone
	$fields['timezone']['input']='selectbox2';
	$fields['timezone']['type']='text';
	$fields['timezone']['req']=false; // true
	$fields['timezone']['value']='Europe/Kiev';
	$fields['timezone']['title']=sys::translate('main::timezone');
	$timezones=timezone_identifiers_list();
	foreach ($timezones as $zone)
	{
		$fields['timezone']['values'][$zone]=$zone;
	}
	unset($zones, $zone);

	$fields=sys_form::parseFields($fields, $_POST, false);

	$result['genres']=main_genres::getGenresTitles();
	$result['question']=main_questions::getQuestion();
	$result['fields']=$fields;
	$result['registred']=false;

	if(isset($_POST['cmd_register']))
	{
	   $_POST['phone'] = str_replace('+','',$_POST['phone']);
	   $_err=sys_check::checkValues($fields,$_POST);
       
	   /*
		if(!main_questions::checkAnswer($_POST['question_id'],$_POST['answer'])) {
            $_err['question'] = array(
                'title' => sys::translate('main::control_question'),
                'text' => sys::translate('main::wrong_control_answer')
            );
        }
		*/
		if(!main_captcha::check()) {
            $_err['captcha'] = array(
                'title' => sys::translate('main::captcha'),
                'text' => sys::translate('main::wrong_captcha')
            );
        }
		
		if($_POST['password']!=$_POST['password2']) {
            $_err['password2'] = array(
                'title' => $fields['password2']['title'],
                'text' => sys::translate('main::registerAttentionDescrPassword2')
            );
        }
//		if($_POST['email']!=$_POST['email2']) {
//            $_err['email2'] = array(
//                'title' => $fields['email2']['title'],
//                'text' => sys::translate('main::registerAttentionDescrEmail2')
//            );
//        }
		if(sys::date2Timestamp($_POST['birth_date'])>time()-10*_YEAR) {
            $_err=sys::translate('main::invalid_birth_date');
        }
        
		if(!$_err)
		{

			$_db->begin();
			//Основная инфа
			if(!$_err && !$_db->query("
				INSERT INTO `#__sys_users`
				(
					`date_reg`,
					`login`,
					`password`,
					`email`,
					`on`,
					`lang_id`
				)
				VALUES
				(
					'".gmdate('Y-m-d H:i:s')."',
					'".mysql_real_escape_string($_POST['login'])."',
					'".md5($_POST['password'])."',
					'".mysql_real_escape_string($_POST['email'])."',
					1,
					'".intval($_POST['lang_id'])."'
				)
			"))
			{
				$_db->rollback();
				$_err=sys::translate('main::db_err');
			}
			else
				$user_id=$_db->last_id;

			//Группы
			if(!$_err && !$_db->query("
				INSERT INTO `#__sys_users_groups`
				(
					`user_id`,
					`group_id`
				)
				VALUES
				(
					'".$user_id."',
					'8'
				)
			"))
			{
				$_db->rollback();
				$_err=sys::translate('main::db_err');
			}

			//Настройки
			if(!$_err && !$_db->query("
				INSERT INTO `#__sys_users_cfg`
				(
					`user_id`,
					`sys_timezone`,
					`main_city_id`
				)
				VALUES
				(
					'".$user_id."',
					'".mysql_real_escape_string($_POST['timezone'])."',
					'".intval($_POST['city_id'])."'
				)

			"))
			{
				$_db->rollback();
				$_err=sys::translate('main::db_err');
			}

			//Данные
			if(!$_err && !$_db->query("
				INSERT INTO `#__sys_users_data`
				(
					`user_id`,
					`main_nick_name`,
					`main_first_name`,
					`main_last_name`,
					`main_sex`,
					`main_birth_date`,
					`main_phone`
				)
				VALUES
				(
					'".$user_id."',
					'".mysql_real_escape_string($_POST['nickname'])."',
					'".mysql_real_escape_string($_POST['firstname'])."',
					'".mysql_real_escape_string($_POST['lastname'])."',
					'".mysql_real_escape_string($_POST['sex'])."',
					'".mysql_real_escape_string(sys::date2Db($_POST['birth_date'],false,$_cfg['sys::date_format']))."',
					'".mysql_real_escape_string($_POST['phone'])."'
				)

			"))
			{
				$_db->rollback();
				$_err=sys::translate('main::db_err');
			}

			if(!$_err)
			{
				if(isset($_POST['genres']) && is_array($_POST['genres']))
				{
					foreach ($_POST['genres'] as $genre_id=>$val)
					{
						if(!$_err && !$_db->query("
							INSERT INTO `#__main_users_genres`
							(
								`user_id`,
								`genre_id`
							)
							VALUES
							(
								'".$user_id."',
								'".intval($genre_id)."'
							)
						"))
						{
							$_db->rollback();
							$_err=sys::translate('main::db_err');
							break;
						}
					}
				}
			}
			if(!$_err)
			{
				$_db->commit();
				sys::authorizeUser($_POST['login'],$_POST['password']);
				$result['imregistred']=true;
			}

		}
	}

	return $result;
}
?>