<?

function maxsite_str_word($text, $counttext = 10, $sep = ' ') {
	$words = split($sep, $text);
	if ( count($words) > $counttext )
		$text = join($sep, array_slice($words, 0, $counttext)).'...';
	return $text;
}


function main_search_films()
{
	sys::useLib('main::genres');
	sys::useLib('main::persons');
	sys::useLib('main::cinemas');
	sys::useLib('main::countries');
	sys::useLib('main::news');
	sys::useLib('sys::form');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('keyword');
	$result=array(0);

	$result['meta']=sys::parseModTpl('main::search','page');

	$_GET['keywords'] = strip_tags($_GET['keywords']);
	$_GET['keywords'] = urldecode($_GET['keyword']);


	$q="
		SELECT
			flm.id,
			flm.name,
			flm.title_orig,
			flm.year,
			flm.rating,
			flm.votes,
			lng.title,
			lng.intro
		FROM
			`grifix_main_films` AS `flm`
		LEFT JOIN
			`grifix_main_films_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	if($_GET['keywords'])
		$q.="WHERE
			`name` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `title_orig` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `title_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `title_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `title` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `title` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `title` LIKE '".mysql_real_escape_string($_GET['keywords'])."'";

	$q.=" ORDER BY flm.total_shows DESC";




	$result['pages']=sys_pages::pocess($q,$_cfg['main::films_on_search']);



	$r=$_db->query($q.$result['pages']['limit']);



	while($obj=$_db->fetchAssoc($r))
	{

		$qw=$_db->query("
			SELECT
				pst.image as `poster`

			FROM `#__main_films_posters` AS `pst`

			WHERE pst.film_id=".intval($obj['id'])."
			AND pst.order_number=1
			LIMIT 1
		");

		$poster = '';

		while($object=$_db->fetchAssoc($qw))
		{
			$poster = $object['poster'];
		}

		if ($poster)
		{
			$obj['poster'] = _ROOT_URL.'public/main/films/x2_'.$poster.'';
		} else {
			$obj['poster'] = _ROOT_URL.'ims/kt_logo.jpg';
		}

		$qw=$_db->query("
			SELECT
				lng.title as `country`

			FROM `#__main_films_countries` AS `cnt`

			LEFT JOIN
				 `#__main_countries_lng` AS `lng`
			ON
				cnt.country_id=lng.record_id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE cnt.film_id=".intval($obj['id'])."
			LIMIT 1
		");



		while($object=$_db->fetchAssoc($qw))
		{
			$obj['country'] = $object['country'];

		}

		$qw=$_db->query("
			SELECT
				lng.title as `genre`

			FROM `#__main_films_genres` AS `cnt`

			LEFT JOIN
				 `#__main_genres_lng` AS `lng`
			ON
				cnt.genre_id=lng.record_id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE cnt.film_id=".intval($obj['id'])."
		");

        $obje['genres'] = array();

		while($object=$_db->fetchAssoc($qw))
		{
			$obje['genres'][] = $object['genre'];
		}

		$obj['genres'] = implode(', ', $obje['genres']);


        $object['actors'] = array();
        $obje['actors'] = array();

        $pi = 0;
			$obje['actors']=main_films::getFilmActors($obj['id']);

            if (is_array($obje['actors'][0]))
            {

	            for ($i=0;$i<=1;$i++)
	            {
	            	if ($obje['actors'][$i]['url'])
	            	{
						$object['actors'][] = '<a href="'.$obje['actors'][$i]['url'].'">'.$obje['actors'][$i]['fio'].'</a>';
						$pi++;
					}
	            }

            }

		if ($pi>0)
		{
			$obj['actors'] = sys::translate('main::actors').': '.implode(', ', $object['actors']);
		}

        $object['directors'] = array();
        $obje['directors'] = array();

        $di = 0;
			$obje['directors']=main_films::getFilmDirectors($obj['id']);


            if (is_array($obje['directors'][0]))
            {

	            for ($i=0;$i<1;$i++)
	            {
						$object['directors'][] = '<a href="'.$obje['directors'][$i]['url'].'">'.$obje['directors'][$i]['fio'].'</a>';
						$di++;
	            }

            }

		if ($di>0)
		{
			$obj['directors'] = sys::translate('main::directors').': '.implode(', ', $object['directors']);
		}

		$obj['third'] = array();
		if ($obj['directors'])
		$obj['third'][] = $obj['directors'];
		if ($obj['actors'])
		$obj['third'][] = $obj['actors'];

		if ($obj['directors'] || $obj['actors'])
		$obj['third_row'] = implode(', ', $obj['third']);







		if ($obj['rating']>0)
		{
			$obj['rate'] = sys::translate('main::rating').': <strong>'.round($obj['rating'], 1).'</strong> / '.sys::translate('main::votes').': '.$obj['votes'].'<br>';
		}


		$obj['round'] = '';

		if ($obj['title_orig'])
		{
			$obj['round'] .= $obj['title_orig'];
		}

		if ($obj['year']>0)
		{
			$obj['year'] = $obj['year'];
		} else {
			$obj['year']='';
		}

		$obj['second'] = array();
		if ($obj['year'])
		$obj['second'][] = $obj['year'];
		if ($obj['country'])
		$obj['second'][] = $obj['country'];
		if ($obj['genres'])
		$obj['second'][] = $obj['genres'];

		if ($obj['year'] || $obj['country'] || $obj['genres'])
		$obj['second_row'] = implode(' | ', $obj['second']).'<br><br>';



		$obj['url']=main_films::getFilmUrl($obj['id']);
		$result['films'][]=$obj;
	}






	$q="
		SELECT
			flm.id,
			flm.lastname_orig,
			flm.firstname_orig,
			flm.rating,
			flm.rating_votes,
			flm.birthdate,
			lng.biography,
			lng.lastname,
			lng.firstname
		FROM
			`grifix_main_persons` AS `flm`
		LEFT JOIN
			`grifix_main_persons_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	if($_GET['keywords'])
		$q.="WHERE
			`lastname_orig` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `firstname_orig` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `lastname` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `firstname` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			";

	$q.=" ORDER BY flm.total_shows DESC LIMIT 5";
	$r=$_db->query($q);


	while($obj=$_db->fetchAssoc($r))
	{

		$qw=$_db->query("
			SELECT
				pst.image as `poster`

			FROM `#__main_persons_photos` AS `pst`

			WHERE pst.person_id=".intval($obj['id'])."
			AND pst.order_number=1
			LIMIT 1
		");

		$obj['title'] = $obj['firstname'].' '.$obj['lastname'];

		$poster = '';

		while($object=$_db->fetchAssoc($qw))
		{
			$poster = $object['poster'];
		}

		if ($poster)
		{
			$obj['poster'] = _ROOT_URL.'public/main/resize2.php?f='.$poster.'&w=60';
		} else {
			$obj['poster'] = _ROOT_URL.'ims/kt_logo.jpg';
		}





		if ($obj['rating']>0)
		{
			$obj['rate'] = sys::translate('main::rating').': <strong>'.round($obj['rating'], 1).'</strong> / '.sys::translate('main::votes').': '.$obj['rating_votes'].'<br>';
		}


		$obj['biography'] = strip_tags(maxsite_str_word($obj['biography'], 30).'...');

		$obj['round'] = '';

		if ($obj['firstname_orig'])
		{
			$obj['round'] .= '<br><font style="color: #AAA; font-size: 10px;">'.$obj['firstname_orig'].' '.$obj['lastname_orig'].'</font><br>';
		}





		$obj['url']=main_persons::getPersonUrl($obj['id']);
		$result['persons'][]=$obj;
	}



	$q="
		SELECT
			flm.id,
			flm.name,
			flm.phone,
			lng.title,
			flm.site,
			lng.address,
			ct.title as `city`
		FROM
			`grifix_main_cinemas` AS `flm`
		LEFT JOIN
			`grifix_main_cinemas_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		LEFT JOIN
			`grifix_main_countries_cities_lng` AS `ct`
		ON
			ct.record_id=flm.city_id
		AND
			ct.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	if($_GET['keywords'])
		$q.="WHERE
			lng.`title` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR lng.`title` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR lng.`title` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `name` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			";

	$q.=" ORDER BY lng.title LIMIT 5";
	$r=$_db->query($q);



	while($obj=$_db->fetchAssoc($r))
	{

		$obj['round'] = '';

		if ($obj['city'])
		{
			$obj['round'] .= '<font style="color: #AAA; ">('.$obj['city'].')</font><br><br>';
		}


			if ($obj['address'])
         $obj['adress'] = '<strong>'.sys::translate('main::address').':</strong> '.$obj['address'].'<br>';
			if ($obj['phone'])
         $obj['phone'] = '<strong>'.sys::translate('main::phone').':</strong> '.$obj['phone'].'<br>';
			if ($obj['site'])
         $obj['website'] = '<strong><a href="'.$obj['site'].'" target="_blank"> '.sys::translate('main::site').'</a>';

		$obj['url']=main_cinemas::getCinemaUrl($obj['id']);
		$result['cinemas'][]=$obj;
	}



	$q="
		SELECT
			art.id,
			art.date,
			art.user_id,
			art.exclusive,
			art.small_image AS `image`,
			art_lng.title,
			art_lng.intro
		FROM `#__main_news_articles` AS `art`

		LEFT JOIN
			`#__main_news_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_news` AS `nws`
		ON art.news_id=nws.id
	";


	if($_GET['keywords'])
		$q.="WHERE
			art_lng.title LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR art_lng.title LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR art_lng.title LIKE '%".mysql_real_escape_string($_GET['keywords'])."'

			";

	$q.="
		ORDER BY art.date DESC  LIMIT 5
	";

	$r=$_db->query($q);



	while($obj=$_db->fetchAssoc($r))
	{

		$obj['round'] = '';

		if($obj['image'])
			$obj['poster']=$_cfg['main::news_url'].$obj['image'];
		else
			$obj['poster']=main::getNoImage('x1');

		$obj['date']='<br><font style="color: #AAA; font-size: 10px;">'.date('d.m.Y', strtotime($obj['date'])).'</font><br>';

		$obj['intro'] = strip_tags($obj['intro']);

		$obj['url']=main_news::getArticleUrl($obj['id']);
		$result['news'][]=$obj;
	}


   	$result['films_title'] = sys::translate('main::films');
   	$result['persons_title'] = sys::translate('main::persons');
   	$result['cinemas_title'] = sys::translate('main::cinemas');
   	$result['news_title'] = sys::translate('main::news');
	return $result;
}
?>