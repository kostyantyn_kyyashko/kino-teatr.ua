<?
function main_ajx_vote_expect()
{
	sys::setTpl();
	sys::useLib('main::films');
	global $_user, $_db, $_cfg;

	if(!$_POST['object_id']) return false;
	$film_id = intval($_POST['object_id']);

	if($_user['id']==2) 
		$marker = main_films::SetExpectUnregistered($film_id, $_POST['mark'], $is_showing);
	 else 
	 	$marker = main_films::SetExpectRegistered($film_id, $_user['id'], $_POST['mark']);
	 	 	
	$marker["yes"] = round(0+$marker["expect_yes"]*100/($marker["expect_yes"]+$marker["expect_no"]));
	$marker["no"] = 100 - $marker["yes"];
	
	echo json_encode($marker);
}
