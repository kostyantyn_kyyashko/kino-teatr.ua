<?
function main_person_wallpapers()
{
	sys::useLib('main::persons');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('person_id','int');
	sys::filterGet('wallpaper_id','int');
	sys::filterGet('page','int');

	define('_CANONICAL',main_persons::getPersonWallpapersUrl($_GET['person_id']),true);

	if(!$_GET['person_id'])
		return 404;
/*
	if(!$_GET['wallpaper_id'])
	{
		if($_GET['page'])
			$limit=intval($_cfg['main::images_on_page']*(intval($_GET['page'])-1));
		else
			$limit=0;
		$q="
			SELECT `wallpaper_id` FROM `#__main_films_wallpapers_persons`
			WHERE `person_id`=".intval($_GET['person_id'])."
			ORDER BY `wallpaper_id` DESC
			LIMIT ".$limit.",1
		";
		$r=$_db->query($q);
		list($_GET['wallpaper_id'])=$_db->fetchArray($r);
	}

	if(!$_GET['wallpaper_id'])
		return 404;
*/
	$r=$_db->query("
		SELECT
			wlp.image,
			wlp.image_1024x768,
			wlp.image_1280x800,
			wlp.image_1280x1024,
			wlp.image_1680x1050,
			flm_lng.title AS `film`,
			flm_lng.record_id AS `film_id`
		FROM
			`#__main_films_wallpapers` AS `wlp`
		LEFT JOIN `#__main_films_lng` AS flm_lng
		ON flm_lng.record_id=wlp.film_id
		AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE wlp.id=".intval($_GET['wallpaper_id'])."
		AND wlp.public=1
	");
	$result=$_db->fetchAssoc($r);
//	if(!$result)
//		return 404;

	$result['person']=main_persons::getPersonFio($_GET['person_id']);

	$meta['person']=$result['person'];
	$meta['wallpaper_id']=doubleval($_GET['wallpaper_id']);	
	$meta['film']=$result['film'];
	$result['title'] = sys::translate('main::wallpapers').' '.$result['person'];
	$result['meta']=sys::parseModTpl('main::person_wallpapers'.($meta['wallpaper_id']?'_selected':''),'page',$meta);
	$result['image']=$_cfg['main::films_url'].'x4_'.$result['image'];

	if($result['image_1024x768'])
		$result['image_1024x768']=$_cfg['main::films_url'].$result['image_1024x768'];

	if($result['image_1280x800'])
		$result['image_1280x800']=$_cfg['main::films_url'].$result['image_1280x800'];

	if($result['image_1280x1024'])
		$result['image_1280x1024']=$_cfg['main::films_url'].$result['image_1280x1024'];

	if($result['image_1680x1050'])
		$result['image_1680x1050']=$_cfg['main::films_url'].$result['image_1680x1050'];

	$result['next_url']=false;
	$result['prev_url']=false;
	$result['alt']=$result['person'].' '.sys::translate('main::wallpaper_from_film').' &quot;'.$result['film'].'&quot;';
	$result['person_url']=main_persons::getPersonUrl($_GET['person_id']);
	$result['film_url']=main_films::getFilmUrl($result['film_id']);


	if($next_id=main_persons::getNextWallpaperId($_GET['person_id'],$_GET['wallpaper_id']))
		$result['next_url']=main_persons::getWallpapersUrl($next_id,$_GET['person_id']);
	if($prev_id=main_persons::getPrevWallpaperId($_GET['person_id'],$_GET['wallpaper_id']))
		$result['prev_url']=main_persons::getWallpapersUrl($prev_id,$_GET['person_id']);

	$page_url = main_persons::getPersonWallpaperUrl($_GET['person_id']);
	$pu = $page_url."?wallpaper_id=".intval($_GET["wallpaper_id"]);
	if ($_GET['page'])	$qp = 'page='.$_GET['page'];
	else  $qp="";

	//Получить инфу об остальных фотках-----------------------------------------------
	$q="
		SELECT wlp.id,wlp.image
		FROM `#__main_films_wallpapers_persons` AS `wlp_prs`

		LEFT JOIN `#__main_films_wallpapers` AS `wlp`
		ON wlp.id=wlp_prs.wallpaper_id

		WHERE wlp_prs.person_id=".$_GET['person_id']."
		AND wlp.public=1
		ORDER BY wlp.id DESC
	";
	
/*	$result['pages']=sys_pages::pocess($q,$_cfg['main::images_on_page'],false);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['previews']=array();
	$i=1;
*/
	// Заменить старые ссылки на страницы на новые
	$result['pages']=sys_pages::pocess($q,$_cfg['main::images_on_page'],false);
	$result['pages']['first_page'] = sys_pages::setPageUrl($result['pages']['first_page'], $pu);
	$result['pages']['prev_page']  = sys_pages::setPageUrl($result['pages']['prev_page'],  $pu);
	$result['pages']['next_page']  = sys_pages::setPageUrl($result['pages']['next_page'],  $pu);
	$result['pages']['last_page']  = sys_pages::setPageUrl($result['pages']['last_page'],  $pu);
	foreach ($result['pages']['pages'] as $i=>$p)
	 $result['pages']['pages'][$i] = sys_pages::setPageUrl($result['pages']['pages'][$i],  $pu);

	$r=$_db->query($q.$result['pages']['limit']);
	$result['previews']=array();
	$result['wallpaper_id']=$_REQUEST['wallpaper_id'];
	$i=1;
	while ($obj=$_db->fetchAssoc($r))
	{
		$from_path = $_cfg['main::films_dir'].$obj['image'];
		$image_info = getImageSize($from_path);

		$obj['width']=$image_info[0];
		$obj['height']=$image_info[1];
		if ($i!=1)
			$obj['number']='- '.sys::translate('main::photo_number').' '.$i;

		$obj['weight']=round(filesize($from_path)/1024);

  	    $image="x2_".$obj['image'];
		if(!file_exists($_cfg['main::films_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$obj['image'] = $image;

		$obj['padding']=$pad;
		
		$obj['url']=$page_url."?wallpaper_id=".doubleval($obj['id']);
		if($qp) $obj['url'] .= ((strpos($obj['url'],"?")===false)?"?":"&").$qp;

		$obj['break']=$i%3;
		$obj['i']=$i;
//		if($_GET['page'])
//			$obj['url'].='&page='.$_GET['page'];
//		$obj['url']=sys::rewriteUrl($obj['url']);
		$result['previews'][]=$obj;
		$i++;
	}
	//============================================================

	main::countShow($_GET['person_id'],'person');
	return $result;
}
?> 
