<?
function main_forgot_pass()
{
	define('_NOINDEX','1');
	sys::useLib('main::users');
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useJs('sys::gui');
	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;

	$result['meta']=sys::parseModTpl('main::forgot_pass','page');

	//email
	$fields['email']['input']='textbox';
	$fields['email']['type']='email';
	$fields['email']['req']=true;
	$fields['email']['title']=sys::translate('main::email');


	$fields=sys_form::parseFields($fields, $_POST, false);

	$result['fields']=$fields;
	$result['sent']=false;

	if(isset($_POST['cmd_restore']))
	{
		$_err=sys_check::checkValues($fields,$_POST);

		if(!$_err)
		{
			$user_id=$_db->getValue('sys_users','id',"`email`='".mysql_real_escape_string($_POST['email'])."'");
			if(!$user_id)
				$_err=sys::translate('main::user_email_not_exist');
		}

		if(!$_err)
		{
			$code=uniqid($user_id.'_');
			$_db->query("
				UPDATE `#__sys_users` SET `confirm_code`='".$code."'
				WHERE `id`=".$user_id."
			");
			$var['url']=sys::rewriteUrl('?mod=main&act=new_password&code='.$code);
			sys::sendMailTpl($user_id,'main::forgot_pass',$var);
			$result['sent']=true;
		}
	}

	return $result;
}
?>