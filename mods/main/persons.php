<?
function main_persons()
{

function maxsite_str_word($text, $counttext = 10, $sep = ' ') {
	$words = split($sep, $text);
	if ( count($words) > $counttext )
		$text = join($sep, array_slice($words, 0, $counttext)).'...';
	return $text;
}

	global $_db, $_cfg, $_err;
	sys::useLib('sys::form');
	sys::useLib('main::persons');
	sys::useLib('sys::pages');
	sys::filterGet('lastname');
	sys::filterGet('letter');
	sys::filterGet('order_by','text','fio.asc');

	if ($_GET['page']!='' || $_GET['lastname']!='')
	{
		define('_NOINDEX',1,true);
	}


		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		define('_CANONICAL',_ROOT_URL.'persons.phtml',true);
		} else {
		define('_CANONICAL',_ROOT_URL.$lang.'/persons.phtml',true);
		}

	$cache_name='main_persons_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::persons_page_cache_period']*60))
		return unserialize($cache);

	$result=array();


	if ($_GET['page'])
	{
		$meta['page'] = sys::translate('main::page').' '.$_GET['page'];
	} else {
		$meta['page'] = '';
	}

	if ($_GET['letter'])
	{
		$meta['letter'] = sys::translate('main::pers_letter').' '.mysql_real_escape_string(urldecode($_GET['letter']));
	} else {
		$meta['letter'] = '';
	}


	$result['meta']=sys::parseModTpl('main::persons','page', $meta);

	//Определение направления сортировки
	$order_by=explode('.',$_GET['order_by']);
	$order_fields=array('fio','rating');
	if($order_by[1]=='asc')
	{
		$result['order']='asc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.desc');
		}

	}
	else
	{
		$result['order']='desc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.asc');
		}
	}
	$result['order_field']=$order_by[0];

	$order_by=sys::parseOrderBy($_GET['order_by']);
	//=====================================================

	//Поля поисковой формы-------------------------------------
	$result['fields']['lastname']['input']='textbox';
	$result['fields']['lastname']['value']=urldecode($_GET['lastname']);
	$result['fields']['lastname']['title']=sys::translate('main::lastname');
	//===================================================================

	//Формирование запроса-----------------------------------------------
	$q="
		SELECT
			prs.id,
			prs.user_id,
			CONCAT(prs.lastname_orig,' ',prs.firstname_orig) AS `fio_orig`,
			CONCAT(prs_lng.lastname,' ',prs_lng.firstname) AS `fio`,
			prs.birthdate,
			prs.rating,
			prs.rating_votes,
			prs_lng.biography as `bio`

		FROM `#__main_persons` AS `prs`
		LEFT JOIN `#__main_persons_lng` AS `prs_lng`
		ON prs_lng.record_id=prs.id
		AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	//Условия
	$q.=" WHERE 1=1";

	if($_GET['lastname'])
	{
		$q.=" AND (prs.lastname_orig LIKE '%".mysql_real_escape_string(urldecode($_GET['lastname']))."%'
			OR prs_lng.lastname LIKE '%".mysql_real_escape_string(urldecode($_GET['lastname']))."%')";
	}

	if($_GET['letter'])
	{
		$q.=" AND (prs_lng.lastname LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%'
			OR prs.lastname_orig LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%')";
	}

	if($_GET['user_id'])
	{
		$q.=" AND prs.user_id=".intval($_GET['user_id']);
	}

	$q.=' ORDER BY prs.total_shows DESC';
	//===========================================================

	$result['pages']=sys_pages::pocess($q,$_cfg['main::persons_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$qw=$_db->query("
			SELECT
				pst.image as `poster`

			FROM `#__main_persons_photos` AS `pst`

			WHERE pst.person_id=".intval($obj['id'])."
			AND pst.order_number=1
			LIMIT 1
		");

		$obj['title'] = $obj['firstname'].' '.$obj['lastname'];

		$poster = '';

		while($object=$_db->fetchAssoc($qw))
		{
			$poster = $object['poster'];
		}

// inserted by Mike begin
		if($poster)
		{
    	$image="x3_".$poster; 
		if(!file_exists('./public/main/persons/'.$image)) $image = $_cfg['sys::root_url'].'images/userpic.jpg';
		 else $image=$_cfg['main::persons_url'].$image;
		} else {
         	$image = $_cfg['sys::root_url'].'images/userpic.jpg';
		}
		$obj['poster'] = $image;
// inserted by Mike end
		
/*		// comments by Mike
		if ($poster)
		{
			$obj['poster'] = '//kino-teatr.ua/public/main/resize2.php?f='.$poster.'&w=60';
		} else {
			$obj['poster'] = '//kino-teatr.ua/images/userpic.jpg';
		}
*/
		$obj['biography'] = strip_tags(maxsite_str_word($obj['bio'], 30).'...');
		$obj['birthdate']=sys::db2Date($obj['birthdate'],false,$_cfg['sys::date_format']);
		$obj['url']=main_persons::getPersonUrl($obj['id']);
		$obj['rating']=main_persons::formatPersonRating($obj['rating']);
		$obj['rating_url']=main_persons::getPersonRatingUrl($obj['id']);
		$result['objects'][]=$obj;
	}

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>