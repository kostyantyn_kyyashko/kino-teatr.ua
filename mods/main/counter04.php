<?
function main_counter04()
{
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('film_id','int');

	$uri=explode('_',$_SERVER['REQUEST_URI']);
	$_GET['film_id']=$uri[1];

	if(!$_GET['film_id'])
		return 404;

	$r=$_db->query("
		SELECT
			imdb.rating as imdb_rating,
			imdb.votes as imdb_votes
		FROM `#__main_films` AS `flm`

		LEFT JOIN `#__main_films_imdb_to_kt` AS `imdb_kt`
		ON imdb_kt.kt_id=flm.id		
		
		LEFT JOIN `#__main_films_imdb` AS `imdb`
		ON imdb_kt.imdb_id=imdb.imdb_id				
		
		WHERE flm.id=".intval($_GET['film_id'])."
	");

	$film=$_db->fetchAssoc($r);


	if(!$film)
		return 404;

	if($film['imdb_rating']==0) $film['imdb_rating'] = "0 ";
	else $film['imdb_rating']= number_format($film['imdb_rating'], 1, '.', '');

	$img=imagecreatefromgif(_IMG_DIR.'counter/rating_imdb.gif');

	$font=_SECURE_DIR.'fonts/tahomabd.ttf';

	$color=imagecolorallocate($img,44,52,68);
	$fontSize=13;
	$text = $film['imdb_rating'];
	$box=imagettfbbox($fontSize,0,$font,$text);

	$x=80-$box[2];
	$y=22;
	imagettftext($img,$fontSize,0,$x,$y,$color,$font,$text);

	header('Content-type: image/gif');
	imagegif($img);
	imagedestroy($img);
	sys::setTpl();
}
?>