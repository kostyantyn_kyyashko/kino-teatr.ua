<?
function main_ajx_vote_person()
{
	sys::setTpl();
	sys::useLib('main::persons');
	global $_user, $_db, $_cfg;	
	
	if(!$_POST['object_id']) return false;

	$person_id = doubleval($_POST['object_id']);
	$mark = $_POST['mark'];
	
	if($_user['id']==2)	
		$marker = main_persons::setVoteUnregistered($person_id, $mark);
	else			
		$marker = main_persons::setVoteRegistered($person_id, $_user['id'], $mark);

	$marker['new_mark'] = $mark;
	$marker['user_id'] = $_user['id'];
	$marker['person_id'] = $person_id;

	echo json_encode(main_persons::fixMarkPerson($marker));
}

/*
#delete FROM `grifix_main_persons_rating_unregistered` where person_id=4;
#delete FROM `grifix_main_persons_rating` where person_id=4;
#update `grifix_main_persons` set rating=0, rating_sum=0, rating_votes=0 where id=4;

#SELECT rating, rating_sum, rating_votes FROM `grifix_main_persons` WHERE id =4;
#SELECT * FROM `grifix_main_persons_rating` WHERE person_id =4;
#SELECT * FROM `grifix_main_persons_rating_unregistered` WHERE person_id =4;
*/
?>
