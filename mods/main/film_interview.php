<?
function main_film_interview()
{
	sys::useLib('main::films');
	sys::useLib('main::interview');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');

	define('_CANONICAL',main_films::getFilminterviewUrl($_GET['film_id']),true);

	if(!$_GET['film_id'])
		return 404;

	$result['film']=$_db->getValue('main_films','title',intval($_GET['film_id']),true);
	if(!$result['film'])
		return 404;
	$result['serial']=$_db->getValue('main_films','serial',intval($_GET['film_id']),false);	

	main::countShow($_GET['film_id'],'film');

	$meta['film']=$result['film'];
	$result['meta']=sys::parseModTpl('main::film_interview','page',$meta);

	$q="
		SELECT
			art.id,
			art.date,
			art.small_image AS `image`,
			art.exclusive,
			art_lng.title,
			art_lng.intro
		FROM
			`#__main_interview_articles_films` AS `art_flm`

		LEFT JOIN
			`#__main_interview_articles` AS `art`
		ON
			art.id=art_flm.article_id

		LEFT JOIN
			`#__main_interview_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE
			art_flm.film_id=".intval($_GET['film_id'])."
		AND
			art.public=1
		AND
			art.date<'".gmdate('Y-m-d H:i:s')."'

		ORDER BY
			art.date DESC
	";

	$result['pages']=sys_pages::pocess($q,$_cfg['main::interview_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true,$_cfg['sys::date_format']);
		if($obj['image'])
			$obj['image']=$_cfg['main::interview_url'].$obj['image'];
		else
			$obj['image']=main::getNoImage('x1');
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_interview::getArticleUrl($obj['id']);
		$result['objects'][]=$obj;
	}

	return $result;
}
?>