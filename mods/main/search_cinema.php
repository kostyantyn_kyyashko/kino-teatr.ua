<?

function maxsite_str_word($text, $counttext = 10, $sep = ' ') {
	$words = split($sep, $text);
	if ( count($words) > $counttext )
		$text = join($sep, array_slice($words, 0, $counttext)).'...';
	return $text;
}


function main_search_cinema()
{
	sys::useLib('main::genres');
	sys::useLib('main::persons');
	sys::useLib('main::cinemas');
	sys::useLib('main::countries');
	sys::useLib('main::news');
	sys::useLib('sys::form');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('keyword');
	$result=array(0);

	$result['meta']=sys::parseModTpl('main::search','page');

	$_GET['keywords'] = strip_tags($_GET['keywords']);
	$_GET['keywords'] = urldecode($_GET['keyword']);


	$q="
		SELECT
			flm.id,
			flm.name,
			flm.phone,
			lng.title,
			flm.site,
			lng.address,
			ct.title as `city`
		FROM
			`grifix_main_cinemas` AS `flm`
		LEFT JOIN
			`grifix_main_cinemas_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		LEFT JOIN
			`grifix_main_countries_cities_lng` AS `ct`
		ON
			ct.record_id=flm.city_id
		AND
			ct.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	if($_GET['keywords'])
		$q.="WHERE
			lng.`title` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR lng.`title` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR lng.`title` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `name` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			";

	$q.=" ORDER BY lng.title";
	$result['pages']=sys_pages::pocess($q,$_cfg['main::films_on_search']);


	$r=$_db->query($q.$result['pages']['limit']);



	while($obj=$_db->fetchAssoc($r))
	{

		$obj['round'] = '';

		if ($obj['city'])
		{
			$obj['round'] .= $obj['city'];
		}


			if ($obj['address'])
         $obj['adress'] = '<strong>'.sys::translate('main::address').':</strong> '.$obj['address'].'<br>';
			if ($obj['phone'])
         $obj['phone'] = '<strong>'.sys::translate('main::phone').':</strong> '.$obj['phone'].'<br>';
			if ($obj['site'])
         $obj['website'] = '<a href="'.$obj['site'].'" target="_blank"> '.sys::translate('main::site').'</a>';

		$obj['url']=main_cinemas::getCinemaUrl($obj['id']);
		$result['cinemas'][]=$obj;
	}



   	$result['films_title'] = sys::translate('main::films');
   	$result['persons_title'] = sys::translate('main::persons');
   	$result['cinemas_title'] = sys::translate('main::cinemas');
   	$result['news_title'] = sys::translate('main::news');
	return $result;
}
?>