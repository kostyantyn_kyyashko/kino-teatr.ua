<?
function main_sitemap_cinemas()
{
	sys::useLib('main::cinemas');
	sys::useLib('sys::form');
	global $_db, $_cfg, $_err, $_user, $_cookie;



	$result=array();

	//Жанры----------------------------------------------------------------
	$r=$_db->query("
		SELECT
			*
		FROM
			`#__main_cinemas`
	");
	$result['cinemas']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['lastmod'] = date('Y-m-d');
		$obj['loc'] = main_cinemas::getCinemaUrl($obj['id']);
		$obj['locuk'] = main_cinemas::getCinemaUkUrl($obj['id']);
		$result['cinemas'][]=$obj;
	}

	$xml = '';


	$xml .= '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

foreach ($result['cinemas'] as $cinema)
{
	$xml .= '
<url>
	<lastmod>'.$cinema['lastmod'].'</lastmod>
	<loc>'.$cinema['loc'].'</loc>
	<priority>1</priority>
	<changefreq>daily</changefreq>
</url>';

}

	$xml .= '</urlset>';




	$xmluk = '';


	$xmluk .= '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

foreach ($result['cinemas'] as $cinema)
{
	$xmluk .= '
<url>
	<lastmod>'.$cinema['lastmod'].'</lastmod>
	<loc>'.$cinema['locuk'].'</loc>
	<priority>1</priority>
	<changefreq>daily</changefreq>
</url>';

}

	$xmluk .= '</urlset>';


	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/cinemas.xml";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $xml);
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/cinemas_uk.xml";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $xmluk);
	fclose($fh);

	exit;

	//return $result;
}
?>