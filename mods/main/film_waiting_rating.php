<?
function main_film_waiting_rating()
{
	sys::useLib('main::films');
	sys::useLib('main::users');
	sys::useLib('sys::form');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');
	sys::filterGet('order_by','text','mark.desc');
	sys::filterGet('age_min','int');
	sys::filterGet('age_max','int');
	sys::filterGet('sex','text');
	sys::filterGet('fan','text');

	define('_CANONICAL',main_films::getFilmRatingUrl($_GET['film_id']),true);

	if(!$_GET['film_id'])
		return 404;

	$result['film']=$_db->getValue('main_films','title',intval($_GET['film_id']),true);
	if(!$result['film'])
		return 404;

	main::countShow($_GET['film_id'],'film');

	$meta['film']=$result['film'];
	$result['meta']=sys::parseModTpl('main::film_rating','page',$meta);

	//Определение направления сортировки
	$order_by=explode('.',$_GET['order_by']);
	$order_fields=array('user','mark','pre_mark');
	if($order_by[1]=='asc')
	{
		$result['order']='asc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.desc');
		}

	}
	else
	{
		$result['order']='desc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.asc');
		}
	}
	$result['order_field']=$order_by[0];

	$order_by=sys::parseOrderBy($_GET['order_by']);
	//=====================================================

	//Поля поисковой формы-------------------------------------
	$result['fields']['age_min']['input']='textbox';
	$result['fields']['age_min']['value']=$_GET['age_min'];
	$result['fields']['age_min']['title']=sys::translate('main::age_min');

	$result['fields']['age_max']['input']='textbox';
	$result['fields']['age_max']['value']=$_GET['age_max'];
	$result['fields']['age_max']['title']=sys::translate('main::age_max');

	$result['fields']['sex']['input']='selectbox';
	$result['fields']['sex']['value']=$_GET['sex'];
	$result['fields']['sex']['empty']=sys::translate('main::any');
	$result['fields']['sex']['values']['man']=sys::translate('main::man');
	$result['fields']['sex']['values']['woman']=sys::translate('main::woman');
	$result['fields']['sex']['title']=sys::translate('main::sex');;

	$result['fields']['fan']['input']='checkbox';
	$result['fields']['fan']['value']=$_GET['fan'];
	$result['fields']['fan']['title']=sys::translate('main::genre_fan');
	//===================================================================

	//Формирование запроса-------------------------------------
	$q="
		SELECT
			rat.user_id,
			usr.login AS `user`,
			rat.pre_mark,
			rat.mark
		FROM `#__main_films_rating` AS `rat`
		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=rat.user_id
	";
	if($_GET['age_min'] || $_GET['age_max'] || $_GET['sex'])
	{
		$q.="
			LEFT JOIN `#__sys_users_data` AS `usr_data`
			ON usr_data.user_id=rat.user_id
		";
	}

	if($_GET['fan'])
	{
		$film_genres=main_films::getFilmGenresIds($_GET['film_id']);
		$q.="
			LEFT JOIN `#__main_users_genres` AS `usr_gnr`
			ON usr_gnr.user_id=rat.user_id
			AND usr_gnr.genre_id IN(".implode(',',$film_genres).")
		";
	}

	$q.="
		WHERE rat.film_id=".intval($_GET['film_id'])."
	";

	if($_GET['age_min'])
	{
		$year=intval(date('Y'))-$_GET['age_min'];
		$date_min=$year.'-'.date('m-d');
		$q.="
			AND usr_data.main_birth_date<'".$date_min."'
		";
	}

	if($_GET['age_max'])
	{
		$year=intval(date('Y'))-$_GET['age_max'];
		$date_max=$year.'-'.date('m-d');
		$q.="
			AND usr_data.main_birth_date>'".$date_max."'
		";
	}

	if($_GET['sex'])
	{
		$q.="
			AND usr_data.main_sex='".mysql_real_escape_string($_GET['sex'])."'
		";
	}

	if($_GET['fan'])
	{
		$q.="AND usr_gnr.genre_id IS NOT NULL";
	}

	$q.=" GROUP BY usr.id";
	$q.=$order_by;

	//====================================================================

	$result['pages']=sys_pages::pocess($q,$_cfg['main::rating_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		if(!$obj['pre_mark'])
			$obj['pre_mark']='-';

		if(!$obj['mark'])
			$obj['mark']='-';

		$obj['url']=main_users::getUserUrl($obj['user_id']);
		$result['objects'][]=$obj;
	}
	return $result;
}
?>