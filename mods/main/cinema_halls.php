<?
function main_cinema_halls()
{
	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('sys::form');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('cinema_id','int');
	
	$cache_name='main_cinema_halls_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::cinema_shows_page_cache_period']*_HOUR))
		return unserialize($cache);
	
	if(!$_GET['cinema_id'])
		return 404;
		
	$r=$_db->query("
		SELECT 
			cnm.user_id,
			cnm.notice_begin,
			cnm.ticket_url,
			cnm.notice_end,
			cnm_lng.title AS `cinema`,
			cnm_lng.notice
		FROM 
			`#__main_cinemas` AS `cnm`
			
		LEFT JOIN
			`#__main_cinemas_lng` AS `cnm_lng`
		ON 
			cnm_lng.record_id=cnm.id
		AND 
			cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			
		WHERE 
			cnm.id=".intval($_GET['cinema_id'])."
	");
	$result=$_db->fetchAssoc($r);
	
	if(!$result)
		return 404;
		
	/*if($result['ticket_url'] && $_user['id']==2)
		$result['ticket_url']=sys::rewriteUrl('?mod=main&act=register');*/
	
	if($result['user_id']==$_user['id'] || sys::checkAccess('main::cinemas_edit'))
		$result['edit']=true;
	else 
		$result['edit']=false;
	
	//Обработка команд------------------------------------------------------
	if($result['edit'])
	{
		if(isset($_POST['_task']) && $_POST['_task'])
		{
			$task=explode('.',$_POST['_task']);
			if($task[0]=='deleteHall')
				main_cinemas::deleteHall($task[1]);
				
			if($task[0]=='deleteShow')
				main_shows::deleteShow($task[1]);
				
			if($task[0]=='moveHallUp')
				$_db->moveUp('main_cinemas_halls',$task[1],"`cinema_id`=".intval($_GET['cinema_id'])."");
				
			if($task[0]=='moveHallDown')
				$_db->moveDown('main_cinemas_halls',$task[1],"`cinema_id`=".intval($_GET['cinema_id'])."");
	
			sys::redirect($_SERVER['REQUEST_URI'],false);
		}
	}
	//============================================================
		
		
	$result['notice_begin']=sys::db2Timestamp($result['notice_begin']);
	$result['notice_end']=sys::db2Timestamp($result['notice_end'])+_DAY;
	if($result['notice_begin']<time() && time()<$result['notice_end'])
		$result['notice']=$result['notice'];
	else 
		$result['notice']=false;
	
	$meta['cinema']=$result['cinema'];	
	$result['meta']=sys::parseModTpl('main::cinema_shows','page',$meta);
		
	$q="
		SELECT 
			shw.begin,
			shw.end,
			shw.film_id,
			shw.id AS `show_id`,
			hls_lng.title AS `hall`,
			hls.cinema_id,
			hls.id AS `hall_id`,
			hls.scheme,
			shw.id AS `show_id`,
			flm_lng.title AS `film`,
			shw.hall_id
		FROM `#__main_shows` AS `shw`
		
		LEFT JOIN `#__main_cinemas_halls` AS `hls`
		ON hls.id=shw.hall_id
		
		LEFT JOIN `#__main_films_lng` AS `flm_lng`
		ON flm_lng.record_id=shw.film_id
		AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])." 
		
		LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng`
		ON hls_lng.record_id=hls.id
		AND hls_lng.lang_id=".intval($_cfg['sys::lang_id'])." 
		
		WHERE hls.cinema_id=".$_GET['cinema_id']." AND shw.end>='".date('Y-m-d')."'
	";
	if(!$result['edit'])
	$q.="
		AND shw.end>='".date('Y-m-d')."'		
	";
	
	$q.="
		ORDER BY hls.order_number, shw.begin
	";
	
	$r=$_db->query($q);
	
	$result['halls']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$r2=$_db->query("
			SELECT tms.time, tms.prices, tms_lng.note 
			FROM `#__main_shows_times` AS `tms`
			LEFT JOIN `#__main_shows_times_lng` AS `tms_lng`
			ON tms_lng.record_id=tms.id
			AND tms_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE tms.show_id=".$obj['show_id']." ORDER BY tms.time
		");
		$obj['prices']=array();
		while ($time=$_db->fetchAssoc($r2))
		{
			$time['time']=sys::cutStrRight($time['time'],3);
			$prices=explode(';',$time['prices']);
			$obj['prices']=array_merge($obj['prices'],$prices);
			$obj['times'][]=$time;
		}
		$obj['prices']=array_unique($obj['prices']);
		sort($obj['prices']);
		
		if($obj['scheme'])
			$obj['scheme_url']=main_cinemas::getHallSchemeUrl($obj['hall_id']);
		else 
			$obj['scheme_url']=false;
			
		$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
		
		$obj['begin']=sys::db2Date($obj['begin'],false, $_cfg['sys::date_format']);
		$obj['end']=sys::db2Date($obj['end'],false, $_cfg['sys::date_format']);
		$result['halls'][$obj['hall_id']]['title']=$obj['hall'];
		$result['halls'][$obj['hall_id']]['id']=$obj['hall_id'];
		$result['halls'][$obj['hall_id']]['scheme_url']=$obj['scheme_url'];
		$result['halls'][$obj['hall_id']]['shows'][]=$obj;
	}
	
	//Получить залы без сеансов
	if($result['edit'])
	{
		$r=$_db->query("
			SELECT 
				hls_lng.title,
				hls.cinema_id,
				hls.id,
				hls.scheme
			FROM
				`#__main_cinemas_halls` AS `hls`
				
			LEFT JOIN 
				`#__main_shows` as `shw`
			ON
				shw.hall_id=hls.id
				
			LEFT JOIN 
				`#__main_cinemas_halls_lng` AS `hls_lng`
			ON 
				hls_lng.record_id=hls.id
			AND 
				hls_lng.lang_id=".intval($_cfg['sys::lang_id'])." 
			WHERE 
				hls.cinema_id=".intval($_GET['cinema_id'])."
				AND shw.id IS NULL
			ORDER BY
				hls.order_number
			
		");
		while ($hall=$_db->fetchAssoc($r))
		{
			if($hall['scheme'])
				$hall['scheme_url']=main_cinemas::getHallSchemeUrl($hall['id']);
			else 
				$hall['scheme_url']=false;
			$hall['shows']=array();
			$result['halls'][$hall['id']]=$hall;
		}
		
	}
	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>