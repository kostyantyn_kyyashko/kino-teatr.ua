<?
function main_person_edit()
{
	sys::useLib('main::persons');
	sys::useLib('main::films');
	sys::useLib('sys::form');
	sys::useLib('sys::check');
		
	global $_db, $_cfg, $_user, $_err;
	sys::filterGet('person_id','int');
	
	if(!isset($_POST['_task']))	$_POST['_task']=false;
		
	$tbl='main_persons';
	$default['date']=strftime($_cfg['sys::date_time_format']);
	$default['user_id']=$_user['id'];
	
	if($_GET['person_id'])
	{
		$record=$_db->getRecord($tbl,$_GET['person_id'],true);
		if(!$record) return 404;
		$title=$record['name'];
		$record['birthdate']=sys::db2Date($record['birthdate'],false,$_cfg['sys::date_format']);
		// первая фотка - это портрет. Поэтому, если фотка есть, то слово "портрет" уже не пишем, а добавляем фотки в галерею
		$additional['have_portrait'] = ($_db->getRecord($tbl."_photos","person_id=".$_GET['person_id'],true) != null);
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_person');
	}
	
	$values=sys::setValues($record, $default);
	
	if($_GET['person_id'] && !sys::checkAccess('main::persons_edit'))	return 403;
	elseif(!sys::checkAccess('main::persons_add'))						return 403;
	//===========================================================================================================//
	
	//Поля и форма---------------------------------------------------------------------------------------------
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';
	$fields['name']['title']=sys::translate('main::firstname');
	
	//lastname_orig
	$fields['lastname_orig']['input']='textbox';
	$fields['lastname_orig']['type']='text';
	$fields['lastname_orig']['req']=true;
	$fields['lastname_orig']['table']=$tbl;
	$fields['lastname_orig']['html_params']='style="width:100%" maxlength="255"';
	$fields['lastname_orig']['title']=sys::translate('main::lastname_orig');
	
	//firstname_orig
	$fields['firstname_orig']['input']='textbox';
	$fields['firstname_orig']['type']='text';
	$fields['firstname_orig']['req']=true;
	$fields['firstname_orig']['table']=$tbl;
	$fields['firstname_orig']['html_params']='style="width:100%" maxlength="255"';
	$fields['firstname_orig']['title']=sys::translate('main::firstname_orig');
	
	//lastname
	$fields['lastname']['input']='textbox';
	$fields['lastname']['type']='text';
	$fields['lastname']['req']=true;
	$fields['lastname']['multi_lang']=true;
	$fields['lastname']['table']=$tbl;
	$fields['lastname']['html_params']='style="width:100%" maxlength="255"';
	$fields['lastname']['title']=sys::translate('main::lastname');
	
	//firstname
	$fields['firstname']['input']='textbox';
	$fields['firstname']['type']='text';
	$fields['firstname']['req']=true;
	$fields['firstname']['multi_lang']=true;
	$fields['firstname']['table']=$tbl;
	$fields['firstname']['html_params']='style="width:100%" maxlength="255"';
	$fields['firstname']['title']=sys::translate('main::firstname');
	
	//birthdate
	$fields['birthdate']['input']='datebox';
	$fields['birthdate']['type']='date';
	$fields['birthdate']['table']=$tbl;
	$fields['birthdate']['format']=$_cfg['sys::date_format'];
	$fields['birthdate']['title']=sys::translate('main::birthdate');
	
	//biography
	$fields['biography']['input']='textarea';
	$fields['biography']['type']='html';
	$fields['biography']['table']=$tbl;
	$fields['biography']['multi_lang']=true;
	$fields['biography']['html_params']='style="width:100%; height:50px"';
	$fields['biography']['height']='300px';
	$fields['biography']['title']=sys::translate('main::biography');

	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------
	
	// Сохранение
	if(isset($_POST['cmd_save']))
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			// вырезать теги
			foreach ($values as $key => $value)
			{
				$value = preg_replace('#<(br|p)\s*/?>#i', "\n", $value); // заменить <br> на перенос строки
				$pattern = '#/\*[^\/*]+\*/#i';
				$replacement = '';
				$values[$key] = stripslashes(preg_replace($pattern, $replacement, strip_tags($value)));
				
				if(strpos($key,"biography")===0) $values[$key] = "<p>".nl2br($values[$key])."</p>";
			}	
			
			$values['birthdate']=sys::date2Db($values['birthdate'],false,$_cfg['sys::date_format']);
			//Вставка данных
			if(!$_GET['person_id'])
			{
                $datetime = new DateTime();
                $values['date'] = $datetime->format('Y-m-d H:i:s');
            	$fields['date']['input']='datebox';
            	$fields['date']['type']='date';
            	$fields['date']['table']=$tbl;
            	
				if(!$_GET['person_id']=$_db->insertArray($tbl,$values,$fields)) $_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['person_id']))	$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['birthdate']=sys::db2Date($values['birthdate'],false, $_cfg['sys::date_format']);
		}
		if(!$_err)
		{
			//Загрузить и отресайзить картинку	
			if($_FILES['photos']['name']) 
				main_persons::uploadPhotos($_GET['person_id']);
		}
		if(!$_err)
		{
			sys::sendMail(false,false,$_db->getValue('sys_users','email',7),'Root','Добавлена/отредактирована новая персона',_ROOT_URL.'backend/?mod=main&act=person_edit&id='.$_GET['person_id']);
			//sys::sendMail(false,false,"happyinvestor@mail.ru",'Root','Добавлена/отредактирована новая персона',_ROOT_URL.'backend/?mod=main&act=person_edit&id='.$_GET['person_id']);
			sys::redirect(main_persons::getPersonAddUrl()."?person_id=".$_GET['person_id'],false);
		}
				
	}
	//==========================================================================================================//
	
	//Удаление картики
//	if(strpos($_POST['_task'],'delete_file')===0)
//	{
//		$task=explode('.',$_POST['_task']);
//		if($task[1]=='image')
//		{
//			$_db->setValue('main_reviews','image',false,$_GET['id']);
//			main_reviews::deleteReviewImage($values['image']);
//			$values['image']=false;
//		}
//	}
	//==================================================================================================
	
	$result['fields']=sys_form::parseFields($fields,$values,false);
	$result['page']=sys::parseModTpl('main::person_edit','page');
	$result['additional']=$additional;
	
	return $result;
}
?>