<?
function main_page()
{
	global $_db, $_cfg;
	sys::useLib('content');
	sys::useLib('sys::pages');
	$result=content::getObject($_GET['id']);
	if(!$result)
		return 404;
	content::prepareObject($result);
	return $result;
}
?>