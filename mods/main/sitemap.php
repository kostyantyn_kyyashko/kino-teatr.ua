<?


function main_sitemap()
{
	global $_db, $_cfg, $_err;
	sys::useLib('main::genres');
	sys::useLib('main::countries');
	sys::useLib('sys::form');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	sys::filterGet('title');
	sys::filterGet('genre_id');
	sys::filterGet('year');
	sys::filterGet('letter');
	sys::filterGet('city_id');
	sys::filterGet('show');
	sys::filterGet('order_by','text','title.asc');

	if ($_GET['page']!='' || $_GET['title']!='' || $_GET['genre_id']!='' || $_GET['year']!='' || $_GET['city_id']!='')
	{
		define('_NOINDEX',1,true);
	}

	if ($_GET['order_by'])
	{
		define('_CANONICAL',$_cfg['sys::root_url'].$_cfg['sys::lang'].'/main/films.phtml',true);
    }

	$cache_name='main_films_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::films_page_cache_period']*60))
		return unserialize($cache);

	$result=array();
	$result['meta']=sys::parseModTpl('main::films','page');

	//Определение направления сортировки
	$order_by=explode('.',$_GET['order_by']);
	$order_fields=array('title','year','rating','pro_rating');
	if($order_by[1]=='asc')
	{
		$result['order']='asc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.desc');
		}

	}
	else
	{
		$result['order']='desc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.asc');
		}
	}
	$result['order_field']=$order_by[0];

	$order_by=sys::parseOrderBy($_GET['order_by']);
	//=====================================================

	//Поля поисковой формы-------------------------------------
	$result['fields']['title']['input']='textbox';
	$result['fields']['title']['value']=urldecode($_GET['title']);
	$result['fields']['title']['title']=sys::translate('main::title');

	$result['fields']['genre_id']['input']='selectbox';
	$result['fields']['genre_id']['value']=$_GET['genre_id'];
	$result['fields']['genre_id']['empty']=sys::translate('main::all_genres');
	$result['fields']['genre_id']['values']=main_genres::getGenresTitles();
	$result['fields']['genre_id']['title']=sys::translate('main::genre');

	$result['fields']['year']['input']='selectbox';
	$result['fields']['year']['value']=$_GET['year'];
	$result['fields']['year']['empty']=sys::translate('main::all_years');
	$result['fields']['year']['values']=main::getYears();
	$result['fields']['year']['title']=sys::translate('main::year');

	$result['fields']['city_id']['input']='selectbox';
	$result['fields']['city_id']['value']=$_GET['city_id'];
	$result['fields']['city_id']['empty']=sys::translate('main::all_cities');
	$result['fields']['city_id']['values']=main_countries::getCountryCitiesTitles(29);
	$result['fields']['city_id']['title']=sys::translate('main::city');

	$result['fields']['show']['input']='checkbox';
	$result['fields']['show']['value']=$_GET['show'];
	$result['fields']['show']['title']=sys::translate('main::only_showing');
	//===================================================================

	//Формирование запроса-----------------------------------------------
	$q="
		SELECT
			flm.id,
			flm.title_orig,
			lng.title,
			flm.year,
			flm.rating,
			flm.votes,
			flm.pro_rating,
			flm.pro_votes
	";

	//Жанр
	if($_GET['genre_id'])
		$q.=" ,gnr.genre_id ";


	//Город
	if($_GET['city_id'])
		$q.=" ,cnm.city_id ";


	$q.="
		FROM `#__main_films` AS `flm`
		LEFT JOIN `#__main_films_lng` AS `lng`
		ON lng.record_id=flm.id
		AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	//Жанр
	if($_GET['genre_id'])
	{
		$q.="LEFT JOIN `#__main_films_genres` AS `gnr`
		ON gnr.film_id=flm.id AND gnr.genre_id=".intval($_GET['genre_id'])."";
	}

	//Город
	if($_GET['city_id'])
	{
		$q.="
		LEFT JOIN `#__main_shows` AS `shw`
		ON shw.film_id=flm.id
		LEFT JOIN `#__main_cinemas_halls` AS `hls`
		ON hls.id=shw.hall_id
		LEFT JOIN `#__main_cinemas` AS `cnm`
		ON cnm.id=hls.cinema_id AND cnm.city_id=".intval($_GET['city_id'])."
		";
	}

	if($_GET['show'] && !$_GET['city_id'])
	{
		$q.="
		LEFT JOIN `#__main_shows` AS `shw`
		ON shw.film_id=flm.id
		";
	}

	//Только в прокате
	if($_GET['show'] && !$_GET['city_id'])
	{

	}

	//Условия
	$q.=" WHERE 1=1";

	if($_GET['title'])
	{
		$q.=" AND (lng.title LIKE '%".mysql_real_escape_string(urldecode($_GET['title']))."%'
			OR flm.title_orig LIKE '%".mysql_real_escape_string(urldecode($_GET['title']))."%')";
	}

	if($_GET['year'])
	{
		$q.=" AND flm.year='".intval($_GET['year'])."'";
	}

	if($_GET['letter'])
	{
		$q.=" AND (lng.title LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%'
			OR flm.title_orig LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%')";
	}

	if($_GET['genre_id'])
	{
		$q.=" AND gnr.genre_id=".intval($_GET['genre_id'])."";
	}

	if($_GET['city_id'])
	{
		$q.=" AND cnm.city_id=".intval($_GET['city_id'])." AND (shw.begin<='".date('Y-m-d')."' AND shw.end>='".date('Y-m-d')."')";
	}

	if($_GET['show'] && !$_GET['city_id'])
	{
		$q.=" AND (shw.begin<='".date('Y-m-d')."' AND shw.end>='".date('Y-m-d')."')";
	}


	$q.="
		GROUP BY flm.id
		".$order_by."
	";
	//===========================================================

	$result['pages']=sys_pages::pocess($q,$_cfg['main::films_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['url']=main_films::getFilmUrl($obj['id']);
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['title_orig']=htmlspecialchars($obj['title_orig']);
		$obj['genres']=main_films::getFilmGenresTitles($obj['id']);
		$obj['countries']=main_films::getFilmCountriesTitles($obj['id']);
		$obj['rating_url']=main_films::getFilmRatingUrl($obj['id']);
		$obj['pro_rating_url']=main_films::getFilmReviewsUrl($obj['id']);
		$obj['rating']=main_films::formatFilmRating($obj['rating']);
		$obj['pro_rating']=main_films::formatFilmRating($obj['pro_rating']);
		$result['objects'][]=$obj;
	}

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>