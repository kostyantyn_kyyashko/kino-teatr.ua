<?
function main_getHall()
{
	sys::setTpl();
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user;

	$dt = date('d.m.Y');
	$sid = 1082;
	if (isset($_REQUEST['sid']))
		$sid = $_REQUEST['sid'];

	if ($_REQUEST['film'])
	{		$rw=$_db->query("
			SELECT
				min(id) as min
			FROM `#__shows_test`

			WHERE film = '".$_REQUEST['film']."'
			AND date='".date('Y-m-d',strtotime($dt))."'
		");

		$result=$_db->fetchAssoc($rw);

		$sid = $result['min'];
	}

		$rw=$_db->query("
			SELECT
				film
			FROM `#__shows_test`

			WHERE id = '".$sid."'
		");

		$result=$_db->fetchAssoc($rw);

		$seans = $result['film'];


	$q = 'SELECT * FROM `#__shows_test` WHERE insell=1 AND date="'.date('Y-m-d',strtotime($dt)).'" GROUP BY film';
	$r=$_db->query($q);
	$films = array();
	$i = 1;
	while($obj=$_db->fetchAssoc($r))
	{

		$rw=$_db->query("
			SELECT
				buy_id
			FROM `#__films_buy`

			WHERE kt_id = '".$obj['film']."'
		");

		$result=$_db->fetchAssoc($rw);

		if ($obj['film']==$seans)
		{			$films[$i]['active'] = 1;
		} else {			$films[$i]['active'] = 0;
		}

		$films[$i]['buy'] = $result['buy_id'];
		$films[$i]['kt'] = $obj['film'];

		$rw=$_db->query("
			SELECT
				title
			FROM `#__main_films_lng`

			WHERE record_id = ".(int) $obj['film']." AND lang_id=1
		");


		$result=$_db->fetchAssoc($rw);

		$films[$i]['name'] = $result['title'];

		$i++;
	}

	$q = 'SELECT * FROM `#__shows_test` WHERE insell=1 AND date="'.date('Y-m-d',strtotime($dt)).'" and film="'.$seans.'"';
	$r=$_db->query($q);
	$times = array();
	$i = 1;
	while($obj=$_db->fetchAssoc($r))
	{

		$times[$i]['time'] = $obj['time'];
		$times[$i]['id'] = $obj['id'];

		if ($obj['id']==$sid)
		{
			$times[$i]['active'] = 1;
		} else {			$times[$i]['active'] = 0;
		}
		$i++;
	}




try{
    $client = new SoapClient("http://91.202.104.253:8080/TheaterServiceTest?wsdl");

     // Поcылка SOAP-запроса и получение результата
//     $result = $client->SayHello("Dmitry");
//     echo "Текущий курс доллара: " . $result . " рублей";

	$params = array("eventId"=>$sid);
	$webService = $client->GetEventRequest($params);
	$wsResult = $webService->GetEventRequestResult;


	function gzdecode($data,&$filename='',&$error='',$maxlength=null)
	{
	    $len = strlen($data);
	    if ($len < 18 || strcmp(substr($data,0,2),"\x1f\x8b")) {
	        $error = "Not in GZIP format.";
	        return null;  // Not GZIP format (See RFC 1952)
	    }
	    $method = ord(substr($data,2,1));  // Compression method
	    $flags  = ord(substr($data,3,1));  // Flags
	    if ($flags & 31 != $flags) {
	        $error = "Reserved bits not allowed.";
	        return null;
	    }
	    // NOTE: $mtime may be negative (PHP integer limitations)
	    $mtime = unpack("V", substr($data,4,4));
	    $mtime = $mtime[1];
	    $xfl   = substr($data,8,1);
	    $os    = substr($data,8,1);
	    $headerlen = 10;
	    $extralen  = 0;
	    $extra     = "";
	    if ($flags & 4) {
	        // 2-byte length prefixed EXTRA data in header
	        if ($len - $headerlen - 2 < 8) {
	            return false;  // invalid
	        }
	        $extralen = unpack("v",substr($data,8,2));
	        $extralen = $extralen[1];
	        if ($len - $headerlen - 2 - $extralen < 8) {
	            return false;  // invalid
	        }
	        $extra = substr($data,10,$extralen);
	        $headerlen += 2 + $extralen;
	    }
	    $filenamelen = 0;
	    $filename = "";
	    if ($flags & 8) {
	        // C-style string
	        if ($len - $headerlen - 1 < 8) {
	            return false; // invalid
	        }
	        $filenamelen = strpos(substr($data,$headerlen),chr(0));
	        if ($filenamelen === false || $len - $headerlen - $filenamelen - 1 < 8) {
	            return false; // invalid
	        }
	        $filename = substr($data,$headerlen,$filenamelen);
	        $headerlen += $filenamelen + 1;
	    }
	    $commentlen = 0;
	    $comment = "";
	    if ($flags & 16) {
	        // C-style string COMMENT data in header
	        if ($len - $headerlen - 1 < 8) {
	            return false;    // invalid
	        }
	        $commentlen = strpos(substr($data,$headerlen),chr(0));
	        if ($commentlen === false || $len - $headerlen - $commentlen - 1 < 8) {
	            return false;    // Invalid header format
	        }
	        $comment = substr($data,$headerlen,$commentlen);
	        $headerlen += $commentlen + 1;
	    }
	    $headercrc = "";
	    if ($flags & 2) {
	        // 2-bytes (lowest order) of CRC32 on header present
	        if ($len - $headerlen - 2 < 8) {
	            return false;    // invalid
	        }
	        $calccrc = crc32(substr($data,0,$headerlen)) & 0xffff;
	        $headercrc = unpack("v", substr($data,$headerlen,2));
	        $headercrc = $headercrc[1];
	        if ($headercrc != $calccrc) {
	            $error = "Header checksum failed.";
	            return false;    // Bad header CRC
	        }
	        $headerlen += 2;
	    }
	    // GZIP FOOTER
	    $datacrc = unpack("V",substr($data,-8,4));
	    $datacrc = sprintf('%u',$datacrc[1] & 0xFFFFFFFF);
	    $isize = unpack("V",substr($data,-4));
	    $isize = $isize[1];
	    // decompression:
	    $bodylen = $len-$headerlen-8;
	    if ($bodylen < 1) {
	        // IMPLEMENTATION BUG!
	        return null;
	    }
	    $body = substr($data,$headerlen,$bodylen);
	    $data = "";
	    if ($bodylen > 0) {
	        switch ($method) {
	        case 8:
	            // Currently the only supported compression method:
	            $data = gzinflate($body,$maxlength);
	            break;
	        default:
	            $error = "Unknown compression method.";
	            return false;
	        }
	    }  // zero-byte body content is allowed
	    // Verifiy CRC32
	    $crc   = sprintf("%u",crc32($data));
	    $crcOK = $crc == $datacrc;
	    $lenOK = $isize == strlen($data);
	    if (!$lenOK || !$crcOK) {
	        $error = ( $lenOK ? '' : 'Length check FAILED. ') . ( $crcOK ? '' : 'Checksum FAILED.');
	        return false;
	    }
	    return $data;
	}


	$str = trim($wsResult);
	$xml1 = gzdecode(base64_decode($str));

//	$xml = iconv('utf8', 'windows-1251', $xml);


//    $xml = simplexml_load_string($xml1);





} catch (Exception $e) {
    print  "Caught exception: ".  $e->getMessage(). "\n";
}



$page = '

<!-- saved from url=(0052)http://simonsarris.com/project/canvasdemo/demo2.html -->
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="UTF-8">
		<!--[if IE]><script type="text/javascript" src="//kino-teatr.ua/excanvas.js"></script><![endif]-->
		<title>Бронирование</title>
	    <!-- A border on the canvas helps us see the edges -->
		<style type="text/css">
			#container2 {
				position: relative;
			}
			#canvas2 {				float: left;
				margin-right: 15px;
			}

			.leftColumn
			{
				float: left;
				width: 200px;
			}

			.leftColumn ul
			{				margin:0;
				padding:0;
				list-style-type: none;
			}

			.leftColumn ul li a
			{
				font: normal 12px/20px Arial;
				color: #000;
				text-decoration: underline;
			}

			.leftColumn ul li a:hover, .leftColumn ul li a.active
			{
				color: #ff0000;
				text-decoration: none;
			}

			.rightColumn ul
			{
				margin:0;
				padding:0;
				list-style-type: none;
			}

			.rightColumn ul li a
			{
				font: normal 12px/20px Arial;
				color: #000;
				text-decoration: underline;
			}

			.rightColumn ul li a:hover, .rightColumn ul li a.active
			{
				color: #ff0000;
				text-decoration: none;
			}

			#canvaData {
				top: 5;
				left: 890;
				position: absolute;
			}


		</style>
		<script type="text/javascript" src="//kino-teatr.ua/hallBuild2.js"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	</head>
	<body onload="init1()" >

		<div id="container2" >
			<div class="leftColumn">
				<ul>';


		foreach ($films as $film)
		{			$page .= '<li><a href="?film='.$film['kt'].'"';

				if ($film['active'])
				{					$page .= ' class="active" '	;
				}

			$page .= '>'.$film['name'].'</a></li>';
		}



$page .= '
           </ul>
			</div>

  <button id="canvaData" type="reservation" onclick="javascript:reserve();" value="Reserve">Бронировать</button>

  <canvas id="canvas2" width="990" height="750" style="cursor: auto;">
     This text is displayed if your browser does not support HTML5 Canvas.
  </canvas>
		<div class="rightColumn">
			<ul>';

		foreach ($times as $time)
		{
			$page .= '<li><a href="?sid='.$time['id'].'"';

				if ($time['active'])
				{
					$page .= ' class="active" '	;
				}

			$page .= '>'.date('H:i', strtotime($time['time'])).'</a></li>';
		}

$page .= '	</ul>
		</div>

	    </div>     ';


	$page .= $xml1;

$page .='	</body>

';


echo $page;

}
?>