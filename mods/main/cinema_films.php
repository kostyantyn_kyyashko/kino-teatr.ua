<?

function main_cinema_films()
{
	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('main::cards');
	sys::useLib('sys::form');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('cinema_id','int');
	
	$cache_name='main_cinema_films_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::cinema_shows_page_cache_period']*_HOUR))
		return unserialize($cache);
	
	if(!$_GET['cinema_id'])
		return 404;
		
	$r=$_db->query("
		SELECT 
			cnm.user_id,
			cnm.notice_begin,
			cnm.ticket_url,
			cnm.notice_end,
			cnm_lng.title AS `cinema`,
			cnm_lng.notice
		FROM 
			`#__main_cinemas` AS `cnm`
			
		LEFT JOIN
			`#__main_cinemas_lng` AS `cnm_lng`
		ON 
			cnm_lng.record_id=cnm.id
		AND 
			cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			
		WHERE 
			cnm.id=".intval($_GET['cinema_id'])."
	");
	$result=$_db->fetchAssoc($r);
	
	if(!$result)
		return 404;
		
	/*if($result['ticket_url'] && $_user['id']==2)
		$result['ticket_url']=sys::rewriteUrl('?mod=main&act=register');*/
	
	if($result['user_id']==$_user['id'] || sys::checkAccess('main::cinemas_edit'))
		$result['edit']=true;
	else 
		$result['edit']=false;
	
	//Обработка команд------------------------------------------------------
	if($result['edit'])
	{
		if(isset($_POST['_task']) && $_POST['_task'])
		{
			$task=explode('.',$_POST['_task']);
			if($task[0]=='deleteHall')
				main_cinemas::deleteHall($task[1]);
				
			if($task[0]=='deleteShow')
				main_shows::deleteShow($task[1]);
				
			if($task[0]=='moveHallUp')
				$_db->moveUp('main_cinemas_halls',$task[1],"`cinema_id`=".intval($_GET['cinema_id'])."");
				
			if($task[0]=='moveHallDown')
				$_db->moveDown('main_cinemas_halls',$task[1],"`cinema_id`=".intval($_GET['cinema_id'])."");
	
			sys::redirect($_SERVER['REQUEST_URI'],false);
		}
	}
	//============================================================
		
		
	$result['notice_begin']=sys::db2Timestamp($result['notice_begin']);
	$result['notice_end']=sys::db2Timestamp($result['notice_end'])+_DAY;
	if($result['notice_begin']<time() && time()<$result['notice_end'])
		$result['notice']=$result['notice'];
	else 
		$result['notice']=false;
	
	$meta['cinema']=$result['cinema'];	
	$result['meta']=sys::parseModTpl('main::cinema_shows','page',$meta);
		
	
	$halls=main_cinemas::getCinemaHallsId($_GET['cinema_id']);
	$r=$_db->query("
		SELECT shw.*, flm_lng.title AS `film`, hls_lng.title AS `hall`
		FROM `#__main_shows` AS `shw`
		
		LEFT JOIN `#__main_films_lng` AS `flm_lng`
		ON flm_lng.record_id=shw.film_id
		AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		
		LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng`
		ON hls_lng.record_id=shw.hall_id
		AND hls_lng.lang_id=".intval($_cfg['sys::lang_id'])." 
		
		WHERE shw.hall_id IN(".implode(',',$halls).")
		AND shw.end>='".date('Y-m-d')."'
		ORDER BY flm_lng.title
	");
	
	while($show=$_db->fetchAssoc($r)){
		
		$r2=$_db->query("
			SELECT t.*, tl.* FROM `#__main_shows_times` AS `t`
			LEFT JOIN `#__main_shows_times_lng` AS `tl`
			ON tl.record_id=t.id
			AND tl.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE t.show_id='".intval($show['id'])."'
			ORDER BY t.time
		");
		
		$show['prices']=array();
		while($time=$_db->fetchAssoc($r2)){
			$time['time']=sys::cutStrRight($time['time'],3);
			$prices=explode(',',$time['prices']);
			$show['prices']=array_merge($show['prices'],$prices);
			$show['times'][]=$time;
		}
		$show['prices']=array_unique($show['prices']);
		sort($show['prices']);
		$show['price_range']=trim($show['prices'][0]);
		if(count($show['prices'])>1)
		{
			$show['price_range'].='-'.trim($show['prices'][count($show['prices'])-1]);
		}
		
		$show['begin']=sys::date2Timestamp($show['begin'],'%Y-%m-%d');
		$show['end']=sys::date2Timestamp($show['end'],'%Y-%m-%d');
		
		
		$shows[]=$show;
	}
	
	$periods=array();
	
	foreach($shows as $show){
		$dates[]=$show['begin'];
		$dates[]=$show['end'];
	}
	
	$dates=array_unique($dates);
	sort($dates);
	
	for($i=0; $i<count($dates)-1; $i++){
		$period['begin']=$dates[$i];
		if($i==count($dates)-2){
			$period['end']=$dates[$i+1]+60*60*24-1;
		}else{
			$period['end']=$dates[$i+1]-1;
		}
		
		foreach($shows as $show){
			if($show['begin']==$period['begin']){
				$period['shows'][]=$show;
			}
		}
		if($period['end']>time()){
			$periods[]=$period;
		}
		
	}
	$result['periods']=$periods;
	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>