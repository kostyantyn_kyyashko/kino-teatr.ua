<?
function main_discussed_articles()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::articles');
	sys::useLib('main::spec_themes');
	sys::useLib('main::users');
	sys::useLib('sys::pages');

	sys::filterGet('month');
	sys::filterGet('year');
	sys::filterGet('articles_id');

	if ($_GET['page']!='')
	{
		define('_NOINDEX',1,true);
	}


		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		define('_CANONICAL',_ROOT_URL.'articles.phtml',true);
		} else {
		define('_CANONICAL',_ROOT_URL.$lang.'/articles.phtml',true);
		}

	$cache_name='main_articles_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::articles_page_cache_period']*60))
		return unserialize($cache);

	/*if($_GET['year']<2000 || $_GET['year']>intval(date('Y')) || $_GET['month']<1 || $_GET['month']>12)
		return 404;*/


	if($_GET['articles_id'])
		$result['section']=$meta['section']=$_db->getValue('main_articles','title',intval($_GET['articles_id']),true);
	else
		$result['section']=$meta['section']=sys::translate('main::all_sections');

	if(!$result['section'])
		return 404;

	if(isset($_POST['_task']) && $_POST['_task'])
	{
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete' && sys::checkAccess('main::articles_delete'))
			main_articles::deleteArticle($task[1]);
	}

	$result['date']=$meta['date']=mb_strtolower(sys::translate('main::month_'.intval($_GET['month']))).' '.$_GET['year'];
	$result['meta']=sys::parseModTpl('main::articles','page',$meta);
	$result['title']=sys::translate('main::articles_cinema');
	$month_begin=date('Y-m-d H:i:s',main_articles::getMonthBegin($_GET['month'],$_GET['year']));
	$month_end=date('Y-m-d H:i:s',main_articles::getMonthEnd($_GET['month'],$_GET['year']));
	$q="
		SELECT
			art.id,
			art.date,
			art.total_shows,
			art.comments,
			art.user_id,
			art.exclusive,
			spec.spec_theme_id as spec_theme,
			art.small_image AS `image`,
			art.image AS `big_image`,
			art_lng.title,
			art_lng.intro,
			usr.login AS `user`,
			usr_data.main_nick_name
		FROM `#__main_articles_articles` AS `art`

		LEFT JOIN
			`#__main_articles_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_articles` AS `nws`
		ON art.articles_id=nws.id

		LEFT JOIN `#__main_articles_articles_spec_themes` AS `spec`
		ON art.id=spec.article_id

		LEFT JOIN
			`#__sys_users` AS `usr`
		ON
			usr.id=art.user_id

		LEFT JOIN
			`#__sys_users_data` AS `usr_data`
		ON
			usr.id=usr_data.user_id
			
		WHERE
			/*art.date BETWEEN '".$month_begin."' AND '".$month_end."'
			AND */(art.city_id=".intval($_cfg['main::city_id'])." OR art.city_id=0)
			AND art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND nws.showtab=1
	";
	if($_GET['articles_id'])
	{
		$q.="
			AND art.articles_id=".intval($_GET['articles_id'])."
		";
	}
	$q.="
			ORDER BY art.comments DESC
	";

	$result['pages']=sys_pages::pocess($q,$_cfg['main::articles_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::russianDate($obj['date']);
		$obj['shows']=$obj['total_shows'];
		$obj['user_url']=main_users::getUserUrl($obj['user_id']);

		if($obj['image'])
			$image=$_cfg['main::articles_url'].$obj['image'];
			$bigimage=$_cfg['main::articles_url'].$obj['big_image'];

		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));
		list($bwid, $bhei, $btype) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$bigimage));

		if ($wid<213 && $obj['big_image'])
		{
			if ($bwid>=213)
			 	$image = $_cfg['main::articles_url'].$obj['big_image'];
			else
	         	$image = 'blank_news_img.jpg';
		} else if ($wid>=213)
		{
		 	$image = $_cfg['main::articles_url'].$obj['image'];
		} else {
         	$image = 'blank_news_img.jpg';
		}

		$obj['image'] = _ROOT_URL.'public/main/rescrop1.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';

		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_articles::getArticleUrl($obj['id']);
		$obj['spec_theme_url']=main_spec_themes::getArticleUrl($obj['spec_theme']);

		if(sys::checkAccess('main::articles_edit') || $_user['id']==$obj['user_id'])
			$obj['edit_url']=main_articles::getArticleEditUrl($obj['id']);
		else
			$obj['edit_url']=false;

		if(sys::checkAccess('main::articles_delete') || $_user['id']==$obj['user_id'])
			$obj['delete']=true;
		else
			$obj['delete']=false;
		$result['objects'][]=$obj;
	}

	if(!$result['objects']/* && !main_articles::articlesReirect($_GET['year'],$_GET['month'],$_GET['articles_id'])*/)
		return 404;

	if(sys::checkAccess('main::articles'))
		$result['add_url']=main_articles::getArticleAddUrl();
	else
		$result['add_url']=false;


		$result['all_news']=sys::rewriteUrl('?mod=main&act=articles');
		$result['popular_news']=sys::rewriteUrl('?mod=main&act=popular_articles');
		$result['discussed_news']=sys::rewriteUrl('?mod=main&act=discussed_articles');
	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>