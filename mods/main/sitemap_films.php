<?
function main_sitemap_films()
{
	sys::useLib('main::films');
	sys::useLib('sys::form');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/films.txt";
	$fh = fopen($myFile, 'r');
	$theData = fread($fh, filesize($myFile));
	fclose($fh);

	$data = explode(':', $theData);

	$filmid = $data[0];
	$newxmlid = (int) $data[1]+1;


	$result=array();

	//Жанры----------------------------------------------------------------
	$r=$_db->query("

		SELECT
			flm.id,
			flm.title_orig,
			flm.title_alt,
			flm_lng.title,
			flm.year,
			flm.budget,
			flm.budget_currency,
			flm.duration,
			flm.ukraine_premiere,
			flm.world_premiere,
			flm.yakaboo_url,
			flm.age_limit,
			flm_lng.intro,
			flm_lng.text,
			flm.pre_rating,
			flm.pre_votes,
			flm.rating,
			flm.votes,
			flm.pro_rating,
			flm.pro_votes,
			flm.comments AS `comments_num`,
			flm.banner
		FROM `#__main_films` AS `flm`

		LEFT JOIN `#__main_films_lng` AS `flm_lng`
		ON flm_lng.record_id=flm.id
		AND flm_lng.lang_id=1

		WHERE flm.id>".$filmid."

		ORDER BY `id`
		LIMIT 500

	");


	$jj = 0;

	$result['films']=array();
	while ($obj=$_db->fetchAssoc($r))
	{

			if(main_films::isFilmTrailers($obj['id']))
					{

			     		$qe="
							SELECT
							trl.film_id,
							trl.id AS `trailer_id`,
							trl.image,
							trl.file,
							trl.url,
							trl.user_id,
							trl.shows,
							trl.width,
							trl.height,
							trl.order_number,
							trl.public,
							flm_pst.image as `poster`
							FROM `#__main_films_trailers` AS `trl`
							LEFT JOIN `#__main_films_photos` AS `flm_pst`
							ON flm_pst.film_id=trl.film_id
							WHERE trl.film_id='".$filmid."'
							AND trl.public=1
							ORDER BY trl.order_number DESC
							LIMIT 1
							";


							$t=$_db->query($qe);
							$trails=$_db->fetchAssoc($t);

							$obj['file'] = '//kino-teatr.ua/public/main/films/'.$trails['file'];
							$obj['image'] = $trails['image'];

					}

		$obj['lastmod'] = date('Y-m-d');
		$obj['intro'] =  strip_tags($obj['intro']);
		$obj['loc'] = main_films::getFilmUrl($obj['id']);
		$obj['locuk'] = main_films::getFilmUkUrl($obj['id']);
		$result['cinemas'][]=$obj;
		$maxid = $obj['id'];
		$jj = 1;
	}



if ($jj>0)
{

	$xml = '';


	$xml .= '<?xml version="1.0" encoding="UTF-8"?>
<urlset  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">';

foreach ($result['cinemas'] as $cinema)
{
	$xml .= '
	<url>
	<lastmod>'.$cinema['lastmod'].'</lastmod>
	<loc>'.$cinema['loc'].'</loc>
	';



$xml .= '
</url>';

}

	$xml .= '
	</urlset>';

	$xmluk = '';


	$xmluk .= '<?xml version="1.0" encoding="UTF-8"?>
<urlset  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">';

foreach ($result['cinemas'] as $cinema)
{
$xmluk .= '
<url>
<lastmod>'.$cinema['lastmod'].'</lastmod>
<loc>'.$cinema['locuk'].'</loc>
';


if ($cinema['image'])
{
$xmluk .= '
<video:video>
<video:thumbnail_loc>//kino-teatr.ua/public/main/films/'.$cinema['image'].'</video:thumbnail_loc>
<video:title><![CDATA['.$cinema['title'].']]></video:title>
<video:description><![CDATA['.$cinema['intro'].']]></video:description>
<video:live>no</video:live>
<video:rating>'.$cinema['rating'].'</video:rating>
</video:video>';
}

$xmluk .= '</url>';

}

	$xmluk .= '
	</urlset>';


	$theText = $maxid.':'.$newxmlid;

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/films.txt";
	$fh = fopen($myFile, 'w') or die("can't open file1");
	fwrite($fh, $theText);
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/films_".$newxmlid.".xml";
	$fh = fopen($myFile, 'w') or die("can't open file2");
	fwrite($fh, $xml);
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/films_uk_".$newxmlid.".xml";
	$fh = fopen($myFile, 'w') or die("can't open file3");
	fwrite($fh, $xmluk);
	fclose($fh);


	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/films.xml";
	$fh = fopen($myFile, 'r');
	$filmsXML = fread($fh, filesize($myFile));
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/films_uk.xml";
	$fh = fopen($myFile, 'r');
	$filmsXMLUk = fread($fh, filesize($myFile));
	fclose($fh);

	$filmsXML = str_replace('</sitemapindex>','',$filmsXML);

	$filmsXML .= '
	<sitemap>
    <loc>//kino-teatr.ua/public/main/cards/films_'.$newxmlid.'.xml</loc>
    <lastmod>'.date('Y-m-d').'</lastmod>
  </sitemap>
</sitemapindex>';

	$filmsXMLUk = str_replace('</sitemapindex>','',$filmsXMLUk);

	$filmsXMLUk .= '
	<sitemap>
    <loc>//kino-teatr.ua/public/main/cards/films_uk_'.$newxmlid.'.xml</loc>
    <lastmod>'.date('Y-m-d').'</lastmod>
  </sitemap>
</sitemapindex>';


	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/films.xml";
	$fh = fopen($myFile, 'w') or die("can't open file4");
	fwrite($fh, $filmsXML);
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/films_uk.xml";
	$fh = fopen($myFile, 'w') or die("can't open file5");
	fwrite($fh, $filmsXMLUk);
	fclose($fh);

}

	exit;

	//return $result;
}
?>