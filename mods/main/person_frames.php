<?
function main_person_frames()
{
	sys::useLib('main::persons');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('person_id','int');
	sys::filterGet('photo_id','int');
	sys::filterGet('page','int');

	if(!$_GET['person_id'])
		return 404;

	if(!$_GET['photo_id'])
	{
		$q="
			SELECT `photo_id` FROM `#__main_films_photos_persons`
			WHERE `person_id`=".intval($_GET['person_id'])."
			ORDER BY `photo_id` DESC
		";

		if($_GET['page'])
			$q.="LIMIT ".intval($_cfg['main::images_on_page']*(intval($_GET['page'])-1)).",1 ";
		else
			$q.="LIMIT 0,1 ";
		$r=$_db->query($q);
		list($_GET['photo_id'])=$_db->fetchArray($r);
	}

	if(!$_GET['photo_id'])
		return 404;

	$r=$_db->query("
		SELECT
			pht.id as `trailer_id`,
			pht.image,
			flm_lng.title AS `film`,
			flm_lng.record_id AS `film_id`
		FROM
			`#__main_films_photos` AS `pht`
		LEFT JOIN `#__main_films_lng` AS flm_lng
		ON flm_lng.record_id=pht.film_id
		AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE pht.id=".intval($_GET['photo_id'])."
		AND pht.public=1
	");
	$result=$_db->fetchAssoc($r);
	if(!$result)
		return 404;

	$result['person']=main_persons::getPersonFio($_GET['person_id']);

	$meta['person']=$result['person'];
	$meta['film']=$result['film'];
	$result['title'] = sys::translate('main::person_frames').' '.$result['person'];

	if ($_GET['page'])
	{
		$meta['page'] = sys::translate('main::page').' '.$_GET['page'].'. ';
	} else {
		$meta['page'] = '';
	}

	$meta['trl_id'] =  sys::translate('main::frame_no').' '.$result['trailer_id'];


	$result['meta']=sys::parseModTpl('main::person_frames','page',$meta);
	$result['url']=main_films::getPhotoUrl($_GET['photo_id']);
		if($result['image'])
			$image=$_cfg['main::films_url'].$result['image'];
		else
         	$image = 'blank_news_img.jpg';


		$result['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=10';
	$result['next_url']=false;
	$result['prev_url']=false;
	$result['alt']=$result['person'].' '.sys::translate('main::in_film').' &quot;'.$result['film'].'&quot;';
	$result['person_url']=main_persons::getPersonUrl($_GET['person_id']);
	$result['film_url']=main_films::getFilmUrl($result['film_id']);


	if($next_id=main_persons::getNextFrameId($_GET['person_id'],$_GET['photo_id']))
		$result['next_url']=main_persons::getFramesUrl($next_id,$_GET['person_id']);
	if($prev_id=main_persons::getPrevFrameId($_GET['person_id'],$_GET['photo_id']))
		$result['prev_url']=main_persons::getFramesUrl($prev_id,$_GET['person_id']);

	//Получить инфу об остальных фотках-----------------------------------------------
	$q="
		SELECT pht.id,pht.image
		FROM `#__main_films_photos_persons` AS `pht_prs`

		LEFT JOIN `#__main_films_photos` AS `pht`
		ON pht.id=pht_prs.photo_id

		WHERE pht_prs.person_id=".$_GET['person_id']."
		AND pht.public=1
		ORDER BY pht.id DESC
	";
	$result['pages']=sys_pages::pocess($q,$_cfg['main::images_on_page'],false,'?mod=main&act=person_frames&person_id='.$_GET['person_id'].'&page=%page%');
	$r=$_db->query($q.$result['pages']['limit']);
	$result['previews']=array();
	$result['photo_id']=$_REQUEST['photo_id'];
	$i=0;
	while ($obj=$_db->fetchAssoc($r))
	{
		if($obj['image'])
			$image=$_cfg['main::films_url'].$obj['image'];
		else
         	$image = 'blank_news_img.jpg';


		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=8';

		$obj['url']=main_persons::getFramesUrl($obj['id'],$_GET['person_id'],false);
		$obj['break']=$i%4;
		$obj['i']=$i;
		if($_GET['page'])
			$obj['url'].='&page='.$_GET['page'];
		$obj['url']=sys::rewriteUrl($obj['url']);
		$result['previews'][]=$obj;
		$i++;
	}
	//============================================================

	main::countShow($result['person_id'],'person');
	return $result;
}
?>