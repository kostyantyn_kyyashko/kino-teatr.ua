<?
function main_ajx_film_vote_number()
{
	sys::setTpl();
	sys::useLib('main::films');
	global $_user, $_db, $_cfg;

	if(!$_POST['object_id'] || $_user['id']==2)
		return false;

	echo json_encode(main_films::filmVotes($_POST['object_id']));

}
?>