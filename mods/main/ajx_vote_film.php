<?
function main_ajx_vote_film()
{
	sys::setTpl();
	sys::useLib('main::films');
	global $_user, $_db, $_cfg;

	if(!$_POST['object_id']) return false;
	$film_id = intval($_POST['object_id']);

	$r=$_db->query("
		SELECT
			flm.ukraine_premiere,
			flm.world_premiere
		FROM
			`#__main_films` AS `flm`
		WHERE flm.id='".intval($film_id)."'
	");

	$film=$_db->fetchAssoc($r);

	$p_date=strtotime($film['ukraine_premiere']?$film['ukraine_premiere']:$film['world_premiere']);
	//$is_showing=$_db->getValue('main_shows','id',"film_id=".intval($film_id)." AND begin<='".date('Y-m-d', time())."'");
	$is_showing = ($p_date<time());

	if($_user['id']==2) 
		$marker = main_films::SetVoteUnregistered($film_id, $_POST['mark'], $is_showing);
	 else 
	 	$marker = main_films::SetVoteRegistered($film_id, $_user['id'], $_POST['mark'], $is_showing);
	 	
	$marker['is_showing'] = $is_showing;
	$marker['new_mark'] = $_POST['mark'];
	$marker['user_id'] = $_user['id'];
	$marker['film_id'] = $film_id;
	 	
	echo json_encode(main_films::fixMarkFilm($marker));
}

/*
delete FROM `grifix_main_films_rating_unregistered` where film_id=6923;
delete FROM `grifix_main_films_rating` where film_id=6923;
update `grifix_main_films` set rating=0, pre_rating=0, sum=0, pre_sum=0, votes=0, pre_votes=0, world_premiere='2014-06-25' where id=6923;


SELECT pre_rating, pre_sum, pre_votes, rating, sum, votes FROM `grifix_main_films` WHERE id =6923;
SELECT * FROM `grifix_main_films_rating` WHERE film_id =6923;
SELECT * FROM `grifix_main_films_rating_unregistered` WHERE film_id =6923;
*/
?>