<?

function maxsite_str_word($text, $counttext = 10, $sep = ' ') {
	$words = split($sep, $text);
	if ( count($words) > $counttext )
		$text = join($sep, array_slice($words, 0, $counttext)).'...';
	return $text;
}


function main_search_gossip()
{
	sys::useLib('main::genres');
	sys::useLib('main::persons');
	sys::useLib('main::cinemas');
	sys::useLib('main::countries');
	sys::useLib('main::gossip');
	sys::useLib('sys::form');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('keyword');
	$result=array(0);

	$result['meta']=sys::parseModTpl('main::search','page');

	$_GET['keywords'] = strip_tags($_GET['keywords']);
	$_GET['keywords'] = urldecode($_GET['keyword']);


	$q="
		SELECT
			art.id,
			art.date,
			art.user_id,
			art.exclusive,
			art.small_image AS `image`,
			art_lng.title,
			art_lng.intro
		FROM `#__main_gossip_articles` AS `art`

		LEFT JOIN
			`#__main_gossip_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_gossip` AS `nws`
		ON art.gossip_id=nws.id
	";


	if($_GET['keywords'])
		$q.="WHERE
			art_lng.title LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR art_lng.title LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR art_lng.title LIKE '%".mysql_real_escape_string($_GET['keywords'])."'

			";

	$q.="
		ORDER BY art.date DESC
	";

	$result['pages']=sys_pages::pocess($q,$_cfg['main::films_on_search']);


	$r=$_db->query($q.$result['pages']['limit']);



	while($obj=$_db->fetchAssoc($r))
	{

		$obj['round'] = '';

		if($obj['image'])
			$obj['poster']=$_cfg['main::gossip_url'].$obj['image'];
		else
			$obj['poster']=main::getNoImage('x1');

		$obj['date']='<br><font style="color: #AAA; font-size: 10px;">'.date('d.m.Y', strtotime($obj['date'])).'</font><br>';

		$obj['intro'] = strip_tags($obj['intro']);

		$obj['url']=main_gossip::getArticleUrl($obj['id']);
		$result['gossip'][]=$obj;
	}


   	$result['films_title'] = sys::translate('main::films');
   	$result['persons_title'] = sys::translate('main::persons');
   	$result['cinemas_title'] = sys::translate('main::cinemas');
   	$result['gossip_title'] = sys::translate('main::gossip');
	return $result;
}
?>