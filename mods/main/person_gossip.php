<?
function main_person_gossip()
{
	sys::useLib('main::persons');
	sys::useLib('main::gossip');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('person_id','int');

	define('_CANONICAL',main_persons::getPersongossipUrl($_GET['person_id']),true);

	if(!$_GET['person_id'])
		return 404;

	$result['person']=main_persons::getPersonFio($_GET['person_id']);
	if(!$result['person'])
		return 404;

	$meta['person']=$result['person'];
	$result['meta']=sys::parseModTpl('main::person_gossip','page',$meta);

	$q="
		SELECT
			art.id,
			art.date,
			art.small_image AS `image`,
			art.exclusive,
			art_lng.title,
			art_lng.intro
		FROM
			`#__main_gossip_articles_persons` AS `art_prs`

		LEFT JOIN
			`#__main_gossip_articles` AS `art`
		ON
			art.id=art_prs.article_id

		LEFT JOIN
			`#__main_gossip_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE
			art_prs.person_id=".intval($_GET['person_id'])."
		AND
			art.public=1
		AND
			art.date<'".gmdate('Y-m-d H:i:s')."'

		ORDER BY
			art.date DESC
	";

	$result['pages']=sys_pages::pocess($q,$_cfg['main::gossip_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true,$_cfg['sys::date_format']);
		if($obj['image'])
			$obj['image']=$_cfg['main::gossip_url'].$obj['image'];
		else
			$obj['image']=main::getNoImage('x1');
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_gossip::getArticleUrl($obj['id']);
		$result['objects'][]=$obj;
	}
	main::countShow($_GET['person_id'],'person');
	return $result;
}
?>