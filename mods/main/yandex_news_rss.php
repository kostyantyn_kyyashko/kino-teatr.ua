<?
function main_yandex_news_rss()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::news');
	sys::setTpl();

//	$cache_name='news_rss_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
//	if($cache=sys::getCache($cache_name,$_cfg['main::RSS_feed_cache_period']*60)) return unserialize($cache);

	$r=$_db->query("
		SELECT 
			art.id,
			art.date,
			art.user_id,
			art.exclusive,
			art.small_image AS `image`,
			art_lng.title,
			art_lng.intro,
			art_lng.text,
			nws_lng.title AS `category`,
			usr.login
		FROM `#__main_news_articles` AS `art`
		
		LEFT JOIN `#__main_news_articles_lng` AS `art_lng`
		ON art_lng.record_id=art.id
		AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		
		LEFT JOIN `#__main_news_lng` AS `nws_lng`
		ON nws_lng.record_id=art.news_id
		AND nws_lng.lang_id=".$_cfg['sys::lang_id']."
		
		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=art.user_id
				
		WHERE art.public=1
		AND art.news_id!=9
		AND art.date<'".gmdate('Y-m-d H:i:s')."'
		
		ORDER BY art.date DESC
		
		LIMIT 0,10
			");

	$result=array();
	$result['objects']=array();
	
$search = array ("'<script[^>]*?>.*?</script>'si",  // Вырезает javaScript
                 "'<[\/\!]*?[^<>]*?>'si",           // Вырезает HTML-теги
                 "'([\r\n])[\s]+'",                 // Вырезает пробельные символы
                 "'&(quot|#34);'i",                 // Заменяет HTML-сущности
                 "'&(amp|#38);'i",
                 "'&(lt|#60);'i",
                 "'&(gt|#62);'i",
                 "'&(nbsp|#160);'i",
                 "'&(iexcl|#161);'i",
                 "'&(cent|#162);'i",
                 "'&(pound|#163);'i",
                 "'&(copy|#169);'i",
                 "'Loading the player \.\.\.'i",
                 "'&#(\d+);'e");                    // интерпретировать как php-код

$replace = array ("",
                  "",
                  "\\1",
                  "\"",
                  "&",
                  "<",
                  ">",
                  " ",
                  chr(161),
                  chr(162),
                  chr(163),
                  chr(169),
                  "",
                  "chr(\\1)");

	while($obj=$_db->fetchAssoc($r))
	{
		
//if(sys::isDebugIP()) var_dump($obj["text"]);		
		
		$obj['date']=date('r',sys::db2Timestamp($obj['date']));	
		if($obj['image'] && is_file($_cfg['main::news_dir'].$obj['image']))
		{
			$obj['image_src']=$_cfg['main::news_url'].$obj['image'];
			$obj['image_mime']=getimagesize($_cfg['main::news_dir'].$obj['image']) ;
			$obj['image_mime']=$obj['image_mime']['mime'];
			$obj['image_size']=filesize($_cfg['main::news_dir'].$obj['image']);
		}
		else 
			$obj['image']=false;
		$obj['title']=htmlspecialchars(stripslashes(strip_tags($obj['title'])));
		$obj['url']=main_news::getArticleUrl($obj['id']);
		$obj['intro']=strip_tags($obj['intro']);
		$obj['login']=strip_tags($obj['login']);
		$obj['text'] = preg_replace($search, $replace, $obj['text']);
		$obj['text'] = preg_replace ("~(\\\|\*|\?|\[|\?|\]|\(|\\\$|\))~", "", html_entity_decode(strip_tags($obj['text'])));
		$obj['text'] = preg_replace("/(\r\n){1,}/", "", $obj['text']);
		$obj['text'] = preg_replace("/ +/", " ", $obj['text']);
	
		$result['objects'][]=$obj;
	}
//	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>