<?
function main_ajx_PopularGossip()
{
	sys::setTpl();
	sys::filterGet('keywords');
	sys::filterGet('field');
	sys::filterGet('mode');
	sys::useLib('main::gossip');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	echo main_gossip::showLastPopularArticles();
}
?>