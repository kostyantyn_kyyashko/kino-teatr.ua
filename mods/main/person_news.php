<?
function main_person_news()
{	
	sys::useLib('main::persons');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('person_id','int');

//if(sys::isDebugIP()) {
//	var_dump($_GET);
//	die;
//}	
	
	define('_CANONICAL',main_persons::getPersonNewsUrl($_GET['person_id']),true);

	if(!$_GET['person_id'])
		return 404;

	$result['person']=main_persons::getPersonFio($_GET['person_id']);
	if(!$result['person'])
		return 404;

	$meta['person']=$result['person'];
	$result['title'] = sys::translate('main::news_about_person').' '.$result['person'];
	$result['meta']=sys::parseModTpl('main::person_news','page',$meta);

	$tables = array("news", "articles", "gossip", "interview", "serials");		
	$q = array();
	foreach ($tables as $table) {
		sys::useLib("main::$table");
		$q[]="
			(SELECT
				art.id,
				art.date,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				art_lng.title,
				art_lng.intro,
				'$table' as tbl
			FROM
				`#__main_".$table."_articles_persons` AS `art_prs`
	
			LEFT JOIN
				`#__main_".$table."_articles` AS `art`
			ON
				art.id=art_prs.article_id
	
			LEFT JOIN
				`#__main_".$table."_articles_lng` AS `art_lng`
			ON
				art_lng.record_id=art.id
			AND
				art_lng.lang_id=".intval($_cfg['sys::lang_id'])."
	
			WHERE
				art_prs.person_id=".intval($_GET['person_id'])."
			AND
				art.public=1
			) 
		";
	}
	$q = "SELECT * FROM (" . implode(" UNION ", $q) . ") AS T ORDER BY date DESC";
	
//$_db->printR($q);	
	
	$result['pages']=sys_pages::pocess($q,$_cfg['main::news_on_page'],false,false,false,true,'page',false);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$tbl = $obj['tbl'];
		$method = "main_$tbl::getArticleUrl({$obj['id']});";  

		$obj['date']=sys::russianDate($obj['date']);
		if($obj['image'] || $obj['big_image'])
		{
  	    	$image=$obj['image'];	
  	    	list($wid, $hei, $type) = getimagesize("./public/main/$tbl/$image");
			if($wid<213 && $obj['big_image']) $image = $obj['big_image'];

			if(!file_exists("./public/main/$tbl/$image")) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg["main::".$tbl."_url"].$image;
			$obj['image'] = $image;
		}
		else 
		 $image = $_cfg['sys::root_url'].'blank_news_img.jpg';


		$obj['title']= sys::translate("main::$tbl") .": ". htmlspecialchars($obj['title']);
		eval('$obj["url"] = ' . $method . ";");	
		$result['objects'][]=$obj;
	}
	
	return $result;		
	
	
//	
//	$result['pages']=sys_pages::pocess($q,$_cfg['main::news_on_page']);
//	$r=$_db->query($q.$result['pages']['limit']);
//	$result['objects']=array();
//	while ($obj=$_db->fetchAssoc($r))
//	{
//		$obj['date']=sys::db2Date($obj['date'],true,$_cfg['sys::date_format']);
//
//		$_image=$obj['big_image']?$obj['big_image']:$obj['image'];
//		if($_image)
//		{
//	 	    $image="x2_".$_image;
//			if(!file_exists($_cfg['main::news_dir'].$image)) 
//			{
//	 	    	$image=$_image;
//	 	    	if(!file_exists($_cfg['main::news_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
//			}
//			
//			$image=$_cfg['main::news_url'].$image;
//			$obj['image'] = $image;
//		}
//
//		$obj['title']=htmlspecialchars($obj['title']);
//		$obj['url']=main_news::getArticleUrl($obj['id']);
//		$result['objects'][]=$obj;
//	}
//	main::countShow($_GET['person_id'],'person');
//	return $result;
}
?>