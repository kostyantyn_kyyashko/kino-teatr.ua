<?
function main_film_anothers()
{
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');

	define('_CANONICAL',main_films::getFilmAnothersUrl($_GET['film_id']),true);

	if(!$_GET['film_id'])
		return 404;

	$result['film']=$_db->getValue('main_films','title',intval($_GET['film_id']),true);
	if(!$result['film'])
		return 404;
	$result['serial']=$_db->getValue('main_films','serial',intval($_GET['film_id']),false);
//	if(sys::isDebugIP()) $_db->printR($result);
	
	main::countShow($_GET['film_id'],'film');

	$meta['film']=$result['film'];
	$result['title'] = $result['film'];
	$result['meta']=sys::parseModTpl('main::film_anothers','page',$meta);

	$tables = array("articles", "gossip", "interview", "serials");		
//	$tables = array("articles");		

	$q = array();
	foreach ($tables as $table) {
		sys::useLib("main::$table");
		$q[]="
			(SELECT
				art.id,
				art.date,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				art_lng.title,
				art_lng.intro,
				'$table' as tbl
			FROM
				`#__main_".$table."_articles_films` AS `art_flm`
	
			LEFT JOIN
				`#__main_".$table."_articles` AS `art`
			ON
				art.id=art_flm.article_id
	
			LEFT JOIN
				`#__main_".$table."_articles_lng` AS `art_lng`
			ON
				art_lng.record_id=art.id
			AND
				art_lng.lang_id=".intval($_cfg['sys::lang_id'])."
	
			WHERE
				art_flm.film_id=".intval($_GET['film_id'])."
			AND
				art.public=1
			) 
		";
	}
	$q = "SELECT * FROM (" . implode(" UNION ", $q) . ") AS T ORDER BY date DESC";
//	if(sys::isDebugIP()) $_db->printR($q);
	
	$result['pages']=sys_pages::pocess($q,$_cfg['main::news_on_page'],false,false,false,true,'page',false);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$tbl = $obj['tbl'];
		$method = "main_$tbl::getArticleUrl({$obj['id']});";  

		$obj['date']=sys::russianDate($obj['date']);
		if($obj['image'] || $obj['big_image'])
		{
  	    	$image=$obj['image'];	
  	    	list($wid, $hei, $type) = getimagesize("./public/main/$tbl/$image");
			if($wid<213 && $obj['big_image']) $image = $obj['big_image'];

			if(!file_exists("./public/main/$tbl/$image")) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg["main::".$tbl."_url"].$image;
			$obj['image'] = $image;
		}
		else 
		 $image = $_cfg['sys::root_url'].'blank_news_img.jpg';


		$obj['title']= sys::translate("main::$tbl") .": ". htmlspecialchars($obj['title']);
		eval('$obj["url"] = ' . $method . ";");	
		$result['objects'][]=$obj;
	}

	return $result;
}
?>