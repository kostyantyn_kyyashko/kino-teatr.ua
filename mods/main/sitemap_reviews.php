<?
function main_sitemap_reviews()
{
	sys::useLib('main::reviews');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/reviews.txt";
	$fh = fopen($myFile, 'r');
	$theData = fread($fh, filesize($myFile));
	fclose($fh);

	$data = explode(':', $theData);

	$filmid = $data[0];
	$newxmlid = (int) $data[1]+1;


	$result=array();

	//Жанры----------------------------------------------------------------
	$r=$_db->query("
		SELECT
			*
		FROM
			`#__main_reviews`
		WHERE `id`>".$filmid."
		ORDER BY `id`
		LIMIT 500
	");


	$jj = 0;

	$result['films']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
//		$obj['lastmod'] = date('Y-m-d', strtotime($obj['date']));
		$obj['lastmod'] = date('Y-m-d');
		$obj['loc'] = main_reviews::getReviewUrl($obj['id'],"ru");
		$obj['locuk'] = main_reviews::getReviewUrl($obj['id'],"uk");
		//$obj['locuk'] = main_reviews::getReviewUkUrl($obj['id']);
		$result['cinemas'][]=$obj;
		$maxid = $obj['id'];
		$jj = 1;
	}



if ($jj>0)
{

	$xml = '';


	$xml .= '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

foreach ($result['cinemas'] as $cinema)
{
	$xml .= '
<url>
	<lastmod>'.$cinema['lastmod'].'</lastmod>
	<loc>'.$cinema['loc'].'</loc>
	<priority>1</priority>
	<changefreq>daily</changefreq>
</url>';

}

	$xml .= '
	</urlset>';

	$xmluk = '';


	$xmluk .= '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

foreach ($result['cinemas'] as $cinema)
{
	$xmluk .= '
<url>
	<lastmod>'.$cinema['lastmod'].'</lastmod>
	<loc>'.$cinema['locuk'].'</loc>
	<priority>1</priority>
	<changefreq>daily</changefreq>
</url>';

}

	$xmluk .= '
	</urlset>';


	$theText = $maxid.':'.$newxmlid;

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/reviews.txt";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $theText);
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/reviews_".$newxmlid.".xml";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $xml);
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/reviews_uk_".$newxmlid.".xml";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $xmluk);
	fclose($fh);


	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/reviews.xml";
	$fh = fopen($myFile, 'r');
	$filmsXML = fread($fh, filesize($myFile));
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/reviews_uk.xml";
	$fh = fopen($myFile, 'r');
	$filmsXMLUk = fread($fh, filesize($myFile));
	fclose($fh);

	$filmsXML = str_replace('</sitemapindex>','',$filmsXML);

	$filmsXML .= '
	<sitemap>
    <loc>//kino-teatr.ua/public/main/cards/reviews_'.$newxmlid.'.xml</loc>
    <lastmod>'.date('Y-m-d').'</lastmod>
  </sitemap>
</sitemapindex>';

	$filmsXMLUk = str_replace('</sitemapindex>','',$filmsXMLUk);

	$filmsXMLUk .= '
	<sitemap>
    <loc>//kino-teatr.ua/public/main/cards/reviews_uk_'.$newxmlid.'.xml</loc>
    <lastmod>'.date('Y-m-d').'</lastmod>
  </sitemap>
</sitemapindex>';


	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/reviews.xml";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $filmsXML);
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/reviews_uk.xml";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $filmsXMLUk);
	fclose($fh);

}

	exit;

	//return $result;
}
?>