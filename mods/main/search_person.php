<?

function maxsite_str_word($text, $counttext = 10, $sep = ' ') {
	$words = split($sep, $text);
	if ( count($words) > $counttext )
		$text = join($sep, array_slice($words, 0, $counttext)).'...';
	return $text;
}


function main_search_person()
{
	sys::useLib('main::genres');
	sys::useLib('main::persons');
	sys::useLib('main::cinemas');
	sys::useLib('main::countries');
	sys::useLib('main::news');
	sys::useLib('sys::form');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('keyword');
	$result=array(0);

	$result['meta']=sys::parseModTpl('main::search','page');

	$_GET['keywords'] = strip_tags($_GET['keywords']);
	$_GET['keywords'] = urldecode($_GET['keyword']);

	$q="
		SELECT
			flm.id,
			flm.lastname_orig,
			flm.firstname_orig,
			flm.rating,
			flm.rating_votes,
			flm.birthdate,
			lng.biography,
			lng.lastname,
			lng.firstname
		FROM
			`grifix_main_persons` AS `flm`
		LEFT JOIN
			`grifix_main_persons_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	if($_GET['keywords'])
		$q.="WHERE
			`lastname_orig` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `firstname_orig` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `lastname` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `firstname` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			";

	$q.=" ORDER BY flm.total_shows DESC";
	$result['pages']=sys_pages::pocess($q,$_cfg['main::films_on_search']);

	$r=$_db->query($q.$result['pages']['limit']);


	while($obj=$_db->fetchAssoc($r))
	{

		$qw=$_db->query("
			SELECT
				pst.image as `poster`

			FROM `#__main_persons_photos` AS `pst`

			WHERE pst.person_id=".intval($obj['id'])."
			AND pst.order_number=1
			LIMIT 1
		");

		$obj['title'] = $obj['firstname'].' '.$obj['lastname'];

		$poster = '';

		while($object=$_db->fetchAssoc($qw))
		{
			$poster = $object['poster'];
		}

// inserted by Mike begin
		if($poster)
		{
    	$image="x3_".$poster; 
		if(!file_exists('./public/main/persons/'.$image)) $image = $_cfg['sys::root_url'].'images/userpic.jpg';
		 else $image=$_cfg['main::persons_url'].$image;
		} else {
         	$image = $_cfg['sys::root_url'].'images/userpic.jpg';
		}
		$obj['poster'] = $image;
// inserted by Mike end

/*		if ($poster)
		{
			$obj['poster'] = _ROOT_URL.'public/main/resize2.php?f='.$poster.'&w=60';
		} else {
			$obj['poster'] = _ROOT_URL.'ims/kt_logo.jpg';
		}
*/

		if ($obj['rating']>0)
		{
			$obj['rate'] = sys::translate('main::rating').': <strong>'.round($obj['rating'], 1).'</strong> / '.sys::translate('main::votes').': '.$obj['rating_votes'].'<br>';
		}


		$obj['biography'] = strip_tags(maxsite_str_word($obj['biography'], 30).'...');

		$obj['round'] = '';

		if ($obj['firstname_orig'])
		{
			$obj['round'] .= $obj['firstname_orig'].' '.$obj['lastname_orig'];
		}





		$obj['url']=main_persons::getPersonUrl($obj['id']);
		$result['persons'][]=$obj;
	}

   	$result['films_title'] = sys::translate('main::films');
   	$result['persons_title'] = sys::translate('main::persons');
   	$result['cinemas_title'] = sys::translate('main::cinemas');
   	$result['news_title'] = sys::translate('main::news');
	return $result;
}
?>