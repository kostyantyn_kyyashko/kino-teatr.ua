<?
function main_user_reviews()
{
	sys::useLib('main::users');
	sys::useLib('sys::pages');
	sys::useLib('main::films');
	sys::useLib('main::reviews');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('user_id','int');


	if(!$_GET['user_id'])
		return 404;

	$result['user']=$_db->getValue('sys_users','login',intval($_GET['user_id']));
	$meta['user']=$result['user'];
	if ($_GET['page'])
	{
		$meta['page'] = sys::translate('main::page').' '.$_GET['page'];
	} else {
		$meta['page'] = '';
	}

	$result['meta']=sys::parseModTpl('main::user_reviews','page',$meta);
	if(!$result['user'])
		return 404;

	if(isset($_POST['_task']) && $_POST['_task'])
	{
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete' && sys::checkAccess('main::reviews_delete'))
			main_reviews::deleteReview($task[1]);
	}

	$q="

	SELECT
			rev.id,
			rev.film_id,
			rev.user_id,
			rev.total_shows,
			rev.comments,
			rev.title,
			rev.intro,
			rev.date,
			rev.image,
			flm_lng.title AS `film`,
			rev.thanks,
			usr.login AS `user`
		FROM
			`#__main_reviews` AS `rev`

		LEFT JOIN
			`#__main_films_lng` AS `flm_lng`
		ON
			flm_lng.record_id=rev.film_id
		AND
			flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN
			`#__sys_users` AS `usr`
		ON
			usr.id=rev.user_id
		LEFT JOIN
			`#__sys_users_groups` AS `users`
		ON
			usr.id=users.user_id
		WHERE rev.public=1
              AND rev.user_id=".intval($_GET['user_id'])."
		ORDER BY rev.date DESC

	";
	$result['objects']=array();
	$result['pages']=sys_pages::pocess($q,$_cfg['main::reviews_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['film']=htmlspecialchars($obj['film']);
		$obj['url']=main_reviews::getReviewUrl($obj['id']);
		$obj['intro'] = strip_tags($obj['intro']);

		if($obj['image'])
		{
					$image=$_cfg['main::reviews_url'].$obj['image'];

                	list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));
 		}



		if ($wid>=200 && $obj['image'])
		{
		 	$image = $_cfg['main::reviews_url'].$obj['image'];
		} else {
           	$poster=main_films::getFilmFirstPoster($obj['film_id']);

           	if ($poster)
           	{
				$image=$_cfg['main::films_url'].$poster['image'];
           	} else {
         	$image = 'blank_news_img.jpg';
           	}
		}


		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';

		if(sys::checkAccess('main::reviews_edit') || $_user['id']==$obj['user_id'])
			$obj['edit_url']=main_reviews::getReviewEditUrl($obj['id']);
		else
			$obj['edit_url']=false;

		$obj['shows']=$obj['total_shows'];
		$obj['date'] = date('Y-m-d', strtotime($obj['date']));
   		$obj['date']=sys::russianDate($obj['date']);


		if(sys::checkAccess('main::reviews_delete') || $_user['id']==$obj['user_id'])
			$obj['delete']=true;
		else
			$obj['delete']=false;

		$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
		$obj['user_url']=main_users::getUserUrl($obj['user_id']);

		$result['objects'][]=$obj;
	}
	if(!$result['objects'])
		return 404;

	return $result;
}
?>