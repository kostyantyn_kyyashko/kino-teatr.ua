<?
function main_film_poster()
{
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('poster_id','int');
	
	if(!$_GET['poster_id'])
		return 404;

		$q="
			SELECT 
				pst.film_id,
				pst.image,
				flm_lng.title AS `film`
			FROM `#__main_films_posters` AS `pst`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=pst.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE pst.id=".intval($_GET['poster_id'])."
			AND pst.public=1
		";
	
	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);
	
	
	if(!$result)
		return 404;
		
	main::countShow($result['film_id'],'film');
		
	$meta['film']=$result['film'];	
	$result['meta']=sys::parseModTpl('main::film_poster','page',$meta);
	
	$result['image']=$_cfg['main::films_url'].$result['image'];
	
	return $result;
}
?>