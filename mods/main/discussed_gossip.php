<?
function main_discussed_gossip()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::gossip');
	sys::useLib('main::spec_themes');
	sys::useLib('main::users');
	sys::useLib('sys::pages');

	sys::filterGet('month');
	sys::filterGet('year');
	sys::filterGet('gossip_id');

	if ($_GET['page']!='')
	{
		define('_NOINDEX',1,true);
	}


		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		define('_CANONICAL',_ROOT_URL.'gossip.phtml',true);
		} else {
		define('_CANONICAL',_ROOT_URL.$lang.'/gossip.phtml',true);
		}

	$cache_name='main_gossip_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::gossip_page_cache_period']*60))
		return unserialize($cache);

	/*if($_GET['year']<2000 || $_GET['year']>intval(date('Y')) || $_GET['month']<1 || $_GET['month']>12)
		return 404;*/


	if($_GET['gossip_id'])
		$result['section']=$meta['section']=$_db->getValue('main_gossip','title',intval($_GET['gossip_id']),true);
	else
		$result['section']=$meta['section']=sys::translate('main::all_sections');

	if(!$result['section'])
		return 404;

	if(isset($_POST['_task']) && $_POST['_task'])
	{
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete' && sys::checkAccess('main::gossip_delete'))
			main_gossip::deleteArticle($task[1]);
	}

	$result['date']=$meta['date']=mb_strtolower(sys::translate('main::month_'.intval($_GET['month']))).' '.$_GET['year'];
	$result['meta']=sys::parseModTpl('main::gossip','page',$meta);
	$result['title']=sys::translate('main::gossip_cinema');
	$month_begin=date('Y-m-d H:i:s',main_gossip::getMonthBegin($_GET['month'],$_GET['year']));
	$month_end=date('Y-m-d H:i:s',main_gossip::getMonthEnd($_GET['month'],$_GET['year']));
	$q="
		SELECT
			art.id,
			art.date,
			art.total_shows,
			art.comments,
			art.user_id,
			art.exclusive,
			spec.spec_theme_id as spec_theme,
			art.small_image AS `image`,
			art.image AS `big_image`,
			art_lng.title,
			art_lng.intro,
			usr.login AS `user`,
			usr_data.main_nick_name
		FROM `#__main_gossip_articles` AS `art`

		LEFT JOIN
			`#__main_gossip_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_gossip` AS `nws`
		ON art.gossip_id=nws.id

		LEFT JOIN `#__main_gossip_articles_spec_themes` AS `spec`
		ON art.id=spec.article_id
		
		LEFT JOIN
			`#__sys_users` AS `usr`
		ON
			usr.id=art.user_id

		LEFT JOIN
			`#__sys_users_data` AS `usr_data`
		ON
			usr.id=usr_data.user_id
		WHERE
			/*art.date BETWEEN '".$month_begin."' AND '".$month_end."'
			AND */(art.city_id=".intval($_cfg['main::city_id'])." OR art.city_id=0)
			AND art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND nws.showtab=1
	";
	if($_GET['gossip_id'])
	{
		$q.="
			AND art.gossip_id=".intval($_GET['gossip_id'])."
		";
	}
	$q.="
			ORDER BY art.comments DESC
	";

	$result['pages']=sys_pages::pocess($q,$_cfg['main::gossip_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::russianDate($obj['date']);
		$obj['shows']=$obj['total_shows'];
		$obj['user_url']=main_users::getUserUrl($obj['user_id']);

		if($obj['image'])
			$image=$_cfg['main::gossip_url'].$obj['image'];

		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$obj['image']));

		if ($wid<213 && $obj['big_image'])
		{
		 	$image = $_cfg['main::gossip_url'].$obj['big_image'];
		} else if ($wid>=213)
		{
		 	$image = $_cfg['main::gossip_url'].$obj['image'];
		} else {
         	$image = 'blank_news_img.jpg';
		}

		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';


		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_gossip::getArticleUrl($obj['id']);
		$obj['spec_theme_url']=main_spec_themes::getArticleUrl($obj['spec_theme']);
		
		if(sys::checkAccess('main::gossip_edit') || $_user['id']==$obj['user_id'])
			$obj['edit_url']=main_gossip::getArticleEditUrl($obj['id']);
		else
			$obj['edit_url']=false;

		if(sys::checkAccess('main::gossip_delete') || $_user['id']==$obj['user_id'])
			$obj['delete']=true;
		else
			$obj['delete']=false;
		$result['objects'][]=$obj;
	}

	if(!$result['objects']/* && !main_gossip::gossipReirect($_GET['year'],$_GET['month'],$_GET['gossip_id'])*/)
		return 404;

	if(sys::checkAccess('main::gossip'))
		$result['add_url']=main_gossip::getArticleAddUrl();
	else
		$result['add_url']=false;

		$result['all_news']=sys::rewriteUrl('?mod=main&act=gossip');
		$result['popular_news']=sys::rewriteUrl('?mod=main&act=popular_gossip');
		$result['discussed_news']=sys::rewriteUrl('?mod=main&act=discussed_gossip');

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>