<?
function main_user_profile_films()
{
	define('_NOINDEX','1');
	sys::useLib('main::users');
	sys::useLib('sys::form');
	sys::useLib('sys::pages');
	sys::useLib('main::films');
	sys::useJs('sys::gui');
	sys::filterGet('order_by','text','title.asc');
	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;
	if($_user['id']==2)
		return 401;

	$result['meta']=sys::parseModTpl('main::user_profile_films','page');

	if(isset($_POST['_task']) && $_POST['_task'])
	{
		$task=explode('.',$_POST['_task']);
		if($task[0]=='unTrack')
			main_films::untrackingFilm($task[1]);
		sys::redirect(main_users::getProfileFilmsUrl(),false);
	}

	//Определение направления сортировки
	$order_by=explode('.',$_GET['order_by']);
	$order_fields=array('title');
	if($order_by[1]=='asc')
	{
		$result['order']='asc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.desc');
		}

	}
	else
	{
		$result['order']='desc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.asc');
		}
	}
	$result['order_field']=$order_by[0];

	$order_by=sys::parseOrderBy($_GET['order_by']);
	//=====================================================

	$q="
		SELECT
			usr_flm.film_id,
			flm_lng.title
		FROM
			`#__main_users_films_subscribe` AS `usr_flm`

		LEFT JOIN
			`#__main_films_lng` AS `flm_lng`
		ON
			flm_lng.record_id=usr_flm.film_id
		AND
			flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE
			usr_flm.user_id=".intval($_user['id'])."

		".$order_by."
	";
	$result['pages']=sys_pages::pocess($q,$_cfg['main::films_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();

	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['url']=main_films::getFilmUrl($obj['film_id']);
		$obj['title']=htmlspecialchars($obj['title']);
		$result['objects'][]=$obj;
	}

	return $result;
}
?>