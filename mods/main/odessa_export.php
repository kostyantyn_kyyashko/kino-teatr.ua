<?
function main_odessa_export()
{


	$ips=file(_SECURE_DIR.'export.txt');
	foreach ($ips as $ip)
	{
		$ip=explode('#',$ip);
		$ips2[]=trim($ip[0]);
	}
//	if(!in_array($_SERVER['REMOTE_ADDR'],$ips2))
//		return 404;
	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('main::persons');
	sys::useLib('main::reviews');
	sys::useLib('main::countries');
	sys::useLib('sys::form');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	if(!sys::checkAccess('main::bill_export'))
		return 403;

	$cache_name='main_bill_xml_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	//if($cache=sys::getCache($cache_name,$_cfg['main::billboard_xml_cache_period']*_HOUR))
	//	return unserialize($cache);
	$result=array();

	$xml = '<?xml version="1.0" encoding="UTF-8"?>
	<billboard>';


$cinemas = array('55', '104', '148', '210');

	//Показы-----------------------------------------------------------------
	$r=$_db->query("
			SELECT
			shw.begin,
			shw.end,
			shw.film_id,
			shw.hall_id,
			shw.id,
			hls.cinema_id,
			hls.`3d` as `hall_3d`,
			flm.`3d` as `film_3d`
		FROM
			`#__main_shows` AS `shw`
		LEFT JOIN
			`#__main_cinemas_halls` AS `hls`
		ON
			hls.id=shw.hall_id
		LEFT JOIN
			`#__main_films` AS `flm`
		ON
			flm.id=shw.film_id

		WHERE
			shw.end>='".mysql_real_escape_string(date('Y-m-d'))."'
		AND
			hls.cinema_id IN(".implode(',',$cinemas).")
		ORDER BY
			shw.end ASC
	");

	$result['shows']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$films[$obj['film_id']]=$obj['film_id'];
		$cinemas[$obj['cinema_id']]=$obj['cinema_id'];
		$halls[$obj['hall_id']]=$obj['hall_id'];
		$shows[$obj['id']]=$obj['id'];

  		if ($obj['hall_3d'] && $obj['film_3d'])
  		{  			$obj['3d'] = 1;
  		} else {
  			$obj['3d'] = 0;
  		}

		$obj['times']=array();
		$result['shows'][$obj['id']]=$obj;
	}

	//=======================================================================

	//Премьеры

	$r=$_db->query("
		SELECT `id` FROM `#__main_films` WHERE `ukraine_premiere`>'".date('Y-m-d')."'
	");
	while (list($film_id)=$_db->fetchArray($r))
	{
		$films[$film_id]=$film_id;
	}



	//Часы показов--------------------------------------------------------

	$r=$_db->query("
		SELECT
			tms.time,
			tms.prices,
			tms_lng.note,
			tms.show_id
		FROM
			`#__main_shows_times` AS `tms`
		LEFT JOIN
			`#__main_shows_times_lng` AS `tms_lng`
		ON
			tms_lng.record_id=tms.id
		AND
			tms_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			tms.show_id IN(".implode(',',$shows).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['prices']=str_replace(';',',',$obj['prices']);
		$result['shows'][$obj['show_id']]['times'][]=$obj;
	}
	//=========================================================================

	//Фильмы------------------------------------------------------------------
	$r=$_db->query("
		SELECT
			flm.id,
			lng.title,
			flm.title_orig,
			flm.duration,
			flm.year,
			flm.ukraine_premiere,
			flm.world_premiere,
			flm.age_limit,
			flm.budget,
			flm.budget_currency,
			lng.intro,
			lng.text,
			flm.rating,
			flm.votes,
			flm.`3d` as `film_3d`,
			flm.pro_rating,
			flm.children,
			flm.pro_votes
		FROM
			`#__main_films` AS `flm`
		LEFT JOIN
			`#__main_films_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".$_cfg['sys::lang_id']."
		WHERE
			flm.id IN(".implode(',',$films).")

	");
	$result['films']=array();
	{
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['title_orig']=htmlspecialchars($obj['title_orig']);
			$obj['photos']=array();
			$obj['trailers']=array();
			$obj['posters']=array();
			$obj['persons']=array();
			$obj['genres']=array();
			$obj['countries']=array();
			$obj['studios']=array();
			$obj['reviews']=array();
			$obj['links']=array();
  			$poster='';
			$poster=main_films::getFilmFirstPoster($obj['id']);
			$img = str_replace(_ROOT_URL,'',$_cfg['main::films_url'].$poster['image']);
			$obj['poster']=_ROOT_URL.'public/main/rescrop3.php?f='.$img;

			$result['films'][$obj['id']]=$obj;
		}
	}
	//============================================================================





	//Кинотеатры-----------------------------------------------------
	$r=$_db->query("
		SELECT
			cnm.id,
			cnm.city_id,
			lng.title,
			cnm.site,
			cnm.phone,
			lng.address,
			lng.text
		FROM
			`#__main_cinemas` AS `cnm`
		LEFT JOIN
			`#__main_cinemas_lng` AS `lng`
		ON
			lng.record_id=cnm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			cnm.id IN(".implode(',',$cinemas).")
	");
	$result['cinemas']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['address']=htmlspecialchars($obj['address']);
		$obj['phone']=htmlspecialchars($obj['phone']);
		$obj['site']=htmlspecialchars($obj['site']);
		$obj['halls']=array();
		$obj['photos']=array();
		$result['cinemas'][$obj['id']]=$obj;
	}




	//============================================================

	//Фотки кинотеатра------------------------------------------------
	$r=$_db->query("
		SELECT
			`cinema_id`,
			`image`
		FROM
			`#__main_cinemas_photos`
		WHERE
			`cinema_id` IN(".implode(',',$cinemas).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{


		$img = str_replace(_ROOT_URL,'',$_cfg['main::cinemas_url'].$obj['image']);
		$result['cinemas'][$obj['cinema_id']]['photo']=_ROOT_URL.'public/main/rescrop3.php?f='.$img;
	}
	//=============================================================



	//Залы кинотеатра--------------------------------------------
	$r=$_db->query("
		SELECT
			hls.id,
			hls.cinema_id,
			hls.`3d`,
			lng.title,
			hls.scheme
		FROM
			`#__main_cinemas_halls` AS `hls`
		LEFT JOIN
			`#__main_cinemas_halls_lng` AS `lng`
		ON
			lng.record_id=hls.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			hls.cinema_id IN(".implode(',',$cinemas).")

	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		if($obj['scheme'])
			$obj['scheme']=$_cfg['main::cinemas_url'].$obj['scheme'];
		$result['cinemas'][$obj['cinema_id']]['halls'][]=$obj;
	}
	//=============================================================





	$xml .= '
	<cinemas>';
		foreach ($result['cinemas'] as $cinema)
		{
			$xml .= '
			<cinema id="'.$cinema['id'].'" city_id="'.$cinema['city_id'].'">
				<city_id>'.$cinema['city_id'].'</city_id>
				<type>Кинотеатр</type>
				<title>'.$cinema['title'].'</title>
				<id>'.$cinema['id'].'</id>
				<address>'.$cinema['address'].'</address>
				<phone>'.$cinema['phone'].'</phone>';
				if($cinema['site'])
				{
					$xml .= '
					<site>'.$cinema['site'].'</site>';
				}
				$xml .= '
				<text><![CDATA['.$cinema['text'].']]></text>';
					if($cinema['photo'])
					{
						$xml .= '
						<photo src="'.$cinema['photo'].'"/>';
					}

				$xml .= '
				<halls>';
					foreach ($cinema['halls'] as $hall)
					{
						$xml .= '
						<hall id="'.$hall['id'].'">
							<title>'.$hall['title'].'</title>
							<id>'.$hall['id'].'</id>
						</hall>';
					}
				$xml .= '
				</halls>
			</cinema>';
		}
	$xml .= '
	</cinemas>';


	$xml .= '
	<shows>';
		foreach ($result['shows'] as $show)
		{
		 	$night = 0;

			$xml .= '
			<show id="'.$show['id'].'" film_id="'.$show['film_id'].'" cinema_id="'.$show['cinema_id'].'" hall_id="'.$show['hall_id'].'">
				<film_id>'.$show['film_id'].'</film_id>
				<cinema_id>'.$show['cinema_id'].'</cinema_id>
				<hall_id>'.$show['hall_id'].'</hall_id>
				<begin>'.$show['begin'].'</begin>
				<ddd>'.$show['3d'].'</ddd>
				<end>'.$show['end'].'</end>
				<times>';
					foreach ($show['times'] as $time)
					{

						if ($time['time'] < '05:00:00')
						{
							$night = 1;
						} else {							$xml .= '
							<time time="'.$time['time'].'">
								<prices>'.$time['prices'].'</prices>
							</time>';
						}

					}
				$xml .= '
				</times>
			</show>';

			if ($night)
			{				$xml .= '
				<show id="'.$show['id'].'" film_id="'.$show['film_id'].'" cinema_id="'.$show['cinema_id'].'" hall_id="'.$show['hall_id'].'">
					<film_id>'.$show['film_id'].'</film_id>
					<cinema_id>'.$show['cinema_id'].'</cinema_id>
					<hall_id>'.$show['hall_id'].'</hall_id>
					<begin>'.(date('Y-m-d', strtotime($show['begin'].' +1 day'))).'</begin>
					<ddd>'.$show['3d'].'</ddd>
					<end>'.(date('Y-m-d', strtotime($show['end'].' +1 day'))).'</end>
					<times>';
						foreach ($show['times'] as $time)
						{
							if ($time['time'] < '05:00:00')
							{
								$xml .= '
									<time time="'.$time['time'].'">
										<prices>'.$time['prices'].'</prices>
									</time>';
							}
						}
					$xml .= '
					</times>
				</show>';
			}

		}
	$xml .= '
	</shows>';






	$xml .= '
	</billboard>';

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/odessa.xml";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $xml);
	fclose($fh);
	exit;

}
?>