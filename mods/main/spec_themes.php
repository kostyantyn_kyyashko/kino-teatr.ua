<?
function main_spec_themes()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::spec_themes');
	sys::useLib('main::users');
	sys::useLib('sys::pages');

	if ($_GET['page']!='')
	{
		define('_NOINDEX',1,true);
	}

		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		define('_CANONICAL',_ROOT_URL.'spec_themes.phtml',true);
		} else {
		define('_CANONICAL',_ROOT_URL.$lang.'/spec_themes.phtml',true);
		}

	$cache_name='main_spec_themes_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::news_page_cache_period']*60)) 
		return unserialize($cache);

	if ($_GET['page'])
	{
		$meta['page'] = sys::translate('main::page').' '.$_GET['page'];
	} else {
		$meta['page'] = '';
	}

	$result['meta']=sys::parseModTpl('main::spec_themes','page',$meta);

	$q="
		SELECT
			art.id,
			art_lng.title
		FROM `#__main_spec_themes` AS `art`				

		LEFT JOIN
			`#__main_spec_themes_lng` AS `art_lng`   
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY art.id DESC	
	";
	
	$result['pages']=sys_pages::pocess($q,$_cfg['main::spec_themes_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_spec_themes::getArticleUrl($obj['id']);
		$result['objects'][]=$obj;
	}

	if(!$result['objects'])		return 404;

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>