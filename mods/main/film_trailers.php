<?
function main_film_trailers()
{
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');
	sys::filterGet('trailer_id','int');
	sys::filterGet('page','int');

	if(!$_GET['film_id']) return 404;

		$q="
			SELECT
				trl.film_id,
				trl.id AS `trailer_id`,
				trl.image,
				trl.duration,
				trl.file,
				trl.url,
				trl.user_id,
				trl.shows,
				trl.width,
				trl.height,
				trl.order_number,
				trl.public,
				flm_lng.title AS `film`,
				flm_pst.image as `poster`
			FROM `#__main_films_trailers` AS `trl`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=trl.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			LEFT JOIN `#__main_films_photos` AS `flm_pst`
			ON flm_pst.film_id=trl.film_id
		";

	if($_GET['trailer_id'])
	{
		$q.="
			WHERE trl.id=".intval($_GET['trailer_id'])."
		";
	}
	elseif($_GET['film_id'])
	{
		if($_GET['page'])
			$limit=intval($_cfg['main::images_on_page']*(intval($_GET['page'])-1));
		else
			$limit=0;

		$q.="
			WHERE trl.film_id='".intval($_GET['film_id'])."'
			AND trl.public=1
			ORDER BY trl.order_number DESC
			LIMIT ".$limit.",1
		";
	}

	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);

	//if(!$result)return 404;

	if(!$result['public'] && !sys::checkAccess('main::films_trailers_confirm'))
		return 404;

	define('_CANONIC', main_films::getFilmTrailersUrl(intval($result['film_id'])));

	main::countShow($result['film_id'],'film');

	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::films_trailers_delete') || $result['user_id']==$_user['id'])
		{
			main_films::deleteTrailer($result['trailer_id']);
			sys::redirect(main_films::getFilmtrailersUrl($result['film_id']),false);
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::films_trailers_confirm'))
	{
		main_films::confirmtrailer($result['trailer_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}

	if($_cookie)
	{
		$_db->query("
			UPDATE `#__main_films_trailers`
			SET `shows`=(`shows`+1)
			WHERE `id`=".doubleval($_GET['trailer_id'])."
		");
	}
	$result['serial']=$_db->getValue('main_films','serial',intval($_GET['film_id']),false);
	$meta['film']=$result['film'];
	$meta['trailerid']=$_GET['trailer_id'];
	$meta['film']=$result['film'];
//	$result['title'] = sys::translate('main::trailers_film').' '.$result['film'];
	$result['title'] = (($result['serial'])?sys::translate('main::trailers_serial'):sys::translate('main::trailers_film')).' '.$result['film'];

	$meta['persons']=false;
	$result['persons']=main_films::gettrailerPersons($_GET['trailer_id']);

	$result['image']=$_cfg['main::films_url'].'x4_'.$result['image'];
	if($result['file'])
		$result['file']=$_cfg['main::films_url'].$result['file'];
	else
		$result['file']=$result['url'];

	$result['trailerid']=$_GET['trailer_id'];

	preg_match('#http://kinoteatr.cdnvideo.ru/public/main/films/(.*?).flv#is', $result['file'] , $img);

	if (!$img[1])
	{
		$img[1] = 'trailer_'.$_GET['trailer_id'];
	}

	$result['post']=$img[1].'.jpg';
	$result['typevideo'] = substr($result['file'], -3);
	$result['next_url']=false;
	$result['prev_url']=false;
	$result['delete_trailer']=false;
	$result['edit_url']=false;
	$result['add_url']=false;
	$result['url']=main_films::getFilmTrailerUrl($_GET['trailer_id']);
	$result['alt']=false;
	if(!$result['width'])
		$result['width']=495;
	if(!$result['height'])
		$result['height']=372;

	if($result['width']>495)
	{
		$result['width']=495;
		$result['height']=372;
	}

		if($result['duration']>60)
		{
			$result['duration']=(floor($result['duration']/60)).' '.sys::translate('main::min').' '.($result['duration']%60).' '.sys::translate('main::sec');//.sys::translate('main::min');
		}
		elseif($result['duration'])
			$result['duration'].=' '.sys::translate('main::sec');
		else
			$result['duration']=false;



	foreach ($result['persons'] as $person)
	{
		$result['alt'].=$person['fio'].', ';
	}

	if($result['alt'])
	{
		$meta['persons']=sys::cutStrRight($result['alt'],2);
		$result['alt']=sys::cutStrRight($result['alt'],2);
		$result['alt']=$result['alt'].' '.sys::translate('main::in_film').' &quot;'.htmlspecialchars($result['film']).'&quot';
	}

	$result['meta']=sys::parseModTpl('main::film_trailers'.($_GET['trailer_id']?'_selected':''),'page',$meta);

	if($next_id=main_films::getNexttrailerId($result['film_id'],$result['order_number']))
		$result['next_url']=main_films::gettrailersUrl($next_id);
	if($prev_id=main_films::getPrevtrailerId($result['film_id'],$result['order_number']))
		$result['prev_url']=main_films::gettrailersUrl($prev_id);

	if(sys::checkAccess('main::films_trailers_edit') || $result['user_id']==$_user['id'])
		$result['edit_url']=main_films::gettrailerEditUrl($result['trailer_id']);

	if(sys::checkAccess('main::films_trailers_add'))
		$result['add_url']=main_films::gettrailerAddUrl($result['film_id']);

	if(sys::checkAccess('main::films_trailers_delete') || $result['user_id']==$_user['id'])
		$result['delete_trailer']=true;
	//=============================================================================

	$url = main_films::getFilmTrailerUrl(intval($_GET['film_id']));
	
	//Получить инфу об остальных трейлрах-----------------------------------------------
	$q="
		SELECT trl.id,trl.image,lng.title,trl.language,trl.duration,trl.file,trl.tab_num
		FROM `#__main_films_trailers` AS `trl`
		LEFT JOIN `#__main_films_trailers_lng` AS `lng`
		ON lng.record_id=trl.id AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE trl.film_id=".$result['film_id']."
		AND trl.public=1
		ORDER BY trl.order_number DESC
	";
	$r=$_db->query($q);
	$result['previews']=array();
	while ($obj=$_db->fetchAssoc($r))
	{

// comment by Mike
/*					
		if($obj['image'])
			$image=$_cfg['main::films_url'].$obj['image'];
		else
         	$image = 'blank_news_img.jpg';

		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=8';
*/
		// inserted by Mike begin
  	    $image="x3_".$obj['image'];	
		if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$obj['image'] = $image;
		// inserted by Mike end



		if($obj['file'])
		{
			$obj['size']=sys::convertFileSize(filesize($_cfg['main::films_dir'].$obj['file']));
			$obj['downloadUrl']=$_cfg['main::films_url'].$obj['file'];
		}
		else
		{
			$obj['size']=false;
			$obj['downloadUrl']=false;
		}

		//$obj['url']=main_films::gettrailersUrl(intval($_GET['film_id']),false,"film_trailers")."?trailer_id=".$obj['id'];
		$obj['url']=$url."?trailer_id=".$obj['id'];
		if($obj['duration']>60)
		{
			$obj['duration']=(floor($obj['duration']/60)).'\''.($obj['duration']%60).'\'\' ';//.sys::translate('main::min');
		}
		elseif($obj['duration'])
			$obj['duration'].=' '.sys::translate('main::sec');
		else
			$obj['duration']=false;
		$result['previews'][]=$obj;
	}
	//============================================================

	return $result;
}
?>