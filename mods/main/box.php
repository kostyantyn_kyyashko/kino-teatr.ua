<?
function main_box()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::box');
	sys::useLib('main::users');
	sys::useLib('sys::pages');

	sys::filterGet('month');
	sys::filterGet('year');
	sys::filterGet('box_id');


		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		define('_CANONICAL',_ROOT_URL.'box.phtml',true);
		} else {
		define('_CANONICAL',_ROOT_URL.$lang.'/box.phtml',true);
		}


	$cache_name='main_box_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::box_page_cache_period']*60))
		return unserialize($cache);

	/*if($_GET['year']<2000 || $_GET['year']>intval(date('Y')) || $_GET['month']<1 || $_GET['month']>12)
		return 404;*/


	if($_GET['box_id'])
		$result['section']=$meta['section']=$_db->getValue('main_box','title',intval($_GET['box_id']),true);
	else
		$result['section']=$meta['section']=sys::translate('main::all_sections');


	if(!$result['section'])
		return 404;

	if(isset($_POST['_task']) && $_POST['_task'])
	{
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete' && sys::checkAccess('main::box_delete'))
			main_box::deleteArticle($task[1]);
	}

	$result['date']=$meta['date']=mb_strtolower(sys::translate('main::month_'.intval($_GET['month']))).' '.$_GET['year'];
	$meta['type']=sys::translate('main::ukraine_box_now');
	$result['meta']=sys::parseModTpl('main::box','page',$meta);


	$result['title']=sys::translate('main::box_cinema');


	$month_begin=date('Y-m-d H:i:s',main_box::getMonthBegin($_GET['month'],$_GET['year']));
	$month_end=date('Y-m-d H:i:s',main_box::getMonthEnd($_GET['month'],$_GET['year']));
	$q="
		SELECT
			art.id,
			art.date,
			art.real_id,
			art.date_w_s,
			art.date_w_f,
			art.user_id,
			art.small_image AS `image`,
			art_lng.title,
			art_lng.intro
		FROM `#__main_box_articles` AS `art`

		LEFT JOIN
			`#__main_box_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE
			/*art.date BETWEEN '".$month_begin."' AND '".$month_end."'
			AND */(art.city_id=".intval($_cfg['main::city_id'])." OR art.city_id=0)
			AND art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
	";
	if($_GET['box_id'])
	{
		$q.="
			AND art.box_id=".intval($_GET['box_id'])."
		";
	}
	$q.="
		ORDER BY art.date DESC
	";

	$result['pages']=sys_pages::pocess($q,$_cfg['main::news_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{


		$q2="
			SELECT
				SUM(money) as money
			FROM `#__main_box_positions`
			WHERE
				box_id='".$obj['id']."'
		";

		$r2=$_db->query($q2);
		while($obj2=$_db->fetchAssoc($r2))
		{
			$obj['money'] = number_format($obj2['money'],0, ",", " ");
		}

		$obj['date']=sys::db2Date($obj['date'],true,$_cfg['sys::date_format']);
		if($obj['image'])
			$obj['image']=$_cfg['main::box_url'].$obj['image'];
		else
			$obj['image']=main::getNoImage('x1');
		$obj['title']=htmlspecialchars($obj['title']).' за '.date('d', strtotime($obj['date_w_s'].' +1 days')).' &mdash; '.sys::russianDate($obj['date_w_f'].' +1 days');
		$obj['url']=main_box::getArticleUrl($obj['id']);

		if(sys::checkAccess('main::box_edit') || $_user['id']==$obj['user_id'])
			$obj['edit_url']=main_box::getArticleEditUrl($obj['id']);
		else
			$obj['edit_url']=false;

		if(sys::checkAccess('main::box_delete') || $_user['id']==$obj['user_id'])
			$obj['delete']=true;
		else
			$obj['delete']=false;
		$result['objects'][]=$obj;
	}




	if(!$result['objects']/* && !main_box::boxReirect($_GET['year'],$_GET['month'],$_GET['box_id'])*/)
		return 404;


	$result['add_url']=false;


		$result['ukraine']=sys::rewriteUrl('?mod=main&act=box');
		$result['world']=sys::rewriteUrl('?mod=main&act=boxw');


	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>