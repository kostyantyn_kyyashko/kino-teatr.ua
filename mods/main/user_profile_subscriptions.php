<?
function main_user_profile_subscriptions()
{
	define('_NOINDEX','1');
	sys::useLib('main::users');
	sys::useLib('sys::form');
	sys::useLib('sys::pages');
	sys::useLib('main::films');
	sys::useLib('main::discuss');
	sys::useLib('main::reviews');
	sys::useLib('main::cinemas');
	sys::useLib('main::persons');
	sys::useLib('main::contest');
	sys::useLib('main::interview');
	sys::useLib('main::articles');
	sys::useLib('main::serials');
	sys::useLib('main::gossip');
	sys::useLib('main::news');
	sys::useJs('sys::gui');
	sys::filterGet('order_by','text','title.asc');
	
	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;
	if($_user['id']==2)
		return 401;

//	$result['meta']=sys::parseModTpl('main::user_profile_films','page');
//	$result['meta']=sys::parseModTpl('main::user_profile_discuss','page');
	$result['meta']=sys::parseModTpl('main::user_profile_subscriptions','page');

	if(isset($_POST['_task']) && $_POST['_task'])
	{
		$task=explode('.',$_POST['_task']);
		if($task[0]=='unTrack')
			main_films::untrackingFilm($task[1]);
		elseif($task[0]=='unSubscribe')
			main_discuss::unsubscribeDiscuss($_user['id'],$task[1],$task[2]);
		elseif($task[0]=='unSubscribeCinema')
			main_cinemas::unsubscribeCinema($_user['id'],$task[1]);
		sys::redirect(main_users::getProfileSubscriptionsUrl(),false);
	}


	//================== films ===================================

	$q="
		SELECT
			usr_flm.film_id,
			flm_lng.title
		FROM
			`#__main_users_films_subscribe` AS `usr_flm`

		LEFT JOIN
			`#__main_films_lng` AS `flm_lng`
		ON
			flm_lng.record_id=usr_flm.film_id
		AND
			flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE
			usr_flm.user_id=".intval($_user['id'])."

		".$order_by."
	";

	$r=$_db->query($q);
	$films['objects']=array();

	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['url']=main_films::getFilmUrl($obj['film_id']);
		$obj['title']=htmlspecialchars($obj['title']);
		$films['objects'][]=$obj;
	}

	$result["films"] = $films;
	
////////////////// discuss ///////////////	
	
	$q="
		SELECT
			`object_id`,
			`object_type`
		FROM
			`#__main_users_discuss_subscribe`
		WHERE
			`user_id`=".intval($_user['id'])."

	";
	$r=$_db->query($q);
	$discuss['objects']=array();

	while ($obj=$_db->fetchAssoc($r))
	{
		switch ($obj['object_type'])
		{
			case 'film':
				$obj['url']=main_films::getFilmDiscussUrl($obj['object_id']);
				$obj['title']=$_db->getValue('main_films','title',$obj['object_id'],true);
			break;
            
			case 'trailer':
				$obj['url']=main_films::getTrailersUrl($obj['object_id']);
				$obj['title']=$_db->getValue('main_films_trailers','title',$obj['object_id'],true);
				$obj['film']=main_films::getFilmTitleByTrailerID($obj['object_id'], $_cfg['sys::lang_id']);
			break;

			case 'news_article':
				$obj['url']=main_news::getArticleDiscussUrl($obj['object_id']);
				$obj['title']=$_db->getValue('main_news_articles','title',$obj['object_id'],true);
			break;

			case 'articles_article':
				$obj['url']=main_articles::getArticleDiscussUrl($obj['object_id']);
				$obj['title']=$_db->getValue('main_articles_articles','title',$obj['object_id'],true);
			break;

			case 'interview_article':
				$obj['url']=main_interview::getArticleDiscussUrl($obj['object_id']);
				$obj['title']=$_db->getValue('main_interview_articles','title',$obj['object_id'],true);
			break;

			case 'serials_article':
				$obj['url']=main_serials::getArticleDiscussUrl($obj['object_id']);
				$obj['title']=$_db->getValue('main_serials_articles','title',$obj['object_id'],true);
			break;

			case 'gossip_article':
				$obj['url']=main_gossip::getArticleDiscussUrl($obj['object_id']);
				$obj['title']=$_db->getValue('main_gossip_articles','title',$obj['object_id'],true);
			break;

			case 'cinema':
				$obj['url']=main_cinemas::getCinemaDiscussUrl($obj['object_id']);
				$obj['title']=$_db->getValue('main_cinemas','title',$obj['object_id'],true);
			break;

			case 'contest_article':
				$obj['url']=main_contest::getContestArticleDiscussUrl($obj['object_id']);
				$obj['title']=$_db->getValue('main_contest_articles','title',$obj['object_id'],true);
			break;

			case 'review':
				$obj['url']=main_reviews::getReviewDiscussUrl($obj['object_id']);
				$obj['title']=$_db->getValue('main_reviews','title',$obj['object_id']);
				$obj['film']=main_films::getFilmTitleByReviewID($obj['object_id']);
			break;

			case 'person':
				$obj['url']=main_persons::getPersonDiscussUrl($obj['object_id']);
				$r2=$_db->query("
					SELECT CONCAT(`firstname`,' ',`lastname`)
					FROM `#__main_persons_lng`
					WHERE `record_id`=".$obj['object_id']."
					AND `lang_id`=".intval($_cfg['sys::lang_id'])."
				");
				list($obj['title'])=$_db->fetchArray($r2);
			break;
		}
		$discuss['objects'][]=$obj;
	}
	
	$result["discuss"] = $discuss;
	
	//================== cinemas ===================================

	$q="
		SELECT
			cnm.id as cinema_id,
			cnm.name as title
		FROM
			`#__main_users_cinema_subscribe` AS `subscr`

		LEFT JOIN
			`#__main_cinemas` AS `cnm`
		ON
			cnm.id=subscr.cinema_id

		WHERE
			subscr.user_id=".intval($_user['id'])."

		ORDER BY 
			cnm.name
	";
	
	$r=$_db->query($q);
	$cinemas['objects']=array();

	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['url']=main_cinemas::getCinemaUrl($obj['cinema_id']);
		$obj['title']=htmlspecialchars($obj['title']);
		$cinemas['objects'][]=$obj;
	}

	$result["cinemas"] = $cinemas;
	
//if(sys::isDebugIP()) $_db->printR($result["cinemas"]);
	
	
	return $result;
}
?>