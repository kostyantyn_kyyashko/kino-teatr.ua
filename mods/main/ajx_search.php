<?
function main_ajx_search()
{

function dmword($string, $is_cyrillic = true)
	{
		static $codes = array(
			'A' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1)),
				'U' => array(array(0, 7, -1))),
			'B' => array(array(7, 7, 7)),
			'C' => array(array(5, 5, 5), array(4, 4, 4),
				'Z' => array(array(4, 4, 4),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'Z' => array(array(4, 4, 4))),
				'K' => array(array(5, 5, 5), array(45, 45, 45)),
				'H' => array(array(5, 5, 5), array(4, 4, 4),
					'S' => array(array(5, 54, 54)))),
			'D' => array(array(3, 3, 3),
				'T' => array(array(3, 3, 3)),
				'Z' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'Z' => array(array(4, 4, 4))),
				'R' => array(
					'S' => array(array(4, 4, 4)),
					'Z' => array(array(4, 4, 4)))),
			'E' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1)),
				'U' => array(array(1, 1, -1))),
			'F' => array(array(7, 7, 7),
				'B' => array(array(7, 7, 7))),
			'G' => array(array(5, 5, 5)),
			'H' => array(array(5, 5, -1)),
			'I' => array(array(0, -1, -1),
				'A' => array(array(1, -1, -1)),
				'E' => array(array(1, -1, -1)),
				'O' => array(array(1, -1, -1)),
				'U' => array(array(1, -1, -1))),
			'J' => array(array(4, 4, 4)),
			'K' => array(array(5, 5, 5),
				'H' => array(array(5, 5, 5)),
				'S' => array(array(5, 54, 54))),
			'L' => array(array(8, 8, 8)),
			'M' => array(array(6, 6, 6),
				'N' => array(array(66, 66, 66))),
			'N' => array(array(6, 6, 6),
				'M' => array(array(66, 66, 66))),
			'O' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1))),
			'P' => array(array(7, 7, 7),
				'F' => array(array(7, 7, 7)),
				'H' => array(array(7, 7, 7))),
			'Q' => array(array(5, 5, 5)),
			'R' => array(array(9, 9, 9),
				'Z' => array(array(94, 94, 94), array(94, 94, 94)), // special case
				'S' => array(array(94, 94, 94), array(94, 94, 94))), // special case
			'S' => array(array(4, 4, 4),
				'Z' => array(array(4, 4, 4),
					'T' => array(array(2, 43, 43)),
					'C' => array(
						'Z' => array(array(2, 4, 4)),
						'S' => array(array(2, 4, 4))),
					'D' => array(array(2, 43, 43))),
				'D' => array(array(2, 43, 43)),
				'T' => array(array(2, 43, 43),
					'R' => array(
						'Z' => array(array(2, 4, 4)),
						'S' => array(array(2, 4, 4))),
					'C' => array(
						'H' => array(array(2, 4, 4))),
					'S' => array(
						'H' => array(array(2, 4, 4)),
						'C' => array(
							'H' => array(array(2, 4, 4))))),
				'C' => array(array(2, 4, 4),
					'H' => array(array(4, 4, 4),
						'T' => array(array(2, 43, 43),
							'S' => array(
								'C' => array(
									'H' => array(array(2, 4, 4))),
								'H' => array(array(2, 4, 4))),
							'C' => array(
								'H' => array(array(2, 4, 4)))),
						'D' => array(array(2, 43, 43)))),
				'H' => array(array(4, 4, 4),
					'T' => array(array(2, 43, 43),
						'C' => array(
							'H' => array(array(2, 4, 4))),
						'S' => array(
							'H' => array(array(2, 4, 4)))),
					'C' => array(
						'H' => array(array(2, 4, 4))),
					'D' => array(array(2, 43, 43)))),
			'T' => array(array(3, 3, 3),
				'C' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4))),
				'Z' => array(array(4, 4, 4),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'Z' => array(array(4, 4, 4)),
					'H' => array(array(4, 4, 4)),
					'C' => array(
						'H' => array(array(4, 4, 4)))),
				'T' => array(
					'S' => array(array(4, 4, 4),
						'Z' => array(array(4, 4, 4)),
						'C' => array(
							'H' => array(array(4, 4, 4)))),
					'C' => array(
						'H' => array(array(4, 4, 4))),
					'Z' => array(array(4, 4, 4))),
				'H' => array(array(3, 3, 3)),
				'R' => array(
					'Z' => array(array(4, 4, 4)),
					'S' => array(array(4, 4, 4)))),
			'U' => array(array(0, -1, -1),
				'E' => array(array(0, -1, -1)),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1))),
			'V' => array(array(7, 7, 7)),
			'W' => array(array(7, 7, 7)),
			'X' => array(array(5, 54, 54)),
			'Y' => array(array(1, -1, -1)),
			'Z' => array(array(4, 4, 4),
				'D' => array(array(2, 43, 43),
					'Z' => array(array(2, 4, 4),
						'H' => array(array(2, 4, 4)))),
				'H' => array(array(4, 4, 4),
					'D' => array(array(2, 43, 43),
						'Z' => array(
							'H' => array(array(2, 4, 4))))),
				'S' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'C' => array(
						'H' => array(array(4, 4, 4))))));
		$length = strlen($string);
		$output = '';
		$i = 0;
		$previous = -1;
		while ($i < $length)
		{
			$current = $last = &$codes[$string[$i]];
			for ($j = $k = 1; $k < 7; $k++)
			{
				if (!isset($string[$i + $k]) || !isset($current[$string[$i + $k]]))
					break;
				$current = &$current[$string[$i + $k]];
				if (isset($current[0]))
				{
					$last = &$current;
					$j = $k + 1;
				}
			}
			if ($i == 0)
				$code = $last[0][0];
			elseif (!isset($string[$i + $j]) || ($codes[$string[$i + $j]][0][0] != 0))
				$code = $is_cyrillic ? (isset($last[1]) ? $last[1][2] : $last[0][2]) : $last[0][2];
			else
				$code = $is_cyrillic ? (isset($last[1]) ? $last[1][1] : $last[0][1]) : $last[0][1];
			if (($code != -1) && ($code != $previous))
				$output .= $code;
			$previous = $code;
			$i += $j;
		}
		return str_pad(substr($output, 0, 6), 6, '0');
	}

//------------------------------------------------------------------------------

	function dmstring($string)
	{
		$is_cyrillic = false;
		if (preg_match('/[А-Яа-я]/iu', $string) === 1)
		{
			$string = translit($string);
			$is_cyrillic = true;
		}

		$firstword = '';
		$lastword = '';


		$words = explode(' ', $string);


		if (preg_match('/[A-Za-z]/iu', $words[0]) !== 1)
		{
			$firstword = $words[0];
		}

		if (preg_match('/[A-Za-z]/iu', $words[count($words)-1]) !== 1)
		{
			$lastword = $words[count($words)-1];
		}

		$string = preg_replace(array('/[^\w\s]|\d/iu', '/\b[^\s]{1,1}\b/iu', '/\s{1,}/iu', '/^\s+|\s+$/iu'), array('', '', ' '), strtoupper($string));


		if ($string[0])
		{
			$matches = explode(' ', $string);
		}  else {
			$matches = array();
		}


		foreach($matches as $key => $match)
			$matches[$key] = dmword($match, $is_cyrillic);


		if ($firstword)
		{
			array_unshift($matches, $firstword);
		}

		if ($lastword)
		{
			$matches[]=$lastword;
		}

		return $matches;
	}

//------------------------------------------------------------------------------

	function translit($string)
	{
		static $ru = array(
			'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
			'Ё', 'ё', 'Ж', 'ж', 'З', 'з', 'И', 'и', 'Й', 'й', 'К', 'к',
			'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',
			'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц',
			'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ', 'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь',
			'Э', 'э', 'Ю', 'ю', 'Я', 'я'
		);
		static $en = array(
			'A', 'a', 'B', 'b', 'V', 'v', 'G', 'g', 'D', 'd', 'E', 'e',
			'E', 'e', 'Zh', 'zh', 'Z', 'z', 'I', 'i', 'J', 'j', 'K', 'k',
			'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'R', 'r',
			'S', 's', 'T', 't', 'U', 'u', 'F', 'f', 'H', 'h', 'C', 'c',
			'Ch', 'ch', 'Sh', 'sh', 'Sch', 'sch', '\'', '\'', 'Y', 'y', '\'', '\'',
			'E', 'e', 'Ju', 'ju', 'Ja', 'ja'
		);
		$string = str_replace($ru, $en, $string);
		return $string;
	}

	function lttoru($string)
	{
		static $ru = array(
			'Q', 'q', 'W', 'w', 'E', 'e', 'R', 'r', 'T', 't', 'Y', 'y',
			'U', 'u', 'I', 'i', 'O', 'o', 'P', 'p', 'A', 'a', 'S', 's',
			'D', 'd', 'F', 'f', 'G', 'g', 'H', 'h', 'J', 'j', 'K', 'k',
			'L', 'l', 'Z', 'z', 'X', 'x', 'C', 'c', 'V', 'v', 'B', 'b',
			'N', 'n', 'M', 'm', '<', ',', '>', '.', '{', '[', '}', ']',
			':', ';', '"', "'"
		);
		static $en = array(
			'Й', 'й', 'Ц', 'ц', 'У', 'у', 'К', 'к', 'Е', 'е', 'Н', 'н',
			'Г', 'г', 'Ш', 'ш', 'Щ', 'щ', 'З', 'з', 'Ф', 'ф', 'Ы', 'ы',
			'В', 'в', 'А', 'а', 'П', 'п', 'Р', 'р', 'О', 'о', 'Л', 'л',
			'Д', 'д', 'Я', 'я', 'Ч', 'ч', 'С', 'с', 'М', 'м', 'И', 'и',
			'Т', 'т', 'Ь', 'ь', 'Б', 'б', 'Ю', 'ю', 'Х', 'х', 'Ъ', 'ъ',
			'Ж', 'ж', 'Э', 'э'
		);
		$string = str_replace($ru, $en, $string);
		return $string;
	}

	function rutolt($string)
	{
		static $en = array(
			'Q', 'q', 'W', 'w', 'E', 'e', 'R', 'r', 'T', 't', 'Y', 'y',
			'U', 'u', 'I', 'i', 'O', 'o', 'P', 'p', 'A', 'a', 'S', 's',
			'D', 'd', 'F', 'f', 'G', 'g', 'H', 'h', 'J', 'j', 'K', 'k',
			'L', 'l', 'Z', 'z', 'X', 'x', 'C', 'c', 'V', 'v', 'B', 'b',
			'N', 'n', 'M', 'm', '<', ',', '>', '.', '{', '[', '}', ']',
			':', ';', '"', "'"
		);
		static $ru = array(
			'Й', 'й', 'Ц', 'ц', 'У', 'у', 'К', 'к', 'Е', 'е', 'Н', 'н',
			'Г', 'г', 'Ш', 'ш', 'Щ', 'щ', 'З', 'з', 'Ф', 'ф', 'Ы', 'ы',
			'В', 'в', 'А', 'а', 'П', 'п', 'Р', 'р', 'О', 'о', 'Л', 'л',
			'Д', 'д', 'Я', 'я', 'Ч', 'ч', 'С', 'с', 'М', 'м', 'И', 'и',
			'Т', 'т', 'Ь', 'ь', 'Б', 'б', 'Ю', 'ю', 'Х', 'х', 'Ъ', 'ъ',
			'Ж', 'ж', 'Э', 'э'
		);
		$string = str_replace($ru, $en, $string);
		return $string;
	}

//------------------------------------------------------------------------------


        $cyrillic = 0;

		if (preg_match('/[А-Яа-я]/iu', $_GET['keywords']) === 1)
		{
			$cyrillic = 1;
		}


	$coded = dmstring($_GET['keywords']);

	$chars = str_split($coded[count($coded)-1]);

	$coded[count($coded)-1] = '';

	$coded[count($coded)-1] .= $chars[0];

	for ($i=1;$i<=(count($chars)-1);$i++)
	{
		if ($chars[$i]!='0')
		{
			$coded[count($coded)-1] .= $chars[$i];
		}
	}

	$text = implode(',', $coded);
	$text = str_replace("'","",$text);

	$lnk = 1;

	sys::setTpl();
	sys::filterGet('keywords');
	sys::filterGet('field');
	sys::filterGet('mode');
	sys::useLib('main::films');
	sys::useLib('main::persons');
	sys::useLib('main::cinemas');
	global $_db, $_cfg, $_err, $_user, $_cookie;


	if(!$_GET['keywords'])	return false;
	$_GET['keywords'] = strip_tags(str_replace("'","",$_GET['keywords']));

	$q="
		SELECT
			flm.id,
			flm.name,
			flm.title_orig,
			flm.year,
			flm.coded as `fcode`,
			lng.coded as `lcode`,
			lng.title,
			lng.lang_id
		FROM
			`grifix_main_films` AS `flm`
		LEFT JOIN
			`grifix_main_films_lng` AS `lng`
		ON
			lng.record_id=flm.id
	";

	if($_GET['keywords']!='*')
	{
			$keyw = ' '.$_GET['keywords'];
			$q.="WHERE ";
			$q .= "flm.title_orig LIKE '".$_GET['keywords']."%'		OR flm.title_orig LIKE '%".$keyw."%'
			OR lng.title LIKE '".$_GET['keywords']."%'				OR lng.title LIKE '%".$keyw."%'
			OR flm.title_orig LIKE '".lttoru($_GET['keywords'])."%'	OR flm.title_orig LIKE '%".lttoru($keyw)."%'
			OR lng.title LIKE '".lttoru($_GET['keywords'])."%'		OR lng.title LIKE '%".lttoru($keyw)."%'
			";
	}

	$q.=" ORDER BY flm.year DESC, flm.total_shows DESC LIMIT 6";

	$r=$_db->query($q);
	$list=array();
	$i=0;

	$films='';
	$lines=array();
	$langs=array();

	while($obj=$_db->fetchAssoc($r))
	{
		
		$obj['name'].=' ('.$obj['title'].')';
		$obj['name']=addslashes(($obj['name']));

		$myLangPresent = (isset($langs[$obj["id"]]) && ($langs[$obj["id"]]==$_cfg['sys::lang_id']));		
		if($myLangPresent)  continue;

		$langs[$obj["id"]] = $obj['lang_id'];
		$langLink = ($obj['lang_id']==1)?(main_films::getFilmUrl($obj['id'])):(main_films::getFilmUkUrl($obj['id']));
		$lines[$obj["id"]] = '<li id="lnk'.($lnk++).'" objid="'.main_films::getFilmUrl($obj['id']).'" type="film">
							  <a href="'.$langLink.'" style="color: #970301 !important">'.$obj['title'].'</a>
							  ';

		if ($obj['title_orig'] || $obj['year'])
		{
			$lines[$obj["id"]] .= '<br><font style="font-size: 10px; color: #AAAAAA;">';			
			if ($obj['title_orig'])	$lines[$obj["id"]] .= $obj['title_orig'].', ';
			$lines[$obj["id"]] .= $obj['year'].'</font>';
		}
		$lines[$obj["id"]] .= '
			<div style="clear: both; height: 1px;"><img src="'._SKIN_URL.'images/blank.gif"></div>
		</li>';

		$list[]=$obj;
		$wherenot .= ' AND flm.id!="'.$obj['id'].'"';
	}
	$films = implode(" ", array_slice($lines, 0, 3));
	

	$near = 0;
		
	$i=sizeof($list);
	if ($i<3)
	{
		$kt = 3-$i;
		$q="
			SELECT
				flm.id,
				flm.name,
				flm.title_orig,
				flm.year,
				flm.coded as `fcode`,
				lng.coded as `lcode`,
				lng.title
			FROM
				`grifix_main_films` AS `flm`
			LEFT JOIN
				`grifix_main_films_lng` AS `lng`
			ON
				lng.record_id=flm.id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."
		";

		if($_GET['keywords']!='*')
		{
			$q.="WHERE ";

			if ($cyrillic)
			{
				$q .= "(lng.coded LIKE '".$text."%'
				OR lng.coded LIKE '%,".$text."%'
				OR lng.coded LIKE '".lttoru($text)."%'
				OR lng.coded LIKE '%,".lttoru($text)."%'
				OR lng.coded LIKE '".rutolt($text)."%'
				OR lng.coded LIKE '%,".rutolt($text)."%'
				)";
			} else {
				$q .= "(flm.coded LIKE '".$text."%'
				OR flm.coded LIKE '%,".$text."%'
				OR flm.coded LIKE '".lttoru($text)."%'
				OR flm.coded LIKE '%,".lttoru($text)."%'
				OR flm.coded LIKE '".rutolt($text)."%'
				OR flm.coded LIKE '%,".rutolt($text)."%'
				)";
			}

			$q .= $wherenot;
        }
		$q.=" ORDER BY flm.year DESC, flm.total_shows DESC LIMIT 50";

//	if(sys::isDebugIP()) $_db->printR($q);	
		
		$fnear = '';
		$r=$_db->query($q);
		$list=array();

		if (intval($_cfg['sys::lang_id'])=='1')
		{
			$lang = 'ru';
		} else {
			$lang = 'uk';
		}

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['min'] = 1000;

			$keyw = strtoupper($_GET['keywords']);

			$origarray = explode(' ', strtoupper($obj['title_orig']));

			foreach ($origarray as $key=>$value)
			{
				if (levenshtein($keyw, substr($value, 0, strlen($keyw)))<$obj['min'])
					{
						$obj['min'] = 	levenshtein($keyw, substr($value, 0, strlen($keyw)));
					}
			}


			$justarray = explode(' ', strtoupper($obj['title']));

			foreach ($justarray as $key=>$value)
			{
				if (levenshtein($keyw, substr($value, 0, strlen($keyw)))<$obj['min'])
					{
						$obj['min'] = 	levenshtein($keyw, substr($value, 0, strlen($keyw)));
					}
			}

			$obj['name'].=' ('.$obj['title'].')';
			$obj['name']=addslashes(($obj['name']));

			if (is_array($list[$obj['min']]))
			{
				if (is_array($list[$obj['min'].'1']))
				{
					$list[$obj['min'].'2']=$obj;
				} else {
					$list[$obj['min'].'1']=$obj;
				}
				$near++;

			} else {
				$list[$obj['min']]=$obj;
				$near++;
			}



		}

		ksort($list);

		$list = array_values($list);

		$lis = array();

		$k=0;

		foreach ($list as $key=>$value)
		{


			$k++;

			if ($k<=$kt)
			{
				$lis[] = $value;
			}
		}

		foreach ($lis as $key=>$obj)
		{
			$fnear .= '<li id="lnk'.$lnk.'" objid="'.main_films::getFilmUrl($obj['id']).'" type="film">
			<a href="'.main_films::getFilmUrl($obj['id']).'" style="color: #970301 !important">'.$obj['title'].'</a>';

			$lnk++;

			if ($obj['title_orig'] || $obj['year'])
			{
				$fnear .= '<br><font style="font-size: 10px; color: #AAAAAA;">';

				if ($obj['title_orig'])
				{
					$fnear .= $obj['title_orig'].', ';
				}

				$fnear .= $obj['year'].'</font>';
			}

			$fnear .= '
				<div style="clear: both; height: 1px;"><img src="'._SKIN_URL.'images/blank.gif"></div>
			</li>';

         }
	}

// ------------------ персоны ----------------- ///



	$q="
		SELECT
			flm.id,
			flm.lastname_orig,
			flm.firstname_orig,
			flm.birthdate,
			lng.lastname,
			lng.firstname
		FROM
			`grifix_main_persons` AS `flm`
		LEFT JOIN
			`grifix_main_persons_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	if($_GET['keywords']!='*')
		$q.="WHERE
			`name` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'

			OR `lastname_orig` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			
			OR `firstname_orig` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			
			OR `lastname` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			
			OR `firstname` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			";

	$q.=" ORDER BY flm.total_shows DESC LIMIT 3";
	$r=$_db->query($q);
	$list=array();
	$people='';
	$ip=0;

	while($obj=$_db->fetchAssoc($r))
	{
		$list[]=$obj;

		$people .= '<li id="lnk'.$lnk.'" type="person" objid="'.main_persons::getPersonUrl($obj['id']).'">
		<a href="'.main_persons::getPersonUrl($obj['id']).'" style="color: #970301 !important">'.$obj['firstname'].' '.$obj['lastname'].'</a>';

        $lnk++;

		if ($obj['lastname_orig'] || $obj['birthdate'])
		{
			$people .= '<br><font style="font-size: 10px; color: #AAAAAA;">';


			if ($obj['lastname_orig'])
			{
    			$orig = $obj['firstname_orig'].' '.$obj['lastname_orig'];
				$people .= $orig.', ';
			}

			if ($obj['birthdate'])
			{
				$people .= date('Y', strtotime($obj['birthdate'])).'</font>';
			} else {
				$people .= '</font>';
			}
		}

		$people .= '
		<div style="clear: both; height: 1px;"><img src="'._SKIN_URL.'images/blank.gif"></div>
		</li>';
        $wherenot .= ' AND flm.id!="'.$obj['id'].'"';
		$i++;
		$ip++;
	}


	if ($ip<3)
	{
		$kt = 3-$ip;
		$q="
			SELECT
				flm.id,
				flm.lastname_orig,
				flm.firstname_orig,
				flm.birthdate,
				lng.lastname,
				lng.firstname,
				flm.coded as `fcode`,
				lng.coded as `lcode`
			FROM
				`grifix_main_persons` AS `flm`
			LEFT JOIN
				`grifix_main_persons_lng` AS `lng`
			ON
				lng.record_id=flm.id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."
		";

		if($_GET['keywords']!='*')
		{
			$q.="WHERE ";

			if ($cyrillic)
			{
				$q .= "(lng.coded LIKE '".$text."%'
				OR lng.coded LIKE '%,".$text."%')";
			} else {
				$q .= "(flm.coded LIKE '".$text."%'
				OR flm.coded LIKE '%,".$text."%')";
			}

			$q .= $wherenot;
        }
		$q.=" ORDER BY flm.total_shows DESC LIMIT 100";

		$pnear = '';
		$r=$_db->query($q);
		$list=array();

		if (intval($_cfg['sys::lang_id'])=='1')
		{
			$lang = 'ru';
		} else {
			$lang = 'uk';
		}

		while($obj=$_db->fetchAssoc($r))
		{


			$obj['min'] = 1000;

			$keyw = strtoupper($_GET['keywords']);

			$origarray = explode(', ', strtoupper($obj['name']));

			foreach ($origarray as $key=>$value)
			{
				if (levenshtein($keyw, substr($value, 0, strlen($keyw)))<$obj['min'])
					{
						$obj['min'] = 	levenshtein($keyw, substr($value, 0, strlen($keyw)));
					}
			}


			$justarray = array();
			$justarray[] = strtoupper($obj['lastname']);
			$justarray[] = strtoupper($obj['firstname']);

			foreach ($justarray as $key=>$value)
			{
				if (levenshtein($keyw, substr($value, 0, strlen($keyw)))<$obj['min'])
					{
						$obj['min'] = 	levenshtein($keyw, substr($value, 0, strlen($keyw)));
					}
			}

			$obj['name'].=' ('.$obj['title'].')';
			$obj['name']=addslashes(($obj['name']));

			if (is_array($list[$obj['min']]))
			{
				if (is_array($list[$obj['min'].'1']))
				{
					$list[$obj['min'].'2']=$obj;
				} else {
					$list[$obj['min'].'1']=$obj;
				}
				$near++;

			} else {
				$list[$obj['min']]=$obj;
				$near++;
			}



		}

		ksort($list);

		$list = array_values($list);

		$lis = array();

		$k=0;

		foreach ($list as $key=>$value)
		{


			$k++;

			if ($k<=$kt)
			{
				$lis[] = $value;
			}
		}

		foreach ($lis as $key=>$obj)
		{

/*

		$pnear .= '<li id="lnk'.$lnk.'" type="person" objid="'.main_persons::getPersonUrl($obj['id']).'">
		<a class="xImage" style="width: 30px; float: left; margin-right: 10px;" href="'.main_persons::getPersonUrl($obj['id']).'">
			<img width="30" src="'.$obj["poster"].'">
		</a>
		<a href="'.main_persons::getPersonUrl($obj['id']).'" style="color: #970301 !important">'.$obj['firstname'].' '.$obj['lastname'].'</a>';


*/


		$pnear .= '<li id="lnk'.$lnk.'" type="person" objid="'.main_persons::getPersonUrl($obj['id']).'">
		<a href="'.main_persons::getPersonUrl($obj['id']).'" style="color: #970301 !important">'.$obj['firstname'].' '.$obj['lastname'].'</a>';

        $lnk++;

		if ($obj['lastname_orig'] || $obj['birthdate'])
		{
			$pnear .= '<br><font style="font-size: 10px; color: #AAAAAA;">';


			if ($obj['lastname_orig'])
			{
    			$orig = $obj['firstname_orig'].' '.$obj['lastname_orig'];
				$pnear .= $orig.', ';
			}

			if ($obj['birthdate'])
			{
				$pnear .= date('Y', strtotime($obj['birthdate'])).'</font>';
			} else {
				$pnear .= '</font>';
			}
		}

		$pnear .= '
		<div style="clear: both; height: 1px;"><img src="'._SKIN_URL.'images/blank.gif"></div>
		</li>';

         }
	}



// ----------------------- кинотеатры ---------------------- ///


/*


	$wherenot = '';

	$q="
		SELECT
			flm.id,
			flm.name,
			lng.title,
			ct.title as `city`
		FROM
			`grifix_main_cinemas` AS `flm`
		LEFT JOIN
			`grifix_main_cinemas_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		LEFT JOIN
			`grifix_main_countries_cities_lng` AS `ct`
		ON
			ct.record_id=flm.city_id
		AND
			ct.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	if($_GET['keywords']!='*')
		$q.="WHERE
			lng.`title` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR lng.`title` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR lng.`title` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `name` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			";

	$q.=" ORDER BY flm.name LIMIT 3";
	$r=$_db->query($q);
	$list=array();

	$ic = 0;

	$cinemas='';

	while($obj=$_db->fetchAssoc($r))
	{
		$list[]=$obj;

		$cinemas .= '<li id="lnk'.$lnk.'" type="cinema" objid="'.$obj['id'].'"><a href="'.main_cinemas::getCinemaUrl($obj['id']).'" style="color: #970301 !important">'.$obj['title'].'</a>';

		$lnk++;


		if ($obj['city'])
		{
			$cinemas .= ' <font style="font-size: 10px; color: #AAAAAA;">('.$obj['city'];
			$cinemas .= ')</font>';
		}

		$cinemas .= '</li>';
        $wherenot .= ' AND flm.id!="'.$obj['id'].'"';
		$ic++;
		$i++;
	}



	if ($ic<3)
	{

		$kt = 3-$ic;



		$q="
			SELECT
				flm.id,
				flm.name,
				lng.title,
				ct.title as `city`,
				flm.coded as `fcode`,
				lng.coded as `lcode`
			FROM
				`grifix_main_cinemas` AS `flm`
			LEFT JOIN
				`grifix_main_cinemas_lng` AS `lng`
			ON
				lng.record_id=flm.id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."
			LEFT JOIN
				`grifix_main_countries_cities_lng` AS `ct`
			ON
				ct.record_id=flm.city_id
			AND
				ct.lang_id=".intval($_cfg['sys::lang_id'])."
		";

		if($_GET['keywords']!='*')
		{
			$q.="WHERE ";

			if ($cyrillic)
			{
				$q .= "(lng.coded LIKE '".$text."%'
				OR lng.coded LIKE '%,".$text."%')";
			} else {
				$q .= "(flm.coded LIKE '".$text."%'
				OR flm.coded LIKE '%,".$text."%')";
			}

			$q .= $wherenot;
        }

		$q.=" ORDER BY flm.name LIMIT 100";

//		print_R ($q);

	//	print_r ($q);


		$cnear = '';
		$r=$_db->query($q);
		$list=array();

		if (intval($_cfg['sys::lang_id'])=='1')
		{
			$lang = 'ru';
		} else {
			$lang = 'uk';
		}

		while($obj=$_db->fetchAssoc($r))
		{


			$obj['min'] = 1000;

			$keyw = strtoupper($_GET['keywords']);

			$origarray = explode(' ', strtoupper($obj['name']));

			foreach ($origarray as $key=>$value)
			{
				if (levenshtein($keyw, substr($value, 0, strlen($keyw)))<$obj['min'])
					{
						$obj['min'] = 	levenshtein($keyw, substr($value, 0, strlen($keyw)));
					}
			}


			$justarray = explode(' ', strtoupper($obj['title']));

			foreach ($justarray as $key=>$value)
			{
				if (levenshtein($keyw, substr($value, 0, strlen($keyw)))<$obj['min'])
					{
						$obj['min'] = 	levenshtein($keyw, substr($value, 0, strlen($keyw)));
					}
			}


			$obj['name'].=' ('.$obj['title'].')';
			$obj['name']=addslashes(($obj['name']));

			if (is_array($list[$obj['min']]))
			{
				if (is_array($list[$obj['min'].'1']))
				{
					$list[$obj['min'].'2']=$obj;
				} else {
					$list[$obj['min'].'1']=$obj;
				}
				$near++;

			} else {
				$list[$obj['min']]=$obj;
				$near++;
			}



		}

		ksort($list);

		$list = array_values($list);

		$lis = array();

		$k=0;

		foreach ($list as $key=>$value)
		{


			$k++;

			if ($k<=$kt)
			{
				$lis[] = $value;
			}
		}

		foreach ($lis as $key=>$obj)
		{



			$cnear .= '<li id="lnk'.$lnk.'" type="cinema" objid="'.$obj['id'].'"><a href="'.main_cinemas::getCinemaUrl($obj['id']).'" style="color: #970301 !important">'.$obj['title'].'</a>';

			$lnk++;


			if ($obj['city'])
			{
				$cnear .= ' <font style="font-size: 10px; color: #AAAAAA;">('.$obj['city'];
				$cnear .= ')</font>';
			}

			$cnear .= '</li>';
         }
	}

*/

	if ($i<=0 && $near<=0)
	{
	$res .= 'ничего не найдено';
	} else {
		if ($films)
		{
			$res .= '<ul>';
			$res .= '<br><strong>Фильмы</strong><br>';
			$res .= $films;
			$res .= '</ul>';
		}

		if ($fnear)
		{
			$res .= '<ul>';
			$res .= '<br><strong class="gray">Похожие варианты (Фильмы)</strong><br>';
			$res .= $fnear;
			$res .= '</ul>';
		}

		if ($people)
		{
			$res .= '<ul>';
			$res .= '<br><strong>Персоны</strong><br>';
			$res .= $people;
			$res .= '</ul>';
		}

		if ($pnear)
		{
			$res .= '<ul>';
			$res .= '<br><strong class="gray">Похожие варианты (Персоны)</strong><br>';
			$res .= $pnear;
			$res .= '</ul>';
		}

/*		if ($cinemas)
		{
			$res .= '<ul>';
			$res .= '<br><strong>Кинотеатры</strong><br>';
			$res .= $cinemas;
			$res .= '</ul>';
		}

		if ($cnear)
		{
			$res .= '<ul>';
			$res .= '<br><strong class="gray">Похожие варианты (Кинотеатры)</strong><br>';
			$res .= $cnear;
			$res .= '</ul>';
		}

*/

	}


	if ($lnk>1)
	{
	$res .= "
	  <script>

	  total = ".($lnk-1).";
	  active = 0;

	  </script>
	";
	} else {
	$res .= "
	  <script>

	  total = 0;
	  active = 0;
	  </script>
	";
	}

	$res .= '<div id="all_results">
	<span class="results"><a href="'._ROOT_URL.$lang.'/main/search/keyword/'.mysql_real_escape_string($_GET['keywords']).'.phtml">Все результаты</a> »</span>
	</div>';

	echo $res;


}
?>