<?

function maxsite_str_word($text, $counttext = 10, $sep = ' ') {
	$words = split($sep, $text);
	if ( count($words) > $counttext )
		$text = join($sep, array_slice($words, 0, $counttext));
	return $text;
}

function main_fest_news_day()
{

	sys::useLib('main::fest');
	sys::useLib('main::news');
	sys::useLib('main::users');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('fest_id','int');



	if(!$_GET['day_id'])
		return 404;

	$arr = explode('-', $_GET['day_id']);

	$fest_id = $arr[0];
	$result['fid'] = $fest_id;

	$r=$_db->query("
		SELECT
			news_id
		FROM `#__main_fest_news`

		WHERE fest_id=".intval($fest_id)."
	");


	while($obj=$_db->fetchAssoc($r))
	{
		$news_id = $obj['news_id'];
	}

	$r=$_db->query("
		SELECT
			title
		FROM `#__main_fest_lng`

		WHERE record_id=".intval($fest_id)."
		AND lang_id=".intval($_cfg['sys::lang_id'])."
	");


	while($obj=$_db->fetchAssoc($r))
	{
		$festival = $obj['title'];
	}


	$r=$_db->query("
		SELECT
			art.id,
			art.date,
			art.user_id,
			art.total_shows,
			art.comments,
			art.exclusive,
			art.small_image AS `image`,
			art.image AS `big_image`,
			art_lng.title,
			art_lng.intro
		FROM `#__main_news_articles` AS `art`

		LEFT JOIN
			`#__main_news_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_news` AS `nws`
		ON art.news_id=nws.id

		WHERE
			art.news_id = '".$news_id."'
			AND DAY(art.date)=".$arr[1]."
			AND MONTH(art.date)=".$arr[2]."
			AND art.public=1

		ORDER BY art.date DESC
	");






	while($obj=$_db->fetchAssoc($r))
	{
		$id='';		$id = date('dmY', strtotime($obj['date']));
		$obj['date']=sys::russianDate($obj['date']);
		$obj['time'] = date('H:i', strtotime($obj['date']));
		$obj['url']=main_news::getArticleUrl($obj['id']);
		$obj['shows']=$obj['total_shows'];
		if($obj['image'])
			$image=$_cfg['main::news_url'].$obj['image'];


		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));


		if ($wid<213 && $obj['big_image'])
		{
		 	$image = $_cfg['main::news_url'].$obj['big_image'];
		} else if ($wid>=213)
		{
		 	$image = $_cfg['main::news_url'].$obj['image'];
		} else {
         	$image = 'blank_news_img.jpg';
		}

		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';





				$result['films'][$id][]=$obj;


				$result['films'][$id]['dayid']=date('dmY', strtotime($obj['date_w_s']));

				$result['films'][$id]['festid']=$obj['fest_id'];



				$result['films'][$id]['dayid']=date('dmY', strtotime($obj['date_w_s']));



//				$result['films'][$id]['month']=date('m', strtotime($obj['date_w_s']));
	}

ksort($result['films']);


	if(!$result)
		return 404;


	$m = sys::translate('main::month_'.$arr[2].'_a');

	$result['title'] = $arr[1].' '.$m.' на фестивале "'.$festival.'"';
	$meta['article']=$result['title'];
	$result['meta']=sys::parseModTpl('main::fest_day','page',$meta);
	if($result['image'])
	{
		$size=getimagesize($_cfg['main::fest_dir'].$result['image']);
		$result['width']=$size[0];
		$result['image']=$_cfg['main::fest_url'].$result['image'];
	}
	$result['alt']=htmlspecialchars($result['alt']);
	$result['source']=htmlspecialchars($result['source']);

	$date=sys::db2Timestamp($result['date']);
	$result['year']=date('Y',$date);
	$result['month']=date('m',$date);

	$result['date']=sys::db2Date($result['date'],false,$_cfg['sys::date_format']);
	$result['user_url']=main_users::getUserUrl($result['user_id']);



	return $result;


}
?>