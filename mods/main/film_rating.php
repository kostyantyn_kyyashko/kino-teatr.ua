<?
function main_film_rating()
{
	sys::useLib('main::films');
	sys::useLib('main::users');
	sys::useLib('sys::form');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');
	sys::filterGet('order_by','text','mark.desc');
	sys::filterGet('age_min','int');
	sys::filterGet('age_max','int');
	sys::filterGet('sex','text');
	sys::filterGet('fan','text');

	if(!$_GET['film_id'])
		return 404;

	define('_CANONICAL',main_films::getFilmRatingUrl($_GET['film_id']),true);

	$film = $_db->getRecord("main_films", $_GET['film_id']);
	
//if(sys::isDebugIP()) sys::printR($film);	
	
	$result['serial'] = $film['serial'];
	$result['stat']['pre_sum'] 		= number_format($film['pre_rating'], 1, '.', '');
	$result['stat']['sum'] 			= number_format($film['rating'], 1, '.', '');
	$result['stat']['pre_votes'] 	= $film['pre_votes'];
	$result['stat']['votes'] 		= $film['votes'];
	
	$result['stat']['reviewer_sum']	= 0;
	$result['stat']['reviewer_votes']= 0;


	$p_date=strtotime($film['ukraine_premiere']?$film['ukraine_premiere']:$film['world_premiere']);
	$is_showing = ($p_date<time());
	
	$result["page_views"] = str_replace('?', $film["total_shows"], sys::translate('main::film_page_wiews'))."<br>";
	$result["expect_yes"] = $film["expect_yes"];
	$result["expect_no"] = $film["expect_no"];
	$result["expect_total"] = $film["expect_yes"] + $film["expect_no"];
	
	$premier = getdate($p_date); // премьера
	$monday = mktime(0, 0, 0, $premier["mon"], $premier["mday"]+9-$premier["wday"], $premier["year"]); // следующий вторник
	$result["premiered"] = (time()>$monday);
	
	$result["premieredZeroExpected"] = ($result["premiered"] && ($result["expect_total"]==0));	 
//if(sys::isDebugIP()) sys::printR($result);		
// данные для рейтингов
		if($_user['id']==2)
		{
			$ip = $_SERVER['REMOTE_ADDR'];
			$sid = session_id();
			$result["expect"]["yourMark"] = $_db->getValue('main_films_expect_unregistered',"mark"   ,"film_id=".intval($film["id"])." AND ip='$ip' AND sid='$sid'");
			$result["stars"]['yourMark'] = $_db->getValue('main_films_rating_unregistered',$is_showing?"mark":'pre_mark',"film_id=".intval($film["id"])." AND ip='$ip' AND sid='$sid'");
		}
		else 
		{
			$result["expect"]['yourMark'] = $_db->getValue('main_films_expect',"mark"    ,"film_id=".intval($film["id"])." AND user_id='".$_user['id']."'");
			$result["stars"]['yourMark']  = $_db->getValue('main_films_rating',$is_showing?"mark":'pre_mark',"film_id=".intval($film["id"])." AND user_id='".$_user['id']."'");
		}

		if($is_showing)
		{
			if ($film['rating']>0)
			{
				if($film['votes']>5) // только при наличии 5 голосов после премьеры показывать реальный рейтинг
				{
					$var['rating']=$film['sum']/($film['pre_votes']+$film['votes']);
				}
				else // до 5 голосов показывать предварительный усредненный рейтинг
				{
					$var['rating']=($film['pre_sum']+$film['sum'])/($film['pre_votes']+$film['votes']);
				}
			} 
			else 
			{
				$_db->query("
					UPDATE
						`#__main_films`
					SET 
						rating = '".$film['pro_rating']."',
						sum = '".$film['pro_sum']."',
						votes = '".$film['pro_votes']."'
					WHERE 
						id='".intval($film["id"])."'
				");
				$film['rating']=($film['pre_sum']+$film['pro_sum'])/($film['pre_votes']+$film['pro_votes']);
			}
		}
		else 
		{
				$film['rating']=$film['pre_rating'];
		}
// звезды
		if(!$result["stars"]['yourMark'] ) $result["stars"]['yourMark'] ="—";
		$result["stars"]['rest']=$film['rating']-floor($film['rating']);
		$result["stars"]['object_id']=$film['id'];
		$result["stars"]['rating']=main_films::formatFilmRating($film['rating']);
		$result["stars"]['ajx_type']='ajx_vote_film';
		$result["stars"]['form_title']='film_rating'.($is_showing?"":'_waiting');

// Рейтинг ожидания		
		$result["expect"]["voted"] = abs($result["expect"]['yourMark']);
		
		if($result["expect"]['yourMark']>0) $result["expect"]['yourMark'] = sys::translate('main::yesExpect');
		else if($result["expect"]['yourMark']<0) $result["expect"]['yourMark'] = sys::translate('main::noExpect');
		else $result["expect"]['yourMark']=sys::translate('main::youNotExpected');
		
		$result["expect"]["film_id"] = $film["id"];
		$result["expect"]["total"] = 0+$film["expect_yes"] + $film["expect_no"];
		$result["expect"]["yes"] = round(0+$film["expect_yes"]*100/($film["expect_yes"]+$film["expect_no"]));
		$result["expect"]["no"] = 100-$result["expect"]["yes"];		
	
	
	$result['film']=$_db->getValue('main_films','title',intval($_GET['film_id']),true);
	if(!$result['film'])
		return 404;

	main::countShow($_GET['film_id'],'film');

	$meta['film']=$result['film'];
	$result['meta']=sys::parseModTpl('main::film_rating','page',$meta);

	//Определение направления сортировки
	$order_by=explode('.',$_GET['order_by']);
	$order_fields=array('user','mark','pre_mark');
	
	if(in_array($order_by[0],$order_fields)) 
	{
		if($order_by[1]=='asc')
		{
			$result['order']='asc';
			foreach ($order_fields as $field)
			{
				$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.desc');
			}
	
		}
		else
		{
			$result['order']='desc';
			foreach ($order_fields as $field)
			{
				$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.asc');
			}
		}
		$result['order_field']=$order_by[0];	
		$order_by=sys::parseOrderBy($_GET['order_by']);
	}
	else 
	 $order_by = " order by user ";
	//=====================================================

	//Поля поисковой формы-------------------------------------
	$result['fields']['age_min']['input']='textbox';
	$result['fields']['age_min']['value']=$_GET['age_min'];
	$result['fields']['age_min']['title']=sys::translate('main::age_min');

	$result['fields']['age_max']['input']='textbox';
	$result['fields']['age_max']['value']=$_GET['age_max'];
	$result['fields']['age_max']['title']=sys::translate('main::age_max');

	$result['fields']['sex']['input']='selectbox';
	$result['fields']['sex']['value']=$_GET['sex'];
	$result['fields']['sex']['empty']=sys::translate('main::any');
	$result['fields']['sex']['values']['man']=sys::translate('main::man');
	$result['fields']['sex']['values']['woman']=sys::translate('main::woman');
	$result['fields']['sex']['title']=sys::translate('main::sex');;

	$result['fields']['fan']['input']='checkbox';
	$result['fields']['fan']['value']=$_GET['fan'];
	$result['fields']['fan']['title']=sys::translate('main::genre_fan');
	//===================================================================

	//Формирование запроса-------------------------------------
	$q="
		SELECT
			rat.user_id,
			usr.login AS `user`,
			rat.pre_mark,
			rat.mark
		FROM `#__main_films_rating` AS `rat`
		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=rat.user_id
	";
	if($_GET['age_min'] || $_GET['age_max'] || $_GET['sex'])
	{
		$q.="
			LEFT JOIN `#__sys_users_data` AS `usr_data`
			ON usr_data.user_id=rat.user_id
		";
	}

	if($_GET['fan'])
	{
		$film_genres=main_films::getFilmGenresIds($_GET['film_id']);
		$q.="
			LEFT JOIN `#__main_users_genres` AS `usr_gnr`
			ON usr_gnr.user_id=rat.user_id
			AND usr_gnr.genre_id IN(".implode(',',$film_genres).")
		";
	}

	$q.="
		WHERE rat.user_id<>2 and rat.film_id=".intval($_GET['film_id'])."
	";

	if($_GET['age_min'])
	{
		$year=intval(date('Y'))-$_GET['age_min'];
		$date_min=$year.'-'.date('m-d');
		$q.="
			AND usr_data.main_birth_date<'".$date_min."'
		";
	}

	if($_GET['age_max'])
	{
		$year=intval(date('Y'))-$_GET['age_max'];
		$date_max=$year.'-'.date('m-d');
		$q.="
			AND usr_data.main_birth_date>'".$date_max."'
		";
	}

	if($_GET['sex'])
	{
		$q.="
			AND usr_data.main_sex='".mysql_real_escape_string($_GET['sex'])."'
		";
	}

	if($_GET['fan'])
	{
		$q.="AND usr_gnr.genre_id IS NOT NULL";
	}

	//====================================================================

	$result['pages']=sys_pages::pocess($q,$_cfg['main::rating_on_page'],false);
	
	
	if ($_REQUEST['order_by'])	$qo = 'order_by='.$_REQUEST['order_by']; else $qo = "";
	
	$pu = _CANONICAL;
	if($qo) $pu .= ((strpos($pu,"?")===false)?"?":"&").$qo;
	
	// Заменить старые ссылки на страницы на новые
	$result['pages']['first_page'] = sys_pages::setPageUrl($result['pages']['first_page'], $pu);
	$result['pages']['prev_page']  = sys_pages::setPageUrl($result['pages']['prev_page'],  $pu);
	$result['pages']['next_page']  = sys_pages::setPageUrl($result['pages']['next_page'],  $pu);
	$result['pages']['last_page']  = sys_pages::setPageUrl($result['pages']['last_page'],  $pu);
	foreach ($result['pages']['pages'] as $i=>$p)
	 $result['pages']['pages'][$i] = sys_pages::setPageUrl($result['pages']['pages'][$i],  $pu);

	$result['form_action'] = $pu;
	
	$q.=" GROUP BY usr.id";
	$q = "SELECT *, (SELECT COUNT(id) FROM #__main_reviews WHERE public AND film_id=".intval($_GET['film_id'])." AND user_id=T.user_id) AS is_reviewer FROM ($q) T";
	$q.=$order_by;	
	$r=$_db->query($q.$result['pages']['limit']);
//	if(sys::isDebugIP()) $_db->printR($q);
	$result['objects']=array();
	$result['stat']['reviewer_sum']	= 0;
	$result['stat']['reviewer_votes'] = 0;
	
	while($obj=$_db->fetchAssoc($r))
	{
//		if($obj['is_reviewer'])
//		{
//			$result['stat']['reviewer_sum']	+= $obj['mark'];
//			$result['stat']['reviewer_votes']++;
//		}

		if(!$obj['pre_mark'])
			$obj['pre_mark']='-';

		if(!$obj['mark'])
			$obj['mark']='-';
					
		$obj['url']=main_users::getUserUrl($obj['user_id']);
		$result['objects'][]=$obj;
	}

	$q = "SELECT mark FROM #__main_reviews WHERE public AND film_id=".intval($_GET['film_id']);
	$r=$_db->query($q);
	while($obj=$_db->fetchAssoc($r))
	{
			$result['stat']['reviewer_sum']	+= $obj['mark'];
			$result['stat']['reviewer_votes']++;		
	}
	
	if($result['stat']['reviewer_votes']>0)
		$result['stat']['reviewer_sum'] = $result['stat']['reviewer_sum'] / $result['stat']['reviewer_votes'];
	$result['stat']['reviewer_sum'] = number_format($result['stat']['reviewer_sum'], 1, '.', '');
	
//	if(sys::isDebugIP()) $_db->printR($result["pages"]);
	
	return $result;
}
?>