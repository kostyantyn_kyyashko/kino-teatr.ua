<?
function main_concurs()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::news');
	sys::useLib('main::users');
	sys::useLib('sys::pages');

	sys::filterGet('month');
	sys::filterGet('year');
	sys::filterGet('news_id');

	$cache_name='main_news_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::news_page_cache_period']*60))
		return unserialize($cache);

	/*if($_GET['year']<2000 || $_GET['year']>intval(date('Y')) || $_GET['month']<1 || $_GET['month']>12)
		return 404;*/

	$_GET['news_id']=18;


	$result['section']=$meta['section']='Хэллоуин';



	if(isset($_POST['_task']) && $_POST['_task'])
	{
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete' && sys::checkAccess('main::news_delete'))
			main_news::deleteArticle($task[1]);
	}

	$result['date']=$meta['date']=mb_strtolower(sys::translate('main::month_'.intval($_GET['month']))).' '.$_GET['year'];
	$result['meta']=sys::parseModTpl('main::concurs','page',$meta);
	$result['title']=sys::translate('main::news_cinema');
	$month_begin=date('Y-m-d H:i:s',main_news::getMonthBegin($_GET['month'],$_GET['year']));
	$month_end=date('Y-m-d H:i:s',main_news::getMonthEnd($_GET['month'],$_GET['year']));
	$q="
		SELECT
			art.id,
			art.date,
			art.user_id,
			art.exclusive,
			art.small_image AS `image`,
			art_lng.title,
			art_lng.intro
		FROM `#__main_news_articles` AS `art`

		LEFT JOIN
			`#__main_news_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_news` AS `nws`
		ON art.news_id=nws.id

		WHERE
			/*art.date BETWEEN '".$month_begin."' AND '".$month_end."'
			AND */(art.city_id=".intval($_cfg['main::city_id'])." OR art.city_id=0)
			AND art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
	";
	if($_GET['news_id'])
	{
		$q.="
			AND art.news_id=".intval($_GET['news_id'])."
		";
	}
	$q.="
		ORDER BY art.date DESC
	";

	$result['pages']=sys_pages::pocess($q,$_cfg['main::news_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true,$_cfg['sys::date_format']);
		if($obj['image'])
			$obj['image']=$_cfg['main::news_url'].$obj['image'];
		else
			$obj['image']=main::getNoImage('x1');
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_news::getArticleUrl($obj['id']);

		if(sys::checkAccess('main::news_edit') || $_user['id']==$obj['user_id'])
			$obj['edit_url']=main_news::getArticleEditUrl($obj['id']);
		else
			$obj['edit_url']=false;

		if(sys::checkAccess('main::news_delete') || $_user['id']==$obj['user_id'])
			$obj['delete']=true;
		else
			$obj['delete']=false;
		$result['objects'][]=$obj;
	}

	if(!$result['objects']/* && !main_news::newsReirect($_GET['year'],$_GET['month'],$_GET['news_id'])*/)
		return 404;

	if(sys::checkAccess('main::news'))
		$result['add_url']=main_news::getArticleAddUrl();
	else
		$result['add_url']=false;

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>