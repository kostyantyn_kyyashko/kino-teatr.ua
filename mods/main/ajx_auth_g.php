<?
// https://accounts.google.com/signin/oauth/oauthchooseaccount?client_id=329278728251-vumhbienhs8og8ps5p4qnq365uca9go4.apps.googleusercontent.com&as=-1fb177f55670f808&destination=https%3A%2F%2Fkino-teatr.ua&approval_state=!ChRPb2d1RFFLUGI0WGNRbzVydk9zUhIfSTRIQ3JWYnRYbk1kd0Q4c01wTnQ0WWIzeE5BaUNoWQ%E2%88%99ACThZt4AAAAAWkeB5cNQGhkqbjnBnMt6uMw9t95DX2mw&xsrfsig=AHgIfE_JhpkbLzZyYRszUW7561tGwikNaA&flowName=GeneralOAuthFlow
// https://developers.google.com/api-client-library/javascript/samples/samples#authorizing-and-making-authorized-requests
// https://github.com/google/google-api-javascript-client/blob/master/samples/authSample.html
// https://habrahabr.ru/post/145988/

const JSON_ID = '{"web":{"client_id":"329278728251-v06bbnf39a2dhi7203h1f7uvusks2kod.apps.googleusercontent.com","project_id":"fourth-dynamo-190505","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://accounts.google.com/o/oauth2/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","client_secret":"QqmFnsWmX3rEaGxFtRg46E_Z","redirect_uris":["https://kino-teatr.ua"]}}';

function Transliterate($string)
{
  $cyr=array(
     "Щ", "Ш", "Ч","Ц", "Ю", "Я", "Ж","А","Б","В",
     "Г","Д","Е","Ё","З","И","Й","К","Л","М","Н",
     "О","П","Р","С","Т","У","Ф","Х","Ь","Ы","Ъ",
     "Э","Є", "Ї","І",
     "щ", "ш", "ч","ц", "ю", "я", "ж","а","б","в",
     "г","д","е","ё","з","и","й","к","л","м","н",
     "о","п","р","с","т","у","ф","х","ь","ы","ъ",
     "э","є", "ї","і"
  );
  $lat=array(
     "Shch","Sh","Ch","C","Yu","Ya","J","A","B","V",
     "G","D","e","e","Z","I","y","K","L","M","N",
     "O","P","R","S","T","U","F","H","",
     "Y","" ,"E","E","Yi","I",
     "shch","sh","ch","c","yu","ya","j","a","b","v",
     "g","d","e","e","z","i","y","k","l","m","n",
     "o","p","r","s","t","u","f","h",
     "", "y","" ,"e","e","yi","i"
  );
  for($i=0; $i<count($cyr); $i++)  {
     $c_cyr = $cyr[$i];
     $c_lat = $lat[$i];
     $string = str_replace($c_cyr, $c_lat, $string);
  }
  $string =
  	preg_replace(
  		"/([qwrtpsdfghklzxcvbnmQWRTPSDFGHKLZXCVBNM]+)[jJ]e/",
  		"\${1}e", $string);
  $string =
  	preg_replace(
  		"/([qwrtpsdfghklzxcvbnmQWRTPSDFGHKLZXCVBNM]+)[jJ]/",
  		"\${1}'", $string);
  $string = preg_replace("/([eyuioaEYUIOA]+)[Kk]h/", "\${1}h", $string);
  $string = preg_replace("/^kh/", "h", $string);
  $string = preg_replace("/^Kh/", "H", $string);
  return $string;
}

function badResult($msg)
{
	$result["success"] = false;
	$result["msg"] = $msg;
	echo json_encode($result);
}

function DoCheckExistsAccount($user)
{
	global $_db, $_cfg, $_err, $_user, $_cookie;
	$accRec = false;

	$socRec = $_db->getRecord("sys_users_social_network_info", "gplus_id='{$user["id"]}'");

	if($socRec)
		$accRec = $_db->getRecord("sys_users", $socRec["user_id"]);	
		
	if($accRec)	
		return $accRec;

	if($user["emails"] && sizeof($user["emails"]))	
	foreach($user["emails"] as $email)	
	{	
		if(empty($email["value"])) continue;
		$accRec = $_db->getRecord("sys_users", "email='".$email["value"]."'");		
		if($accRec) return $accRec;
	}		
		
	return $accRec?$accRec:false;
}

function DoCreateAccount($user)
{
	global $_db, $_cfg, $_err, $_user, $_cookie;
	
	$flame = Transliterate(isset($user["name"]["familyName"])?$user["name"]["familyName"]:"");
	$lfame = Transliterate(isset($user["name"]["givenName"])?$user["name"]["givenName"]:"");	

	$tlname = strtolower(Transliterate(isset($user["name"]["familyName"])?$user["name"]["familyName"]:""));
	$tfname = strtolower(Transliterate(isset($user["name"]["givenName"])?$user["name"]["givenName"]:""));	
	$login = substr($tfname, 0,1).$tlname;
	
	$email = isset($user["emails"])?$user["emails"][0]["value"]:"";
	$gender = (isset($user["gender"]) && ($user["gender"]=="male"))?"man":"woman";
	$np = '';
	
	$count = 2;
	$testLogin = $login;
	
	while( $_db->getRecord("sys_users", "login='".$testLogin."'") )
		$testLogin = $login.($count++);
	$login = $testLogin;	

	$_db->query("INSERT INTO `#__sys_users` (`login`, `password`, `date_reg`, `email`, `on`, `date_last_visit`, `lang_id`) VALUES ('".$login."', '".md5($login)."','".date("Y-m-d H:i:s")."', '$email', '1','".date("Y-m-d H:i:s")."','1')");
	$newid = $_db->last_id;	

	if (isset($user["image"]) && isset($user["image"]["url"]))
	{
		$content = file_get_contents($user["image"]["url"]);
	
		$fp = fopen("/var/www/html/multiplex/multiplex.in.ua/public/main/users/user_".$newid.".jpg", "w");
		fwrite($fp, $content);
		fclose($fp);
		$np = "user_".$newid.".jpg";
		sys::resizeImage($_cfg['main::users_dir'].$np,false,65,65);
	}   		
	
	$_db->query("INSERT INTO `#__sys_users_cfg` (`user_id`, `sys_timezone`, `main_city_id`) VALUES ('".$newid."','Europe/Kiev','1')");
	$_db->query("INSERT INTO `#__sys_users_data` (`user_id`, `main_nick_name`, `main_first_name`, `main_last_name`, `main_sex`, `main_avatar`)	VALUES ('".$newid."','".$login."','".$fname."','".$lname."','".$gender."','".$np."')");
	$_db->query("INSERT INTO `#__sys_users_groups` (`user_id`, `group_id`) VALUES ('".$newid."','8')	");	
	$_db->query("INSERT INTO `#__sys_users_social_network_info` (user_id, gplus_id) VALUES (".$newid.", ".$user["id"].")");
	
	return $_db->getRecord("sys_users", $newid, false);
}

function DoLogin($user)
{
	global $_db, $_cfg, $_err, $_user, $_cookie;
	$result["success"] = false;

	if ( $user )
	{
		if( !($userRec = DoCheckExistsAccount($user)) )
			$userRec = DoCreateAccount($user);				

		$logged=sys::authorizeUserSoc($userRec["login"]);			

 		if($logged===true)
 		{
 			$_db->query("UPDATE `#__sys_users_social_network_info` SET gplus_id=".$user["id"]." WHERE user_id=".$userRec["id"]."");			
 			
			$result["success"] = true;				
			echo json_encode($result);
			return;	 		
 		}
		else
			return badResult(sys::translate('main::errAuthErrLogin')); 
	}	
	else 
		return badResult(sys::translate('main::errAuthG')." ".sys::translate('main::errAuthErrLogin'));
	
	badResult(sys::translate('main::errAuthErrLogin'));	
}

function DoUnlink()
{
	global $_db, $_cfg, $_err, $_user, $_cookie;
	$_db->query("UPDATE `#__sys_users_social_network_info` SET gplus_id=NULL WHERE user_id=".$_user["id"]."");			
	$result["success"] = false;	
}

function main_ajx_auth_g()
{
	sys::setTpl();
	global $_db, $_cfg, $_err, $_user, $_cookie;
	$result["success"] = false;
	
	$data = isset($_POST["response"])?$_POST["response"]:"";
	
	switch($_POST["_task"])
	{
		case "login": doLogin($data);
					  break;
					  					  
		case "unlink": doUnlink($data);
					   break;
					  
		default:	$result["msg"] = sys::translate('main::errAuthCommon');
					echo json_encode($result);
	}
}

?>