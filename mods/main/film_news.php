<?
function main_film_news()
{
	sys::useLib('main::films');
	sys::useLib('main::news');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');

	define('_CANONICAL',main_films::getFilmNewsUrl($_GET['film_id']),true);

	if(!$_GET['film_id'])
		return 404;

	$result['film']=$_db->getValue('main_films','title',intval($_GET['film_id']),true);
	if(!$result['film'])
		return 404;

	main::countShow($_GET['film_id'],'film');

	$result['serial']=$_db->getValue('main_films','serial',intval($_GET['film_id']),false);
	$meta['film']=$result['film'];
	$result['title'] = ($result["serial"]?sys::translate('main::news_about_serial'):sys::translate('main::news_about_film')).' '.$result['film'];
	$result['meta']=sys::parseModTpl('main::film_news','page',$meta);

	$q="
		SELECT
			art.id,
			art.date,
			art.small_image AS `image`,
			art.image AS `big_image`,
			art.exclusive,
			art_lng.title,
			art_lng.intro
		FROM
			`#__main_news_articles_films` AS `art_flm`

		LEFT JOIN
			`#__main_news_articles` AS `art`
		ON
			art.id=art_flm.article_id

		LEFT JOIN
			`#__main_news_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE
			art_flm.film_id=".intval($_GET['film_id'])."
		AND
			art.public=1
		AND
			art.date<'".gmdate('Y-m-d H:i:s')."'

		ORDER BY
			art.date DESC
	";

	$result['pages']=sys_pages::pocess($q,$_cfg['main::news_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::russianDate($obj['date']);

		if($obj['image'] || $obj['big_image'])
		{
			// inserted by Mike begin
  	    	$image=$obj['image'];	
  	    	list($wid, $hei, $type) = getimagesize('./public/main/news/'.$image);
			if($wid<213 && $obj['big_image']) $image = $obj['big_image'];

			if(!file_exists('./public/main/news/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::news_url'].$image;
			$obj['image'] = $image;
			// inserted by Mike end
		}
		else 
		 $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
/*		
		if($obj['image'])
			$image=$_cfg['main::news_url'].$obj['image'];

		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$obj['image']));

		if ($wid<213 && $obj['big_image'])
		{
		 	$image = $_cfg['main::news_url'].$obj['big_image'];
		} else if ($wid>=213)
		{
		 	$image = $_cfg['main::news_url'].$obj['image'];
		} else {
         	$image = 'blank_news_img.jpg';
		}

		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';
*/

		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_news::getArticleUrl($obj['id']);
		$result['objects'][]=$obj;
	}

	return $result;
}
?>