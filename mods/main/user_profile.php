<?
function main_user_profile()
{
	define('_NOINDEX','1');
	sys::useLib('main::users');
	sys::useLib('main::questions');
	sys::useLib('main::genres');;
	sys::useLib('sys::form');
	sys::useLib('sys::langs');
	sys::useLib('main::countries');
	sys::useLib('sys::check');
	sys::useJs('sys::gui');
	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;
	if($_user['id']==2)
		return 401;



	$result['meta']=sys::parseModTpl('main::user_profile','page');
	
	$q="
		SELECT
			usr.email,
			usr.login,
			usr.lang_id,
			usr_cfg.sys_timezone AS `timezone`,
			usr_cfg.main_city_id AS `city_id`,
			usr_dat.main_nick_name AS `nickname`,
			usr_dat.main_first_name AS `firstname`,
			usr_dat.main_last_name AS `lastname`,
			usr_dat.main_sex AS `sex`,
			usr_dat.main_birth_date AS `birth_date`,
			usr_dat.main_avatar AS `avatar`,
			usr_dat.main_info AS `info`,
			usr_dat.main_phone AS `phone`
		FROM `#__sys_users` AS `usr`

		LEFT JOIN `#__sys_users_cfg` AS `usr_cfg`
		ON usr_cfg.user_id=usr.id

		LEFT JOIN `#__sys_users_data` AS `usr_dat`
		ON usr_dat.user_id=usr.id

		WHERE usr.id=".$_user['id']."

	";
	//if(sys::isDebugIP()) $_db->printR($q); 

	$r=$_db->query($q);

	$user=$_db->fetchAssoc($r);
	$user['birth_date']=sys::db2Date($user['birth_date'],false,$_cfg['sys::date_format']);
	$values=sys::setValues($user);

	//login
	$fields['login']['input']='textbox';
	#$fields['login']['unique']=true;
	#$fields['login']['table']='sys_users';
	$fields['login']['type']='text';
	$fields['login']['req']=true;
	$fields['login']['max_chars']=16;
	$fields['login']['min_chars']=2;
	$fields['login']['title']=sys::translate('main::login');

	//email
	$fields['email']['input']='textbox';
	#$fields['email']['unique']=true;
	#$fields['email']['table']='sys_users';
	$fields['email']['type']='email';
	$fields['email']['req']=true;
	$fields['email']['title']=sys::translate('main::email');

	//nickname
	$fields['nickname']['input']='textbox';
	$fields['nickname']['type']='text';
	$fields['nickname']['max_chars']=70;
	$fields['nickname']['req']=false; // true
	$fields['nickname']['title']=sys::parseModTpl('main::nickname');
    
	//firstname
	$fields['firstname']['input']='textbox';
	$fields['firstname']['type']='text';
	$fields['firstname']['max_chars']=55;
	$fields['firstname']['req']=false; // true
//	$fields['firstname']['title']=sys::translate('main::firstname');
	$fields['firstname']['title']=sys::translate('main::feal_firstname');

	//lastname
	$fields['lastname']['input']='textbox';
	$fields['lastname']['type']='text';
	$fields['lastname']['max_chars']=55;
	$fields['lastname']['req']=false; // true
	$fields['lastname']['title']=sys::translate('main::lastname');
	
	//phone
	$fields['phone']['input']='textbox';
	$fields['phone']['type']='phone';
	$fields['phone']['req']=false;	
	$fields['phone']['html_params']='tabindex="9" autocomplete="off" id="phone" placeholder="380651234567" maxlength=12'; // !!! 3 parameter in sys_form::parseField() overload this value !!!
	$fields['phone']['title']=sys::translate('main::phone');

	//birth_date
	$fields['birth_date']['input']='datebox';
	$fields['birth_date']['type']='date';
	$fields['birth_date']['icon']='';
	$fields['birth_date']['format']=$_cfg['sys::date_format'];
	$fields['birth_date']['req']=false; // true
	$fields['birth_date']['title']=sys::translate('main::birth_date');

	//sex
	$fields['sex']['input']='selectbox2';
	$fields['sex']['type']='text';
	$fields['sex']['values']['man']=sys::translate('main::man');
	$fields['sex']['values']['woman']=sys::translate('main::woman');
	$fields['sex']['req']=false; // true
	$fields['sex']['title']=sys::translate('main::sex');
	$fields['sex']['empty']='';

	//avatar
	$fields['avatar']['input']='filebox';
	$fields['avatar']['type']='file';
	$fields['avatar']['file::dir']=$_cfg['main::users_dir'];
	$fields['avatar']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['avatar']['title']=sys::translate('main::avatar');
	$fields['avatar']['file::max_size']=10*_MB;
	$fields['avatar']['delete']=true;
	$fields['avatar']['preview']=true;

	//city_id
	$fields['city_id']['input']='selectbox2';
	$fields['city_id']['type']='int';
	$fields['city_id']['values']=main_countries::getCountryCitiesTitles(29);
	$fields['city_id']['req']=true;
	$fields['city_id']['title']=sys::translate('main::city');

	//lang_id
	$fields['lang_id']['input']='selectbox2';
	$fields['lang_id']['type']='int';
	foreach ($_langs as $lang_id=>$lang) $fields['lang_id']['values'][$lang_id]=$lang['title'];
	$fields['lang_id']['req']=false; // true
	$fields['lang_id']['title']=sys::translate('main::language');

	//timezone
	$fields['timezone']['input']='selectbox2';
	$fields['timezone']['type']='text';
	$fields['timezone']['req']=false; // true
	$fields['timezone']['value']='Europe/Kiev';
	$fields['timezone']['title']=sys::translate('main::timezone');
	$timezones=timezone_identifiers_list();
	foreach ($timezones as $zone) $fields['timezone']['values'][$zone]=$zone;
	unset($zones, $zone);

	if(sys::isDebugIP()){
		//info
		$fields['info']['input']='textarea';
		$fields['info']['type']='text';
		$fields['info']['max_chars']=1000;
		$fields['info']['req']=false;
		$fields['info']['title']=sys::translate('main::about_me');
	}
	
	$fields=sys_form::parseFields($fields, $values, false);
//if(sys::isDebugIP()){
//	sys::printR($fields);
//}
	
	$result['genres']=main_genres::getGenresTitles();
	$result['question']=main_questions::getQuestion();
	$result['fields']=$fields;
	$result['registred']=false;

	if(!isset($_POST['genres']))
	{
		$r=$_db->query("
			SELECT `genre_id` FROM `#__main_users_genres`
			WHERE `user_id`=".intval($_user['id'])."
		");
		while (list($genre_id)=$_db->fetchArray($r))
		{
			$_POST['genres'][$genre_id]='on';
		}
	}



	if(isset($_POST['cmd_save']))
	{
        $_err = sys_check::checkValues($fields,$_POST);
		if(!$_err) {
            if (!$_err) $err = array();
            
			if($_db->getValue('sys_users','id',"`email`='".mysql_real_escape_string($_POST['email'])."' AND `id`!=".intval($_user['id'])."")) {
				$_err['email'] = array(
                    'title' => $fields['email']['title'],
                    'text' => sys::translate('main::email_busy')
                );
            }
            
			if($_db->getValue('sys_users','id',"`login`='".mysql_real_escape_string($_POST['login'])."' AND `id`!=".intval($_user['id'])."")) {
				$_err['login'] = array(
                    'title' => $fields['login']['title'],
                    'text' => sys::translate('main::login_busy')
                );
            }

			$_db->begin();
			//Основная инфа
			if(!$_err && !$_db->query("
				UPDATE `#__sys_users`
				SET
					`login`='".mysql_real_escape_string($values['login'])."',
					`email`='".mysql_real_escape_string($values['email'])."',
					`lang_id`=".intval($values['lang_id'])."
				WHERE
					`id`=".intval($_user['id'])."
			"))
			{
				$_db->rollback();
				$_err=sys::translate('main::db_err');
			}
			else
				$user_id=$_db->last_id;

			//Настройки
			if(!$_err && !$_db->query("
				UPDATE `#__sys_users_cfg`
				SET
					`sys_timezone`='".mysql_real_escape_string($values['timezone'])."',
					`main_city_id`=".intval($values['city_id'])."
				WHERE
					`user_id`=".intval($_user['id'])."

			"))
			{
				$_db->rollback();
				$_err=sys::translate('main::db_err');
			}
			$values['phone'] = str_replace('+','',$values['phone']);

			//Данные
			if(!$_err && !$_db->query("
				UPDATE `#__sys_users_data`
					SET
					`main_nick_name`='".mysql_real_escape_string($values['nickname'])."',
					`main_first_name`='".mysql_real_escape_string($values['firstname'])."',
					`main_last_name`='".mysql_real_escape_string($values['lastname'])."',
					`main_sex`='".mysql_real_escape_string($values['sex'])."',
					`main_phone`='".mysql_real_escape_string($values['phone'])."',
					`main_info`='".mysql_real_escape_string(strip_tags($values['info']))."',
					`main_birth_date`='".mysql_real_escape_string(sys::date2Db($values['birth_date'],false,$_cfg['sys::date_format']))."'

				WHERE
					`user_id`=".intval($_user['id'])."

			"))
			{
				$_db->rollback();
				$_err=sys::translate('main::db_err');
			}

			if(!$_err)
			{
				$_db->delete('main_users_genres',"`user_id`=".intval($_user['id'])."");
				if(isset($_POST['genres']) && is_array($_POST['genres']))
				{
					foreach ($_POST['genres'] as $genre_id=>$val)
					{
						if(!$_err && !$_db->query("
							INSERT INTO `#__main_users_genres`
							(
								`user_id`,
								`genre_id`
							)
							VALUES
							(
								'".$_user['id']."',
								'".intval($genre_id)."'
							)
						"))
						{
							$_db->rollback();
							$_err=sys::translate('main::db_err');
							break;
						}
					}
				}
			}
            
			if(!$_err)
			{
				$_db->commit();
				if($_FILES['avatar']['name'])
					main_users::uploadUseraAvatar($_FILES['avatar'],$_user['id']);
				sys::redirect(main_users::getProfileUrl(),false);
			}


		}
	}

	if(isset($_POST['_task']))
	{
		if(strpos($_POST['_task'],'delete_file')===0)
		{
			$task=explode('.',$_POST['_task']);
			if($task[1]=='avatar')
			{
				$_db->query("
					UPDATE `#__sys_users_data`
					SET `main_avatar`=''
					WHERE `user_id`=".intval($_user['id'])."
				");
				main_users::deleteUserAvatar($values['avatar']);
				$values['avatar']=false;
				$_user['main_avatar']=false;
                sys::redirect(main_users::getProfileUrl(),false);
			}
		}
	}


	return $result;
}
?>