<?
function main_films_near()
{
function dmword($string, $is_cyrillic = true)
	{
		static $codes = array(
			'A' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1)),
				'U' => array(array(0, 7, -1))),
			'B' => array(array(7, 7, 7)),
			'C' => array(array(5, 5, 5), array(4, 4, 4),
				'Z' => array(array(4, 4, 4),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'Z' => array(array(4, 4, 4))),
				'K' => array(array(5, 5, 5), array(45, 45, 45)),
				'H' => array(array(5, 5, 5), array(4, 4, 4),
					'S' => array(array(5, 54, 54)))),
			'D' => array(array(3, 3, 3),
				'T' => array(array(3, 3, 3)),
				'Z' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'Z' => array(array(4, 4, 4))),
				'R' => array(
					'S' => array(array(4, 4, 4)),
					'Z' => array(array(4, 4, 4)))),
			'E' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1)),
				'U' => array(array(1, 1, -1))),
			'F' => array(array(7, 7, 7),
				'B' => array(array(7, 7, 7))),
			'G' => array(array(5, 5, 5)),
			'H' => array(array(5, 5, -1)),
			'I' => array(array(0, -1, -1),
				'A' => array(array(1, -1, -1)),
				'E' => array(array(1, -1, -1)),
				'O' => array(array(1, -1, -1)),
				'U' => array(array(1, -1, -1))),
			'J' => array(array(4, 4, 4)),
			'K' => array(array(5, 5, 5),
				'H' => array(array(5, 5, 5)),
				'S' => array(array(5, 54, 54))),
			'L' => array(array(8, 8, 8)),
			'M' => array(array(6, 6, 6),
				'N' => array(array(66, 66, 66))),
			'N' => array(array(6, 6, 6),
				'M' => array(array(66, 66, 66))),
			'O' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1))),
			'P' => array(array(7, 7, 7),
				'F' => array(array(7, 7, 7)),
				'H' => array(array(7, 7, 7))),
			'Q' => array(array(5, 5, 5)),
			'R' => array(array(9, 9, 9),
				'Z' => array(array(94, 94, 94), array(94, 94, 94)), // special case
				'S' => array(array(94, 94, 94), array(94, 94, 94))), // special case
			'S' => array(array(4, 4, 4),
				'Z' => array(array(4, 4, 4),
					'T' => array(array(2, 43, 43)),
					'C' => array(
						'Z' => array(array(2, 4, 4)),
						'S' => array(array(2, 4, 4))),
					'D' => array(array(2, 43, 43))),
				'D' => array(array(2, 43, 43)),
				'T' => array(array(2, 43, 43),
					'R' => array(
						'Z' => array(array(2, 4, 4)),
						'S' => array(array(2, 4, 4))),
					'C' => array(
						'H' => array(array(2, 4, 4))),
					'S' => array(
						'H' => array(array(2, 4, 4)),
						'C' => array(
							'H' => array(array(2, 4, 4))))),
				'C' => array(array(2, 4, 4),
					'H' => array(array(4, 4, 4),
						'T' => array(array(2, 43, 43),
							'S' => array(
								'C' => array(
									'H' => array(array(2, 4, 4))),
								'H' => array(array(2, 4, 4))),
							'C' => array(
								'H' => array(array(2, 4, 4)))),
						'D' => array(array(2, 43, 43)))),
				'H' => array(array(4, 4, 4),
					'T' => array(array(2, 43, 43),
						'C' => array(
							'H' => array(array(2, 4, 4))),
						'S' => array(
							'H' => array(array(2, 4, 4)))),
					'C' => array(
						'H' => array(array(2, 4, 4))),
					'D' => array(array(2, 43, 43)))),
			'T' => array(array(3, 3, 3),
				'C' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4))),
				'Z' => array(array(4, 4, 4),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'Z' => array(array(4, 4, 4)),
					'H' => array(array(4, 4, 4)),
					'C' => array(
						'H' => array(array(4, 4, 4)))),
				'T' => array(
					'S' => array(array(4, 4, 4),
						'Z' => array(array(4, 4, 4)),
						'C' => array(
							'H' => array(array(4, 4, 4)))),
					'C' => array(
						'H' => array(array(4, 4, 4))),
					'Z' => array(array(4, 4, 4))),
				'H' => array(array(3, 3, 3)),
				'R' => array(
					'Z' => array(array(4, 4, 4)),
					'S' => array(array(4, 4, 4)))),
			'U' => array(array(0, -1, -1),
				'E' => array(array(0, -1, -1)),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1))),
			'V' => array(array(7, 7, 7)),
			'W' => array(array(7, 7, 7)),
			'X' => array(array(5, 54, 54)),
			'Y' => array(array(1, -1, -1)),
			'Z' => array(array(4, 4, 4),
				'D' => array(array(2, 43, 43),
					'Z' => array(array(2, 4, 4),
						'H' => array(array(2, 4, 4)))),
				'H' => array(array(4, 4, 4),
					'D' => array(array(2, 43, 43),
						'Z' => array(
							'H' => array(array(2, 4, 4))))),
				'S' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'C' => array(
						'H' => array(array(4, 4, 4))))));
		$length = strlen($string);
		$output = '';
		$i = 0;
		$previous = -1;
		while ($i < $length)
		{
			$current = $last = &$codes[$string[$i]];
			for ($j = $k = 1; $k < 7; $k++)
			{
				if (!isset($string[$i + $k]) || !isset($current[$string[$i + $k]]))
					break;
				$current = &$current[$string[$i + $k]];
				if (isset($current[0]))
				{
					$last = &$current;
					$j = $k + 1;
				}
			}
			if ($i == 0)
				$code = $last[0][0];
			elseif (!isset($string[$i + $j]) || ($codes[$string[$i + $j]][0][0] != 0))
				$code = $is_cyrillic ? (isset($last[1]) ? $last[1][2] : $last[0][2]) : $last[0][2];
			else
				$code = $is_cyrillic ? (isset($last[1]) ? $last[1][1] : $last[0][1]) : $last[0][1];
			if (($code != -1) && ($code != $previous))
				$output .= $code;
			$previous = $code;
			$i += $j;
		}
		return str_pad(substr($output, 0, 6), 6, '0');
	}

//------------------------------------------------------------------------------

	function dmstring($string)
	{
		$is_cyrillic = false;
		if (preg_match('/[А-Яа-я]/iu', $string) === 1)
		{
			$string = translit($string);
			$is_cyrillic = true;
		}

		$firstword = '';
		$lastword = '';
		$words = explode(' ', $string);

		if (preg_match('/[A-Za-z]/iu', $words[0]) !== 1)
		{
			$firstword = $words[0];
		}

		if (preg_match('/[A-Za-z]/iu', $words[count($words)-1]) !== 1)
		{
			$lastword = $words[count($words)-1];
		}

		$string = preg_replace(array('/[^\w\s]|\d/iu', '/\b[^\s]{1,1}\b/iu', '/\s{1,}/iu', '/^\s+|\s+$/iu'), array('', '', ' '), strtoupper($string));

		if ($string[0])
		{
			$matches = explode(' ', $string);
		}  else {
			$matches = array();
		}

		foreach($matches as $key => $match)
			$matches[$key] = dmword($match, $is_cyrillic);


		if ($firstword)
		{
			array_unshift($matches, $firstword);
		}

		if ($lastword)
		{
			$matches[]=$lastword;
		}

		return $matches;
	}

//------------------------------------------------------------------------------

	function translit($string)
	{
		static $ru = array(
			'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
			'Ё', 'ё', 'Ж', 'ж', 'З', 'з', 'И', 'и', 'Й', 'й', 'К', 'к',
			'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',
			'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц',
			'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ', 'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь',
			'Э', 'э', 'Ю', 'ю', 'Я', 'я'
		);
		static $en = array(
			'A', 'a', 'B', 'b', 'V', 'v', 'G', 'g', 'D', 'd', 'E', 'e',
			'E', 'e', 'Zh', 'zh', 'Z', 'z', 'I', 'i', 'J', 'j', 'K', 'k',
			'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'R', 'r',
			'S', 's', 'T', 't', 'U', 'u', 'F', 'f', 'H', 'h', 'C', 'c',
			'Ch', 'ch', 'Sh', 'sh', 'Sch', 'sch', '\'', '\'', 'Y', 'y', '\'', '\'',
			'E', 'e', 'Ju', 'ju', 'Ja', 'ja'
		);
		$string = str_replace($ru, $en, $string);
		return $string;
	}

//------------------------------------------------------------------------------


	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	sys::filterGet('title');
	sys::filterGet('genre_id');
	sys::filterGet('year');
	sys::filterGet('letter');
	sys::filterGet('city_id');
	sys::filterGet('show');
	sys::filterGet('order_by','text','title.asc');

		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		define('_CANONICAL',_ROOT_URL.'films-near.phtml',true);
		} else {
		define('_CANONICAL',_ROOT_URL.$lang.'/films-near.phtml',true);
		}


	$cache_name='main_films_near_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
//	if($cache=sys::getCache($cache_name,$_cfg['main::films_page_cache_period']*60))
//		return unserialize($cache);

	$result=array();
	$result['meta']=sys::parseModTpl('main::films_near','page');


		$current_day = date("N");
		$days_to_friday = 7 - $current_day;
		$days_from_monday = $current_day - 1;
		$monday = date("Y-m-d", strtotime("- {$days_from_monday} Days"));
		$friday = date("Y-m-d", strtotime("+ {$days_to_friday} Days"));

	//Формирование запроса-----------------------------------------------	
	$q="
		SELECT
			flm.id,
			flm.year,
			flm.title_orig,
			lng.title,
			flm.ukraine_premiere,
			flm.world_premiere,
			flm.pre_rating,
			flm.pre_votes,
			flm.rating,
			flm.votes,
			flm.pro_rating,
			flm.pro_votes,
			flm.pre_sum,
			flm.sum,
			flm.expect_yes,
			flm.expect_no			
		FROM `#__main_films` AS `flm`
		LEFT JOIN `#__main_films_lng` AS `lng`
		ON lng.record_id=flm.id
		AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			flm.ukraine_premiere>='".$monday."'
		ORDER BY
			flm.ukraine_premiere ASC
	";

	$r=$_db->query($q);
	$result['objects']=array();

$year = 0;

	while($obj=$_db->fetchAssoc($r))
	{
		$new_year = date('Y',strtotime($obj['ukraine_premiere']));
		if ($new_year>$year)
		{
			$year = $new_year;
			$obj['new_year'] = $new_year;
		}


	$poster=main_films::getFilmFirstPoster($obj['id']);

		if($poster)
		{
	// inserted by Mike begin
			if ($obj['ukraine_premiere']>=$monday && $obj['ukraine_premiere']<=$friday)
	  	    	$image="x2_".$poster['image']; 
	  	    else 	
	  	    	$image="x2_".$poster['image']; // was 1
			if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::films_url'].$image;
			$poster['image'] = $image;
		} else {
         	//$image = 'blank_news_img.jpg';
         	$image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		}

		$obj['poster'] = $image; 

		$qw=$_db->query("
			SELECT
				lng.title as `country`

			FROM `#__main_films_countries` AS `cnt`

			LEFT JOIN
				 `#__main_countries_lng` AS `lng`
			ON
				cnt.country_id=lng.record_id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE cnt.film_id=".intval($obj['id'])."
			LIMIT 1
		");



		while($object=$_db->fetchAssoc($qw))
		{
			$obj['country'] = $object['country'];

		}

		$qw=$_db->query("
			SELECT
				lng.title as `genre`

			FROM `#__main_films_genres` AS `cnt`

			LEFT JOIN
				 `#__main_genres_lng` AS `lng`
			ON
				cnt.genre_id=lng.record_id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE cnt.film_id=".intval($obj['id'])."
		");

        $obje['genres'] = array();

		while($object=$_db->fetchAssoc($qw))
		{
			$obje['genres'][] = $object['genre'];
		}

		$obj['genres'] = implode(', ', $obje['genres']);


        $object['actors'] = array();
        $obje['actors'] = array();

        $pi = 0;
			$obje['actors']=main_films::getFilmActors($obj['id']);

            if (is_array($obje['actors'][0]))
            {

	            for ($i=0;$i<=1;$i++)
	            {
	            	if ($obje['actors'][$i]['url'])
	            	{
						$object['actors'][] = '<a href="'.$obje['actors'][$i]['url'].'" title="'.$obje['actors'][$i]['fio'].'">'.$obje['actors'][$i]['fio'].'</a>';
						$pi++;
					}
	            }

            }

		if ($pi>0)
		{
			$obj['actors'] = sys::translate('main::actors').': '.implode(', ', $object['actors']);
		}

        $object['directors'] = array();
        $obje['directors'] = array();

        $di = 0;
			$obje['directors']=main_films::getFilmDirectors($obj['id']);


            if (is_array($obje['directors'][0]))
            {

	            for ($i=0;$i<1;$i++)
	            {
						$object['directors'][] = '<a href="'.$obje['directors'][$i]['url'].'" title="'.$obje['directors'][$i]['fio'].'">'.$obje['directors'][$i]['fio'].'</a>';
						$di++;
	            }

            }

		if ($di>0)
		{
			$obj['directors'] = sys::translate('main::directors').': '.implode(', ', $object['directors']);
		}

		$obj['third'] = array();
		if ($obj['directors'])
		$obj['third'][] = $obj['directors'];
		if ($obj['actors'])
		$obj['third'][] = $obj['actors'];

		if ($obj['directors'] || $obj['actors'])
		$obj['third_row'] = implode('<br>', $obj['third']);




		if(main_films::isFilmReviews($obj['id']))
		{
			$obj['revurl']=main_films::getFilmReviewsUrl($obj['id']);
		}

		if(main_films::isFilmPhotos($obj['id']))
		{
			$obj['photurl']=main_films::getFilmPhotosUrl($obj['id']);
		}

		if(main_films::isFilmPosters($obj['id']))
		{
			$obj['posturl']=main_films::getFilmPostersUrl($obj['id']);
		}

		if(main_films::isFilmTrailers($obj['id']))
		{
			$obj['trailurl']=main_films::getFilmTrailersUrl($obj['id']);
		}

		if($obj['votes']>5) // только при наличии 5 голосов после премьеры показывать реальный рейтинг	
			$obj['rating']=$obj['sum']/$obj['votes'];
		else // до 5 голосов показывать предварительный усредненный рейтинг
			$obj['rating']=($obj['pre_sum']+$obj['sum'])/($obj['pre_votes']+$obj['votes']);
		
		$obj['rate'] = sys::translate('main::rating').": <span id=span_rating_val_".$obj["id"]."><strong>".round($obj['rating'], 1).'</strong></span> / '.sys::translate('main::votes').": <span id=span_rating_votes_".$obj["id"].">".($obj['pre_votes']+$obj['votes']).'</span><br>';

		$obj['round'] = '';

		if ($obj['title_orig'])
		{
			$obj['round'] .= '<font style="color: #AAA; font-size: 10px;">'.$obj['title_orig'].'</font>';
		}

		if ($obj['year']>0)
		{
			$obj['year'] = $obj['year'];
		} else {
			$obj['year']='';
		}

		$obj['second'] = array();
		if ($obj['year'])
		$obj['second'][] = $obj['year'];
		if ($obj['country'])
		$obj['second'][] = $obj['country'];
		if ($obj['genres'])
		$obj['second'][] = $obj['genres'];

		if ($obj['year'] || $obj['country'] || $obj['genres'])
		$obj['second_row'] = implode(' | ', $obj['second']).'<br>';




		$obj['url']=main_films::getFilmUrl($obj['id']);
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['title_orig']=htmlspecialchars($obj['title_orig']);


		if ($obj['ukraine_premiere']>=$monday && $obj['ukraine_premiere']<=$friday)
		{
			$obj['class'] = 'thisWeek';
		}

		$obj['day'] = date('d', strtotime($obj['ukraine_premiere']))+0;
		$obj['month'] = sys::translate('main::month_'.(date('m', strtotime($obj['ukraine_premiere']))+0).'_a');
		$obj['date']=sys::db2Date($obj['ukraine_premiere'],false,$_cfg['sys::date_format']);
		$obj['dateY'] = substr($obj['date'],-4);
		$obj['dateD'] = substr($obj['date'],0,-5);
		
		$p_date=strtotime($obj['ukraine_premiere']?$obj['ukraine_premiere']:$obj['world_premiere']);
		$is_showing = ($p_date<time());
		
// данные для рейтингов
		if($_user['id']==2)
		{
			$ip = $_SERVER['REMOTE_ADDR'];
			$sid = session_id();
			$obj["expect"]["yourMark"] = $_db->getValue('main_films_expect_unregistered',"mark"   ,"film_id=".intval($obj["id"])." AND ip='$ip' AND sid='$sid'");
			$obj["stars"]['yourMark'] = $_db->getValue('main_films_rating_unregistered',$is_showing?"mark":'pre_mark',"film_id=".intval($obj["id"])." AND ip='$ip' AND sid='$sid'");
		}
		else 
		{
			$obj["expect"]['yourMark'] = $_db->getValue('main_films_expect',"mark"    ,"film_id=".intval($obj["id"])." AND user_id='".$_user['id']."'");
			$obj["stars"]['yourMark']  = $_db->getValue('main_films_rating',$is_showing?"mark":'pre_mark',"film_id=".intval($obj["id"])." AND user_id='".$_user['id']."'");
		}

// звезды
		if(!$obj["stars"]['yourMark'] ) $obj["stars"]['yourMark'] ="—";
		$obj["stars"]['rest']=$var['rating']-floor($var['rating']);
		$obj["stars"]['object_id']=$obj['id'];
		$obj["stars"]['rating']=main_films::formatFilmRating($obj['rating']);
		$obj["stars"]['ajx_type']='ajx_vote_film';
		$obj["stars"]['form_title']='film_rating'.($is_showing?"":'_waiting');

// Рейтинг ожидания		
		$obj["expect"]["voted"] = abs($obj["expect"]['yourMark']);
		
		if($obj["expect"]['yourMark']>0) $obj["expect"]['yourMark'] = sys::translate('main::yesExpect');
		else if($obj["expect"]['yourMark']<0) $obj["expect"]['yourMark'] = sys::translate('main::noExpect');
		else $obj["expect"]['yourMark']=sys::translate('main::youNotExpected');
		
		$obj["expect"]["film_id"] = $obj["id"];
		$obj["expect"]["total"] = 0+$obj["expect_yes"] + $obj["expect_no"];
		$obj["expect"]["yes"] = round(0+$obj["expect_yes"]*100/($obj["expect_yes"]+$obj["expect_no"]));
		$obj["expect"]["no"] = 100-$obj["expect"]["yes"];
			
		$result['objects'][]=$obj;
	}

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>