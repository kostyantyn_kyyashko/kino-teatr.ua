<?
function main_box_export()
{
	$ips=file(_SECURE_DIR.'export.txt');
	foreach ($ips as $ip)
	{
		$ip=explode('#',$ip);
		$ips2[]=trim($ip[0]);
	}
//	if(!in_array($_SERVER['REMOTE_ADDR'],$ips2))
//		return 404;
	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('main::persons');
	sys::useLib('main::reviews');
	sys::useLib('main::countries');
	sys::useLib('sys::form');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	if(!sys::checkAccess('main::bill_export'))
		return 403;

	$cache_name='main_bill_xml_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::RSS_feed_cache_period']*60))
	return unserialize($cache);
	$result=array();


	//Фильмы------------------------------------------------------------------

	$r=$_db->query("
		SELECT
			MAX(`id`) as box_id
		FROM
			`#__main_box_articles`
	");

		while ($obj=$_db->fetchAssoc($r))
		{
			$box_id = $obj['box_id'];
		}


	$r=$_db->query("
		SELECT
			box.real_id,
			box.film_id,
			box.position,
			box.money,
			box.screens,
			box.total,
			flm.year,
			flm.title_orig,
			flm.ukraine_premiere,
			lng.title
		FROM
			`#__main_box_positions` AS `box`
		LEFT JOIN
			`#__main_films` AS `flm`
		ON
			flm.id=box.film_id
		LEFT JOIN
			`#__main_films_lng` AS `lng`
		ON
			box.film_id=lng.record_id
		AND
			lng.lang_id=".$_cfg['sys::lang_id']."
		WHERE
			box.box_id='".$box_id."'
		ORDER BY
			box.position

	");


	$result['films']=array();
	{
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['week']=$obj['real_id'];
			$obj['pos']=$obj['position'];
			$obj['id']=$obj['film_id'];
			$obj['title-r']=$obj['title'];
			$obj['title-o']=$obj['title_orig'];
			$obj['year']=$obj['year'];
			$obj['date']=$obj['ukraine_premiere'];
			$obj['screens']=$obj['screens'];
			$obj['usd']=$obj['money'];
			$obj['total']=$obj['total'];

			$result['films'][$obj['film_id']]=$obj;
		}
	}
	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>