<?
function main_person()
{
	sys::useLib('main::persons');
	sys::useLib('main::users');
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('person_id','int');

	define('_CANONIC',main_persons::getPersonUrl($_GET['person_id']),true);

	if(!$_GET['person_id'])
		return 404;


	if($_REQUEST['editedtext'] && sys::checkAccess('main::persons'))
	{

		$r=$_db->query("
			UPDATE
				`#__main_persons_lng`
			SET
				biography = '".$_REQUEST['editedtext']."'

			WHERE
				record_id=".intval($_GET['person_id'])."
			AND
				lang_id = ".$_cfg['sys::lang_id']."
		");

	}

		define('_AUTH','1',true);

	$r=$_db->query("
		SELECT
			prs.lastname_orig,
			prs.firstname_orig,
			prs_lng.lastname,
			prs_lng.firstname,
			prs.birthdate,
			prs.deathdate,
			prs.rating,
			prs.rating_votes,
			prs_lng.biography,
			prs.comments AS `commentsNum`
		FROM `#__main_persons` AS `prs`

		LEFT JOIN `#__main_persons_lng` AS `prs_lng`
		ON prs_lng.record_id=prs.id
		AND prs_lng.lang_id=".$_cfg['sys::lang_id']."

		WHERE prs.id=".intval($_GET['person_id'])."
	");

	$result=$_db->fetchAssoc($r);


	if(!$result)
		return 404;

	main::countShow($_GET['person_id'],'person');


	$result['title']=htmlspecialchars($result['firstname'].' '.$result['lastname']);
	$result['title_orig']=htmlspecialchars($result['firstname_orig'].' '.$result['lastname_orig']);

	$result['fio']=$result['firstname'].' '.$result['lastname'];
	$result['fio_orig']=$result['firstname_orig'].' '.$result['lastname_orig'];

	$meta['person']=$result['firstname'].' '.$result['lastname'];
	$result['meta']=sys::parseModTpl('main::person','page',$meta);
	$result['rating_url']=main_persons::getPersonRatingUrl($_GET['person_id']);
	$result['rating']=main_persons::formatPersonRating($result['rating']);
	$result['discuss_url']=main_persons::getPersonDiscussUrl($_GET['person_id']);


	$result['biography'] = eregi_replace("<div[^>]*>", "<div>", $result['biography']);
	$result['biography'] = eregi_replace("<b[^>]*>", "<b>", $result['biography']);
	$result['biography'] = eregi_replace("<strong[^>]*>", "<strong>", $result['biography']);
	$result['biography'] = eregi_replace("<span[^>]*>", "", $result['biography']);
	$result['biography'] = eregi_replace("</span>", "", $result['biography']);
	$search = array ("'<span[^>]*?>.*?</span>'si");                    // интерпретировать как php-код
	$replace = array ("");
	$result['biography'] = eregi_replace($search, $replace, $result['biography']);
	$result['biography'] = str_replace("<div>&nbsp;</div>","",$result['biography']);
	$result['biography'] = str_replace('<div >&nbsp;</div>',"",$result['biography']);
	$result['biography'] = str_replace("<p>&nbsp;</p>","",$result['biography']);
	$result['biography'] = eregi_replace("<p[^>]*>","<p>",$result['biography']);
/*
//// Награды //kino-teatr.ua/person/de-niro-robert-928.phtml
	$q="
		SELECT
			flm_awd.id,
            flm_awd.film_id,
			flm_awd.year,
			awd_lng.title as nomination,
            flm_lng.title as `film`,
			awd_cat_lng.title AS `title`,
			flm_awd.result
		FROM `#__main_films_awards` AS `flm_awd`

		LEFT JOIN `#__main_awards_categories` AS `awd_cat`
		ON awd_cat.id=flm_awd.award_id

		LEFT JOIN `#__main_awards_categories_lng` AS `awd_cat_lng`
		ON awd_cat_lng.record_id=flm_awd.award_id
		AND awd_cat_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN `#__main_awards_lng` AS `awd_lng`
		ON awd_lng.record_id=awd_cat.award_id
		AND awd_lng.lang_id=".$_cfg['sys::lang_id']."


		LEFT JOIN `#__main_films_awards_persons` AS `awd_prs`
		ON awd_prs.film_award_id=flm_awd.id		

		LEFT JOIN `#__main_films_lng` AS `flm_lng`
		ON flm_lng.record_id=flm_awd.film_id 
        AND flm_lng.lang_id=".$_cfg['sys::lang_id']."

		WHERE awd_prs.person_id=".intval($_GET['person_id'])."
		ORDER BY 
        	flm_awd.year DESC, 
            awd_lng.title,
            flm_lng.title,
            awd_cat_lng.title
        ";
//if(sys::isDebugIP()) $_db->printR($q);		
	$r = $_db->query($q);
	while ($award=$_db->fetchAssoc($r)) {
		$award["url"] = main_films::getFilmUrl($award["film_id"]);
		$result["awards"][] = $award;
	}
*/	
////
	
	$photo=main_persons::getPersonFirstPhoto($_GET['person_id']);

	if($photo['image'])
	{
  	    $image="x2_".$photo['image']; 
		if(!file_exists($_cfg['main::persons_dir'].$image)) $image = $_cfg['sys::root_url'].'images/userpic.jpg';
		 else $image=$_cfg['main::persons_url'].$image;
	} else {
     	$image = $_cfg['sys::root_url'].'images/userpic.jpg';
	}
	$result['photo']['image'] = $image;

	if ($result['birthdate'])
	{
		$result['birthdate_iso']=$result['birthdate'];
		$result['birthdate']=sys::russianDate($result['birthdate']);
	}
		if ($result['deathdate'])
	{
		$result['deathdate_iso']=$result['deathdate'];
		$result['deathdate']=sys::russianDate($result['deathdate']);
	}
	$result['links']=main_persons::getPersonLinks($_GET['person_id']);

	
	//Постеры--------------------------------------------
	if(main_persons::isPersonPosters($_GET['person_id'])) 
	{
		$posters_url = main_persons::getPersonPostersUrl($_GET['person_id']);
		$q="
			SELECT pht.poster_id, 
				   pst.image,
				   flm_lng.title AS `film`,
				   flm_lng.record_id as film_id
				   
			FROM `#__main_persons_posters` as pht
	
			LEFT join `#__main_films_posters` AS `pst`
			ON pht.poster_id = pst.id
				
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=pst.film_id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			
			WHERE `person_id`=".intval($_GET['person_id'])."
			ORDER BY pht.poster_id DESC, flm_lng.record_id DESC
		";
		$r=$_db->query($q);
		$result['previews']=array();
		$filmIds = array();
		
		while ($obj=$_db->fetchAssoc($r))
		{
			if(sizeof($filmIds)>=4) break;
			if (in_array($obj["film_id"], $filmIds)) continue;
			$filmIds[] = $obj["film_id"];
			
	  	    $image="x2_".$obj['image'];
	  	    
			if(!file_exists($_cfg['main::films_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::films_url'].$image;
			$obj['image'] = $image;
	
			$obj["film"] = htmlspecialchars($obj["film"]);
	
			$obj['url']=$posters_url;
			
			$obj['break']=$i%4;
			$obj['i']=$i;
			$result['previews'][]=$obj;
			$i++;
		}		
	}
	
	
	//Комментарии--------------------------------------------
	$q="
		SELECT
			msg.id,
			msg.date,
			msg.user_id,
			msg.user_name,
			msg.rating,
			usr.login AS `user`,
			msg.text
		FROM `#__main_discuss_messages` AS `msg`

		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=msg.user_id

		WHERE msg.object_id='".intval($_GET['person_id'])."'
		AND msg.object_type='person'

		ORDER BY msg.date DESC

		LIMIT 0,5
	";

	$r=$_db->query($q);
	$result['comments']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true);
		if($obj['user_id']==2)
		{
			$obj['user']=$obj['user_name'];
			$obj['user_url']=false;
		}
		else
			$obj['user_url']=main_users::getUserUrl($obj['user_id']);
		$obj['bb_text']=sys::parseBBCode($obj['text']);
		$result['comments'][]=$obj;
	}

	if(sys::checkAccess('main::news_edit'))
		$result['edit_url']='ok';

	return $result;
}
?>