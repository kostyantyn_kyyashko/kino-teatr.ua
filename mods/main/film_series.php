<?
function main_film_series()
{
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');

	define('_CANONICAL',main_films::getFilmSeriesUrl($_GET['film_id']),true);

	if(!$_GET['film_id'])
		return 404;

	$result['film']=$_db->getValue('main_films','title',intval($_GET['film_id']),true);
	if(!$result['film'])
		return 404;

	main::countShow($_GET['film_id'],'film');
	
	
	$r=$_db->query("
		SELECT
			flm_lng.title as film,
			trk.film_id AS `tracking`
			
		FROM `#__main_films` AS `flm`

		LEFT JOIN `#__main_films_lng` AS `flm_lng`
		ON flm_lng.record_id=flm.id
		AND flm_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN `#__main_users_films_subscribe` AS `trk`
		ON trk.film_id=".intval($_GET['film_id'])."
		AND trk.user_id=".intval($_user['id'])."
		
		WHERE flm.id=".intval($_GET['film_id'])."
	");
	
	$result=$_db->fetchAssoc($r);
	
	
	//Отслеживание--------------------------------------------
	if(isset($_POST['cmd_tracking']))
	{
		main_films::trackingFilm($_GET['film_id']);
		sys::redirect(main_films::getFilmSeriesUrl($_GET['film_id']), false);
	}
	if(isset($_POST['cmd_untracking']))
	{
		main_films::untrackingFilm($_GET['film_id']);
		sys::redirect(main_films::getFilmSeriesUrl($_GET['film_id']), false);
	}
	//============================================================
	

	$result['serial']=$_db->getValue('main_films','serial',intval($_GET['film_id']),false);
	$meta['film']=$result['film'];
	$result['title'] = ($result["serial"]?sys::translate('main::series_serial'):sys::translate('main::series_film')).' '.$result['film'];
	$result['meta']=sys::parseModTpl('main::film_series','page',$meta);

	$q="
		SELECT
			id,
			date,
			season,
			seria,
			title
		FROM
			`#__main_films_serials_series` AS `series`

		WHERE
			series.film_id=".intval($_GET['film_id'])."

		ORDER BY
			season DESC, CONCAT(SUBSTRING(date,7,4),SUBSTRING(date,4,2),SUBSTRING(date,1,2)) DESC, seria desc
	";

	//if(sys::isDebugIP()) $_db->printR($q);
	
	$r=$_db->query($q);
	$result['objects']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		if(trim($obj['title'])=="") continue;
		$result['objects'][$obj["season"]][$obj['id']] = array(
													"date" => $obj['date'],
													"seria" => $obj['seria'],
													"title" => $obj['title']
													);
	}

	return $result;
}
?>