<?
function main_person_biography()
{
	sys::useLib('main::persons');
	sys::useLib('main::users');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('person_id','int');

	define('_CANONICAL',main_persons::getPersonUrl($_GET['person_id']),true);

	if(!$_GET['person_id'])
		return 404;

	if($_REQUEST['editedtext'] && sys::checkAccess('main::persons'))
	{

		$r=$_db->query("
			UPDATE
				`#__main_persons_lng`
			SET
				biography = '".$_REQUEST['editedtext']."'

			WHERE
				record_id=".intval($_GET['person_id'])."
			AND
				lang_id = ".$_cfg['sys::lang_id']."
		");

	}

	$r=$_db->query("
		SELECT
			prs.lastname_orig,
			prs.firstname_orig,
			prs_lng.lastname,
			prs_lng.firstname,
			prs.birthdate,
			prs.rating,
			prs.rating_votes,
			prs_lng.biography,
			prs.comments AS `commentsNum`
		FROM `#__main_persons` AS `prs`

		LEFT JOIN `#__main_persons_lng` AS `prs_lng`
		ON prs_lng.record_id=prs.id
		AND prs_lng.lang_id=".$_cfg['sys::lang_id']."

		WHERE prs.id=".intval($_GET['person_id'])."
	");

	$result=$_db->fetchAssoc($r);


	if(!$result)
		return 404;

	main::countShow($_GET['person_id'],'person');


	$result['title']=htmlspecialchars($result['firstname'].' '.$result['lastname']);
	$result['title_orig']=htmlspecialchars($result['firstname_orig'].' '.$result['lastname_orig']);

	$result['fio']=$result['firstname'].' '.$result['lastname'];
	$result['fio_orig']=$result['firstname_orig'].' '.$result['lastname_orig'];

	$meta['person']=$result['firstname'].' '.$result['lastname'];
	$result['meta']=sys::parseModTpl('main::person','page',$meta);
	$result['rating_url']=main_persons::getPersonRatingUrl($_GET['person_id']);
	$result['rating']=main_persons::formatPersonRating($result['rating']);
	$result['discuss_url']=main_persons::getPersonDiscussUrl($_GET['person_id']);



	$photo=main_persons::getPersonFirstPhoto($_GET['person_id']);

		if($photo['image'])
		{
			$image=$_cfg['main::persons_url'].$photo['image'];
			$result['photo']['url']==main_persons::getPhotoUrl($photo['id']);
		} else {
         	$image = 'blank_news_img.jpg';
		}

		$result['photo']['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=5';


	$result['birthdate']=sys::russianDate($result['birthdate']);
	$result['links']=main_persons::getPersonLinks($_GET['person_id']);

	//Комментарии--------------------------------------------
	$q="
		SELECT
			msg.id,
			msg.date,
			msg.user_id,
			msg.user_name,
			msg.rating,
			usr.login AS `user`,
			msg.text
		FROM `#__main_discuss_messages` AS `msg`

		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=msg.user_id

		WHERE msg.object_id='".intval($_GET['person_id'])."'
		AND msg.object_type='person'

		ORDER BY msg.date DESC

		LIMIT 0,5
	";

	$r=$_db->query($q);
	$result['comments']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true);
		if($obj['user_id']==2)
		{
			$obj['user']=$obj['user_name'];
			$obj['user_url']=false;
		}
		else
			$obj['user_url']=main_users::getUserUrl($obj['user_id']);
		$obj['bb_text']=sys::parseBBCode($obj['text']);
		$result['comments'][]=$obj;
	}

	return $result;
}
?>