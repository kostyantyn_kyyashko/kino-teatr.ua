<?
function main_imax_now()
{

	$xz=1;

function dmword($string, $is_cyrillic = true)
	{
		static $codes = array(
			'A' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1)),
				'U' => array(array(0, 7, -1))),
			'B' => array(array(7, 7, 7)),
			'C' => array(array(5, 5, 5), array(4, 4, 4),
				'Z' => array(array(4, 4, 4),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'Z' => array(array(4, 4, 4))),
				'K' => array(array(5, 5, 5), array(45, 45, 45)),
				'H' => array(array(5, 5, 5), array(4, 4, 4),
					'S' => array(array(5, 54, 54)))),
			'D' => array(array(3, 3, 3),
				'T' => array(array(3, 3, 3)),
				'Z' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'Z' => array(array(4, 4, 4))),
				'R' => array(
					'S' => array(array(4, 4, 4)),
					'Z' => array(array(4, 4, 4)))),
			'E' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1)),
				'U' => array(array(1, 1, -1))),
			'F' => array(array(7, 7, 7),
				'B' => array(array(7, 7, 7))),
			'G' => array(array(5, 5, 5)),
			'H' => array(array(5, 5, -1)),
			'I' => array(array(0, -1, -1),
				'A' => array(array(1, -1, -1)),
				'E' => array(array(1, -1, -1)),
				'O' => array(array(1, -1, -1)),
				'U' => array(array(1, -1, -1))),
			'J' => array(array(4, 4, 4)),
			'K' => array(array(5, 5, 5),
				'H' => array(array(5, 5, 5)),
				'S' => array(array(5, 54, 54))),
			'L' => array(array(8, 8, 8)),
			'M' => array(array(6, 6, 6),
				'N' => array(array(66, 66, 66))),
			'N' => array(array(6, 6, 6),
				'M' => array(array(66, 66, 66))),
			'O' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1))),
			'P' => array(array(7, 7, 7),
				'F' => array(array(7, 7, 7)),
				'H' => array(array(7, 7, 7))),
			'Q' => array(array(5, 5, 5)),
			'R' => array(array(9, 9, 9),
				'Z' => array(array(94, 94, 94), array(94, 94, 94)), // special case
				'S' => array(array(94, 94, 94), array(94, 94, 94))), // special case
			'S' => array(array(4, 4, 4),
				'Z' => array(array(4, 4, 4),
					'T' => array(array(2, 43, 43)),
					'C' => array(
						'Z' => array(array(2, 4, 4)),
						'S' => array(array(2, 4, 4))),
					'D' => array(array(2, 43, 43))),
				'D' => array(array(2, 43, 43)),
				'T' => array(array(2, 43, 43),
					'R' => array(
						'Z' => array(array(2, 4, 4)),
						'S' => array(array(2, 4, 4))),
					'C' => array(
						'H' => array(array(2, 4, 4))),
					'S' => array(
						'H' => array(array(2, 4, 4)),
						'C' => array(
							'H' => array(array(2, 4, 4))))),
				'C' => array(array(2, 4, 4),
					'H' => array(array(4, 4, 4),
						'T' => array(array(2, 43, 43),
							'S' => array(
								'C' => array(
									'H' => array(array(2, 4, 4))),
								'H' => array(array(2, 4, 4))),
							'C' => array(
								'H' => array(array(2, 4, 4)))),
						'D' => array(array(2, 43, 43)))),
				'H' => array(array(4, 4, 4),
					'T' => array(array(2, 43, 43),
						'C' => array(
							'H' => array(array(2, 4, 4))),
						'S' => array(
							'H' => array(array(2, 4, 4)))),
					'C' => array(
						'H' => array(array(2, 4, 4))),
					'D' => array(array(2, 43, 43)))),
			'T' => array(array(3, 3, 3),
				'C' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4))),
				'Z' => array(array(4, 4, 4),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'Z' => array(array(4, 4, 4)),
					'H' => array(array(4, 4, 4)),
					'C' => array(
						'H' => array(array(4, 4, 4)))),
				'T' => array(
					'S' => array(array(4, 4, 4),
						'Z' => array(array(4, 4, 4)),
						'C' => array(
							'H' => array(array(4, 4, 4)))),
					'C' => array(
						'H' => array(array(4, 4, 4))),
					'Z' => array(array(4, 4, 4))),
				'H' => array(array(3, 3, 3)),
				'R' => array(
					'Z' => array(array(4, 4, 4)),
					'S' => array(array(4, 4, 4)))),
			'U' => array(array(0, -1, -1),
				'E' => array(array(0, -1, -1)),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1))),
			'V' => array(array(7, 7, 7)),
			'W' => array(array(7, 7, 7)),
			'X' => array(array(5, 54, 54)),
			'Y' => array(array(1, -1, -1)),
			'Z' => array(array(4, 4, 4),
				'D' => array(array(2, 43, 43),
					'Z' => array(array(2, 4, 4),
						'H' => array(array(2, 4, 4)))),
				'H' => array(array(4, 4, 4),
					'D' => array(array(2, 43, 43),
						'Z' => array(
							'H' => array(array(2, 4, 4))))),
				'S' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'C' => array(
						'H' => array(array(4, 4, 4))))));
		$length = strlen($string);
		$output = '';
		$i = 0;
		$previous = -1;
		while ($i < $length)
		{
			$current = $last = &$codes[$string[$i]];
			for ($j = $k = 1; $k < 7; $k++)
			{
				if (!isset($string[$i + $k]) || !isset($current[$string[$i + $k]]))
					break;
				$current = &$current[$string[$i + $k]];
				if (isset($current[0]))
				{
					$last = &$current;
					$j = $k + 1;
				}
			}
			if ($i == 0)
				$code = $last[0][0];
			elseif (!isset($string[$i + $j]) || ($codes[$string[$i + $j]][0][0] != 0))
				$code = $is_cyrillic ? (isset($last[1]) ? $last[1][2] : $last[0][2]) : $last[0][2];
			else
				$code = $is_cyrillic ? (isset($last[1]) ? $last[1][1] : $last[0][1]) : $last[0][1];
			if (($code != -1) && ($code != $previous))
				$output .= $code;
			$previous = $code;
			$i += $j;
		}
		return str_pad(substr($output, 0, 6), 6, '0');
	}

//------------------------------------------------------------------------------

	function dmstring($string)
	{
		$is_cyrillic = false;
		if (preg_match('/[А-Яа-я]/iu', $string) === 1)
		{
			$string = translit($string);
			$is_cyrillic = true;
		}







		$firstword = '';
		$lastword = '';


		$words = explode(' ', $string);


		if (preg_match('/[A-Za-z]/iu', $words[0]) !== 1)
		{
			$firstword = $words[0];
		}

		if (preg_match('/[A-Za-z]/iu', $words[count($words)-1]) !== 1)
		{
			$lastword = $words[count($words)-1];
		}






		$string = preg_replace(array('/[^\w\s]|\d/iu', '/\b[^\s]{1,1}\b/iu', '/\s{1,}/iu', '/^\s+|\s+$/iu'), array('', '', ' '), strtoupper($string));


		if ($string[0])
		{
			$matches = explode(' ', $string);
		}  else {
			$matches = array();
		}


		foreach($matches as $key => $match)
			$matches[$key] = dmword($match, $is_cyrillic);


		if ($firstword)
		{
			array_unshift($matches, $firstword);
		}

		if ($lastword)
		{
			$matches[]=$lastword;
		}




		return $matches;
	}

//------------------------------------------------------------------------------

	function translit($string)
	{
		static $ru = array(
			'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
			'Ё', 'ё', 'Ж', 'ж', 'З', 'з', 'И', 'и', 'Й', 'й', 'К', 'к',
			'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',
			'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц',
			'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ', 'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь',
			'Э', 'э', 'Ю', 'ю', 'Я', 'я'
		);
		static $en = array(
			'A', 'a', 'B', 'b', 'V', 'v', 'G', 'g', 'D', 'd', 'E', 'e',
			'E', 'e', 'Zh', 'zh', 'Z', 'z', 'I', 'i', 'J', 'j', 'K', 'k',
			'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'R', 'r',
			'S', 's', 'T', 't', 'U', 'u', 'F', 'f', 'H', 'h', 'C', 'c',
			'Ch', 'ch', 'Sh', 'sh', 'Sch', 'sch', '\'', '\'', 'Y', 'y', '\'', '\'',
			'E', 'e', 'Ju', 'ju', 'Ja', 'ja'
		);
		$string = str_replace($ru, $en, $string);
		return $string;
	}

//------------------------------------------------------------------------------



	global $_db, $_cfg, $_err;
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	sys::filterGet('title');
	sys::filterGet('genre_id');
	sys::filterGet('year');
	sys::filterGet('letter');
	sys::filterGet('city_id');
	sys::filterGet('show');
	sys::filterGet('order_by','text','title.asc');


		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		define('_CANONICAL',_ROOT_URL.'films-near.phtml',true);
		} else {
		define('_CANONICAL',_ROOT_URL.$lang.'/films-near.phtml',true);
		}


	$cache_name='main_films_near_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::films_page_cache_period']*60))
		return unserialize($cache);

	$result=array();
	$result['meta']=sys::parseModTpl('main::films_near','page');

	//Формирование запроса-----------------------------------------------

		$date = date_create(date('Y-m-d'));
		$day = date_format($date, 'Y-m-d');

	$q="
		SELECT
			flm.id,
			flm.title_orig,
			flm.imax_id,
			lng.title,
			flm.imax_premiere ,
			flm.imax_page ,
			flm.world_premiere ,
			flm.ukraine_premiere ,
			flm.duration ,
			lng.intro,
			flm.year
		FROM `#__main_films` AS `flm`
		LEFT JOIN `#__main_films_lng` AS `lng`
		ON lng.record_id=flm.id
		AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			flm.imax_premiere!=''
		AND
			flm.imax_premiere<='".$day."'
		ORDER BY
			flm.imax_premiere ASC
	";



	$r=$_db->query($q);
	$result['objects']=array();

	while($obj=$_db->fetchAssoc($r))
	{

		$q="
			SELECT
				COUNT(*) as shows
			FROM `#__main_shows`
			WHERE
				end>='".$day."'
				AND (
				hall_id='215' OR hall_id='331' OR hall_id='360'
				) AND
				film_id = ".$obj['id']."
		";

	//	echo $q;

		$rq=$_db->query($q);

		while($obje=$_db->fetchAssoc($rq))
		{
				if ($obje['shows']>0)
				{
					$obj['imax']=1;
				} else {
					$obj['imax']=0;
				}
		}


		$qw=$_db->query("
			SELECT
				pst.image as `poster`

			FROM `#__main_films_posters` AS `pst`

			WHERE pst.film_id=".intval($obj['id'])."
			AND pst.order_number=1
			LIMIT 1
		");

		$poster = '';

	$obj['ukraine_premiere'] = sys::russianDate($obj['ukraine_premiere']);
	$obj['world_premiere'] = sys::russianDate($obj['world_premiere']);


	$obj['imax_premiere'] = 'с '.sys::russianDate2($obj['imax_premiere']);

	$obj['intro'] = ereg_replace("<([^>]*)(class|lang|style|size|face)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>","<\\1>",$obj['intro']);

	$obj['intro'] = str_replace("<div>&nbsp;</div>","",$obj['intro']);
	$obj['intro'] = str_replace('<div >&nbsp;</div>',"",$obj['intro']);

	$obj['intro'] = str_replace("<p>&nbsp;</p>","",$obj['intro']);
	$obj['details'] = main_films::getFilmUrl($obj['id']);

		while($object=$_db->fetchAssoc($qw))
		{
			$poster = $object['poster'];
		}



	 	$image = $_cfg['main::films_url'].$poster;
		$obj['poster'] = '//kino-teatr.ua/public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=14';


		$qw=$_db->query("
			SELECT
				lng.title as `country`

			FROM `#__main_films_countries` AS `cnt`

			LEFT JOIN
				 `#__main_countries_lng` AS `lng`
			ON
				cnt.country_id=lng.record_id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE cnt.film_id=".intval($obj['id'])."
			LIMIT 1
		");



		while($object=$_db->fetchAssoc($qw))
		{
			$obj['country'] = $object['country'];

		}




				$qt=$_db->query("
			SELECT
				trl.film_id,
				trl.id AS `trailer_id`,
				trl.image,
				trl.file,
				trl.url,
				trl.user_id,
				trl.shows,
				trl.width,
				trl.height,
				trl.order_number,
				trl.public,
				flm_lng.title AS `film`,
				flm_pst.image as `poster`
			FROM `#__main_films_trailers` AS `trl`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=trl.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			LEFT JOIN `#__main_films_photos` AS `flm_pst`
			ON flm_pst.film_id=trl.film_id
			WHERE trl.film_id='".intval($obj['id'])."'
			AND trl.public=1
			ORDER BY trl.order_number DESC
			LIMIT 0,1
		");




		while($object=$_db->fetchAssoc($qt))
		{


			$obj['file']=$_cfg['main::films_url'].$object['file'];

			$obj['trailerUrl'] = main_films::getFilmTrailersUrl($obj['id']);


/*			preg_match('#//kino-teatr.ua/public/main/films/(.*?).flv#is', $result['file'] , $img);

			if (!$img[1])
			{
				$img[1] = 'trailer_'.$object['trailer_id'];
			} */

			$posterimage = main_films::getBiggestPoster($obj['id']);

			$image = $_cfg['main::films_url'].$posterimage;

			$obj['post'] = '//kino-teatr.ua/public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=15';


		}


			if(main_films::isFilmTrailers($obj['id']))
					{

     		$q="
			SELECT
				trl.film_id,
				trl.id AS `trailer_id`,
				trl.image,
				trl.file,
				trl.url,
				trl.user_id,
				trl.shows,
				trl.width,
				trl.height,
				trl.order_number,
				trl.public,
				flm_lng.title AS `film`,
				flm_pst.image as `poster`
			FROM `#__main_films_trailers` AS `trl`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=trl.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			LEFT JOIN `#__main_films_photos` AS `flm_pst`
			ON flm_pst.film_id=trl.film_id
			WHERE trl.film_id='".intval($obj['id'])."'
			AND trl.public=1
			ORDER BY trl.order_number DESC
			LIMIT 1
		";


				$t=$_db->query($q);
				$trails=$_db->fetchAssoc($t);

				$obj['trfile'] = '//kino-teatr.ua/public/main/films/'.$trails['file'];
				$obj['trimage'] = $trails['image'];

				$obj['trailer']=true;
			}




		if ($_cfg['main::city_id'] == '4')
		{
         	$obj['imax_link'] = 'http://planeta-kino.com.ua/odessa/movies/'.$obj['imax_id'].'/#week';
		} else if ($_cfg['main::city_id'] == '9')
		{
         	$obj['imax_link'] = 'http://planeta-kino.com.ua/lvov/ua/movies/'.$obj['imax_id'].'/#week';
		} else {
         	$obj['imax_link'] = 'http://planeta-kino.com.ua/ua/movies/'.$obj['imax_id'].'/#week';
		}



		$qw=$_db->query("
			SELECT
				lng.title as `genre`

			FROM `#__main_films_genres` AS `cnt`

			LEFT JOIN
				 `#__main_genres_lng` AS `lng`
			ON
				cnt.genre_id=lng.record_id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE cnt.film_id=".intval($obj['id'])."
		");

        $obje['genres'] = array();

		while($object=$_db->fetchAssoc($qw))
		{
			$obje['genres'][] = $object['genre'];
		}

		$obj['genres'] = implode(', ', $obje['genres']);




        $object['actors'] = array();
        $obje['actors'] = array();

        $pi = 0;
			$obje['actors']=main_films::getFilmActors($obj['id']);

            if (is_array($obje['actors'][0]))
            {

	            for ($i=0;$i<=1;$i++)
	            {
	            	if ($obje['actors'][$i]['url'])
	            	{
						$object['actors'][] = $obje['actors'][$i]['fio'];
						$pi++;
					}
	            }

            }

		if ($pi>0)
		{
			$obj['actors'] = implode(', ', $object['actors']);
		}

        $object['directors'] = array();
        $obje['directors'] = array();

        $di = 0;
			$obje['directors']=main_films::getFilmDirectors($obj['id']);


            if (is_array($obje['directors'][0]))
            {

	            for ($i=0;$i<1;$i++)
	            {
						$object['directors'][] = $obje['directors'][$i]['fio'];
						$di++;
	            }

            }

		if ($di>0)
		{
			$obj['directors'] = implode(', ', $object['directors']);
		}

        $object['producers'] = array();
        $obje['producers'] = array();

        $di = 0;
			$obje['producers']=main_films::getFilmProducers($obj['id']);


            if (is_array($obje['producers'][0]))
            {

	            for ($i=0;$i<1;$i++)
	            {
						$object['producers'][] = $obje['producers'][$i]['fio'];
						$di++;
	            }

            }

		if ($di>0)
		{
			$obj['producers'] = implode(', ', $object['producers']);
		}

        $object['writists'] = array();
        $obje['writists'] = array();

        $di = 0;
			$obje['writists']=main_films::getFilmWritists($obj['id']);


            if (is_array($obje['writists'][0]))
            {

	            for ($i=0;$i<1;$i++)
	            {
						$object['writists'][] = $obje['writists'][$i]['fio'];
						$di++;
	            }

            }

		if ($di>0)
		{
			$obj['writists'] = implode(', ', $object['writists']);
		}

        $object['cameras'] = array();
        $obje['cameras'] = array();

        $di = 0;
			$obje['cameras']=main_films::getFilmCameras($obj['id']);


            if (is_array($obje['cameras'][0]))
            {

	            for ($i=0;$i<1;$i++)
	            {
						$object['cameras'][] = $obje['cameras'][$i]['fio'];
						$di++;
	            }

            }

		if ($di>0)
		{
			$obj['cameras'] = implode(', ', $object['cameras']);
		}

        $object['musics'] = array();
        $obje['musics'] = array();

        $di = 0;
			$obje['musics']=main_films::getFilmCameras($obj['id']);


            if (is_array($obje['musics'][0]))
            {

	            for ($i=0;$i<1;$i++)
	            {
						$object['musics'][] = $obje['musics'][$i]['fio'];
						$di++;
	            }

            }

		if ($di>0)
		{
			$obj['musics'] = implode(', ', $object['musics']);
		}



		$obj['third'] = array();
		if ($obj['directors'])
		$obj['third'][] = $obj['directors'];
		if ($obj['actors'])
		$obj['third'][] = $obj['actors'];

		if ($obj['directors'] || $obj['actors'])
		$obj['third_row'] = implode(', ', $obj['third']);







		if ($obj['rating']>0)
		{
			$obj['rate'] = sys::translate('main::rating').': <strong>'.round($obj['rating'], 1).'</strong> / '.sys::translate('main::votes').': '.$obj['votes'].'<br>';
		}


		$obj['round'] = '';

		if ($obj['title_orig'])
		{
			$obj['round'] .= '<br><font style="color: #AAA; font-size: 10px;">'.$obj['title_orig'].'</font>';
		}

		if ($obj['year']>0)
		{
			$obj['year'] = $obj['year'];
		} else {
			$obj['year']='';
		}

		$obj['second'] = array();
		if ($obj['year'])
		$obj['second'][] = $obj['year'];
		if ($obj['country'])
		$obj['second'][] = $obj['country'];
		if ($obj['genres'])
		$obj['second'][] = $obj['genres'];

		if ($obj['year'] || $obj['country'] || $obj['genres'])
		$obj['second_row'] = implode(' | ', $obj['second']).'<br><br>';




		$obj['url']=main_films::getFilmUrl($obj['id']);
		$obj['title']=htmlspecialchars($obj['title']);

/*
		if ($obj['title']=='Ледниковый период 4: Континентальный дрейф 3D')
		{			$obj['title'] = 'Ледниковый период 4';
		}

		if ($obj['title']=='Джон Картер: Между двумя мирами 3D')
		{
			$obj['title'] = 'Джон Картер';
		}

		if ($obj['title']=='Путешествие 2: Таинственный остров')
		{
			$obj['title'] = 'Путешествие 2';
		}

		if ($obj['title']=='Рожденные быть свободными')
		{
			$obj['title'] = 'Рожденные свободными';
		}   */


		$obj['title_orig']=htmlspecialchars($obj['title_orig']);
		$obj['dateY'] = substr($obj['date'],-4);
		$obj['dateD'] = substr($obj['date'],0,-5);
		$obj['i'] = $xz;
		if ($obj['imax']>0)
		{
			$result['objects'][]=$obj;
			$xz++;
		}
	}


	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>