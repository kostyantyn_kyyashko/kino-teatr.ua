<?
function main_user_profile_my()
{
	define('_NOINDEX','1');
	sys::useLib('main::users');
	sys::useLib('sys::form');
	sys::useLib('sys::pages');
	sys::useJs('sys::gui');
	sys::useLib('main::persons');
	sys::useLib('main::orders');
	sys::useLib('main::payment');

	sys::filterGet('order_by','text','title.asc');
	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;
	
	if($_user['id']==2)	return 401;
	
	$result['meta']=sys::parseModTpl('main::user_profile_my','page');

	$obj['url']=main_users::getUserUrl(intval($_user['id']));
	$obj['title']=sys::translate('main::own_page');
	$result['objects'][]=$obj;

	$obj['url']=main_persons::getPersonsUrl()."?user_id=".intval($_user['id']);
	$obj['title']=sys::translate('main::persons');
	$result['objects'][]=$obj;
	
	$obj['url']=main_orders::getUserUrl(intval($_user['id']));
	$obj['title']=sys::translate('main::orders');
	$result['objects'][]=$obj;
	
	$obj['url']=main_payment::getUserUrl(intval($_user['id']));
	$obj['title']=sys::translate('main::payment');
	$result['objects'][]=$obj;

	return $result;
}
?>