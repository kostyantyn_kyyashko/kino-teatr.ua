<?

function maxsite_str_word($text, $counttext = 10, $sep = ' ') {
	$words = split($sep, $text);
	if ( count($words) > $counttext )
		$text = join($sep, array_slice($words, 0, $counttext)).'...';
	return $text;
}


function main_search_news()
{
	sys::useLib('main::genres');
	sys::useLib('main::persons');
	sys::useLib('main::cinemas');
	sys::useLib('main::countries');
	sys::useLib('main::news');
	sys::useLib('sys::form');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('keyword');
	$result=array(0);

	$result['meta']=sys::parseModTpl('main::search','page');

	$_GET['keywords'] = strip_tags($_GET['keywords']);
	$_GET['keywords'] = urldecode($_GET['keyword']);


	$q="
		SELECT
			art.id,
			art.date,
			art.user_id,
			art.exclusive,
			art.small_image AS `image`,
			art.image AS `big_image`,
			art_lng.title,
			art_lng.intro
		FROM `#__main_news_articles` AS `art`

		LEFT JOIN
			`#__main_news_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_news` AS `nws`
		ON art.news_id=nws.id
	";


	if($_GET['keywords'])
		$q.="WHERE
			art_lng.title LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR art_lng.title LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR art_lng.title LIKE '%".mysql_real_escape_string($_GET['keywords'])."'

			";

	$q.="
		ORDER BY art.date DESC
	";

	$result['pages']=sys_pages::pocess($q,$_cfg['main::films_on_search']);


	$r=$_db->query($q.$result['pages']['limit']);



	while($obj=$_db->fetchAssoc($r))
	{

		$obj['round'] = '';

		if($obj['image'])
			$image=$_cfg['main::news_url'].$obj['image'];

		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$obj['image']));

		if ($wid<120 && $obj['big_image'])
		{
		 	$image = $_cfg['main::news_url'].$obj['big_image'];
		} else if ($wid>=120)
		{
		 	$image = $_cfg['main::news_url'].$obj['image'];
		} else {
         	$image = 'blank_news_img.jpg';
		}

		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=20';

		$obj['date']=sys::russianDate($obj['date']);

		$obj['intro'] = strip_tags($obj['intro']);

		$obj['url']=main_news::getArticleUrl($obj['id']);
		$result['news'][]=$obj;
	}


   	$result['films_title'] = sys::translate('main::films');
   	$result['persons_title'] = sys::translate('main::persons');
   	$result['cinemas_title'] = sys::translate('main::cinemas');
   	$result['news_title'] = sys::translate('main::news');
	return $result;
}
?>