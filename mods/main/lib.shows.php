<?php
class main_shows
{
	static function deleteShow($show_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_shows_times`
			WHERE `show_id`=".intval($show_id)."
		");
		while (list($time_id)=$_db->fetchArray($r))
		{
			self::deleteTime($time_id);
		}
		$_db->delete('main_shows',intval($show_id));
	}

	static function deleteTime($time_id)
	{
		global $_db;
		$_db->delete('main_shows_times',$time_id,true);
	}

	static function showShowTabs($show_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('sys::info');
		$tabs['info']['url']='?mod=main&act=show_edit&id='.$show_id;

		$tabs['times']['title']=sys::translate('main::show_times');
		$tabs['times']['url']='?mod=main&act=show_times_table&show_id='.$show_id;

		return sys_gui::showTabs($tabs, $active_tab);
	}

	static function getShowEditUrl($show_id)
	{
		return sys::rewriteUrl('?mod=main&act=show_edit&id='.$show_id);
	}

	static function getShowName($show_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT
				shw.begin,
				shw.end,
				flm.name AS `film`,
				cnm.name AS `cinema`,
				hls.name AS `hall`,
				cts.name AS `city`
			FROM
				`#__main_shows` AS `shw`

			LEFT JOIN `#__main_films` AS `flm`
				ON flm.id=shw.film_id

			LEFT JOIN `#__main_cinemas_halls` AS `hls`
				ON hls.id=shw.hall_id

			LEFT JOIN `#__main_cinemas` AS `cnm`
				ON cnm.id=hls.cinema_id

			LEFT JOIN `#__main_countries_cities` AS `cts`
				ON cts.id=cnm.city_id

			WHERE shw.id=".intval($show_id)."
 		");
		$name=$_db->fetchAssoc($r);
		return $name['film'].' - '.$name['cinema'].'::'.$name['hall'].' ('.$name['city'].') ['.sys::db2Date($name['begin'],false,$_cfg['sys::date_format']).'-'.sys::db2Date($name['end'],false,$_cfg['sys::date_format']).']';
	}

	static function checkShowDates($begin, $end, $film_id, $hall_id, $show_id=false)
	{
		global $_db, $_cfg;
		if(!$begin || !$end || !$film_id || !$hall_id)
			return false;


		if(sys::date2Timestamp($begin)>sys::date2Timestamp($end))
			return false;

		$begin=sys::date2Db($begin,false,$_cfg['sys::date_format']);
		$end=sys::date2Db($end,false,$_cfg['sys::date_format']);

		$q="
			SELECT `id` FROM `#__main_shows`
			WHERE
				`film_id`=".intval($film_id)." AND `hall_id`=".intval($hall_id)."
				AND
				(
					('".htmlspecialchars($begin)."' BETWEEN `begin` AND `end`)
					OR
					('".htmlspecialchars($end)."' BETWEEN `begin` AND `end`)
				)
		";

		if($show_id)
			$q.= " AND `id`!=".intval($show_id)."";
		$r=$_db->query($q);
		list($id)=$_db->fetchArray($r);
		if($id)
			return false;
		return true;
	}

	static function checkTimes()
	{
		for($i=0; $i<count($_POST['time']); $i++)
		{

			if($_POST['time'][$i])
			{
				$time=explode(':',$_POST['time'][$i]);
				if($time[0]<0 || $time[0]>23 || $time[1]<0 || $time[1]>59)
					return sys::translate('main::time').': '.sys::translate('main::invalid_format');
				if(!$_POST['prices'][$i])
					return sys::translate('main::prices_not_set');
			}
		}
	}

	static function insertTimes($show_id)
	{
		global $_db, $_langs;
		for($i=0; $i<count($_POST['time']); $i++)
		{
			if($_POST['time'][$i])
			{
				$_db->begin();
				if(!$_db->query("
					INSERT INTO `#__main_shows_times`
					(
						`id`,
						`show_id`,
						`time`,
						`prices`,
						`may3d`,
						`3d`
					)
					VALUES
					(
						'0',
						'".intval($show_id)."',
						'".mysql_real_escape_string($_POST['time'][$i])."',
						'".mysql_real_escape_string($_POST['prices'][$i])."',
						'".intval( isset($_POST["may3d"])?intval($_POST["may3d"]):0 )."',
						'".intval( (isset($_POST['3d']) && isset($_POST['3d'][$i]) && ($_POST['3d'][$i]=="on"))?1:0 )."'
					)
				"))
					return $_db->rollback();

				$time_id=$_db->last_id;
				foreach ($_langs as $lang_id=>$lang)
				{
					if(!$_db->query("
						INSERT INTO `#__main_shows_times_lng`
						(
							`record_id`,
							`lang_id`,
							`note`
						)
						VALUES
						(
							'".intval($time_id)."',
							'".intval($lang_id)."',
							'".mysql_real_escape_string($_POST['note_'.$lang['code']][$i])."'
						)
					"))
						return $_db->rollback();
				}
				$_db->commit();
			}
		}
	}

	static function showCalendar($days=35, $html_params=false)
	{
		global $_db, $_cfg;
		sys::filterGet('date');
		sys::filterGet('order','text','cinemas');
		$var['html_params']=$html_params;
		$time=time();
		$today=date('w',$time);
		if($today==0)
			$today=7;
		// $_GET['order']== "films" || "cinemas"
		if($_cfg['sys::lang']=='uk'):
		$calendar_url=_ROOT_URL.(($_GET['order']=="films")?"uk/afisha-kino-":"uk/kinoafisha-").$_GET['city'].'.phtml'.'?date=';
		else:
		$calendar_url=_ROOT_URL.(($_GET['order']=="films")?"afisha-kino-":"kinoafisha-").$_GET['city'].'.phtml'.'?date=';
		endif;
		$monday=$time-(($today-1)*_DAY);
		$today=date('d.m.Y',$time);
		$city=$_db->getValue('main_countries_cities','title_what',$_cfg['main::city_id'],true);

		for($i=0; $i<$days; $i++)
		{
			$date=date('d.m.Y',$monday+($i*_DAY));
			$day=intval(date('d',$monday+($i*_DAY)));

			if($date==$_GET['date'])
				$var['objects'][$date]['current']=true;
			else
				$var['objects'][$date]['current']=false;

			$var['objects'][$date]['day']=$day;
			if(($monday+($i*_DAY))>=$time)
				$var['objects'][$date]['url']=$calendar_url.$date;//sys::rewriteUrl('?mod=main&act=bill&order='.$_GET['order'].'&date='.$date);
			else
				$var['objects'][$date]['url']=false;
			$var['objects'][$date]['alt']=sys::translate('main::shows_schedule_in_cinemas').' '.$city.' '.sys::translate('main::_on').' '.intval(strftime('%d',$monday+($i*_DAY))).' '.sys::translate('main::month_'.strftime('%m',$monday+($i*_DAY)).'_a');
			$var['objects'][$date]['today']=($date==$today);

		}

		return sys::parseTpl('main::shows_calendar',$var);
	}

	static function getBillBoard($city_id, $date, $order='cinemas', $mincost, $maxcost, $mintime, $maxtime, $genre)
	{
		sys::useLib('main::cinemas');
		sys::useLib('main::films');
		sys::useLib('main::cards');
		global $_db, $_cfg, $_user;
		sys::filterGet('city_id','int',$_cfg['main::city_id']);

		$date=sys::date2Db($_GET['date'],false,'%d.%m.%Y');

		if(!in_array($order,array('films','cinemas')))
			$order='cinemas';


			if($_user['id']!=2)
			{

				$r2=$_db->query("
					SELECT *
					FROM `#__main_cinemas_hide`
					WHERE user_id='".$_user['id']."'
				");

				$cinemas=array();
				while ($cin=$_db->fetchAssoc($r2))
				{
					$cinemas[$cin['cinema_id']]=1;
				}

			} else {
				$cinemas = $_COOKIE['cinemas'];
			}

		$q="
			SELECT
				shw.begin,
				shw.end,
				shw.film_id,
				flm.title_orig,
				flm.rating,
				flm.pre_rating,
				flm.votes,
				flm.pre_votes,
				flm.year,
				flm.`3d` as `film_3d`,
				shw.id AS `show_id`,
				hls_lng.title AS `hall`,
				cnm_lng.title AS `cinema`,
				cnm.site,
				cnm.notice_begin,
				cnm.notice_end,
				cnm_lng.notice,
				cnm.ticket_url,
				cnm.phone,
				hls.cinema_id,
				hls.id AS `hall_id`,
				hls.scheme,
				hls.`3d`,
				flm_lng.title AS `film`,
				shw.hall_id,
				pst.image";

			if ($genre)
			{
					$q .= ",
					flm_gnr.genre_id as `genre`";
	  		}
			$q .= " FROM `#__main_shows` AS `shw`

			LEFT JOIN `#__main_cinemas_halls` AS `hls`
			ON hls.id=shw.hall_id

			LEFT JOIN `#__main_films` AS `flm`
			ON flm.id=shw.film_id

			LEFT JOIN `#__main_films_posters` AS `pst`
			ON pst.film_id=shw.film_id
			AND pst.order_number=1

			";


		if ($genre)
		{
		$q .= "

			LEFT JOIN `#__main_films_genres` AS `flm_gnr`
			ON flm.id=flm_gnr.film_id
			AND flm_gnr.genre_id = ".$genre."

		";
		}


			$q .= "LEFT JOIN `#__main_cinemas` AS `cnm`
			ON cnm.id=hls.cinema_id

			LEFT JOIN `#__main_cinemas_lng` AS `cnm_lng`
			ON cnm_lng.record_id=cnm.id
			AND cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng`
			ON hls_lng.record_id=hls.id
			AND hls_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=shw.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE shw.end>='".mysql_real_escape_string($date)."'
			AND shw.begin<='".mysql_real_escape_string($date)."'
			AND cnm.city_id=".intval($city_id)."
		";

		if($order=='cinemas')
		{
			$q.="
				ORDER BY cnm.order_number , shw.hall_id, hls.order_number, shw.begin
			";
		}
		else
		{
			$q.="
				ORDER BY flm.ukraine_premiere DESC, flm.rating DESC, flm.pre_rating DESC, cnm.order_number, shw.hall_id, hls.order_number, shw.begin
			";
		}

		$r=$_db->query($q);

		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$q = "
				SELECT tms.time, tms.prices, tms_lng.note, tms.3d as seans_3d, tms.sale_id, tms.sale_status
				FROM `#__main_shows_times` AS `tms`
				LEFT JOIN `#__main_shows_times_lng` AS `tms_lng`
				ON tms_lng.record_id=tms.id
				AND tms_lng.lang_id=".intval($_cfg['sys::lang_id'])."
				WHERE tms.show_id=".$obj['show_id']." AND
				RIGHT(tms.prices,3)>=".doubleval($mincost)." AND
				RIGHT(tms.prices,3)<=".doubleval($maxcost)." AND
				tms.time >= '".$mintime."' AND
				tms.time <= '".$maxtime."'
				ORDER BY tms.time
			";

			$r2=$_db->query($q);
//if(sys::isDebugIP() )
//{
//	$_db->printR($q);
//}
			$obj['prices']=array();
			while ($time=$_db->fetchAssoc($r2))
			{
				$time['past']=false;
				if($date==date('Y-m-d').' 00:00:00')
				{
					$timestamp=sys::date2Timestamp(date('Y-m-d').' '.$time['time'],'%Y-%m-%d %H:%M:%S');
					if($timestamp<time())
						$time['past']=true;
					else
						$time['past']=false;
				}


				$time['time']=sys::cutStrRight($time['time'],3);
//if(sys::isDebugIP() )
//{


				if($time['sale_id']!=0 and $time['past']==false)
				{
					$time['time'] = '<a href="http://bilet.kino-teatr.ua/showtime?theater='.$time['sale_status'].'&showtime='.$time['sale_id'].'&agent=kino-teatr" class="vkino-link">'.$time['time'].'</a>';
				}
				elseif($time['sale_status']!='' and $time['sale_id']==0 and $time['past']==false)
				{
					$temp_st=explode('|',$time['sale_status']);
					if($temp_st[0]=='imax' and $temp_st[1]!='')
					{
							$time['time'] = '<noindex><a rel="nofollow" href="'.$temp_st[1].'" onclick="goPartner(this, 1);" class="mult-link">'.$time['time'].'</a></noindex>';
						//	$time['time'] = '<a href="'.$link.'" class="mult-link">'.$time['time'].'</a>';
					}
					if($temp_st[0]=='mult' and $temp_st[1]!='')
					{
						$link = $temp_st[1];
						$time['time'] = '<a href="'.$temp_st[1].'" onclick="goPartner(this, 2);" class="mult-link">'.$time['time'].'</a>';
					}
					if($temp_st[0]=='mega')
					{
						$time['time'] = ''.$time['time'].'';
					}
				}
	//$_db->printR($time);
//}

				$time['url']=main_cards::getCardUrl($time['time'],$_GET['date'],$obj['film_id'],$obj['hall_id']);
				$prices=explode(',',$time['prices']);
				$obj['prices']=array_merge($obj['prices'],$prices);
				$obj['times'][]=$time;
			}




			$obj['prices']=array_unique($obj['prices']);
			sort($obj['prices']);
			$obj['price_range']=trim($obj['prices'][0]);
			if(count($obj['prices'])>1)
			{
				$obj['price_range'].='-'.trim($obj['prices'][count($obj['prices'])-1]);
			}

			if($obj['scheme'])
				$obj['scheme_url']=main_cinemas::getHallSchemeUrl($obj['hall_id']);
			else
				$obj['scheme_url']=false;

			$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
			$obj['cinema_url']=main_cinemas::getCinemaUrl($obj['cinema_id']);


			$obj['begin']=sys::db2Date($obj['begin'],false, $_cfg['sys::date_format']);
			$obj['end']=sys::db2Date($obj['end'],false, $_cfg['sys::date_format']);

			if($_GET['order']=='cinemas')
			{

				$result[$obj['cinema_id']]['title']=$obj['cinema'];
				if(strtotime($obj['notice_begin'])<time() && time()<strtotime($obj['notice_end']))
				{
					$result[$obj['cinema_id']]['notice']=$obj['notice'];
				}
				$result[$obj['cinema_id']]['phone']=$obj['phone'];
				$result[$obj['cinema_id']]['id']=$obj['cinema_id'];
				$result[$obj['cinema_id']]['site']=$obj['site'];

				/*if($obj['ticket_url'] && $_user['id']==2)
					$obj['ticket_url']=sys::rewriteUrl('?mod=main&act=register');*/
				$result[$obj['cinema_id']]['ticket_url']=$obj['ticket_url'];
				$result[$obj['cinema_id']]['shows_url']=main_cinemas::getCinemaShowsUrl($obj['cinema_id']);
				$result[$obj['cinema_id']]['url']=main_cinemas::getCinemaUrl($obj['cinema_id']);


				if ($cinemas[$obj['cinema_id']])
				{
					$result[$obj['cinema_id']]['hide']=1;
				} else {
					$result[$obj['cinema_id']]['hide']=0;
				}



				$result[$obj['cinema_id']]['halls'][$obj['hall_id']]['title']=$obj['hall'];
				$result[$obj['cinema_id']]['halls'][$obj['hall_id']]['3d']=$obj['3d'];
				$result[$obj['cinema_id']]['halls'][$obj['hall_id']]['scheme_url']=$obj['scheme_url'];
				$result[$obj['cinema_id']]['halls'][$obj['hall_id']]['url']=main_cinemas::getHallUrl($obj['cinema_id'],$obj['hall_id']);
				$result[$obj['cinema_id']]['halls'][$obj['hall_id']]['shows'][]=$obj;

				if (count($obj['times'])!=0)
					{
						$result[$obj['cinema_id']]['shows'][$obj['show_id']]=$obj;
						$result[$obj['cinema_id']]['countfilms']++;
					}
			}
			else
			{
				$res = array();

				if ($_GET['city_id']=='1')
				{
					$ro=$_db->query("
						SELECT
							count(*) as count
						FROM `#__main_shows`

						WHERE
							hall_id='215'
						AND
							begin<='".date('Y-m-d')."'
						AND
							end>='".date('Y-m-d')."'
						AND
							film_id='".$obj['film_id']."'
					");

					$imax = $_db->fetchAssoc($ro);

					if ($imax['count'])
					{
						$res['imax'] = 1;
						$res['imaxcinema'] = 135;
						$res['imaxlabel'] = 'imax';
					}
				}

				if ($_GET['city_id']=='9')
				{
					$ro=$_db->query("
						SELECT
							count(*) as count,
							hall_id
						FROM `#__main_shows`

						WHERE
							(hall_id='356'
						OR
							hall_id='357'
						OR
							hall_id='358'
						OR
							hall_id='359'
						OR
							hall_id='360'
						OR
							hall_id='361'
						OR
							hall_id='362'
							)
						AND
							begin<='".date('Y-m-d')."'
						AND
							end>='".date('Y-m-d')."'
						AND
							film_id='".$obj['film_id']."'
					");

					$imax = $_db->fetchAssoc($ro);

					if ($imax['count'])
					{
						$res['imax'] = 1;
						$res['imaxcinema'] = 207;
						$res['imaxlabel'] = 'planeta';
					}
				}

				if ($_GET['city_id']=='4')
				{
					$ro=$_db->query("
						SELECT
							count(*) as count,
							hall_id
						FROM `#__main_shows`

						WHERE
							(hall_id='331'
						OR
							hall_id='332'
						OR
							hall_id='333'
						OR
							hall_id='334'
						OR
							hall_id='335'
						OR
							hall_id='336'
						OR
							hall_id='337'
						OR
							hall_id='338'
						OR
							hall_id='339'
							)
						AND
							begin<='".date('Y-m-d')."'
						AND
							end>='".date('Y-m-d')."'
						AND
							film_id='".$obj['film_id']."'
					");

					$imax = $_db->fetchAssoc($ro);

					if ($imax['count'])
					{
						$res['imax'] = 1;
						$res['imaxcinema'] = 197;
						$res['imaxlabel'] = 'planeta';
					}
				}

				$result[$obj['film_id']]['title']=$obj['film'];
				$result[$obj['film_id']]['imax']=$res['imax'];
				$result[$obj['film_id']]['imaxcinema']=$res['imaxcinema'];
				$result[$obj['film_id']]['imaxlabel']=$res['imaxlabel'];
				$result[$obj['film_id']]['year']=$obj['year'];
				$result[$obj['film_id']]['id']=$obj['film_id'];
				$result[$obj['film_id']]['shows_url']=main_films::getFilmShowsUrl($obj['film_id']);
				$result[$obj['film_id']]['url']=main_films::getFilmUrl($obj['film_id']);
				$result[$obj['film_id']]['cinemas'][$obj['cinema_id']]['title']=$obj['cinema'];
				$result[$obj['film_id']]['cinemas'][$obj['cinema_id']]['phone']=$obj['phone'];
				$result[$obj['film_id']]['cinemas'][$obj['cinema_id']]['id']=$obj['cinema_id'];
				$result[$obj['film_id']]['cinemas'][$obj['cinema_id']]['site']=$obj['site'];
				$result[$obj['film_id']]['cinemas'][$obj['cinema_id']]['url']=main_cinemas::getCinemaUrl($obj['cinema_id']);
				$result[$obj['film_id']]['cinemas'][$obj['cinema_id']]['halls'][$obj['hall_id']]['title']=$obj['hall'];
				$result[$obj['film_id']]['cinemas'][$obj['cinema_id']]['halls'][$obj['hall_id']]['scheme_url']=$obj['scheme_url'];
				$result[$obj['film_id']]['cinemas'][$obj['cinema_id']]['halls'][$obj['hall_id']]['url']=main_cinemas::getHallUrl($obj['cinema_id'],$obj['hall_id']);
				$result[$obj['film_id']]['cinemas'][$obj['cinema_id']]['halls'][$obj['hall_id']]['shows'][]=$obj;

				$obj['countries']=main_films::getFilmCountriesTitles($obj['film_id']);
				$gen=main_films::getFilmGenresTitles($obj['film_id']);

				$second = array();
				if ($obj['year'])
				$second[] = $obj['year'];
				if ($obj['countries'])
				$second[] = implode(', ', $obj['countries']);
				if ($gen)
				$second[] = implode(', ', $gen);


				if ($obj['year'] || $obj['countries'] || $gen)
				$result[$obj['film_id']]['second_row'] = implode(' | ', $second).'<br>';


				if ($obj['rating']>0)
				{
					$obj['votes']=$obj['votes']+$obj['pre_votes'];
					$result[$obj['film_id']]['rate'] = sys::translate('main::rating').': <strong>'.round($obj['rating'], 1).'</strong> / '.sys::translate('main::votes').': '.$obj['votes'].'<br>';
				}

				$result[$obj['film_id']]['round']=htmlspecialchars($obj['title_orig']);

// comment by Mike
/*				if($obj['image'])
				{
					$image=$_cfg['main::films_url'].$obj['image'];
				} else {
		         	$image = 'blank_news_img.jpg';
				}

				$result[$obj['film_id']]['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=22';
*/
		// inserted by Mike begin
  	    $image="x1_".$obj['image'];
		if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$result[$obj['film_id']]['image'] = $image;
		// inserted by Mike end



				$object['actors'] = array();
    		    $obje['actors'] = array();

		        $pi = 0;
					$obje['actors']=main_films::getFilmActors($obj['film_id']);

		            if (is_array($obje['actors'][0]))
		            {

			            for ($i=0;$i<=1;$i++)
			            {
			            	if ($obje['actors'][$i]['url'])
			            	{
								$object['actors'][] = '<a href="'.$obje['actors'][$i]['url'].'" title="'.$obje['actors'][$i]['fio'].'">'.$obje['actors'][$i]['fio'].'</a>';
								$pi++;
							}
			            }

		            }

				if ($pi>0)
				{
					$obj['actors'] = sys::translate('main::actors').': '.implode(', ', $object['actors']);
				}

		        $object['directors'] = array();
		        $obje['directors'] = array();

		        $di = 0;
					$obje['directors']=main_films::getFilmDirectors($obj['film_id']);


		            if (is_array($obje['directors'][0]))
		            {

			            for ($i=0;$i<1;$i++)
			            {
								$object['directors'][] = '<a href="'.$obje['directors'][$i]['url'].'" title="'.$obje['directors'][$i]['fio'].'">'.$obje['directors'][$i]['fio'].'</a>';
								$di++;
			            }

		            }

				if ($di>0)
				{
					$obj['directors'] = sys::translate('main::directors').': '.implode(', ', $object['directors']);
				}

				$obj['third'] = array();
				if ($obj['directors'])
				$obj['third'][] = $obj['directors'];
				if ($obj['actors'])
				$obj['third'][] = $obj['actors'];

				if ($obj['directors'] || $obj['actors'])
				$result[$obj['film_id']]['third_row'] = implode('<br>', $obj['third']);



				if (count($obj['times'])!=0)
					{
						if (!$genre)
						{
							$result[$obj['film_id']]['shows'][$obj['show_id']]=$obj;
							$result[$obj['film_id']]['countfilms']++;
						} else {
							if($obj['genre'])
							{
								$result[$obj['film_id']]['shows'][$obj['show_id']]=$obj;
								$result[$obj['film_id']]['countfilms']++;
							}
						}
					}


			}
		}
		return $result;
	}


	static function getBillBoardMain($city_id, $date, $order='films')
	{
		sys::useLib('main::cinemas');
		sys::useLib('main::films');
		sys::useLib('main::cards');
		global $_db, $_cfg, $_user;

		$date=sys::date2Db($_GET['date'],false,'%d.%m.%Y');

		if(!in_array($order,array('films','cinemas')))
			$order='cinemas';

		$q="
			SELECT
				shw.film_id,
				flm_lng.title AS `film`,
				flm.ukraine_premiere,
				flm.year,
				pst.image
			FROM `#__main_shows` AS `shw`

			LEFT JOIN `#__main_cinemas_halls` AS `hls`
			ON hls.id=shw.hall_id

			LEFT JOIN `#__main_films` AS `flm`
			ON flm.id=shw.film_id

			LEFT JOIN `#__main_films_posters` AS `pst`
			ON pst.film_id=flm.id
			AND pst.order_number=1

			LEFT JOIN `#__main_cinemas` AS `cnm`
			ON cnm.id=hls.cinema_id

			LEFT JOIN `#__main_cinemas_lng` AS `cnm_lng`
			ON cnm_lng.record_id=cnm.id
			AND cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng`
			ON hls_lng.record_id=hls.id
			AND hls_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=shw.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE shw.end>='".mysql_real_escape_string($date)."'
			AND shw.begin<='".mysql_real_escape_string($date)."'
			AND cnm.city_id=".intval($city_id)."
		";


			$q.="
				GROUP BY flm.id ORDER BY flm.ukraine_premiere DESC, flm.rating DESC, flm.pre_rating DESC
			";


		$r=$_db->query($q);

		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$poster=main_films::getFilmFirstPoster($obj['film_id']);

			if($obj['image'])
			{
				$image=$_cfg['main::films_url'].$obj['image'];
			} else {
	         	$image = 'blank_news_img.jpg';
			}

			$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=28';

            if (date('Y-m-d', strtotime('- 7 days'))<$obj['ukraine_premiere'])
            {
            	$obj['premiere'] = 1;
            } else {
            	$obj['premiere'] = 0;
            }

            $obj['genres']=mb_strtolower(implode(', ',main_films::getFilmGenresTitles($obj['film_id'])));
			$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
			$obj['trailers']=main_films::getFilmTrailersUrl($obj['film_id']);
			$obj['shows']=main_films::getFilmShowsUrl($obj['film_id']);
			$result[]=$obj;
		}

		return $result;
	}

	static function getBillBoardMainList($city_id, $date, $order='films')
	{
		sys::useLib('main::cinemas');
		sys::useLib('main::films');
		sys::useLib('main::cards');
		global $_db, $_cfg, $_user;

		$date=sys::date2Db($_GET['date'],false,'%d.%m.%Y');

		if(!in_array($order,array('films','cinemas')))
			$order='cinemas';

		$q="
			SELECT
				shw.film_id,
				flm_lng.title AS `film`,
				flm.ukraine_premiere,
				flm.year,
				pst.image
			FROM `#__main_shows` AS `shw`

			LEFT JOIN `#__main_cinemas_halls` AS `hls`
			ON hls.id=shw.hall_id

			LEFT JOIN `#__main_films` AS `flm`
			ON flm.id=shw.film_id

			LEFT JOIN `#__main_films_posters` AS `pst`
			ON pst.film_id=flm.id
			AND pst.order_number=1

			LEFT JOIN `#__main_cinemas` AS `cnm`
			ON cnm.id=hls.cinema_id

			LEFT JOIN `#__main_cinemas_lng` AS `cnm_lng`
			ON cnm_lng.record_id=cnm.id
			AND cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng`
			ON hls_lng.record_id=hls.id
			AND hls_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=shw.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE shw.end>='".mysql_real_escape_string($date)."'
			AND shw.begin<='".mysql_real_escape_string($date)."'
			AND cnm.city_id=".intval($city_id)."
			AND flm.ukraine_premiere >= '".date('Y-m-d', strtotime('-14 days'))."'
			AND flm.ukraine_premiere <= '".date('Y-m-d', strtotime('+7 days'))."'
		";


			$q.="
				GROUP BY flm.id ORDER BY flm.ukraine_premiere DESC, flm.rating DESC, flm.pre_rating DESC
				LIMIT 5
			";

//if(sys::isDebugIP()) $_db->printR($q);

		$r=$_db->query($q);

		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$poster=main_films::getFilmFirstPoster($obj['film_id']);

// comment by Mike
/*				if($obj['image'])
				{
					$image=$_cfg['main::films_url'].$obj['image'];
				} else {
		         	$image = 'blank_news_img.jpg';
				}

			$obj['image'] = _ROOT_URL.'public/main/rescrop_4.php?f='.str_replace(_ROOT_URL,'',$image).'&t=25';
*/
		// inserted by Mike begin
  	    $image="x2_".$obj['image'];
		if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$obj['image'] = $image;
		// inserted by Mike end


            if (date('Y-m-d', strtotime('- 7 days'))<$obj['ukraine_premiere'])
            {
            	$obj['premiere'] = 1;
            } else {
            	$obj['premiere'] = 0;
            }

            $obj['genres']=mb_strtolower(implode(', ',main_films::getFilmGenresTitles($obj['film_id'])));
			$obj['film_url']=main_films::getFilmUrl($obj['film_id']);

			$obj['trailers']=main_films::getFilmTrailersUrl($obj['film_id']);
			$obj['shows']=main_films::getFilmShowsUrl($obj['film_id']);
			$obj['title'] = $obj['film'];
			$result[]=$obj;
		}

		return $result;
	}

	static function getGenresMain($city_id, $date, $order='films')
	{
		sys::useLib('main::cinemas');
		sys::useLib('main::films');
		sys::useLib('main::genres');
		sys::useLib('main::cards');
		global $_db, $_cfg, $_user;

		$date=sys::date2Db($_GET['date'],false,'%d.%m.%Y');

		if(!in_array($order,array('films','cinemas')))
			$order='cinemas';

		$q="
			SELECT
				shw.film_id
			FROM `#__main_shows` AS `shw`

			LEFT JOIN `#__main_cinemas_halls` AS `hls`
			ON hls.id=shw.hall_id

			LEFT JOIN `#__main_films` AS `flm`
			ON flm.id=shw.film_id

			LEFT JOIN `#__main_films_posters` AS `pst`
			ON pst.film_id=flm.id
			AND pst.order_number=1

			LEFT JOIN `#__main_cinemas` AS `cnm`
			ON cnm.id=hls.cinema_id

			LEFT JOIN `#__main_cinemas_lng` AS `cnm_lng`
			ON cnm_lng.record_id=cnm.id
			AND cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng`
			ON hls_lng.record_id=hls.id
			AND hls_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=shw.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE shw.end>='".mysql_real_escape_string($date)."'
			AND shw.begin<='".mysql_real_escape_string($date)."'
			AND cnm.city_id=".intval($city_id)."
		";


			$q.="
				GROUP BY flm.id
			";


		$r=$_db->query($q);

		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{

			$genres=main_films::getFilmGenresTitlesS($obj['film_id']);

			foreach ($genres as $genre)
			{


			$result[$genre['id']]['id'] = $genre['id'];
			$result[$genre['id']]['title'] = $genre['title'];
			$result[$genre['id']]['count']++;

			$arr[$genre['id']]++;

			}
		}

			arsort($arr);

			foreach ($arr as $key=>$value)
			{
				$res[$key]['id'] = $key;
				$res[$key]['title'] = $result[$key]['title'];
				$res[$key]['count'] = $value;
			}


		return $res;
	}

	static function getTodayFilm($city_id, $date, $order='cinemas')
	{
		sys::useLib('main::cinemas');
		sys::useLib('main::films');
		sys::useLib('main::cards');
		global $_db, $_cfg, $_user;

		if(!in_array($order,array('films','cinemas')))
			$order='cinemas';



		$q="
			SELECT
				shw.begin,
				flm.id as `filmid`,
				shw.end,
				shw.film_id,
				shw.id AS `show_id`,
				hls_lng.title AS `hall`,
				cnm_lng.title AS `cinema`,
				cnm.site,
				cnm.ticket_url,
				cnm.phone,
				hls.cinema_id,
				hls.id AS `hall_id`,
				hls.scheme,
				flm_lng.title AS `film`,
				pst.image,
				shw.hall_id
			FROM `#__main_shows` AS `shw`

			LEFT JOIN `#__main_cinemas_halls` AS `hls`
			ON hls.id=shw.hall_id

			LEFT JOIN `#__main_films` AS `flm`
			ON flm.id=shw.film_id

			LEFT JOIN `#__main_films_posters` AS `pst`
			ON pst.film_id=flm.id
			AND pst.order_number=1000


			LEFT JOIN `#__main_cinemas` AS `cnm`
			ON cnm.id=hls.cinema_id

			LEFT JOIN `#__main_cinemas_lng` AS `cnm_lng`
			ON cnm_lng.record_id=cnm.id
			AND cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng`
			ON hls_lng.record_id=hls.id
			AND hls_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=shw.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE shw.end>='".mysql_real_escape_string($date)."'
			AND shw.begin<='".mysql_real_escape_string($date)."'
			AND cnm.city_id=".intval($city_id)."
			AND pst.image!=''
		";
			$q.="
				GROUP BY flm.id ORDER BY RAND() LIMIT 1
			";


		$r=$_db->query($q);

		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			if($obj['image'])
			{
				$image=$_cfg['main::films_url'].$obj['image'];
			} else {
	         	$image = 'blank_news_img.jpg';
			}

			$obj['image'] = $image;
//			$obj['image'] = _ROOT_URL.'public/main/rescrop_4.php?f='.str_replace(_ROOT_URL,'',$image).'&t=21';




			$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
			$obj['cinema_url']=main_cinemas::getCinemaUrl($obj['cinema_id']);


			$obj['begin']=sys::db2Date($obj['begin'],false, $_cfg['sys::date_format']);
			$obj['end']=sys::db2Date($obj['end'],false, $_cfg['sys::date_format']);


				$result['image']=$obj['image'];
				$result['filmid']=$obj['filmid'];
		}
		return $result;
	}

	static function getTodayFilmsBot($city_id, $date, $order='films')
	{
		sys::useLib('main::cinemas');
		sys::useLib('main::films');
		sys::useLib('main::cards');
		global $_db, $_cfg, $_user;

		if(!in_array($order,array('films','cinemas')))
			$order='cinemas';



		$q="
			SELECT
				shw.begin,
				flm.id as `filmid`,
				shw.end,
				shw.film_id,
				shw.id AS `show_id`,
				hls_lng.title AS `hall`,
				cnm_lng.title AS `cinema`,
				cnm.site,
				cnm.ticket_url,
				cnm.phone,
				hls.cinema_id,
				hls.id AS `hall_id`,
				hls.scheme,
				flm_lng.title AS `film`,
				pst.image,
				shw.hall_id
			FROM `#__main_shows` AS `shw`

			LEFT JOIN `#__main_cinemas_halls` AS `hls`
			ON hls.id=shw.hall_id

			LEFT JOIN `#__main_films` AS `flm`
			ON flm.id=shw.film_id

			LEFT JOIN `#__main_films_posters` AS `pst`
			ON pst.film_id=flm.id
			AND pst.order_number=1000


			LEFT JOIN `#__main_cinemas` AS `cnm`
			ON cnm.id=hls.cinema_id

			LEFT JOIN `#__main_cinemas_lng` AS `cnm_lng`
			ON cnm_lng.record_id=cnm.id
			AND cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng`
			ON hls_lng.record_id=hls.id
			AND hls_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=shw.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE shw.end>='".mysql_real_escape_string($date)."'
			AND shw.begin<='".mysql_real_escape_string($date)."'
			AND cnm.city_id=".intval($city_id)."
			AND pst.image!=''
		";
			$q.="
				GROUP BY flm.id ORDER BY  flm.ukraine_premiere
			";


		$r=$_db->query($q);

		$var=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$var[$obj['filmid']]['url']=main_films::getFilmUrl($obj['filmid']);
			$var[$obj['filmid']]['film']=$obj['film'];
		}


		$result=sys::parseTpl('main::bot_now_er',$var);
		return $result;
	}
}
?>