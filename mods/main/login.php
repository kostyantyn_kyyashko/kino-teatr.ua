<?
function main_login()
{
	define('_NOINDEX',1);


	global $_db, $_cfg, $_err, $_user;

	sys::registerUser(2);
	sys::setCookie('sys::login');
	sys::setCookie('sys::password');
	$result['register_url']=sys::rewriteUrl('?mod=main&act=register');
	$result['forgot_url']=sys::rewriteUrl('?mod=main&act=forgot_pass');
	$result['meta']=sys::parseModTpl('main::login','page');

	$_POST['remember'] = 1;

	if(isset($_POST['login']))
	{

		$err=sys::authorizeUser($_POST['login'],$_POST['password']);
		if($err===true)
		{
			if($_POST['remember'])
			{
				sys::setCookie('sys::login',$_POST['login']);
				sys::setCookie('sys::password',md5($_POST['password']));
			}
			if($_POST['url'])
				$uri=sys::parseUri($_POST['url']);
			else
			{
				$uri['mod']='main';
				$uri['act']='login';
			}

			if ($_POST['return'])
			{
					sys::redirect('?mod=main&act=contest_article&article_id='.$_POST['return']);
			} else {
				if($uri['mod']=='main' && $uri['act']=='login') {
					sys::redirect('?mod=main&act=default');
				} else {
				    $pu = parse_url($_POST['url']);
                    $pu_ru = parse_url($_SERVER['REQUEST_URI']);
                    if ($pu['path'] == $pu_ru['path']) $u = '/';
                    else $u = $_POST['url'];
                    sys::redirect($u,false);
                }
			}
		}
		else
			$_err=sys::translate('sys::'.$err);
	} else {
	   setcookie('sys::attention_user_info', 0, time()-1, '/');
	}

	return $result;
}
?>