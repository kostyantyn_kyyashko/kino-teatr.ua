<?
function main_card()
{
	define('_NOINDEX',1);
	sys::useLib('main::cards');
	sys::useLib('main::cinemas');
	sys::useLib('main::films');
	sys::useLib('sys::check');
	sys::useLib('sys::form');

	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('time');
	sys::filterGet('date');
	sys::filterGet('film_id','int');
	sys::filterGet('hall_id','int');
	sys::filterGet('sent','int');

	if(!$_GET['film_id'] || !$_GET['time'] || !$_GET['date'] || !$_GET['hall_id'])
		return 404;

	$result['film']['url']=main_films::getFilmUrl($_GET['film_id']);
	$result['film']['title']=$_db->getValue('main_films','title',intval($_GET['film_id']),true);
	if(!$result['film']['title'])
		return 404;

	$cinema_id=$_db->getValue('main_cinemas_halls','cinema_id',intval($_GET['hall_id']));
	$result['cinema']['url']=main_cinemas::getCinemaUrl($cinema_id);
	$result['cinema']['title']=$_db->getValue('main_cinemas','title',intval($cinema_id),true);
	if(!$result['cinema']['title'])
		return 404;

	$result['date']=sys::date2Timestamp($_GET['date']);
	$result['date']=strftime('%A, %e %B',$result['date']);


	$result['meta']=sys::parseModTpl('main::card','page');

	//Получить открытки
	$r=$_db->query("
		SELECT
			`id`,
			`image`
		FROM
			`#__main_cards`
		ORDER BY
			`order_number`
	");
	$first_card=false;
	while ($obj=$_db->fetchAssoc($r))
	{
		if(!$first_card)
			$first_card=$obj['id'];
		$obj['src']=$_cfg['main::cards_url'].'x3_'.$obj['image'];
		$obj['url']=$_cfg['main::cards_url'].$obj['image'];
		$result['cards'][$obj['id']]=$obj;
	}

	if(!isset($_POST['card_id']))
		$_POST['card_id']=$first_card;

	if(!isset($_POST['text_align']))
		$_POST['text_align']='left';

	//recipient_name
	$fields['recipient_name']['input']='textbox';
	$fields['recipient_name']['type']='text';
	$fields['recipient_name']['req']=true;
	$fields['recipient_name']['max_chars']=255;
	$fields['recipient_name']['title']=sys::translate('main::recipient_name');

	//recipient_email
	$fields['recipient_email']['input']='textbox';
	$fields['recipient_email']['type']='email';
	$fields['recipient_email']['req']=true;
	$fields['recipient_email']['max_chars']=255;
	$fields['recipient_email']['title']=sys::translate('main::recipient_email');

	//sender_name
	$fields['sender_name']['input']='textbox';
	$fields['sender_name']['type']='text';
	$fields['sender_name']['req']=true;
	$fields['sender_name']['max_chars']=255;
	$fields['sender_name']['title']=sys::translate('main::your_name');

	//sender_email
	$fields['sender_email']['input']='textbox';
	$fields['sender_email']['type']='email';
	$fields['sender_email']['req']=true;
	$fields['sender_email']['max_chars']=255;
	$fields['sender_email']['title']=sys::translate('main::your_email');

	//text
	$fields['text']['input']='textarea';
	$fields['text']['type']='text';
	$fields['text']['req']=true;
	$fields['text']['max_chars']=1000;
	$fields['text']['title']=sys::translate('main::text');

	$fields=sys_form::parseFields($fields, $_POST, false);
	$result['fields']=$fields;
	$result['preview']=false;
	$result['preview_url']=sys::rewriteUrl('?mod=main&act=card_preview');

	if(isset($_POST['cmd_send']) || isset($_POST['cmd_preview']))
	{
		$_err=sys_check::checkValues($fields,$_POST);
		if(!$_err)
		{
			$var['recipient']=$_POST['recipient_name'];
			$var['recipient_email']=$_POST['recipient_email'];
			$var['sender']=$_POST['sender_name'];
			$var['sender_email']=$_POST['sender_email'];
			$var['invite_text']=nl2br(strip_tags($_POST['text']));
			$var['cinema']=$result['cinema']['title'];
			$var['cinema_url']=$result['cinema']['url'];
			$var['film']=$result['film']['title'];
			$var['film_url']=$result['film']['url'];
			$var['date']=$result['date'];
			$var['time']=$_GET['time'];

			$email=sys::parseModTpl('main::card','email',$var);
			unset($var);

			$var['src']=$result['cards'][$_POST['card_id']]['url'];
			$var['text']=$email['text'];
			$result['preview']=sys::parseTpl('main::card',$var);
			unset($var);

			$var['src']=$result['cards'][$_POST['card_id']]['image'];
			$var['text']=$email['text'];
			$email['text']=sys::parseTpl('main::card',$var);
			unset($var);

			$email['files'][]=$_cfg['main::cards_dir'].$result['cards'][$_POST['card_id']]['image'];

			if(isset($_POST['cmd_send']) && sys::sendMail(false,false,$_POST['recipient_email'],$_POST['recipient_name'],$email['subject'],false,$email['text'],$email['files']))
				sys::redirect('?mod=main&act=card&time='.$_GET['time'].'&date='.$_GET['date'].'&film_id='.$_GET['film_id'].'&hall_id='.$_GET['hall_id'].'&sent=1');

			if($_POST['text_align']=='left')
				$result['align']='right';
			elseif($_POST['text_align']=='right')
				$result['align']='left';
			else
				$result['align']='center';

		}
	}



	return $result;
}
?>