<?


function main_film_posts()
{

	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');
	sys::filterGet('poster_id','int');
	sys::filterGet('page','int');

	$cache_name=__FUNCTION__.'_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,_HOUR))
		return unserialize($cache);
	define('_CANONICAL',main_films::getFilmPostersUrl($_GET['film_id']),true);

	if(!$_GET['film_id'] && !$_GET['poster_id'])
		return 404;


		$q="
			SELECT
				pst.film_id,
				pst.id AS `poster_id`,
				pst.image,
				pst.user_id,
				pst.shows,
				pst.order_number,
				pst.public,
				flm_lng.title AS `film`,
				flm.serial,
				flm.name,
				flm.title_orig
								
			FROM `#__main_films_posters` AS `pst`

			LEFT JOIN `#__main_films` AS `flm`			
			ON flm.id=pst.film_id			

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=pst.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		";



	if($_GET['film_id'])
	{
		if($_GET['page'])
			$limit=intval($_cfg['main::images_on_page']*(intval($_GET['page'])-1));
		else
			$limit=0;
		$q.="
			WHERE pst.film_id='".intval($_GET['film_id'])."'
			AND pst.public=1
			ORDER BY pst.order_number ASC
			LIMIT ".$limit.",1
		";
	}



	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);

	if(!$result)
		return 404;

	if(!$result['public'] && !sys::checkAccess('main::films_posters_confirm'))
		return 404;

	main::countShow($result['film_id'],'film');

	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::films_posters_delete') || $result['user_id']==$_user['id'])
		{
			main_films::deletePoster($result['poster_id']);
			sys::redirect(main_films::getFilmPostersUrl($result['film_id']),false);
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::films_posters_confirm'))
	{
		main_films::confirmposter($result['poster_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}

	if($_cookie)
	{
		$_db->query("
			UPDATE `#__main_films_posters`
			SET `shows`=(`shows`+1)
			WHERE `id`=".doubleval($result['poster_id'])."
		");
	}
	
//	$result['serial']=$_db->getValue('main_films','serial',intval($_GET['film_id']),false);
	$meta['film']=$result['film'];
	$result['title'] = (($result['serial'])?sys::translate('main::posters_for_serial'):sys::translate('main::posters_for_film')).' '.$result['film'];

	
	$result['image']=$_cfg['main::films_url_new'].$result['image'].'&w=495';
	$result['next_url']=false;
	$result['prev_url']=false;
	$result['delete_poster']=false;
	$result['edit_url']=false;
	$result['add_url']=false;
	$result['url']=main_films::getposterUrl($result['poster_id']);

	$result['meta']=sys::parseModTpl('main::film_posters','page',$meta);



	//=============================================================================

	//Получить инфу об остальных фотках-----------------------------------------------
	$q="
		SELECT `id`,`image`
		FROM `#__main_films_posters`
		WHERE `film_id`=".$result['film_id']."
		AND `public`=1
		ORDER BY `order_number` ASC
	";
	$r=$_db->query($q);
	$result['previews']=array();

   	$i = 1;

	while ($obj=$_db->fetchAssoc($r))
	{
		//$from_path = '/var/www/html/multiplex/multiplex.in.ua/public/main/films/'.$obj['image']; by Mike
		$from_path = $_cfg['sys::root_dir'].'/public/main/films/'.$obj['image'];
		$image_info = getImageSize($from_path);

		$obj['width']=$image_info[0];
		$obj['height']=$image_info[1];
		if ($i!=1)
		{
			$obj['number']='- '.sys::translate('main::photo_number').' '.$i;
		}
		$obj['weight']=round(filesize($from_path)/1024);

  	    $image="x2_".$obj['image']; 
		if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$obj['image'] = $image;

		//$obj['url']=main_films::getPostersUrl($obj['id'],false);
		$obj['url']=sys::getHumanUrl($result['film_id'],$result['name'],"film_posters")."?poster_id=".$obj['id'];
		
		if($_GET['page'])
			$obj['url'].='&page='.$_GET['page'];
				$obj['break'] = $i%3;
//		$obj['url']=sys::rewriteUrl($obj['url']);
		$result['previews'][]=$obj;
		$i++;
	}
	//============================================================

	sys::setCache($cache_name,serialize($result));
	return $result;
}


?>