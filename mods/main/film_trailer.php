<?
function main_film_trailer()
{
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('trailer_id','int');

	if(!$_GET['trailer_id'])
		return 404;

		$q="
			SELECT
				trl.film_id,
				trl.file,
				trl.url,
				trl.width,
				trl.height,
				flm_lng.title AS `film`,
				flm_pst.image as `poster`
			FROM `#__main_films_trailers` AS `trl`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=trl.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			LEFT JOIN `#__main_films_photos` AS `flm_pst`
			ON flm_pst.film_id=trl.film_id
			WHERE trl.id=".intval($_GET['trailer_id'])."
			AND trl.public=1
		";

	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);


	if(!$result)
		return 404;

	if ($_GET['trailer_id'])
	{
		define('_CANONIC', main_films::getFilmTrailersUrl(intval($result['film_id'])));
	}
		
	main::countShow($result['film_id'],'film');

	$meta['film']=$result['film'];
	if(!$result['width'])
		$result['width']=770;
	if(!$result['height'])
		$result['height']=450;

	if($result['file'])
		$result['file']=$_cfg['main::films_url'].$result['file'];
	else
		$result['file']=$result['url'];


	preg_match('#http://kinoteatr.cdnvideo.ru/public/main/films/(.*?).flv#is', $result['file'] , $img);


	$result['post']=$img[1].'.jpg';

	$result['meta']=sys::parseModTpl('main::film_trailer','page',$meta);

	return $result;
}
?>