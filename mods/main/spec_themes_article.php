<?
function main_spec_themes_article()
{
	global $_db, $_cfg, $_err, $_user;

	sys::useLib('main::spec_themes');
	sys::useLib('main::news');
	sys::useLib('main::articles');
	sys::useLib('main::serials');
	sys::useLib('main::gossip');
	sys::useLib('main::interview');
	sys::useLib('main::reviews');
	sys::useLib('main::users');
	sys::useLib('sys::pages');

	sys::filterGet('spec_themes_id','int');
	if(!$_GET['spec_themes_id']) return 404;

	define('_CANONICAL',main_spec_themes::getArticleUrl($_GET['spec_themes_id']),true);

	$r=$_db->query("
		SELECT
			art.id,
			art_lng.title
		FROM `#__main_spec_themes` AS `art`				

		LEFT JOIN
			`#__main_spec_themes_lng` AS `art_lng`   
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE art.id=".intval($_GET['spec_themes_id'])
		);

	$result=$_db->fetchAssoc($r);

	if(!$result) return 404;

	if ($_GET['page'])
	{
		$meta['page'] = sys::translate('main::page').' '.$_GET['page'];
	} else {
		$meta['page'] = '';
	}

	$meta['article']=$result['title'];
	$result['meta']=sys::parseModTpl('main::spec_themes_article','page',$meta);
	//$result['meta']['title']=$result['title'];

	// Выборка данных из текустухи --------------------------------------------
	$q_news = "
		SELECT
			art.id,
			art.user_id,
			'news' as the_type, 
			art.date,
			art.total_shows,
			art.comments,
			art.exclusive,
			art.image,
			art_lng.title,
			art_lng.intro
		FROM `#__main_news_articles` AS `art`

		LEFT JOIN	`#__main_news_articles_lng` AS `art_lng`
		ON	art_lng.record_id=art.id AND art_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN	`#__main_news_articles_spec_themes` AS `st`
		ON	art.id=st.article_id 

		WHERE art.public AND st.spec_theme_id=".intval($_GET['spec_themes_id'])."
		";
	$q_articles = "
		SELECT
			art.id,
			art.user_id,
			'articles' as the_type, 
			art.date,
			art.total_shows,
			art.comments,
			art.exclusive,
			art.image,
			art_lng.title,
			art_lng.intro
		FROM `#__main_articles_articles` AS `art`

		LEFT JOIN	`#__main_articles_articles_lng` AS `art_lng`
		ON	art_lng.record_id=art.id AND art_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN	`#__main_articles_articles_spec_themes` AS `st`
		ON	art.id=st.article_id 

		WHERE art.public AND st.spec_theme_id=".intval($_GET['spec_themes_id'])."
		";
	$q_serials = "
		SELECT
			art.id,
			art.user_id,
			'serials' as the_type, 
			art.date,
			art.total_shows,
			art.comments,
			art.exclusive,
			art.image,
			art_lng.title,
			art_lng.intro
		FROM `#__main_serials_articles` AS `art`

		LEFT JOIN	`#__main_serials_articles_lng` AS `art_lng`
		ON	art_lng.record_id=art.id AND art_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN	`#__main_serials_articles_spec_themes` AS `st`
		ON	art.id=st.article_id 

		WHERE art.public AND st.spec_theme_id=".intval($_GET['spec_themes_id'])."
		";
	$q_gossip = "
		SELECT
			art.id,
			art.user_id,
			'gossip' as the_type, 
			art.date,
			art.total_shows,
			art.comments,
			art.exclusive,
			art.image,
			art_lng.title,
			art_lng.intro
		FROM `#__main_gossip_articles` AS `art`

		LEFT JOIN	`#__main_gossip_articles_lng` AS `art_lng`
		ON	art_lng.record_id=art.id AND art_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN	`#__main_gossip_articles_spec_themes` AS `st`
		ON	art.id=st.article_id 

		WHERE art.public AND st.spec_theme_id=".intval($_GET['spec_themes_id'])."
		";
	$q_interview = "
		SELECT
			art.id,
			art.user_id,
			'interview' as the_type, 
			art.date,
			art.total_shows,
			art.comments,
			art.exclusive,
			art.image,
			art_lng.title,
			art_lng.intro
		FROM `#__main_interview_articles` AS `art`

		LEFT JOIN	`#__main_interview_articles_lng` AS `art_lng`
		ON	art_lng.record_id=art.id AND art_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN	`#__main_interview_articles_spec_themes` AS `st`
		ON	art.id=st.article_id 

		WHERE art.public AND st.spec_theme_id=".intval($_GET['spec_themes_id'])."
		";
	$q_reviews = "
		SELECT
			art.id,
			art.user_id,
			'reviews' as the_type, 
			art.date,
			art.total_shows,
			art.comments,
			'' as exclusive,
			art.image,
			art.title,
			art.intro
		FROM `#__main_reviews` AS `art`

		LEFT JOIN	`#__main_reviews_articles_spec_themes` AS `st`
		ON	art.id=st.article_id 

		WHERE art.public AND st.spec_theme_id=".intval($_GET['spec_themes_id'])."
		";

	$q = "SELECT 
			T.*, 
			usr.login AS `user` 
			FROM (
					$q_news union $q_articles union $q_gossip union $q_interview  union $q_reviews union $q_serials
				  ) AS T 
			LEFT JOIN
				`#__sys_users` AS `usr`
			ON
				usr.id=T.user_id
		
		ORDER BY T.`date` DESC";

	$pu = main_spec_themes::getArticleUrl($_GET['spec_themes_id']);		
	if ($_GET['page'])	$qp = 'page='.$_GET['page'];
	else  $qp="";	
	
	$result['pages']=sys_pages::pocess($q,$_cfg['main::news_on_page'],false,false,false,true,'page',false);
	$result['pages']['first_page'] = sys_pages::setPageUrl($result['pages']['first_page'], $pu);
	$result['pages']['prev_page']  = sys_pages::setPageUrl($result['pages']['prev_page'],  $pu);
	$result['pages']['next_page']  = sys_pages::setPageUrl($result['pages']['next_page'],  $pu);
	$result['pages']['last_page']  = sys_pages::setPageUrl($result['pages']['last_page'],  $pu);
	foreach ($result['pages']['pages'] as $i=>$p)
	 $result['pages']['pages'][$i] = sys_pages::setPageUrl($result['pages']['pages'][$i],  $pu);
	
	$r=$_db->query($q.$result['pages']['limit']);

	$result['objects']=array();


	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::russianDate($obj['date']);
		$obj['title']=htmlspecialchars($obj['title']);

		$image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		switch ($obj['the_type'])
		{
			case 'news': 
						$obj['url']=main_news::getArticleUrl($obj['id']);
				  	    $image="x3_".$obj['image'];
						if(file_exists('./public/main/news/'.$image)) $image=$_cfg['main::news_url'].$image;
						$obj['the_type'] = sys::translate('main::'.$obj['the_type']);
						break;
			
			case 'articles':
						$obj['url']=main_articles::getArticleUrl($obj['id']);
				  	    $image="x3_".$obj['image'];
						if(file_exists('./public/main/articles/'.$image)) $image=$_cfg['main::articles_url'].$image;
						$obj['the_type'] = sys::translate('main::'.$obj['the_type']);
						break;

			case 'serials':
						$obj['url']=main_serials::getArticleUrl($obj['id']);
				  	    $image="x3_".$obj['image'];
						if(file_exists('./public/main/serials/'.$image)) $image=$_cfg['main::serials_url'].$image;
						$obj['the_type'] = sys::translate('main::'.$obj['the_type']);
						break;

			case 'gossip':
						$obj['url']=main_gossip::getArticleUrl($obj['id']);
				  	    $image="x3_".$obj['image'];
						if(file_exists('./public/main/gossip/'.$image)) $image=$_cfg['main::gossip_url'].$image;
						$obj['the_type'] = sys::translate('main::'.$obj['the_type']);
						break;

			case 'interview':
						$obj['url']=main_interview::getArticleUrl($obj['id']);
				  	    $image="x3_".$obj['image'];
						if(file_exists('./public/main/interview/'.$image)) $image=$_cfg['main::interview_url'].$image;
						$obj['the_type'] = sys::translate('main::'.$obj['the_type']);
						break;

			case 'reviews':
						$obj['url']=main_reviews::getReviewUrl($obj['id']);
				  	    $image="x3_".$obj['image'];
						if(file_exists('./public/main/reviews/'.$image)) $image=$_cfg['main::reviews_url'].$image;
						$obj['the_type'] = sys::translate('main::'.$obj['the_type']);
						break;

			default:	$obj['the_type'] = "";
		}		
		$obj['image'] = $image;
				
		$result['objects'][]=$obj;
	}

	return $result;
}
?>