<?
function main_sitemap_videos()
{

	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user, $_cookie;


	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/videos.txt";
	$fh = fopen($myFile, 'r');
	$theData = fread($fh, filesize($myFile));
	fclose($fh);



	$data = explode(':', $theData);



	$filmid = $data[0];
	$newxmlid = (int) $data[1]+1;


	$result=array();


	//Жанры----------------------------------------------------------------
	$r=$_db->query("
		SELECT
			a.*, flm.title, flm.intro, flm_ua.title as `ua_title`, flm_ua.intro as `ua_intro`, f.name
		FROM
			`#__main_films_trailers` a
		LEFT JOIN
			`#__main_films_lng` flm
		ON
			flm.record_id = a.film_id
		AND
			flm.lang_id = 1
		LEFT JOIN
			`#__main_films_lng` flm_ua
		ON
			flm_ua.record_id = a.film_id
		AND
			flm_ua.lang_id = 3
			
		LEFT JOIN `#__main_films` AS `f`
		ON f.id=a.film_id

		WHERE a.`id`>".$filmid."
		AND
			a.`public` = '1'
		AND
			a.image IS NOT NULL
		AND
			a.file != ''
		ORDER BY a.`id`
		LIMIT 500
	");




	$jj = 0;

	$result['cinemas']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$res = array();
//		$res['loc'] = main_films::getTrailerUrl($obj['id']);
//		$res['locuk'] = main_films::getTrailerUkUrl($obj['id']);
		$res['loc'] = sys::getHumanUrl($obj['film_id'],$obj['name'],"film-trailers","ru")."?trailer_id=".$obj['id']; 
		$res['locuk'] = sys::getHumanUrl($obj['film_id'],$obj['name'],"film-trailers","uk")."?trailer_id=".$obj['id']; 
		$res['image'] = '//kino-teatr.ua/public/main/films/'.$obj['image'];
		$res['title'] = $obj['title'].' - Трейлер №'.$obj['id'];
		$res['title_ua'] = $obj['ua_title'].' - Трейлер №'.$obj['id'];
		$res['description'] = htmlspecialchars(mb_substr(strip_tags($obj['intro']),0,1000));
		$res['description_ua'] = htmlspecialchars(mb_substr(strip_tags($obj['ua_intro']),0,1000));
		$res['file'] = '//kino-teatr.ua/public/main/films/'.$obj['file'];
		$res['duration'] = $obj['duration'];
		$res['shows'] = $obj['shows'];
		$res['date'] = date('Y-m-d', strtotime($obj['date']));
		$result['cinemas'][]=$res;
		$maxid = $obj['id'];
		$jj = 1;
	}


if ($jj>0)
{

	$xml = '';


	$xml .= '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">';

foreach ($result['cinemas'] as $cinema)
{
	$xml .= '

<url>
     <loc>'.$cinema['loc'].'</loc>
     <video:video>
       <video:thumbnail_loc>'.$cinema['image'].'</video:thumbnail_loc>
       <video:title>'.$cinema['title'].'</video:title>
       <video:description>'.$cinema['description'].'</video:description>
       <video:content_loc>'.$cinema['file'].'</video:content_loc>';

       if ($cinema['duration']>0)
       $xml .= '
       <video:duration>'.$cinema['duration'].'</video:duration>';

       $xml .= '
       <video:view_count>'.$cinema['shows'].'</video:view_count>
       <video:publication_date>'.$cinema['date'].'</video:publication_date>
       <video:family_friendly>yes</video:family_friendly>
       <video:live>no</video:live>
     </video:video>
</url> 	 ';


}

	$xml .= '
	</urlset>';




	$xmluk = '';


	$xmluk .= '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"  xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">';

foreach ($result['cinemas'] as $cinema)
{

	$xmluk .= '

<url>
     <loc>'.$cinema['locuk'].'</loc>
     <video:video>
       <video:thumbnail_loc>'.$cinema['image'].'</video:thumbnail_loc>
       <video:title>'.$cinema['title_ua'].'</video:title>
       <video:description>'.$cinema['description_ua'].'</video:description>
       <video:content_loc>'.$cinema['file'].'</video:content_loc>';

       if ($cinema['duration']>0)
       $xmluk .= '
       <video:duration>'.$cinema['duration'].'</video:duration>';

       $xmluk .= '
       <video:view_count>'.$cinema['shows'].'</video:view_count>
       <video:publication_date>'.$cinema['date'].'</video:publication_date>
       <video:family_friendly>yes</video:family_friendly>
       <video:live>no</video:live>
     </video:video>
</url> 	 ';


}

	$xmluk .= '
	</urlset>';


	$theText = $maxid.':'.$newxmlid;

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/videos.txt";
	$fh = fopen($myFile, 'w') or die("can't open file1");
	fwrite($fh, $theText);
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/videos_".$newxmlid.".xml";
	$fh = fopen($myFile, 'w') or die("can't open file2");
	fwrite($fh, $xml);
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/videos_uk_".$newxmlid.".xml";
	$fh = fopen($myFile, 'w') or die("can't open file3");
	fwrite($fh, $xmluk);
	fclose($fh);


	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/videos.xml";
	$fh = fopen($myFile, 'r');
	$filmsXML = fread($fh, filesize($myFile));
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/videos_uk.xml";
	$fh = fopen($myFile, 'r');
	$filmsXMLUk = fread($fh, filesize($myFile));
	fclose($fh);

	$filmsXML = str_replace('</sitemapindex>','',$filmsXML);

	$filmsXML .= '
	<sitemap>
    <loc>//kino-teatr.ua/public/main/cards/videos_'.$newxmlid.'.xml</loc>
    <lastmod>'.date('Y-m-d').'</lastmod>
  </sitemap>
</sitemapindex>';

	$filmsXMLUk = str_replace('</sitemapindex>','',$filmsXMLUk);

	$filmsXMLUk .= '
	<sitemap>
    <loc>//kino-teatr.ua/public/main/cards/videos_uk_'.$newxmlid.'.xml</loc>
    <lastmod>'.date('Y-m-d').'</lastmod>
  </sitemap>
</sitemapindex>';


	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/videos.xml";
	$fh = fopen($myFile, 'w') or die("can't open file4");
	fwrite($fh, $filmsXML);
	fclose($fh);

	$myFile = "/var/www/html/multiplex/multiplex.in.ua/public/main/cards/videos_uk.xml";
	$fh = fopen($myFile, 'w') or die("can't open file5");
	fwrite($fh, $filmsXMLUk);
	fclose($fh);

}

	exit;

	//return $result;
}
?>