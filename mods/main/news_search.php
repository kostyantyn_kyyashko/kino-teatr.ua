<?
function main_news_search()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::news');
	sys::useLib('main::users');
	sys::useLib('sys::pages');
	sys::useLib('sys::form');
	
	sys::filterGet('keywords');
	$_GET['keywords'] = strip_tags($_GET['keywords']);
		
	$result['meta']=sys::parseModTpl('main::news_search','page');
	$result['title']=sys::translate('main::news_search');
	
	//Поля поисковой формы-------------------------------------
	$result['fields']['keywords']['input']='textbox';
	$result['fields']['keywords']['value']=urldecode($_GET['keywords']);
	$result['fields']['keywords']['title']=sys::translate('main::keywords');
	//===================================================================
	$_GET['keywords']=trim($_GET['keywords']);
	if($_GET['keywords'])
	{
		$q="
			SELECT
				art_lng.title,
				idx.text,
				idx.article_id
			FROM
				`#__main_news_index` AS `idx`
				
			LEFT JOIN
				`#__main_news_articles_lng` AS `art_lng`
			ON
				art_lng.record_id=idx.article_id
			AND
				art_lng.lang_id=".intval($_cfg['sys::lang_id'])."
				
			WHERE
				MATCH (idx.text) AGAINST ('".mysql_real_escape_string(urldecode($_GET['keywords']))."')
			AND 
				idx.lang_id=".intval($_cfg['sys::lang_id'])."
			ORDER BY
				idx.article_id DESC
		";
		
		$result['pages']=sys_pages::pocess($q,$_cfg['main::news_on_page']);
		$r=$_db->query($q.$result['pages']['limit']);
		$result['objects']=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=main_news::getArticleUrl($obj['article_id']);
			$obj['text']=mb_substr($obj['text'],mb_strlen($obj['title']));
			$obj['intro']=sys::markKeywords($obj['text'],urldecode($_GET['keywords']));
			$result['objects'][]=$obj;
		}
	
	}
	return $result;
}
?>