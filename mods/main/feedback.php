<?
function main_feedback()
{
	define('_NOINDEX','1');
	sys::useLib('main::users');
	sys::useLib('main::questions');
	sys::useLib('main::captcha');
	sys::useLib('sys::form');
	sys::useLib('sys::langs');
	sys::useLib('main::countries');
	sys::useLib('sys::check');
	sys::useJs('sys::gui');
	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;
	
	$result['meta']=sys::parseModTpl('main::feedback','page');

	//email
	$fields['email']['input']='textbox';
	$fields['email']['value']=($_user['id']!=2)?$_user['email']:"";
	$fields['email']['type']='email';
	$fields['email']['req']=true;
	$fields['email']['title']=sys::translate('main::email');

	//nickname
	$fields['nickname']['input']='textbox';
	$fields['nickname']['type']='text';
	$fields['nickname']['value']=($_user['id']!=2)?($_user['main_nick_name']?$_user['main_nick_name']:($_user["main_last_name"]." ".$_user["main_first_name"])):"";
	$fields['nickname']['req']=true;
	$fields['nickname']['title']=sys::parseModTpl('main::nickname');

	//feedback_theme
	$fields['feedback_theme']['input']='selectbox2';
	$fields['feedback_theme']['type']='text';
	$fields['feedback_theme']['empty']='';
	$fields['feedback_theme']['req']=true;
	$fields['feedback_theme']['title']=sys::translate('main::feedback_theme_title');
	$fields['feedback_theme']['values']['feedback_theme_1']=sys::translate('main::feedback_theme_1');
	$fields['feedback_theme']['values']['feedback_theme_2']=sys::translate('main::feedback_theme_2');
	$fields['feedback_theme']['values']['feedback_theme_3']=sys::translate('main::feedback_theme_3');
	$fields['feedback_theme']['values']['feedback_theme_4']=sys::translate('main::feedback_theme_4');
	$fields['feedback_theme']['values']['feedback_theme_5']=sys::translate('main::feedback_theme_5');
	$fields['feedback_theme']['values']['feedback_theme_6']=sys::translate('main::feedback_theme_6');
	
	//text
	$fields['text']['input']='textarea';
	$fields['text']['type']='text';
	$fields['text']['req']=true;
	$fields['text']['title']=sys::translate('main::text');

	$fields=sys_form::parseFields($fields, $_POST, false);

	if($_user['id']==2) $result['question']=main_questions::getQuestion();
	$result['fields']=$fields;
	$result['registred']=false;

	if(isset($_POST['cmd_feedback']))
	{
		$_err=sys_check::checkValues($fields,$_POST);

		if(mb_strlen($_POST['text'])>2000) {
            $_err['text'] = array(
                'title' => sys::translate('main::text'),
                'text' => sys::translate('main::wrong_feedback_text_length').mb_strlen($_POST['text'])
            );
        }
		
        if($_user['id']==2){
			/*
			if(!main_questions::checkAnswer($_POST['question_id'],$_POST['answer'])) {
	            $_err['question'] = array(
	                'title' => sys::translate('main::control_question'),
	                'text' => sys::translate('main::wrong_control_answer')
	            );
	        }
			*/
			if(!main_captcha::check()) {
            	$_err['captcha'] = array(
                	'title' => sys::translate('main::captcha'),
                	'text' => sys::translate('main::wrong_captcha')
            	);
        	}
		}
			
        //	var_dump($_err);	die;
		if(!$_err)
		{
			// Отправить письмо
			//addMail($from_email,$from_name,$to_email,$to_name,$subject, $text=false, $html=false, $att_files=false, $charset=false, $date=false, $method='sendMail')
			sys::addMail ($_POST['email'],$_POST['nickname'],$_cfg['sys::from_email'],sys::translate('main::'.$_POST['feedback_theme']),sys::translate('main::feedback_subject').sys::translate('main::'.$_POST['feedback_theme']),$_POST['text']);
			
			$result['registred']=true;
		}		
	}

	return $result;
}
?>