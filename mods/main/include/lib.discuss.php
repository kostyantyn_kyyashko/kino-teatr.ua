﻿<?php
class main_discuss
{
	static function deleteMessage($message_id)
	{
		global $_db;
		$_db->delete('main_discuss_messages',intval($message_id));
	}

	static function deleteDiscuss($object_id, $object_type)
	{
		global $_db;
		$_db->query("
			DELETE FROM `#__main_discuss_messages`
			WHERE `object_id`=".intval($object_id)."
			AND `object_type`='".mysql_real_escape_string($object_type)."'
		");
	}

	static function subscribeDiscuss($user_id, $object_id, $object_type)
	{
		global $_db;
		$_db->query("
			INSERT INTO `#__main_users_discuss_subscribe`
			(
				`user_id`,
				`object_id`,
				`object_type`,
				`date`
			)
			VALUES
			(
				'".intval($user_id)."',
				'".intval($object_id)."',
				'".mysql_real_escape_string($object_type)."',
				'".gmdate('Y-m-d H:i:s')."'
			)
		");
	}

	static function unsubscribeDiscuss($user_id, $object_id, $object_type)
	{
		global $_db;
		$_db->query("
			DELETE FROM `#__main_users_discuss_subscribe`
			WHERE `user_id`=".intval($user_id)."
			AND `object_id`=".intval($object_id)."
			AND `object_type`='".mysql_real_escape_string($object_type)."'
		");
	}

	static function getObjectTableName($object_type)
	{
		switch ($object_type)
		{
			case 'film':
				return "#__main_films";
			break;

			case 'person':
				return "#__main_persons";
			break;

			case 'review':
				return "#__main_reviews";
			break;

			case 'cinema':
				return "#__main_cinemas";
			break;

			case 'serials_article':
				return "#__main_serials_articles";
			break;

			case 'news_article':
				return "#__main_news_articles";
			break;


			case 'gossip_article':
				return "#__main_gossip_articles";
			break;

			case 'interview_article':
				return "#__main_interview_articles";
			break;

			case 'articles_article':
				return "#__main_articles_articles";
			break;
			
			case 'contest_article':
				return "#__main_contest_articles";
			break;
		}
	}

	static function plusComment($object_type, $object_id)
	{
		global $_db;
		$table=self::getObjectTableName($object_type);


		if($table)
		{
			$_db->query("
				UPDATE `".$table."`
				SET `comments`=`comments`+1
				WHERE `id`=".intval($object_id)."
			");
		}
	}

	static function minusComment($object_type, $object_id)
	{
		global $_db;
		$table=self::getObjectTableName($object_type);
		if($table)
		{
			$_db->query("
				UPDATE `".$table."`
				SET `comments`=`comments`-1
				WHERE `id`=".intval($object_id)."
			");
		}
	}

	// $chpu_base_url by Mike
	// при появлении урл вида http://kino-teatr.ua/film/hunger-games-mockingjay-part-1-25821.phtml
	// дискусии остались http://kino-teatr.ua/ru/main/film/film_id/25821/page/3.phtml
	// Параметр $chpu_base_url = новому параметру и если он есть, то алгоритм должен сделать http://kino-teatr.ua/film/hunger-games-mockingjay-part-1-25821.phtml?page=3
	static function showDiscussMain($object_id, $object_type, $chpu_base_url=false)
	{
		if(!$object_id) return "";
		
		global $_db, $_cfg, $_user;
		sys::useLib('main::users');
		sys::useLib('sys::pages');
		sys::useLib('main::questions');
		sys::useLib('main::captcha');
		
		if($_user['id']==2)
			$var['question']=main_questions::getQuestion();

		//Подписка на дисскусию
		if(isset($_POST['cmd_subscribe']) && $_POST['cmd_subscribe'])
		{
			self::subscribeDiscuss($_user['id'],$object_id,$object_type);
            #if ($_SERVER['REMOTE_ADDR'] != '188.239.13.151')
			     sys::redirect($_SERVER['REQUEST_URI'],false);
		}

		//Отписка от дискусии
		if(isset($_POST['cmd_unsubscribe']) && $_POST['cmd_unsubscribe'])
		{
			self::unsubscribeDiscuss($_user['id'],$object_id,$object_type);
			sys::redirect($_SERVER['REQUEST_URI'],false);
		}

		if(isset($_POST['_task']) && $_POST['_task'])
		{
			$task=explode('.',$_POST['_task']);
			//Удаление сообщения
			if($task[0]=='delete' && sys::checkAccess('main::discuss_messages_delete'))
			{
				main_discuss::deleteMessage($task[1]);
				self::minusComment($object_type, $object_id);
				sys::redirect($_SERVER['REQUEST_URI'],false);
			}

			//+1
			if($task[0]=='plus' && sys::checkAccess('main::discuss_messages_mark'))
			{
				self::markMessage($task[1], $_user['id'],1);
				sys::redirect($_SERVER['REQUEST_URI'].'#message_'.$task[1],false);
			}


			//+1
			if($task[0]=='minus' && sys::checkAccess('main::discuss_messages_mark'))
			{
				self::markMessage($task[1], $_user['id'],-1);
				sys::redirect($_SERVER['REQUEST_URI'].'#message_'.$task[1],false);
			}
		}



		if(isset($_POST['cmd_add']) && $_POST['cmd_add'])
		{
			$_POST['text']=trim($_POST['text']);
			if($_user['id']==2 && !$_POST['name'])
				$_POST['name']=sys::translate('main::guest');
			$_POST['name']=strip_tags($_POST['name']);
			// Добавить можно любые строчки. Если строчка найдена в посте, то пост не пройдет.
			$filter_keywords = array();
			$filter_keywords[] = 'kino';
			$filter_keywords[] = 'Кинофильмс';
			$filter_keywords[] = 'bobfilm';
			$filter_keywords[] = 'ПОЛФИЛЬМА ВЫРЕЗАНО';
			$filter_keywords[] = 'сюжет - супер!';
			$filter_keywords[] = '80aqffbe6ay';
			$filter_keywords[] = 'filmgid.ga';
			$filter_keywords[] = 'p1ai';
			$filter_keywords[] = 'filmpro';
			$filter_keywords[] = 'FILMPRO';
			$filter_keywords[] = '.GA';
			$filter_keywords[] = '. GA';
			$filter_keywords[] = 'МАКСФИЛМ';
			$filter_keywords[] = '/film/';
			$filter_keywords[] = '===';
			$filter_keywords[] = 'malohit';
			$filter_keywords[] = 'brend';
			$filter_keywords[] = '.cf';
			$filter_keywords[] = 'color';
			$filter_keywords[] = 'hdmovie';
			$filter_keywords[] = 'online';
			$filter_keywords[] = 'ONLINE';
			$filter_keywords[] = 'GO';
			$filter_keywords[] = 'pub';
			$filter_keywords[] = '720';
			$filter_keywords[] = 'parkos';
			$filter_keywords[] = 'com';
			$filter_keywords[] = 'tinyurl';
			$filter_keywords[] = 'vk';
			$filter_keywords[] = 'cc';
			$filter_keywords[] = 'bit';
			$filter_keywords[] = 'aridan-spb';
			$filter_keywords[] = '1ru.in';
			$filter_keywords[] = '.ac';
			$filter_keywords[] = 'blogspot';
			$filter_keywords[] = '8b.kz';
			$filter_keywords[] = 'is.gd';
			$filter_keywords[] = 'is.qd';
			$filter_keywords[] = 'u.to';
			$filter_keywords[] = '<';
			$filter_keywords[] = '[url';
			$filter_keywords[] = 'http';
			$filter_keywords[] = 'смотрим здесь';
			$bad_filter = false;
			foreach ($filter_keywords as $value) 
				if(mb_strpos($_POST['text'],$value)!==false)
				{
					$bad_filter = true;
					break;
				}		
//if(sys::isDebugIP()) die(var_dump($_POST));			
						
			$ok_captcha = true;	
			
			// для форм, где установлена reCaptcha, проверяется g-recaptcha-response, а 
			// в формах ответов reCaptch'и нет, но есть object_id, поэтому считаем, что reCaptcha пройдена и проверяем только 
			// ответ из списка вопросов
			if( ($_user['id']==2) && !isset($_POST["object_id"])) 
				$ok_captcha = main_captcha::check();				
			
			
//			if($_POST['text'] && mb_strpos($_POST['text'],'tt')===false &&  mb_strpos($_POST['text'],'F')===false &&  mb_strpos($_POST['text'],'TT')===false &&  mb_strpos($_POST['text'],'��')===false &&  mb_strpos($_POST['text'],'A')===false &&  mb_strpos($_POST['text'],'hdmovie')===false &&  mb_strpos($_POST['text'],'online')===false &&  mb_strpos($_POST['text'],'ONLINE')===false &&  mb_strpos($_POST['text'],'GQ')===false &&  mb_strpos($_POST['text'],'pub')===false &&  mb_strpos($_POST['text'],'link')===false &&  mb_strpos($_POST['text'],'fr')===false &&  mb_strpos($_POST['text'],'HD')===false &&  mb_strpos($_POST['text'],'chilp')===false &&  mb_strpos($_POST['text'],'720')===false &&  mb_strpos($_POST['text'],'ga')===false &&  mb_strpos($_POST['text'],'TK')===false &&  mb_strpos($_POST['text'],'pw')===false &&  mb_strpos($_POST['text'],'parkos')===false &&  mb_strpos($_POST['text'],'com')===false &&  mb_strpos($_POST['text'],'tinyurl')===false &&  mb_strpos($_POST['text'],'vk')===false &&  mb_strpos($_POST['text'],'cc')===false &&  mb_strpos($_POST['text'],'bit')===false &&  mb_strpos($_POST['text'],'aridan-spb')===false &&  mb_strpos($_POST['text'],'ly')===false &&  mb_strpos($_POST['text'],'0lv')===false &&  mb_strpos($_POST['text'],'re1')===false &&  mb_strpos($_POST['text'],'.gs')===false &&  mb_strpos($_POST['text'],'1ru.in')===false &&  mb_strpos($_POST['text'],'.ac')===false &&  mb_strpos($_POST['text'],'.ru')===false &&  mb_strpos($_POST['text'],'su')===false &&  mb_strpos($_POST['text'],'blogspot')===false &&  mb_strpos($_POST['text'],'8b.kz')===false &&  mb_strpos($_POST['text'],'cc')===false &&  mb_strpos($_POST['text'],'is.gd')===false &&  mb_strpos($_POST['text'],'is.qd')===false &&  mb_strpos($_POST['text'],'ru')===false &&  mb_strpos($_POST['text'],'by')===false &&  mb_strpos($_POST['text'],'u.to')===false &&  mb_strpos($_POST['text'],'gl')===false &&  mb_strpos($_POST['text'],'<')===false &&  mb_strpos($_POST['text'],'[url')===false && mb_strpos($_POST['text'],'http')===false)
			if($_POST['text'] && ($bad_filter==false) && $ok_captcha)
			{
				
//				if($_user['id']==2 && !main_questions::checkAnswer($_POST['question_id'],$_POST['answer']))
//					sys::redirect($_SERVER['REQUEST_URI'],false);
				
					
				$_POST['text']=mb_substr(trim($_POST['text']),0,$_cfg['main::message_limit']);	

				switch(mysql_real_escape_string($object_type)) // переключающее выражение
				{
				   case 'articles_article': // константное выражение 1
						sys::useLib('main::articles');
						$link = main_articles::getArticleUrl($object_id);
				   break;
				   case 'cinema': // константное выражение 1
						sys::useLib('main::cinemas');
						$link = main_cinemas::getCinemaUrl($object_id);
				   break;

				   case 'serials_article': // константное выражение 1
						sys::useLib('main::serials');
						$link = main_serials::getArticleUrl($object_id);
				   break;
				   case 'contest_article': // константное выражение 1
						sys::useLib('main::contest');
						$link = main_contest::getArticleUrl($object_id);
				   break;
				   case 'film': // константное выражение 1
						sys::useLib('main::films');
						$link = main_films::getFilmUrl($object_id);
				   break;
				   case 'trailer': // константное выражение 1
						sys::useLib('main::films');
						$link = main_films::getFilmTrailerUrl($object_id);
				   break;

				   case 'gossip_article': // константное выражение 1
						sys::useLib('main::gossip');
						$link = main_gossip::getArticleUrl($object_id);
				   break;
				   case 'interview_article': // константное выражение 1
						sys::useLib('main::interview');
						$link = main_interview::getArticleUrl($object_id);
				   break;

				   case 'news_article': // константное выражение 1
						sys::useLib('main::news');
						$link = main_news::getArticleUrl($object_id);
				   break;

				   case 'person': // константное выражение 1
						sys::useLib('main::persons');
						$link = main_persons::getPersonUrl($object_id);
				   break;
				   case 'review': // константное выражение 1
						sys::useLib('main::reviews');
						$link = main_reviews::getReviewUrl($object_id);
				   break;
				   default:
				      $link='';
				}


				/* $letter = '<strong>Новый комментарий на сайте Kino-teatr.ua</strong><br><br>
				<div style="margin:0 0 5px 0;" class="box">
				'.mysql_real_escape_string($_POST['text']).'<br><br>
				
				<a href="'.$link.'">Просмотреть детальнее</a></div>
				';

				sys::sendCommentMail('info@kino-teatr.ua','Kino-Teatr.ua','noreply@kino-teatr.ua','Олег Бойко','New Comment','',$letter);
				*/
				if($_db->query("
					INSERT INTO `#__main_discuss_messages`
					(
						`id`,
						`object_id`,
						`object_type`,
						`user_id`,
						`user_name`,
						`date`,
						`ip`,
						`text`
					)
					VALUES
					(
						0,
						'".intval($object_id)."',
						'".mysql_real_escape_string($object_type)."',
						'".intval($_user['id'])."',
						'".mysql_real_escape_string($_POST['name'])."',
						'".gmdate('Y-m-d H:i:s')."',
						'".ip2long($_SERVER['REMOTE_ADDR'])."',
						'".mysql_real_escape_string($_POST['text'])."'
					)
				"))
					self::plusComment($object_type, $object_id);

				
				sys::redirect($_SERVER['REQUEST_URI'],false);
			}
		
			if($bad_filter)
				sys::jsAlertErr(sys::translate('main::links_forbid'));
			elseif (!$ok_captcha)	
				sys::jsAlertErr(sys::translate('main::bad_captcha'));
			// sys::jsAlertErr(sys::translate('main::links_forbid')); // comment by Mike. Bug on contest_article when post new participant
		}
/*
		if(isset($_POST['cmd_edit']) && $_POST['cmd_edit'])
		{
			$user_id=$_db->getValue('main_discuss_messages','user_id',$_POST['id']);

			if(sys::checkAccess('main::discuss_messages_edit' || ($user_id==$_user['id'] && $_user['id']!==2)))
			{
				$_POST['text']=mb_substr(trim($_POST['text']),0,$_cfg['main::message_limit']);
				$_POST['name']=strip_tags($_POST['name']);
				if($_POST['text'])
				{
					$_db->query("
						UPDATE `#__main_discuss_messages`
						SET `text`='".mysql_real_escape_string($_POST['text'])."',
						`date_edit`='".gmdate('Y-m-d H:i:s')."'
						WHERE `id`=".intval($_POST['id'])."
					");
					sys::redirect($_SERVER['REQUEST_URI'],false);
				}
			}
		}
*/
		//Пометить в рассылке что юзер просмотрел тему
		$_db->query("
			UPDATE `#__main_users_discuss_subscribe` SET `sent`=0
			WHERE `user_id`=".intval($_user['id'])."
			AND `object_id`=".intval($object_id)."
			AND `object_type`=".intval($object_type)."
		");

		//Получить сообщения дискусии
		$q="
			SELECT
				msg.id,
				msg.date,
				msg.date_edit,
				msg.ip,
				msg.user_id,
				msg.user_name,
				msg.rating,
				usr.login AS `user`,
				grp.group_id AS `star`,
				data.main_avatar AS `avatar`,
				msg.text,
				rat.mark
			FROM `#__main_discuss_messages` AS `msg`

			LEFT JOIN `#__sys_users` AS `usr`
			ON usr.id=msg.user_id

			LEFT JOIN `#__sys_users_data` AS `data`
			ON data.user_id=msg.user_id

			LEFT JOIN `#__main_discuss_rating` AS `rat`
			ON rat.message_id=msg.id
			AND rat.user_id=".intval($_user['id'])."

			LEFT JOIN `#__sys_users_groups` as grp
			ON grp.user_id=msg.user_id
			AND grp.group_id=23

			WHERE msg.object_id='".intval($object_id)."'
			AND msg.object_type='".mysql_real_escape_string($object_type)."'

			ORDER BY msg.date DESC
		";
		$var['pages']=sys_pages::pocess($q,10);

		// by Mike 
		// Заменить старые ссылки на новые
		if($chpu_base_url)
		{
			$var['pages']['first_page'] = sys_pages::setPageUrl($var['pages']['first_page'], $chpu_base_url);
			$var['pages']['prev_page']  = sys_pages::setPageUrl($var['pages']['prev_page'],  $chpu_base_url);
			$var['pages']['next_page']  = sys_pages::setPageUrl($var['pages']['next_page'],  $chpu_base_url);
			$var['pages']['last_page']  = sys_pages::setPageUrl($var['pages']['last_page'],  $chpu_base_url);
			foreach ($var['pages']['pages'] as $i=>$p)
			 $var['pages']['pages'][$i] = sys_pages::setPageUrl($var['pages']['pages'][$i],  $chpu_base_url);
		}
		
		$r=$_db->query($q.$var['pages']['limit']);

		$var['objects']=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['date']=sys::russianDateTime($obj['date']);
			$obj['date_edit']=sys::db2Date($obj['date_edit'],true);
			if($obj['avatar'])
			{
				$obj['avatar']=$_cfg['main::users_url'].$obj['avatar'];
			} else {
				$obj['avatar']=$_cfg['main::users_url'].'avatar.jpg';
			}

			$obj['ip']=long2ip($obj['ip']);
			if($obj['user_id']==2)
			{
				$obj['user']=$obj['user_name'];
				$obj['user_url']=false;
			}
			else
				$obj['user_url']=main_users::getUserUrl($obj['user_id']);
			$obj['bb_text']=sys::parseBBCode($obj['text']);
			if($obj['rating']>0)
				$obj['rating']='+'.$obj['rating'];
			if(!$obj['rating'])
				$obj['rating']=0;

			$var['objects'][]=$obj;
		}


		if ($_user['id']!=2)
		{
			$q="
				SELECT
					usr.login AS `user`,
					data.main_avatar AS `avatar`
				FROM `#__sys_users` AS `usr`

				LEFT JOIN `#__sys_users_data` AS `data`
				ON data.user_id=usr.id

				WHERE usr.id='".$_user['id']."'

			";
			$r=$_db->query($q.$var['pages']['limit']);

			while ($obj=$_db->fetchAssoc($r))
			{
				if($obj['user'])
				{
					$var['mylogin']=$obj['user'];
				}

					$var['myurl']=main_users::getUserUrl($_user['id']);


				if($obj['avatar'])
				{
					$var['myavatar']=$_cfg['main::users_url'].$obj['avatar'];
				} else {
					$var['myavatar']=$_cfg['main::users_url'].'avatar.jpg';
				}
			}
		} else {
					$var['myavatar']=$_cfg['main::users_url'].'avatar.jpg';
		}

					$var['nowdate']=sys::russianDate(date('d.m.Y'));

		//Узнать подписан ли юзер на дисскусию
		$r=$_db->query("
			SELECT `user_id` FROM `#__main_users_discuss_subscribe`
			WHERE
				`user_id`=".intval($_user['id'])." AND
				`object_id`=".intval($object_id)." AND
				`object_type`='".mysql_real_escape_string($object_type)."'
		");

		list($var['subscribe'])=$_db->fetchArray($r);
		$var['object_id'] = $object_id;
		$var['object_type'] = $object_type;
		$var['chpu_base_url'] = $chpu_base_url;
		
		return sys::parseTpl('main::discuss',$var);
	}


	static function MarkComment($type, $mark, $user_id)
	{
		global $_db, $_cfg;

		$r=$_db->query("
			SELECT `rating`
			FROM `#__main_discuss_messages`
			WHERE `id`=".intval($mark)."
		");
		$rate=$_db->fetchAssoc($r);


		if ($type=='plus')
		{
			$rate['rating']++;
			$rat = '1';
		} else {
			$rate['rating']--;
			$rat = '-1';
		}

		if(!$_db->query("
			UPDATE `#__main_discuss_messages`
			SET `rating`='".$rate['rating']."'
			WHERE `id`=".intval($mark)."
		"))
			return $_db->rollback();

		if(!$_db->query("
				INSERT INTO `#__main_discuss_rating`
				(
					`message_id`,
					`user_id`,
					`mark`
				)
				VALUES
				(
					'".intval($mark)."',
					'".intval($user_id)."',
					'".intval($rat)."'
				)
			"))
				return $_db->rollback();

		if ($rate['rating']>0)
		{
			$rate['rating'] = '+'.$rate['rating'];
		}

		return array('rating'=>$rate['rating']);

	}


	static function showDiscuss($object_id, $object_type)
	{
		global $_db, $_cfg, $_user;
		sys::useLib('main::users');
		sys::useLib('sys::pages');
		sys::useLib('main::questions');

		if($_user['id']==2)
			$var['question']=main_questions::getQuestion();

		//Подписка на дисскусию
		if(isset($_POST['cmd_subscribe']) && $_POST['cmd_subscribe'])
		{
			self::subscribeDiscuss($_user['id'],$object_id,$object_type);
			sys::redirect($_SERVER['REQUEST_URI'],false);
		}

		//Отписка от дискусии
		if(isset($_POST['cmd_unsubscribe']) && $_POST['cmd_unsubscribe'])
		{
			self::unsubscribeDiscuss($_user['id'],$object_id,$object_type);
			sys::redirect($_SERVER['REQUEST_URI'],false);
		}


		if(isset($_POST['_task']) && $_POST['_task'])
		{
			$task=explode('.',$_POST['_task']);
			//Удаление сообщения
			if($task[0]=='delete' && sys::checkAccess('main::discuss_messages_delete'))
			{
				main_discuss::deleteMessage($task[1]);
				self::minusComment($object_type, $object_id);
				sys::redirect($_SERVER['REQUEST_URI'],false);
			}

			//+1
			if($task[0]=='plus' && sys::checkAccess('main::discuss_messages_mark'))
			{
				self::markMessage($task[1], $_user['id'],1);
				sys::redirect($_SERVER['REQUEST_URI'].'#message_'.$task[1],false);
			}


			//+1
			if($task[0]=='minus' && sys::checkAccess('main::discuss_messages_mark'))
			{
				self::markMessage($task[1], $_user['id'],-1);
				sys::redirect($_SERVER['REQUEST_URI'].'#message_'.$task[1],false);
			}
		}



		if(isset($_POST['cmd_add']) && $_POST['cmd_add'] && sys::checkAccess('main::discuss_messages_add'))
		{
			$_POST['text']=trim($_POST['text']);
			if($_user['id']==2 && !$_POST['name'])
				$_POST['name']=sys::translate('main::guest');
			$_POST['name']=strip_tags($_POST['name']);

			if($_POST['text'] && mb_strpos($_POST['text'],'tt')===false &&  mb_strpos($_POST['text'],'F')===false &&  mb_strpos($_POST['text'],'TT')===false &&  mb_strpos($_POST['text'],'��')===false &&  mb_strpos($_POST['text'],'A')===false &&  mb_strpos($_POST['text'],'hdmovie')===false &&  mb_strpos($_POST['text'],'online')===false &&  mb_strpos($_POST['text'],'ONLINE')===false &&  mb_strpos($_POST['text'],'GQ')===false &&  mb_strpos($_POST['text'],'pub')===false &&  mb_strpos($_POST['text'],'link')===false &&  mb_strpos($_POST['text'],'fr')===false &&  mb_strpos($_POST['text'],'HD')===false &&  mb_strpos($_POST['text'],'chilp')===false &&  mb_strpos($_POST['text'],'720')===false &&  mb_strpos($_POST['text'],'ga')===false &&  mb_strpos($_POST['text'],'TK')===false &&  mb_strpos($_POST['text'],'pw')===false &&  mb_strpos($_POST['text'],'parkos')===false &&  mb_strpos($_POST['text'],'com')===false &&  mb_strpos($_POST['text'],'tinyurl')===false &&  mb_strpos($_POST['text'],'vk')===false &&  mb_strpos($_POST['text'],'cc')===false &&  mb_strpos($_POST['text'],'bit')===false &&  mb_strpos($_POST['text'],'aridan-spb')===false &&  mb_strpos($_POST['text'],'ly')===false &&  mb_strpos($_POST['text'],'0lv')===false &&  mb_strpos($_POST['text'],'re1')===false &&  mb_strpos($_POST['text'],'.gs')===false &&  mb_strpos($_POST['text'],'1ru.in')===false &&  mb_strpos($_POST['text'],'.ac')===false &&  mb_strpos($_POST['text'],'.ru')===false &&  mb_strpos($_POST['text'],'su')===false &&  mb_strpos($_POST['text'],'blogspot')===false &&  mb_strpos($_POST['text'],'8b.kz')===false &&  mb_strpos($_POST['text'],'cc')===false &&  mb_strpos($_POST['text'],'is.gd')===false &&  mb_strpos($_POST['text'],'is.qd')===false &&  mb_strpos($_POST['text'],'ru')===false &&  mb_strpos($_POST['text'],'by')===false &&  mb_strpos($_POST['text'],'u.to')===false &&  mb_strpos($_POST['text'],'gl')===false &&  mb_strpos($_POST['text'],'<')===false &&  mb_strpos($_POST['text'],'[url')===false && mb_strpos($_POST['text'],'http')===false)
				{
				if($_user['id']==2 && !main_questions::checkAnswer($_POST['question_id'],$_POST['answer']))
					sys::redirect($_SERVER['REQUEST_URI'],false);
				$_POST['text']=mb_substr(trim($_POST['text']),0,$_cfg['main::message_limit']);
				if($_db->query("
					INSERT INTO `#__main_discuss_messages`
					(
						`id`,
						`object_id`,
						`object_type`,
						`user_id`,
						`user_name`,
						`date`,
						`ip`,
						`text`
					)
					VALUES
					(
						0,
						'".intval($object_id)."',
						'".mysql_real_escape_string($object_type)."',
						'".intval($_user['id'])."',
						'".mysql_real_escape_string($_POST['name'])."',
						'".gmdate('Y-m-d H:i:s')."',
						'".ip2long($_SERVER['REMOTE_ADDR'])."',
						'".mysql_real_escape_string($_POST['text'])."'
					)
				"))
					self::plusComment($object_type, $object_id);

				sys::redirect($_SERVER['REQUEST_URI'],false);
			}
			sys::jsAlertErr(sys::translate('main::links_forbid'));
		}

/*		if(isset($_POST['cmd_edit']) && $_POST['cmd_edit'])
		{
			$user_id=$_db->getValue('main_discuss_messages','user_id',$_POST['id']);

			if(sys::checkAccess('main::discuss_messages_edit' || ($user_id==$_user['id'] && $_user['id']!==2)))
			{
				$_POST['text']=mb_substr(trim($_POST['text']),0,$_cfg['main::message_limit']);
				$_POST['name']=strip_tags($_POST['name']);
				if($_POST['text'])
				{
					$_db->query("
						UPDATE `#__main_discuss_messages`
						SET `text`='".mysql_real_escape_string($_POST['text'])."',
						`date_edit`='".gmdate('Y-m-d H:i:s')."'
						WHERE `id`=".intval($_POST['id'])."
					");
					sys::redirect($_SERVER['REQUEST_URI'],false);
				}
			}
		}
*/
		//Пометить в рассылке что юзер просмотрел тему
		$_db->query("
			UPDATE `#__main_users_discuss_subscribe` SET `sent`=0
			WHERE `user_id`=".intval($_user['id'])."
			AND `object_id`=".intval($object_id)."
			AND `object_type`=".intval($object_type)."
		");

		//Получить сообщения дискусии
		$q="
			SELECT
				msg.id,
				msg.date,
				msg.date_edit,
				msg.ip,
				msg.user_id,
				msg.user_name,
				msg.rating,
				usr.login AS `user`,
				grp.group_id AS `star`,
				data.main_avatar AS `avatar`,
				msg.text,
				rat.mark
			FROM `#__main_discuss_messages` AS `msg`

			LEFT JOIN `#__sys_users` AS `usr`
			ON usr.id=msg.user_id

			LEFT JOIN `#__sys_users_data` AS `data`
			ON data.user_id=msg.user_id

			LEFT JOIN `#__main_discuss_rating` AS `rat`
			ON rat.message_id=msg.id
			AND rat.user_id=".intval($_user['id'])."

			LEFT JOIN `#__sys_users_groups` as grp
			ON grp.user_id=msg.user_id
			AND grp.group_id=23

			WHERE msg.object_id='".intval($object_id)."'
			AND msg.object_type='".mysql_real_escape_string($object_type)."'

			ORDER BY msg.date DESC
		";
		$var['pages']=sys_pages::pocess($q,$_cfg['main::comments_on_page']);
		$r=$_db->query($q.$var['pages']['limit']);

		$var['objects']=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['date']=sys::russianDate($obj['date']);;
			$obj['date_edit']=sys::db2Date($obj['date_edit'],true);
			if($obj['avatar'])
			{
				$obj['avatar']=$_cfg['main::users_url'].$obj['avatar'];
			} else {
				$obj['avatar']=$_cfg['main::users_url'].'avatar.jpg';
			}

			$obj['ip']=long2ip($obj['ip']);
			if($obj['user_id']==2)
			{
				$obj['user']=$obj['user_name'];
				$obj['user_url']=false;
			}
			else
				$obj['user_url']=main_users::getUserUrl($obj['user_id']);
			$obj['ip']=long2ip($obj['ip']);
			if($obj['user_id']==2)
			{
				$obj['user']=$obj['user_name'];
				$obj['user_url']=false;
			}
			else
				$obj['user_url']=main_users::getUserUrl($obj['user_id']);
			$obj['bb_text']=sys::parseBBCode($obj['text']);
			if($obj['rating']>0)
				$obj['rating']='+'.$obj['rating'];
			if(!$obj['rating'])
				$obj['rating']=0;

			$var['objects'][]=$obj;
		}

		if ($_user['id']!=2)
		{
			$q="
				SELECT
					usr.login AS `user`,
					data.main_avatar AS `avatar`
				FROM `#__sys_users` AS `usr`

				LEFT JOIN `#__sys_users_data` AS `data`
				ON data.user_id=usr.id

				WHERE usr.id='".$_user['id']."'

			";
			$r=$_db->query($q.$var['pages']['limit']);

			while ($obj=$_db->fetchAssoc($r))
			{
				if($obj['user'])
				{
					$var['mylogin']=$obj['user'];
				}

					$var['myurl']=main_users::getUserUrl($_user['id']);


				if($obj['avatar'])
				{
					$var['myavatar']=$_cfg['main::users_url'].$obj['avatar'];
				} else {
					$var['myavatar']=$_cfg['main::users_url'].'avatar.jpg';
				}
			}
		} else {
					$var['myavatar']=$_cfg['main::users_url'].'avatar.jpg';
		}

					$var['nowdate']=sys::russianDate(date('d.m.Y'));

		//Узнать подписан ли юзер на дисскусию
		$r=$_db->query("
			SELECT `user_id` FROM `#__main_users_discuss_subscribe`
			WHERE
				`user_id`=".intval($_user['id'])." AND
				`object_id`=".intval($object_id)." AND
				`object_type`='".mysql_real_escape_string($object_type)."'
		");

		list($var['subscribe'])=$_db->fetchArray($r);



		return sys::parseTpl('main::discuss',$var);
	}

	static function markMessage($message_id, $user_id, $mark=1)
	{
		global $_db;
		$_db->query("
			SELECT `user_id` FROM `#__main_discuss_rating`
			WHERE `user_id`=".intval($user_id)."
			AND `message_id`=".intval($message_id)."
		");

		list($is_mark)=$_db->fetchArray($r);
		if($is_mark)
			return false;

		$_db->begin();

		if(!$_db->query("
			INSERT INTO `#__main_discuss_rating`
			(
				`user_id`,
				`message_id`,
				`mark`
			)
			VALUES
			(
				'".intval($user_id)."',
				'".intval($message_id)."',
				'".intval($mark)."'
			)
		"))
			return $_db->rollback();

		if(!$_db->query("
			UPDATE `#__main_discuss_messages`
			SET `rating`=(`rating`+".intval($mark).")
			WHERE `id`=".intval($message_id)."
		"))
			return $_db->rollback();
		$_db->commit();
	}

	static function isMessages($object_id, $object_type)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_discuss_messages`
			WHERE `object_id`=".intval($object_id)."
			AND `object_type`='".mysql_real_escape_string($object_type)."'
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

}
?>