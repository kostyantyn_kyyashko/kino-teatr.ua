<?php
class main_awards
{	
	static function deleteCategory($category_id)
	{
		sys::useLib('main::films');
		global $_db;
		
		$r=$_db->query("SELECT `id` FROM `#__main_films_awards` WHERE `award_id`=".intval($category_id)."");
		while (list($film_award_id)=$_db->fetchArray($r)) 
		{
			main_films::deleteAwardLink($film_award_id);
		}
		$_db->delete('main_awards_categories',intval($category_id),true);
	}
	
	static function deleteAward($award_id)
	{
		global $_db;
		$r=$_db->query("SELECT `id` FROM `#__main_awards_categories` WHERE `award_id`=".intval($award_id)."");
		while(list($category_id)=$_db->fetchArray($r))
		{
			self::deleteCategory($category_id);
		}
		
		$_db->delete('main_awards',intval($award_id));
	}
	
	static function getAwardsNames($arr_ids)
	{
		global $_db;
		$result=array();
		if(!$arr_ids || !count($arr_ids))
			return false;
		for($i=0; $i<count($arr_ids); $i++)
		{
			$arr_ids[$i]=intval($arr_ids[$i]);
		}
		$str_ids=implode(',',$arr_ids);
		$r=$_db->query("SELECT `id`, `name` 
			FROM `#__main_awards` 
			WHERE `id` IN(".$str_ids.")");
		while($obj=$_db->fetchAssoc($r))
		{
			$result[$obj['id']]=$obj['name'];
		}
		return $result;
	}
	
	static function showAwardTabs($award_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('sys::info');
		$tabs['info']['url']='?mod=main&act=award_edit&id='.$award_id;
		
		$tabs['categories']['title']=sys::translate('main::categories');
		$tabs['categories']['url']='?mod=main&act=award_categories_table&award_id='.$award_id;
		
		return sys_gui::showTabs($tabs, $active_tab);
	}
	
	static function getAwardName($category_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT CONCAT(awd.name,'::',cat.name)
			FROM `#__main_awards_categories` AS `cat`
			LEFT JOIN `#__main_awards` AS `awd`
			ON awd.id=cat.award_id
			WHERE cat.id=".intval($category_id)."
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}
}
?>