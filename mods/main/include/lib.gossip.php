<?php
class main_gossip
{
	static function linkArticleSpecThemes($article_id, $spec_themes)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_gossip_articles_spec_themes`
			WHERE `article_id`='".intval($article_id)."'");
		if($spec_themes)
		{
			$spec_themes=array_unique($spec_themes);
			foreach ($spec_themes as $spec_themes_id=>$name)
			{
				if($spec_themes_id)
				{
					$_db->query("INSERT INTO `#__main_gossip_articles_spec_themes`
						(
							`article_id`,
							`spec_theme_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($spec_themes_id)."
						)
					");
				}
			}
		}
	}

	static function getArticleSpecThemesIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `spec_theme_id`
			FROM `#__main_gossip_articles_spec_themes`
			WHERE `article_id`=".intval($article_id)."");
		while(list($id)=$_db->fetchArray($r))
		{
			$result[]=$id;
		}
		return $result;
	}
		
	static function deletegossip($gossip_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_gossip_articles`
			WHERE `gossip_id`=".intval($gossip_id)."
		");
		while(list($article_id)=$_db->fetchArray($r))
		{
			self::deleteArticle($article_id);
		}
		$_db->delete('main_gossip',intval($gossip_id),true);
	}

	static function confirmArticle($article_id)
	{
		global $_db;
		$_db->setValue('main_gossip_articles','public',1,intval($article_id));
	}


	static function showgossipAdminTabs($gossip_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('sys::info');
		$tabs['info']['url']='?mod=main&act=gossip_edit&id='.$gossip_id;

		$tabs['articles']['title']=sys::translate('main::articles');
		$tabs['articles']['url']='?mod=main&act=gossip_articles_table&gossip_id='.$gossip_id;

		return sys_gui::showTabs($tabs, $active_tab);
	}

	static function deleteArticleImage($filename)
	{
		global $_cfg;
		if(is_file($_cfg['main::gossip_dir'].$filename))
			unlink($_cfg['main::gossip_dir'].$filename);
		if(is_file($_cfg['main::gossip_dir'].'x0_'.$filename))
			unlink($_cfg['main::gossip_dir'].'x0_'.$filename);
		if(is_file($_cfg['main::gossip_dir'].'x1_'.$filename))
			unlink($_cfg['main::gossip_dir'].'x1_'.$filename);
		if(is_file($_cfg['main::gossip_dir'].'x2_'.$filename))
			unlink($_cfg['main::gossip_dir'].'x2_'.$filename);
		if(is_file($_cfg['main::gossip_dir'].'x3_'.$filename))
			unlink($_cfg['main::gossip_dir'].'x3_'.$filename);
		if(is_file($_cfg['main::gossip_dir'].'x5_'.$filename))
			unlink($_cfg['main::gossip_dir'].'x5_'.$filename);
	}

	static function deleteArticle($article_id)
	{
		sys::useLib('main::discuss');
		global $_db, $_cfg;
		$article=$_db->getRecord('main_gossip_articles',intval($article_id));

		if(is_file($_cfg['main::gossip_dir'].$article['small_image']))
			unlink($_cfg['main::gossip_dir'].$article['small_image']);

		self::deleteArticleImage($article['image']);

		$_db->delete('main_gossip_articles_persons',"`article_id`=".intval($article_id)."");
		$_db->delete('main_gossip_articles_films',"`article_id`=".intval($article_id)."");


		main_discuss::deleteDiscuss($article_id,'gossip_article');
		$_db->delete('main_gossip_index',"`article_id`=".intval($article_id)."");
		$_db->delete('main_gossip_articles',$article_id,true);
	}

	static function uploadArticleImage($file,$article_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deleteArticleImage($current_image);

		if($image=sys::uploadFile($file,$_cfg['main::gossip_dir'],'article_'.$article_id, true))
		{
			$_db->setValue('main_gossip_articles','image',$image,$article_id);
			self::resizeArticleImage($image);
		}
		return $image;
	}

	static function resizeArticleImage($filename)
	{
		global $_cfg;
		sys::resizeImage($_cfg['main::gossip_dir'].$filename,$_cfg['main::gossip_dir'].'x1_'.$filename,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		sys::resizeImage($_cfg['main::gossip_dir'].$filename,$_cfg['main::gossip_dir'].'x2_'.$filename,$_cfg['main::x2_width'],$_cfg['main::x2_height']);
		sys::resizeImage($_cfg['main::gossip_dir'].$filename,$_cfg['main::gossip_dir'].'x5_'.$filename,$_cfg['main::x5_width'],$_cfg['main::x5_height']);
	}

	static function uploadArticleSmallImage($file,$article_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image && is_file($_cfg['main::gossip_dir'].$current_image))
			unlink($_cfg['main::gossip_dir'].$current_image);

		if($image=sys::uploadFile($file,$_cfg['main::gossip_dir'],'x1_'.$article_id, true))
		{
		// inserted by Mike begin			
			$path_info=pathInfo($_cfg['main::gossip_dir'].$image);
			if(isset($path_info['extension']) && $path_info['extension'])
			{
				$extention = ".".strtolower($path_info['extension']);
				sys::resizeImage($_cfg['main::gossip_dir'].$image,$_cfg['main::gossip_dir'].'x0_article_'.$article_id.$extention,$_cfg['main::x0_width'],$_cfg['main::x0_height'],true); 
				sys::resizeImage($_cfg['main::gossip_dir'].$image,$_cfg['main::gossip_dir'].'x3_article_'.$article_id.$extention,$_cfg['main::x3_width'],$_cfg['main::x3_height'],true); 
			}
		// inserted by Mike end
			$_db->setValue('main_gossip_articles','small_image',$image,$article_id);
			sys::resizeImage($_cfg['main::gossip_dir'].$image,false,213,137);
		}
		return $image;
	}

	static function linkArticlePersons($article_id, $persons)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_gossip_articles_persons`
			WHERE `article_id`='".intval($article_id)."'");
		if($persons)
		{
			$persons=array_unique($persons);
			foreach ($persons as $person_id=>$name)
			{
				if($person_id)
				{
					$_db->query("INSERT INTO `#__main_gossip_articles_persons`
						(
							`article_id`,
							`person_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($person_id)."
						)
					");
				}
			}
		}
	}

	static function linkArticleFilms($article_id, $films)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_gossip_articles_films`
			WHERE `article_id`='".intval($article_id)."'");
		if($films)
		{
			$films=array_unique($films);
			foreach ($films as $film_id=>$name)
			{
				if($film_id)
				{
					$_db->query("INSERT INTO `#__main_gossip_articles_films`
						(
							`article_id`,
							`film_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($film_id)."
						)
					");
				}
			}
		}
	}

	static function getArticlePersonsIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `person_id`
			FROM `#__main_gossip_articles_persons`
			WHERE `article_id`=".intval($article_id)."");
		while(list($person_id)=$_db->fetchArray($r))
		{
			$result[]=$person_id;
		}
		return $result;
	}

	static function getArticleFilmsIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `film_id`
			FROM `#__main_gossip_articles_films`
			WHERE `article_id`=".intval($article_id)."");
		while(list($film_id)=$_db->fetchArray($r))
		{
			$result[]=$film_id;
		}
		return $result;
	}

	static function translitIt($str)
	{
	    $tr = array(
	        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
	        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
	        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
	        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
	        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
	        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
	        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	        "the"=>"","the "=>""," the"=>""," the "=>"-",
	        "The"=>"","The "=>""," The"=>""," The "=>"-",
	        "... "=> "", " - "=> "-"," "=> "-", "."=> "", "/"=> "-"
	    );
	    return strtr($str,$tr);
	}

	static function getArticleUkUrl($article_id)
	{
			return _ROOT_URL.'uk/main/gossip_article/article_id/'.$article_id.'.phtml';
	}

	static function getArticleUrl($article_id, $lang=false)
	{
		global $_cfg, $_db;

		if(!$lang) $lang=$_cfg['sys::lang'];
		
		if ($lang=='ru') 	$lang="";
		else 				$lang="$lang/";

		$r=$_db->query("
			SELECT name
			FROM `#__main_gossip_articles`
			WHERE id='".$article_id."'
		");

		if($obj=$_db->fetchAssoc($r))
		{
		    $urlstr = self::translitIt(trim($obj['name']));
		    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);
			return _ROOT_URL.$lang.'gossip/'.$urlstr.'-'.$article_id.'.phtml';
		}
		return sys::rewriteUrl('?mod=main&act=gossip_article&article_id='.$article_id);
	}

	static function getgossipUrl($gossip_id)
	{
		return sys::rewriteUrl('?mod=main&act=gossip&id='.$gossip_id);
	}

	static function showLastPopularArticles()
	{
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_last_gossip_'.__FUNCTION__."_".$_cfg['sys::lang'].$_cfg['main::city_id'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::last_gossip_cache_period']*60))
			return $cache;


		$r=$_db->query("
			SELECT
				art.id,
				art.date,
				art.gossip_id,
				art.total_shows as shows,
				art.comments,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				spec.spec_theme_id as spec_theme,
				art_lng.title,
				nws_lng.title AS `gossip`,
				art_lng.intro
			FROM `#__main_gossip_articles` AS `art`

			LEFT JOIN `#__main_gossip_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_gossip_lng` AS `nws_lng`
			ON nws_lng.record_id=art.gossip_id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_gossip` AS `nws`
			ON art.gossip_id=nws.id

			LEFT JOIN `#__main_gossip_articles_spec_themes` AS `spec`
			ON art.id=spec.article_id

			WHERE art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND art.date>'".gmdate('Y-m-d H:i:s', strtotime('- 180 days'))."'
			AND (art.city_id='".intval($_cfg['main::city_id'])."' OR art.city_id=0)
			AND nws.showtab=1
			ORDER BY art.today_shows DESC, art.total_shows DESC
			LIMIT 0,5
		");

		$i=1;

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['intro']=sys::cutString(htmlspecialchars(strip_tags($obj['intro'])),140);
	  	    
			$image="x".(($i==1)?3:0)."_article_".$obj['id'].".jpg";
			if(!file_exists('./public/main/gossip/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::gossip_url'].$image;
			$obj['image'] = $image;

			$obj['url']=self::getArticleUrl($obj['id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';
			if ($i==3 || $i==5)	$obj['bottom']=' bottomNews';

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::last_news',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function showLastDiscussedArticles()
	{
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_last_gossip_'.__FUNCTION__."_".$_cfg['sys::lang'].$_cfg['main::city_id'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::last_gossip_cache_period']*60))
			return $cache;


		$r=$_db->query("
			SELECT
				art.id,
				art.date,
				art.gossip_id,
				art.total_shows as shows,
				art.comments,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				spec.spec_theme_id as spec_theme,
				art_lng.title,
				nws_lng.title AS `gossip`,
				art_lng.intro
			FROM `#__main_gossip_articles` AS `art`

			LEFT JOIN `#__main_gossip_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_gossip_lng` AS `nws_lng`
			ON nws_lng.record_id=art.gossip_id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_gossip` AS `nws`
			ON art.gossip_id=nws.id

			LEFT JOIN `#__main_gossip_articles_spec_themes` AS `spec`
			ON art.id=spec.article_id

			WHERE art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND art.date>'".gmdate('Y-m-d H:i:s', strtotime('- 180 days'))."'
			AND (art.city_id='".intval($_cfg['main::city_id'])."' OR art.city_id=0)
			AND nws.showtab=1
			ORDER BY art.comments DESC
			LIMIT 0,5
		");

		$i=1;

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['intro']=sys::cutString(htmlspecialchars(strip_tags($obj['intro'])),140);
	  	    
			$image="x".(($i==1)?3:0)."_article_".$obj['id'].".jpg";
			if(!file_exists('./public/main/gossip/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::gossip_url'].$image;
			$obj['image'] = $image;

			$obj['url']=self::getArticleUrl($obj['id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';
			if ($i==3 || $i==5)	$obj['bottom']=' bottomNews';

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::last_news',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}


	static function showLastArticles()
	{
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_last_gossip_'.__FUNCTION__."_".$_cfg['sys::lang'].$_cfg['main::city_id'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::last_gossip_cache_period']*60))
			return $cache;


		$r=$_db->query("
			SELECT
				art.id,
				art.date,
				art.gossip_id,
				art.total_shows as shows,
				art.comments,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				spec.spec_theme_id as spec_theme,
				art_lng.title,
				nws_lng.title AS `gossip`,
				art_lng.intro
			FROM `#__main_gossip_articles` AS `art`

			LEFT JOIN `#__main_gossip_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_gossip_lng` AS `nws_lng`
			ON nws_lng.record_id=art.gossip_id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_gossip` AS `nws`
			ON art.gossip_id=nws.id

			LEFT JOIN `#__main_gossip_articles_spec_themes` AS `spec`
			ON art.id=spec.article_id

			WHERE art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND art.date>'".gmdate('Y-m-d H:i:s', strtotime('- 180 days'))."'
			AND (art.city_id='".intval($_cfg['main::city_id'])."' OR art.city_id=0)
			AND nws.showtab=1
			ORDER BY art.date DESC
			LIMIT 0,5
		");

		$i=1;

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['intro']=sys::cutString(htmlspecialchars(strip_tags($obj['intro'])),140);
	  	    
			$image="x".(($i==1)?3:0)."_article_".$obj['id'].".jpg";
			if(!file_exists('./public/main/gossip/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::gossip_url'].$image;
			$obj['image'] = $image;

			$obj['url']=self::getArticleUrl($obj['id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';
			if ($i==3 || $i==5)	$obj['bottom']=' bottomNews';

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::last_news',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function getMonthBegin($month,$year)
	{
		return mktime(0,0,0,$month,1,$year);
	}

	static function getMonthEnd($month,$year)
	{
		return mktime(23,59,59,$month,date('t',self::getMonthBegin($month,$year)),$year);
	}

	static function getCalendar($year=false, $month=false, $gossip_id=false)
	{
		global $_db, $_cfg;

		$file=_CACHE_DIR.'main_gossip_calendar_'.$gossip_id.'.dat';
		if(is_file($file))
		{
			clearstatcache();
			if(date('m')==date('m',filemtime($file)) && $_cfg['main::cache_gossip_calendar'])
				return unserialize(file_get_contents($file));
			else
				unlink($file);

		}

		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$month=intval($month);
		$q="
			SELECT
				MAX(`date`),
				MIN(`date`)
			FROM `#__main_gossip_articles`
			WHERE `public`=1
		";
		if($gossip_id)
		{
			$q.="
				AND `gossip_id`=".intval($gossip_id)."
			";
		}
		$r=$_db->query($q);
		list($max_date, $min_date)=$_db->fetchArray($r);
		$min_date=sys::db2Timestamp($min_date);
		$max_date=sys::db2Timestamp($max_date);
		$result=array();
		if($min_date)
		{
			$min_year=date('Y',$min_date);
			$max_yar=date('Y',$max_date);

			for($y=$min_year; $y<$max_yar+1; $y++)
			{
				$obj['title']=$y;
				for($m=1; $m<13; $m++)
				{
					$obj['months'][$m]['title']=strftime('%b',self::getMonthBegin($m,$y));
					$obj['months'][$m]['is_gossip']=self::isgossip($y,$m,$gossip_id);
				}
				$result[$y]=$obj;
			}
		}

		$result=array_reverse($result,true);
		if($_cfg['main::cache_gossip_calendar'])
			file_put_contents($file, serialize($result));
		return $result;
	}

	static function isgossip($year, $month, $gossip_id=false)
	{
		global $_db, $_cfg;
		$month_begin=self::getMonthBegin($month,$year);
		$month_end=self::getMonthEnd($month,$year);

		$q="
			SELECT `id` FROM `#__main_gossip_articles`
			WHERE `date` BETWEEN '".date('Y-m-d H:i:s',$month_begin)."' AND '".date('Y-m-d H:i:s',$month_end)."'
		";
		if($gossip_id)
		{
			$q.="
				AND `gossip_id`=".intval($gossip_id)."
			";
		}
		$q.="
			AND (`city_id`=".intval($_cfg['main::city_id'])." OR `city_id`=0)
			LIMIT 0,1
		";

		$r=$_db->query($q);
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function showCalendar($year=false, $month=false, $gossip_id=false)
	{
		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$month=intval($month);

		$var['years']=self::getCalendar($year, $month, $gossip_id);

		foreach ($var['years'] as $y=>$arr_year)
		{
			foreach ($arr_year['months'] as $m=>$arr_month)
			{
				if($month==$m && $year==$y)
					$var['years'][$y]['months'][$m]['current']=true;
				else
					$var['years'][$y]['months'][$m]['current']=false;

				if($var['years'][$y]['months'][$m]['is_gossip'])
				{
					$url='?mod=main&act=gossip&year='.$y.'&month='.intval($m);
					if($gossip_id)
						$url.='&gossip_id='.$gossip_id;
					$var['years'][$y]['months'][$m]['url']=sys::rewriteUrl($url);
				}
				else
					$var['years'][$y]['months'][$m]['url']=false;
				$var['years'][$y]['months'][$m]['alt']=sys::translate('main::gossip_cinema').' '.sys::translate('main::for').' '.sys::translate('main::month_'.$m).' '.$y;
			}
		}
		return sys::parseTpl('main::gossip_calendar',$var);
	}

	static function showgossipTabs($year,$month,$gossip_id)
	{
		global $_db, $_cfg;

		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$var['active']=$gossip_id;

		$var['objects'][0]['title']=sys::translate('main::all_sections');
		$var['objects'][0]['url']=sys::rewriteUrl('?mod=main&act=gossip');
		$var['objects'][0]['alt']=sys::translate('main::gossip_cinema').'::'.sys::translate('main::all_sections');

		$r=$_db->query("
			SELECT nws.id, nws_lng.title
			FROM `#__main_gossip` AS `nws`

			LEFT JOIN `#__main_gossip_lng` AS `nws_lng`
			ON nws_lng.record_id=nws.id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE nws.showtab='1'

			ORDER BY nws.order_number
		");
		while ($obj=$_db->fetchAssoc($r))
		{
			$var['objects'][$obj['id']]['title']=$obj['title'];
			$var['objects'][$obj['id']]['url']=sys::rewriteUrl('?mod=main&act=gossip&gossip_id='.$obj['id']);
			$var['objects'][$obj['id']]['alt']=sys::translate('main::gossip_cinema').'::'.$obj['title'];
		}


		return sys::parseTpl('main::tabs',$var);
	}

	static function gossipReirect($year, $month, $gossip_id=false)
	{
		if($year<2000)
			return false;
		if($month==1)
		{
			$month=12;
			$year=$year-1;
		}
		else
		{
			$month=$month-1;
		}
		if(self::isgossip($year,$month,$gossip_id))
		{
			$url='?mod=main&act=gossip&year='.$year.'&month='.$month;
			if($gossip_id)
				$url.='&gossip_id='.$gossip_id;
			sys::redirect($url);
		}
		self::gossipReirect($year, $month,$gossip_id);
	}

	static function getArticleAddUrl()
	{
		return sys::rewriteUrl('?mod=main&act=gossip_article_edit');
	}

	static function getArticleEditUrl($article_id)
	{
		return sys::rewriteUrl('?mod=main&act=gossip_article_edit&article_id='.$article_id);
	}

	static function getArticleDiscussUrl($article_id)
	{
		return sys::rewriteUrl('?mod=main&act=gossip_article&article_id='.$article_id);
	}

	static function isArticleDiscuss($article_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT COUNT(*) as count
			FROM `#__main_discuss_messages`

			WHERE object_type='gossip_article' AND object_id='".$article_id."'
		");
		while ($obj=$_db->fetchAssoc($r))
		{
			$count = $obj['count'];
		}

		return $count;
	}


	static function showArticleTabs($article_id,$article_title,$active_tab=false)
	{
			if(self::isArticleDiscuss($article_id))
		{
		$var['active']=$active_tab;
		$var['objects']['text']['title']=sys::translate('main::text');
		$var['objects']['text']['url']=self::getArticleUrl($article_id);
		$var['objects']['text']['alt']=$article_title;

		$var['objects']['discuss']['title']=sys::translate('main::discuss');
		$var['objects']['discuss']['url']=$var['objects']['text']['url']."#discuss";
   	  //$var['objects']['discuss']['url']=self::getArticleDiscussUrl($article_id)."#discuss";
		$var['objects']['discuss']['alt']=$article_title.'::'.sys::translate('main::discuss');

		return sys::parseTpl('main::tabs',$var);
		}
	}

	static function setArticleIndex($article_id,$values)
	{
		global $_db, $_cfg, $_langs;
		$_db->delete('main_gossip_index',"`article_id`=".intval($article_id)."");
		foreach ($_langs as $lang_id=>$lang)
		{
			$_db->query("
				INSERT INTO `#__main_gossip_index`
				(
					`article_id`,
					`lang_id`,
					`text`
				)
				VALUES
				(
					'".intval($article_id)."',
					'".$lang_id."',
					'".mysql_real_escape_string($values['title_'.$lang['code']])." ".strip_tags(mysql_real_escape_string($values['text_'.$lang['code']]))."'
				)

			");
		}

	}


}
?>