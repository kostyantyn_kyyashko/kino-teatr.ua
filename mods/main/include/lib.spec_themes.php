<?php
class main_spec_themes
{	
	static function deleteSpecThemes($id)
	{
		global $_db;
		$_db->delete('main_spec_themes',intval($id),true);
	}	
	
	static function getSpecThemesNames($arr_ids)
	{
		global $_db;
		$result=array();
		if(!$arr_ids || !count($arr_ids))
			return false;
		for($i=0; $i<count($arr_ids); $i++)
		{
			$arr_ids[$i]=intval($arr_ids[$i]);
		}
		$str_ids=implode(',',$arr_ids);
		$r=$_db->query("SELECT `id`, `name` 
			FROM `#__main_spec_themes` 
			WHERE `id` IN(".$str_ids.")");
		while($obj=$_db->fetchAssoc($r))
		{
			$result[$obj['id']]=$obj['name'];
		}
		return $result;
	}
	
	static function getSpecThemesTitles()
	{
		global $_cfg, $_db;
		$r=$_db->query("
			SELECT st.id, lng.title 
			FROM `#__main_spec_themes` AS `st`
			LEFT JOIN `#__main_spec_themes_lng` AS `lng`
			ON lng.record_id=st.id 
			AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
			ORDER BY st.order_number 
		");
		while ($st=$_db->fetchAssoc($r)) 
		{
			$result[$st['id']]=$st['title'];	
		}
		return $result;
	}
	
	static function translitIt($str)
	{
	    $tr = array(
	        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
	        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
	        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
	        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
	        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
	        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
	        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	        "the"=>"","the "=>""," the"=>""," the "=>"-",
	        "The"=>"","The "=>""," The"=>""," The "=>"-",
	        "... "=> "", " - "=> "-"," "=> "-", "."=> "", "/"=> "-"
	    );
	    return strtr($str,$tr);
	}

	static function getArticleUrl($article_id, $lang=false)
	{
		global $_cfg, $_db;

		if(!$lang) $lang=$_cfg['sys::lang'];
		
		if ($lang=='ru') 	$lang="";
		else 				$lang="$lang/";
		
		$r=$_db->query("SELECT name, id	FROM `#__main_spec_themes` WHERE id='".$article_id."'");

		if($obj=$_db->fetchAssoc($r))
		{
		    $urlstr = self::translitIt(trim($obj['name']));
		    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);
			return _ROOT_URL.$lang.'spec_themes/'.$urlstr.'-'.$article_id.'.phtml';
		}
		return sys::rewriteUrl('?mod=main&act=spec_themes_article&article_id='.$article_id);
	}
	
}
?>