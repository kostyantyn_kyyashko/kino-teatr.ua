<?php
class main_fest
{
	static function deletefest($fest_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_fest_articles`
			WHERE `fest_id`=".intval($fest_id)."
		");
		while(list($article_id)=$_db->fetchArray($r))
		{
			self::deleteArticle($article_id);
		}
		$_db->delete('main_fest',intval($fest_id),true);
	}

	static function confirmArticle($article_id)
	{
		global $_db;
		$_db->setValue('main_fest_articles','public',1,intval($article_id));
	}


	static function showfestAdminTabs($fest_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('sys::info');
		$tabs['info']['url']='?mod=main&act=fest_edit&id='.$fest_id;

		$tabs['articles']['title']=sys::translate('main::articles');
		$tabs['articles']['url']='?mod=main&act=fest_articles_table&fest_id='.$fest_id;

		return sys_gui::showTabs($tabs, $active_tab);
	}

	static function deleteArticleImage($filename)
	{
		global $_cfg;
		if(is_file($_cfg['main::fest_dir'].$filename))
			unlink($_cfg['main::fest_dir'].$filename);
		if(is_file($_cfg['main::fest_dir'].'x1_'.$filename))
			unlink($_cfg['main::fest_dir'].'x1_'.$filename);
		if(is_file($_cfg['main::fest_dir'].'x2_'.$filename))
			unlink($_cfg['main::fest_dir'].'x2_'.$filename);
		if(is_file($_cfg['main::fest_dir'].'x3_'.$filename))
			unlink($_cfg['main::fest_dir'].'x3_'.$filename);
		if(is_file($_cfg['main::fest_dir'].'x5_'.$filename))
			unlink($_cfg['main::fest_dir'].'x5_'.$filename);
	}

	static function deleteArticle($article_id)
	{


		sys::useLib('main::discuss');


		global $_db, $_cfg;




		$article=$_db->getRecord('main_fest_articles',intval($article_id));

		main_discuss::deleteDiscuss($article_id,'fest_article');
		$_db->delete('main_fest_articles',$article_id,true);
	}

	static function uploadArticleImage($file,$article_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deleteArticleImage($current_image);

		if($image=sys::uploadFile($file,$_cfg['main::fest_dir'],'article_'.$article_id, true))
		{
			$_db->setValue('main_fest_articles','image',$image,$article_id);
			self::resizeArticleImage($image);
		}
		return $image;
	}

	static function resizeArticleImage($filename)
	{
		global $_cfg;
		sys::resizeImage($_cfg['main::fest_dir'].$filename,$_cfg['main::fest_dir'].'x1_'.$filename,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		sys::resizeImage($_cfg['main::fest_dir'].$filename,$_cfg['main::fest_dir'].'x2_'.$filename,$_cfg['main::x2_width'],$_cfg['main::x2_height']);
		sys::resizeImage($_cfg['main::fest_dir'].$filename,$_cfg['main::fest_dir'].'x3_'.$filename,$_cfg['main::x3_width'],$_cfg['main::x3_height']);
		sys::resizeImage($_cfg['main::fest_dir'].$filename,$_cfg['main::fest_dir'].'x5_'.$filename,$_cfg['main::x5_width'],$_cfg['main::x5_height']);
	}

	static function uploadArticleSmallImage($file,$article_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image && is_file($_cfg['main::fest_dir'].$current_image))
			unlink($_cfg['main::fest_dir'].$current_image);

		if($image=sys::uploadFile($file,$_cfg['main::fest_dir'],'x1_'.$article_id, true))
		{
			$_db->setValue('main_fest_articles','small_image',$image,$article_id);
			sys::resizeImage($_cfg['main::fest_dir'].$image,false,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		}
		return $image;
	}

	static function linkArticlePersons($article_id, $persons)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_fest_articles_persons`
			WHERE `article_id`='".intval($article_id)."'");
		if($persons)
		{
			$persons=array_unique($persons);
			foreach ($persons as $person_id=>$name)
			{
				if($person_id)
				{
					$_db->query("INSERT INTO `#__main_fest_articles_persons`
						(
							`article_id`,
							`person_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($person_id)."
						)
					");
				}
			}
		}
	}

	static function linkArticleFilms($article_id, $films)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_fest_articles_films`
			WHERE `article_id`='".intval($article_id)."'");
		if($films)
		{
			$films=array_unique($films);
			foreach ($films as $film_id=>$name)
			{
				if($film_id)
				{
					$_db->query("INSERT INTO `#__main_fest_articles_films`
						(
							`article_id`,
							`film_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($film_id)."
						)
					");
				}
			}
		}
	}

	static function getArticlePersonsIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `person_id`
			FROM `#__main_fest_articles_persons`
			WHERE `article_id`=".intval($article_id)."");
		while(list($person_id)=$_db->fetchArray($r))
		{
			$result[]=$person_id;
		}
		return $result;
	}

	static function getArticleFilmsIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `film_id`
			FROM `#__main_fest_articles_films`
			WHERE `article_id`=".intval($article_id)."");
		while(list($film_id)=$_db->fetchArray($r))
		{
			$result[]=$film_id;
		}
		return $result;
	}

	static function getArticleUrl($article_id)
	{
		return sys::rewriteUrl('?mod=main&act=fest_article&article_id='.$article_id);
	}

	static function getfestUrl($fest_id)
	{
		return sys::rewriteUrl('?mod=main&act=fest&id='.$fest_id);
	}

	static function showLastArticles()
	{
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_last_fest_'.$_cfg['sys::lang'].$_cfg['main::city_id'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::last_fest_cache_period']*60))
			return $cache;

		$r=$_db->query("
			SELECT
				art.id,
				art.date,
				art.fest_id,
				art.small_image AS `image`,
				art.exclusive,
				art_lng.title,
				nws_lng.title AS `fest`
			FROM `#__main_fest_articles` AS `art`

			LEFT JOIN `#__main_fest_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_fest_lng` AS `nws_lng`
			ON nws_lng.record_id=art.fest_id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND (art.city_id='".intval($_cfg['main::city_id'])."' OR art.city_id=0)
			ORDER BY art.date DESC
			LIMIT 0,6
		");

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['fest']=htmlspecialchars($obj['fest']);
			if($obj['image'])
				$obj['image']=$_cfg['main::fest_url'].$obj['image'];
			else
				$obj['image']=main::getNoImage('x1');
			$obj['url']=self::getArticleUrl($obj['id']);
			$obj['fest_url']=self::getfestUrl($obj['fest_id']);
			$obj['date']=sys::db2Date($obj['date'],true,$_cfg['sys::date_format']);
			$var['objects'][]=$obj;
		}
		$result=sys::parseTpl('main::last_fest',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function getMonthBegin($month,$year)
	{
		return mktime(0,0,0,$month,1,$year);
	}

	static function getMonthEnd($month,$year)
	{
		return mktime(23,59,59,$month,date('t',self::getMonthBegin($month,$year)),$year);
	}

	static function getCalendar($year=false, $month=false, $fest_id=false)
	{
		global $_db, $_cfg;

		$file=_CACHE_DIR.'main_fest_calendar_'.$fest_id.'.dat';
		if(is_file($file))
		{
			clearstatcache();
			if(date('m')==date('m',filemtime($file)) && $_cfg['main::cache_fest_calendar'])
				return unserialize(file_get_contents($file));
			else
				unlink($file);

		}

		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$month=intval($month);
		$q="
			SELECT
				MAX(`date`),
				MIN(`date`)
			FROM `#__main_fest_articles`
			WHERE `public`=1
		";
		if($fest_id)
		{
			$q.="
				AND `fest_id`=".intval($fest_id)."
			";
		}
		$r=$_db->query($q);
		list($max_date, $min_date)=$_db->fetchArray($r);
		$min_date=sys::db2Timestamp($min_date);
		$max_date=sys::db2Timestamp($max_date);
		$result=array();
		if($min_date)
		{
			$min_year=date('Y',$min_date);
			$max_yar=date('Y',$max_date);

			for($y=$min_year; $y<$max_yar+1; $y++)
			{
				$obj['title']=$y;
				for($m=1; $m<13; $m++)
				{
					$obj['months'][$m]['title']=strftime('%b',self::getMonthBegin($m,$y));
					$obj['months'][$m]['is_fest']=self::isfest($y,$m,$fest_id);
				}
				$result[$y]=$obj;
			}
		}

		$result=array_reverse($result,true);
		if($_cfg['main::cache_fest_calendar'])
			file_put_contents($file, serialize($result));
		return $result;
	}

	static function isfest($year, $month, $fest_id=false)
	{
		global $_db, $_cfg;
		$month_begin=self::getMonthBegin($month,$year);
		$month_end=self::getMonthEnd($month,$year);

		$q="
			SELECT `id` FROM `#__main_fest_articles`
			WHERE `date` BETWEEN '".date('Y-m-d H:i:s',$month_begin)."' AND '".date('Y-m-d H:i:s',$month_end)."'
		";
		if($fest_id)
		{
			$q.="
				AND `fest_id`=".intval($fest_id)."
			";
		}
		$q.="
			AND (`city_id`=".intval($_cfg['main::city_id'])." OR `city_id`=0)
			LIMIT 0,1
		";

		$r=$_db->query($q);
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function showCalendar($year=false, $month=false, $fest_id=false)
	{
		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$month=intval($month);

		$var['years']=self::getCalendar($year, $month, $fest_id);

		foreach ($var['years'] as $y=>$arr_year)
		{
			foreach ($arr_year['months'] as $m=>$arr_month)
			{
				if($month==$m && $year==$y)
					$var['years'][$y]['months'][$m]['current']=true;
				else
					$var['years'][$y]['months'][$m]['current']=false;

				if($var['years'][$y]['months'][$m]['is_fest'])
				{
					$url='?mod=main&act=fest&year='.$y.'&month='.intval($m);
					if($fest_id)
						$url.='&fest_id='.$fest_id;
					$var['years'][$y]['months'][$m]['url']=sys::rewriteUrl($url);
				}
				else
					$var['years'][$y]['months'][$m]['url']=false;
				$var['years'][$y]['months'][$m]['alt']=sys::translate('main::fest_cinema').' '.sys::translate('main::for').' '.sys::translate('main::month_'.$m).' '.$y;
			}
		}
		return sys::parseTpl('main::fest_calendar',$var);
	}

	static function showfestTabs($year,$month,$fest_id)
	{
		global $_db, $_cfg;

		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$var['active']=$fest_id;

		$var['objects'][0]['title']=sys::translate('main::all_sections');
		$var['objects'][0]['url']=sys::rewriteUrl('?mod=main&act=fest&year='.$year.'&month='.$month);
		$var['objects'][0]['alt']=sys::translate('main::fest_cinema').'::'.sys::translate('main::all_sections');

		$r=$_db->query("
			SELECT nws.id, nws_lng.title
			FROM `#__main_fest` AS `nws`

			LEFT JOIN `#__main_fest_lng` AS `nws_lng`
			ON nws_lng.record_id=nws.id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			ORDER BY nws.order_number
		");
		while ($obj=$_db->fetchAssoc($r))
		{
			$var['objects'][$obj['id']]['title']=$obj['title'];
			$var['objects'][$obj['id']]['url']=sys::rewriteUrl('?mod=main&act=fest&year='.$year.'&month='.$month.'&fest_id='.$obj['id']);
			$var['objects'][$obj['id']]['alt']=sys::translate('main::fest_cinema').'::'.$obj['title'];
		}


		return sys::parseTpl('main::tabs',$var);
	}

	static function festReirect($year, $month, $fest_id=false)
	{
		if($year<2000)
			return false;
		if($month==1)
		{
			$month=12;
			$year=$year-1;
		}
		else
		{
			$month=$month-1;
		}
		if(self::isfest($year,$month,$fest_id))
		{
			$url='?mod=main&act=fest&year='.$year.'&month='.$month;
			if($fest_id)
				$url.='&fest_id='.$fest_id;
			sys::redirect($url);
		}
		self::festReirect($year, $month,$fest_id);
	}

	static function getArticleAddUrl()
	{
		return sys::rewriteUrl('?mod=main&act=fest_article_edit');
	}

	static function getArticleEditUrl($article_id)
	{
		return sys::rewriteUrl('?mod=main&act=fest_article_edit&article_id='.$article_id);
	}

	static function getArticleDiscussUrl($article_id)
	{
		return sys::rewriteUrl('?mod=main&act=fest_article_discuss&article_id='.$article_id);
	}

	static function showArticleTabs($article_id,$article_title,$active_tab=false)
	{
		$var['active']=$active_tab;
		$var['objects']['text']['title']=sys::translate('main::text');
		$var['objects']['text']['url']=self::getArticleUrl($article_id);
		$var['objects']['text']['alt']=$article_title;

		$var['objects']['discuss']['title']=sys::translate('main::discuss');
		$var['objects']['discuss']['url']=self::getArticleDiscussUrl($article_id);
		$var['objects']['discuss']['alt']=$article_title.'::'.sys::translate('main::discuss');

		return sys::parseTpl('main::tabs',$var);
	}

	static function setArticleIndex($article_id,$values)
	{
		global $_db, $_cfg, $_langs;
		$_db->delete('main_fest_index',"`article_id`=".intval($article_id)."");
		foreach ($_langs as $lang_id=>$lang)
		{
			$_db->query("
				INSERT INTO `#__main_fest_index`
				(
					`article_id`,
					`lang_id`,
					`text`
				)
				VALUES
				(
					'".intval($article_id)."',
					'".$lang_id."',
					'".mysql_real_escape_string($values['title_'.$lang['code']])." ".strip_tags(mysql_real_escape_string($values['text_'.$lang['code']]))."'
				)

			");
		}

	}


}
?>