<?php

class main_payment {
	
	public static $login = 'kinoteatrua';
	public static $sign = 'c3fafa8bf07bfc95abc932d2327313e8d1a441d8';
	public static $url = 'https://walletmc.ipay.ua/';
	public static $test_msisdn = '380509899595';
	public static $real_msisdn = '';
	public static $debug_mode = false;
	public static $test_payment = false;
	
	public static function Check(){
		global $_user;
		$data = date('Y-m-d H:i:s');
		$sign = md5($data.self::$sign);
		if(!self::$debug_mode && empty($_user['main_phone']))
			return false;
		if(self::$debug_mode)
			$msdn = self::$test_msisdn;
		else
			$msdn = $_user['main_phone'];
		$data_string = '{
   		"request":{
        "auth":{
            "login":"'.self::$login.'",
            "time":"'.$data.'",
            "sign":"'.$sign.'"
        },
        "action":"Check",
        "body":{
            "msisdn": "'.$msdn.'",
			"user_id":"'.$_user['id'].'"
        }
    }
}';
//   		echo $data_string.'<br>';
  		$ch = curl_init(self::$url);                                                                      
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      	'Content-Type: application/json',                                                                                
      	'Content-Length: ' . strlen($data_string))                                                                       
  		);                                                                                                                   
   
  		$result = curl_exec($ch);
		return $result;
	
}

	public static function PaymentCancel(){
		global $_user;
		$pmt_id = $_REQUEST['pmt_id'];
		$data = date('Y-m-d H:i:s');
		$sign = md5($data.self::$sign);
		if(!self::$debug_mode && empty($_user['main_phone']))
			return false;
		if(self::$debug_mode)
			$msdn = self::$test_msisdn;
		else
			$msdn = $_user['main_phone'];
		$data_string = '{
   		"request":{
        "auth":{
            "login":"'.self::$login.'",
            "time":"'.$data.'",
            "sign":"'.$sign.'"
        },
        "action":"PaymentCancel",
        "body":{
            "msisdn": "'.$msdn.'",
			"user_id":"'.$_user['id'].'",
			"pmt_id": "'.$pmt_id.'"
        }
    }
}';
  		$ch = curl_init(self::$url);                                                                      
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      	'Content-Type: application/json',                                                                                
      	'Content-Length: ' . strlen($data_string))                                                                       
  		);                                                                                                                   
  		$result = curl_exec($ch);

		return $result;
	}
	
	public static function testOtp(){
		$result['response']['status'] = 'OK';
		return json_encode($result);
	}

	public static function PaymentSale(){
		global $_user;
		$token = $_REQUEST['token'];
		$code = $_REQUEST['code'];
		$pmt_id = $_REQUEST['pmt_id'];
		$invoice = $_REQUEST['invoice'];
		$data = date('Y-m-d H:i:s');
		$sign = md5($data.self::$sign);
		if(self::$test_payment)
			return self::testPaymentSale();
		if(!self::$debug_mode && empty($_user['main_phone']))
			return false;
		if(self::$debug_mode)
			$msdn = self::$test_msisdn;
		else
			$msdn = $_user['main_phone'];
		if(self::$debug_mode)
			$invoice = '100';
		$data_string = '{
   		"request":{
        "auth":{
            "login":"'.self::$login.'",
            "time":"'.$data.'",
            "sign":"'.$sign.'"
        },
        "action":"PaymentSale",
        "body":{
            "msisdn": "'.$msdn.'",
			"user_id":"'.$_user['id'].'",
			"pmt_id": "'.$pmt_id.'",
            "invoice": "'.$invoice.'"
        }
    }
}';
  		$ch = curl_init(self::$url);                                                                      
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      	'Content-Type: application/json',                                                                                
      	'Content-Length: ' . strlen($data_string))                                                                       
  		);                                                                                                                   
  		$result = curl_exec($ch);

		return $result;
	}
	
	public static function ThreeDS(){
		global $_user;
		$pares = $_REQUEST['PaRes'];
		$md = $_REQUEST['MD'];
		$pmt_id = $_REQUEST['pmt_id'];
		$data = date('Y-m-d H:i:s');
		$sign = md5($data.self::$sign);
		if(!self::$debug_mode && empty($_user['main_phone']))
			return false;
		if(self::$debug_mode)
			$msdn = self::$test_msisdn;
		else
			$msdn = $_user['main_phone'];

		$data_string = '{
    "request": {
        "auth": {
            "login":"'.self::$login.'",
            "time":"'.$data.'",
            "sign":"'.$sign.'"
        },
        "action": "PaymentVerify3DS",
        "body": {
             "msisdn": "'.$msdn.'",
			"user_id":"'.$_user['id'].'",
            "pmt_id": "'.$pmt_id.'",
            "pares": "'.$pares.'",
            "md": "'.$md.'"
        }
    }
}';
//		echo $data_string.'<br>';
  		$ch = curl_init(self::$url);                                                                      
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      	'Content-Type: application/json',                                                                                
      	'Content-Length: ' . strlen($data_string))                                                                       
  		);                                                                                                                   
   
  		$result = curl_exec($ch);
		return $result;
	}

	public static function Otp(){
		global $_user;
		$token = $_REQUEST['token'];
		$code = $_REQUEST['code'];
		$pmt_id = $_REQUEST['pmt_id'];
		$data = date('Y-m-d H:i:s');
		$sign = md5($data.self::$sign);
		if(!self::$debug_mode && empty($_user['main_phone']))
			return false;
		if(self::$debug_mode)
			$msdn = self::$test_msisdn;
		else
			$msdn = $_user['main_phone'];
		if(self::$test_payment)
			return self::testOtp();
		$data_string = '{
   		"request":{
        "auth":{
            "login":"'.self::$login.'",
            "time":"'.$data.'",
            "sign":"'.$sign.'"
        },
        "action":"Otp",
        "body":{
            "msisdn": "'.$msdn.'",
			"user_id":"'.$_user['id'].'",
			"token": "'.$token.'",
            "value": "'.$code.'"
        }
    }
}';
  		$ch = curl_init(self::$url);                                                                      
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      	'Content-Type: application/json',                                                                                
      	'Content-Length: ' . strlen($data_string))                                                                       
  		);                                                                                                                   
   
  		$result = curl_exec($ch);
		return $result;
	}
	
	public static function testPaymentCreate(){
		$response = '{"code":0,"items":[{"siteId":"fakel","showId":"952","eventId":"359289","sectorId":"1","placeId":50691,"discountId":"0","rowNumber":"6","placeNumber":"4","hallName":"\u0424\u0430\u043a\u0435\u043b","showName":"\u0422\u0430\u0447\u043a\u0438 3","sectorName":"","eventDate":"2017-06-28 17:15:00","price":4500,"hallId":"17"}],"message":"\u0417\u0430\u043f\u0440\u043e\u0441 \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d"}';
		return $response;
	}
	public static function testPaymentSale(){
		$result['response']['pmt_status'] = 5;
		$result['response']['pmt_id'] = 556595;
		return json_encode($result);
		
	}
	
	public static function CalcPaymentAmount($invoice){
		global $_user;
		if(!self::$debug_mode && empty($_user['main_phone']))
			return false;
		if(!$invoice)
			return false;
//		if(self::$debug_mode)
//			$msdn = self::$test_msisdn;
//		else
		$msdn = $_user['main_phone'];
		$data = date('Y-m-d H:i:s');
		$sign = md5($data.self::$sign);
		

		$data_string = '{
   		"request":{
        "auth":{
            "login":"'.self::$login.'",
            "time":"'.$data.'",
            "sign":"'.$sign.'"
        },
        "action":"CalcPaymentAmount",
       "body": {      
    	"invoice": "'.$invoice.'",
    	"msisdn": "'.$msdn.'",
    	"user_id": "'.$_user['id'].'"
		}
    	}
	}';
//	echo $data_string;
  		$ch = curl_init(self::$url);                                                                      
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      	'Content-Type: application/json',                                                                                
      	'Content-Length: ' . strlen($data_string))                                                                       
  		);                                                                                                                   
   
  		$result = curl_exec($ch);
//		var_dump($result);exit;
		return $result;
	}

	public static function resultPaymentCreate($response){
		$secure = $response['secure'];
		$token = $response['token'];
		$pmt_id = $response['pmt_id'];
		$invoice = $response['invoice'];
		$cinema_name = $_REQUEST['cinema_name'];
		$film_id = $_REQUEST['film_id'];
		$hall_id = $_REQUEST['hall_id'];
		$cinema_id = $_REQUEST['cinema_id'];
		$site_id = $_REQUEST['site_id'];
	
	
			$html = '<div class="verification"><table><tr><td valign="top">';
			$html .= '<span>Введите код верификации.</span></td>';
			$html .= '<td valign="top">';
//			if(isset($response['verify_code']))
//			$html .= '<input type="textbox" class="code" name="code" size="15" value="'.$response['verify_code'].'"/></td>';
//			else
			$html .= '<input type="textbox" class="code" name="code" size="15"/></td>';
			$html .= '<td valign="top"><div class="container_otp" style="margin-left: 0px;"><input type="button" onclick="otp_verification(this);return false;" value="Ввод" style="size: 50px;" class="btn_otp"/></div></td>';
			$html .= '<td valign="top"><input type="button" onclick="cancel_otp(this);return false;" value="Отменить"/></td>';
			$html .= '<input type="hidden" class="token" name="token" value="'.$token.'"/>';
			$html .= '<input type="hidden" class="pmt_id" name="pmt_id" value="'.$pmt_id.'"/>';
			$html .= '<input type="hidden" class="invoice" name="invoice" value="'.$invoice.'"/>';
			$html .= '<input type="hidden" class="cinema_name" name="cinema_name" value="'.$cinema_name.'"/>';
			$html .= '<input type="hidden" class="film_id" name="film_id" value="'.$film_id.'"/>';
			$html .= '<input type="hidden" class="hall_id" name="hall_id" value="'.$hall_id.'"/>';
			$html .= '<input type="hidden" class="cinema_id" name="cinema_id" value="'.$cinema_id.'"/>';
			$html .= '<input type="hidden" class="site_id" name="site_id" value="'.$site_id.'"/>';
			$html .= '</tr>';
			$html .= '<tr><td colspan="4"><span style="font-size: 10px; margin-top: 5px;">Вам придет СМС сообщение с кодом подтверждения (в формате LOOKUPCODE: XXXXXX) от банка на тот  номер телефона, который Вы указали во время получения карты. В некоторых случаях, сообщение может прийти в мобильное приложение банка.</span></td></tr>';
			$html .= '</table></div>';
	

		return $html;
	}

	public static function PaymentCreate($card_alias,$invoice){
		global $_user;
		if(!$card_alias)
			return false;

		if(self::$test_payment)
			return self::testPaymentCreate();
			
		if(!$invoice)
			return false;

			
		$data = date('Y-m-d H:i:s');
		$sign = md5($data.self::$sign);
		if(!self::$debug_mode && empty($_user['main_phone']))
			return false;
		if(self::$debug_mode)
			$msdn = self::$test_msisdn;
		else
			$msdn = $_user['main_phone'];
		if(self::$debug_mode)
			$invoice = '500';
		$hash = ''	;
		if(isset($_REQUEST['site_id']))
			$hash.=$_REQUEST['site_id'].';';
		if(isset($_REQUEST['film_id']))
			$hash.=$_REQUEST['film_id'].';';
		if(isset($_REQUEST['hall_id']))
			$hash.=$_REQUEST['hall_id'].';';
		if(isset($_REQUEST['cinema_id']))
			$hash.=$_REQUEST['cinema_id'].';';
		if(isset($_REQUEST['event_id']))
			$hash.=$_REQUEST['event_id'].';';
		if(isset($_REQUEST['show_id']))
			$hash.=$_REQUEST['show_id'];
		$guid=md5($hash);
		
		
		$data_string = '{
   		"request":{
        "auth":{
            "login":"'.self::$login.'",
            "time":"'.$data.'",
            "sign":"'.$sign.'"
        },
        "action":"PaymentCreate",
       "body": {      
    	"invoice": "'.$invoice.'",
    	"card_alias": "'.$card_alias.'",
    	"pmt_info": "",
    	"pmt_desc": "",
    	"msisdn": "'.$msdn.'",
    	"user_id": "'.$_user['id'].'",
    	"guid": "'.$guid.'"
		}
    	}
	}';
//	echo $data_string;
  		$ch = curl_init(self::$url);                                                                      
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      	'Content-Type: application/json',                                                                                
      	'Content-Length: ' . strlen($data_string))                                                                       
  		);                                                                                                                   
   
  		$result = curl_exec($ch);
//		var_dump($result);exit;
		return $result;
	}
	
	
	public static function getCards(){
		global $_user;
		if(!self::$debug_mode && empty($_user['main_phone']))
			return false;
		if(self::$debug_mode)
			$msdn = self::$test_msisdn;
		else
			$msdn = $_user['main_phone'];
		$data = date('Y-m-d H:i:s');
		$sign = md5($data.self::$sign);
		
		$response_check = self::Check();
		$buffer = json_decode($response_check,true);
		if(is_null($buffer))
			return false;
		if(isset($buffer['response']['user_status']) && $buffer['response']['user_status'] != 'exists')
			return false;
		
		$data_string = '{
   		"request":{
        "auth":{
            "login":"'.self::$login.'",
            "time":"'.$data.'",
            "sign":"'.$sign.'"
        },
        "action":"List",
        "body":{
            "msisdn": "'.$msdn.'",
			 "user_id":"'.$_user['id'].'"
        }
    }
}';
  		$ch = curl_init(self::$url);                                                                      
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      	'Content-Type: application/json',                                                                                
      	'Content-Length: ' . strlen($data_string))                                                                       
  		);                                                                                                                   
   
  		$result = curl_exec($ch);
		$result = json_decode($result,true);
		if(isset($result['response']))
			return $result['response'];
		elseif(isset($result['error']))
			return $result;
	}
	
	public static function getListCards(){
		$result = self::getCards();
		
		if(empty($result['error']) && count($result)){
			$html = '<select name="card_alias" class="card_alias" style="width: 100px;">';
			$counter=0;
			foreach($result as $item){
				$html .= '<option value="'.$item['card_alias'].'">'.$item['card_alias'].'</option>';
				$counter++;
			}
			$html .= '</select>';
			if(!$counter)
				return '';
			return $html;
		}
		return '';
	}
	
	public static function deleteCard($alias){
		global $_user;
		if(!self::$debug_mode && empty($_user['main_phone']))
			return false;
		if(self::$debug_mode)
			$msdn = self::$test_msisdn;
		else
			$msdn = $_user['main_phone'];
		$data = date('Y-m-d H:i:s');
		$sign = md5($data.self::$sign);
		$data_string = '{
   		"request":{
        "auth":{
            "login":"'.self::$login.'",
            "time":"'.$data.'",
            "sign":"'.$sign.'"
        },
        "action":"DeleteCard",
        "body":{
            "msisdn": "'.$msdn.'",
			"card_alias": "'.$alias.'",
			"user_id":"'.$_user['id'].'"

        }
    }
}';
   
  		$ch = curl_init(self::$url);                                                                      
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      	'Content-Type: application/json',                                                                                
      	'Content-Length: ' . strlen($data_string))                                                                       
  		);                                                                                                                   
   
  		$result = curl_exec($ch);
		return $result;
	}
	
	public static function registerByUrl(){
		global $_user,$_cfg;
		if(!self::$debug_mode && empty($_user['main_phone']))
			return false;
		if(self::$debug_mode)
			$msdn = self::$test_msisdn;
		else
			$msdn = $_user['main_phone'];
		$data = date('Y-m-d H:i:s');
		$sign = md5($data.self::$sign);
		$success_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$_cfg['sys::lang'].'/main/user_profile_payment/user_id/'.$_user['id'].'.phtml?success=1&action=registerByURL';
		$error_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$_cfg['sys::lang'].'/main/user_profile_payment/user_id/'.$_user['id'].'.phtml?error=1&action=registerByURL';
		$data_string = '{
   		"request":{
        "auth":{
            "login":"'.self::$login.'",
            "time":"'.$data.'",
            "sign":"'.$sign.'"
        },
        "action":"RegisterByURL",
        "body":{
            "msisdn": "'.$msdn.'",
			"lang": "'.$_cfg['sys::lang'].'",
    		"success_url": "'.$success_url.'",
    		"error_url": "'.$error_url.'",
			"user_id":"'.$_user['id'].'"
        }
    }
}';
   
  		$ch = curl_init(self::$url);                                                                      
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      	'Content-Type: application/json',                                                                                
      	'Content-Length: ' . strlen($data_string))                                                                       
  		);                                                                                                                   
   
  		$result = curl_exec($ch);
		return $result;

	}
	
	public static function addByUrl(){
		global $_user,$_cfg;
		if(!self::$debug_mode && empty($_user['main_phone']))
			return false;
		if(self::$debug_mode)
			$msdn = self::$test_msisdn;
		else
			$msdn = $_user['main_phone'];
		$lang = $_cfg['sys::lang'];
		$data = date('Y-m-d H:i:s');
		$sign = md5($data.self::$sign);
		$success_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$_cfg['sys::lang'].'/main/user_profile_payment/user_id/'.$_user['id'].'.phtml?success=1&action=addByUrl';
		$error_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$_cfg['sys::lang'].'/main/user_profile_payment/user_id/'.$_user['id'].'.phtml?error=1&action=addByUrl';
//		$error_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$_cfg['sys::lang'].'/mainuser_profile_payment/user_id/'.$_user['id'].'.phtml';
		$data_string = '{
   		"request":{
        "auth":{
            "login":"'.self::$login.'",
            "time":"'.$data.'",
            "sign":"'.$sign.'"
        },
        "action":"AddcardByURL",
        "body":{
            "msisdn": "'.$msdn.'",
			"lang": "'.$_cfg['sys::lang'].'",
    		"success_url": "'.$success_url.'",
    		"error_url": "'.$error_url.'",
			"user_id":"'.$_user['id'].'"

        }
    }
}';
   
  		$ch = curl_init(self::$url);                                                                      
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      	'Content-Type: application/json',                                                                                
      	'Content-Length: ' . strlen($data_string))                                                                       
  		);                                                                                                                   
   
  		$result = curl_exec($ch);
		return $result;

	}
	
	public static function InviteByUrl(){
		global $_user,$_cfg;
		if(!self::$debug_mode && empty($_user['main_phone']))
			return false;
		if(self::$debug_mode)
			$msdn = self::$test_msisdn;
		else
			$msdn = $_user['main_phone'];
		$data = date('Y-m-d H:i:s');
		$sign = md5($data.self::$sign);
		$success_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$_cfg['sys::lang'].'/main/user_profile_payment/user_id/'.$_user['id'].'.phtml?success=1&action=InviteByURL';
		$error_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$_cfg['sys::lang'].'/main/user_profile_payment/user_id/'.$_user['id'].'.phtml?error=1&action=InviteByURL';
		$data_string = '{
   		"request":{
        "auth":{
            "login":"'.self::$login.'",
            "time":"'.$data.'",
            "sign":"'.$sign.'"
        },
        "action":"InviteByURL",
        "body":{
            "msisdn": "'.$msdn.'",
			"lang": "'.$_cfg['sys::lang'].'",
    		"success_url": "'.$success_url.'",
    		"error_url": "'.$error_url.'",
			"user_id":"'.$_user['id'].'"

        }
    }
}';
   
  		$ch = curl_init(self::$url);                                                                      
  		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
  		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
      	'Content-Type: application/json',                                                                                
      	'Content-Length: ' . strlen($data_string))                                                                       
  		);                                                                                                                   
   
  		$result = curl_exec($ch);
		return $result;

	}
	
	static function getUserUrl($user_id)
	{
		return sys::rewriteUrl('?mod=main&act=user_profile_payment&user_id='.$user_id);
	}
		
	}

?>