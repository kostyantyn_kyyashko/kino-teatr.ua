<?
$_cfg['main::x0_width']=137; //для квадратных превью 
$_cfg['main::x0_height']=137;
 
//4x3 или 3x4
$_cfg['main::x1_width']=100; //для всех маленких
$_cfg['main::x1_height']=113;

$_cfg['main::x2_width']=150; //для постеров на главной и в превью
$_cfg['main::x2_height']=500;

$_cfg['main::x3_width']=213; //для широкоформатных слева
$_cfg['main::x3_height']=137;

$_cfg['main::x4_width']=495; //для всех в области контента
$_cfg['main::x4_height']=1350;

$_cfg['main::x5_width']=80; //для широкоформатных в превью 4 на 3
$_cfg['main::x5_height']=60;

$_cfg['main::avatar_big_width']=200; //аватар пользователя большой
$_cfg['main::avatar_big_height']=300;

$_cfg['main::avatar_small_width']=100; //аватар пользователя малый
$_cfg['main::avatar_small_height']=100;

$_cfg['main::x3_film_photos_width']=155; //для превью фотографий фильмов
$_cfg['main::x3_film_photos_height']=98;

$_cfg['main::max_width']=700; //максимальные значения при закачке
$_cfg['main::max_height']=1500;

$_cfg['main::persons_dir']=_PUBLIC_DIR.'main/persons/';
$_cfg['main::persons_url']=_PUBLIC_URL.'main/persons/';

$_cfg['main::films_dir']=_PUBLIC_DIR.'main/films/';
$_cfg['main::films_url']=_PUBLIC_URL.'main/films/';
$_cfg['main::films_url_new']=_PUBLIC_URL.'main/resize.php?f=';

$_cfg['main::persons_url_new']=_PUBLIC_URL.'main/resize2.php?f=';

$_cfg['main::cinemas_dir']=_PUBLIC_DIR.'main/cinemas/';
$_cfg['main::cinemas_url']=_PUBLIC_URL.'main/cinemas/';

$_cfg['main::contest_dir']=_PUBLIC_DIR.'main/contest/';
$_cfg['main::contest_url']=_PUBLIC_URL.'main/contest/';

$_cfg['main::reviews_dir']=_PUBLIC_DIR.'main/reviews/';
$_cfg['main::reviews_url']=_PUBLIC_URL.'main/reviews/';

$_cfg['main::concurs_dir']=_PUBLIC_DIR.'main/concurs/';
$_cfg['main::concurs_url']=_PUBLIC_URL.'main/concurs/';

$_cfg['main::news_dir']=_PUBLIC_DIR.'main/news/';
$_cfg['main::news_url']=_PUBLIC_URL.'main/news/';

$_cfg['main::anounce_dir']=_PUBLIC_DIR.'main/gallery/';
$_cfg['main::anounce_url']=_PUBLIC_URL.'main/gallery/';

$_cfg['main::articles_dir']=_PUBLIC_DIR.'main/articles/';
$_cfg['main::articles_url']=_PUBLIC_URL.'main/articles/';

$_cfg['main::serials_dir']=_PUBLIC_DIR.'main/serials/';
$_cfg['main::serials_url']=_PUBLIC_URL.'main/serials/';

$_cfg['main::gossip_dir']=_PUBLIC_DIR.'main/gossip/';
$_cfg['main::gossip_url']=_PUBLIC_URL.'main/gossip/';

$_cfg['main::interview_dir']=_PUBLIC_DIR.'main/interview/';
$_cfg['main::interview_url']=_PUBLIC_URL.'main/interview/';

$_cfg['main::box_dir']=_PUBLIC_DIR.'main/box/';
$_cfg['main::box_url']=_PUBLIC_URL.'main/box/';

$_cfg['main::cards_dir']=_PUBLIC_DIR.'main/cards/';
$_cfg['main::cards_url']=_PUBLIC_URL.'main/cards/';

$_cfg['main::currency']['usd']=sys::translate('main::usd');
$_cfg['main::currency']['grn']=sys::translate('main::grn');
$_cfg['main::currency']['eur']=sys::translate('main::eur');
$_cfg['main::currency']['rur']=sys::translate('main::rur');

$_cfg['main::users_dir']=_PUBLIC_DIR.'main/users/';
$_cfg['main::users_url']=_PUBLIC_URL.'main/users/';

$_cfg['main::films_on_search']=15;

?>