<?php
class main_distributors
{	
	static function deleteDistributor($distributor_id)
	{
		global $_db;
		$_db->delete('main_films_distributors',"`distributor_id`=".intval($distributor_id)."");
		$_db->delete('main_distributors',intval($distributor_id),true);
	}
	
	static function getDistributorsNames($arr_ids)
	{
		global $_db;
		$result=array();
		if(!$arr_ids || !count($arr_ids))
			return false;
		for($i=0; $i<count($arr_ids); $i++)
		{
			$arr_ids[$i]=intval($arr_ids[$i]);
		}
		$str_ids=implode(',',$arr_ids);
		$r=$_db->query("SELECT `id`, `name` 
			FROM `#__main_distributors` 
			WHERE `id` IN(".$str_ids.")");
		while($obj=$_db->fetchAssoc($r))
		{
			$result[$obj['id']]=$obj['name'];
		}
		return $result;
	}
}
?>