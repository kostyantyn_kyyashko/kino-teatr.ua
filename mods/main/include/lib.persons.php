<?php
class main_persons
{
	// Голосование зарегистрированного пользователя за персону
	// Только проверяется: есть ли уже запись о голосовании по этому персу и при необходимости
	// вставляется нулевая запись для него
	// Возвращает признак вставки, что равно по смыслу добавлению 1 голоса, и разницу между новым 
	// и старым значением рейтинга
	static function SetVoteRegistered($person_id=0, $user_id=0, $new_mark=0)
	{
		global $_db, $_cfg;

		if(intval($new_mark)<0) $new_mark=0;
		if(intval($new_mark)>10) $new_mark=10;

		$q = "
			SELECT `mark`
			FROM `#__main_persons_rating`
			WHERE `user_id`=".intval($user_id)."
			AND `person_id`=".intval($person_id)."
		";

		$r=$_db->query($q);
		if($obj=$_db->fetchAssoc($r))
		{
			$result['delta_mark'] = $new_mark - $obj['mark'];	// значение, на которое надо изменить рейтинг.
			$result['inserted'] = !$obj['mark'];		// вставлена новая запись или произведено только изменение значения голоса. 
		}
		else 
		{
		    $q = "
				INSERT INTO `#__main_persons_rating`
				(`user_id`,`person_id`,`mark`)
				VALUES
				(".intval($user_id).",".intval($person_id).",0)";
		    $_db->query($q);
		    
		    if($new_mark<0) $new_mark=0;
		    if($new_mark>10) $new_mark=10;
		    
			$result['delta_mark'] = $new_mark;	// значение, на которое надо изменить рейтинг.
			$result['inserted'] = true;		// вставлена новая запись или произведено только изменение значения голоса.
		}
		
		return $result;
	}
	
	// Голосование незарегистрированного пользователя за персону
	// Только проверяется: есть ли уже запись о голосовании по этому персу и при необходимости
	// вставляется запись-признак для него
	// Возвращает признак вставки, что равно по смыслу добавлению 1 голоса, и разницу между новым 
	// и старым значением рейтинга
	// Записи старее 10 часов удаляются
	static function SetVoteUnregistered($person_id=0, $new_mark=0)
	{
		global $_db, $_cfg;
		
		if(!$person_id) return 0;
		if(intval($new_mark)<0) $new_mark=0;
		if(intval($new_mark)>10) $new_mark=10;
		
		$ip = $_SERVER['REMOTE_ADDR'];
		$sid = session_id();
		$mark = $delta_mark = $id = 0;
		
		// Удалить устаревшие
		$q="
			DELETE FROM `#__main_persons_rating_unregistered` 
			WHERE DATE_ADD(dt, INTERVAL 10 HOUR)<now()
			";
		$_db->query($q);

		// Если для незарегистрированного пользователя для этого перса нет записи, то создать ее с нулями.
		$q = "
			SELECT *
			FROM `#__main_persons_rating`
			WHERE `user_id`=2
			AND `person_id`=".intval($person_id)."
		";

		$r=$_db->query($q);
		if(!$obj=$_db->fetchAssoc($r))
		{
		    $q = "
				INSERT INTO `#__main_persons_rating`
				(
					`user_id`,`person_id`,`mark`
				)
				VALUES
				(
					2,".intval($person_id).",0)";
		    $_db->query($q);
		}
			
		// Обеспечить уникальность данных для текущего незарегистрированного пользователя
		$q = "
			SELECT 
				`id`, 
				`mark`
			FROM `#__main_persons_rating_unregistered`
			WHERE 
			`person_id`='$person_id' AND `ip` = '$ip' AND `sid` = '$sid' 
		";
		
		$result['inserted'] = false;
		$r=$_db->query($q);		
		if($obj=$_db->fetchAssoc($r)) 
		{
			$id = $obj['id'];
			$mark=intval($obj['mark']);
			$result['inserted'] = !$obj['mark'];		// вставлена новая запись или произведено только изменение значения голоса. 
		}

		$delta_mark = $new_mark - $mark;
				
		if($id>0)
		{
		 $q = "
		    UPDATE `#__main_persons_rating_unregistered`
		    SET 
		     `mark`=$new_mark, `dt`=NOW() 
		    WHERE `id`='$id'; 
		 "; 
		}
		 else 
		{
		 $q = "
		    INSERT INTO `#__main_persons_rating_unregistered`
		        (`person_id`, `mark` , `ip` , `sid` , `dt` )
		    VALUES    
		    	('$person_id', '$new_mark', '$ip', '$sid', NOW())
		 "; 
		 $result['inserted'] = true;			// вставлена новая запись или произведено только изменение значения голоса.
		}
		 $_db->query($q);
		
		$result['delta_mark'] = $delta_mark;	// значение, на которое надо изменить рейтинг.		
		
		return $result; 
	}
	
	// Фиксирование результатов голосования
	// Входной параметр - массив с данными	
	// delta_mark - на сколько изменить существующее значение
	// inserted - добавлять ли голос
	// new_mark - новое значение, которое в посте
	// user_id - id пользователя
	// film_id - id фильма
	static function fixMarkPerson($marker)
	{
		global $_db, $_cfg;
		
		$result['yourMark'] = $marker['new_mark'];
		$q="
				UPDATE `#__main_persons_rating`
				SET `mark`='".$marker['new_mark']."'
				WHERE `user_id`='".$marker['user_id']."'
				AND `person_id`='".$marker['person_id']."';
			";
		$_db->query($q);

		$q="
			UPDATE `#__main_persons`
			SET
				`rating_votes`=`rating_votes`+'".($marker['inserted']?1:0)."',
				`rating_sum`=`rating_sum`+'".$marker['delta_mark']."',
				`rating`=`rating_sum`/`rating_votes`
			WHERE `id`='".$marker['person_id']."';";
		$_db->query($q);
		
		$q="SELECT `rating`, `rating_votes`, `rating_sum` FROM `#__main_persons` WHERE `id`='".$marker['person_id']."';";
		$r=$_db->query($q);		
		$pers=$_db->fetchAssoc($r);
   	    
		$result['votes']=$pers['rating_votes'];
		$result['rating']=self::formatPersonRating($pers['rating_sum']/$result['votes']);

		return $result;
	}	
	
	// Новая версия отображения рейтинга 
	static function showPersonRatingForm($person_id)
	{
		global $_user, $_db, $_cfg;

		$r=$_db->query("
			SELECT
				pers.id,
				pers.rating,
				pers.rating_votes,
				pers.rating_sum
			FROM
				`#__main_persons` AS `pers`
			LEFT JOIN `#__main_persons_lng` AS `pers_lng`
			ON pers_lng.record_id='".intval($person_id)."'
			AND pers_lng.lang_id='".intval(intval($_cfg['sys::lang_id']))."'
			WHERE pers.id='".intval($person_id)."'
		");
		$pers=$_db->fetchAssoc($r);

		if ($pers['rating']>0)
		{
			$var['votes']=$pers['rating_votes'];
			$var['rating']=$pers['rating_sum']/$var['votes'];
		} 

		$var['rest']=$var['rating']-floor($var['rating']);
		$var['object_id']=$pers['id'];
		$var['rating']=self::formatPersonRating($var['rating']);
		$var['ajx_type']='ajx_vote_person';
		$var['form_title']='person_rating';

		if($_user['id']==2)
		{
			$ip = $_SERVER['REMOTE_ADDR'];
			$sid = session_id();
			$var['yourMark'] = $_db->getValue('main_persons_rating_unregistered',"mark","person_id=".intval($person_id)." AND ip='$ip' AND sid='$sid'");
		}
		else 
			$var['yourMark'] = $_db->getValue('main_persons_rating',"mark","person_id=".intval($person_id)." AND user_id='".$_user['id']."'");
		if(!$var['yourMark']) $var['yourMark']="—";
		
		return sys::parseTpl('main::raiting_stars',$var);
	}
	

	static function deletePerson($person_id)
	{
		sys::useLib('main::discuss');
		global $_db, $_cfg;

		//Удалить ссылки в новостях
		$_db->delete('main_news_articles_persons',"`person_id`=".intval($person_id)."");

		$r=$_db->query("
			SELECT `id`
			FROM `#__main_persons_photos`
			WHERE `person_id`=".intval($person_id)."
		");
		while (list($photo_id)=$_db->fetchArray($r))
		{
			self::deletePhoto($photo_id);
		}

		$r=$_db->query("
			SELECT `id`
			FROM `#__main_persons_links`
			WHERE `person_id`=".intval($person_id)."
		");
		while ($link_id=$_db->fetchArray($r))
		{
			self::deleteLink($link_id);
		}

		main_discuss::deleteDiscuss($person_id,'person');

		$_db->delete('main_persons',intval($person_id),true);
	}

	static function confirmPhoto($photo_id)
	{
		global $_db;
		$_db->setValue('main_persons_photos','public',1,intval($photo_id));
	}

	static function deletePhoto($photo_id)
	{
		global $_db;
		$image=$_db->getValue('main_persons_photos','image',$photo_id);
		self::deletePhotoImage($image);
		$_db->delete('main_persons_photos',intval($photo_id));
	}

	static function deletePhotoImage($filename)
	{
		global $_cfg;
		if(is_file($_cfg['main::persons_dir'].$filename))
			unlink($_cfg['main::persons_dir'].$filename);
		if(is_file($_cfg['main::persons_dir'].'x1_'.$filename))
			unlink($_cfg['main::persons_dir'].'x1_'.$filename);
		if(is_file($_cfg['main::persons_dir'].'x2_'.$filename))
			unlink($_cfg['main::persons_dir'].'x2_'.$filename);
		if(is_file($_cfg['main::persons_dir'].'x3_'.$filename))
			unlink($_cfg['main::persons_dir'].'x3_'.$filename);
		if(is_file($_cfg['main::persons_dir'].'x4_'.$filename))
			unlink($_cfg['main::persons_dir'].'x4_'.$filename);
		if(is_file($_cfg['main::persons_dir'].'x5_'.$filename))
			unlink($_cfg['main::persons_dir'].'x5_'.$filename);
	}

	static function uploadPhotoImage($file,$photo_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deletePhotoImage($current_image);

		if($image=sys::uploadFile($file,$_cfg['main::persons_dir'],'photo_'.$photo_id, true))
		{
			$_db->setValue('main_persons_photos','image',$image,$photo_id);
			self::resizePhotoImage($image);
		}
		return $image;
	}

	static function resizePhotoImage($filename)
	{
		global $_cfg;
		sys::resizeImage($_cfg['main::persons_dir'].$filename,$_cfg['main::persons_dir'].'x1_'.$filename,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		sys::resizeImage($_cfg['main::persons_dir'].$filename,$_cfg['main::persons_dir'].'x2_'.$filename,$_cfg['main::x2_width'],$_cfg['main::x2_height']);
		sys::resizeImage($_cfg['main::persons_dir'].$filename,$_cfg['main::persons_dir'].'x3_'.$filename,$_cfg['main::x3_width'],$_cfg['main::x3_height']);
		sys::resizeImage($_cfg['main::persons_dir'].$filename,$_cfg['main::persons_dir'].'x4_'.$filename,$_cfg['main::x4_width'],$_cfg['main::x4_height']);
		sys::resizeImage($_cfg['main::persons_dir'].$filename,$_cfg['main::persons_dir'].'x5_'.$filename,$_cfg['main::x5_width'],$_cfg['main::x5_height']);
	}

	static function uploadPhotos($person_id)
	{
		global $_db, $_cfg, $_user;
		for($i=0; $i<count($_FILES['photos']['name']); $i++)
		{
			$file=array();
			$file['name']=$_FILES['photos']['name'][$i];
			$file['type']=$_FILES['photos']['type'][$i];
			$file['tmp_name']=$_FILES['photos']['tmp_name'][$i];
			$file['error']=$_FILES['photos']['error'][$i];
			$file['size']=$_FILES['photos']['size'][$i];
			if($filename=sys::uploadFile($file,$_cfg['main::persons_dir'],uniqid('photo_')))
			{
				self::resizePhotoImage($filename);
				$order_number=$_db->getMaxOrderNumber('main_persons_photos',"`person_id`=".intval($person_id)."");
				$_db->query("INSERT INTO `#__main_persons_photos`
					(`id`,`person_id`,`user_id`,`image`,`order_number`)
					VALUES
					(0,".intval($person_id).",".$_user['id'].",'".$filename."',".$order_number.")");
			}
		}
	}

	static function deleteLink($link_id)
	{
		global $_db;
		$_db->delete('main_persons_links',intval($link_id),true);
	}

	static function showPersonAdminTabs($person_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('sys::info');
		$tabs['info']['url']='?mod=main&act=person_edit&id='.$person_id;

		$tabs['photos']['title']=sys::translate('main::photos');
		$tabs['photos']['url']='?mod=main&act=person_photos_table&person_id='.$person_id;

		$tabs['links']['title']=sys::translate('main::links');
		$tabs['links']['url']='?mod=main&act=person_links_table&person_id='.$person_id;

		return sys_gui::showTabs($tabs, $active_tab);
	}

	static function getPersonsNames($arr_ids)
	{
		global $_db;
		$result=array();
		if(!$arr_ids || !count($arr_ids))
			return false;
		for($i=0; $i<count($arr_ids); $i++)
		{
			$arr_ids[$i]=intval($arr_ids[$i]);
		}
		$str_ids=implode(',',$arr_ids);
		$r=$_db->query("SELECT `id`, `name`
			FROM `#__main_persons`
			WHERE `id` IN(".$str_ids.")");
		while($obj=$_db->fetchAssoc($r))
		{
			$result[$obj['id']]=$obj['name'];
		}
		return $result;
	}

	static function translitIt($str)
	{
	    $tr = array(
	        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
	        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
	        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
	        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
	        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
	        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
	        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	        "the"=>"","the "=>""," the"=>""," the "=>"-",
	        "The"=>"","The "=>""," The"=>""," The "=>"-",
	        "... "=> "", " - "=> "-"," "=> "-", "."=> "", "/"=> "-"
	    );
	    return strtr($str,$tr);
	}

	static function getPersonUrl($person_id, $lang=false)
	{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");

		$obj=$_db->fetchAssoc($r);
		return sys::getHumanUrl($person_id,$obj['name'],"person",$lang);
	}

	static function getPersonUkUrl($person_id)
	{
		return self::getPersonUrl($person_id, "uk");
	}

	static function getPersonRatingUrl($person_id)
	{
		if ($person_id>4200)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			    $urlstr = self::translitIt($obj['name']);
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);
		}



		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'person-rating/'.$urlstr.'-'.$person_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/person-rating/'.$urlstr.'-'.$person_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=person_rating&person_id='.$person_id);
		}
	}

	static function formatPersonRating($rating)
	{
		if($rating)
			return number_format($rating,1,'.',' ');
		else
			return 0;
	}

	static function getPersonFirstPhoto($person_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT `id`, `image` FROM `#__main_persons_photos`
			WHERE `person_id`=".intval($person_id)."
			ORDER BY `order_number` ASC
			LIMIT 0,1
		");
		$result=$_db->fetchAssoc($r);
		return $result;
	}

	static function getPhotoUrl($photo_id)
	{
		return sys::rewriteUrl('?mod=main&act=person_photo&photo_id='.$photo_id);
	}

	static function getPosterUrl($poster_id)
	{
		return sys::rewriteUrl('?mod=main&act=person_posters&poster_id='.$poster_id);
	}

	static function getPhotoEditUrl($photo_id)
	{
		return sys::rewriteUrl('?mod=main&act=person_photo_edit&photo_id='.$photo_id);
	}

	static function getPhotoAddUrl($person_id)
	{
		return sys::rewriteUrl('?mod=main&act=person_photo_edit&person_id='.$person_id);
	}
	
	static function getPersonAddUrl()
	{
		global $_cfg;
		$lang=$_cfg['sys::lang'];

		if ($lang=='ru') return _ROOT_URL.'ru/main/person_edit.phtml';
		 else return _ROOT_URL.$lang.'/main/person_edit.phtml';		
	}
	
	static function getPersonsUrl()
	{
		return _ROOT_URL.'persons.phtml';
	}
	

	static function getPersonLinks($person_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT lnk_lng.title, lnk.url
			FROM `#__main_persons_links` AS `lnk`
			LEFT JOIN `#__main_persons_links_lng` AS `lnk_lng`
			ON lnk_lng.record_id=lnk.id
			AND lnk_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE lnk.person_id=".intval($person_id)."
		");
		$result=array();
		while ($obj=$_db->fetchArray($r))
		{
			$result[]=$obj;
		}
		return $result;
	}

	static function getTrailersUrl($trailer_id,$person_id,$rewrite=false)
	{
		$url='?mod=main&act=person_trailers&&person_id='.$person_id.'&trailer_id='.$trailer_id;
		if($rewrite)
			return sys::rewriteUrl($url);
		else
			return $url;
	}

	static function getFramesUrl($frame_id, $person_id, $rewrite=false)
	{
		$url='?mod=main&act=person_frames&person_id='.$person_id.'&photo_id='.$frame_id;
		if($rewrite)
			return sys::rewriteUrl($url);
		else
			return $url;
	}

	static function getWallpapersUrl($wallpaper_id, $person_id, $rewrite=false)
	{
		$url='?mod=main&act=person_wallpapers&person_id='.$person_id.'&wallpaper_id='.$wallpaper_id;
		if($rewrite)
			return sys::rewriteUrl($url);
		else
			return $url;
	}

	static function getPersonNewsUrl($person_id,$lang=false)
	{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");
		$obj=$_db->fetchAssoc($r);
		return sys::getHumanUrl($person_id,$obj['name'],"person_news",$lang);
	}

	static function getPersonPhotosUrl($person_id, $lang=false)
	{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");
		$obj=$_db->fetchAssoc($r);
		return sys::getHumanUrl($person_id,$obj['name'],"person_photos",$lang);
	}

	static function getPersonPostersUrl($person_id, $lang=false)
	{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");
		$obj=$_db->fetchAssoc($r);
		return sys::getHumanUrl($person_id,$obj['name'],"person_posters",$lang);
	}

	static function getPersonPhotoUrl($person_id, $lang=false)
	{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");
		$obj=$_db->fetchAssoc($r);
		return sys::getHumanUrl($person_id,$obj['name'],"person_photo",$lang);
	}

	static function getPersonFramesUrl($person_id, $lang=false)
	{
		global $_cfg, $_db;
		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");

		$obj=$_db->fetchAssoc($r);
		return sys::getHumanUrl($person_id,$obj['name'],"person_frams",$lang);
	}

	static function getPersonFrameUrl($person_id, $lang=false)
	{
		global $_cfg, $_db;
		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");

		$obj=$_db->fetchAssoc($r);
		return sys::getHumanUrl($person_id,$obj['name'],"person_fram",$lang);
	}

	static function getPersonWallpapersUrl($person_id, $lang=false)
	{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");
		$obj=$_db->fetchAssoc($r);
		return sys::getHumanUrl($person_id,$obj['name'],"person_wallpapers",$lang);
	}

	static function getPersonWallpaperUrl($person_id, $lang=false)
	{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");
		$obj=$_db->fetchAssoc($r);
		return sys::getHumanUrl($person_id,$obj['name'],"person_wallpaper",$lang);
	}
	
	static function getPersonTrailersUrl($person_id,$lang=false)
	{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");
		$obj=$_db->fetchAssoc($r);
		return sys::getHumanUrl($person_id,$obj['name'],"person_trailers",$lang);
	}

	static function getPersonTrailerUrl($person_id,$lang=false)
	{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");
		$obj=$_db->fetchAssoc($r);
		return sys::getHumanUrl($person_id,$obj['name'],"person_trailer",$lang);
	}

	static function getPersonBiographyUrl($person_id)
	{
		return sys::rewriteUrl('?mod=main&act=person_biography&person_id='.$person_id);
	}

	static function getPersonAwardsUrl($person_id)
	{
		if ($person_id>4200)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			    $urlstr = self::translitIt($obj['name']);
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);
		}



		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'person-awards/'.$urlstr.'-'.$person_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/person-awards/'.$urlstr.'-'.$person_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=person_awards&person_id='.$person_id);
		}
	}

	static function getPersonDiscussUrl($person_id)
	{
		if ($person_id>4200)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			    $urlstr = self::translitIt($obj['name']);
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);
		}



		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'person/'.$urlstr.'-'.$person_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/person/'.$urlstr.'-'.$person_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=person&person_id='.$person_id);
		}
	}

	static function getPersonFilmsUrl($person_id, $lang=false)
	{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name
			FROM `#__main_persons`
			WHERE id='".$person_id."'
		");
		$obj=$_db->fetchAssoc($r);
		return sys::getHumanUrl($person_id,$obj['name'],"person_films",$lang);
	}

	static function showPersonMedia($person_id, $person_fio)
	{
		global $_db, $_cfg;
		$var['title']=$person_fio;
		$r=$_db->query("
			SELECT trl.id,trl.image FROM `#__main_films_trailers` AS `trl`
			LEFT JOIN `#__main_films_trailers_persons` AS `trl_prs`
			ON trl_prs.trailer_id=trl.id AND trl_prs.person_id=".intval($person_id)."
			WHERE trl_prs.person_id=".intval($person_id)."
			AND trl.public=1
			ORDER BY RAND()
		");
		$var['trailer']=$_db->fetchAssoc($r);
		if($var['trailer'])
		{
			$var['trailer']['alt']=$person_fio.'::'.sys::translate('main::trailers');
			$var['trailer']['url']=self::getTrailersUrl($var['trailer']['id'],$person_id);
			$var['trailer']['image']=$_cfg['main::films_url_new'].$var['trailer']['image'].'&w=214';
		}


		$r=$_db->query("
			SELECT pht.id, pht.image FROM `#__main_films_photos` AS `pht`
			LEFT JOIN `#__main_films_photos_persons` AS `pht_prs`
			ON pht_prs.photo_id=pht.id AND pht_prs.person_id=".intval($person_id)."
			WHERE pht_prs.person_id=".intval($person_id)."
			AND pht.public=1
			ORDER BY RAND()
		");
		$var['photo']=$_db->fetchAssoc($r);
		if($var['photo'])
		{
			$var['photo']['alt']=$person_fio.'::'.sys::translate('main::frames');
			$var['photo']['url']=self::getFramesUrl($var['photo']['id'],$person_id);
			$var['photo']['image']=$_cfg['main::films_url_new'].$var['photo']['image'].'&w=214';
		}

		$r=$_db->query("
			SELECT wlp.id,wlp.image FROM `#__main_films_wallpapers` AS `wlp`
			LEFT JOIN `#__main_films_wallpapers_persons` AS `wlp_prs`
			ON wlp_prs.wallpaper_id=wlp.id AND wlp_prs.person_id=".intval($person_id)."
			WHERE wlp_prs.person_id=".intval($person_id)."
			AND wlp.public=1
			ORDER BY RAND()
		");
		$var['wallpaper']=$_db->fetchAssoc($r);
		if($var['wallpaper'])
		{
			$var['wallpaper']['alt']=$person_fio.'::'.sys::translate('main::wallpapers');
			$var['wallpaper']['url']=self::getWallpapersUrl($var['wallpaper']['id'], $person_id);
			$var['wallpaper']['image']=$_cfg['main::films_url_new'].$var['wallpaper']['image'].'&w=214';
		}
		return sys::parseTpl('main::media',$var);

	}

	static function isPersonNews($person_id)
	{
		global $_db;
		$r=$_db->query("SELECT `person_id`
			FROM `#__main_news_articles_persons`
			WHERE `person_id`=".intval($person_id)."
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isPersonAwards($person_id)
	{
		global $_db;
		$r=$_db->query("SELECT `person_id`
			FROM `#__main_films_awards_persons`
			WHERE `person_id`=".intval($person_id)."
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isPersonPhotos($person_id)
	{
		global $_db;
		$r=$_db->query("SELECT `person_id`
			FROM `#__main_persons_photos`
			WHERE `person_id`=".intval($person_id)."
			AND `public`=1
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isPersonPosters($person_id)
	{
		global $_db;
		$r=$_db->query("SELECT `person_id`
			FROM `#__main_persons_posters`
			WHERE `person_id`=".intval($person_id)."
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isPersonWallpapers($person_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT wlp.id
			FROM `#__main_films_wallpapers_persons` AS `wlp_prs`

			LEFT JOIN `#__main_films_wallpapers` AS `wlp`
			ON wlp.id=wlp_prs.wallpaper_id

			WHERE wlp_prs.person_id=".intval($person_id)."
			AND wlp.public=1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isPersonFrames($person_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT pht.id
			FROM `#__main_films_photos_persons` AS `pht_prs`

			LEFT JOIN `#__main_films_photos` AS `pht`
			ON pht.id=pht_prs.photo_id

			WHERE pht_prs.person_id=".intval($person_id)."
			AND pht.public=1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isPersonTrailers($person_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT trl.id
			FROM `#__main_films_trailers_persons` AS `trl_prs`

			LEFT JOIN `#__main_films_trailers` AS `trl`
			ON trl.id=trl_prs.trailer_id

			WHERE trl_prs.person_id=".intval($person_id)."
			AND trl.public=1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isPersonBiography($person_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT biography
			FROM `#__main_persons_lng`

			WHERE record_id=".intval($person_id)."

		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}



	static function isPersonFilms($person_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id`
			FROM `#__main_films_persons`
			WHERE `person_id`=".intval($person_id)."
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function showPersonTabs($person_id,$person_fio,$active_tab=false)
	{
		$person_url = self::getPersonUrl($person_id);	// http://kino-teatr.ua/person/meyer-stephenie-1662.phtml		
		$a_url = explode("/person/",$person_url);
		
		$var['active']=$active_tab;
		$var['objects']['info']['title']=sys::translate('main::info');
		$var['objects']['info']['url']=$person_url;
		$var['objects']['info']['alt']=$person_fio;

		if(self::isPersonFilms($person_id))
		{
			$var['objects']['films']['title']=sys::translate('main::filmography');
			$var['objects']['films']['url']=$a_url[0]."/person_films/".$a_url[1];
//			$var['objects']['films']['url']=self::getPersonFilmsUrl($person_id);	// http://kino-teatr.ua/ru/main/person_films/person_id/1662.phtml
			$var['objects']['films']['alt']=$person_fio.'::'.sys::translate('main::filmography');
		}

		if(self::isPersonNews($person_id))
		{
			$var['objects']['news']['title']=sys::translate('main::texts');
			$var['objects']['news']['url']=$a_url[0]."/person_news/".$a_url[1];
//			$var['objects']['news']['url']=self::getPersonNewsUrl($person_id);
			$var['objects']['news']['alt']=$person_fio.'::'.sys::translate('main::texts');
		}

		if(self::isPersonPhotos($person_id))
		{
			$var['objects']['photos']['title']=sys::translate('main::photos');
			$var['objects']['photos']['url']=$a_url[0]."/person_photos/".$a_url[1];
//			$var['objects']['photos']['url']=self::getPersonPhotosUrl($person_id);
			$var['objects']['photos']['alt']=$person_fio.'::'.sys::translate('main::photos');
		}

		if(self::isPersonPosters($person_id))
		{
			$var['objects']['posters']['title']=sys::translate('main::posters');
			$var['objects']['posters']['url']=$a_url[0]."/person_posters/".$a_url[1];
			$var['objects']['posters']['alt']=$person_fio.'::'.sys::translate('main::posters');
		}
		
		if(self::isPersonFrames($person_id))
		{
			$var['objects']['frames']['title']=sys::translate('main::frames');
			$var['objects']['frames']['url']=$a_url[0]."/person_frams/".$a_url[1];
//			$var['objects']['frames']['url']=self::getPersonFramesUrl($person_id);
			$var['objects']['frames']['alt']=$person_fio.'::'.sys::translate('main::frames');
		}

		if(self::isPersonWallpapers($person_id))
		{
			$var['objects']['wallpapers']['title']=sys::translate('main::wallpapers');
			$var['objects']['wallpapers']['url']=$a_url[0]."/person_wallpapers/".$a_url[1];
//			$var['objects']['wallpapers']['url']=self::getPersonWallpapersUrl($person_id);
			$var['objects']['wallpapers']['alt']=$person_fio.'::'.sys::translate('main::wallpapers');
		}

		if(self::isPersonTrailers($person_id))
		{
			$var['objects']['trailers']['title']=sys::translate('main::trailers');
			$var['objects']['trailers']['url']=$a_url[0]."/person_trailers/".$a_url[1];
//			$var['objects']['trailers']['url']=self::getPersonTrailersUrl($person_id);
			$var['objects']['trailers']['alt']=$person_fio.'::'.sys::translate('main::trailers');
		}

		if(self::isPersonAwards($person_id))
		{
			$var['objects']['awards']['title']=sys::translate('main::awards');
			$var['objects']['awards']['url']=$a_url[0]."/person_awards/".$a_url[1];
			$var['objects']['awards']['alt']=$person_fio.'::'.sys::translate('main::awards');
		}

		return sys::parseTpl('main::tabs',$var);
	}


	static function personFilms($person_id)
	{
		sys::useLib('main::films');
		global $_db, $_cfg, $_err, $_user, $_cookie;


		$result['person']=self::getPersonFio($person_id);

		$r=$_db->query("
			SELECT
				prf_lng.title AS `profession`,
				flm_prs.profession_id,
				flm_prs_lng.role,
				flm.year,
				flm.rating,
				flm.id AS `film_id`,
				flm_lng.title AS `film`
			FROM
				`#__main_films_persons` AS `flm_prs`

			LEFT JOIN
				`#__main_professions` AS `prf`
			ON
				prf.id=flm_prs.profession_id

			LEFT JOIN
				`#__main_professions_lng` AS `prf_lng`
			ON
				prf_lng.record_id=flm_prs.profession_id
			AND
				prf_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN
				`#__main_films` AS `flm`
			ON
				flm.id=flm_prs.film_id

			LEFT JOIN
				`#__main_films_lng` AS `flm_lng`
			ON
				flm_lng.record_id=flm_prs.film_id
			AND
				flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN
				`#__main_films_persons_lng` AS `flm_prs_lng`
			ON
				flm_prs_lng.record_id=flm_prs.id
			AND
				flm_prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE
				flm_prs.person_id=".intval($person_id)."

			ORDER BY prf.order_number, flm.year DESC

		");

		$result['professions']=array();
		while ($obj=$_db->fetchAssoc($r))
		{

			if(main_films::isFilmTrailers($obj['film_id']))
			{
				$obj['trailers']=main_films::getFilmTrailersUrl($obj['film_id']);
			}

				if ($obj['rating']>0)
				{
					$obj['rate'] = round($obj['rating'], 2);
				} else {
					$obj['rate'] = 0;
				}


			$obj['film']=htmlspecialchars($obj['film']);
			$obj['profession']=htmlspecialchars($obj['profession']);
			$obj['role']=htmlspecialchars($obj['role']);
			$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
			$result['professions'][$obj['profession_id']]['title']=$obj['profession'];
			$result['professions'][$obj['profession_id']]['objects'][]=$obj;
		}


		return sys::parseTpl('main::personFilms',$result);

	}

	static function markPerson($person_id, $mark, $user_id)
	{
		global $_db, $_cfg;

		if(intval($mark)<1 || intval($mark)>10)
			return false;

		$r=$_db->query("
			SELECT `user_id`,`mark`
			FROM `#__main_persons_rating`
			WHERE `user_id`=".intval($user_id)."
			AND `person_id`=".intval($person_id)."
		");
		$vote=$_db->fetchAssoc($r);

		$r=$_db->query("
			SELECT
				`rating_sum` AS `sum`,
				`rating_votes` AS `votes`
			FROM `#__main_persons`
			WHERE `id`=".intval($person_id)."
		");
		$person=$_db->fetchAssoc($r);


		if($vote)
			$person['sum']=$person['sum']-$vote['mark']+$mark;
		else
		{
			$person['votes']=$person['votes']+1;
			$person['sum']=$person['sum']+$mark;
		}
		$person['rating']=($person['sum']/$person['votes']);

		$_db->begin();

		if($vote)
		{
			if(!$_db->query("
				UPDATE `#__main_persons_rating`
				SET `mark`='".intval($mark)."'
				WHERE `user_id`='".intval($user_id)."'
				AND `person_id`='".intval($person_id)."'
			"))
				return $_db->rollback();
		}
		else
		{
			if(!$_db->query("
				INSERT INTO `#__main_persons_rating`
				(
					`user_id`,
					`person_id`,
					`mark`
				)
				VALUES
				(
					".intval($user_id).",
					".intval($person_id).",
					".intval($mark)."
				)
			"))
				return $_db->rollback;
		}

		if(!$_db->query("
			UPDATE `#__main_persons`
			SET
				`rating_sum`='".$person['sum']."',
				`rating`='".$person['rating']."',
				`rating_votes`=".$person['votes']."
			WHERE `id`='".$person_id."'
		"))
			return $_db->rollback();
		$_db->commit();
		return array('rating'=>self::formatPersonRating($person['rating']), 'votes'=>$person['votes']);

	}

	static function showPersonRatingForm_OLD($person_id)
	{
		global $_user, $_db, $_cfg;

		$r=$_db->query("
			SELECT `mark` FROM `#__main_persons_rating`
			WHERE `user_id`=".intval($_user['id'])."
			AND `person_id`=".intval($person_id)."
		");
		list($var['mark'])=$_db->fetchArray($r);

		if($var['mark'])
			$var['btn_name']=sys::translate('main::change_mark');
		else
			$var['btn_name']=sys::translate('main::to_mark');
		$r=$_db->query("
			SELECT `rating`, `rating_votes` FROM `#__main_persons`
			WHERE `id`='".intval($person_id)."'
		");
		list($var['rating'], $var['votes'])=$_db->fetchArray($r);
		$var['rest']=$var['rating']-floor($var['rating']);
		$var['object_id']=$person_id;
		$var['rating_url']=self::getPersonRatingUrl($film['id']);
		$var['rating']=self::formatPersonRating($var['rating']);
		$var['type']='person';

		return sys::parseTpl('main::mark_form_2',$var);
	}

	static function getPersonFio($person_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT CONCAT(`firstname`,' ',`lastname`)
			FROM `#__main_persons_lng`
			WHERE `record_id`=".intval($person_id)."
			AND `lang_id`=".intval($_cfg['sys::lang_id'])."
		");

		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getNextPhotoId($person_id, $order_number)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_persons_photos`
			WHERE `person_id`=".intval($person_id)."
			AND `order_number`>".intval($order_number)."
			AND `public`=1
			ORDER BY `order_number` ASC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getPrevPhotoId($person_id, $order_number)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_persons_photos`
			WHERE `person_id`=".intval($person_id)."
			AND `order_number`<".intval($order_number)."
			AND `public`=1
			ORDER BY `order_number` DESC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getPrevFrameId($person_id, $photo_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT pht_prs.photo_id FROM `#__main_films_photos_persons` AS `pht_prs`

			LEFT JOIN `#__main_films_photos` AS `pht`
			ON pht.id=pht_prs.photo_id

			WHERE pht_prs.person_id=".intval($person_id)."
			AND pht_prs.photo_id>".intval($photo_id)."
			AND pht.public=1
			ORDER BY pht_prs.photo_id ASC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getNextFrameId($person_id, $photo_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT pht_prs.photo_id FROM `#__main_films_photos_persons` AS `pht_prs`

			LEFT JOIN `#__main_films_photos` AS `pht`
			ON pht.id=pht_prs.photo_id

			WHERE pht_prs.person_id=".intval($person_id)."
			AND pht_prs.photo_id<".intval($photo_id)."
			AND pht.public=1
			ORDER BY pht_prs.photo_id DESC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getNextWallpaperId($person_id, $wallpaper_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT wlp_prs.wallpaper_id FROM `#__main_films_wallpapers_persons` AS `wlp_prs`

			LEFT JOIN `#__main_films_wallpapers` AS `wlp`
			ON wlp.id=wlp_prs.wallpaper_id

			WHERE wlp_prs.person_id=".intval($person_id)."
			AND wlp_prs.wallpaper_id<".intval($wallpaper_id)."
			AND wlp.public=1
			ORDER BY wlp_prs.wallpaper_id DESC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getPrevWallpaperId($person_id, $wallpaper_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT wlp_prs.wallpaper_id FROM `#__main_films_wallpapers_persons` AS `wlp_prs`

			LEFT JOIN `#__main_films_wallpapers` AS `wlp`
			ON wlp.id=wlp_prs.wallpaper_id

			WHERE wlp_prs.person_id=".intval($person_id)."
			AND wlp_prs.wallpaper_id>".intval($wallpaper_id)."
			AND wlp.public=1
			ORDER BY wlp_prs.wallpaper_id ASC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getNextTrailerId($person_id, $trailer_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT trl_prs.trailer_id FROM `#__main_films_trailers_persons` AS `trl_prs`

			LEFT JOIN `#__main_films_trailers` AS `trl`
			ON trl.id=trl_prs.trailer_id

			WHERE trl_prs.person_id=".intval($person_id)."
			AND trl_prs.trailer_id<".intval($trailer_id)."
			AND trl.public=1
			ORDER BY trl_prs.trailer_id DESC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getPrevTrailerId($person_id, $trailer_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT trl_prs.trailer_id FROM `#__main_films_trailers_persons` AS `trl_prs`

			LEFT JOIN `#__main_films_trailers` AS `trl`
			ON trl.id=trl_prs.trailer_id

			WHERE trl_prs.person_id=".intval($person_id)."
			AND trl_prs.trailer_id>".intval($trailer_id)."
			AND trl.public=1
			ORDER BY trl_prs.trailer_id ASC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getPhotosUrl($photo_id, $rewrite=true)
	{
		$url='?mod=main&act=person_photos&photo_id='.$photo_id;
		if($rewrite)
			return sys::rewriteUrl($url);
		else
			return $url;
	}
}
?>