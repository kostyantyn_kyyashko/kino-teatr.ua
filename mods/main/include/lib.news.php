<?php
class main_news
{
	static function linkArticleSpecThemes($article_id, $spec_themes)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_news_articles_spec_themes`
			WHERE `article_id`='".intval($article_id)."'");
		if($spec_themes)
		{
			$spec_themes=array_unique($spec_themes);
			foreach ($spec_themes as $spec_themes_id=>$name)
			{
				if($spec_themes_id)
				{
					$_db->query("INSERT INTO `#__main_news_articles_spec_themes`
						(
							`article_id`,
							`spec_theme_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($spec_themes_id)."
						)
					");
				}
			}
		}
	}

	static function getArticleSpecThemesIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `spec_theme_id`
			FROM `#__main_news_articles_spec_themes`
			WHERE `article_id`=".intval($article_id)."");
		while(list($id)=$_db->fetchArray($r))
		{
			$result[]=$id;
		}
		return $result;
	}

	static function deleteNews($news_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_news_articles`
			WHERE `news_id`=".intval($news_id)."
		");
		while(list($article_id)=$_db->fetchArray($r))
		{
			self::deleteArticle($article_id);
		}
		$_db->delete('main_news',intval($news_id),true);
	}

	static function confirmArticle($article_id)
	{
		global $_db;
		$_db->setValue('main_news_articles','public',1,intval($article_id));
	}


	static function showNewsAdminTabs($news_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('sys::info');
		$tabs['info']['url']='?mod=main&act=news_edit&id='.$news_id;

		$tabs['articles']['title']=sys::translate('main::articles');
		$tabs['articles']['url']='?mod=main&act=news_articles_table&news_id='.$news_id;

		return sys_gui::showTabs($tabs, $active_tab);
	}

	static function deleteArticleImage($filename)
	{
		global $_cfg;
		if(is_file($_cfg['main::news_dir'].$filename))
			unlink($_cfg['main::news_dir'].$filename);
		if(is_file($_cfg['main::news_dir'].'x0_'.$filename))
			unlink($_cfg['main::news_dir'].'x0_'.$filename);
		if(is_file($_cfg['main::news_dir'].'x1_'.$filename))
			unlink($_cfg['main::news_dir'].'x1_'.$filename);
		if(is_file($_cfg['main::news_dir'].'x2_'.$filename))
			unlink($_cfg['main::news_dir'].'x2_'.$filename);
		if(is_file($_cfg['main::news_dir'].'x3_'.$filename))
			unlink($_cfg['main::news_dir'].'x3_'.$filename);
		if(is_file($_cfg['main::news_dir'].'x5_'.$filename))
			unlink($_cfg['main::news_dir'].'x5_'.$filename);
	}

	static function deleteArticle($article_id)
	{
		sys::useLib('main::discuss');
		global $_db, $_cfg;
		$article=$_db->getRecord('main_news_articles',intval($article_id));

		if(is_file($_cfg['main::news_dir'].$article['small_image']))
			unlink($_cfg['main::news_dir'].$article['small_image']);

		self::deleteArticleImage($article['image']);

		$_db->delete('main_news_articles_persons',"`article_id`=".intval($article_id)."");
		$_db->delete('main_news_articles_films',"`article_id`=".intval($article_id)."");


		main_discuss::deleteDiscuss($article_id,'news_article');
		$_db->delete('main_news_index',"`article_id`=".intval($article_id)."");
		$_db->delete('main_news_articles',$article_id,true);
	}

	static function uploadArticleImage($file,$article_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deleteArticleImage($current_image);

		if($image=sys::uploadFile($file,$_cfg['main::news_dir'],'article_'.$article_id, true))
		{
			$_db->setValue('main_news_articles','image',$image,$article_id);
			self::resizeArticleImage($image);
		}
		return $image;
	}

	static function resizeArticleImage($filename)
	{
		global $_cfg;
		sys::resizeImage($_cfg['main::news_dir'].$filename,$_cfg['main::news_dir'].'x1_'.$filename,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		sys::resizeImage($_cfg['main::news_dir'].$filename,$_cfg['main::news_dir'].'x2_'.$filename,$_cfg['main::x2_width'],$_cfg['main::x2_height']);
		sys::resizeImage($_cfg['main::news_dir'].$filename,$_cfg['main::news_dir'].'x5_'.$filename,$_cfg['main::x5_width'],$_cfg['main::x5_height']);
	}

	static function uploadArticleSmallImage($file,$article_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image && is_file($_cfg['main::news_dir'].$current_image))
			unlink($_cfg['main::news_dir'].$current_image);

		if($image=sys::uploadFile($file,$_cfg['main::news_dir'],'x1_'.$article_id, true))
		{
		// inserted by Mike begin			
			$path_info=pathInfo($_cfg['main::news_dir'].$image);
			if(isset($path_info['extension']) && $path_info['extension'])
			{
				$extention = ".".strtolower($path_info['extension']);
				sys::resizeImage($_cfg['main::news_dir'].$image,$_cfg['main::news_dir'].'x0_article_'.$article_id.$extention,$_cfg['main::x0_width'],$_cfg['main::x0_height'],true); 
				sys::resizeImage($_cfg['main::news_dir'].$image,$_cfg['main::news_dir'].'x3_article_'.$article_id.$extention,$_cfg['main::x3_width'],$_cfg['main::x3_height'],true); 
			}
		// inserted by Mike end
			$_db->setValue('main_news_articles','small_image',$image,$article_id);
			sys::resizeImage($_cfg['main::news_dir'].$image,false,213,137);
		}
		return $image;
	}

	static function linkArticlePersons($article_id, $persons)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_news_articles_persons`
			WHERE `article_id`='".intval($article_id)."'");
		if($persons)
		{
			$persons=array_unique($persons);
			foreach ($persons as $person_id=>$name)
			{
				if($person_id)
				{
					$_db->query("INSERT INTO `#__main_news_articles_persons`
						(
							`article_id`,
							`person_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($person_id)."
						)
					");
				}
			}
		}
	}

	static function linkArticleFilms($article_id, $films)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_news_articles_films`
			WHERE `article_id`='".intval($article_id)."'");
		if($films)
		{
			$films=array_unique($films);
			foreach ($films as $film_id=>$name)
			{
				if($film_id)
				{
					$_db->query("INSERT INTO `#__main_news_articles_films`
						(
							`article_id`,
							`film_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($film_id)."
						)
					");
				}
			}
		}
	}

	static function linkArticleCinemas($article_id, $cinemas)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_news_articles_cinemas`
			WHERE `article_id`='".intval($article_id)."'");
		if($cinemas)
		{
			//$cinemas=array_unique($cinemas);
			foreach ($cinemas as $cinema_id=>$name)
			{
				if($cinema_id)
				{
					$_db->query("INSERT INTO `#__main_news_articles_cinemas`
						(
							`article_id`,
							`cinema_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($cinema_id)."
						)
					");
				}
			}
		}
	}

	static function getArticlePersonsIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `person_id`
			FROM `#__main_news_articles_persons`
			WHERE `article_id`=".intval($article_id)."");
		while(list($person_id)=$_db->fetchArray($r))
		{
			$result[]=$person_id;
		}
		return $result;
	}

	static function getArticleFilmsIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `film_id`
			FROM `#__main_news_articles_films`
			WHERE `article_id`=".intval($article_id)."");
		while(list($film_id)=$_db->fetchArray($r))
		{
			$result[]=$film_id;
		}
		return $result;
	}

	static function getArticleCinemasIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `cinema_id`
			FROM `#__main_news_articles_cinemas`
			WHERE `article_id`=".intval($article_id)."");
		while(list($film_id)=$_db->fetchArray($r))
		{
			$result[]=$film_id;
		}
		return $result;
	}

	static function translitIt($str)
	{
	    $tr = array(
	        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
	        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
	        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
	        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
	        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
	        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
	        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	        "the"=>"","the "=>""," the"=>""," the "=>"-",
	        "The"=>"","The "=>""," The"=>""," The "=>"-",
	        "... "=> "", " - "=> "-"," "=> "-", "."=> "", "/"=> "-"
	    );
	    return strtr($str,$tr);
	}

	static function getArticleUrl($article_id, $lang=false)
	{
		global $_cfg, $_db;

		if(!$lang) $lang=$_cfg['sys::lang'];
		
		if ($lang=='ru') 	$lang="";
		else 				$lang="$lang/";

		$r=$_db->query("
			SELECT name, news_id
			FROM `#__main_news_articles`
			WHERE id='".$article_id."'
		");

		if($obj=$_db->fetchAssoc($r))
		{
		    $news_id = $obj['news_id']; 
			$urlstr = self::translitIt(trim($obj['name']));
		    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);
			return ($news_id != 19)?(_ROOT_URL.$lang.'news/'.$urlstr.'-'.$article_id.'.phtml'):(sys::rewriteUrl('?mod=main&act=news_article_imax&article_id='.$article_id));
		}
		return sys::rewriteUrl('?mod=main&act=news_article&article_id='.$article_id);
	}

	static function getArticleUrl_OLD_TO_DELETE($article_id)
	{
		if ($article_id>3620)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, news_id
			FROM `#__main_news_articles`
			WHERE id='".$article_id."'
		");

		while($obj=$_db->fetchAssoc($r))
		{
			$news_id = $obj['news_id'];

			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);
		}

		$lang=$_cfg['sys::lang'];
        	if ($news_id != 19)
        	{
				if ($lang=='ru')
				{
				return _ROOT_URL.'news/article/'.$urlstr.'-'.$article_id.'.phtml';
				} else {
				return _ROOT_URL.$lang.'/news/article/'.$urlstr.'-'.$article_id.'.phtml';
				}
			} else {
				return sys::rewriteUrl('?mod=main&act=news_article_imax&article_id='.$article_id);
			}
		} else {
			global $_cfg, $_db;

			$r=$_db->query("
				SELECT name, news_id
				FROM `#__main_news_articles`
				WHERE id='".$article_id."'
			");

			while($obj=$_db->fetchAssoc($r))
			{
				$news_id = $obj['news_id'];
			}

		    if ($news_id != 19)
        	{
				return sys::rewriteUrl('?mod=main&act=news_article&article_id='.$article_id);
			} else {
				return sys::rewriteUrl('?mod=main&act=news_article_imax&article_id='.$article_id);
			}
		}
	}

	static function getArticleUkUrl($article_id)
	{
		if ($article_id>3620)
		{
			global $_cfg, $_db;

			$r=$_db->query("
				SELECT name, news_id
				FROM `#__main_news_articles`
				WHERE id='".$article_id."'
			");



			while($obj=$_db->fetchAssoc($r))
			{
				$news_id = $obj['news_id'];

				if ($obj['title_orig'])
				{
				    $urlstr = self::translitIt($obj['title_orig']);
				} else {
				    $urlstr = self::translitIt($obj['name']);
				}
				    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

			}



			$lang='uk';
	        	if ($news_id != 19)
	        	{
					return _ROOT_URL.$lang.'/news/article/'.$urlstr.'-'.$article_id.'.phtml';
				} else {
					return _ROOT_URL.'uk/main/news_article_imax/article_id/'.$article_id.'.phtml';
				}
		} else {
			global $_cfg, $_db;

			$r=$_db->query("
				SELECT name, news_id
				FROM `#__main_news_articles`
				WHERE id='".$article_id."'
			");



			while($obj=$_db->fetchAssoc($r))
			{
				$news_id = $obj['news_id'];
			}


		    if ($news_id != 19)
        	{
				return _ROOT_URL.'uk/main/news_article/article_id/'.$article_id.'.phtml';
			} else {
				return _ROOT_URL.'uk/main/news_article_imax/article_id/'.$article_id.'.phtml';
			}
		}
	}

	static function getArticleRuUrl($article_id)
	{
		if ($article_id>3620)
		{
			global $_cfg, $_db;

			$r=$_db->query("
				SELECT name, news_id
				FROM `#__main_news_articles`
				WHERE id='".$article_id."'
			");



			while($obj=$_db->fetchAssoc($r))
			{
				$news_id = $obj['news_id'];

				if ($obj['title_orig'])
				{
				    $urlstr = self::translitIt($obj['title_orig']);
				} else {
				    $urlstr = self::translitIt($obj['name']);
				}
				    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

			}



			$lang='ru';
	        	if ($news_id != 19)
	        	{
					return _ROOT_URL.'news/article/'.$urlstr.'-'.$article_id.'.phtml';
				} else {
					return _ROOT_URL.'main/news_article_imax/article_id/'.$article_id.'.phtml';
				}
		} else {
			global $_cfg, $_db;

			$r=$_db->query("
				SELECT name, news_id
				FROM `#__main_news_articles`
				WHERE id='".$article_id."'
			");



			while($obj=$_db->fetchAssoc($r))
			{
				$news_id = $obj['news_id'];
			}


		    if ($news_id != 19)
        	{
				return _ROOT_URL.'main/news_article/article_id/'.$article_id.'.phtml';
			} else {
				return _ROOT_URL.'main/news_article_imax/article_id/'.$article_id.'.phtml';
			}
		}
	}

	static function getNewsUrl($news_id)
	{
		return sys::rewriteUrl('?mod=main&act=news&id='.$news_id);
	}

	static function showLastPopularArticles()
	{
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_last_news_'.__FUNCTION__."_".$_cfg['sys::lang'].$_cfg['main::city_id'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::last_news_cache_period']*60))
			return $cache;


		$r=$_db->query("
			SELECT
				art.id,
				art.date,
				art.news_id,
				art.total_shows,
				art.today_shows,
				art.comments,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				art_lng.title,
				nws_lng.title AS `news`,
				art_lng.intro
			FROM `#__main_news_articles` AS `art`

			LEFT JOIN `#__main_news_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_news_lng` AS `nws_lng`
			ON nws_lng.record_id=art.news_id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_news` AS `nws`
			ON art.news_id=nws.id

			WHERE
			art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND art.date>'".gmdate('Y-m-d H:i:s', strtotime('- 30 days'))."'
			AND (art.city_id='".intval($_cfg['main::city_id'])."' OR art.city_id=0)
			AND nws.showtab=1
			ORDER BY art.total_shows DESC
			LIMIT 0,5
		");

		$i=1;

		while($obj=$_db->fetchAssoc($r))
		{
			$image = '';
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['intro']=sys::cutString(htmlspecialchars(strip_tags($obj['intro'])),140);
			$obj['shows']=$obj['total_shows'];
			$obj['news']=htmlspecialchars($obj['news']);
			
			if($obj['image'])
				$image=$_cfg['main::news_url'].$obj['image'];

	  	    $image="x".(($i==1)?3:0)."_article_".$obj['id'].".jpg";
			if(!file_exists('./public/main/news/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::news_url'].$image;
			$obj['image'] = $image;

			$obj['url']=self::getArticleUrl($obj['id']);
			$obj['news_url']=self::getNewsUrl($obj['news_id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';

			if ($i==3 || $i==5)
			{
			$obj['bottom']=' bottomNews';
			}

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::last_news',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function showLastDiscussedArticles()
	{
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_last_news_'.__FUNCTION__."_".$_cfg['sys::lang'].$_cfg['main::city_id'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::last_news_cache_period']*60))
			return $cache;


		$r=$_db->query("
			SELECT
				art.id,
				art.date,
				art.news_id,
				art.total_shows,
				art.comments,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				art_lng.title,
				nws_lng.title AS `news`,
				art_lng.intro
			FROM `#__main_news_articles` AS `art`

			LEFT JOIN `#__main_news_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_news_lng` AS `nws_lng`
			ON nws_lng.record_id=art.news_id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_news` AS `nws`
			ON art.news_id=nws.id

			WHERE
			art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND art.date>'".gmdate('Y-m-d H:i:s', strtotime('- 30 days'))."'
			AND (art.city_id='".intval($_cfg['main::city_id'])."' OR art.city_id=0)
			AND nws.showtab=1
			AND art.id != '1461'
			ORDER BY art.comments DESC
			LIMIT 0,5
		");

		$i=1;

		while($obj=$_db->fetchAssoc($r))
		{
			$image = '';
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['intro']=sys::cutString(htmlspecialchars(strip_tags($obj['intro'])),140);
			$obj['shows']=$obj['total_shows'];
			$obj['news']=htmlspecialchars($obj['news']);

// comment by Mike
/*		if($obj['image'])
			$image=$_cfg['main::news_url'].$obj['image'];

		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));

		if ($wid<213 && $obj['big_image'])
		{
		 	$image = $_cfg['main::news_url'].$obj['big_image'];
		} else if ($wid>=213)
		{
		 	$image = $_cfg['main::news_url'].$obj['image'];
		} else {
         	$image = 'blank_news_img.jpg';
		}

		if ($i==1)
		{
		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';
		} else {
		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=2';
		}
*/
		// inserted by Mike begin
  	    $image="x".(($i==1)?3:0)."_article_".$obj['id'].".jpg";
		if(!file_exists('./public/main/news/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::news_url'].$image;
		$obj['image'] = $image;
		// inserted by Mike end
		
			$obj['url']=self::getArticleUrl($obj['id']);
			$obj['news_url']=self::getNewsUrl($obj['news_id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';

			if ($i==3 || $i==5)
			{
			$obj['bottom']=' bottomNews';
			}

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::last_news',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}


	static function showLastArticles()
	{
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_last_news_'.__FUNCTION__."_".$_cfg['sys::lang'].$_cfg['main::city_id'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::last_news_cache_period']*60))
			return $cache;


		$r=$_db->query("
			SELECT
				art.id,
				art.date,
				art.news_id,
				art.total_shows,
				art.comments,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				spec.spec_theme_id as spec_theme,
				art_lng.title,
				nws_lng.title AS `news`,
				art_lng.intro
			FROM `#__main_news_articles` AS `art`

			LEFT JOIN `#__main_news_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_news_lng` AS `nws_lng`
			ON nws_lng.record_id=art.news_id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_news` AS `nws`
			ON art.news_id=nws.id

			LEFT JOIN `#__main_news_articles_spec_themes` AS `spec`
			ON art.id=spec.article_id

			WHERE art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND (art.city_id='".intval($_cfg['main::city_id'])."' OR art.city_id=0)
			AND nws.showtab=1
			ORDER BY art.date DESC
			LIMIT 0,5
		");

		$i=1;

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['intro']=sys::cutString(htmlspecialchars(strip_tags($obj['intro'])),140);
			$obj['shows']=$obj['total_shows'];
			$obj['news']=htmlspecialchars($obj['news']);

	  	    $image="x".(($i==1)?3:0)."_article_".$obj['id'].".jpg";
			if(!file_exists('./public/main/news/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::news_url'].$image;
			$obj['image'] = $image;

			$obj['url']=self::getArticleUrl($obj['id']);
			$obj['news_url']=self::getNewsUrl($obj['news_id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';

			if ($i==3 || $i==5)
			{
			$obj['bottom']=' bottomNews';
			}

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::last_news',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function getMonthBegin($month,$year)
	{
		return mktime(0,0,0,$month,1,$year);
	}

	static function getMonthEnd($month,$year)
	{
		return mktime(23,59,59,$month,date('t',self::getMonthBegin($month,$year)),$year);
	}

	static function getCalendar($year=false, $month=false, $news_id=false)
	{
		global $_db, $_cfg;

		$file=_CACHE_DIR.'main_news_calendar_'.$news_id.'.dat';
		if(is_file($file))
		{
			clearstatcache();
			if(date('m')==date('m',filemtime($file)) && $_cfg['main::cache_news_calendar'])
				return unserialize(file_get_contents($file));
			else
				unlink($file);

		}

		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$month=intval($month);
		$q="
			SELECT
				MAX(`date`),
				MIN(`date`)
			FROM `#__main_news_articles`
			WHERE `public`=1
		";
		if($news_id)
		{
			$q.="
				AND `news_id`=".intval($news_id)."
			";
		}
		$r=$_db->query($q);
		list($max_date, $min_date)=$_db->fetchArray($r);
		$min_date=sys::db2Timestamp($min_date);
		$max_date=sys::db2Timestamp($max_date);
		$result=array();
		if($min_date)
		{
			$min_year=date('Y',$min_date);
			$max_yar=date('Y',$max_date);

			for($y=$min_year; $y<$max_yar+1; $y++)
			{
				$obj['title']=$y;
				for($m=1; $m<13; $m++)
				{
					$obj['months'][$m]['title']=strftime('%b',self::getMonthBegin($m,$y));
					$obj['months'][$m]['is_news']=self::isNews($y,$m,$news_id);
				}
				$result[$y]=$obj;
			}
		}

		$result=array_reverse($result,true);
		if($_cfg['main::cache_news_calendar'])
			file_put_contents($file, serialize($result));
		return $result;
	}

	static function isNews($year, $month, $news_id=false)
	{
		global $_db, $_cfg;
		$month_begin=self::getMonthBegin($month,$year);
		$month_end=self::getMonthEnd($month,$year);

		$q="
			SELECT `id` FROM `#__main_news_articles`
			WHERE `date` BETWEEN '".date('Y-m-d H:i:s',$month_begin)."' AND '".date('Y-m-d H:i:s',$month_end)."'
		";
		if($news_id)
		{
			$q.="
				AND `news_id`=".intval($news_id)."
			";
		}
		$q.="
			AND (`city_id`=".intval($_cfg['main::city_id'])." OR `city_id`=0)
			LIMIT 0,1
		";

		$r=$_db->query($q);
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function showCalendar($year=false, $month=false, $news_id=false)
	{
		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$month=intval($month);

		$var['years']=self::getCalendar($year, $month, $news_id);

		foreach ($var['years'] as $y=>$arr_year)
		{
			foreach ($arr_year['months'] as $m=>$arr_month)
			{
				if($month==$m && $year==$y)
					$var['years'][$y]['months'][$m]['current']=true;
				else
					$var['years'][$y]['months'][$m]['current']=false;

				if($var['years'][$y]['months'][$m]['is_news'])
				{
					$url='?mod=main&act=news&year='.$y.'&month='.intval($m);
					if($news_id)
						$url.='&news_id='.$news_id;
					$var['years'][$y]['months'][$m]['url']=sys::rewriteUrl($url);
				}
				else
					$var['years'][$y]['months'][$m]['url']=false;
				$var['years'][$y]['months'][$m]['alt']=sys::translate('main::news_cinema').' '.sys::translate('main::for').' '.sys::translate('main::month_'.$m).' '.$y;
			}
		}
		return sys::parseTpl('main::news_calendar',$var);
	}

	static function showNewsTabs($year,$month,$news_id)
	{
		global $_db, $_cfg;

		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$var['active']=$news_id;

		$var['objects'][0]['title']=sys::translate('main::all_sections');
		$var['objects'][0]['url']=sys::rewriteUrl('?mod=main&act=news');
		$var['objects'][0]['alt']=sys::translate('main::news_cinema').'::'.sys::translate('main::all_sections');

		$r=$_db->query("
			SELECT nws.id, nws_lng.title
			FROM `#__main_news` AS `nws`

			LEFT JOIN `#__main_news_lng` AS `nws_lng`
			ON nws_lng.record_id=nws.id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE nws.showtab='1'

			ORDER BY nws.order_number
		");
		while ($obj=$_db->fetchAssoc($r))
		{
			$var['objects'][$obj['id']]['title']=$obj['title'];
			$var['objects'][$obj['id']]['url']=sys::rewriteUrl('?mod=main&act=news&news_id='.$obj['id']);
			$var['objects'][$obj['id']]['alt']=sys::translate('main::news_cinema').'::'.$obj['title'];
		}


		return sys::parseTpl('main::tabs',$var);
	}

	static function newsReirect($year, $month, $news_id=false)
	{
		if($year<2000)
			return false;
		if($month==1)
		{
			$month=12;
			$year=$year-1;
		}
		else
		{
			$month=$month-1;
		}
		if(self::isNews($year,$month,$news_id))
		{
			$url='?mod=main&act=news&year='.$year.'&month='.$month;
			if($news_id)
				$url.='&news_id='.$news_id;
			sys::redirect($url);
		}
		self::newsReirect($year, $month,$news_id);
	}

	static function getArticleAddUrl()
	{
		return sys::rewriteUrl('?mod=main&act=news_article_edit');
	}

	static function getArticleEditUrl($article_id)
	{
		return sys::rewriteUrl('?mod=main&act=news_article_edit&article_id='.$article_id);
	}

	static function getArticleDiscussUrl($article_id)
	{
		return sys::rewriteUrl('?mod=main&act=news_article&article_id='.$article_id);
	}

	static function isArticleDiscuss($article_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT COUNT(*) as count
			FROM `#__main_discuss_messages`

			WHERE object_type='news_article' AND object_id='".$article_id."'
		");
		while ($obj=$_db->fetchAssoc($r))
		{
			$count = $obj['count'];
		}

		return $count;
	}

	static function showArticleTabs($article_id,$article_title,$active_tab=false)
	{
		if(self::isArticleDiscuss($article_id))
		{
		$var['active']=$active_tab;
		$var['objects']['text']['title']=sys::translate('main::text');
		$var['objects']['text']['url']=self::getArticleUrl($article_id);
		$var['objects']['text']['alt']=$article_title;

		$var['objects']['discuss']['title']=sys::translate('main::discuss');
		$var['objects']['discuss']['url']=$var['objects']['text']['url']."#discuss";
   	  //$var['objects']['discuss']['url']=self::getArticleDiscussUrl($article_id)."#discuss";
		$var['objects']['discuss']['alt']=$article_title.'::'.sys::translate('main::discuss');


		return sys::parseTpl('main::tabs',$var);
		}
	}

	static function setArticleIndex($article_id,$values)
	{
		global $_db, $_cfg, $_langs;
		$_db->delete('main_news_index',"`article_id`=".intval($article_id)."");
		foreach ($_langs as $lang_id=>$lang)
		{
			$_db->query("
				INSERT INTO `#__main_news_index`
				(
					`article_id`,
					`lang_id`,
					`text`
				)
				VALUES
				(
					'".intval($article_id)."',
					'".$lang_id."',
					'".mysql_real_escape_string($values['title_'.$lang['code']])." ".strip_tags(mysql_real_escape_string($values['text_'.$lang['code']]))."'
				)

			");
		}

	}


}
?>