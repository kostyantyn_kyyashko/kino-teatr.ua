<?php
class main_reviews
{	
	static function deleteReview($review_id)
	{
		sys::useLib('main::films');
		sys::useLib('main::discuss');
		global $_db;
		
		$r=$_db->query("
			SELECT `film_id`, `image` 
			FROM `#__main_reviews`
			WHERE `id`='".intval($review_id)."'
		");
		
		list($film_id,$image)=$_db->fetchArray($r);
		self::deleteReviewImage($image);
		
		main_discuss::deleteDiscuss($review_id,'review');
			
		$_db->delete('main_reviews',intval($review_id));
		main_films::countFilmProRating($film_id);
	}
	
	static function getReviewsNames($arr_ids)
	{
		global $_db;
		$result=array();
		if(!$arr_ids || !count($arr_ids))
			return false;
		for($i=0; $i<count($arr_ids); $i++)
		{
			$arr_ids[$i]=intval($arr_ids[$i]);
		}
		$str_ids=implode(',',$arr_ids);
		$r=$_db->query("SELECT `id`, `name` 
			FROM `#__main_reviews` 
			WHERE `id` IN(".$str_ids.")");
		while($obj=$_db->fetchAssoc($r))
		{
			$result[$obj['id']]=$obj['name'];
		}
		return $result;
	}
	
	static function uploadReviewImage($file,$review_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deleteReviewImage($current_image);
			
		if($image=sys::uploadFile($file,$_cfg['main::reviews_dir'],'review_'.$review_id, true))
		{
			$_db->setValue('main_reviews','image',$image,$review_id);
			self::resizeReviewImage($image);
		}
		return $image;
	}
	
	static function resizeReviewImage($filename)
	{
		global $_cfg;
		sys::resizeImage($_cfg['main::reviews_dir'].$filename,$_cfg['main::reviews_dir'].'x1_'.$filename,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		sys::resizeImage($_cfg['main::reviews_dir'].$filename,$_cfg['main::reviews_dir'].'x2_'.$filename,$_cfg['main::x2_width'],$_cfg['main::x2_height']);
		sys::resizeImage($_cfg['main::reviews_dir'].$filename,$_cfg['main::reviews_dir'].'x3_'.$filename,$_cfg['main::x3_width'],$_cfg['main::x3_height']);
	}
	
	static function deleteReviewImage($filename)
	{
		global $_cfg;
		if(is_file($_cfg['main::reviews_dir'].$filename))
			unlink($_cfg['main::reviews_dir'].$filename);
		if(is_file($_cfg['main::reviews_dir'].'x1_'.$filename))
			unlink($_cfg['main::reviews_dir'].'x1_'.$filename);
		if(is_file($_cfg['main::reviews_dir'].'x2_'.$filename))
			unlink($_cfg['main::reviews_dir'].'x2_'.$filename);
		if(is_file($_cfg['main::reviews_dir'].'x3_'.$filename))
			unlink($_cfg['main::reviews_dir'].'x3_'.$filename);
	}
	
	static function showTopRevews()
	{
		global $_cfg, $_db;
		$start_date=gmdate('Y-m-d H:i:s',time()-7*_DAY);
		$cache_name='main_reviews_top_'.$_cfg['sys::lang'].'.html';
		
		if($cache=sys::getCache($cache_name,$_cfg['main::top_cache_period']*_HOUR))
			return $cache;
		$r=$_db->query("
			SELECT rev.title, rev.id AS `id`, flm.title AS `film`, COUNT(msg.id) AS `num` 
			FROM `#__main_discuss_messages` AS `msg`
			LEFT JOIN `#__main_reviews` AS `rev`
			ON rev.id=msg.object_id 
			
			LEFT JOIN `#__main_films_lng` AS `flm`
			ON flm.record_id=rev.film_id
			AND flm.lang_id=".intval($_cfg['sys::lang_id'])."
			
			WHERE msg.date>'".$start_date."' AND msg.object_type='review'
			
			GROUP BY msg.object_id
			
			ORDER BY `num` DESC, `id` DESC
			
			LIMIT 0,6	
		");
		
		$var['objects']=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=self::getReviewUrl($obj['id']);
			$obj['alt']=sys::translate('main::review_on_film').' &laquo;'.htmlspecialchars($obj['film']).'&raquo;';
			$var['objects'][]=$obj;
		}
		$var['title']=sys::translate('main::reviews');
		$var['class']='graph2';
		
		$result=sys::parseTpl('main::week_top',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}
	
	static function getReviewUrl($review_id)
	{
		return sys::rewriteUrl('?mod=main&act=review&review_id='.$review_id);
	}
	
	static function getReviewDiscussUrl($review_id)
	{
		return sys::rewriteUrl('?mod=main&act=review_discuss&review_id='.$review_id);
	}
	
	static function getReviewAddUrl($film_id)
	{
		return sys::rewriteUrl('?mod=main&act=review_edit&film_id='.$film_id);
	}
	
	static function getReviewEditUrl($review_id)
	{
		return sys::rewriteUrl('?mod=main&act=review_edit&review_id='.$review_id);
	}
	
	
	static function showLastReviews()
	{
		sys::useLib('main::films');
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_new_reviews_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::new_reviews_cache_period']*60))
			return $cache;
		$r=$_db->query("SELECT rev.id, rev.title, rev.film_id, rev.image, flm_lng.title AS `film`
				FROM `#__main_reviews` AS `rev`
				LEFT JOIN `#__main_films_lng` AS `flm_lng`
				ON flm_lng.record_id=rev.film_id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
				WHERE rev.public=1
				AND rev.date<'".gmdate('Y-m-d H:i:s')."'
				ORDER BY rev.date DESC LIMIT 0,6 
			");
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['film']=htmlspecialchars($obj['film']);
			if($obj['image'])
				$obj['image']=$_cfg['main::reviews_url'].'x1_'.$obj['image'];
			else 
				$obj['image']=main::getNoImage('x1');;
			$obj['url']=self::getReviewUrl($obj['id']);
			$obj['alt']=sys::translate('main::review_on_film').' &quot;'.$obj['film'].'&quot;';
			$obj['film_alt']=sys::translate('main::film').' &quot;'.$obj['film'].'&quot;';
			$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
			$var['objects'][]=$obj;
		}
		$result=sys::parseTpl('main::new_reviews',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}
	
	static function confirmReview($review_id)
	{
		global $_db;
		$_db->setValue('main_films_reviews','public',1,intval($review_id));
	}
	
	static function showReviewTabs($review_id,$review_title,$active_tab=false)
	{
		$var['active']=$active_tab;
		$var['objects']['text']['title']=sys::translate('main::text');
		$var['objects']['text']['url']=self::getReviewUrl($review_id);
		$var['objects']['text']['alt']=$review_title;
		
		$var['objects']['discuss']['title']=sys::translate('main::discuss');
		$var['objects']['discuss']['url']=self::getReviewDiscussUrl($review_id);
		$var['objects']['discuss']['alt']=$review_title.'::'.sys::translate('main::discuss');
		
		return sys::parseTpl('main::tabs',$var);
	}
}
?>