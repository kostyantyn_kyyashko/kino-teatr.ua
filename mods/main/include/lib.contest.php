<?php
class main_contest
{
	static function deletecontest($contest_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_contest_articles`
			WHERE `contest_id`=".intval($contest_id)."
		");
		while(list($article_id)=$_db->fetchArray($r))
		{
			self::deleteArticle($article_id);
		}
		$_db->delete('main_contest',intval($contest_id),true);
	}

	static function confirmArticle($article_id)
	{
		global $_db;
		$_db->setValue('main_contest_articles','public',1,intval($article_id));
	}

	static function showFrontEndContest()
	{
		global $_db,$_cfg;
	  	$q="
			SELECT
				art.id,
				art.date,
				art.book,
				art.cd,
				art.prizes,
				art.tickets,
				art.tshirt,
				art.finished,
				art.exclusive,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art_lng.title,
				art_lng.intro
			FROM `#__main_contest_articles` AS `art`

			LEFT JOIN
				`#__main_contest_articles_lng` AS `art_lng`
			ON
				art_lng.record_id=art.id
			AND
				art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_contest` AS `nws`
			ON art.contest_id=nws.id

			WHERE
				/*art.date BETWEEN '".$month_begin."' AND '".$month_end."'
				AND */(art.city_id=".intval($_cfg['main::city_id'])." OR art.city_id=0)
				AND art.public=1
				AND art.finished = 0
				AND art.date<'".gmdate('Y-m-d H:i:s')."'
				AND nws.showtab=1
		";
		if($_GET['contest_id'])
		{
			$q.="
				AND art.contest_id=".intval($_GET['contest_id'])."
			";
		}
		$q.="
			ORDER BY art.date DESC
		";

		$r=$_db->query($q);
		$result['objects']=array();
		$i = 1;
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['date']=date('d.m.Y',strtotime($obj['date']));
			$obj['shows']=$obj['total_shows'];
			if($obj['image'])
				$image=$_cfg['main::contest_url'].$obj['image'];

			list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$obj['image']));

			if ($wid<213 && $obj['big_image'])
			{
			 	$image = $_cfg['main::contest_url'].$obj['big_image'];
			} else if ($wid>=213)
			{
			 	$image = $_cfg['main::contest_url'].$obj['image'];
			} else {
	         	$image = 'blank_contest_img.jpg';
			}

			$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';



			$obj['title']=htmlspecialchars($obj['title']);
			$obj['url']=main_contest::getArticleUrl($obj['id']);

			if(sys::checkAccess('main::contest_edit') || $_user['id']==$obj['user_id'])
				$obj['edit_url']=main_contest::getArticleEditUrl($obj['id']);
			else
				$obj['edit_url']=false;

			if(sys::checkAccess('main::contest_delete') || $_user['id']==$obj['user_id'])
				$obj['delete']=true;
			else
				$obj['delete']=false;

			$obj['position'] = $i;
			$var['objects'][]=$obj;
			$i++;
		}

		$result=sys::parseTpl('main::front_contest',$var);
		return $result;


	}



	static function showcontestAdminTabs($contest_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('sys::info');
		$tabs['info']['url']='?mod=main&act=contest_edit&id='.$contest_id;

		$tabs['articles']['title']=sys::translate('main::contest');
		$tabs['articles']['url']='?mod=main&act=contest_articles_table&contest_id='.$contest_id;

		return sys_gui::showTabs($tabs, $active_tab);
	}

	static function deleteArticleImage($filename)
	{
		global $_cfg;
		if(is_file($_cfg['main::contest_dir'].$filename))
			unlink($_cfg['main::contest_dir'].$filename);
		if(is_file($_cfg['main::contest_dir'].'x1_'.$filename))
			unlink($_cfg['main::contest_dir'].'x1_'.$filename);
		if(is_file($_cfg['main::contest_dir'].'x2_'.$filename))
			unlink($_cfg['main::contest_dir'].'x2_'.$filename);
		if(is_file($_cfg['main::contest_dir'].'x3_'.$filename))
			unlink($_cfg['main::contest_dir'].'x3_'.$filename);
		if(is_file($_cfg['main::contest_dir'].'x5_'.$filename))
			unlink($_cfg['main::contest_dir'].'x5_'.$filename);
	}

	static function deleteArticle($article_id)
	{
		sys::useLib('main::discuss');
		global $_db, $_cfg;
		$article=$_db->getRecord('main_contest_articles',intval($article_id));

		if(is_file($_cfg['main::contest_dir'].$article['small_image']))
			unlink($_cfg['main::contest_dir'].$article['small_image']);

		self::deleteArticleImage($article['image']);

		$_db->delete('main_contest_articles_persons',"`article_id`=".intval($article_id)."");
		$_db->delete('main_contest_articles_films',"`article_id`=".intval($article_id)."");


		main_discuss::deleteDiscuss($article_id,'contest_article');
		$_db->delete('main_contest_index',"`article_id`=".intval($article_id)."");
		$_db->delete('main_contest_articles',$article_id,true);
		$_db->delete('main_contest_questions',$article_id,false,'contest_id');
		$_db->delete('main_contest_winners',$article_id,false,'contest_id');		
		$_db->delete('main_contest_participants',$article_id,false,'contest_id');		
	}

	static function deleteWinner($user_id, $contest_id)
	{
		sys::useLib('main::discuss');
		global $_db, $_cfg;
		$_db->delete('main_contest_winners',"`user_id`=".intval($user_id)." AND `contest_id`=".intval($contest_id)."");
	}

	static function deleteQuestion($question_id, $contest_id)
	{
		sys::useLib('main::discuss');
		global $_db, $_cfg;
		$_db->delete('main_contest_questions',"`id`=".intval($question_id)." AND `contest_id`=".intval($contest_id)."");
	}


	static function uploadArticleImage($file,$article_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deleteArticleImage($current_image);

		if($image=sys::uploadFile($file,$_cfg['main::contest_dir'],'article_'.$article_id, true))
		{
			$_db->setValue('main_contest_articles','image',$image,$article_id);
			self::resizeArticleImage($image);
		}
		return $image;
	}

	static function resizeArticleImage($filename)
	{
		global $_cfg;
		sys::resizeImage($_cfg['main::contest_dir'].$filename,$_cfg['main::contest_dir'].'x1_'.$filename,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		sys::resizeImage($_cfg['main::contest_dir'].$filename,$_cfg['main::contest_dir'].'x2_'.$filename,$_cfg['main::x2_width'],$_cfg['main::x2_height']);
		sys::resizeImage($_cfg['main::contest_dir'].$filename,$_cfg['main::contest_dir'].'x3_'.$filename,$_cfg['main::x3_width'],$_cfg['main::x3_height']);
		sys::resizeImage($_cfg['main::contest_dir'].$filename,$_cfg['main::contest_dir'].'x5_'.$filename,$_cfg['main::x5_width'],$_cfg['main::x5_height']);
	}

	static function uploadArticleSmallImage($file,$article_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image && is_file($_cfg['main::contest_dir'].$current_image))
			unlink($_cfg['main::contest_dir'].$current_image);

		if($image=sys::uploadFile($file,$_cfg['main::contest_dir'],'x1_'.$article_id, true))
		{
			$_db->setValue('main_contest_articles','small_image',$image,$article_id);
			sys::resizeImage($_cfg['main::contest_dir'].$image,false,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		}
		return $image;
	}

	static function linkArticlePersons($article_id, $persons)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_contest_articles_persons`
			WHERE `article_id`='".intval($article_id)."'");
		if($persons)
		{
			$persons=array_unique($persons);
			foreach ($persons as $person_id=>$name)
			{
				if($person_id)
				{
					$_db->query("INSERT INTO `#__main_contest_articles_persons`
						(
							`article_id`,
							`person_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($person_id)."
						)
					");
				}
			}
		}
	}

	static function linkArticleFilms($article_id, $films)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_contest_articles_films`
			WHERE `article_id`='".intval($article_id)."'");
		if($films)
		{
			$films=array_unique($films);
			foreach ($films as $film_id=>$name)
			{
				if($film_id)
				{
					$_db->query("INSERT INTO `#__main_contest_articles_films`
						(
							`article_id`,
							`film_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($film_id)."
						)
					");
				}
			}
		}
	}

	static function getContestArticleDiscussUrl($id)
	{
        $url = sys::rewriteUrl('?mod=main&act=contest_article&record_id='.$id);
        $pu = parse_url($url);
        $path = explode('/', $pu['path']);
        end($path);
        prev($path);
        $path[key($path)] = 'article_id';
		return preg_replace('/[\/]+$/', '', _ROOT_URL).implode('/', $path);
	}

	static function getArticlePersonsIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `person_id`
			FROM `#__main_contest_articles_persons`
			WHERE `article_id`=".intval($article_id)."");
		while(list($person_id)=$_db->fetchArray($r))
		{
			$result[]=$person_id;
		}
		return $result;
	}

	static function getArticleFilmsIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `film_id`
			FROM `#__main_contest_articles_films`
			WHERE `article_id`=".intval($article_id)."");
		while(list($film_id)=$_db->fetchArray($r))
		{
			$result[]=$film_id;
		}
		return $result;
	}

	static function translitIt($str)
	{
	    $tr = array(
	        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
	        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","�?"=>"i",
	        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
	        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
	        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
	        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
	        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	        "the"=>"","the "=>""," the"=>""," the "=>"-",
	        "The"=>"","The "=>""," The"=>""," The "=>"-",
	        "... "=> "", " - "=> "-"," "=> "-", "."=> "", "/"=> "-"
	    );
	    return strtr($str,$tr);
	}

	static function getArticleUrl($article_id)
	{
			return sys::rewriteUrl('?mod=main&act=contest_article&article_id='.$article_id);
	}

	static function getcontestUrl($contest_id)
	{
		return sys::rewriteUrl('?mod=main&act=contest&id='.$contest_id);
	}
	
	static function getArticleUkUrl($article_id)  // inserted by Mike
	{
			return _ROOT_URL.'uk/main/contest_article/article_id/'.$article_id.'.phtml';
	}

	static function getArticleRuUrl($article_id) // inserted by Mike
	{
			return _ROOT_URL.'ru/main/contest_article/article_id/'.$article_id.'.phtml';
	}

	static function showLastPopularArticles()
	{
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_last_contest_'.$_cfg['sys::lang'].$_cfg['main::city_id'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::last_contest_cache_period']*60))
			return $cache;


		$r=$_db->query("
			SELECT
				art.id,
				art.date,
				art.contest_id,
				art.total_shows,
				art.comments,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				art_lng.title,
				nws_lng.title AS `contest`,
				art_lng.intro
			FROM `#__main_contest_articles` AS `art`

			LEFT JOIN `#__main_contest_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_contest_lng` AS `nws_lng`
			ON nws_lng.record_id=art.contest_id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_contest` AS `nws`
			ON art.contest_id=nws.id

			WHERE
			art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND (art.city_id='".intval($_cfg['main::city_id'])."' OR art.city_id=0)
			AND nws.showtab=1
			AND art.id != '3544'
			ORDER BY art.today_shows DESC, art.total_shows DESC
			LIMIT 0,5
		");

		$i=1;

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['intro']=sys::cutString(htmlspecialchars(strip_tags($obj['intro'])),140);
			$obj['shows']=$obj['total_shows'];
			$obj['contest']=htmlspecialchars($obj['contest']);
		if($obj['image'])
			$image=$_cfg['main::contest_url'].$obj['image'];

		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));

		if ($wid<213 && $obj['big_image'])
		{
		 	$image = $_cfg['main::contest_url'].$obj['big_image'];
		} else if ($wid>=213)
		{
		 	$image = $_cfg['main::contest_url'].$obj['image'];
		} else {
         	$image = 'blank_contest_img.jpg';
		}

		if ($i==1)
		{
		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';
		} else {
		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=2';
		}
			$obj['url']=self::getArticleUrl($obj['id']);
			$obj['contest_url']=self::getcontestUrl($obj['contest_id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';

			if ($i==3 || $i==5)
			{
			$obj['bottom']=' bottomcontest';
			}

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::last_contest',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function showLastDiscussedArticles()
	{
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_last_contest_'.$_cfg['sys::lang'].$_cfg['main::city_id'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::last_contest_cache_period']*60))
			return $cache;


		$r=$_db->query("
			SELECT
				art.id,
				art.date,
				art.contest_id,
				art.total_shows,
				art.comments,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				art_lng.title,
				nws_lng.title AS `contest`,
				art_lng.intro
			FROM `#__main_contest_articles` AS `art`

			LEFT JOIN `#__main_contest_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_contest_lng` AS `nws_lng`
			ON nws_lng.record_id=art.contest_id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_contest` AS `nws`
			ON art.contest_id=nws.id

			WHERE
			art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND (art.city_id='".intval($_cfg['main::city_id'])."' OR art.city_id=0)
			AND nws.showtab=1
			AND art.id != '3544'
			ORDER BY art.comments DESC
			LIMIT 0,5
		");

		$i=1;

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['intro']=sys::cutString(htmlspecialchars(strip_tags($obj['intro'])),140);
			$obj['shows']=$obj['total_shows'];
			$obj['contest']=htmlspecialchars($obj['contest']);
		if($obj['image'])
			$image=$_cfg['main::contest_url'].$obj['image'];

		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));

		if ($wid<213 && $obj['big_image'])
		{
		 	$image = $_cfg['main::contest_url'].$obj['big_image'];
		} else if ($wid>=213)
		{
		 	$image = $_cfg['main::contest_url'].$obj['image'];
		} else {
         	$image = 'blank_contest_img.jpg';
		}

		if ($i==1)
		{
		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';
		} else {
		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=2';
		}
			$obj['url']=self::getArticleUrl($obj['id']);
			$obj['contest_url']=self::getcontestUrl($obj['contest_id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';

			if ($i==3 || $i==5)
			{
			$obj['bottom']=' bottomcontest';
			}

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::last_contest',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}


	static function showLastArticles()
	{
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_last_contest_'.$_cfg['sys::lang'].$_cfg['main::city_id'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::last_contest_cache_period']*60))
			return $cache;


		$r=$_db->query("
			SELECT
				art.id,
				art.date,
				art.contest_id,
				art.total_shows,
				art.comments,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				art_lng.title,
				nws_lng.title AS `contest`,
				art_lng.intro
			FROM `#__main_contest_articles` AS `art`

			LEFT JOIN `#__main_contest_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_contest_lng` AS `nws_lng`
			ON nws_lng.record_id=art.contest_id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_contest` AS `nws`
			ON art.contest_id=nws.id

			WHERE art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND (art.city_id='".intval($_cfg['main::city_id'])."' OR art.city_id=0)
			AND nws.showtab=1
			AND art.id != '3544'
			ORDER BY art.date DESC
			LIMIT 0,5
		");

		$i=1;

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['intro']=sys::cutString(htmlspecialchars(strip_tags($obj['intro'])),140);
			$obj['shows']=$obj['total_shows'];
			$obj['contest']=htmlspecialchars($obj['contest']);

		if($obj['image'])
			$image=$_cfg['main::contest_url'].$obj['image'];

		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));

		if ($wid<213 && $obj['big_image'])
		{
		 	$image = $_cfg['main::contest_url'].$obj['big_image'];
		} else if ($wid>=213)
		{
		 	$image = $_cfg['main::contest_url'].$obj['image'];
		} else {
         	$image = 'blank_contest_img.jpg';
		}

		if ($i==1)
		{
		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';
		} else {
		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=2';
		}


			$obj['url']=self::getArticleUrl($obj['id']);
			$obj['contest_url']=self::getcontestUrl($obj['contest_id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';

			if ($i==3 || $i==5)
			{
			$obj['bottom']=' bottomcontest';
			}

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::last_contest',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function getMonthBegin($month,$year)
	{
		return mktime(0,0,0,$month,1,$year);
	}

	static function getMonthEnd($month,$year)
	{
		return mktime(23,59,59,$month,date('t',self::getMonthBegin($month,$year)),$year);
	}

	static function getCalendar($year=false, $month=false, $contest_id=false)
	{
		global $_db, $_cfg;

		$file=_CACHE_DIR.'main_contest_calendar_'.$contest_id.'.dat';
		if(is_file($file))
		{
			clearstatcache();
			if(date('m')==date('m',filemtime($file)) && $_cfg['main::cache_contest_calendar'])
				return unserialize(file_get_contents($file));
			else
				unlink($file);

		}

		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$month=intval($month);
		$q="
			SELECT
				MAX(`date`),
				MIN(`date`)
			FROM `#__main_contest_articles`
			WHERE `public`=1
		";
		if($contest_id)
		{
			$q.="
				AND `contest_id`=".intval($contest_id)."
			";
		}
		$r=$_db->query($q);
		list($max_date, $min_date)=$_db->fetchArray($r);
		$min_date=sys::db2Timestamp($min_date);
		$max_date=sys::db2Timestamp($max_date);
		$result=array();
		if($min_date)
		{
			$min_year=date('Y',$min_date);
			$max_yar=date('Y',$max_date);

			for($y=$min_year; $y<$max_yar+1; $y++)
			{
				$obj['title']=$y;
				for($m=1; $m<13; $m++)
				{
					$obj['months'][$m]['title']=strftime('%b',self::getMonthBegin($m,$y));
					$obj['months'][$m]['is_contest']=self::iscontest($y,$m,$contest_id);
				}
				$result[$y]=$obj;
			}
		}

		$result=array_reverse($result,true);
		if($_cfg['main::cache_contest_calendar'])
			file_put_contents($file, serialize($result));
		return $result;
	}

	static function iscontest($year, $month, $contest_id=false)
	{
		global $_db, $_cfg;
		$month_begin=self::getMonthBegin($month,$year);
		$month_end=self::getMonthEnd($month,$year);

		$q="
			SELECT `id` FROM `#__main_contest_articles`
			WHERE `date` BETWEEN '".date('Y-m-d H:i:s',$month_begin)."' AND '".date('Y-m-d H:i:s',$month_end)."'
		";
		if($contest_id)
		{
			$q.="
				AND `contest_id`=".intval($contest_id)."
			";
		}
		$q.="
			AND (`city_id`=".intval($_cfg['main::city_id'])." OR `city_id`=0)
			LIMIT 0,1
		";

		$r=$_db->query($q);
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function showCalendar($year=false, $month=false, $contest_id=false)
	{
		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$month=intval($month);

		$var['years']=self::getCalendar($year, $month, $contest_id);

		foreach ($var['years'] as $y=>$arr_year)
		{
			foreach ($arr_year['months'] as $m=>$arr_month)
			{
				if($month==$m && $year==$y)
					$var['years'][$y]['months'][$m]['current']=true;
				else
					$var['years'][$y]['months'][$m]['current']=false;

				if($var['years'][$y]['months'][$m]['is_contest'])
				{
					$url='?mod=main&act=contest&year='.$y.'&month='.intval($m);
					if($contest_id)
						$url.='&contest_id='.$contest_id;
					$var['years'][$y]['months'][$m]['url']=sys::rewriteUrl($url);
				}
				else
					$var['years'][$y]['months'][$m]['url']=false;
				$var['years'][$y]['months'][$m]['alt']=sys::translate('main::contest_cinema').' '.sys::translate('main::for').' '.sys::translate('main::month_'.$m).' '.$y;
			}
		}
		return sys::parseTpl('main::contest_calendar',$var);
	}

	static function showcontestTabs($year,$month,$contest_id)
	{
		global $_db, $_cfg;

		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$var['active']=$contest_id;

		$var['objects'][0]['title']=sys::translate('main::all_sections');
		$var['objects'][0]['url']=sys::rewriteUrl('?mod=main&act=contest');
		$var['objects'][0]['alt']=sys::translate('main::contest_cinema').'::'.sys::translate('main::all_sections');

		$r=$_db->query("
			SELECT nws.id, nws_lng.title
			FROM `#__main_contest` AS `nws`

			LEFT JOIN `#__main_contest_lng` AS `nws_lng`
			ON nws_lng.record_id=nws.id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE nws.showtab='1'

			ORDER BY nws.order_number
		");
		while ($obj=$_db->fetchAssoc($r))
		{
			$var['objects'][$obj['id']]['title']=$obj['title'];
			$var['objects'][$obj['id']]['url']=sys::rewriteUrl('?mod=main&act=contest&contest_id='.$obj['id']);
			$var['objects'][$obj['id']]['alt']=sys::translate('main::contest_cinema').'::'.$obj['title'];
		}


		return sys::parseTpl('main::tabs',$var);
	}

	static function contestReirect($year, $month, $contest_id=false)
	{
		if($year<2000)
			return false;
		if($month==1)
		{
			$month=12;
			$year=$year-1;
		}
		else
		{
			$month=$month-1;
		}
		if(self::iscontest($year,$month,$contest_id))
		{
			$url='?mod=main&act=contest&year='.$year.'&month='.$month;
			if($contest_id)
				$url.='&contest_id='.$contest_id;
			sys::redirect($url);
		}
		self::contestReirect($year, $month,$contest_id);
	}

	static function getArticleAddUrl()
	{
		return sys::rewriteUrl('?mod=main&act=contest_article_edit');
	}

	static function getArticleEditUrl($article_id)
	{
		return sys::rewriteUrl('?mod=main&act=contest_article_edit&article_id='.$article_id);
	}

	static function getArticleDiscussUrl($article_id)
	{
		return sys::rewriteUrl('?mod=main&act=contest_article_discuss&article_id='.$article_id);
	}

	static function showArticleTabs($article_id,$article_title,$active_tab=false)
	{
		$var['active']=$active_tab;
		$var['objects']['text']['title']=sys::translate('main::text');
		$var['objects']['text']['url']=self::getArticleUrl($article_id);
		$var['objects']['text']['alt']=$article_title;

		$var['objects']['discuss']['title']=sys::translate('main::discuss');
		$var['objects']['discuss']['url']=self::getArticleDiscussUrl($article_id);
		$var['objects']['discuss']['alt']=$article_title.'::'.sys::translate('main::discuss');

		return sys::parseTpl('main::tabs',$var);
	}

	static function setArticleIndex($article_id,$values)
	{
		global $_db, $_cfg, $_langs;
		$_db->delete('main_contest_index',"`article_id`=".intval($article_id)."");
		foreach ($_langs as $lang_id=>$lang)
		{
			$_db->query("
				INSERT INTO `#__main_contest_index`
				(
					`article_id`,
					`lang_id`,
					`text`
				)
				VALUES
				(
					'".intval($article_id)."',
					'".$lang_id."',
					'".mysql_real_escape_string($values['title_'.$lang['code']])." ".strip_tags(mysql_real_escape_string($values['text_'.$lang['code']]))."'
				)

			");
		}

	}


}
?>