<?php

class main_captcha {
	
	public static $private_key = '6LfJtjAUAAAAAIcEuxkli2WEqaYCFhmiAqO0HIf7';
	public static $public_key = '6LfJtjAUAAAAAPXzqMYmryCQS-PpsgdZQ5aFvBZS';
	
	static function check(){

		if(empty($_POST['g-recaptcha-response']))
			return false;
		$recaptcha=$_POST['g-recaptcha-response'];
		$google_url="https://www.google.com/recaptcha/api/siteverify";
		$secret=self::$private_key;
		$ip=$_SERVER['REMOTE_ADDR'];
		$url=$google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
		$res=self::getCurlData($url);
		$res= json_decode($res, true);

		if($res['success']){
			return true;
		}
		return false;
	}
	
	static function getCurlData($url)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
		$curlData = curl_exec($curl);
		curl_close($curl);
		return $curlData;
	}
}

?>