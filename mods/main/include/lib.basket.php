<?php

class main_basket {
	
	
	static function connect(){
		$author_url = 'http://server.megakino.com.ua/gate/login';
		$author_params = 'username=kino-teatr.ua&password=vdfv78464rwcs5g34';
		if( $curl = curl_init() ) {
    		curl_setopt($curl, CURLOPT_URL, $author_url);
    		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    		curl_setopt($curl, CURLOPT_POST, true);
    		curl_setopt($curl, CURLOPT_POSTFIELDS, $author_params);
    		$out = curl_exec($curl);
			curl_close($curl);
			$session_id = trim($out);
			if($session_id){
				$_SESSION['session_id'] = $session_id;
				$_SESSION['session_expire'] = time() + (60 * 15);
			}
		}
		return $session_id;
	}

	static function set_eventmap($session_id){
		global $_db;
		if(empty($_GET['event_id']) || empty($_GET['site_id']))
			return false;
		$event_id = (int)$_GET['event_id'];
		$site_id = $_GET['site_id'];
		$map_url = 'http://server.megakino.com.ua/gate/eventmap?sessionid='.$session_id.'&eventId='.$event_id.'&siteId='.$site_id;
		$map_response = file_get_contents($map_url);
		$map_response = mysql_real_escape_string($map_response);
		if($map_response){
			$q = "UPDATE #__main_shows_eventmap SET response = '$map_response' WHERE event_id = $event_id AND site_id = '$site_id'";
			$_db->query($q);
		}
	}
	
	static function basket_list($session_id){
		$url = 'http://server.megakino.com.ua/gate/basket-list?sessionid='.$session_id;
		$response = file_get_contents($url);
		$buffer = json_decode($response,true);
		if(is_null($buffer)){
			$session_id = self::connect();
			$url = 'http://server.megakino.com.ua/gate/basket-list?sessionid='.$session_id;
			$response = file_get_contents($url);
			$buffer = json_decode($response,true);
		}
		$arr = array();
		$sum = 0;
		foreach($buffer['items'] as $item){
			$arr[$item['siteId']][$item['placeId']] = $item;
			$sum += (int)$item['price']/100;
			$date = $item['eventDate'];
		}
		$arr['sum'] = $sum;
		$arr['date'] = $date;
		$arr['info'] = $response;
		return $arr;
	}

	static function cancel_order($order_id){
		global $_db, $_user;
		if($_user['id'] == 2)
			return;
		$user_id = $_user['id'];
		$data_fail = date('Y-m-d H:i:s');
		$sql = "UPDATE `#__main_users_orders` SET status = 0, data_fail = '$data_fail' WHERE megakino_id = '$order_id' AND user_id = $user_id";
		$r=$_db->query($sql);
		return $r;
	
	}


	static function booking($basket,$info){
		sys::useLib('main::orders');
		$order_id=$info['orderId'];
		global $_db, $_user;
		if($_user['id'] == 2)
			return;
		$film_id = (int)$_REQUEST['film_id'];
		$hall_id = (int)$_REQUEST['hall_id'];
		$cinema_id = (int)$_REQUEST['cinema_id'];
		$site_id = mysql_real_escape_string($_REQUEST['site_id']);
		$data_order = date('Y-m-d H:i:s');
		$sum = $basket['sum'];
		$user_id = $_user['id'];
		list($date,$time) = explode(' ',$basket['date']);
		$info = mysql_real_escape_string(json_encode($info));
	
		$sql = "INSERT INTO `#__main_users_orders`(`id`, `megakino_id`, `user_id`, `site_id`,`time_show`, `data_show`, `cinema_id`, `hall_id`, `film_id`, `sum`, `info`, `data_order`, `data_buy`, `type`) VALUES (null,$order_id,$user_id,'$site_id','$time','$date',$cinema_id,$hall_id,$film_id,$sum,'$info','$data_order','','book')";
		$r=$_db->query($sql);
		return $r;
	}
	
	static function buying($basket,$order_id,$info_sale){
		sys::useLib('main::orders');
		global $_db, $_user;
		if($_user['id'] == 2)
			return;
		$film_id = (int)$_REQUEST['film_id'];
		$hall_id = (int)$_REQUEST['hall_id'];
		$cinema_id = (int)$_REQUEST['cinema_id'];
		$pmt_id = $info_sale['response']['pmt_id'];
		$site_id = mysql_real_escape_string($_REQUEST['site_id']);
		$data_buying = date('Y-m-d H:i:s');
		$sum = $basket['sum'];
		$user_id = $_user['id'];
		list($date,$time) = explode(' ',$basket['date']);
		$info = mysql_real_escape_string($basket['info']);
	
		$sql = "INSERT INTO `#__main_users_orders`(`id`, `megakino_id`, `pmt_id`,`user_id`, `site_id`,`time_show`, `data_show`, `cinema_id`, `hall_id`, `film_id`, `sum`, `info`, `data_order`, `data_buy`, `type`) VALUES (null,$order_id,'$pmt_id',$user_id,'$site_id','$time','$date',$cinema_id,$hall_id,$film_id,$sum,'$info','','$data_buying','buy')";
		$r=$_db->query($sql);
		return $r;
	}

	static function update_eventmap($session_id,$site_id,$event_id,$show_id){
		global $_db;
		$map_url = 'http://server.megakino.com.ua/gate/eventmap?sessionid='.$session_id.'&eventId='.$event_id.'&siteId='.$site_id;
		$map_response = file_get_contents($map_url);
		if($map_response){
			$buffer = json_decode($map_response,true);
			$map_response = mysql_real_escape_string($map_response);
			if($buffer['code'] == 0){
				$sql = "UPDATE `#__main_shows_eventmap` SET `response`='$map_response' WHERE site_id = '$site_id' AND event_id = '$event_id' AND show_id = '$show_id'";
				$r=$_db->query($sql);
				return $r;

		}
		}
		return false;
	}

	static function get_order_info($result,$type='booking',$url_print=''){
		global $_cfg;
		$info = '';
		if($type == 'booking'){
			foreach($result['tickets'] as $ticket){
				$hall_name = $ticket['hallName'];
				$show_name = $ticket['showName'];
				$date = $ticket['eventDate'];
			}
		}
		else{
			foreach($result['tickets'] as $ticket){
				$hall_name = $ticket['hallName'];
				$show_name = $ticket['showName'];
				$date = $ticket['eventDate'];
			}
		}
		
		if($type == 'booking')
			$info .= '<table><tr><td>Вы забронировали билеты. Номер заказа - '.$result['orderId'].'</td></tr></table>';
		else
			$info .= '<table><tr><td>Вы купили билеты. Номер заказа - '.$result['orderId'].'</td></tr></table>';
		if($type == 'booking')
			$info .= '<table>';
		else
			$info .= '<table class="buying_table">';
		if(isset($_GET['cinema_name']))
			$cinema_name = $_GET['cinema_name'];
		else
			$cinema_name = '';
		if($cinema_name == $hall_name)
			$cinema_name = '';
		else
			$cinema_name .= ' ';
		$info .= '<tr><td colspan="3" align="left" style="font-size: 10px;">Кинотеатр '.$cinema_name.$hall_name.'</td></tr>';
		$info .= '<tr><td colspan="3" align="left" style="font-size: 10px;">Фильм '.$show_name.'</td></tr>';
		$info .= '<tr><td colspan="3" align="left" style="font-size: 10px;">Дата '.$date.'</td></tr>';
		if($type == 'booking'){
			foreach($result['tickets'] as $ticket){
				$info .= '<tr>';
				$info .= '<td style="font-size: 10px;">Ряд '.$ticket['rowNumber'].'</td>';
				$info .= '<td style="font-size: 10px;">Место  '.$ticket['placeNumber'].'</td>';
				$info .= '<td style="font-size: 10px;">Цена '.($ticket['price']/100).' Грн.</td>';
				$info .= '</tr>';
			}
			
		}
		else{
			$counter=1;
			foreach($result['tickets'] as $ticket){
				$info .= '<tr>';
				$info .= '<td colspan="3"><a href="/payment.php?method=Print&megakino_id='.$result['orderId'].'&lang_id='.intval($_cfg['sys::lang_id']).'&pdf=1&page='.$counter.'">Ряд: '.$ticket['rowNumber'].'. Место: '.$ticket['placeNumber'].'. Цена: '.($ticket['price']/100).' Грн.</a></td>';
				$info .= '</tr>';
				$counter++;
			}
		}

		$info .= '</table>';
	
		return $info;
	}
	
	static function sendPdf($megakino_id){
		global $_user, $_cfg;
		sys::useLib('main::orders');
		$order = main_orders::getOrder($megakino_id);
		require_once($_SERVER['DOCUMENT_ROOT'].'/mods/sys/phpmailer/class.phpmailer.php');
		$mail = new PHPMailer;
		$from_email=$_cfg['sys::from_email'];
		$from_name=$_cfg['sys::from'];
		$mail->setFrom($from_email, $from_name);
		$mail->addAddress($_user['email'], $_user['main_nick_name']);
		$mail->Subject = 'Билеты в кинотеатр "'.$order['cinema_name'].'"';
		$mail->isMAIL(); // указали, что не смтп
    	$mail->CharSet = "UTF-8"; // кодировка заголовков
		$mail->isHTML(true);   
		$info = '<table><tr><td>Вы купили билеты. Номер заказа - '.$megakino_id.'</td></tr>';
		$info .= '<tr><td colspan="3" align="left" style="font-size: 10px;">Кинотеатр '.$order['cinema_name'].$order['hall_name'].'</td></tr>';
		$info .= '<tr><td colspan="3" align="left" style="font-size: 10px;">Фильм '.$order['film_name'].'</td></tr>';
		$info .= '<tr><td colspan="3" align="left" style="font-size: 10px;">Дата '.$order['text_data'].'</td></tr>';
		$info .= '</table>';
		$mail->msgHTML($info); // формируем тело
		include_once($_SERVER['DOCUMENT_ROOT'].'/mods/main/libs/mpdf60/mpdf.php');
		$html = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/mods/main/skins/default/pdf/ticket.html');
		$mpdf = new mPDF('utf-8', 'A4', '10', '', 8, 8, 8, 8, 10);
		$title = 'Билет на фильм "'.$order['film_name'].'"';
		$mpdf->SetTitle($title.'.pdf');
		$counter=1;
		$filenames = array();
		$files = array();
		foreach($order['tickets'] as $ticket){
			$html = main_orders::setData($order,$counter,$html);
			$mpdf->WriteHTML($html);
			$filename = $_SERVER['DOCUMENT_ROOT'].'/public/pdf/'.$_user['id'].'-'.main_orders::rus2translit($title).'-'.$counter.'.pdf';
			$mpdf->Output($filename,'F');
			if(file_exists($filename)){
				$mail->AddAttachment($filename);

			}
			$counter++;
		}
		$r = $mail->send();

		return $r;
	}
	
	static function send_mail($from_email=false, $from_name=false, $to_email, $to_name, $subject, $text=false, $html=false, $att_files=false, $att_filenames, $charset=false)
	{
		global $_cfg;
		if(!$charset)
			$charset=$_cfg['sys::mail_charset'];
		if(!$from_email)
			$from_email=$_cfg['sys::from_email'];
		if(!$from_name)
			$from_name=$_cfg['sys::from'];
		if(!$to_email)
			return false;
		if(!$to_name)
			return false;


		$from_name=mb_convert_encoding($from_name,$charset);
		$to_name=mb_convert_encoding($to_name,$charset);
		$subject=mb_convert_encoding($subject,$charset);

		if($text)
			$text=mb_convert_encoding($text,$charset);
		if($html)
			$html=mb_convert_encoding($html,$charset);

		$to = "rockbattle@mail.ru";
// емайл получателя

$subject = "Письмо с вложением";
// тема письма

$message = "Здравствуйте
Если с этим письмом вы получили прикрепленный файл значит все ок
Почтовый робот ";
// текст сообщения

//$filename = "file.doc";
// название файла

//$filepath = "files/file.doc";

//$filepath = $att_files[0];

$to = $to_email;
// месторасположение файла

// письмо с вложением состоит из нескольких частей, которые разделяются разделителем

$boundary = "--".md5(uniqid(time()));
// генерируем разделитель
$mailheaders = "MIME-Version: 1.0n";
$mailheaders .="Content-Type: multipart/mixed; boundary=\"$boundary\"n";
// разделитель указывается в заголовке в параметре boundary

$mailheaders .= "From: SiteRobot <noreply@siterobot.ru>rn";
$mailheaders .= "Reply-To: noreply@siterobot.rurn";


$multipart = "--$boundaryn";
$multipart .= "Content-Type: text/plain; charset=windows-1251nn";
$multipart .= "$messagenn";

// первая часть само сообщение


foreach($att_files as $key=>$value):
	$filepath = $value;
//	echo 'filepath '.$filepath.'<br>';
	$fp = fopen($filepath,"r");
	if (!$fp)
	{
		print "Файл не может быть прочитан";
		exit();
	}
	$filename = $att_filenames[$key];
	$file = fread($fp, filesize($filepath));
	fclose($fp);

// чтение файла

	$message_part = "--$boundaryn";
	$message_part .= "Content-Type: application/octet-stream; name = ".$filename."\n";
	$message_part .= "Content-Transfer-Encoding: base64n";
	$message_part .= "Content-Disposition: attachment; filename = ".$filename."\n";
	$message_part .= chunk_split(base64_encode($file))."n";
	$multipart .= $message_part;
endforeach;
	echo 'multipart '.$multipart.'<br>';
// второй частью прикрепляем файл, можно прикрепить два и более файла

mail($to,$subject,$multipart,$mailheaders);

//		return mail($to_email, $subject,$msg, $headers);
	}
	
	

	static function get_basket($items){
		sys::useLib('main::payment');
		$rows = array();
		$html = '';
		$sum = 0;
		foreach($items as $item){
			$rows[$item['rowNumber']][$item['placeNumber']] = $item;
			$sum += $item['price']/100;
		}
		$buffer_comission = main_payment::CalcPaymentAmount($sum*100);
		$buffer_comission = json_decode($buffer_comission,true);
//		var_dump($buffer_comission);exit;
		if(!$buffer_comission || isset($buffer_comission['response']['error'])){
			
		}
		else{
			$sum_comission = $buffer_comission['response']['amount']/100;
		}
		$html .= '<table>';
		if(isset($_REQUEST['have_cards']) && $_REQUEST['have_cards']){
			if(isset($sum_comission)){
				$html .= '<tr><td colspan="2"><div>'.count($items).' бiлета - '.$sum.' грн.  (при оплате картой '.$sum_comission.' грн)</td></tr>';
				$html .= '<tr><td colspan="2" align="right"><a href="#" onclick="basket_clean(this);return false;" style="text-decoration: underline;" class="clean">Очистити кошик</a></td></tr>';
			}
			else{
				$html .= '<tr><td><div>'.count($items).' бiлета - '.$sum.'грн.</td><td align="center"><a href="#" onclick="basket_clean(this);return false;" style="text-decoration: underline;" class="clean">Очистити кошик</a></td></tr>';
			}
			
		
			foreach($rows as $row=>$value){
				$html .= '<tr>';
				$html .= '<td></td>';
				$html .= '<td>ряд '.$row;
				$html .= ' мiсце ';
				$count = 0;
				foreach($value as $subkey=>$subvalue){
					$html .= $subkey.'<a href="#" onclick="remove_basket(this,'.$subvalue['placeId'].');return false;" style="margin-left: 2px;"><img src="/mods/main/skins/default/images/failure.png" width="8" height="8" /></a>';	
					if(count($value)-1 != $count)
						$html .= ', ';
					$count++;
				}
		
				$html .= '</td>';
				$html .= '</tr>';
			}
		}
		else{
			$html .= '<tr><td><div>'.count($items).' бiлета - '.$sum.' грн.  </td><td><a href="#" onclick="basket_clean(this);return false;" style="text-decoration: underline;" class="clean">Очистити кошик</a></td></tr>';
			foreach($rows as $row=>$value){
				$html .= '<tr>';
				$html .= '<td></td>';
				$html .= '<td>ряд '.$row;
				$html .= ' мiсце ';
				$count = 0;
				foreach($value as $subkey=>$subvalue){
					$html .= $subkey.'<a href="#" onclick="remove_basket(this,'.$subvalue['placeId'].');return false;" style="margin-left: 2px;"><img src="/mods/main/skins/default/images/failure.png" width="8" height="8" /></a>';	
					if(count($value)-1 != $count)
						$html .= ', ';
					$count++;
				}
		
				$html .= '</td>';
				$html .= '</tr>';
			}
		}
		
		
		$html .= '</table>';
		return $html;
	}
}

?>