<?php
class main_cards
{	
	static function deleteCard($card_id)
	{
		global $_db;
		$image=$_db->getValue('main_cards','image',intval($card_id));
		self::deleteCardImage($image);
		$_db->delete('main_cards',intval($card_id));
	}	
	
	static function deleteCardImage($filename)
	{
		global $_cfg;
		if(is_file($_cfg['main::cards_dir'].$filename))
			unlink($_cfg['main::cards_dir'].$filename);
		if(is_file($_cfg['main::cards_dir'].'x1_'.$filename))
			unlink($_cfg['main::cards_dir'].'x1_'.$filename);
		if(is_file($_cfg['main::cards_dir'].'x2_'.$filename))
			unlink($_cfg['main::cards_dir'].'x2_'.$filename);
		if(is_file($_cfg['main::cards_dir'].'x3_'.$filename))
			unlink($_cfg['main::cards_dir'].'x3_'.$filename);
	}
	
	static function resizeCardImage($filename)
	{
		global $_cfg;
		sys::resizeImage($_cfg['main::cards_dir'].$filename,$_cfg['main::cards_dir'].'x1_'.$filename,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		sys::resizeImage($_cfg['main::cards_dir'].$filename,$_cfg['main::cards_dir'].'x2_'.$filename,$_cfg['main::x2_width'],$_cfg['main::x2_height']);
		sys::resizeImage($_cfg['main::cards_dir'].$filename,$_cfg['main::cards_dir'].'x3_'.$filename,$_cfg['main::x3_width'],$_cfg['main::x3_height']);
		sys::resizeImage($_cfg['main::cards_dir'].$filename,$_cfg['main::cards_dir'].$filename,460,1000);
	}
	
	static function uploadCardImage($file,$card_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deleteCardImage($current_image);
			
		if($image=sys::uploadFile($file,$_cfg['main::cards_dir'],'card_'.$card_id, true))
		{
			$_db->setValue('main_cards','image',$image,$card_id);
			self::resizeCardImage($image);
		}
		return $image;
	}
	
	static function getCardUrl($time,$date,$film_id, $hall_id)
	{
		return sys::rewriteUrl('?mod=main&act=card&time='.$time.'&date='.$date.'&film_id='.$film_id.'&hall_id='.$hall_id);
	}
	
	
}
?>