<?php
class main_users
{
	static function deleteUser($user_id)
	{
		global $_db, $_cfg;
		$_db->query("DELETE FROM `#__main_users_genres` WHERE `user_id`=".intval($user_id)."");
		$_db->query("DELETE FROM `#__main_users_films_subscribe` WHERE `user_id`=".intval($user_id)."");
		$_db->query("DELETE FROM `#__main_users_discuss_subscribe` WHERE `user_id`=".intval($user_id)."");
		$_db->query("DELETE FROM `#__main_users_news_subscribe` WHERE `user_id`=".intval($user_id)."");
	}

	static function getUserTextsUrl($user_id, $act)
	{
		return sys::rewriteUrl('?mod=main&act='.$act.'&user_id='.$user_id);
	}
	
	static function getUserReviewsUrl($user_id)
	{
		return sys::rewriteUrl('?mod=main&act=user_reviews&user_id='.$user_id);
	}

	static function getUserMarksUrl($user_id)
	{
		return sys::rewriteUrl('?mod=main&act=user_marks&user_id='.$user_id);
	}

	static function getUserUrl($user_id)
	{
		return sys::rewriteUrl('?mod=main&act=user&user_id='.$user_id);
	}

	static function getProfileUrl()
	{
		return sys::rewriteUrl('?mod=main&act=user_profile');
	}

	static function getProfilePassUrl()
	{
		return sys::rewriteUrl('?mod=main&act=user_profile_pass');
	}

	static function getProfileNewsUrl()
	{
		return sys::rewriteUrl('?mod=main&act=user_profile_news');
	}

	static function getProfileFilmsUrl()
	{
		return sys::rewriteUrl('?mod=main&act=user_profile_films');
	}
	
	static function getProfileSubscriptionsUrl()
	{
		return sys::rewriteUrl('?mod=main&act=user_profile_subscriptions');
	}

	static function getProfileMyUrl()
	{
		return sys::rewriteUrl('?mod=main&act=user_profile_my');
	}

	static function getProfileDiscussUrl()
	{
		return sys::rewriteUrl('?mod=main&act=user_profile_discuss');
	}

	static function getProfileDeliveryUrl()
	{
		return sys::rewriteUrl('?mod=main&act=user_profile_delivery');
	}

	static function showProfileTabs($active_tab)
	{
        global $_cfg, $_user;
        
		$var['active']=$active_tab;
		$var['objects']['info']['title']=sys::translate('main::info');
		$var['objects']['info']['url']=self::getProfileUrl();
		$var['objects']['info']['alt']=sys::translate('main::info');

		$var['objects']['pass']['title']=sys::translate('main::change_pass');
		$var['objects']['pass']['url']=self::getProfilePassUrl();
		$var['objects']['pass']['alt']=sys::translate('main::change_pass');
        
		$var['objects']['delivery']['title']=sys::translate('main::delivery');
		$var['objects']['delivery']['url']=self::getProfileDeliveryUrl();
		$var['objects']['delivery']['alt']=sys::translate('main::delivery');
		$var['objects']['delivery']['help_icon']=_MODS_URL."main/skins/default/images/Information-icon.png";
		$var['objects']['delivery']['help_title']=sys::translate('main::profile_delivery_help_title');

//		$var['objects']['news']['title']=sys::translate('main::news');
//		$var['objects']['news']['url']=self::getProfileNewsUrl();
//		$var['objects']['news']['alt']=sys::translate('main::news');

//		$var['objects']['films']['title']=sys::translate('main::films');
//		$var['objects']['films']['url']=self::getProfileFilmsUrl();
//		$var['objects']['films']['alt']=sys::translate('main::films');
//		$var['objects']['films']['help_icon']=_MODS_URL."main/skins/default/images/Information-icon.png";
//		$var['objects']['films']['help_title']=sys::translate('main::profile_films_help_title');

//		$var['objects']['discuss']['title']=sys::translate('main::discuss');
//		$var['objects']['discuss']['url']=self::getProfileDiscussUrl();
//		$var['objects']['discuss']['alt']=sys::translate('main::discuss');
//		$var['objects']['discuss']['help_icon']=_MODS_URL."main/skins/default/images/Information-icon.png";
//		$var['objects']['discuss']['help_title']=sys::translate('main::profile_discuss_help_title');

		$var['objects']['subscriptions']['title']=sys::translate('main::subscriptions');
		$var['objects']['subscriptions']['url']=self::getProfileSubscriptionsUrl();
		$var['objects']['subscriptions']['alt']=sys::translate('main::subscriptions');

		$var['objects']['my']['title']=sys::translate('main::my_data');
		$var['objects']['my']['url']=self::getProfileMyUrl();
		$var['objects']['my']['alt']=sys::translate('main::my_data');
		$var['objects']['my']['help_icon']=_MODS_URL."main/skins/default/images/Information-icon.png";
		$var['objects']['my']['help_title']=sys::translate('main::profile_my_data_help_title');
		
		return sys::parseTpl('main::tabs',$var);
	}

	static function showUserTabs($user_id, $active_tab)
	{
		
		$isUserReviews = self::isUserReviews($user_id);
		$isUserInterview = self::isUserInterview($user_id);
		$isUserArticles = self::isUserArticles($user_id);
		$isUserNews = self::isUserNews($user_id);
		$isUserGossip = self::isUserGossip($user_id);
		
		if($isUserReviews || $isUserInterview || $isUserArticles || $isUserNews || $isUserGossip)
		{
			$var['active']=$active_tab;
			$var['objects']['info']['title']=sys::translate('main::info');
			$var['objects']['info']['url']=self::getUserUrl($user_id);
			$var['objects']['info']['alt']=sys::translate('main::info');
		}

		if($isUserNews)
		{
			$var['objects']['news']['title']=sys::translate('main::news');
			$var['objects']['news']['url']=self::getUserTextsUrl($user_id, "user_news");
			$var['objects']['news']['alt']=sys::translate('main::user_news');
		}
		
		if($isUserArticles)
		{
			$var['objects']['articles']['title']=sys::translate('main::articles');
			$var['objects']['articles']['url']=self::getUserTextsUrl($user_id, "user_articles");
			$var['objects']['articles']['alt']=sys::translate('main::user_articles');
		}
		
		if($isUserInterview)
		{
			$var['objects']['interview']['title']=sys::translate('main::interview');
			$var['objects']['interview']['url']=self::getUserTextsUrl($user_id, "user_interview");
			$var['objects']['interview']['alt']=sys::translate('main::user_interview');
		}
		
		if($isUserGossip)
		{
			$var['objects']['gossip']['title']=sys::translate('main::gossip');
			$var['objects']['gossip']['url']=self::getUserTextsUrl($user_id, "user_gossip");
			$var['objects']['gossip']['alt']=sys::translate('main::user_gossip');
		}
		
		if($isUserReviews)
		{
			$var['objects']['reviews']['title']=sys::translate('main::reviews');
			$var['objects']['reviews']['url']=self::getUserTextsUrl($user_id, "user_reviews");
			$var['objects']['reviews']['alt']=sys::translate('main::user_reviews');
		}
		
		return sys::parseTpl('main::tabs',$var);
	}

	static function isUserReviews($user_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_reviews`
			WHERE `user_id`=".intval($user_id)."
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isUserGossip($user_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_gossip_articles`
			WHERE `user_id`=".intval($user_id)."
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isUserNews($user_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_news_articles`
			WHERE `user_id`=".intval($user_id)."
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isUserInterview($user_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_interview_articles`
			WHERE `user_id`=".intval($user_id)."
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isUserArticles($user_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_articles_articles`
			WHERE `user_id`=".intval($user_id)."
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function uploadUseraAvatar($file,$user_id,$current_avatar=false)
	{
		global $_db, $_cfg;
		if($current_avatar)
			self::deleteUserAvatar($current_avatar);

		if($avatar=sys::uploadFile($file,$_cfg['main::users_dir'],'user_'.$user_id, true))
		{
			$_db->query("
				UPDATE `#__sys_users_data`
				SET `main_avatar`='".mysql_real_escape_string($avatar)."'
				WHERE `user_id`=".intval($user_id)."
			");
			// sys::resizeImage($_cfg['main::users_dir'].$avatar,false,65,65); // старая версия
		
			$from_path = $_cfg['main::users_dir'].$avatar;			// sys::resizeImage($from_path, $to_path=false, $to_width=100, $to_height=100, $cutting=false)			
			sys::resizeImage($from_path, $from_path, 								$_cfg['main::avatar_big_width'], 	$_cfg['main::avatar_big_height'], false);
			sys::resizeImage($from_path, $_cfg['main::users_dir'].'small_'.$avatar, $_cfg['main::avatar_small_width'], 	$_cfg['main::avatar_small_height'], false);
		}
		return $avatar;
	}

	static function deleteUserAvatar($filename)
	{
		global $_cfg;
		if(is_file($_cfg['main::users_dir'].$filename))
			unlink($_cfg['main::users_dir'].$filename);
		if(is_file($_cfg['main::users_dir']."small_".$filename))
			unlink($_cfg['main::users_dir']."small_".$filename);
	}
}
?>