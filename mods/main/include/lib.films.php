<?php
class main_films 
{
	static function SetExpectRegistered($film_id=0, $user_id=0, $new_mark=0)
	{
		global $_db, $_cfg;

		if(intval($new_mark)<=0) $new_mark=-1;
		else $new_mark=1;

		$old_mark = 0;
		$q = "
			SELECT `mark`
			FROM `#__main_films_expect`
			WHERE `user_id`=".intval($user_id)."
			AND `film_id`=".intval($film_id)."
		";		
		$r=$_db->query($q);

		if($obj=$_db->fetchAssoc($r))
		{
			$old_mark = $obj["mark"];
			if($old_mark != $new_mark)
			{
			    $q = "
					UPDATE `#__main_films_expect`
					SET `mark`= '$new_mark'
					WHERE `user_id`=".intval($user_id)." AND `film_id`=".intval($film_id);
			    $_db->query($q);			
			}
		}
		else
		{
		    $q = "
				INSERT INTO `#__main_films_expect`
				(`user_id`,`film_id`,`mark`)
				VALUES
				(".intval($user_id).",".intval($film_id).", '$new_mark')";
		    $_db->query($q);		    
		}
		
	
		if($old_mark != $new_mark)
		{
			// сброс предыдущего голосования
			if($old_mark != 0) // не впервые (впервые не обрабатываем это)
			{
				$delta_yes = ($old_mark>0)?-1:0;
				$delta_no = ($old_mark<0)?-1:0;
				$q = "
					UPDATE `#__main_films`
					SET 
						expect_yes=expect_yes+($delta_yes), 
						expect_no=expect_no+($delta_no)
					WHERE `id`=".intval($film_id)."
				";		
			$_db->query($q);
			}

			// запись нового голосования
			$delta_yes = ($new_mark>0)?1:0;
			$delta_no = ($new_mark<0)?1:0;
			$q = "
				UPDATE `#__main_films`
				SET 
					expect_yes=expect_yes+$delta_yes, 
					expect_no=expect_no+$delta_no
				WHERE `id`=".intval($film_id)."
			";		
			$_db->query($q);
		}		
		
		$q = "
			SELECT `expect_yes`, `expect_no`
			FROM `#__main_films`
			WHERE `id`=".intval($film_id)."
		";		
					
		$r=$_db->query($q);
		return $_db->fetchAssoc($r);
	}
	
	static function SetExpectUnregistered($film_id=0, $new_mark=0)
	{
		global $_db, $_cfg;
		
		if(!$film_id) return 0;

		if(intval($new_mark)<=0) $new_mark=-1;
		else $new_mark=1;
		
		$ip = $_SERVER['REMOTE_ADDR'];
		$sid = session_id();

		// Удалить устаревшие
		$q="
			DELETE FROM `#__main_films_expect_unregistered` 
			WHERE DATE_ADD(dt, INTERVAL 10 HOUR)<now()
			";
		$_db->query($q);

		// Если для незарегистрированного пользователя для этого фильма нет записи, то создать ее с нулем.
		$q = "
			SELECT *
			FROM `#__main_films_expect`
			WHERE `user_id`=2
			AND `film_id`=".intval($film_id)."
		";
		$r=$_db->query($q);
		if(!$obj=$_db->fetchAssoc($r))
		{
		    $q = "
				INSERT INTO `#__main_films_expect`
				(
					`user_id`,`film_id`,`mark`
				)
				VALUES
				(
					2,".intval($film_id).",0)";
		    $_db->query($q);
		}
		
		// выбрать голосовальную запись для текущего незарегистрированного юзера	
		$old_mark = 0;
		$q = "
			SELECT 
				`id`, 
				`mark`
			FROM `#__main_films_expect_unregistered`
			WHERE 
			`film_id`='$film_id' AND `ip` = '$ip' AND `sid` = '$sid' 
		";
		
		$id = 0;
		$r=$_db->query($q);		
		if($obj=$_db->fetchAssoc($r)) // видим, что он проголосовал
		{			
			$id = $obj['id'];
			$old_mark=intval($obj['mark']);	 // прошлое мнение
		}

		if($id>0)						// меняем старое мнение
		{	
		 $q = "
		    UPDATE `#__main_films_expect_unregistered`
		    SET 
		     `mark`=$new_mark, `dt`=NOW() 
		    WHERE `id`='$id'; 
		 "; 
		}
		 else 							// сохраняем мнение впервые
		{
		 $q = "
		    INSERT INTO `#__main_films_expect_unregistered`
		        (`film_id`, `mark` , `ip` , `sid` , `dt` )
		    VALUES    
		    	('$film_id', '$new_mark', '$ip', '$sid', NOW())
		 "; 
		}
		 $_db->query($q);
		
		if($old_mark != $new_mark)
		{
			// сброс предыдущего голосования
			if($old_mark != 0) // не впервые (впервые не обрабатываем это)
			{
				$delta_yes = ($old_mark>0)?-1:0;
				$delta_no = ($old_mark<0)?-1:0;
				$q = "
					UPDATE `#__main_films`
					SET 
						expect_yes=expect_yes+($delta_yes), 
						expect_no=expect_no+($delta_no)
					WHERE `id`=".intval($film_id)."
				";		

				$_db->query($q);
			}

			// запись нового голосования
			$delta_yes = ($new_mark>0)?1:0;
			$delta_no = ($new_mark<0)?1:0;
			$q = "
				UPDATE `#__main_films`
				SET 
					expect_yes=expect_yes+($delta_yes), 
					expect_no=expect_no+($delta_no)
				WHERE `id`=".intval($film_id)."
			";		
			$_db->query($q);
		}		
		
		$q = "
			SELECT `expect_yes`, `expect_no`
			FROM `#__main_films`
			WHERE `id`=".intval($film_id)."
		";		
		$r=$_db->query($q);
		return $_db->fetchAssoc($r);
	}
	
	
	// Голосование зарегистрированного пользователя за фильм
	// Только проверяется: есть ли уже запись о голосовании по этому фильму и при необходимости
	// вставляется нулевая запись для фильма
	// Возвращает признак вставки, что равно по смыслу добавлению 1 голоса, и разницу между новым 
	// и старым значением рейтинга
	static function SetVoteRegistered($film_id=0, $user_id=0, $new_mark=0, $is_showing=false)
	{
		global $_db, $_cfg;

		if(intval($new_mark)<0) $new_mark=0;
		if(intval($new_mark)>10) $new_mark=10;

		$field = $is_showing?'mark':'pre_mark';
		$q = "
			SELECT `$field` as `mark`
			FROM `#__main_films_rating`
			WHERE `user_id`=".intval($user_id)."
			AND `film_id`=".intval($film_id)."
		";
		$r=$_db->query($q);
		if($obj=$_db->fetchAssoc($r))
		{
			$result['delta_mark'] = $new_mark - $obj['mark'];	// значение, на которое надо изменить рейтинг.
			$result['inserted'] = !$obj['mark'];		// вставлена новая запись или произведено только изменение значения голоса. 
			//Если переключился pre на не pre и $obj['mark'], то считаем, что добавлена новая запись, чтобы не произошло искажения подсчета.
		}
		else 
		{
		    $q = "
				INSERT INTO `#__main_films_rating`
				(`user_id`,`film_id`,`mark`,`pre_mark`)
				VALUES
				(".intval($user_id).",".intval($film_id).",0,0)";
		    $_db->query($q);
		    
		    if($new_mark<0) $new_mark=0;
		    if($new_mark>10) $new_mark=10;
		    
			$result['delta_mark'] = $new_mark;	// значение, на которое надо изменить рейтинг.
			$result['inserted'] = true;		// вставлена новая запись или произведено только изменение значения голоса.
		}
		
		return $result;
	}
	
	// Голосование незарегистрированного пользователя за фильм
	// Только проверяется: есть ли уже запись о голосовании по этому фильму и при необходимости
	// вставляется запись-признак для этого фильма
	// Возвращает признак вставки, что равно по смыслу добавлению 1 голоса, и разницу между новым 
	// и старым значением рейтинга
	// Записи старее 10 часов удаляются
	static function SetVoteUnregistered($film_id=0, $new_mark=0, $is_showing=false)
	{
		global $_db, $_cfg;
		
		if(!$film_id) return 0;
		if(intval($new_mark)<0) $new_mark=0;
		if(intval($new_mark)>10) $new_mark=10;
		
		$ip = $_SERVER['REMOTE_ADDR'];
		$sid = session_id();
		$mark = $delta_mark = $id = 0;
		$field = $is_showing?'mark':'pre_mark';

		// Удалить устаревшие
		$q="
			DELETE FROM `#__main_films_rating_unregistered` 
			WHERE DATE_ADD(dt, INTERVAL 10 HOUR)<now()
			";
		$_db->query($q);

		// Если для незарегистрированного пользователя для этого фильма нет записи, то создать ее с нулями.
		$q = "
			SELECT *
			FROM `#__main_films_rating`
			WHERE `user_id`=2
			AND `film_id`=".intval($film_id)."
		";
		$r=$_db->query($q);
		if(!$obj=$_db->fetchAssoc($r))
		{
		    $q = "
				INSERT INTO `#__main_films_rating`
				(
					`user_id`,`film_id`,`mark`,`pre_mark`
				)
				VALUES
				(
					2,".intval($film_id).",0,0)";
		    $_db->query($q);
		}
			
		// Обеспечить уникальность данных для текущего незарегистрированного пользователя
		$q = "
			SELECT 
				`id`, 
				`$field` as `mark`
			FROM `#__main_films_rating_unregistered`
			WHERE 
			`film_id`='$film_id' AND `ip` = '$ip' AND `sid` = '$sid' 
		";
		
		$result['inserted'] = false;
		$r=$_db->query($q);		
		if($obj=$_db->fetchAssoc($r)) 
		{
			$id = $obj['id'];
			$mark=intval($obj['mark']);
			$result['inserted'] = !$obj['mark'];		// вставлена новая запись или произведено только изменение значения голоса. 
			//Если переключился pre на не pre и $obj['mark'], то считаем, что добавлена новая запись, чтобы не произошло искажения подсчета.
		}

		$delta_mark = $new_mark - $mark;
				
		if($id>0)
		{
		 $q = "
		    UPDATE `#__main_films_rating_unregistered`
		    SET 
		     `$field`=$new_mark, `dt`=NOW() 
		    WHERE `id`='$id'; 
		 "; 
		}
		 else 
		{
		 $q = "
		    INSERT INTO `#__main_films_rating_unregistered`
		        (`film_id`, `$field` , `ip` , `sid` , `dt` )
		    VALUES    
		    	('$film_id', '$new_mark', '$ip', '$sid', NOW())
		 "; 
		 $result['inserted'] = true;			// вставлена новая запись или произведено только изменение значения голоса.
		}
		 $_db->query($q);
		
		$result['delta_mark'] = $delta_mark;	// значение, на которое надо изменить рейтинг.		
		
		return $result; 
	}
	
	// Фиксирование результатов голосования
	// Входной параметр - массив с данными	
	// delta_mark - на сколько изменить существующее значение
	// inserted - добавлять ли голос
	// is_showing - уже идет ли в к/т
	// new_mark - новое значение, которое в посте
	// user_id - id пользователя
	// film_id - id фильма
	static function fixMarkFilm($marker)
	{
		global $_db, $_cfg;
		
		$result['yourMark'] = $marker['new_mark'];
		$prefix = $marker['is_showing']?'':'pre_';
		$q="
				UPDATE `#__main_films_rating`
				SET `".$prefix."mark`='".$marker['new_mark']."'
				WHERE `user_id`='".$marker['user_id']."'
				AND `film_id`='".$marker['film_id']."';
			";
		$_db->query($q);

		$q="
			UPDATE `#__main_films`
			SET
				`".$prefix."votes`=`".$prefix."votes`+'".($marker['inserted']?1:0)."',
				`".$prefix."sum`=`".$prefix."sum`+'".$marker['delta_mark']."',
				`".$prefix."rating`= `".$prefix."sum`/`".$prefix."votes`
			WHERE `id`='".$marker['film_id']."';";
		$_db->query($q);
		
		$q="SELECT `pre_rating`,`rating`, `pre_votes`, `votes`, `sum`, `pre_sum` FROM `#__main_films` WHERE `id`='".$marker['film_id']."';";
		$r=$_db->query($q);		
		$film=$_db->fetchAssoc($r);
   	    
		if($film['votes']>5) // только при наличии 5 голосов после премьеры показывать реальный рейтинг
		{
			$result['votes']=$film['pre_votes']+$film['votes'];
			$result['rating']=self::formatFilmRating($film['sum']/$film['votes']);
		}
		else // до 5 голосов показывать предварительный усредненный рейтинг
		{
			$result['votes']=$film['pre_votes']+$film['votes'];
			$result['rating']=self::formatFilmRating(($film['pre_sum']+$film['sum'])/$result['votes']);
		}
		return $result;
	}	
		
	// Новая версия отображения рейтинга фильмов
	static function showFilmRatingForm($film_id)
	{
		global $_user, $_db, $_cfg;

		$r=$_db->query("
			SELECT
				flm.id,
				flm.rating,
				flm.pre_rating,
				flm.votes,
				flm.pre_votes,
				flm.sum,
				flm.pre_sum,
				flm.pro_rating,
				flm.pro_votes,
				flm.pro_sum,
				flm.expect_yes,
				flm.expect_no,
				flm.ukraine_premiere,
				flm.world_premiere
			
			FROM
				`#__main_films` AS `flm`
			
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id='".intval($film_id)."'
			AND flm_lng.lang_id='".intval(intval($_cfg['sys::lang_id']))."'
			
			WHERE flm.id='".intval($film_id)."'
		");

		$film=$_db->fetchAssoc($r);
		$p_date=strtotime($film['ukraine_premiere']?$film['ukraine_premiere']:$film['world_premiere']);
		//$is_showing=$_db->getValue('main_shows','id',"film_id=".intval($film_id)." AND begin<='".date('Y-m-d', time())."'");
		$is_showing = ($p_date<time());

		if($is_showing)
		{
			if ($film['rating']>0)
			{
				if($film['votes']>5) // только при наличии 5 голосов после премьеры показывать реальный рейтинг
				{
					$var['votes']=$film['pre_votes']+$film['votes'];
					$var['rating']=$film['sum']/$film['votes'];
				}
				else // до 5 голосов показывать предварительный усредненный рейтинг
				{
					$var['votes']=$film['pre_votes']+$film['votes'];
					$var['rating']=($film['pre_sum']+$film['sum'])/$var['votes'];
				}
			} 
			else 
			{
				$_db->query("
					UPDATE
						`#__main_films`
					SET 
						rating = '".$film['pro_rating']."',
						sum = '".$film['pro_sum']."',
						votes = '".$film['pro_votes']."'
					WHERE 
						id='".intval($film_id)."'
				");
				$var['votes']=($film['pre_votes']+$film['pro_votes']);
				$var['rating']=($film['pre_sum']+$film['pro_sum'])/$var['votes'];
			}
		}
		else 
		{
				$var['rating']=$film['pre_rating'];
				$var['votes']=$film['pre_votes'];
		}
// звезды
		$var['rest']=$var['rating']-floor($var['rating']);
		$var['object_id']=$film['id'];
		$var['rating']=self::formatFilmRating($var['rating']);
		$var['ajx_type']='ajx_vote_film';
		$var['form_title']='film_rating'.($is_showing?"":'_waiting');

// данные для рейтинга ожидания
		$var["expect"]["film_id"] = $film_id;
		$var["expect"]["total"] = 0+$film["expect_yes"] + $film["expect_no"];
		$var["expect"]["yes"] = round(0+$film["expect_yes"]*100/($film["expect_yes"]+$film["expect_no"]));
		$var["expect"]["no"] = 100-$var["expect"]["yes"];
		
		if($_user['id']==2)
		{
			$ip = $_SERVER['REMOTE_ADDR'];
			$sid = session_id();
			$var['yourMark'] = $_db->getValue('main_films_rating_unregistered',$is_showing?"mark":'pre_mark',"film_id=".intval($film_id)." AND ip='$ip' AND sid='$sid'");
			$var["expect"]["yourMark"] = $_db->getValue('main_films_expect_unregistered',"mark","film_id=".intval($film_id)." AND ip='$ip' AND sid='$sid'");
		}
		else 
		{
			$var['yourMark'] = $_db->getValue('main_films_rating',$is_showing?"mark":'pre_mark',"film_id=".intval($film_id)." AND user_id='".$_user['id']."'");
			$var["expect"]['yourMark'] = $_db->getValue('main_films_expect',"mark","film_id=".intval($film_id)." AND user_id='".$_user['id']."'");
		}
		if(!$var['yourMark']) $var['yourMark']="—";
		
		if(!$var["expect"]['yourMark']) $var["expect"]['yourMark']="—";
		$var["expect"]["voted"] = ($var["expect"]['yourMark']!="—");
		
		$premier = getdate($p_date); // премьера
		$monday = mktime(0, 0, 0, $premier["mon"], $premier["mday"]+9-$premier["wday"], $premier["year"]); // следующий вторник
		$var["expect"]["is_showing"] = (time()<$monday) && !$var["expect"]["voted"];
		$var["expect"]["premiered"] = (time()>$monday);
		
		if(!$var["expect"]["is_showing"] && ($var["expect"]['yourMark']=="—")) $var["expect"]['yourMark'] = sys::translate('main::youNotExpected');
		
		$premieredZeroExpected = ($var["expect"]["premiered"] && ($var["expect"]["total"]==0));
		
		//if(sys::isDebugIP()) 
		return sys::parseTpl('main::raiting_stars',$var).($premieredZeroExpected?"":sys::parseTpl('main::mark_form_pre',$var["expect"]));
		//return sys::parseTpl('main::raiting_stars',$var);
	}
	
	
	static function showLastBot()
	{

		global $_db, $_cfg;

		$current_day = date("N");
		$days_to_friday = 7 - $current_day;
		$days_from_monday = $current_day - 1;
		$monday = date("Y-m-d", strtotime("- {$days_from_monday} Days"));
		$friday = date("Y-m-d", strtotime("+ {$days_to_friday} Days"));

		$q="
			SELECT
				flm.id,
				flm.year,
				flm.title_orig,
				lng.title,
				flm.ukraine_premiere
			FROM `#__main_films` AS `flm`
			LEFT JOIN `#__main_films_lng` AS `lng`
			ON lng.record_id=flm.id
			AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE
				flm.ukraine_premiere>='".$monday."'
			ORDER BY
				flm.ukraine_premiere ASC
			LIMIT 0,5
		";

		$r=$_db->query($q);
		$var['objects']=array();

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=self::getFilmUrl($obj['id']);
			$var['objects'][]=$obj;
		}

		$result=sys::parseTpl('main::bot_last',$var);
		return $result;


	}

	static function showLastBotEr()
	{

		global $_db, $_cfg;

		$current_day = date("N");
		$days_to_friday = 7 - $current_day;
		$days_from_monday = $current_day - 1;
		$monday = date("Y-m-d", strtotime("- {$days_from_monday} Days"));
		$friday = date("Y-m-d", strtotime("+ {$days_to_friday} Days"));

		$q="
			SELECT
				flm.id,
				flm.year,
				flm.title_orig,
				lng.title,
				flm.ukraine_premiere
			FROM `#__main_films` AS `flm`
			LEFT JOIN `#__main_films_lng` AS `lng`
			ON lng.record_id=flm.id
			AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE
				flm.ukraine_premiere>='".$monday."'
			ORDER BY
				flm.ukraine_premiere ASC
			LIMIT 0,10
		";

		$r=$_db->query($q);
		$var['objects']=array();

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=self::getFilmUrl($obj['id']);
			$var['objects'][]=$obj;
		}

		$result=sys::parseTpl('main::bot_last_er',$var);
		return $result;


	}

	static function deleteFilm($film_id)
	{
		sys::useLib('main::shows');
		sys::useLib('main::discuss');
		global $_db, $_cfg;


		//Удалить фотки
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_photos`
			WHERE `film_id`=".intval($film_id)."
		");
		while(list($photo_id)=$_db->fetchArray($r))
		{
			self::deletePhoto($photo_id);
		}

		//Удалить постеры
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_posters`
			WHERE `film_id`=".intval($film_id)."
		");
		while(list($poster_id)=$_db->fetchArray($r))
		{
			self::deletePoster($poster_id);
		}

		//Удалить трейлеры
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_trailers`
			WHERE `film_id`=".intval($film_id)."
		");
		while(list($trailer_id)=$_db->fetchArray($r))
		{
			self::deleteTrailer($trailer_id);
		}

		//Удалить персон
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_persons`
			WHERE `film_id`=".intval($film_id)."
		");
		while(list($link_id)=$_db->fetchArray($r))
		{
			self::deletePersonLink($link_id);
		}

		//Удалить обои
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_wallpapers`
			WHERE `film_id`=".intval($film_id)."
		");
		while(list($walp_id)=$_db->fetchArray($r))
		{
			self::deleteWallPaper($walp_id);
		}

		//Удалить ссылки
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_links`
			WHERE `film_id`=".intval($film_id)."
		");
		while(list($link_id)=$_db->fetchArray($r))
		{
			self::deleteLink($link_id);
		}

		//Удалить саундтрэки
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_soundtracks`
			WHERE `film_id`=".intval($film_id)."
		");
		while(list($snd_id)=$_db->fetchArray($r))
		{
			self::deleteSoundTrack($snd_id);
		}

		//Удалить награды
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_awards`
			WHERE `film_id`=".intval($film_id)."
		");
		while(list($award_id)=$_db->fetchArray($r))
		{
			self::deleteAwardLink($award_id);
		}

		//Удалить показы
		$r=$_db->query("
			SELECT `id` FROM `#__main_shows`
			WHERE `film_id`=".intval($film_id)."
		");
		while(list($show_id)=$_db->fetchArray($r))
		{
			main_shows::deleteShow($show_id);
		}

		//Удалить ссылки в новостях
		$_db->delete('main_news_articles_films',"`film_id`=".intval($film_id)."");

		$image=$_db->getValue('main_films','album_poster',intval($film_id));

		if(is_file($_cfg['main::films_dir'].$image))
			unlink($_cfg['main::films_dir'].$image);

		//Удалить обсуждения
		$_db->query("
			SELECT `id` FROM `#__main_discuss_messages`
			WHERE `object_id`=".intval($film_id)."
			AND `object_type`='film'
		");


		main_discuss::deleteDiscuss($film_id,'film');


		//Удалить жанры
		$_db->delete('main_films_genres',"film_id=".intval($film_id)."");
		//Удалить страны
		$_db->delete('main_films_countries',"film_id=".intval($film_id)."");
		//Удалить дистрибюторов
		$_db->delete('main_films_distributors',"film_id=".intval($film_id)."");
		//Удалить студии
		$_db->delete('main_films_studios',"film_id=".intval($film_id)."");
		//Удалить рейтинг
		$_db->delete('main_films_rating',"film_id=".intval($film_id)."");
		
		//Удалить imdb (ссылки по foregn key)
		$_db->delete('main_films_imdb_to_kt',"kt_id=".intval($film_id)."");
		//Удалить фаворитов (ссылки по foregn key)
		$_db->query('DELETE FROM filmgoer_users_favorite_films where film_id='.intval($film_id));

		//Удалить фильм
		$_db->delete('main_films',intval($film_id),true);
	}

	static function showFrontFilms()
	{

		sys::useLib('main::films');
		sys::useLib('main::shows');
		sys::useLib('main::cinemas');
		sys::useLib('main::countries');
		sys::useLib('sys::form');
		global $_db, $_cfg, $_err, $_user, $_cookie;
		sys::filterGet('date','text',date('d.m.Y'));
		sys::filterGet('order','text','films');
		sys::filterGet('city_id','int',$_cfg['main::city_id']);

		$var['objects']=main_shows::getBillBoardMain($_GET['city_id'],$_GET['date'], $_GET['order']);

		return sys::parseTpl('main::front_films',$var);
	}

	static function showFrontFilmsList()
	{

		sys::useLib('main::films');
		sys::useLib('main::shows');
		sys::useLib('main::cinemas');
		sys::useLib('main::countries');
		sys::useLib('sys::form');
		global $_db, $_cfg, $_err, $_user, $_cookie;
		sys::filterGet('date','text',date('d.m.Y'));
		sys::filterGet('order','text','films');
		sys::filterGet('city_id','int',$_cfg['main::city_id']);

		$var['objects']=main_shows::getBillBoardMainList($_GET['city_id'],$_GET['date'], $_GET['order']);

		return sys::parseTpl('main::front_films_list',$var);
	}

	static function showFrontNewTrailersList()
	{
		global $_cfg, $_db;

		$cache_name='main_front_new_trailers_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::new_trailers_cache_period']*60))
			return $cache;
		$var['objects']=array();
		$q="
			SELECT data.id, data.film_id, flm_lng.title 
			FROM 
			( SELECT id, film_id, tab_num, date  FROM `#__main_films_trailers` ORDER BY date DESC LIMIT 0,100 ) AS `data`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=data.film_id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE data.tab_num IN (1,2)
			GROUP BY data.film_id
			ORDER BY data.date DESC
			LIMIT 0,".$_cfg['main::v3_max_lines_in_block_trailers'];
		
		//if(sys::isDebugIP()) $_db->printR($q);
		
		$r=$_db->query($q);
 
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['alt']=sys::translate('main::film').' &laquo;'.$obj['title'].'&raquo;, '.sys::translate('main::trailers');
			$obj['url']=self::getFilmTrailersUrl($obj['film_id'])."?trailer_id=".$obj["id"];
			$var['objects'][]=$obj;
		}
		$var['caption'] = "main::new_trailers";
		$result=sys::parseTpl('main::front_trailers',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function showFrontNewPostersList()
	{
		global $_cfg, $_db;

		$cache_name='main_front_new_posters_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::new_posters_cache_period']*60))
			return $cache;
		$var['objects']=array();
		$q="
			SELECT data.id, data.film_id, flm_lng.title 
			FROM `#__main_films_posters` AS `data`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=data.film_id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			GROUP BY data.film_id
			ORDER BY data.date DESC
			LIMIT 0,".$_cfg['main::v3_max_lines_in_block_posters'];
		$r=$_db->query($q);

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['alt']=sys::translate('main::film').' &laquo;'.$obj['title'].'&raquo;, '.sys::translate('main::posters');
			$obj['url']=self::getFilmPostersUrl($obj['film_id']);
			$var['objects'][]=$obj;
		}
		$var['caption'] = "main::new_posters";
		$result=sys::parseTpl('main::front_trailers',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function showFrontNewFilmsList()
	{
		global $_cfg, $_db;

		$cache_name='main_front_new_films_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::new_trailers_cache_period']*60))
			return $cache;
		$var['objects']=array();
		$q="
			SELECT data.id, data.title_orig, flm_lng.title 
			FROM `#__main_films` AS `data`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=data.id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			GROUP BY data.id
			ORDER BY data.date DESC
			LIMIT 0,".$_cfg['main::v3_max_lines_in_block_films'];
		$r=$_db->query($q);
 //if(sys::isDebigIP()) $_db->printR($q);
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['alt']=sys::translate('main::film').' &laquo;'.$obj['title'].'&raquo;';
			$obj['url']=self::getFilmUrl($obj['id']);
			$var['objects'][]=$obj;
		}
		$var['caption'] = "main::new_films";
		$result=sys::parseTpl('main::front_trailers',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}
	
	static function showFrontPopularFilmsList()
	{
		global $_cfg, $_db;

		$cache_name='main_front_popular_films_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::new_trailers_cache_period']*60))
			return $cache;
		$var['objects']=array();
		$q="
			SELECT
				flm.id,
				lng.title,
				flm.title_orig,
				flm.today_shows,
				flm.total_shows
			FROM
				`#__main_films` AS `flm`
			LEFT JOIN `#__main_films_lng` AS `lng`
			ON lng.record_id=flm.id
			AND lng.lang_id='".intval($_cfg['sys::lang_id'])."'
			ORDER BY flm.today_shows DESC, flm.total_shows DESC
			LIMIT 0,".$_cfg['main::v3_max_lines_in_block_films'];
		
		$r=$_db->query($q);
//$_db->printR($q);
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['alt']=sys::translate('main::film').' &laquo;'.$obj['title'].'&raquo;';
			$obj['url']=self::getFilmUrl($obj['id']);
			$var['objects'][]=$obj;
		}
		$var['caption'] = "main::popular_films";
		$result=sys::parseTpl('main::front_popular_films',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}
	
	static function showFrontNewOzhFilmsList()
	{
		global $_cfg, $_db;

		$cache_name='main_front_new_ozh_films_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::new_trailers_cache_period']*60))
			return $cache;
		$var['objects']=array();
		$q="
			SELECT data.id, data.title_orig, flm_lng.title 
			FROM `#__main_films` AS `data`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=data.id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE data.world_premiere>NOW() OR data.ukraine_premiere>NOW()
			GROUP BY data.id
			ORDER BY data.expect_yes DESC
			LIMIT 0,".$_cfg['main::v3_max_lines_in_block_ozh_films'];
		$r=$_db->query($q);
 
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['alt']=sys::translate('main::film').' &laquo;'.$obj['title'].'&raquo;';
			$obj['url']=self::getFilmUrl($obj['id']);
			$var['objects'][]=$obj;
		}
		$var['caption'] = "main::new_ozh_films";
		$result=sys::parseTpl('main::front_trailers',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}
	
	static function showFrontGenres()
	{

		sys::useLib('main::films');
		sys::useLib('main::shows');
		sys::useLib('main::cinemas');
		sys::useLib('main::countries');
		sys::useLib('sys::form');
		global $_db, $_cfg, $_err, $_user, $_cookie;
		sys::filterGet('date','text',date('d.m.Y'));
		sys::filterGet('order','text','films');
		sys::filterGet('city_id','int',$_cfg['main::city_id']);

		$var['objects']=main_shows::getGenresMain($_GET['city_id'],$_GET['date'], $_GET['order']);

		return sys::parseTpl('main::front_genres',$var);
	}


	static function showFrontTime()
	{

		sys::useLib('main::films');
		sys::useLib('main::shows');
		sys::useLib('main::cinemas');
		sys::useLib('main::countries');
		sys::useLib('sys::form');
		global $_db, $_cfg, $_err, $_user, $_cookie;

		$var['curentHour'] = date('H');
		$var['nextDay'] = date('d.m.Y', strtotime('+ 1 day'));
		$var['twoDay'] = date('d.m.Y', strtotime('+ 2 days'));
		$var['threeDay'] = date('d.m.Y', strtotime('+ 3 days'));

		$var['nextDayName'] = sys::translate('main::dayname_'.date('N', strtotime('+ 1 day')));
		$var['twoDayName'] = sys::translate('main::dayname_'.date('N', strtotime('+ 2 days')));
		$var['threeDayName'] = sys::translate('main::dayname_'.date('N', strtotime('+ 3 days')));


		return sys::parseTpl('main::front_time',$var);
	}


	static function showSocials()
	{
		return sys::parseTpl('main::socials',$var);
	}


	static function showTrailerMedia($film_id, $film_title)
	{
		sys::useLib('main::films');
		global $_db, $_cfg;
		$var['title']=$film_title;


		$r=$_db->query("
			SELECT
				flm.title_orig,
				flm.title_alt,
				flm_lng.title,
				flm.year,
				flm.budget,
				flm.budget_currency,
				flm.duration,
				flm.ukraine_premiere,
				flm.world_premiere,
				flm.yakaboo_url,
				flm.age_limit,
				flm_lng.intro,
				flm_lng.text,
				flm.pre_rating,
				flm.pre_votes,
				flm.rating,
				flm.votes,
				flm.pro_rating,
				flm.pro_votes,
				flm.comments AS `comments_num`,
				flm.banner
			FROM `#__main_films` AS `flm`

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=flm.id
			AND flm_lng.lang_id=".$_cfg['sys::lang_id']."

			WHERE flm.id=".intval($film_id)."

			LIMIT 1
		");

		$result=$_db->fetchAssoc($r);





	$d=$_db->query("
		SELECT
			rev.mark
		FROM `#__main_reviews` AS `rev`

		WHERE rev.id=".intval($_GET['review_id'])."
	");

	$data=$_db->fetchAssoc($d);

	$var['mark'] = $data['mark'];
	$var['year'] = $result['year'];
		$poster=main_films::getFilmFirstPoster($film_id);

		if($poster)
		{
			$image=$_cfg['main::films_url'].$poster['image'];
			$var['poster']['url']=main_films::getFilmUrl($film_id);
		} else {
         	$image = 'blank_news_img.jpg';
		}


		$var['actors']=main_films::getFilmActors($film_id);
		$var['directors']=main_films::getFilmDirectors($film_id);
		$var['poster']['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=7&DDDDD';



		$rat="
			SELECT
				pre_mark
			FROM `#__main_films_rating`

			WHERE
				film_id = '".intval($film_id)."'
		";

		$b=$_db->query($rat);

		$yes = 0;
		$no = 0;
		$total = 0;
		while($obj=$_db->fetchAssoc($b))
		{
			if ($obj['pre_mark']>6)
			{
				$yes++;
			} else {
				$no++;
			}

			$total++;
		}

		if ($total>0)
		{
			$var['pre'] = 1;
			$var['yes'] = round($yes/$total*100);
			$var['no'] = 100 - $var['yes'];
		} else {
			$var['pre'] = 1;
			$var['yes'] = 0;
			$var['no'] = 100;
		}



		$p_date=$result['ukraine_premiere'];


		$p_date=strtotime($p_date);



		if($p_date>time())
		{
			$pre=true;
			$showrat = 0;
		}
		else
		{
			$pre=false;
			$showrat = 1;
		}

		if($pre)
		{
			$var['rating']=$var['yes'].'%';
			$var['votes']=$result['pre_votes'];
		}
		else
		{
			$var['ratin']=$result['rating'];
			$var['rating']=str_replace('.',',',round($result['rating'],1));
			$var['ratin']=str_replace('.',',',round($result['rating'],1));
			$var['votes']=$result['votes'];
		}

		$var['pre'] = $showrat;


		return sys::parseTpl('main::trailer_media',$var);

	}


	static function deletePersonLink($link_id)
	{
		global $_db;
		$_db->delete('main_films_persons',intval($link_id),true);
	}

	static function deleteAwardLink($link_id)
	{
		global $_db;
		$_db->delete('main_films_awards',intval($link_id));
		$_db->delete('main_films_awards_persons',"`film_award_id`=".intval($link_id)."");
	}

	static function deletePhoto($photo_id)
	{
		global $_db;
		$image=$_db->getValue('main_films_photos','image',$photo_id);
		self::deletePhotoImage($image);
		$_db->delete('main_films_photos_persons',"`photo_id`=".intval($photo_id)."");
		$_db->delete('main_films_photos',intval($photo_id));
	}

	static function deletePhotoImage($filename)
	{
		global $_cfg;
		if(is_file($_cfg['main::films_dir'].$filename))
			unlink($_cfg['main::films_dir'].$filename);
		if(is_file($_cfg['main::films_dir'].'x1_'.$filename))
			unlink($_cfg['main::films_dir'].'x1_'.$filename);
		if(is_file($_cfg['main::films_dir'].'x2_'.$filename))
			unlink($_cfg['main::films_dir'].'x2_'.$filename);
		if(is_file($_cfg['main::films_dir'].'x3_'.$filename))
			unlink($_cfg['main::films_dir'].'x3_'.$filename);
		if(is_file($_cfg['main::films_dir'].'x4_'.$filename))
			unlink($_cfg['main::films_dir'].'x4_'.$filename);
		if(is_file($_cfg['main::films_dir'].'x5_'.$filename))
			unlink($_cfg['main::films_dir'].'x5_'.$filename);
	}

	static function uploadPhotoImage($file,$photo_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deletePhotoImage($current_image);

		if($image=sys::uploadFile($file,$_cfg['main::films_dir'],'photo_'.$photo_id, true))
		{
			$_db->setValue('main_films_photos','image',$image,$photo_id);
			self::resizePhotoImage($image);
		}
		return $image;
	}

	static function resizePhotoImage($filename)
	{
		global $_cfg;
		sys::resizeImage($_cfg['main::films_dir'].$filename,$_cfg['main::films_dir'].'x1_'.$filename,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		sys::resizeImage($_cfg['main::films_dir'].$filename,$_cfg['main::films_dir'].'x2_'.$filename,$_cfg['main::x2_width'],$_cfg['main::x2_height']);
		sys::resizeImage($_cfg['main::films_dir'].$filename,$_cfg['main::films_dir'].'x3_'.$filename,$_cfg['main::x3_film_photos_width'],$_cfg['main::x3_film_photos_height'],true);
		sys::resizeImage($_cfg['main::films_dir'].$filename,$_cfg['main::films_dir'].'x4_'.$filename,$_cfg['main::x4_width'],$_cfg['main::x4_height']);
		sys::resizeImage($_cfg['main::films_dir'].$filename,$_cfg['main::films_dir'].'x5_'.$filename,$_cfg['main::x5_width'],$_cfg['main::x5_height']);
	}

	static function uploadPhotos($film_id)
	{
		global $_db, $_cfg, $_user;
		for($i=0; $i<count($_FILES['photos']['name']); $i++)
		{
			$file=array();
			$file['name']=$_FILES['photos']['name'][$i];
			$file['type']=$_FILES['photos']['type'][$i];
			$file['tmp_name']=$_FILES['photos']['tmp_name'][$i];
			$file['error']=$_FILES['photos']['error'][$i];
			$file['size']=$_FILES['photos']['size'][$i];
			if($filename=sys::uploadFile($file,$_cfg['main::films_dir'],uniqid('photo_')))
			{
				self::resizePhotoImage($filename);
				$order_number=$_db->getMaxOrderNumber('main_films_photos',"`film_id`=".intval($film_id)."");
				$_db->query("INSERT INTO `#__main_films_photos`
					(
						`id`,
						`film_id`,
						`user_id`,
						`image`,
						`order_number`,
						`date`,
						`public`
					)
					VALUES
					(
						0,
						'".intval($film_id)."',
						'".$_user['id']."',
						'".$filename."',
						'".$order_number."',
						'".gmdate('Y-m-d H:i:s')."',
						'1'
					)
				");
			}
		}
	}

	static function deletePoster($poster_id)
	{
		global $_db;
		$image=$_db->getValue('main_films_posters','image',$poster_id);
		self::deleteposterImage($image);
		$_db->delete('main_films_posters',intval($poster_id));
	}

	static function uploadPosterImage($file,$poster_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deletePosterImage($current_image);

		if($image=sys::uploadFile($file,$_cfg['main::films_dir'],'poster_'.$poster_id, true))
		{
			$_db->setValue('main_films_posters','image',$image,$poster_id);
			self::resizePosterImage($image);
		}
		return $image;
	}

	static function uploadPosters($film_id)
	{
		global $_db, $_cfg, $_user;
		for($i=0; $i<count($_FILES['posters']['name']); $i++)
		{
			$file=array();
			$file['name']=$_FILES['posters']['name'][$i];
			$file['type']=$_FILES['posters']['type'][$i];
			$file['tmp_name']=$_FILES['posters']['tmp_name'][$i];
			$file['error']=$_FILES['posters']['error'][$i];
			$file['size']=$_FILES['posters']['size'][$i];
			if($filename=sys::uploadFile($file,$_cfg['main::films_dir'],uniqid('poster_')))
			{
				self::resizePosterImage($filename);
				$order_number=$_db->getMaxOrderNumber('main_films_posters',"`film_id`=".intval($film_id)."");
				$_db->query("INSERT INTO `#__main_films_posters`
					(`id`,`film_id`,`user_id`,`image`,`order_number`,`date`)
					VALUES
					(0,".intval($film_id).",".$_user['id'].",'".$filename."',".$order_number.",'".gmdate('Y-m-d H:i:s')."')");
			}
		}
	}

	static function resizePosterImage($filename)
	{
		self::resizePhotoImage($filename);
	}

	static function deletePosterImage($filename)
	{
		self::deletePhotoImage($filename);
	}

	static function deleteTrailer($trailer_id)
	{
		global $_db, $_cfg;
		$trailer=$_db->getRecord('main_films_trailers', $trailer_id);
		if(is_file($_cfg['main::films_dir'].$trailer['file']))
			unlink($_cfg['main::films_dir'].$trailer['file']);
		if(is_file($_cfg['main::films_dir'].$trailer['file_3gp']))
			unlink($_cfg['main::films_dir'].$trailer['file_3gp']);
		self::deleteTrailerImage($trailer['image']);
		$_db->delete('main_films_trailers_persons',"`trailer_id`=".intval($trailer_id)."");
		$_db->delete('main_films_trailers',intval($trailer_id));
	}

	static function uploadTrailerImage($file,$trailer_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deleteTrailerImage($current_image);

		if($image=sys::uploadFile($file,$_cfg['main::films_dir'],'trailer_'.$trailer_id, true))
		{
			$_db->setValue('main_films_trailers','image',$image,$trailer_id);
			self::resizeTrailerImage($image);
		}
		return $image;
	}

	static function uploadTrailerFile($file,$trailer_id,$current_file=false)
	{
		global $_db, $_cfg;
		if($current_file && is_file($_cfg['main::films_dir'].$current_file))
			unlink($_cfg['main::films_dir'].$current_file);

		if($new_file=sys::uploadFile($file,$_cfg['main::films_dir'],'trailer_'.$trailer_id, true))
		{
			$_db->setValue('main_films_trailers','file',$new_file,$trailer_id);
		}
		return $new_file;
	}

	static function uploadSoundtrackFile($file,$soundtrack_id,$current_file=false)
	{
		global $_db, $_cfg;
		if($current_file && is_file($_cfg['main::films_dir'].$current_file))
			unlink($_cfg['main::films_dir'].$current_file);

		if($new_file=sys::uploadFile($file,$_cfg['main::films_dir'],'soundtrack_'.$soundtrack_id, true))
		{
			$_db->setValue('main_films_soundtracks','file',$new_file,$soundtrack_id);
		}
		return $new_file;
	}

	static function uploadTrailerFile3gp($file,$trailer_id,$current_file=false)
	{
		global $_db, $_cfg;
		if($current_file && is_file($_cfg['main::films_dir'].$current_file))
			unlink($_cfg['main::films_dir'].$current_file);

		if($new_file=sys::uploadFile($file,$_cfg['main::films_dir'],'trailer_'.$trailer_id, true))
		{
			$_db->setValue('main_films_trailers','file_3gp',$new_file,$trailer_id);
		}
		return $new_file;
	}

	static function deleteTrailerImage($filename)
	{
		self::deletePhotoImage($filename);
	}

	static function resizeTrailerImage($filename)
	{
		self::resizePhotoImage($filename);
	}

	static function deleteWallpaper($wallpaper_id)
	{
		global $_db, $_cfg;
		$wallpaper=$_db->getRecord('main_films_wallpapers', $wallpaper_id);
		if(is_file($_cfg['main::films_dir'].$wallpaper['image_1024x768']))
			unlink($_cfg['main::films_dir'].$wallpaper['image_1024x768']);
		if(is_file($_cfg['main::films_dir'].$wallpaper['image_1280x800']))
			unlink($_cfg['main::films_dir'].$wallpaper['image_1280x800']);
		if(is_file($_cfg['main::films_dir'].$wallpaper['image_1280x1024']))
			unlink($_cfg['main::films_dir'].$wallpaper['image_1280x1024']);
		if(is_file($_cfg['main::films_dir'].$wallpaper['image_1680x1050']))
			unlink($_cfg['main::films_dir'].$wallpaper['image_1680x1050']);
		self::deleteWallpaperImage($wallpaper['image']);
		$_db->delete('main_films_wallpapers_persons',"`wallpaper_id`=".intval($wallpaper_id)."");
		$_db->delete('main_films_wallpapers',intval($wallpaper_id));
	}

	static function deleteWallpaperImage($filename)
	{
		self::deletePhotoImage($filename);
	}

	static function uploadWallpaperImage($file,$wallpaper_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deleteWallpaperImage($current_image);

		if($image=sys::uploadFile($file,$_cfg['main::films_dir'],'wallpaper_'.$wallpaper_id, true))
		{
			$_db->setValue('main_films_wallpapers','image',$image,$wallpaper_id);
			self::resizeWallpaperImage($image);
		}
		return $image;
	}

	static function uploadWallpaperFile($file,$wallpaper_id,$current_file=false, $resulution='1024x768')
	{
		global $_db, $_cfg;
		if($current_file && is_file($_cfg['main::films_dir'].$current_file))
			unlink($_cfg['main::films_dir'].$current_file);

		if($new_file=sys::uploadFile($file,$_cfg['main::films_dir'],'wallp_'.$resulution.'_'.$wallpaper_id, true))
		{
			$_db->setValue('main_films_wallpapers','image_'.$resulution,$new_file,$wallpaper_id);
		}
		return $new_file;
	}

	static function resizeWallpaperImage($filename)
	{
		self::resizePhotoImage($filename);
	}

	static function deleteSoundTrack($soundtrack_id)
	{
		global $_db, $_cfg;
		$file=$_db->getValue('main_films_soundtracks','file',$soundtrack_id);
		if(is_file($_cfg['main::films_dir'].$file))
			unlink($_cfg['main::films_dir'].$file);
		$_db->delete('main_films_soundtracks',$soundtrack_id);
	}

	static function deleteLink($link_id)
	{
		global $_db;
		$_db->delete('main_films_links',intval($link_id),true);
	}

	static function uploadAlbumPoster($file,$film_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image && is_file($_cfg['main::films_dir'].$current_image))
			unlink($_cfg['main::films_dir'].$current_image);

		if($image=sys::uploadFile($file,$_cfg['main::films_dir'],'album_'.$film_id, true))
		{
			$_db->setValue('main_films','album_poster',$image,$film_id);
			sys::resizeImage($_cfg['main::films_dir'].$image,false,$_cfg['main::x2_width'],$_cfg['main::x2_height']);
		}
		return $image;
	}

	static function showFilmAdminTabs($film_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('sys::info');
		$tabs['info']['url']='?mod=main&act=film_edit&id='.$film_id;

		$tabs['persons']['title']=sys::translate('main::persons');
		$tabs['persons']['url']='?mod=main&act=film_persons_table&film_id='.$film_id;

		$tabs['trailers']['title']=sys::translate('main::trailers');
		$tabs['trailers']['url']='?mod=main&act=film_trailers_table&film_id='.$film_id;

		$tabs['photos']['title']=sys::translate('main::photos');
		$tabs['photos']['url']='?mod=main&act=film_photos_table&film_id='.$film_id;

		$tabs['posters']['title']=sys::translate('main::posters');
		$tabs['posters']['url']='?mod=main&act=film_posters_table&film_id='.$film_id;

		$tabs['wallpapers']['title']=sys::translate('main::wallpapers');
		$tabs['wallpapers']['url']='?mod=main&act=film_wallpapers_table&film_id='.$film_id;

		$tabs['soundtracks']['title']=sys::translate('main::soundtracks');
		$tabs['soundtracks']['url']='?mod=main&act=film_soundtracks_table&film_id='.$film_id;

		$tabs['links']['title']=sys::translate('main::links');
		$tabs['links']['url']='?mod=main&act=film_links_table&film_id='.$film_id;

		$tabs['awards']['title']=sys::translate('main::awards');
		$tabs['awards']['url']='?mod=main&act=film_awards_table&film_id='.$film_id;

		$tabs['shows']['title']=sys::translate('main::shows');
		$tabs['shows']['url']='?mod=main&act=film_shows_table&film_id='.$film_id;

		$tabs['series']['title']=sys::translate('main::series');
		$tabs['series']['url']='?mod=main&act=film_series_table&film_id='.$film_id;

		return sys_gui::showTabs($tabs, $active_tab);
	}

	static function getFilmGenresIds($film_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `genre_id`
			FROM `#__main_films_genres`
			WHERE `film_id`=".intval($film_id)."");
		while(list($genre_id)=$_db->fetchArray($r))
		{
			$result[]=$genre_id;
		}
		return $result;
	}

	static function linkFilmGenres($film_id, $genres)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_films_genres`
			WHERE `film_id`='".intval($film_id)."'");
		if($genres)
		{
			$genres=array_unique($genres);
			foreach ($genres as $genre_id=>$name)
			{
				if($genre_id)
				{
					$_db->query("INSERT INTO `#__main_films_genres`
						(
							`film_id`,
							`genre_id`
						)
						VALUES
						(
							".intval($film_id).",
							".intval($genre_id)."
						)
					");
				}
			}
		}
	}

	static function getFilmStudiosIds($film_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `studio_id`
			FROM `#__main_films_studios`
			WHERE `film_id`=".intval($film_id)."");
		while(list($studio_id)=$_db->fetchArray($r))
		{
			$result[]=$studio_id;
		}
		return $result;
	}

	static function linkFilmStudios($film_id, $studios)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_films_studios`
			WHERE `film_id`='".intval($film_id)."'");
		if($studios)
		{
			$studios=array_unique($studios);
			foreach ($studios as $studio_id=>$name)
			{
				if($studio_id)
				{
					$_db->query("INSERT INTO `#__main_films_studios`
						(
							`film_id`,
							`studio_id`
						)
						VALUES
						(
							".intval($film_id).",
							".intval($studio_id)."
						)
					");
				}
			}
		}
	}

	static function getFilmDistributorsIds($film_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `distributor_id`
			FROM `#__main_films_distributors`
			WHERE `film_id`=".intval($film_id)."");
		while(list($distributor_id)=$_db->fetchArray($r))
		{
			$result[]=$distributor_id;
		}
		return $result;
	}

	static function linkFilmDistributors($film_id, $distributors)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_films_distributors`
			WHERE `film_id`='".intval($film_id)."'");
		if($distributors)
		{
			$distributors=array_unique($distributors);
			foreach ($distributors as $distributor_id=>$name)
			{
				if($distributor_id)
				{
					$_db->query("INSERT INTO `#__main_films_distributors`
						(
							`film_id`,
							`distributor_id`
						)
						VALUES
						(
							".intval($film_id).",
							".intval($distributor_id)."
						)
					");
				}
			}
		}
	}

	static function getFilmCountriesIds($film_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `country_id`
			FROM `#__main_films_countries`
			WHERE `film_id`=".intval($film_id)."");
		while(list($country_id)=$_db->fetchArray($r))
		{
			$result[]=$country_id;
		}
		return $result;
	}

	static function linkFilmCountries($film_id, $countries)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_films_countries`
			WHERE `film_id`='".intval($film_id)."'");
		if($countries)
		{
			$countries=array_unique($countries);
			foreach ($countries as $country_id=>$name)
			{
				if($country_id)
				{
					$_db->query("INSERT INTO `#__main_films_countries`
						(
							`film_id`,
							`country_id`
						)
						VALUES
						(
							".intval($film_id).",
							".intval($country_id)."
						)
					");
				}
			}
		}
	}

	static function getPhotoPersonsIds($photo_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `person_id`
			FROM `#__main_films_photos_persons`
			WHERE `photo_id`=".intval($photo_id)."");
		while(list($person_id)=$_db->fetchArray($r))
		{
			$result[]=$person_id;
		}
		return $result;
	}

	static function getPosterPersonsIds($poster_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `person_id`
			FROM `#__main_persons_posters`
			WHERE `poster_id`=".intval($poster_id)."");
		while(list($person_id)=$_db->fetchArray($r))
		{
			$result[]=$person_id;
		}
		return $result;
	}

	static function getAwardPersonsIds($award_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `person_id`
			FROM `#__main_films_awards_persons`
			WHERE `film_award_id`=".intval($award_id)."");
		while(list($person_id)=$_db->fetchArray($r))
		{
			$result[]=$person_id;
		}
		return $result;
	}

	static function linkPhotoPersons($photo_id, $persons)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_films_photos_persons`
			WHERE `photo_id`='".intval($photo_id)."'");
		if($persons)
		{
			$persons=array_unique($persons);
			foreach ($persons as $person_id=>$name)
			{
				if($person_id)
				{
					$_db->query("INSERT INTO `#__main_films_photos_persons`
						(
							`photo_id`,
							`person_id`
						)
						VALUES
						(
							".intval($photo_id).",
							".intval($person_id)."
						)
					");
				}
			}
		}
	}

	static function linkPosterPersons($poster_id, $persons)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_persons_posters`
			WHERE `poster_id`='".intval($poster_id)."'");
		if($persons)
		{
			$persons=array_unique($persons);
			foreach ($persons as $person_id=>$name)
			{
				if($person_id)
				{
					$_db->query("INSERT INTO `#__main_persons_posters`
						(
							`poster_id`,
							`person_id`
						)
						VALUES
						(
							".intval($poster_id).",
							".intval($person_id)."
						)
					");
				}
			}
		}
	}

	static function linkAwardPersons($award_id, $persons)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_films_awards_persons`
			WHERE `film_award_id`='".intval($award_id)."'");
		if($persons)
		{
			$persons=array_unique($persons);
			foreach ($persons as $person_id=>$name)
			{
				if($person_id)
				{
					$_db->query("INSERT INTO `#__main_films_awards_persons`
						(
							`film_award_id`,
							`person_id`
						)
						VALUES
						(
							".intval($award_id).",
							".intval($person_id)."
						)
					");
				}
			}
		}
	}

	static function getTrailerPersonsIds($trailer_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `person_id`
			FROM `#__main_films_trailers_persons`
			WHERE `trailer_id`=".intval($trailer_id)."");
		while(list($person_id)=$_db->fetchArray($r))
		{
			$result[]=$person_id;
		}
		return $result;
	}

	static function getWallpaperPersonsIds($wallpaper_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `person_id`
			FROM `#__main_films_wallpapers_persons`
			WHERE `wallpaper_id`=".intval($wallpaper_id)."");
		while(list($person_id)=$_db->fetchArray($r))
		{
			$result[]=$person_id;
		}
		return $result;
	}

	static function linkTrailerPersons($trailer_id, $persons)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_films_trailers_persons`
			WHERE `trailer_id`='".intval($trailer_id)."'");
		if($persons)
		{
			$persons=array_unique($persons);
			foreach ($persons as $person_id=>$name)
			{
				if($person_id)
				{
					$_db->query("INSERT INTO `#__main_films_trailers_persons`
						(
							`trailer_id`,
							`person_id`
						)
						VALUES
						(
							".intval($trailer_id).",
							".intval($person_id)."
						)
					");
				}
			}
		}
	}


	static function linkwallpaperPersons($wallpaper_id, $persons)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_films_wallpapers_persons`
			WHERE `wallpaper_id`='".intval($wallpaper_id)."'");
		if($persons)
		{
			$persons=array_unique($persons);
			foreach ($persons as $person_id=>$name)
			{
				if($person_id)
				{
					$_db->query("INSERT INTO `#__main_films_wallpapers_persons`
						(
							`wallpaper_id`,
							`person_id`
						)
						VALUES
						(
							".intval($wallpaper_id).",
							".intval($person_id)."
						)
					");
				}
			}
		}
	}

	static function getFilmsNames($arr_ids)
	{
		global $_db;
		$result=array();
		if(!$arr_ids || !count($arr_ids))
			return false;
		for($i=0; $i<count($arr_ids); $i++)
		{
			$arr_ids[$i]=intval($arr_ids[$i]);
		}
		$str_ids=implode(',',$arr_ids);
		$r=$_db->query("SELECT `id`, `name`
			FROM `#__main_films`
			WHERE `id` IN(".$str_ids.")");
		while($obj=$_db->fetchAssoc($r))
		{
			$result[$obj['id']]=$obj['name'];
		}
		return $result;
	}

	static function showTopFilms()
	{
		global $_cfg, $_db;
		$start_date=gmdate('Y-m-d H:i:s',time()-7*_DAY);
		$cache_name='main_films_top_'.$_cfg['sys::lang'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::top_cache_period']*_HOUR))
			return $cache;
		$r=$_db->query("
			SELECT lng.title, lng.record_id AS `id`, COUNT(msg.id) AS `num`
			FROM `#__main_discuss_messages` AS `msg`
			LEFT JOIN `#__main_films_lng` AS `lng`
			ON lng.record_id=msg.object_id
			AND lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE msg.date>'".$start_date."' AND msg.object_type='film'

			GROUP BY msg.object_id

			ORDER BY `num` DESC, `id` DESC

			LIMIT 0,4
		");

		$var['objects']=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=self::getFilmUrl($obj['id']);
			$obj['alt']=sys::translate('main::film').' &laquo;'.htmlspecialchars($obj['title']).'&raquo;';
			$var['objects'][]=$obj;
		}
		$var['title']=sys::translate('main::films');
		$var['class']='graph';

		$result=sys::parseTpl('main::week_top',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function getFilmProducers($film_id)
	{
		global $_db, $_cfg;
		sys::useLib('main::persons');
		$r=$_db->query("
			SELECT
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`,
				flm_prs.person_id AS `id`
			FROM `#__main_films_persons` AS `flm_prs`

			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=flm_prs.person_id
			AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE flm_prs.film_id=".intval($film_id)."
			AND flm_prs.profession_id=6

			ORDER BY flm_prs.order_number
		");
		$result=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=main_persons::getPersonUrl($obj['id']);
			$result[]=$obj;
		}
		return $result;
	}

	static function getFilmWritists($film_id)
	{
		global $_db, $_cfg;
		sys::useLib('main::persons');
		$r=$_db->query("
			SELECT
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`,
				flm_prs.person_id AS `id`
			FROM `#__main_films_persons` AS `flm_prs`

			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=flm_prs.person_id
			AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE flm_prs.film_id=".intval($film_id)."
			AND flm_prs.profession_id=3

			ORDER BY flm_prs.order_number
		");
		$result=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=main_persons::getPersonUrl($obj['id']);
			$result[]=$obj;
		}
		return $result;
	}

	static function getFilmCameras($film_id)
	{
		global $_db, $_cfg;
		sys::useLib('main::persons');
		$r=$_db->query("
			SELECT
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`,
				flm_prs.person_id AS `id`
			FROM `#__main_films_persons` AS `flm_prs`

			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=flm_prs.person_id
			AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE flm_prs.film_id=".intval($film_id)."
			AND flm_prs.profession_id=4

			ORDER BY flm_prs.order_number
		");
		$result=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=main_persons::getPersonUrl($obj['id']);
			$result[]=$obj;
		}
		return $result;
	}

	static function getFilmMusics($film_id)
	{
		global $_db, $_cfg;
		sys::useLib('main::persons');
		$r=$_db->query("
			SELECT
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`,
				flm_prs.person_id AS `id`
			FROM `#__main_films_persons` AS `flm_prs`

			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=flm_prs.person_id
			AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE flm_prs.film_id=".intval($film_id)."
			AND flm_prs.profession_id=5

			ORDER BY flm_prs.order_number
		");
		$result=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=main_persons::getPersonUrl($obj['id']);
			$result[]=$obj;
		}
		return $result;
	}

	static function getBiggestPoster($film)
	{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT image
			FROM `#__main_films_posters`
			WHERE film_id='".$film."'
		");

		$max['w'] = 0;
		$max['i'] = 0;

		while($obj=$_db->fetchAssoc($r))
		{
			$from_path = '/var/www/html/multiplex/multiplex.in.ua/public/main/films/'.$obj['image'];
			$image_info = getImageSize($from_path);

			if ($image_info[0]==1000 && $image_info[1]==200)
			{
				$max['w'] = $image_info[0];
				$max['i'] = $obj['image'];
			}

		}
		return $max['i'];
	}

	static function translitIt($str)
	{
	    $tr = array(
	        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
	        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","�?"=>"i",
	        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
	        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
	        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
	        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
	        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	        "the"=>"","the "=>""," the"=>""," the "=>"-",
	        "The"=>"","The "=>""," The"=>""," The "=>"-",
	        "... "=> "", " - "=> "-"," "=> "-", "."=> "", "/"=> "-"
	    );
	    return strtolower(strtr($str,$tr));
	}

	static function translitIt2($str)
	{
	    $tr = array(
	        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
	        "Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","�?"=>"I",
	        "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
	        "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
	        "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"Ts","Ч"=>"Ch",
	        "Ш"=>"Sh","Щ"=>"Sch","Ъ"=>"","Ы"=>"Yi","Ь"=>"",
	        "Э"=>"E","Ю"=>"Yu","Я"=>"Ya","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	        "... "=> "", " - "=> "-", "."=> "", "/"=> "-"
	    );
	    return strtolower(strtr($str,$tr));
	}


	static function getFilmUrl($film_id, $lang=false)
	{

		if ($film_id>0)
		{
			global $_cfg, $_db;

			$r=$_db->query("
				SELECT name, title_orig
				FROM `#__main_films`
				WHERE id='".$film_id."'
			");



			while($obj=$_db->fetchAssoc($r))
			{
				if ($obj['title_orig'])
				{
				    $urlstr = self::translitIt($obj['title_orig']);
				} else {
				    $urlstr = self::translitIt($obj['name']);
				}
				    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

			}


            
            if (!$lang) $lang=$_cfg['sys::lang'];

			if ($lang=='ru')
			{
			return _ROOT_URL.'film/'.$urlstr.'-'.$film_id.'.phtml';
			} else {
			return _ROOT_URL.$lang.'/film/'.$urlstr.'-'.$film_id.'.phtml';
			}
		} else {
			return sys::rewriteUrl('?mod=main&act=film&film_id='.$film_id);
		}

	}

	static function getFilmUkUrl($film_id)
	{

		if ($film_id>0)
		{
			global $_cfg, $_db;

			$r=$_db->query("
				SELECT name, title_orig
				FROM `#__main_films`
				WHERE id='".$film_id."'
			");



			while($obj=$_db->fetchAssoc($r))
			{
				if ($obj['title_orig'])
				{
				    $urlstr = self::translitIt($obj['title_orig']);
				} else {
				    $urlstr = self::translitIt($obj['name']);
				}
				    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

			}



			$lang='uk';

			return _ROOT_URL.$lang.'/film/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
			return _ROOT_URL.'uk/main/film/film_id/'.$film_id.'.phtml';
		}

	}


	static function showNearestPremieres()
	{
		global $_cfg, $_db;
		$cache_name='main_nearest_premiers_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::premiers_cache_period']*_HOUR))
			return $cache;
		$var['objects']=array();
		$r=$_db->query("SELECT
				flm.id,
				flm_lng.title,
				flm.year,
				MONTH(flm.ukraine_premiere) AS `month`,
				DATE_FORMAT(flm.ukraine_premiere,'%e') AS `day`,
				pst.image
			FROM `#__main_films` AS `flm`

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=flm.id
			AND flm_lng.lang_id='".intval($_cfg['sys::lang_id'])."'

			LEFT JOIN `#__main_films_posters` AS `pst`
			ON pst.film_id=flm.id
			AND pst.order_number=1

			WHERE flm.ukraine_premiere>'".date('Y-m-d')."' AND pst.image!=''

			ORDER BY flm.ukraine_premiere ASC, flm.pre_rating DESC
			LIMIT 0,5
			");

		$i=1;

		while($film=$_db->fetchAssoc($r))
		{
			$film['url']=self::getFilmUrl($film['id']);
			if(self::isFilmTrailers($film['id']))
			{
				$film['trailers_url']=self::getFilmTrailersUrl($film['id']);
				$film['isTrailer']='1';
			}else{
				$film['trailers_url']=false;
				$film['isTrailer']='0';
			}

			if(self::isFilmShows($film['id']))
			{
				$film['shows_url']=self::getFilmShowsUrl($film['id']);
				$film['isShows']='1';
			}else{
				$film['shows_url']=false;
				$film['isShows']='0';
			}

			$film['title']=htmlspecialchars($film['title']);

// comment by Mike
/*					
			if($film['image'])
			{
				$image=$_cfg['main::films_url'].$film['image'];
			} else {
	         	$image = 'blank_news_img.jpg';
			}
			$film['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=25&111111111';
*/
		// inserted by Mike begin
  	    $image="x2_".$film['image'];	
		if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$film['image'] = $image;
		// inserted by Mike end


			if ($i==5)
			{
				$film['class']='no-margin';
			}
			$film['date']=sys::translate('main::from').' '.$film['day'].' '.sys::translate('main::month_'.$film['month'].'_a');
			$var['objects'][]=$film;
			$i++;
		}

		$result=sys::parseTpl('main::nearest_premiers',$var);
		sys::setCache($cache_name,$result);
		return $result;

	}

	static function showNearestWaitedPremieres()
	{
		global $_cfg, $_db;
		$cache_name='main_nearest_premiers_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::premiers_cache_period']*_HOUR))
			return $cache;
		$var['objects']=array();
		$r=$_db->query("SELECT
				flm.id,
				rat.pre_mark,
				flm_lng.title,
				flm.year,
				MONTH(flm.ukraine_premiere) AS `month`,
				DATE_FORMAT(flm.ukraine_premiere,'%e') AS `day`,
				pst.image
			FROM `#__main_films` AS `flm`

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=flm.id
			AND flm_lng.lang_id='".intval($_cfg['sys::lang_id'])."'

			LEFT JOIN `#__main_films_posters` AS `pst`
			ON pst.film_id=flm.id
			AND pst.order_number=1


			LEFT JOIN (SELECT SUM(pre_mark) as pre_mark,film_id FROM #__main_films_rating GROUP BY film_id) `rat`
			ON flm.id=rat.film_id

			WHERE (YEARWEEK(DATE_SUB(flm.ukraine_premiere, INTERVAL 1 DAY))=YEARWEEK(NOW())
			OR (flm.ukraine_premiere>='".date('Y-m-d')."' AND
			flm.ukraine_premiere<='".date('Y-m-d',mktime(0, 0, 0, date("m")+3, date("d"),   date("Y")))."')) AND pst.image!=''


			ORDER BY rat.pre_mark DESC, flm.ukraine_premiere ASC, flm.pre_rating DESC
			LIMIT 0,5
			");


		$i=1;

		while($film=$_db->fetchAssoc($r))
		{
			$film['url']=self::getFilmUrl($film['id']);
			if(self::isFilmTrailers($film['id']))
			{
				$film['trailers_url']=self::getFilmTrailersUrl($film['id']);
				$film['isTrailer']='1';
			}else{
				$film['trailers_url']=false;
				$film['isTrailer']='0';
			}

			if(self::isFilmShows($film['id']))
			{
				$film['shows_url']=self::getFilmShowsUrl($film['id']);
				$film['isShows']='1';
			}else{
				$film['shows_url']=false;
				$film['isShows']='0';
			}

			$film['title']=htmlspecialchars($film['title']);
			
// comment by Mike
/*					
			if($film['image'])
			{
				$image=$_cfg['main::films_url'].$film['image'];
			} else {
	         	$image = 'blank_news_img.jpg';
			}
		$film['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=25&222222222222';
*/
		// inserted by Mike begin
  	    $image="x2_".$film['image'];	
		if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$film['image'] = $image;
		// inserted by Mike end
			

			if ($i==5)
			{
				$film['class']='no-margin';
			}
			$film['date']=sys::translate('main::from').' '.$film['day'].' '.sys::translate('main::month_'.$film['month'].'_a');
			$var['objects'][]=$film;
			$i++;
		}

		$var['id'] = 'ajaxWaited';
		$result=sys::parseTpl('main::nearest_premiers',$var);
		sys::setCache($cache_name,$result);
		return $result;

	}

	static function showNearestDiscussedPremieres()
	{

		global $_cfg, $_db;
		$cache_name='main_nearest_premiers_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::premiers_cache_period']*_HOUR))
			return $cache;
		$var['objects']=array();
		$r=$_db->query("SELECT
				flm.id,
				flm_lng.title,
				MONTH(flm.ukraine_premiere) AS `month`,
				DATE_FORMAT(flm.ukraine_premiere,'%e') AS `day`,
				pst.image
			FROM `#__main_films` AS `flm`

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=flm.id
			AND flm_lng.lang_id='".intval($_cfg['sys::lang_id'])."'

			LEFT JOIN `#__main_films_posters` AS `pst`
			ON pst.film_id=flm.id
			AND pst.order_number=1

			WHERE (YEARWEEK(DATE_SUB(flm.ukraine_premiere, INTERVAL 1 DAY))=YEARWEEK(NOW())
			OR (flm.ukraine_premiere>='".date('Y-m-d')."' AND
			flm.ukraine_premiere<='".date('Y-m-d',mktime(0, 0, 0, date("m")+3, date("d"),   date("Y")))."')) AND pst.image!=''

			ORDER BY flm.comments DESC, flm.ukraine_premiere ASC, flm.pre_rating DESC
			LIMIT 0,5
			");




		$i=1;

		while($film=$_db->fetchAssoc($r))
		{
			$film['url']=self::getFilmUrl($film['id']);
			if(self::isFilmTrailers($film['id']))
			{
				$film['trailers_url']=self::getFilmTrailersUrl($film['id']);
				$film['isTrailer']='1';
			}else{
				$film['trailers_url']=false;
				$film['isTrailer']='0';
			}

			if(self::isFilmShows($film['id']))
			{
				$film['shows_url']=self::getFilmShowsUrl($film['id']);
				$film['isShows']='1';
			}else{
				$film['shows_url']=false;
				$film['isShows']='0';
			}

			$film['title']=htmlspecialchars($film['title']);
		if($film['image'])
		{
			$image=$_cfg['main::films_url'].$film['image'];
		} else {
         	$image = 'blank_news_img.jpg';
		}

		$film['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=25&333333333333333';
			if ($i==5)
			{
				$film['class']='no-margin';
			}
			$film['date']=sys::translate('main::from').' '.$film['day'].' '.sys::translate('main::month_'.$film['month'].'_a');
			$var['objects'][]=$film;
			$i++;
		}

		$var['id'] = 'ajaxPremiers';
		$result=sys::parseTpl('main::nearest_premiers',$var);
		sys::setCache($cache_name,$result);
		return $result;

	}



	static function getFilmPhotosUrl($film_id, $lang=false)
	{
		if ($film_id>0)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}


		if (!$lang) $lang=$_cfg['sys::lang'];
		
		if ($lang=='ru')
		{
		return _ROOT_URL.'film-photo/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film-photo/'.$urlstr.'-'.$film_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=film_phots&film_id='.$film_id);
		}
	}

	static function getFilmPostersUrl($film_id)
	{
		if ($film_id>0)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}



		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'film-poster/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film-poster/'.$urlstr.'-'.$film_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=film_posts&film_id='.$film_id);
		}
	}

	static function getFilmTrailersUrl($film_id)
	{
		// Заменил trailers на trailer чтоб не было двух разных страниц.
		if ($film_id>0)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");
	
		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}
		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'film-trailer/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film-trailer/'.$urlstr.'-'.$film_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=film_trailer&film_id='.$film_id);
		}
	}

	static function getFilmTrailerUrl($film_id)
	{
		if ($film_id>0)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");
	
		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}
		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'film-trailer/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film-trailer/'.$urlstr.'-'.$film_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=film_trailer&film_id='.$film_id);
		}		
//		return sys::rewriteUrl('?mod=main&act=film_trailer&trailer_id='.$trailer_id);
	}

	static function getFilmWallpapersUrl($film_id, $lang=false)
	{
		global $_cfg, $_db;
		if(!$lang) $lang=$_cfg['sys::lang'];
		
		if ($lang=='ru') 	$lang="";
		else 				$lang="$lang/";

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");

		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])	$urlstr = self::translitIt($obj['title_orig']);
			 else 			    	$urlstr = self::translitIt($obj['name']);
			$urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);
		}
		return _ROOT_URL.$lang.'film_walls/'.$urlstr.'-'.$film_id.'.phtml';
	}

	static function showNewPhotos()
	{
		global $_cfg, $_db;
		$cache_name='main_new_photos_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::new_photos_cache_period']*60))
			return $cache;
		$var['objects']=array();
		$r=$_db->query("
				SELECT data.film_id, flm_lng.title FROM `#__main_films_photos` AS `data`
				LEFT JOIN `#__main_films_lng` AS `flm_lng`
				ON flm_lng.record_id=data.film_id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
				GROUP BY data.film_id
				ORDER BY data.date DESC
				LIMIT 0,5
			");

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['alt']=sys::translate('main::film').' &laquo;'.$obj['title'].'&raquo;, '.sys::translate('main::photos');
			$obj['url']=self::getFilmPhotosUrl($obj['film_id']);
			$var['objects'][]=$obj;
		}
		$result=sys::parseTpl('main::noveltys',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function showNewPosters()
	{
		global $_cfg, $_db;
		$cache_name='main_new_posters_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::new_posters_cache_period']*60))
			return $cache;
		$var['objects']=array();
		$r=$_db->query("
				SELECT data.film_id, flm_lng.title FROM `#__main_films_posters` AS `data`
				LEFT JOIN `#__main_films_lng` AS `flm_lng`
				ON flm_lng.record_id=data.film_id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
				GROUP BY data.film_id
				ORDER BY data.date DESC
				LIMIT 0,5
			");
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['alt']=sys::translate('main::film').' &laquo;'.$obj['title'].'&raquo;, '.sys::translate('main::posters');
			$obj['url']=self::getFilmPostersUrl($obj['film_id']);
			$var['objects'][]=$obj;
		}
		$result=sys::parseTpl('main::noveltys',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function showNewTrailers()
	{
		global $_cfg, $_db;
		$cache_name='main_new_trailers_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::new_trailers_cache_period']*60))
			return $cache;
		$var['objects']=array();
		$r=$_db->query("
				SELECT data.film_id, flm_lng.title FROM `#__main_films_trailers` AS `data`
				LEFT JOIN `#__main_films_lng` AS `flm_lng`
				ON flm_lng.record_id=data.film_id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
				GROUP BY data.film_id
				ORDER BY data.date DESC
				LIMIT 0,5
			");
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['alt']=sys::translate('main::film').' &laquo;'.$obj['title'].'&raquo;, '.sys::translate('main::trailers');
			$obj['url']=self::getFilmTrailersUrl($obj['film_id']);
			$var['objects'][]=$obj;
		}
		$result=sys::parseTpl('main::noveltys',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function showNewWallpapers()
	{
		global $_cfg, $_db;
		$cache_name='main_new_wallpapers_'.$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::new_wallpapers_cache_period']*60))
			return $cache;
		$var['objects']=array();
		$r=$_db->query("
				SELECT data.film_id, flm_lng.title FROM `#__main_films_wallpapers` AS `data`
				LEFT JOIN `#__main_films_lng` AS `flm_lng`
				ON flm_lng.record_id=data.film_id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
				GROUP BY data.film_id
				ORDER BY data.date DESC
				LIMIT 0,5
			");
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['alt']=sys::translate('main::film').' &laquo;'.$obj['title'].'&raquo;, '.sys::translate('main::wallpapers');
			$obj['url']=self::getFilmWallpapersUrl($obj['film_id']);
			$var['objects'][]=$obj;
		}
		$result=sys::parseTpl('main::noveltys',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function getFilmGenresTitles($film_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT gnr_lng.title FROM `#__main_films_genres` AS `flm_gnr`
			LEFT JOIN `#__main_genres_lng` AS `gnr_lng`
			ON gnr_lng.record_id=flm_gnr.genre_id
			AND gnr_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_genres` AS `gnr`
			ON gnr.id=flm_gnr.genre_id
			WHERE flm_gnr.film_id=".intval($film_id)."
			ORDER BY gnr.order_number
		");
		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$result[]=htmlspecialchars($obj['title']);
		}
		return $result;
	}

	static function getFilmGenresTitlesIds($film_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT gnr.id, gnr_lng.title FROM `#__main_films_genres` AS `flm_gnr`
			LEFT JOIN `#__main_genres_lng` AS `gnr_lng`
			ON gnr_lng.record_id=flm_gnr.genre_id
			AND gnr_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_genres` AS `gnr`
			ON gnr.id=flm_gnr.genre_id
			WHERE flm_gnr.film_id=".intval($film_id)."
			ORDER BY gnr.order_number
		");
		$result=array();
		$res=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$res['id']=$obj['id'];
			$res['title']=htmlspecialchars($obj['title']);
			$result[]=$res;
		}
		return $result;
	}

	static function getGenreTitle($genre_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT title FROM `#__main_genres_lng`

			WHERE record_id=".intval($genre_id)."
			AND  lang_id=".intval($_cfg['sys::lang_id'])."
		");
		$result=array();
		$res=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$res['title']=htmlspecialchars($obj['title']);
		}
		return $res['title'];
	}

	static function my_mb_ucfirst($str) {
	    $fc = mb_strtoupper(mb_substr($str, 0, 1));
	    return $fc.mb_substr($str, 1);
	}

	static function getFilmGenresTitlesS($film_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT gnr_lng.titles,gnr_lng.record_id FROM `#__main_films_genres` AS `flm_gnr`
			LEFT JOIN `#__main_genres_lng` AS `gnr_lng`
			ON gnr_lng.record_id=flm_gnr.genre_id
			AND gnr_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_genres` AS `gnr`
			ON gnr.id=flm_gnr.genre_id
			WHERE flm_gnr.film_id=".intval($film_id)."
			ORDER BY gnr.order_number
		");
		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{

			$var['id'] = $obj['record_id'];
			$var['title'] = self::my_mb_ucfirst($obj['titles']);

			$result[]=$var;
		}
		return $result;
	}


	static function getFilmCountriesTitles($film_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT ctr_lng.title FROM `#__main_films_countries` AS `flm_ctr`
			LEFT JOIN `#__main_countries_lng` AS `ctr_lng`
			ON ctr_lng.record_id=flm_ctr.country_id
			AND ctr_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_countries` AS `ctr`
			ON ctr.id=flm_ctr.country_id
			WHERE flm_ctr.film_id=".intval($film_id)."
			ORDER BY ctr.order_number
		");
		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$result[]=htmlspecialchars($obj['title']);
		}
		return $result;
	}

	static function getFilmCountriesTitlesIds($film_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT ctr.id, ctr_lng.title FROM `#__main_films_countries` AS `flm_ctr`
			LEFT JOIN `#__main_countries_lng` AS `ctr_lng`
			ON ctr_lng.record_id=flm_ctr.country_id
			AND ctr_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_countries` AS `ctr`
			ON ctr.id=flm_ctr.country_id
			WHERE flm_ctr.film_id=".intval($film_id)."
			ORDER BY ctr.order_number
		");
		$result=array();
		$res=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$res['id']=$obj['id'];
			$res['title']=htmlspecialchars($obj['title']);
			$result[]=$res;
		}
		return $result;
	}


	static function getFilmStudiosTitles($film_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT std_lng.title FROM `#__main_films_studios` AS `flm_std`
			LEFT JOIN `#__main_studios_lng` AS `std_lng`
			ON std_lng.record_id=flm_std.studio_id
			AND std_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_studios` AS `std`
			ON std.id=flm_std.studio_id
			WHERE flm_std.film_id=".intval($film_id)."
		");
		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$result[]=htmlspecialchars($obj['title']);
		}
		return $result;
	}

	static function getFilmSeriesUrl($film_id)
	{
		if ($film_id>0)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");

		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);
		}

		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'film-series/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film-series/'.$urlstr.'-'.$film_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=film_series&film_id='.$film_id);
		}
	}

	static function getFilmRatingUrl($film_id)
	{
		if ($film_id>0)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}



		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'film-rating/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film-rating/'.$urlstr.'-'.$film_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=film_rating&film_id='.$film_id);
		}
	}

	static function getFilmReviewsUrl($film_id)
	{
		if ($film_id>0)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}



		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'film-reviews/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film-reviews/'.$urlstr.'-'.$film_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=film_reviews&film_id='.$film_id);
		}
	}

	static function getPosterUrl($poster_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_poster&poster_id='.$poster_id);
	}

	static function getFilmDiscussUrl($film_id)
	{
		if ($film_id>0)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}



		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'film/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film/'.$urlstr.'-'.$film_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=film&film_id='.$film_id);
		}
	}

	static function getFilmImdbUrl($film_title)
	{
		return 'http://imdb.com/find?q='.urlencode($film_title);
	}

	static function getFilmFirstPoster($film_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT `id`, `image` FROM `#__main_films_posters`
			WHERE `film_id`=".intval($film_id)."
			ORDER BY `order_number` ASC
			LIMIT 0,1
		");
		$result=$_db->fetchAssoc($r);
		return $result;
	}

	static function getFilmDirectors($film_id)
	{
		global $_db, $_cfg;
		sys::useLib('main::persons');
		$r=$_db->query("
			SELECT
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`,
				flm_prs.person_id AS `id`
			FROM `#__main_films_persons` AS `flm_prs`

			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=flm_prs.person_id
			AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE flm_prs.film_id=".intval($film_id)."
			AND flm_prs.profession_id=1

			ORDER BY flm_prs.order_number
		");
		$result=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=main_persons::getPersonUrl($obj['id']);
			$result[]=$obj;
		}
		return $result;
	}


	static function getFilmDirectorsIdsRu($film_id)
	{
		global $_db, $_cfg;
		sys::useLib('main::persons');
		$r=$_db->query("
			SELECT
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`,
				flm_prs.person_id AS `id`
			FROM `#__main_films_persons` AS `flm_prs`

			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=flm_prs.person_id
			AND prs_lng.lang_id=1

			WHERE flm_prs.film_id=".intval($film_id)."
			AND flm_prs.profession_id=1

			ORDER BY flm_prs.order_number
		");
		$result=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$result[]=$obj['fio'];
		}
		return $result;
	}

	static function getFilmDirectorsIdsUa($film_id)
	{
		global $_db, $_cfg;
		sys::useLib('main::persons');
		$r=$_db->query("
			SELECT
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`,
				flm_prs.person_id AS `id`
			FROM `#__main_films_persons` AS `flm_prs`

			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=flm_prs.person_id
			AND prs_lng.lang_id=3

			WHERE flm_prs.film_id=".intval($film_id)."
			AND flm_prs.profession_id=1

			ORDER BY flm_prs.order_number
		");
		$result=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$result[]=$obj['fio'];
		}
		return $result;
	}

	static function getFilmActors($film_id)
	{
		global $_db, $_cfg;
		sys::useLib('main::persons');
		$r=$_db->query("
			SELECT
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`,
				flm_prs_lng.role,
				flm_prs.person_id AS `id`
			FROM `#__main_films_persons` AS `flm_prs`

			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=flm_prs.person_id
			AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_films_persons_lng` AS `flm_prs_lng`
			ON flm_prs_lng.record_id=flm_prs.id
			AND flm_prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE flm_prs.film_id=".intval($film_id)."
			AND flm_prs.profession_id=2

			ORDER BY flm_prs.order_number LIMIT 10
		");
		$result=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=main_persons::getPersonUrl($obj['id']);
			$result[]=$obj;
		}
		return $result;
	}

	static function getFilmActorsIdsRu($film_id)
	{
		global $_db, $_cfg;
		sys::useLib('main::persons');
		$r=$_db->query("
			SELECT
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`,
				flm_prs_lng.role,
				flm_prs.person_id AS `id`
			FROM `#__main_films_persons` AS `flm_prs`

			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=flm_prs.person_id
			AND prs_lng.lang_id=1

			LEFT JOIN `#__main_films_persons_lng` AS `flm_prs_lng`
			ON flm_prs_lng.record_id=flm_prs.id
			AND flm_prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE flm_prs.film_id=".intval($film_id)."
			AND flm_prs.profession_id=2

			ORDER BY flm_prs.order_number
		");
		$result=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$result[]=$obj['fio'];
		}
		return $result;
	}

	static function getFilmActorsIdsUa($film_id)
	{
		global $_db, $_cfg;
		sys::useLib('main::persons');
		$r=$_db->query("
			SELECT
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`,
				flm_prs_lng.role,
				flm_prs.person_id AS `id`
			FROM `#__main_films_persons` AS `flm_prs`

			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=flm_prs.person_id
			AND prs_lng.lang_id=3

			LEFT JOIN `#__main_films_persons_lng` AS `flm_prs_lng`
			ON flm_prs_lng.record_id=flm_prs.id
			AND flm_prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE flm_prs.film_id=".intval($film_id)."
			AND flm_prs.profession_id=2

			ORDER BY flm_prs.order_number
		");
		$result=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$result[]=$obj['fio'];
		}
		return $result;
	}

	static function getFilmLinks($film_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT lnk_lng.title, lnk.url
			FROM `#__main_films_links` AS `lnk`
			LEFT JOIN `#__main_films_links_lng` AS `lnk_lng`
			ON lnk_lng.record_id=lnk.id
			AND lnk_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE lnk.film_id=".intval($film_id)."
		");
		$result=array();
		while ($obj=$_db->fetchArray($r))
		{
			$result[]=$obj;
		}
		return $result;
	}


	static function getFilmShowsUrl($film_id)
	{
		if ($film_id>0)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}



		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'film-shows/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film-shows/'.$urlstr.'-'.$film_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=film_shows&film_id='.$film_id);
		}
	}

	static function getFilmNewsUrl($film_id)
	{
		if ($film_id>0)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}



		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'film-news/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film-news/'.$urlstr.'-'.$film_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=film_news&film_id='.$film_id);
		}
	}

	static function getFilmAnothersUrl($film_id)
	{
		if ($film_id>0)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}



		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'film-anothers/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film-anothers/'.$urlstr.'-'.$film_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=film_anothers&film_id='.$film_id);
		}
	}

	static function getFilmPersonsUrl($film_id)
	{
		if ($film_id>0)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}



		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'film-persons/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film-persons/'.$urlstr.'-'.$film_id.'.phtml';
		}
		} else {
			return sys::rewriteUrl('?mod=main&act=film_persons&film_id='.$film_id);
		}
	}

	static function getFilmSoundtracksUrl($film_id, $lang=false)
	{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT name, title_orig
			FROM `#__main_films`
			WHERE id='".$film_id."'
		");

		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}
        if (!$lang) $lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		return _ROOT_URL.'film_soundtracks/'.$urlstr.'-'.$film_id.'.phtml';
		} else {
		return _ROOT_URL.$lang.'/film_soundtracks/'.$urlstr.'-'.$film_id.'.phtml';
		}

		return sys::rewriteUrl('?mod=main&act=film_soundtracks&film_id='.$film_id);
	}

	static function showFilmTabs($film_id,$film_title,$active_tab=false)
	{
		$var['active']=$active_tab;
		$var['objects']['info']['title']=sys::translate('main::info');
		$var['objects']['info']['url']=self::getFilmUrl($film_id);
		$var['objects']['info']['alt']=sys::translate('main::info_about_film').' &quot;'.$film_title.'&quot;';

		if(self::isFilmShows($film_id))
		{
			$var['objects']['shows']['title']=sys::translate('main::shows');
			$var['objects']['shows']['url']=self::getFilmShowsUrl($film_id);
			$var['objects']['shows']['alt']=sys::translate('main::shows_schedule_on_film').' &quot;'.$film_title.'&quot;';
			// если есть в продаже билеты в текущем городе
			if(self::isFilmBuy($film_id))
			{
				$var['objects']['shows']['buy_ticket']=true;
				$var['objects']['shows']['icon']=_MODS_URL."main/skins/default/images/ticket.png";
				$var['objects']['shows']['icon_title']=sys::translate('main::buy_ticket');
				$var['objects']['shows']['icon_href']=$var['objects']['shows']['url'];
			}
        }

		if(self::isFilmReviews($film_id))
		{
			$var['objects']['reviews']['title']=sys::translate('main::reviews');
			$var['objects']['reviews']['url']=self::getFilmReviewsUrl($film_id);
			$var['objects']['reviews']['alt']=sys::translate('main::reviews_on_film').' &quot;'.$film_title.'&quot;';
		}

		if(self::isFilmNews($film_id))
		{
			$var['objects']['news']['title']=sys::translate('main::news');
			$var['objects']['news']['url']=self::getFilmNewsUrl($film_id);
			$var['objects']['news']['alt']=sys::translate('main::news_about_film').' &quot;'.$film_title.'&quot;';
		}

		if(self::isFilmAnotherArticles($film_id))
		{
			$var['objects']['anothers']['title']=sys::translate('main::another_texts');
			$var['objects']['anothers']['url']=self::getFilmAnothersUrl($film_id); //!!!!!!!!!
			$var['objects']['anothers']['alt']=sys::translate('main::another_texts').' &quot;'.$film_title.'&quot;';
		}

		if(self::isFilmPersons($film_id))
		{
			$var['objects']['titres']['title']=sys::translate('main::titres');
			$var['objects']['titres']['url']=self::getFilmPersonsUrl($film_id);
			$var['objects']['titres']['alt']=sys::translate('main::titres_from_film').' &quot;'.$film_title.'&quot;';
		}

		if(self::isFilmPhotos($film_id))
		{
			$var['objects']['frames']['title']=sys::translate('main::frames');
			$var['objects']['frames']['url']=self::getFilmPhotosUrl($film_id);
			$var['objects']['frames']['alt']=sys::translate('main::frames_from_film').' &quot;'.$film_title.'&quot;';
		}

		if(self::isFilmWallpapers($film_id))
		{
			$var['objects']['wallpapers']['title']=sys::translate('main::wallpapers');
			$var['objects']['wallpapers']['url']=self::getFilmWallpapersUrl($film_id);
			$var['objects']['wallpapers']['alt']=sys::translate('main::wallpapers_on_film').' &quot;'.$film_title.'&quot;';
		}

		if(self::isFilmPosters($film_id))
		{
			$var['objects']['posters']['title']=sys::translate('main::posters');
			$var['objects']['posters']['url']=self::getFilmPostersUrl($film_id);
			$var['objects']['posters']['alt']=sys::translate('main::posters_for_film').' &quot;'.$film_title.'&quot;';
		}

		if(self::isFilmTrailers($film_id))
		{
			$var['objects']['trailers']['title']=sys::translate('main::trailers');
			$var['objects']['trailers']['url']=self::getFilmTrailersUrl($film_id);
			$var['objects']['trailers']['alt']=sys::translate('main::trailers_film').' &quot;'.$film_title.'&quot;';
		}

		if(self::isFilmSountracks($film_id))
		{
			$var['objects']['soundtracks']['title']=sys::translate('main::soundtracks');
			$var['objects']['soundtracks']['url']=self::getFilmSoundtracksUrl($film_id);
			$var['objects']['soundtracks']['alt']=sys::translate('main::soundtracks_from_film').' &quot;'.$film_title.'&quot;';
		}

		if(self::isFilmAwards($film_id))
		{
			$var['objects']['awards']['title']=sys::translate('main::awards');
			$var['objects']['awards']['url']=self::getFilmAwardsUrl($film_id);
			$var['objects']['awards']['alt']=sys::translate('main::awards_film').' &quot;'.$film_title.'&quot;';
		}

		if(self::isFilmRating($film_id))
		{
			$var['objects']['rating']['title']=sys::translate('main::rating');
			$var['objects']['rating']['url']=self::getFilmRatingUrl($film_id);
			$var['objects']['rating']['alt']=sys::translate('main::rating').' &quot;'.$film_title.'&quot;';
		}
		
		if(self::isFilmSeries($film_id))
		{
			$var['objects']['series']['title']=sys::translate('main::series');
			$var['objects']['series']['url']=self::getFilmSeriesUrl($film_id);
			$var['objects']['series']['alt']=sys::translate('main::series_film').' &quot;'.$film_title.'&quot;';
		}

		if(self::isFilmOnline($film_id))
		{
			$var['objects']['online']['title']=sys::translate('main::online');
			$var['objects']['online']['url']=self::getFilmOnlineUrl($film_id);
			$var['objects']['online']['alt']=sys::translate('main::online_film').' &quot;'.$film_title.'&quot;';
		}
		
		return sys::parseTpl('main::tabs',$var);
	}

	static function getFilmAwardsUrl($film_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_awards&film_id='.$film_id);
	}

	static function getFilmOnlineUrl($film_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_online&film_id='.$film_id);
	}

	static function isFilmAwards($film_id)
	{
		global $_db;
		$r=$_db->query("SELECT `film_id`
			FROM `#__main_films_awards`
			WHERE `film_id`=".intval($film_id)."
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isFilmOnline($film_id)
	{
		global $_db;
		$r=$_db->query("SELECT `flm`.`id`
			FROM `#__main_films` AS `flm`
			
			LEFT JOIN `partner_megogo_objects` AS `prt`	ON prt.id=flm.megogo_id 
			
			WHERE `flm`.`id`=".intval($film_id)." AND `flm`.`megogo_id` IS NOT NULL AND `flm`.`megogo_id` != '' AND prt.vod in ('advod','fvod')
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isFilmRating($film_id)
	{
		global $_db;
		$r=$_db->query("SELECT id
			FROM `#__main_films`
			WHERE `id`=".intval($film_id)." and (sum+pre_sum)>0");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isFilmBuy($film_id)
	{
		global $_db, $_cfg;
		$q = "SELECT cnm.* 
			FROM `#__main_cinemas` as cnm
			LEFT JOIN `#__main_cinemas_halls` AS hls ON hls.cinema_id=cnm.id			
			LEFT JOIN `#__main_shows` AS shw ON shw.hall_id=hls.id 						
			WHERE 
				cnm.ticket_url='vkino' 		
				AND cnm.city_id=".intval($_cfg['main::city_id'])."
				AND shw.film_id=".intval($film_id)."					
				AND (shw.begin<=curdate() AND shw.end>=curdate())
			LIMIT 0,1
		";
		$r=$_db->query($q);
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function trackingFilm($film_id)
	{
		global $_db, $_user;
		if(!sys::checkAccess('main::films_tracking'))
			return false;

		$_db->query("
			INSERT INTO `#__main_users_films_subscribe`
			(`film_id`,`user_id`,`date`)
			VALUES
			(".intval($film_id).",".intval($_user['id']).",'".gmdate('Y-m-d H:i:s')."')
		");
	}

	static function untrackingFilm($film_id)
	{
		global $_db, $_user;
		if(!sys::checkAccess('main::films_tracking'))
			return false;

		$_db->query("
			DELETE FROM `#__main_users_films_subscribe`
			WHERE `film_id`=".intval($film_id)."
			AND `user_id`=".intval($_user['id'])."
		");

	}

	static function markFilm($film_id, $mark, $user_id)
	{

		global $_db, $_cfg;

		if(intval($mark)<1 || intval($mark)>10)
			return false;

		$r=$_db->query("
			SELECT `user_id`,`mark`
			FROM `#__main_films_rating`
			WHERE `user_id`=".intval($user_id)."
			AND `film_id`=".intval($film_id)."
		");
		$vote=$_db->fetchAssoc($r);




		$r=$_db->query("
			SELECT
				`sum` ,
				`rating`,
				`votes`
			FROM `#__main_films`
			WHERE `id`=".intval($film_id)."
		");
		$film=$_db->fetchAssoc($r);


		if($vote)
		{
			if ($film['votes']=='0')
			{
				$film['votes'] = 1;
				$film['sum'] = $mark;
			} else {
				$film['sum']=$film['sum']-$vote['mark']+$mark;
			}
		}
		else
		{
			if ($film['votes']=='0')
			{
				$film['votes'] = 1;
				$film['sum'] = $mark;
			} else {
				$film['votes']=$film['votes']+1;
				$film['sum']=$film['sum']+$mark;
			}
		}
		$film['rating']=($film['sum']/$film['votes']);


		if ($film['rating']>10)
		{
			$film['votes'] = $film['votes']+1;
			$film['rating']=($film['sum']/$film['votes']);
		}

		$_db->begin();

		if($vote)
		{
			if(!$_db->query("
				UPDATE `#__main_films_rating`
				SET `mark`='".intval($mark)."'
				WHERE `user_id`='".intval($user_id)."'
				AND `film_id`='".intval($film_id)."'
			"))
				return $_db->rollback();
		}
		else
		{
			if(!$_db->query("
				INSERT INTO `#__main_films_rating`
				(
					`user_id`,
					`film_id`,
					`mark`
				)
				VALUES
				(
					".intval($user_id).",
					".intval($film_id).",
					".intval($mark)."
				)
			"))
				return $_db->rollback;
		}

		if(!$_db->query("
			UPDATE `#__main_films`
			SET
				`sum`='".$film['sum']."',
				`rating`='".$film['rating']."',
				`votes`=".$film['votes']."
			WHERE `id`='".$film_id."'
		"))
			return $_db->rollback();
		$_db->commit();
		return array('rating'=>self::formatFilmRating($film['rating']), 'votes'=>$film['votes']);

	}

	static function preMarkFilm($film_id, $pre_mark, $user_id)
	{
		global $_db, $_cfg;
		if(intval($pre_mark)<1 || intval($pre_mark)>10)
			return false;
		$r=$_db->query("
			SELECT `user_id`,`pre_mark`
			FROM `#__main_films_rating`
			WHERE `user_id`=".intval($user_id)."
			AND `film_id`=".intval($film_id)."
		");
		$vote=$_db->fetchAssoc($r);

		$r=$_db->query("
			SELECT
				`pre_sum` ,
				`pre_rating`,
				`pre_votes`
			FROM `#__main_films`
			WHERE `id`=".intval($film_id)."
		");
		$film=$_db->fetchAssoc($r);


		if($vote)
			$film['pre_sum']=$film['pre_sum']-$vote['pre_mark']+$pre_mark;
		else
		{
			$film['pre_votes']=$film['pre_votes']+1;
			$film['pre_sum']=$film['pre_sum']+$pre_mark;
		}
		$film['pre_rating']=$film['pre_sum']/$film['pre_votes'];

		$_db->begin();

		if($vote)
		{
			if(!$_db->query("
				UPDATE `#__main_films_rating`
				SET `pre_mark`='".intval($pre_mark)."'
				WHERE `user_id`='".intval($user_id)."'
				AND `film_id`='".intval($film_id)."'
			"))
				return $_db->rollback();
		}
		else
		{
			if(!$_db->query("
				INSERT INTO `#__main_films_rating`
				(
					`user_id`,
					`film_id`,
					`pre_mark`
				)
				VALUES
				(
					'".intval($user_id)."',
					'".intval($film_id)."',
					'".intval($pre_mark)."'
				)
			"))
				return $_db->rollback();
		}

		if(!$_db->query("
			UPDATE `#__main_films`
			SET
				`pre_sum`='".$film['pre_sum']."',
				`pre_rating`='".$film['pre_rating']."',
				`pre_votes`='".$film['pre_votes']."'
			WHERE `id`=".$film_id."
		"))
			return $_db->rollback();
		$_db->commit();

			$rat="
				SELECT
					pre_mark
				FROM `#__main_films_rating`

				WHERE
					film_id = '".$film_id."'
			";

			$b=$_db->query($rat);
			$result['ratings']=array();

			$yes = 0;
			$no = 0;
			$total = 0;
			while($obj=$_db->fetchAssoc($b))
			{
				if ($obj['pre_mark']>6)
				{
					$yes++;
				} else {
					$no++;
				}

				$total++;
			}

			if ($total>0)
			{
				$var['yes'] = round($yes/$total*100);
				$var['no'] = 100 - $var['yes'];

				if ($var['yes']==0)
				{
					$var['yes'] = '';
					$var['yes1'] = 0;
				}
				else
				{
					$var['yes1'] = $var['yes'];
					$var['yes'] = $var['yes'].'%';
				}

				if ($var['no']==0)
				{
					$var['no'] = '';
					$var['no1'] = 0;
				} else {
					$var['no1'] = $var['no'];
					$var['no'] = $var['no'].'%';
				}

				$waiting = "
					<div id='waiting_yes' style='width: ".$var['yes1']."%;'>".$var['yes']."</div>
					<div id='waiting_no' style='width: ".$var['no1']."%;'>".$var['no']."</div>
				";

			}


		return array('rating'=>self::formatFilmRating($film['pre_rating']), 'votes'=>$film['pre_votes'], 'waiting'=>$waiting);

	}

	static function proMarkFilm($film_id, $mark, $review_id=false)
	{
		global $_db, $_cfg;

		if(intval($mark)<1 || intval($mark)>10)
			return false;

		$old_mark=false;
		if($review_id)
		{
			$r=$_db->query("
				SELECT `mark`
				FROM `#__main_reviews`
				WHERE `id`=".intval($review_id)."
			");
			list($old_mark)=$_db->fetchArray($r);
		}

		$r=$_db->query("
			SELECT
				`pro_sum` AS `sum`,
				`pro_rating` AS `rating`,
				`pro_votes` AS `votes`
			FROM `#__main_films`
			WHERE `id`=".intval($film_id)."
		");
		$film=$_db->fetchAssoc($r);


		if($old_mark)
			$film['sum']=$film['sum']-$old_mark+$mark;
		else
		{
			$film['votes']=$film['votes']+1;
			$film['sum']=$film['sum']+$mark;
		}
		$film['rating']=($film['sum']/$film['votes']);


		$_db->query("
			UPDATE `#__main_films`
			SET
				`pro_sum`='".$film['sum']."',
				`pro_rating`='".$film['rating']."',
				`pro_votes`=".$film['votes']."
			WHERE `id`='".$film_id."'
		");
		return $film;

	}
	/*
	static function showFilmRatingForm($film_id)
	{
		global $_user, $_db, $_cfg;


		$p_date=$_db->getValue('main_films','ukraine_premiere',intval($film_id));
		$p_date=sys::db2Timestamp($p_date);
		if($p_date>time())
			$pre=true;
		else
			$pre=false;

		if(isset($_POST['cmd_mark']))
		{
			if($_user['id']==2)
				sys::redirect($_SERVER['REQUEST_URI'],false);
			if($pre)
				self::preMarkFilm($film_id,$_POST['mark'],$_user['id']);
			else
				self::markFilm($film_id,$_POST['mark'],$_user['id']);
			sys::redirect($_SERVER['REQUEST_URI'],false);
		}

		if($pre)
		{
			$r=$_db->query("
				SELECT `pre_mark` FROM `#__main_films_rating`
				WHERE `user_id`=".intval($_user['id'])."
				AND `film_id`=".intval($film_id)."
			");
			list($var['mark'])=$_db->fetchArray($r);
		}
		else
		{
			$r=$_db->query("
				SELECT `mark` FROM `#__main_films_rating`
				WHERE `user_id`=".intval($_user['id'])."
				AND `film_id`=".intval($film_id)."
			");
			list($var['mark'])=$_db->fetchArray($r);
		}
		if($var['mark'])
			$var['btn_name']=sys::translate('main::change_mark');
		else
			$var['btn_name']=sys::translate('main::to_mark');


		return sys::parseTpl('main::mark_form',$var);
	}
	*/
/* //comment by Mike
	static function showFilmRatingForm($film_id)
	{
		global $_user, $_db, $_cfg;

		$r=$_db->query("
			SELECT
				flm.id,
				flm_lng.title,
				flm.rating,
				flm.pre_rating,
				flm.votes,
				flm.pre_votes,
				flm.pro_rating,
				flm.pro_votes,
				flm.pro_sum,
				flm.ukraine_premiere,
				flm.world_premiere
			FROM
				`#__main_films` AS `flm`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id='".intval($film_id)."'
			AND flm_lng.lang_id='".intval(intval($_cfg['sys::lang_id']))."'
			WHERE flm.id='".intval($film_id)."'
		");

		$film=$_db->fetchAssoc($r);




	$rat="
		SELECT
			pre_mark
		FROM `#__main_films_rating`

		WHERE
			film_id = '".intval($film_id)."'
	";

	$b=$_db->query($rat);
	$result['ratings']=array();

	$yes = 0;
	$no = 0;
	$total = 0;
	while($obj=$_db->fetchAssoc($b))
	{
		if ($obj['pre_mark']>6)
		{
			$yes++;
		} else {
			$no++;
		}

		$total++;
	}

	if ($total>0)
	{
		$var['pre'] = 1;
		$var['yes'] = round($yes/$total*100);
		$var['no'] = 100 - $var['yes'];
	} else {
		$var['pre'] = 1;
		$var['yes'] = 0;
		$var['no'] = 100;
	}




		if ($film['ukraine_premiere'])
		{
			$p_date=$film['ukraine_premiere'];
		} else {
			$p_date=$film['world_premiere'];
		}


		$p_date=strtotime($p_date);



		$rww=$_db->query("
			SELECT
				id
			FROM
				`#__main_shows`
			WHERE film_id='".intval($film_id)."'
			AND begin<='".date('Y-m-d', time())."'
			LIMIT 1
		");

		$showsalready=$_db->fetchAssoc($rww);


		if($p_date<=time() || $showsalready['id']>0)
		{
			$pre=false;
			$showrat = 1;
		}
		else
		{
			$pre=true;
			$showrat = 0;
		}

		if($pre)
		{
			$var['rating']=$film['pre_rating'];
			$var['votes']=$film['pre_votes'];
		}
		else
		{

			if ($film['rating']>0)
			{
				$var['rating']=$film['rating'];
				$var['votes']=$film['votes'];
			} else {

				$rwwx=$_db->query("
					UPDATE
						`#__main_films`
					SET rating = '".$film['pro_rating']."',
					sum = '".$film['pro_sum']."',
					votes = '".$film['pro_votes']."'
					WHERE id='".intval($film_id)."'
				");



				$var['rating']=$film['pro_rating'];
				$var['votes']=$film['pro_votes'];
			}
		}

		if($pre)
		{
			$r=$_db->query("
				SELECT `pre_mark` FROM `#__main_films_rating`
				WHERE `user_id`=".intval($_user['id'])."
				AND `film_id`=".intval($film_id)."
			");
			list($var['mark'])=$_db->fetchArray($r);
		}
		else
		{
			$r=$_db->query("
				SELECT `mark` FROM `#__main_films_rating`
				WHERE `user_id`=".intval($_user['id'])."
				AND `film_id`=".intval($film_id)."
			");
			list($var['mark'])=$_db->fetchArray($r);
		}
		$var['rest']=$var['rating']-floor($var['rating']);
		$var['object_id']=$film['id'];
		$var['rating_url']=self::getFilmRatingUrl($film['id']);
		$var['rating']=self::formatFilmRating($var['rating']);
		$var['preVotes']=$film['pre_votes'];
		$var['type']='film';

		if ($showrat==1)
			return sys::parseTpl('main::mark_form',$var);
		else
			return sys::parseTpl('main::mark_form_pre',$var);
	}
*/
	static function showFilmRatingForm2($film_id)
	{
		global $_user, $_db, $_cfg;

		$r=$_db->query("
			SELECT
				flm.id,
				flm_lng.title,
				flm.rating,
				flm.pre_rating,
				flm.votes,
				flm.pre_votes,
				flm.ukraine_premiere
			FROM
				`#__main_films` AS `flm`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id='".intval($film_id)."'
			AND flm_lng.lang_id='".intval(intval($_cfg['sys::lang_id']))."'
			WHERE flm.id='".intval($film_id)."'
		");

		$film=$_db->fetchAssoc($r);




	$rat="
		SELECT
			pre_mark
		FROM `#__main_films_rating`

		WHERE
			film_id = '".intval($film_id)."'
	";

	$b=$_db->query($rat);
	$result['ratings']=array();

	$yes = 0;
	$no = 0;
	$total = 0;
	while($obj=$_db->fetchAssoc($b))
	{
		if ($obj['pre_mark']>6)
		{
			$yes++;
		} else {
			$no++;
		}

		$total++;
	}

	if ($total>0)
	{
		$var['pre'] = 1;
		$var['yes'] = round($yes/$total*100);
		$var['no'] = 100 - $var['yes'];
	} else {
		$var['pre'] = 1;
		$var['yes'] = 0;
		$var['no'] = 100;
	}




		$p_date=$film['ukraine_premiere'];


		$p_date=strtotime($p_date);



		if($p_date>time())
		{
			$pre=true;
			$showrat = 0;
		}
		else
		{
			$pre=false;
			$showrat = 1;
		}

		if($pre)
		{
			$var['rating']=$film['pre_rating'];
			$var['votes']=$film['pre_votes'];
		}
		else
		{
			$var['rating']=$film['rating'];
			$var['votes']=$film['votes'];
		}

		if($pre)
		{
			$r=$_db->query("
				SELECT `pre_mark` FROM `#__main_films_rating`
				WHERE `user_id`=".intval($_user['id'])."
				AND `film_id`=".intval($film_id)."
			");
			list($var['mark'])=$_db->fetchArray($r);
		}
		else
		{
			$r=$_db->query("
				SELECT `mark` FROM `#__main_films_rating`
				WHERE `user_id`=".intval($_user['id'])."
				AND `film_id`=".intval($film_id)."
			");
			list($var['mark'])=$_db->fetchArray($r);
		}
		$var['rest']=$var['rating']-floor($var['rating']);
		$var['object_id']=$film['id'];
		$var['id'] = $film['id'];
		$var['rating_url']=self::getFilmRatingUrl($film['id']);
		$var['rating']=self::formatFilmRating($var['rating']);
		$var['preVotes']=$film['pre_votes'];
		$var['type']='film';

//		if ($showrat==1)
//			return sys::parseTpl('main::mark_form2',$var);
//		else
			return sys::parseTpl('main::mark_form_pre',$var);
	}

	static function formatFilmRating($rating)
	{
		if($rating)
			return number_format($rating,2,'.',' ');
		else
			return 0;
	}

	static function getFilmTextUrl($film_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_details&film_id='.$film_id);
	}

	static function showFilmMedia($film_id, $film_title)
	{
		global $_db, $_cfg;
		$var['title']=$film_title;
		$r=$_db->query("
			SELECT `id`,`image` FROM `#__main_films_trailers`
			WHERE `film_id`=".intval($film_id)."
			AND `public`=1
			ORDER BY RAND()
		");
		$var['trailer']=$_db->fetchAssoc($r);
		if($var['trailer'])
		{
			$var['trailer']['alt']=$film_title.'::'.sys::translate('main::trailers');
			$var['trailer']['url']=self::getTrailersUrl($var['trailer']['id']);
			if($var['trailer']['image'])
				$var['trailer']['image']=$_cfg['main::films_url_new'].$var['trailer']['image'].'&w=214';
			else
				$var['trailer']['image']=_IMG_URL.'player.gif';
		}


		$r=$_db->query("
			SELECT `id`,`image` FROM `#__main_films_photos`
			WHERE `film_id`=".intval($film_id)."
			AND `public`=1
			ORDER BY RAND()
		");
		$var['photo']=$_db->fetchAssoc($r);
		if($var['photo'])
		{
			$var['photo']['alt']=$film_title.'::'.sys::translate('main::frames');
			$var['photo']['url']=self::getPhotosUrl($var['photo']['id']);
			$var['photo']['image']=$_cfg['main::films_url_new'].$var['photo']['image'].'&w=214';
		}

		$r=$_db->query("
			SELECT `id`,`image` FROM `#__main_films_wallpapers`
			WHERE `film_id`=".intval($film_id)."
			AND `public`=1
			ORDER BY RAND()
		");
		$var['wallpaper']=$_db->fetchAssoc($r);
		if($var['wallpaper'])
		{
			$var['wallpaper']['alt']=$film_title.'::'.sys::translate('main::wallpapers');
			$var['wallpaper']['url']=self::getWallpapersUrl($var['wallpaper']['id']);
			$var['wallpaper']['image']=$_cfg['main::films_url_new'].$var['wallpaper']['image'].'&w=214';
		}
		return sys::parseTpl('main::media',$var);

	}

	static function getTrailerUrl_new($trailer_id, $lang=false, $uri_part="film_trailers")
	{
			global $_db;
			$trailer_id = doubleval($trailer_id);
			$q="
				SELECT flm.name, flm.id
				
				FROM `#__main_films_trailers` AS `trl`
				
				LEFT JOIN `#__main_films` AS `flm`
				ON flm.id=trl.film_id
		
				WHERE trl.id=$trailer_id";

				$r=$_db->query($q);
				$obj=$_db->fetchAssoc($r);
			
				return sys::getHumanUrl($obj['id'],$obj['name'],$uri_part,$lang)."?trailer_id=".$trailer_id;
	}

	static function getTrailersUrl($trailer_id, $lang=false, $uri_part="last_trailers")
	{
			global $_db;
			$q="
				SELECT flm.name
				
				FROM `#__main_films_trailers` AS `trl`
				
				LEFT JOIN `#__main_films` AS `flm`
				ON flm.id=trl.film_id
		
				WHERE trl.id=$trailer_id";

				$r=$_db->query($q);
				$obj=$_db->fetchAssoc($r);
			
				return sys::getHumanUrl($trailer_id,$obj['name'],$uri_part,$lang);
	}

	static function getFilmTitleByTrailerID($trailer_id, $lang=1)
	{
        global $_db;
		$r=$_db->query("
			SELECT `mft`.`film_id`, `mfl`.`title`
            FROM `#__main_films_trailers` AS `mft`
            LEFT JOIN `#__main_films_lng` AS `mfl` ON (`mfl`.`record_id`=`mft`.`film_id` AND `mfl`.`lang_id`=".$lang.")
			WHERE `mft`.`id`=".intval($trailer_id)."
			AND `mft`.`public`=1
		");
        $res=$_db->fetchAssoc($r);
		return $res['title'];
	}

	static function getFilmByReviewID($review_id, $lang=1)
	{
        global $_db;
		$r=$_db->query("
			SELECT `r`.`film_id`, `mfl`.`title`
            FROM `#__main_reviews` AS `r`
            LEFT JOIN `#__main_films_lng` AS `mfl` ON (`mfl`.`record_id`=`r`.`film_id` AND `mfl`.`lang_id`=".$lang.")
			WHERE `r`.`id`=".intval($review_id)."
			AND `r`.`public`=1
		");
        $res=$_db->fetchAssoc($r);
		return $res['title'];
	}

	static function getFilmTitleByReviewID($id)
	{
        global $_db;
		$r=$_db->query("
			SELECT `mr`.`film_id` FROM `#__main_reviews` AS `mr`
			WHERE `mr`.`id`=".intval($id)."
			AND `mr`.`public`=1
		");
        $res=$_db->fetchAssoc($r);
        $name=$_db->getValue('main_films','title',(int)$res['film_id'],true);
		return $name;
	}

	static function getTrailerUrl($trailer_id, $lang="ru", $uri_part="film_trailers")
	{
		return self::getTrailerUrl_new($trailer_id,$lang,$uri_part); 
		//return 'http://kino-teatr.ua/ru/main/film_trailers/trailer_id/'.$trailer_id.'.phtml';
	}

	static function getTrailerUkUrl($trailer_id, $lang="uk", $uri_part="film_trailers")
	{
		return self::getTrailerUrl_new($trailer_id,$lang,$uri_part);
		//return 'http://kino-teatr.ua/uk/main/film_trailers/trailer_id/'.$trailer_id.'.phtml';
	}

	static function getPhotosUrl($photo_id, $rewrite=true)
	{
		$url='?mod=main&act=film_photos&photo_id='.$photo_id;
		if($rewrite)
			return sys::rewriteUrl($url);
		else
			return $url;
	}

	static function getSoundtracksUrl($soundtrack_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_soundtracks&soundtrack_id='.$soundtrack_id);
	}

	static function getPostersUrl($poster_id,$rewrite=false)
	{
		$url='?mod=main&act=film_posters&poster_id='.$poster_id;
		if($rewrite)
			return sys::rewriteUrl($url);
		else
			return $url;
	}


	static function getWallPapersUrl($wallpaper_id, $rewrite=true)
	{
		$url='?mod=main&act=film_wallpapers&wallpaper_id='.$wallpaper_id;
		if($rewrite)
			return sys::rewriteUrl($url);
		else
			return $url;
	}

	static function getWallpaperPersons($wallpaper_id)
	{
		sys::useLib('main::persons');
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT
				pht_prs.person_id,
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`
			FROM `#__main_films_wallpapers_persons` AS `pht_prs`
			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=pht_prs.person_id
			AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE pht_prs.wallpaper_id=".intval($wallpaper_id)."
		");
		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=main_persons::getPersonUrl($obj['person_id']);
			$result[]=$obj;
		}
		return $result;
	}

	static function getPhotoPersons($photo_id)
	{
		sys::useLib('main::persons');
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT
				pht_prs.person_id,
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`
			FROM `#__main_films_photos_persons` AS `pht_prs`
			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=pht_prs.person_id
			AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE pht_prs.photo_id=".intval($photo_id)."
		");
		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=main_persons::getPersonUrl($obj['person_id']);
			$result[]=$obj;
		}
		return $result;
	}

	static function getAwardPersons($award_id)
	{
		sys::useLib('main::persons');
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT
				awd_prs.person_id,
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`
				
			FROM `#__main_films_awards_persons` AS `awd_prs`
			
			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=awd_prs.person_id	AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			
			WHERE awd_prs.film_award_id=".intval($award_id)."
		");
		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=main_persons::getPersonUrl($obj['person_id']);
			$result[]=$obj;
		}
		return $result;
	}

	static function getTrailerPersons($trailer_id)
	{
		sys::useLib('main::persons');
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT
				awd_prs.person_id,
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `fio`
			FROM `#__main_films_trailers_persons` AS `awd_prs`
			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=awd_prs.person_id
			AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE awd_prs.trailer_id=".intval($trailer_id)."
		");
		$result=array();
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=main_persons::getPersonUrl($obj['person_id']);
			$result[]=$obj;
		}
		return $result;
	}

	static function getPrevPhotoId($film_id, $order_number)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_photos`
			WHERE `film_id`=".intval($film_id)."
			AND `order_number`>".intval($order_number)."
			AND `public`=1
			ORDER BY `order_number` ASC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getPrevTrailerId($film_id, $order_number)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_trailers`
			WHERE `film_id`=".intval($film_id)."
			AND `order_number`>".intval($order_number)."
			AND `public`=1
			ORDER BY `order_number` ASC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getNextPhotoId($film_id, $order_number)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_photos`
			WHERE `film_id`=".intval($film_id)."
			AND `order_number`<".intval($order_number)."
			AND `public`=1
			ORDER BY `order_number` DESC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getNextTrailerId($film_id, $order_number)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_trailers`
			WHERE `film_id`=".intval($film_id)."
			AND `order_number`<".intval($order_number)."
			AND `public`=1
			ORDER BY `order_number` DESC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getNextPosterId($film_id, $order_number)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_posters`
			WHERE `film_id`=".intval($film_id)."
			AND `order_number`>".intval($order_number)."
			AND `public`=1
			ORDER BY `order_number` DESC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getPrevPosterId($film_id, $order_number)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_posters`
			WHERE `film_id`=".intval($film_id)."
			AND `order_number`<".intval($order_number)."
			AND `public`=1
			ORDER BY `order_number` ASC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getPrevWallpaperId($film_id, $order_number)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_wallpapers`
			WHERE `film_id`=".intval($film_id)."
			AND `order_number`>".intval($order_number)."
			AND `public`=1
			ORDER BY `order_number` ASC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getNextWallpaperId($film_id, $order_number)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_films_wallpapers`
			WHERE `film_id`=".intval($film_id)."
			AND `order_number`<".intval($order_number)."
			AND `public`=1
			ORDER BY `order_number` DESC
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isFilmPhotos($film_id)
	{
		global $_db;
		$r=$_db->query("SELECT `film_id`
			FROM `#__main_films_photos`
			WHERE `film_id`=".intval($film_id)."
			AND `public`=1
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isFilmTrailers($film_id)
	{
		global $_db;
		$r=$_db->query("SELECT `film_id`
			FROM `#__main_films_trailers`
			WHERE `film_id`=".intval($film_id)."
			AND `public`=1
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}


	static function isFilmShows($film_id)
	{
		global $_db;		
		$r=$_db->query("SELECT `film_id`
			FROM `#__main_shows`
			WHERE `film_id`=".intval($film_id)."
			AND `end`>='".date('Y-m-d')."'
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isFilmSeries($film_id)
	{
		global $_db;		
		$r=$_db->query("SELECT `id`
			FROM `#__main_films`
			WHERE `id`=".intval($film_id)."
			AND `serial`=1
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isFilmWallpapers($film_id)
	{
		global $_db;
		$r=$_db->query("SELECT `film_id`
			FROM `#__main_films_wallpapers`
			WHERE `film_id`=".intval($film_id)."
			AND `public`=1
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isFilmReviews($film_id)
	{
		global $_db;
		$r=$_db->query("SELECT `film_id`
			FROM `#__main_reviews`
			WHERE `film_id`=".intval($film_id)."
			AND `public`=1 AND `date`<='".date('Y-m-d H:i:s')."'
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isFilmNews($film_id)
	{
		global $_db;
		$r=$_db->query("SELECT `film_id`
			FROM `#__main_news_articles_films`
			WHERE `film_id`=".intval($film_id)."
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isFilmAnotherArticles($film_id)
	{
		global $_db;
		
		//if(!sys::isDebugIP()) return false;
		
		$tables = array("articles", "gossip", "interview", "serials");
		
		foreach ($tables as $table) {
			$r=$_db->query("SELECT `film_id`
				FROM `#__main_".$table."_articles_films`
				WHERE `film_id`=".intval($film_id)."
				LIMIT 0,1
			");
			if($_db->fetchArray($r)) return true;
		}		
		return false;
	}

	static function isFilmPersons($film_id)
	{
		global $_db;
		$r=$_db->query("SELECT `film_id`
			FROM `#__main_films_persons`
			WHERE `film_id`=".intval($film_id)."
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isFilmPosters($film_id)
	{
		global $_db;
		$r=$_db->query("SELECT `film_id`
			FROM `#__main_films_posters`
			WHERE `film_id`=".intval($film_id)."
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function isFilmSountracks($film_id)
	{
		global $_db;
		$r=$_db->query("SELECT `film_id`
			FROM `#__main_films_soundtracks`
			WHERE `film_id`=".intval($film_id)."
			LIMIT 0,1
		");
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function getPhotoUrl($photo_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_photo&photo_id='.$photo_id);
	}

	static function getPhotoEditUrl($photo_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_photo_edit&photo_id='.$photo_id);
	}

	static function getPosterEditUrl($poster_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_poster_edit&photo_id='.$poster_id);
	}

	static function getSoundtrackEditUrl($soundtrack_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_soundtrack_edit&soundtrack_id='.$soundtrack_id);
	}

	static function getSoundtrackAddUrl($film_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_soundtrack_edit&film_id='.$film_id);
	}

	static function getTrailerEditUrl($trailer_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_trailer_edit&trailer_id='.$trailer_id);
	}

	static function getTrailerAddUrl($film_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_trailer_edit&film_id='.$film_id);
	}

	static function getWallpaperEditUrl($wallpaper_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_wallpaper_edit&wallpaper_id='.$wallpaper_id);
	}

	static function confirmPhoto($photo_id)
	{
		global $_db;
		$_db->setValue('main_films_photos','public',1,intval($photo_id));
	}

	static function confirmPoster($poster_id)
	{
		global $_db;
		$_db->setValue('main_films_posters','public',1,intval($poster_id));
	}

	static function confirmSoundtrack($soundtrack_id)
	{
		global $_db;
		$_db->setValue('main_films_soundtracks','public',1,intval($soundtrack_id));
	}

	static function confirmWallpaper($wallpaper_id)
	{
		global $_db;
		$_db->setValue('main_films_wallpapers','public',1,intval($wallpaper_id));
	}

	static function confirmTrailer($trailer_id)
	{
		global $_db;
		$_db->setValue('main_films_trailers','public',1,intval($trailer_id));
	}

	static function getPhotoAddUrl($film_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_photo_edit&film_id='.$film_id);
	}

	static function getPosterAddUrl($film_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_poster_edit&film_id='.$film_id);
	}

	static function getWallpaperAddUrl($film_id)
	{
		return sys::rewriteUrl('?mod=main&act=film_wallpaper_edit&film_id='.$film_id);
	}

	static function countFilmProRating($film_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT COUNT(*)
			FROM `#__main_reviews`
			WHERE `film_id`=".intval($film_id)."
		");
		list($votes)=$_db->fetchArray($r);

		$r=$_db->query("
			SELECT SUM(`mark`)
			FROM `#__main_reviews`
			WHERE `film_id`=".intval($film_id)."
		");
		list($sum)=$_db->fetchArray($r);

		if($votes)
			$rating=$sum/$votes;
		else
			$rating=0;

		$_db->query("
			UPDATE `#__main_films`
			SET
				`pro_votes`='".$votes."',
				`pro_sum`='".$sum."',
				`pro_rating`='".$rating."'
			WHERE
				`id`=".intval($film_id)."
		");
	}

	static function countFilmPreRating($film_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT COUNT(*)
			FROM `#__main_films_rating`
			WHERE `film_id`=".intval($film_id)."
			AND `pre_mark`>0
		");
		list($votes)=$_db->fetchArray($r);

		$r=$_db->query("
			SELECT SUM(`pre_mark`)
			FROM `#__main_films_rating`
			WHERE `film_id`=".intval($film_id)."
			AND `pre_mark`>0
		");
		list($sum)=$_db->fetchArray($r);

		if($votes)
			$rating=$sum/$votes;
		else
			$rating=0;

		$_db->query("
			UPDATE `#__main_films`
			SET
				`pre_votes`='".$votes."',
				`pre_sum`='".$sum."',
				`pre_rating`='".$rating."'
			WHERE
				`id`=".intval($film_id)."
		");
	}

	static function countFilmRating($film_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT COUNT(*)
			FROM `#__main_films_rating`
			WHERE `film_id`=".intval($film_id)."
			AND `mark`>0
		");
		list($votes)=$_db->fetchArray($r);

		$r=$_db->query("
			SELECT SUM(`mark`)
			FROM `#__main_films_rating`
			WHERE `film_id`=".intval($film_id)."
			AND `mark`>0
		");
		list($sum)=$_db->fetchArray($r);

		if($votes)
			$rating=$sum/$votes;
		else
			$rating=0;

		$_db->query("
			UPDATE `#__main_films`
			SET
				`votes`='".$votes."',
				`sum`='".$sum."',
				`rating`='".$rating."'
			WHERE
				`id`=".intval($film_id)."
		");
	}

	static function showCalendar($film_id, $days=35, $html_params=false)
	{
		global $_db, $_cfg;
		sys::filterGet('date');
		$var['html_params']=$html_params;
		$time=time();
		$today=date('w',$time);
		if($today==0)
			$today=7;

		$monday=$time-(($today-1)*_DAY);

		$today=date('d.m.Y',$time);
		$city=$_db->getValue('main_countries_cities','title_what',$_cfg['main::city_id'],true);

		for($i=0; $i<$days; $i++)
		{
			$date=date('d.m.Y',$monday+($i*_DAY));
			$day=intval(date('d',$monday+($i*_DAY)));

			if($date==$_GET['date'])
				$var['objects'][$date]['current']=true;
			else
				$var['objects'][$date]['current']=false;

			$var['objects'][$date]['day']=$day;
			if(($monday+($i*_DAY))>=$time)
				$var['objects'][$date]['url']=sys::rewriteUrl('?mod=main&act=film_shows&film_id='.$film_id.'&date='.$date);
			else
				$var['objects'][$date]['url']=false;
			$var['objects'][$date]['alt']='';
			$var['objects'][$date]['today']=($date==$today);

		}

		return sys::parseTpl('main::shows_calendar',$var);
	}

	static function showFilmsLinks2($dir_id)
	{

		global $_db, $_cfg;
		$var['objects']=array();

		$r=$_db->query("
			SELECT per.film_id, lng.title, flm.name, pst.image
			FROM `#__main_films_persons` AS `per`
			LEFT JOIN `#__main_films_lng` AS `lng` ON per.film_id=lng.record_id
			AND lng.lang_id=".$_cfg['sys::lang_id']."
			LEFT JOIN `#__main_films` AS `flm`
			ON per.film_id=flm.id
			LEFT JOIN `#__main_films_posters` AS `pst`
			ON pst.film_id=flm.id
			AND pst.order_number=1
			WHERE per.person_id = ".$dir_id." AND per.profession_id=1
			ORDER BY RAND() LIMIT 10
		");

		$i=1;

		while($obj=$_db->fetchAssoc($r))
		{

					if(self::isFilmTrailers($obj['film_id']))
					{
						$obj['trailers_url']=self::getFilmTrailersUrl($obj['film_id']);
						$obj['isTrailer']='1';
					}else{
						$obj['trailers_url']=false;
						$obj['isTrailer']='0';
					}

					if(self::isFilmShows($obj['film_id']))
					{
						$obj['shows_url']=self::getFilmShowsUrl($obj['film_id']);
						$obj['isShows']='1';
					}else{
						$obj['shows_url']=false;
						$obj['isShows']='0';
					}

					$obj['title']=htmlspecialchars($obj['title']);


				if($obj['image'])
				{
					$image=$_cfg['main::films_url'].$obj['image'];
				} else {
		         	$image = 'blank_news_img.jpg';
				}

				$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=25&4444444444444';


					if ($i%5==0)
					{
						$obj['class']='no-margin';
						if ($i!=10 )
						$obj['clear']=1;
					} else {
						$obj['clear']=0;
					}

				$obj['url'] = self::getFilmUrl($obj['film_id']);

//				$obj['url']=sys::rewriteUrl("?mod=main&act=film&film_id=".$obj['film_id']);
				$obj['alt']=sys::translate('main::film').' '.$obj['title'];
				$var['objects'][]=$obj;
				$i++;
		}




		$result=sys::parseTpl('main::films_links_2',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}


	static function showFilmsLinks4($film_id)
	{

		global $_db, $_cfg, $_user;
		$var['objects']=array();


		$r=$_db->query("
			SELECT flm.year, flm.rating
			FROM `#__main_films` as `flm`
			WHERE flm.id = ".$film_id."
			AND flm.ukraine_premiere <= '".date('Y-m-d')."'
		");


		while($obj=$_db->fetchAssoc($r))
		{
			$year = $obj['year'];
			$rating = $obj['rating'];
		}

		if ($year)
		{

			$genres = self::getFilmGenresIds($film_id);			
			$anim = 0;
			$is_erotic = false;

			foreach ($genres as $key=>$value)
			{
				if ($value != 9 && $value != 27) $genre[] = 'gnr.genre_id='.$value;
				if ($value == 16) $anim = 1;
				if ($value == 20) $is_erotic = true;
			}
			
			$genr = implode(' OR ', $genre);

			if ($anim)
			{
				$genr = 'gnr.genre_id=16';
			}

			$filter_erotic=($is_erotic)?"":"AND not 20 in (select `genre_id` from `grifix_main_films_genres` as `g` where `g`.`film_id`=`gnr`.`film_id`)";
						
			$countries = self::getFilmCountriesIds($film_id);

			foreach ($countries as $key=>$value)
			{
				$country[] = 'cnt.country_id='.$value;
			}

			$countr = implode(' OR ', $country);
			if($countr == "") $countr = "1";

			

			$q = "SELECT flm.id as `film_id`, lng.title, flm.name, pst.image
				FROM `#__main_films` as `flm`
				LEFT JOIN `#__main_films_genres` as `gnr`
				ON (gnr.film_id=flm.id	AND	(".(($genr!="")?$genr:1)."))
				LEFT JOIN `#__main_films_countries` as `cnt`
				ON (cnt.film_id=flm.id	AND	(".$countr."))
				LEFT JOIN `#__main_films_lng` AS `lng` ON flm.id=lng.record_id
				AND lng.lang_id=".$_cfg['sys::lang_id']."
				LEFT JOIN `#__main_films_posters` AS `pst`
				ON pst.film_id=flm.id
				AND pst.order_number=1
				WHERE flm.id != '".$film_id."'
				AND gnr.genre_id IS NOT NULL
				$filter_erotic
				AND cnt.country_id IS NOT NULL
				AND pst.image IS NOT NULL
				AND flm.year >= ".($year - 2)."
				AND flm.year <= ".($year + 2)."
				AND votes > 2
				AND flm.rating >= ".($rating - 1)."
				AND flm.rating <= ".($rating + 1)."
				ORDER BY flm.rating DESC
				LIMIT 10
			";		
			$r=$_db->query($q);
			$i=1;
			$filmss = array();
			while($obj=$_db->fetchAssoc($r))
			{
				if ($i<6)
				{
					if(self::isFilmTrailers($obj['film_id']))
						{
							$obj['trailers_url']=self::getFilmTrailersUrl($obj['film_id']);
							$obj['isTrailer']='1';
						}else{
							$obj['trailers_url']=false;
							$obj['isTrailer']='0';
						}

						if(self::isFilmShows($obj['film_id']))
						{
							$obj['shows_url']=self::getFilmShowsUrl($obj['film_id']);
							$obj['isShows']='1';
						}else{
							$obj['shows_url']=false;
							$obj['isShows']='0';
						}

						$obj['title']=htmlspecialchars($obj['title']);


		// inserted by Mike begin
  	    $image="x2_".$obj['image'];	
		if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$obj['image'] = $image;
		// inserted by Mike end


						if ($i%5==0)
						{
							$obj['class']='no-margin';
							if ($i!=10 )
							$obj['clear']=1;
						} else {
							$obj['clear']=0;
						}

					$obj['url'] = self::getFilmUrl($obj['film_id']);

					$obj['alt']=sys::translate('main::film').' '.$obj['title'];

					$ro=$_db->query("SELECT count(*) as count
						FROM `#__main_films_genres`
						WHERE genre_id = 16
						AND film_id = ".$obj['film_id']."
					");

					while($objo=$_db->fetchAssoc($ro))
					{
						$ann = $objo['count'];
					}


					if ($anim)
					{
						if (!$filmss[$obj['film_id']])
						{
							$var['objects'][]=$obj;
							$i++;
							$filmss[$obj['film_id']] = $obj['film_id'];
						}
					} else {
						if (!$ann)
						{
							if (!$filmss[$obj['film_id']])
							{
								$var['objects'][]=$obj;
								$i++;
								$filmss[$obj['film_id']] = $obj['film_id'];
							}
						}
					}
				}
			}


			if ($i==1)
			{
			$q = "SELECT flm.id as `film_id`, lng.title, flm.name, pst.image
				FROM `#__main_films` as `flm`
				LEFT JOIN `#__main_films_genres` as `gnr`
				ON (gnr.film_id=flm.id	AND	(".(($genr!="")?$genr:1)."))
				LEFT JOIN `#__main_films_countries` as `cnt`
				ON (cnt.film_id=flm.id	AND	(".$countr."))
				LEFT JOIN `#__main_films_lng` AS `lng` ON flm.id=lng.record_id
				AND lng.lang_id=".$_cfg['sys::lang_id']."
				LEFT JOIN `#__main_films_posters` AS `pst`
				ON pst.film_id=flm.id
				AND pst.order_number=1
				WHERE flm.id != '".$film_id."'
				AND gnr.genre_id IS NOT NULL
				$filter_erotic
				AND cnt.country_id IS NOT NULL
				AND pst.image IS NOT NULL
				AND flm.year >= ".($year - 2)."
				AND flm.year <= ".($year + 2)."
				AND votes > 2
				ORDER BY flm.rating DESC
				LIMIT 10
			";
			$r=$_db->query($q);

			$i=1;
			$filmss = array();
			while($obj=$_db->fetchAssoc($r))
			{
				if ($i<6)
				{
					if(self::isFilmTrailers($obj['film_id']))
						{
							$obj['trailers_url']=self::getFilmTrailersUrl($obj['film_id']);
							$obj['isTrailer']='1';
						}else{
							$obj['trailers_url']=false;
							$obj['isTrailer']='0';
						}

						if(self::isFilmShows($obj['film_id']))
						{
							$obj['shows_url']=self::getFilmShowsUrl($obj['film_id']);
							$obj['isShows']='1';
						}else{
							$obj['shows_url']=false;
							$obj['isShows']='0';
						}

						$obj['title']=htmlspecialchars($obj['title']);

		// inserted by Mike begin
  	    $image="x2_".$obj['image'];	
		if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$obj['image'] = $image;
		// inserted by Mike end



						if ($i%5==0)
						{
							$obj['class']='no-margin';
							if ($i!=10 )
							$obj['clear']=1;
						} else {
							$obj['clear']=0;
						}

					$obj['url'] = self::getFilmUrl($obj['film_id']);

					$obj['alt']=sys::translate('main::film').' '.$obj['title'];

					$ro=$_db->query("SELECT count(*) as count
						FROM `#__main_films_genres`
						WHERE genre_id = 16
						AND film_id = ".$obj['film_id']."
					");

					while($objo=$_db->fetchAssoc($ro))
					{
						$ann = $objo['count'];
					}


					if ($anim)
					{
						if (!$filmss[$obj['film_id']])
						{
							$var['objects'][]=$obj;
							$i++;
							$filmss[$obj['film_id']] = $obj['film_id'];
						}
					} else {
						if (!$ann)
						{
							if (!$filmss[$obj['film_id']])
							{
								$var['objects'][]=$obj;
								$i++;
								$filmss[$obj['film_id']] = $obj['film_id'];
							}
						}
					}
				}
			}
			}



			$result=sys::parseTpl('main::films_links_4',$var);
			sys::setCache($cache_name,$result);
			return $result;
		}
	}

	static function showFilmsLinks3()
	{

		sys::useLib('main::films');
		sys::useLib('main::shows');
		sys::useLib('main::cinemas');
		sys::useLib('main::countries');
		sys::useLib('sys::form');
		global $_db, $_cfg, $_err, $_user, $_cookie;
		sys::filterGet('date','text',date('d.m.Y'));
		sys::filterGet('order','text','cinemas');
		sys::filterGet('city_id','int',$_cfg['main::city_id']);
		$_GET['order']='films';

		if ($_GET['city_id'] && $_GET['city_id']!='1')
		{
			$can = '?city_id='.$_GET['city_id'];
		}

		$meta['city']=$result['city']=main_countries::getCityTitle($_GET['city_id'],'what');
		$meta['date']=$result['date']=strftime('%d %B',sys::date2Timestamp($_GET['date'],'%d.%m.%Y'));
		$result['city_id']=$_GET['city_id'];
		if(!$result['city'])
			return 404;



		$result['bill_url']='?mod=main&act=bill&date='.$_GET['date'].'&city_id='.$_GET['city_id'];
		$result['print_url']=sys::rewriteUrl('?mod=main&act=bill_print&date='.$_GET['date'].'&order='.$_GET['order'].'&city_id='.$_GET['city_id']);
		$result['objects']=main_shows::getBillBoard($_GET['city_id'],$_GET['date'], $_GET['order']);
		$result=sys::parseTpl('main::films_links_3',$result);
		sys::setCache($cache_name,serialize($result));
		return $result;
	}

    static function getFilmUniformUrl($film_id)
	{

			global $_cfg, $_db;

			$r=$_db->query("
				SELECT name, title_orig
				FROM `#__main_films`
				WHERE id='".$film_id."'
			");

			while($obj=$_db->fetchAssoc($r))
			{
				if ($obj['title_orig'])
				{
				    $urlstr = self::translitIt($obj['title_orig']);
				} else {
				    $urlstr = self::translitIt($obj['name']);
				}
				    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

			}



			$lang=$_cfg['sys::lang'];

			if ($lang=='ru')
			{
			return _ROOT_URL.'film/'.$urlstr.'-'.$film_id.'.phtml';
			} else {
			return _ROOT_URL.$lang.'/film/'.$urlstr.'-'.$film_id.'.phtml';
			}


	}

    static function getFilmUrlForRedirect($url)
    {
        $parseurl_arr = explode("/",$url);
        $key = array_search('film_id', $parseurl_arr);
        if($key > 0 && count($parseurl_arr>5) && isset($parseurl_arr[$key+1]))
        {
                $arr = explode(".",$parseurl_arr[$key+1]);
                $film_id = $arr[0];
                //echo $film_id;
                $filmredirecturl = self::getFilmUniformUrl($film_id);
                return $filmredirecturl;
        }
        return '';
    }


	static function getFilmWallpapersUrlForRedirect($wallpaper_id, $lang=false)
	{
		global $_cfg, $_db;
		if(!$lang) $lang=$_cfg['sys::lang'];
		
		$lng=3;
		if (($lang=='ru')||($lang=='rus')) $lng=1;
		
		$r=$_db->query("
			SELECT wp.film_id, lng.title as film
			FROM
			`#__main_films_wallpapers` AS wp
			LEFT JOIN `#__main_films_lng` AS `lng`
			ON lng.record_id=wp.id	AND lng.lang_id=$lng
			WHERE wp.id='".$wallpaper_id."'
		");

		$obj=$_db->fetchAssoc($r);

		return sys::getHumanUrl($obj['film_id'], $obj['film'], "film_wallpapers", $lang)."?wallpapper_id=$wallpaper_id";
	}
    
	static function getFilmPhotoUrlForRedirect($photo_id, $lang=false)
	{
		global $_cfg, $_db;
		if(!$lang) $lang=$_cfg['sys::lang'];
		
		$lng=3;
		if (($lang=='ru')||($lang=='rus')) $lng=1;
		
		$r=$_db->query("
			SELECT pht.film_id, lng.title as film
			FROM
			`#__main_films_photos` AS pht
			LEFT JOIN `#__main_films_lng` AS `lng`
			ON lng.record_id=pht.film_id	AND lng.lang_id=$lng
			WHERE pht.id='".$photo_id."'
		");
		$obj=$_db->fetchAssoc($r);

		return sys::getHumanUrl($obj['film_id'], $obj['film'], "film_photo", $lang)."?photo_id=$photo_id";
	}
    
	static function getFilmPosterUrlForRedirect($poster_id, $lang=false)
	{
		global $_cfg, $_db;
		if(!$lang) $lang=$_cfg['sys::lang'];
		
		$lng=3;
		if (($lang=='ru')||($lang=='rus')) $lng=1;
		
		$r=$_db->query("
			SELECT lng.title as film
			FROM
			`#__main_films_posters` AS pst
			LEFT JOIN `#__main_films_lng` AS `lng`
			ON lng.record_id=pst.film_id AND lng.lang_id=$lng
			WHERE pst.id='".$poster_id."'
		");
		$obj=$_db->fetchAssoc($r);

		return sys::getHumanUrl($poster_id, $obj['film'], "film_poster", $lang);
	}
}

?>