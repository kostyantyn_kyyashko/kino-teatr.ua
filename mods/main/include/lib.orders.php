<?php

class main_orders {
	
	static function getUserUrl($user_id)
	{
		return sys::rewriteUrl('?mod=main&act=user_profile_orders&user_id='.$user_id);
	}
	
	static function getOrders($user_id){
		
		sys::useLib('main::cinemas');
		sys::useLib('main::films');
		global $_db, $_cfg;
		$lang_id = intval($_cfg['sys::lang_id']);
		$q="
			SELECT o.data_order, o.data_buy, o.`sum`, hls_lng.title AS 'hall_name', hls.name AS 'hall', o.cinema_id, o.film_id, o.time_show, o.info, o.`sum`, cl.title AS cinema_name, flm_lng.title AS film_name, o.type, o.megakino_id, o.site_id, o.pmt_id
			FROM `#__main_users_orders` AS `o`
			LEFT JOIN `#__main_cinemas_halls` AS `hls` ON hls.id=o.hall_id
			LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng` ON hls_lng.record_id=hls.id AND hls_lng.lang_id=$lang_id
			LEFT JOIN `#__main_films` AS `flm` ON flm.id=o.film_id
			LEFT JOIN `#__main_films_lng` AS `flm_lng` ON flm_lng.record_id=o.film_id AND flm_lng.lang_id=$lang_id
			LEFT JOIN `#__main_cinemas` AS `c` ON c.id=o.cinema_id
			LEFT JOIN `#__main_cinemas_lng` AS `cl` ON cl.record_id = c.id AND cl.lang_id = $lang_id
			WHERE o.user_id=$user_id AND o.status = 1 ORDER BY o.data_buy DESC";
			
		$result=array();
	
		$result['pages']=sys_pages::pocess($q,$_cfg['main::films_on_page']);
	

		foreach(array("first_page", "prev_page", "next_page", "last_page") as $key=>$keyval)
		{
			if(isset($result['pages'][$keyval]) && $result['pages'][$keyval])
				$result['pages'][$keyval] = $result['pages'][$keyval];
		}
		
		for($page=1; $page<=$result['pages']['last_page_number']; $page++)
		 if(isset($result['pages']['pages'][$page]))
			$result['pages']['pages'][$page] = $result['pages']['pages'][$page];	
		
		$r=$_db->query($q.$result['pages']['limit']);
//		$r=$_db->query($q);
		
		
		
		 
		
		while ($obj=$_db->fetchArray($r))
		{
			$obj['cinema_url']=main_cinemas::getCinemaUrl($obj['cinema_id']);
			$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
			$buffer = json_decode($obj['info'],true);
			$obj['tickets_count'] = count($buffer['tickets']);
			$obj['tickets'] = $buffer['tickets'];
			$obj['event_time']=self::getDate($obj['info']);
			$obj['info']=self::getInfo($obj['info']);
			if(!$obj['hall_name'] && $obj['hall'])
				$obj['hall_name'] = $obj['hall'];
			$result[]=$obj;
		}
//		$to_email = 'mail@vitaliy43.ru';
//		$to_name = 'vitaliy';
//		$subject = 'Check';
//		$text = 'Проверка сообщения';
//		$res = sys::sendMail('','',$to_email,$to_name,$subject,$text);
//		if($res)
//			echo 'Sended!!! <br>';

		
			
			
		return $result;
		
	}
	
	static function getOrder($megakino_id){
		
		sys::useLib('main::cinemas');
		sys::useLib('main::films');
		global $_db, $_cfg, $_user;
		$lang_id = intval($_cfg['sys::lang_id']);
		$q="
			SELECT o.data_order, o.data_buy, o.`sum`, o.hall_id, hls_lng.title AS 'hall_name', hls.name AS 'hall', o.cinema_id, o.film_id, o.time_show, o.data_show, o.info, o.`sum`, cl.title AS cinema_name, flm_lng.title AS film_name, o.type, o.megakino_id, o.site_id, flm.3d, flm.age_limit, cl.address, cli.title AS city_name, c.phone, o.user_id
			FROM `#__main_users_orders` AS `o`
			LEFT JOIN `#__main_cinemas_halls` AS `hls` ON hls.id=o.hall_id
			LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng` ON hls_lng.record_id=hls.id AND hls_lng.lang_id=$lang_id
			LEFT JOIN `#__main_films` AS `flm` ON flm.id=o.film_id
			LEFT JOIN `#__main_films_lng` AS `flm_lng` ON flm_lng.record_id=o.film_id AND flm_lng.lang_id=$lang_id
			LEFT JOIN `#__main_cinemas` AS `c` ON c.id=o.cinema_id
			LEFT JOIN `#__main_cinemas_lng` AS `cl` ON cl.record_id = c.id AND cl.lang_id = $lang_id
			LEFT JOIN `#__main_countries_cities` AS `ci` ON c.city_id = ci.id
			LEFT JOIN `#__main_countries_cities_lng` AS `cli` ON cli.record_id = ci.id AND cli.lang_id = $lang_id
			WHERE o.megakino_id=$megakino_id";
		$r=$_db->query($q);
		
		
		
		$result=array();
		while ($obj=$_db->fetchArray($r))
		{
			if($obj['user_id'] != $_user['id']){
				return $result[0]['error'] = 'Вы не имеете права на просмотр данной страницы!';
			}
			$obj['cinema_url']=main_cinemas::getCinemaUrl($obj['cinema_id']);
			$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
			$obj['arr']=json_decode($obj['info'],true);
			$obj['event_time']=self::getDate($obj['info']);
			$obj['arr']['expireAt']=$obj['arr']['tickets'][0]['eventDate'];
			$buffer_tickets=json_decode($obj['info'],true);
			$tickets=$buffer_tickets['tickets'];
			$obj['tickets']=$tickets;
			$obj['info']=self::getInfo($obj['info']);
			if(!$obj['age_limit'])
				$obj['age_limit']='0';
			
			$obj['text_data'] = self::get_text_data($obj['data_show']);
			if(!$obj['hall_name'] && $obj['hall'])
				$obj['hall_name'] = $obj['hall'];
			
			
			$qw=$_db->query("
			SELECT
				pst.image as `poster`

			FROM `#__main_films_posters` AS `pst`

			WHERE pst.film_id=".intval($obj['film_id'])."
			AND pst.order_number=1
			LIMIT 1
		");

		$poster = '';

		while($object=$_db->fetchAssoc($qw))
		{
			$poster = $object['poster'];
		}

		if ($poster)
		{
//			$obj['poster'] = 'http://kino-teatr.ua/public/main/resize.php?f='.$poster.'&w=60';
		// inserted by Mike begin
	  	    $image="x2_".$poster;	
			if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'ims/kt_logo.jpg';
//			 else $image=$_cfg['main::films_url'].$image;
			 else $image='/public/main/films/'.$image;
			$obj['poster'] = $image;
			// inserted by Mike end
		} else {
			$obj['poster'] = $_cfg['sys::root_url'].'ims/kt_logo.jpg';
		}
			$result[]=$obj;
		}
		$result[0]['link_profile'] = '/'.$_cfg['sys::lang'].'/main/user_profile_orders/user_id/'.$_user['id'].'.phtml';
;
		return $result[0];
	}
	
	static function createPdf($result,$megakino_id,$title){
		sys::useLib('main::basket');
		include_once($_SERVER['DOCUMENT_ROOT'].'/mods/main/libs/mpdf60/mpdf.php');
		$html = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/mods/main/skins/default/pdf/ticket.html');
		$mpdf = new mPDF('utf-8', 'A4', '10', '', 8, 8, 8, 8, 10);
		$mpdf->SetTitle($title.'.pdf');
		if(isset($_REQUEST['page']))
			$page = (int)$_REQUEST['page'];
		$html = self::setData($result,$page,$html);
		$mpdf->WriteHTML($html);
		if(isset($_REQUEST['send_pdf']))
			main_basket::sendPdf($megakino_id);
		else
			$mpdf->Output(self::rus2translit($title).'-'.$page.'.pdf','D');
			
		exit;
	}
	
	static function rus2translit($st)
{
	$st = mb_strtolower($st, "utf-8");
	$st = str_replace(array(
		'?', '!', '.', ',', ':', ';', '*', '(', ')', '{', '}', '[', ']', '%', '#', '№', '@', '$', '^', '-', '+', '/', '\\', '=', '|', '"', '\'',
		'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'з', 'и', 'й', 'к',
		'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х',
		'ъ', 'ы', 'э', ' ', 'ж', 'ц', 'ч', 'ш', 'щ', 'ь', 'ю', 'я'
	), array(
		'_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_',
		'a', 'b', 'v', 'g', 'd', 'e', 'e', 'z', 'i', 'y', 'k',
		'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h',
		'j', 'i', 'e', '_', 'zh', 'ts', 'ch', 'sh', 'shch',
		'', 'yu', 'ya'
	), $st);
	$st = preg_replace("/[^a-z0-9_]/", "", $st);
	$st = trim($st, '_');

	$prev_st = '';
	do {
		$prev_st = $st;
		$st      = preg_replace("/_[a-z0-9]_/", "_", $st);
	} while ($st != $prev_st);

	$st = preg_replace("/_{2,}/", "_", $st);
	return $st;
}
	
	static function setData($result,$page,$html){
		if($page<0)
			$page=0;
		$info = $result['tickets'][$page-1];
		$html = str_replace('%poster%',$result['poster'],$html);
		$html = str_replace('%cinema_name%',$result['cinema_name'],$html);
		$html = str_replace('%data%',$result['text_data'].','.$result['time_show'],$html);
		$html = str_replace('%place_name%',$result['city_name'].','.$result['address'],$html);
		$html = str_replace('%row%',$info['rowNumber'],$html);
		$html = str_replace('%place%',$info['placeNumber'],$html);
		$html = str_replace('%price%',$info['price']/100,$html);
		if($result['3d'])
			$html = str_replace('%film_name%',$result['film_name'].' / 3D',$html);
		else
			$html = str_replace('%film_name%',$result['film_name'].' / 2D',$html);
		$html = str_replace('%age_limit%','Обмеження за віком: '.$result['age_limit'].'+',$html);
		if($result['hall'] == 'Зал' || $result['hall'] == 'зал'){
			$html = str_replace('%hall_name%','',$html);
		}
		else{
			$html = str_replace('%hall_name%','<tr><td>Зал: '.$result['hall'].'</td></tr>',$html);
		}
		$html = str_replace('%barcode%','<img src="http://'.$_SERVER['HTTP_HOST'].'/barcode.php?barcode='.$info['barcode'].'" />',$html);
		$html = str_replace('%barcode_footer%','<img src="http://'.$_SERVER['HTTP_HOST'].'/barcode.php?barcode='.$info['barcode'].'&text_size=2&height=60&width=300" />',$html);
		$html = str_replace('%ticket%',$result['megakino_id'],$html);

		return $html;
	}
	
	static function get_text_data($date){
		$months=array(
		'01' => 'Січня',
		'02' => 'Лютого',
		'03' => 'Березня',
		'04' => 'Квітня',
		'05' => 'Травня',
		'06' => 'Червеня',
		'07' => 'Липня',
		'08' => 'Серпня',
		'09' => 'Вересня',
		'10' => 'Жовтня',
		'11' => 'Листопада',
		'12' => 'Грудня'
		);
		list($year,$month,$day) = explode('-',$date);
		$str = $day.' '.$months[$month].' '.$year;
		return $str;
	}
	
	static function getDate($info){
		$arr = json_decode($info,true);
		$buffer_time = time() + (365*86400);
		if(!count($arr['tickets']))
			return false;
		foreach($arr['tickets'] as $item){
			$event_time = strtotime($item['eventDate']);
			if($event_time<$buffer_time)
				$buffer_time=$event_time;
		}
		return $buffer_time;
	}
	

	
	static function getInfo($info){
		$arr = json_decode($info,true);
		$html = '<table>';
		$rows = array();
		if(!count($arr['tickets']))
			return '';
		foreach($arr['tickets'] as $item){
			$rows[$item['rowNumber']][] = $item['placeNumber'];
		}
		
		foreach($rows as $row=>$places){
			$html .= '<tr><td>';
			if(is_array($places)){
				$html .= 'Ряд '.$row;
				if(count($places)>1){
					$html .= '. Места '.implode(',',$places);
				}
				else{
					$html .= '. Место '.$places[0];
				}
			}
			$html .= '</td></tr>';
		}
		$html .= '</table>';
		return $html;
	}
}

?>