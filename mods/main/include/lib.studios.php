<?php
class main_studios
{	
	static function deletestudio($studio_id)
	{
		global $_db;
		$_db->delete('main_films_studios',"`studio_id`=".intval($studio_id)."");
		$_db->delete('main_studios',intval($studio_id),true);
	}
	
	static function getStudiosNames($arr_ids)
	{
		global $_db;
		$result=array();
		if(!$arr_ids || !count($arr_ids))
			return false;
		for($i=0; $i<count($arr_ids); $i++)
		{
			$arr_ids[$i]=intval($arr_ids[$i]);
		}
		$str_ids=implode(',',$arr_ids);
		$r=$_db->query("SELECT `id`, `name` 
			FROM `#__main_studios` 
			WHERE `id` IN(".$str_ids.")");
		while($obj=$_db->fetchAssoc($r))
		{
			$result[$obj['id']]=$obj['name'];
		}
		return $result;
	}
}
?>