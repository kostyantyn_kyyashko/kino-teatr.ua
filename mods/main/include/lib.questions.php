<?php
class main_questions
{	
	static function deleteQuestion($question_id)
	{
		global $_db;
		$_db->delete('main_questions',intval($question_id),true);
	}
	

	static function getQuestion()
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT 
				qst.id,
				qst_lng.question,
				qst_lng.answers
			FROM `#__main_questions` AS `qst`
			LEFT JOIN `#__main_questions_lng` AS `qst_lng`
			ON qst_lng.record_id=qst.id
			AND qst_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			ORDER BY RAND()
			LIMIT 0,1
		");
		$result=$_db->fetchAssoc($r);
		
		$result['answers']=explode("\n",$result['answers']);
		$last=array_pop($result['answers']);
		shuffle($result['answers']);
		array_push($result['answers'],$last);
		$result['answers']=array_reverse($result['answers']);
		return $result;
	}
	
	static function checkAnswer($question_id, $answer)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT 
				qst_lng.answers
			FROM `#__main_questions` AS `qst`
			LEFT JOIN `#__main_questions_lng` AS `qst_lng`
			ON qst_lng.record_id=qst.id
			AND qst_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE qst.id=".intval($question_id)."
		");
		list($answers)=$_db->fetchArray($r);
		$answers=explode("\n",$answers);
		return(trim($answer)==trim($answers[0]));
	}
}
?>