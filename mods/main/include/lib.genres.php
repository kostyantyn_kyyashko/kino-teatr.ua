<?php
class main_genres
{	
	static function deleteGenre($genre_id)
	{
		global $_db;
		$_db->delete('main_films_genres',"`genre_id`=".intval($genre_id)."");
		$_db->delete('main_genres',intval($genre_id),true);
	}	
	
	static function getGenresNames($arr_ids)
	{
		global $_db;
		$result=array();
		if(!$arr_ids || !count($arr_ids))
			return false;
		for($i=0; $i<count($arr_ids); $i++)
		{
			$arr_ids[$i]=intval($arr_ids[$i]);
		}
		$str_ids=implode(',',$arr_ids);
		$r=$_db->query("SELECT `id`, `name` 
			FROM `#__main_genres` 
			WHERE `id` IN(".$str_ids.")");
		while($obj=$_db->fetchAssoc($r))
		{
			$result[$obj['id']]=$obj['name'];
		}
		return $result;
	}
	
	static function getGenresTitles()
	{
		global $_cfg, $_db;
		$r=$_db->query("
			SELECT gnr.id, lng.title 
			FROM `#__main_genres` AS `gnr`
			LEFT JOIN `#__main_genres_lng` AS `lng`
			ON lng.record_id=gnr.id 
			AND lng.lang_id=".intval($_cfg['sys::lang_id'])."
			ORDER BY gnr.order_number 
		");
		while ($genre=$_db->fetchAssoc($r)) 
		{
			$result[$genre['id']]=$genre['title'];	
		}
		return $result;
	}
	
	static function getTextGenresIds($text_id, $text_type="news")
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `genre_id`
			FROM `#__main_texts_genres`
			WHERE `text_id`=".intval($text_id)." AND `text_type`='".$text_type."'");
		while(list($genre_id)=$_db->fetchArray($r))
		{
			$result[]=$genre_id;
		}
		return $result;
	}

	static function linkTextsGenres($text_id, $text_type="news", $genres=false)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_texts_genres`
			WHERE `text_id`=".intval($text_id)." AND `text_type`='".$text_type."'");
		if($genres)
		{
			$genres=array_unique($genres);
			foreach ($genres as $genre_id=>$name)
			{
				if($genre_id)
				{
					$_db->query("INSERT INTO `#__main_texts_genres`
						(
							`text_id`,
							`genre_id`,
							`text_type`
						)
						VALUES
						(
							".intval($text_id).",
							".intval($genre_id).",
							'$text_type'
						)
					");
				}
			}
		}
	}	
}
?>