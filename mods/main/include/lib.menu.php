<?php
class main_menu
{
	static function deletemenu($menu_id)
	{
		global $_db;
		$r=$_db->query("
			SELECT `id` FROM `#__main_menu_articles`
			WHERE `menu_id`=".intval($menu_id)."
		");
		while(list($article_id)=$_db->fetchArray($r))
		{
			self::deleteArticle($article_id);
		}
		$_db->delete('main_menu',intval($menu_id),true);
	}

	static function confirmArticle($article_id)
	{
		global $_db;
		$_db->setValue('main_menu_articles','public',1,intval($article_id));
	}


	static function showmenuAdminTabs($menu_id, $active_tab)
	{
		$tabs['info']['title']=sys::translate('sys::info');
		$tabs['info']['url']='?mod=main&act=menu_edit&id='.$menu_id;

		$tabs['articles']['title']=sys::translate('main::articles');
		$tabs['articles']['url']='?mod=main&act=menu_articles_table&menu_id='.$menu_id;

		return sys_gui::showTabs($tabs, $active_tab);
	}

	static function deleteArticleImage($filename)
	{
		global $_cfg;
		if(is_file($_cfg['main::menu_dir'].$filename))
			unlink($_cfg['main::menu_dir'].$filename);
		if(is_file($_cfg['main::menu_dir'].'x1_'.$filename))
			unlink($_cfg['main::menu_dir'].'x1_'.$filename);
		if(is_file($_cfg['main::menu_dir'].'x2_'.$filename))
			unlink($_cfg['main::menu_dir'].'x2_'.$filename);
		if(is_file($_cfg['main::menu_dir'].'x3_'.$filename))
			unlink($_cfg['main::menu_dir'].'x3_'.$filename);
		if(is_file($_cfg['main::menu_dir'].'x5_'.$filename))
			unlink($_cfg['main::menu_dir'].'x5_'.$filename);
	}

	static function deleteArticle($article_id)
	{
		sys::useLib('main::discuss');
		global $_db, $_cfg;
		$article=$_db->getRecord('main_menu_articles',intval($article_id));

		if(is_file($_cfg['main::menu_dir'].$article['small_image']))
			unlink($_cfg['main::menu_dir'].$article['small_image']);

		self::deleteArticleImage($article['image']);

		$_db->delete('main_menu_articles_persons',"`article_id`=".intval($article_id)."");
		$_db->delete('main_menu_articles_films',"`article_id`=".intval($article_id)."");


		main_discuss::deleteDiscuss($article_id,'menu_article');
		$_db->delete('main_menu_index',"`article_id`=".intval($article_id)."");
		$_db->delete('main_menu_articles',$article_id,true);
	}

	static function deleteWinner($user_id, $menu_id)
	{
		sys::useLib('main::discuss');
		global $_db, $_cfg;
		$_db->delete('main_menu_winners',"`user_id`=".intval($user_id)." AND `menu_id`=".intval($menu_id)."");
	}

	static function deleteQuestion($question_id, $menu_id)
	{
		sys::useLib('main::discuss');
		global $_db, $_cfg;
		$_db->delete('main_menu_questions',"`id`=".intval($question_id)." AND `menu_id`=".intval($menu_id)."");
	}


	static function uploadArticleImage($file,$article_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deleteArticleImage($current_image);

		if($image=sys::uploadFile($file,$_cfg['main::menu_dir'],'article_'.$article_id, true))
		{
			$_db->setValue('main_menu_articles','image',$image,$article_id);
			self::resizeArticleImage($image);
		}
		return $image;
	}

	static function resizeArticleImage($filename)
	{
		global $_cfg;
		sys::resizeImage($_cfg['main::menu_dir'].$filename,$_cfg['main::menu_dir'].'x1_'.$filename,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		sys::resizeImage($_cfg['main::menu_dir'].$filename,$_cfg['main::menu_dir'].'x2_'.$filename,$_cfg['main::x2_width'],$_cfg['main::x2_height']);
		sys::resizeImage($_cfg['main::menu_dir'].$filename,$_cfg['main::menu_dir'].'x3_'.$filename,$_cfg['main::x3_width'],$_cfg['main::x3_height']);
		sys::resizeImage($_cfg['main::menu_dir'].$filename,$_cfg['main::menu_dir'].'x5_'.$filename,$_cfg['main::x5_width'],$_cfg['main::x5_height']);
	}

	static function uploadArticleSmallImage($file,$article_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image && is_file($_cfg['main::menu_dir'].$current_image))
			unlink($_cfg['main::menu_dir'].$current_image);

		if($image=sys::uploadFile($file,$_cfg['main::menu_dir'],'x1_'.$article_id, true))
		{
			$_db->setValue('main_menu_articles','small_image',$image,$article_id);
			sys::resizeImage($_cfg['main::menu_dir'].$image,false,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		}
		return $image;
	}

	static function linkArticlePersons($article_id, $persons)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_menu_articles_persons`
			WHERE `article_id`='".intval($article_id)."'");
		if($persons)
		{
			$persons=array_unique($persons);
			foreach ($persons as $person_id=>$name)
			{
				if($person_id)
				{
					$_db->query("INSERT INTO `#__main_menu_articles_persons`
						(
							`article_id`,
							`person_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($person_id)."
						)
					");
				}
			}
		}
	}

	static function linkArticleFilms($article_id, $films)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_menu_articles_films`
			WHERE `article_id`='".intval($article_id)."'");
		if($films)
		{
			$films=array_unique($films);
			foreach ($films as $film_id=>$name)
			{
				if($film_id)
				{
					$_db->query("INSERT INTO `#__main_menu_articles_films`
						(
							`article_id`,
							`film_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($film_id)."
						)
					");
				}
			}
		}
	}

	static function getArticlePersonsIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `person_id`
			FROM `#__main_menu_articles_persons`
			WHERE `article_id`=".intval($article_id)."");
		while(list($person_id)=$_db->fetchArray($r))
		{
			$result[]=$person_id;
		}
		return $result;
	}

	static function getArticleFilmsIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `film_id`
			FROM `#__main_menu_articles_films`
			WHERE `article_id`=".intval($article_id)."");
		while(list($film_id)=$_db->fetchArray($r))
		{
			$result[]=$film_id;
		}
		return $result;
	}

	static function translitIt($str)
	{
	    $tr = array(
	        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
	        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
	        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
	        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
	        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
	        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
	        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	        "the"=>"","the "=>""," the"=>""," the "=>"-",
	        "The"=>"","The "=>""," The"=>""," The "=>"-",
	        "... "=> "", " - "=> "-"," "=> "-", "."=> "", "/"=> "-"
	    );
	    return strtr($str,$tr);
	}

	static function getArticleUrl($article_id)
	{
			return sys::rewriteUrl('?mod=main&act=menu_article&article_id='.$article_id);
	}

	static function getmenuUrl($menu_id)
	{
		return sys::rewriteUrl('?mod=main&act=menu&id='.$menu_id);
	}

	static function showLastPopularArticles()
	{
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_last_menu_'.$_cfg['sys::lang'].$_cfg['main::city_id'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::last_menu_cache_period']*60))
			return $cache;


		$r=$_db->query("
			SELECT
				art.id,
				art.date,
				art.menu_id,
				art.total_shows,
				art.comments,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				art_lng.title,
				nws_lng.title AS `menu`,
				art_lng.intro
			FROM `#__main_menu_articles` AS `art`

			LEFT JOIN `#__main_menu_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_menu_lng` AS `nws_lng`
			ON nws_lng.record_id=art.menu_id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_menu` AS `nws`
			ON art.menu_id=nws.id

			WHERE
			art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND (art.city_id='".intval($_cfg['main::city_id'])."' OR art.city_id=0)
			AND nws.showtab=1
			AND art.id != '3544'
			ORDER BY art.today_shows DESC, art.total_shows DESC
			LIMIT 0,5
		");

		$i=1;

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['intro']=sys::cutString(htmlspecialchars(strip_tags($obj['intro'])),140);
			$obj['shows']=$obj['total_shows'];
			$obj['menu']=htmlspecialchars($obj['menu']);
		if($obj['image'])
			$image=$_cfg['main::menu_url'].$obj['image'];

		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));

		if ($wid<213 && $obj['big_image'])
		{
		 	$image = $_cfg['main::menu_url'].$obj['big_image'];
		} else if ($wid>=213)
		{
		 	$image = $_cfg['main::menu_url'].$obj['image'];
		} else {
         	$image = 'blank_menu_img.jpg';
		}

		if ($i==1)
		{
		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';
		} else {
		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=2';
		}
			$obj['url']=self::getArticleUrl($obj['id']);
			$obj['menu_url']=self::getmenuUrl($obj['menu_id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';

			if ($i==3 || $i==5)
			{
			$obj['bottom']=' bottommenu';
			}

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::last_menu',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function showLastDiscussedArticles()
	{
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_last_menu_'.$_cfg['sys::lang'].$_cfg['main::city_id'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::last_menu_cache_period']*60))
			return $cache;


		$r=$_db->query("
			SELECT
				art.id,
				art.date,
				art.menu_id,
				art.total_shows,
				art.comments,
				art.small_image AS `image`,
				art.image AS `big_image`,
				art.exclusive,
				art_lng.title,
				nws_lng.title AS `menu`,
				art_lng.intro
			FROM `#__main_menu_articles` AS `art`

			LEFT JOIN `#__main_menu_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_menu_lng` AS `nws_lng`
			ON nws_lng.record_id=art.menu_id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_menu` AS `nws`
			ON art.menu_id=nws.id

			WHERE
			art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND (art.city_id='".intval($_cfg['main::city_id'])."' OR art.city_id=0)
			AND nws.showtab=1
			AND art.id != '3544'
			ORDER BY art.comments DESC
			LIMIT 0,5
		");

		$i=1;

		while($obj=$_db->fetchAssoc($r))
		{
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['intro']=sys::cutString(htmlspecialchars(strip_tags($obj['intro'])),140);
			$obj['shows']=$obj['total_shows'];
			$obj['menu']=htmlspecialchars($obj['menu']);
		if($obj['image'])
			$image=$_cfg['main::menu_url'].$obj['image'];

		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));

		if ($wid<213 && $obj['big_image'])
		{
		 	$image = $_cfg['main::menu_url'].$obj['big_image'];
		} else if ($wid>=213)
		{
		 	$image = $_cfg['main::menu_url'].$obj['image'];
		} else {
         	$image = 'blank_menu_img.jpg';
		}

		if ($i==1)
		{
		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';
		} else {
		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=2';
		}
			$obj['url']=self::getArticleUrl($obj['id']);
			$obj['menu_url']=self::getmenuUrl($obj['menu_id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';

			if ($i==3 || $i==5)
			{
			$obj['bottom']=' bottommenu';
			}

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::last_menu',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}


	static function showMenu()
	{
		global $_db, $_cfg;
		$var['objects']=array();

		$r=$_db->query("
			SELECT
				art.id,
				art.parent_id,
				art.link,
				art_lng.title
			FROM `#__main_menu_articles` AS `art`

			LEFT JOIN `#__main_menu_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE art.public=1
			ORDER BY art.id
		");

		$i=1;




		while($obj=$_db->fetchAssoc($r))
		{

			if ($obj['link']=='bill.phtml')
			{

						$qq="SELECT * FROM `#__main_countries_cities`
						WHERE id='".$_cfg['main::city_id']."'
						";

						$rr=$_db->query($qq);
						while($objq=$_db->fetchAssoc($rr))
						{
							$city = $objq['translit'];
						}



				if ($_cfg['sys::lang']=='uk')
				{
					$obj['link'] = 'uk/kinoafisha-'.$city.'.phtml';
				} else {
					$obj['link'] = 'kinoafisha-'.$city.'.phtml';
				}

			} else if ($obj['link']=='ru/main/bill/order/films.phtml')
			{

						$qq="SELECT * FROM `#__main_countries_cities`
						WHERE id='".$_cfg['main::city_id']."'
						";

						$rr=$_db->query($qq);
						while($objq=$_db->fetchAssoc($rr))
						{
							$city = $objq['translit'];
						}



				if ($_cfg['sys::lang']=='uk')
				{
					$obj['link'] = 'uk/afisha-kino-'.$city.'.phtml';
				} else {
					$obj['link'] = 'afisha-kino-'.$city.'.phtml';
				}
            } else if ($obj['link']=='cinemas.phtml')
			{

						$qq="SELECT * FROM `#__main_countries_cities`
						WHERE id='".$_cfg['main::city_id']."'
						";

						$rr=$_db->query($qq);
						while($objq=$_db->fetchAssoc($rr))
						{
							$city = $objq['translit'];
						}



				if ($_cfg['sys::lang']=='uk')
				{
					$obj['link'] = 'uk/cinemas-'.$city.'.phtml';
				} else {
					$obj['link'] = 'cinemas-'.$city.'.phtml';
				}

			} else {
				if ($_cfg['sys::lang']=='uk')
				{
					if(substr($obj['link'],0,2)=='ru')
					{
						$obj['link'] = 'uk'.substr($obj['link'],2);
					} else {
						$obj['link'] = 'uk/'.$obj['link'];
					}
				}
			}

			if ($obj['parent_id']==0)
			{
				$var['objects'][$obj['id']]=$obj;
			} else {
				$var['objects'][$obj['parent_id']]['child'][]=$obj;
			}
			$i++;
		}



		$result=sys::parseTpl('main::top_menu',$var);
		return $result;
	}

	static function showBotMenu()
	{
		global $_db, $_cfg;
		sys::useLib('main::cinemas');
		$var['objects']=array();

		$r=$_db->query("
			SELECT
				art.id,
				art.parent_id,
				art.link,
				art.class,
				art_lng.title
			FROM `#__main_menu_articles` AS `art`

			LEFT JOIN `#__main_menu_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE art.public=1
			ORDER BY art.id
		");

		$i=1;




		while($obj=$_db->fetchAssoc($r))
		{

			if ($obj['link']=='bill.phtml')
			{

						$qq="SELECT a.* FROM `#__main_countries_cities` a
						WHERE a.id='".$_cfg['main::city_id']."'
						";

						$rr=$_db->query($qq);
						while($objq=$_db->fetchAssoc($rr))
						{
							$city = $objq['translit'];
						}




				if ($_cfg['sys::lang']=='uk')
				{
					$obj['link'] = 'uk/kinoafisha-'.$city.'.phtml';
				} else {
					$obj['link'] = 'kinoafisha-'.$city.'.phtml';
				}

			} else if ($obj['link']=='cinemas.phtml')
			{

						$qq="SELECT a.*,lng.title_what FROM `#__main_countries_cities` a
						LEFT JOIN `#__main_countries_cities_lng` lng
						ON lng.record_id = a.id
						AND lng.lang_id = '".intval($_cfg['sys::lang_id'])."'
						WHERE a.id='".$_cfg['main::city_id']."'

						";

						$rr=$_db->query($qq);
						while($objq=$_db->fetchAssoc($rr))
						{
							$city = $objq['translit'];
							$what = $objq['title_what'];
						}


				$obj['title'] .= ' '.$what;


				if ($_cfg['sys::lang']=='uk')
				{
					$obj['link'] = 'uk/cinemas-'.$city.'.phtml';
				} else {
					$obj['link'] = 'cinemas-'.$city.'.phtml';
				}

			} else {
				if ($_cfg['sys::lang']=='uk')
				{
					if(substr($obj['link'],0,2)=='ru')
					{
						$obj['link'] = 'uk'.substr($obj['link'],2);
					} else {
						$obj['link'] = 'uk/'.$obj['link'];
					}
				}
			}

			if ($obj['id']!=40)
			{

			if ($obj['parent_id']==0)
			{
				$var['objects'][$obj['id']]=$obj;
			} else {
				$var['objects'][$obj['parent_id']]['child'][]=$obj;
			}
			$i++;

			}
		}

		$qq="SELECT * FROM `#__main_cinemas` cnm
		WHERE cnm.city_id='".$_cfg['main::city_id']."'
		ORDER BY order_number LIMIT 0,5
		";

		$rr=$_db->query($qq);
		while($objq=$_db->fetchAssoc($rr))
		{
			$obj['title'] = sys::translate('main::cinema').' '.$objq['name'];
			$obj['link'] = str_replace(_ROOT_URL,'',main_cinemas::getCinemaUrl($objq['id']));

			$var['objects'][5]['child'][]=$obj;
		}

		$result=sys::parseTpl('main::bot_menu',$var);
		return $result;
	}

	static function showBotMenuCinema()
	{
		global $_db, $_cfg;
		sys::useLib('main::cinemas');
		$var['objects']=array();

		$r=$_db->query("
			SELECT
				art.id,
				art.parent_id,
				art.link,
				art.class,
				art_lng.title
			FROM `#__main_menu_articles` AS `art`

			LEFT JOIN `#__main_menu_articles_lng` AS `art_lng`
			ON art_lng.record_id=art.id
			AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE art.public=1
			ORDER BY art.id
		");

		$i=1;




		while($obj=$_db->fetchAssoc($r))
		{

			if ($obj['link']=='bill.phtml')
			{

						$qq="SELECT a.* FROM `#__main_countries_cities` a
						WHERE a.id='".$_cfg['main::city_id']."'
						";

						$rr=$_db->query($qq);
						while($objq=$_db->fetchAssoc($rr))
						{
							$city = $objq['translit'];
						}




				if ($_cfg['sys::lang']=='uk')
				{
					$obj['link'] = 'uk/kinoafisha-'.$city.'.phtml';
				} else {
					$obj['link'] = 'kinoafisha-'.$city.'.phtml';
				}

			} else if ($obj['link']=='cinemas.phtml')
			{

						$qq="SELECT a.*,lng.title_what FROM `#__main_countries_cities` a
						LEFT JOIN `#__main_countries_cities_lng` lng
						ON lng.record_id = a.id
						AND lng.lang_id = '".intval($_cfg['sys::lang_id'])."'
						WHERE a.id='".$_cfg['main::city_id']."'

						";

						$rr=$_db->query($qq);
						while($objq=$_db->fetchAssoc($rr))
						{
							$city = $objq['translit'];
							$what = $objq['title_what'];
						}


				$obj['title'] .= ' '.$what;


				if ($_cfg['sys::lang']=='uk')
				{
					$obj['link'] = 'uk/cinemas-'.$city.'.phtml';
				} else {
					$obj['link'] = 'cinemas-'.$city.'.phtml';
				}

			} else {
				if ($_cfg['sys::lang']=='uk')
				{
					if(substr($obj['link'],0,2)=='ru')
					{
						$obj['link'] = 'uk'.substr($obj['link'],2);
					} else {
						$obj['link'] = 'uk/'.$obj['link'];
					}
				}
			}

			if ($obj['id']!=40)
			{

				if ($obj['id']!=6 && $obj['parent_id']!=6   && $obj['id']!=18 && $obj['parent_id']!=18)
				{

					if ($obj['parent_id']==0)
					{
						$var['objects'][$obj['id']]=$obj;
					} else {
						$var['objects'][$obj['parent_id']]['child'][]=$obj;
					}
					$i++;

				}

			}
		}

		$qq="SELECT * FROM `#__main_cinemas` cnm
		WHERE cnm.city_id='".$_cfg['main::city_id']."'
		ORDER BY order_number LIMIT 0,5
		";

		$rr=$_db->query($qq);
		while($objq=$_db->fetchAssoc($rr))
		{
			$obj['title'] = sys::translate('main::cinema').' '.$objq['name'];
			$obj['link'] = str_replace(_ROOT_URL,'',main_cinemas::getCinemaUrl($objq['id']));

			$var['objects'][5]['child'][]=$obj;
		}

		$result=sys::parseTpl('main::bot_menu_cinema',$var);
		return $result;
	}

	static function getMonthBegin($month,$year)
	{
		return mktime(0,0,0,$month,1,$year);
	}

	static function getMonthEnd($month,$year)
	{
		return mktime(23,59,59,$month,date('t',self::getMonthBegin($month,$year)),$year);
	}

	static function getCalendar($year=false, $month=false, $menu_id=false)
	{
		global $_db, $_cfg;

		$file=_CACHE_DIR.'main_menu_calendar_'.$menu_id.'.dat';
		if(is_file($file))
		{
			clearstatcache();
			if(date('m')==date('m',filemtime($file)) && $_cfg['main::cache_menu_calendar'])
				return unserialize(file_get_contents($file));
			else
				unlink($file);

		}

		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$month=intval($month);
		$q="
			SELECT
				MAX(`date`),
				MIN(`date`)
			FROM `#__main_menu_articles`
			WHERE `public`=1
		";
		if($menu_id)
		{
			$q.="
				AND `menu_id`=".intval($menu_id)."
			";
		}
		$r=$_db->query($q);
		list($max_date, $min_date)=$_db->fetchArray($r);
		$min_date=sys::db2Timestamp($min_date);
		$max_date=sys::db2Timestamp($max_date);
		$result=array();
		if($min_date)
		{
			$min_year=date('Y',$min_date);
			$max_yar=date('Y',$max_date);

			for($y=$min_year; $y<$max_yar+1; $y++)
			{
				$obj['title']=$y;
				for($m=1; $m<13; $m++)
				{
					$obj['months'][$m]['title']=strftime('%b',self::getMonthBegin($m,$y));
					$obj['months'][$m]['is_menu']=self::ismenu($y,$m,$menu_id);
				}
				$result[$y]=$obj;
			}
		}

		$result=array_reverse($result,true);
		if($_cfg['main::cache_menu_calendar'])
			file_put_contents($file, serialize($result));
		return $result;
	}

	static function ismenu($year, $month, $menu_id=false)
	{
		global $_db, $_cfg;
		$month_begin=self::getMonthBegin($month,$year);
		$month_end=self::getMonthEnd($month,$year);

		$q="
			SELECT `id` FROM `#__main_menu_articles`
			WHERE `date` BETWEEN '".date('Y-m-d H:i:s',$month_begin)."' AND '".date('Y-m-d H:i:s',$month_end)."'
		";
		if($menu_id)
		{
			$q.="
				AND `menu_id`=".intval($menu_id)."
			";
		}
		$q.="
			AND (`city_id`=".intval($_cfg['main::city_id'])." OR `city_id`=0)
			LIMIT 0,1
		";

		$r=$_db->query($q);
		list($result)=$_db->fetchArray($r);
		return $result;
	}

	static function showCalendar($year=false, $month=false, $menu_id=false)
	{
		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$month=intval($month);

		$var['years']=self::getCalendar($year, $month, $menu_id);

		foreach ($var['years'] as $y=>$arr_year)
		{
			foreach ($arr_year['months'] as $m=>$arr_month)
			{
				if($month==$m && $year==$y)
					$var['years'][$y]['months'][$m]['current']=true;
				else
					$var['years'][$y]['months'][$m]['current']=false;

				if($var['years'][$y]['months'][$m]['is_menu'])
				{
					$url='?mod=main&act=menu&year='.$y.'&month='.intval($m);
					if($menu_id)
						$url.='&menu_id='.$menu_id;
					$var['years'][$y]['months'][$m]['url']=sys::rewriteUrl($url);
				}
				else
					$var['years'][$y]['months'][$m]['url']=false;
				$var['years'][$y]['months'][$m]['alt']=sys::translate('main::menu_cinema').' '.sys::translate('main::for').' '.sys::translate('main::month_'.$m).' '.$y;
			}
		}
		return sys::parseTpl('main::menu_calendar',$var);
	}

	static function showmenuTabs($year,$month,$menu_id)
	{
		global $_db, $_cfg;

		if(!$year)
			$year=date('Y');
		if(!$month)
			$month=date('m');

		$var['active']=$menu_id;

		$var['objects'][0]['title']=sys::translate('main::all_sections');
		$var['objects'][0]['url']=sys::rewriteUrl('?mod=main&act=menu');
		$var['objects'][0]['alt']=sys::translate('main::menu_cinema').'::'.sys::translate('main::all_sections');

		$r=$_db->query("
			SELECT nws.id, nws_lng.title
			FROM `#__main_menu` AS `nws`

			LEFT JOIN `#__main_menu_lng` AS `nws_lng`
			ON nws_lng.record_id=nws.id
			AND nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE nws.showtab='1'

			ORDER BY nws.order_number
		");
		while ($obj=$_db->fetchAssoc($r))
		{
			$var['objects'][$obj['id']]['title']=$obj['title'];
			$var['objects'][$obj['id']]['url']=sys::rewriteUrl('?mod=main&act=menu&menu_id='.$obj['id']);
			$var['objects'][$obj['id']]['alt']=sys::translate('main::menu_cinema').'::'.$obj['title'];
		}


		return sys::parseTpl('main::tabs',$var);
	}

	static function menuReirect($year, $month, $menu_id=false)
	{
		if($year<2000)
			return false;
		if($month==1)
		{
			$month=12;
			$year=$year-1;
		}
		else
		{
			$month=$month-1;
		}
		if(self::ismenu($year,$month,$menu_id))
		{
			$url='?mod=main&act=menu&year='.$year.'&month='.$month;
			if($menu_id)
				$url.='&menu_id='.$menu_id;
			sys::redirect($url);
		}
		self::menuReirect($year, $month,$menu_id);
	}

	static function getArticleAddUrl()
	{
		return sys::rewriteUrl('?mod=main&act=menu_article_edit');
	}

	static function getArticleEditUrl($article_id)
	{
		return sys::rewriteUrl('?mod=main&act=menu_article_edit&article_id='.$article_id);
	}

	static function getArticleDiscussUrl($article_id)
	{
		return sys::rewriteUrl('?mod=main&act=menu_article_discuss&article_id='.$article_id);
	}

	static function showArticleTabs($article_id,$article_title,$active_tab=false)
	{
		$var['active']=$active_tab;
		$var['objects']['text']['title']=sys::translate('main::text');
		$var['objects']['text']['url']=self::getArticleUrl($article_id);
		$var['objects']['text']['alt']=$article_title;

		$var['objects']['discuss']['title']=sys::translate('main::discuss');
		$var['objects']['discuss']['url']=self::getArticleDiscussUrl($article_id);
		$var['objects']['discuss']['alt']=$article_title.'::'.sys::translate('main::discuss');

		return sys::parseTpl('main::tabs',$var);
	}

	static function setArticleIndex($article_id,$values)
	{
		global $_db, $_cfg, $_langs;
		$_db->delete('main_menu_index',"`article_id`=".intval($article_id)."");
		foreach ($_langs as $lang_id=>$lang)
		{
			$_db->query("
				INSERT INTO `#__main_menu_index`
				(
					`article_id`,
					`lang_id`,
					`text`
				)
				VALUES
				(
					'".intval($article_id)."',
					'".$lang_id."',
					'".mysql_real_escape_string($values['title_'.$lang['code']])." ".strip_tags(mysql_real_escape_string($values['text_'.$lang['code']]))."'
				)

			");
		}

	}


}
?>