<?php
class main_reviews
{
	static function linkArticleSpecThemes($article_id, $spec_themes)
	{
		global $_db;
		$_db->query("DELETE FROM `#__main_reviews_articles_spec_themes`
			WHERE `article_id`='".intval($article_id)."'");
		if($spec_themes)
		{
			$spec_themes=array_unique($spec_themes);
			foreach ($spec_themes as $spec_themes_id=>$name)
			{
				if($spec_themes_id)
				{
					$_db->query("INSERT INTO `#__main_reviews_articles_spec_themes`
						(
							`article_id`,
							`spec_theme_id`
						)
						VALUES
						(
							".intval($article_id).",
							".intval($spec_themes_id)."
						)
					");
				}
			}
		}
	}

	static function getArticleSpecThemesIds($article_id)
	{
		global $_db;
		$result=array();
		$r=$_db->Query("SELECT `spec_theme_id`
			FROM `#__main_reviews_articles_spec_themes`
			WHERE `article_id`=".intval($article_id)."");
		while(list($id)=$_db->fetchArray($r))
		{
			$result[]=$id;
		}
		return $result;
	}

	static function deleteReview($review_id)
	{
		sys::useLib('main::films');
		sys::useLib('main::discuss');
		global $_db;

		$r=$_db->query("
			SELECT `film_id`, `image`
			FROM `#__main_reviews`
			WHERE `id`='".intval($review_id)."'
		");

		list($film_id,$image)=$_db->fetchArray($r);
		self::deleteReviewImage($image);

		main_discuss::deleteDiscuss($review_id,'review');

		$_db->delete('main_reviews',intval($review_id));
		main_films::countFilmProRating($film_id);
	}

	static function getReviewsNames($arr_ids)
	{
		global $_db;
		$result=array();
		if(!$arr_ids || !count($arr_ids))
			return false;
		for($i=0; $i<count($arr_ids); $i++)
		{
			$arr_ids[$i]=intval($arr_ids[$i]);
		}
		$str_ids=implode(',',$arr_ids);
		$r=$_db->query("SELECT `id`, `name`
			FROM `#__main_reviews`
			WHERE `id` IN(".$str_ids.")");
		while($obj=$_db->fetchAssoc($r))
		{
			$result[$obj['id']]=$obj['name'];
		}
		return $result;
	}

	static function uploadReviewImage($file,$review_id,$current_image=false)
	{
		global $_db, $_cfg;
		if($current_image)
			self::deleteReviewImage($current_image);

		if($image=sys::uploadFile($file,$_cfg['main::reviews_dir'],'review_'.$review_id, true))
		{
		// inserted by Mike start
			if(is_file($_cfg['main::reviews_dir'].$image))
			{	
				$image_info = getImageSize($_cfg['main::reviews_dir'].$image);
				$from_width = $image_info[0];
				$from_height = $image_info[1];
				
				if(($from_width>$_cfg['main::max_width']) || ($from_height>$_cfg['main::max_height']))
					sys::resizeImage($_cfg['main::reviews_dir'].$image,false,$_cfg['main::max_width'], $_cfg['main::max_height']);
			}
		// inserted by Mike end

			$_db->setValue('main_reviews','image',$image,$review_id);
			self::resizeReviewImage($image);
		}
		return $image;
	}

	static function resizeReviewImage($filename)
	{
		global $_cfg;
		sys::resizeImage($_cfg['main::reviews_dir'].$filename,$_cfg['main::reviews_dir'].'x0_'.$filename,$_cfg['main::x0_width'],$_cfg['main::x0_height'],true);
		sys::resizeImage($_cfg['main::reviews_dir'].$filename,$_cfg['main::reviews_dir'].'x1_'.$filename,$_cfg['main::x1_width'],$_cfg['main::x1_height']);
		sys::resizeImage($_cfg['main::reviews_dir'].$filename,$_cfg['main::reviews_dir'].'x2_'.$filename,$_cfg['main::x2_width'],$_cfg['main::x2_height']);
		sys::resizeImage($_cfg['main::reviews_dir'].$filename,$_cfg['main::reviews_dir'].'x3_'.$filename,$_cfg['main::x3_width'],$_cfg['main::x3_height'],true);
	}

	static function deleteReviewImage($filename)
	{
		global $_cfg;
		if(is_file($_cfg['main::reviews_dir'].$filename))
			unlink($_cfg['main::reviews_dir'].$filename);
		if(is_file($_cfg['main::reviews_dir'].'x0_'.$filename))
			unlink($_cfg['main::reviews_dir'].'x0_'.$filename);
		if(is_file($_cfg['main::reviews_dir'].'x1_'.$filename))
			unlink($_cfg['main::reviews_dir'].'x1_'.$filename);
		if(is_file($_cfg['main::reviews_dir'].'x2_'.$filename))
			unlink($_cfg['main::reviews_dir'].'x2_'.$filename);
		if(is_file($_cfg['main::reviews_dir'].'x3_'.$filename))
			unlink($_cfg['main::reviews_dir'].'x3_'.$filename);
	}

	static function showTopRevews()
	{
		global $_cfg, $_db;
		$start_date=gmdate('Y-m-d H:i:s',time()-7*_DAY);
		$cache_name='main_reviews_top_'.$_cfg['sys::lang'].'.html';

		if($cache=sys::getCache($cache_name,$_cfg['main::top_cache_period']*_HOUR))
			return $cache;
		$r=$_db->query("
			SELECT rev.title, rev.id AS `id`, flm.title AS `film`, COUNT(msg.id) AS `num`
			FROM `#__main_discuss_messages` AS `msg`
			LEFT JOIN `#__main_reviews` AS `rev`
			ON rev.id=msg.object_id

			LEFT JOIN `#__main_films_lng` AS `flm`
			ON flm.record_id=rev.film_id
			AND flm.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE msg.date>'".$start_date."' AND msg.object_type='review'

			GROUP BY msg.object_id

			ORDER BY `num` DESC, `id` DESC

			LIMIT 0,6
		");

		$var['objects']=array();
		while($obj=$_db->fetchAssoc($r))
		{
			$obj['url']=self::getReviewUrl($obj['id']);
			$obj['alt']=sys::translate('main::review_on_film').' &laquo;'.htmlspecialchars($obj['film']).'&raquo;';
			$var['objects'][]=$obj;
		}
		$var['title']=sys::translate('main::reviews');
		$var['class']='graph2';

		$result=sys::parseTpl('main::week_top',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function translitIt($str)
	{
	    $tr = array(
	        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
	        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
	        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
	        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
	        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
	        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
	        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
	        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
	        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
	        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
	        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
	        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
	        "the"=>"","the "=>""," the"=>""," the "=>"-",
	        "The"=>"","The "=>""," The"=>""," The "=>"-",
	        "... "=> "", " - "=> "-"," "=> "-", "."=> "", "/"=> "-"
	    );
	    return strtr($str,$tr);
	}

	static function getReviewUrl($review_id, $lang=false)
	{
		global $_cfg, $_db;

		if(!$lang) $lang=$_cfg['sys::lang'];
		
		if ($lang=='ru') 	$lang="";
		else 				$lang="$lang/";

		$r=$_db->query("
			SELECT flm.name, flm.title_orig
			FROM `#__main_reviews` as rvw
			LEFT JOIN `#__main_films` as flm
			ON rvw.film_id=flm.id
			WHERE rvw.id='".$review_id."'
		");

		if($obj=$_db->fetchAssoc($r))
		{
		    $urlstr = $obj['title_orig']?self::translitIt(trim($obj['title_orig'])):self::translitIt(trim($obj['name']));
		    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);
			return _ROOT_URL.$lang.'review/'.$urlstr.'-'.$review_id.'.phtml';
		}
		return sys::rewriteUrl('?mod=main&act=review&review_id='.$review_id);
	}

	static function getReviewUkUrl($review_id)
	{
		if ($review_id>1680)
		{
		global $_cfg, $_db;

		$r=$_db->query("
			SELECT flm.name, flm.title_orig
			FROM `#__main_reviews` as rvw
			LEFT JOIN `#__main_films` as flm
			ON rvw.film_id=flm.id
			WHERE rvw.id='".$review_id."'
		");



		while($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig'])
			{
			    $urlstr = self::translitIt($obj['title_orig']);
			} else {
			    $urlstr = self::translitIt($obj['name']);
			}
			    $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

		}



		$lang='uk';

		return _ROOT_URL.$lang.'/review/films/'.$urlstr.'-'.$review_id.'.phtml';
		} else {
			return _ROOT_URL.'uk/main/review/review_id/'.$review_id.'.phtml';
		}
	}

	static function getOldReviewUrl($review_id)
	{

			return sys::rewriteUrl('?mod=main&act=review&review_id='.$review_id);
	}

	static function getReviewDiscussUrl($review_id)
	{
		return sys::rewriteUrl('?mod=main&act=review&review_id='.$review_id);
	}

	static function getReviewAddUrl($film_id)
	{
		return sys::rewriteUrl('?mod=main&act=review_edit&film_id='.$film_id);
	}

	static function getReviewEditUrl($review_id)
	{
		return sys::rewriteUrl('?mod=main&act=review_edit&review_id='.$review_id);
	}


	static function showLastEditorsReviews()
	{
		sys::useLib('main::films');
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_new_reviews_'.__FUNCTION__."_".$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::new_reviews_cache_period']*60))
		 return $cache;
		$r=$_db->query("SELECT 
					rev.id, 
					rev.date, 
					rev.intro, 
					rev.title, 
					rev.film_id, 
					rev.image,
					rev.total_shows,
					rev.comments,
				 	flm_lng.title AS `film`,
					spec.spec_theme_id as spec_theme
				
				FROM `#__main_reviews` AS `rev`
				LEFT JOIN `#__main_films_lng` AS `flm_lng`
				ON flm_lng.record_id=rev.film_id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
				LEFT JOIN `#__sys_users_groups` AS `users` ON rev.user_id=users.user_id
				LEFT JOIN `#__main_reviews_articles_spec_themes` AS `spec` ON rev.id=spec.article_id
				
				WHERE rev.public=1
					AND (users.group_id=10 OR users.group_id=20)
					AND rev.date<'".gmdate('Y-m-d H:i:s')."'
				    AND rev.date>'".gmdate('Y-m-d H:i:s', strtotime('- 30 days'))."'
				ORDER BY rev.date DESC LIMIT 0,5
			");
		$i=1;

		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['shows']=$obj['total_shows'];
			$obj['intro']=sys::cutString(strip_tags($obj['intro']),140);
			$obj['film']=htmlspecialchars($obj['film']);
	
			// inserted by Mike begin
	  	    $image="x".(($i==1)?3:0)."_review_".$obj['id'].".jpg";
			if(!file_exists('./public/main/reviews/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::reviews_url'].$image;
			$obj['image'] = $image;
			// inserted by Mike end

			$obj['url']=self::getReviewUrl($obj['id']);
			$obj['spec_theme_url']=main_spec_themes::getArticleUrl($obj['spec_theme']);
			$obj['alt']=sys::translate('main::review_on_film').' &quot;'.$obj['film'].'&quot;';
			$obj['film_alt']=sys::translate('main::film').' &quot;'.$obj['film'].'&quot;';
			$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';

			if ($i==3 || $i==5)
			{
			$obj['bottom']=' bottomNews';
			}

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::new_reviews',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function showReviewMedia($film_id, $film_title)
	{
		sys::useLib('main::films');
		global $_db, $_cfg;
		$var['title']=$film_title;

		$r=$_db->query("
			SELECT
				flm.title_orig,
				flm.title_alt,
				flm_lng.title,
				flm.year,
				flm.budget,
				flm.budget_currency,
				flm.duration,
				flm.ukraine_premiere,
				flm.world_premiere,
				flm.yakaboo_url,
				flm.age_limit,
				flm_lng.intro,
				flm_lng.text,
				flm.pre_rating,
				flm.pre_votes,
				flm.rating,
				flm.votes,
				flm.pro_rating,
				flm.pro_votes,
				flm.comments AS `comments_num`,
				flm.banner
			FROM `#__main_films` AS `flm`

			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=flm.id
			AND flm_lng.lang_id=".$_cfg['sys::lang_id']."

			WHERE flm.id=".intval($film_id)."

			LIMIT 1
		");

		$result=$_db->fetchAssoc($r);

	$d=$_db->query("
		SELECT
			rev.mark
		FROM `#__main_reviews` AS `rev`

		WHERE rev.id=".intval($_GET['review_id'])."
	");

	$data=$_db->fetchAssoc($d);

	$var['mark'] = $data['mark'];
	$var['year'] = $result['year'];
		$poster=main_films::getFilmFirstPoster($film_id);

		if($poster)
		{
	  	    $image="x1_".$poster['image']; 
			if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::films_url'].$image;
			$var['poster']['url']=main_films::getFilmUrl($film_id);
		} else {
         	$image = $_cfg['sys::root_url'].'blank_news_img.jpg';         	
		}

		$var['actors']=main_films::getFilmActors($film_id);
		$var['directors']=main_films::getFilmDirectors($film_id);
//		$var['poster']['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=7';
		$var['poster']['image'] = $image;

		$rat="
			SELECT
				pre_mark
			FROM `#__main_films_rating`

			WHERE
				film_id = '".intval($film_id)."'
		";

		$b=$_db->query($rat);

		$yes = 0;
		$no = 0;
		$total = 0;
		while($obj=$_db->fetchAssoc($b))
		{
			if ($obj['pre_mark']>6)
			{
				$yes++;
			} else {
				$no++;
			}

			$total++;
		}

		if ($total>0)
		{
			$var['pre'] = 1;
			$var['yes'] = round($yes/$total*100);
			$var['no'] = 100 - $var['yes'];
		} else {
			$var['pre'] = 1;
			$var['yes'] = 0;
			$var['no'] = 100;
		}

		$p_date=$result['ukraine_premiere'];

		$p_date=strtotime($p_date);

		if($p_date>time())
		{
			$pre=true;
			$showrat = 0;
		}
		else
		{
			$pre=false;
			$showrat = 1;
		}

		if($pre)
		{
			$var['rating']=$var['yes'].'%';
			$var['votes']=$result['pre_votes'];
		}
		else
		{
			$var['rating']=str_replace('.',',',round($result['rating'],1));
			$var['votes']=$result['votes'];
		}

		$var['pre'] = $showrat;


		return sys::parseTpl('main::review_media',$var);

	}

	static function showLastUsersReviews()
	{
		sys::useLib('main::films');
		global $_db, $_cfg;
		$var['objects']=array();
		$cache_name='main_new_reviews_'.__FUNCTION__."_".$_cfg['sys::lang'].'.html';
		if($cache=sys::getCache($cache_name,$_cfg['main::new_reviews_cache_period']*60))
			return $cache;
		$r=$_db->query("SELECT rev.id, rev.date, rev.intro, rev.title, rev.film_id,	rev.total_shows,
			rev.comments, rev.image, flm_lng.title AS `film`
				FROM `#__main_reviews` AS `rev`
				LEFT JOIN `#__main_films_lng` AS `flm_lng`
				ON flm_lng.record_id=rev.film_id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
				LEFT JOIN `#__sys_users_groups` AS `users` ON rev.user_id=users.user_id
				WHERE rev.public=1
				AND rev.date<'".gmdate('Y-m-d H:i:s')."'
			    AND rev.date>'".gmdate('Y-m-d H:i:s', strtotime('- 30 days'))."'
				ORDER BY rev.total_shows DESC LIMIT 0,5
			");

		$i=1;

		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['nr']=$i;
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['shows']=$obj['total_shows'];
			$obj['intro']=sys::cutString(strip_tags($obj['intro']),140);
			$obj['film']=htmlspecialchars($obj['film']);
	
	  	    $image="x".(($i==1)?3:0)."_review_".$obj['id'].".jpg";
			if(!file_exists('./public/main/reviews/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::reviews_url'].$image;
			$obj['image'] = $image;

			$obj['url']=self::getReviewUrl($obj['id']);
			$obj['alt']=sys::translate('main::review_on_film').' &quot;'.$obj['film'].'&quot;';
			$obj['film_alt']=sys::translate('main::film').' &quot;'.$obj['film'].'&quot;';
			$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
			$obj['date']=sys::russianDate($obj['date']);
			$obj['bottom']='';

			if ($i==3 || $i==5)
			{
			$obj['bottom']=' bottomNews';
			}

			$var['objects'][]=$obj;
			$i++;
		}
		$result=sys::parseTpl('main::new_reviews',$var);
		sys::setCache($cache_name,$result);
		return $result;
	}

	static function confirmReview($review_id)
	{
		global $_db;
		$_db->setValue('main_films_reviews','public',1,intval($review_id));
	}

	static function isArticleDiscuss($review_id)
	{
		global $_db, $_cfg;
		$r=$_db->query("
			SELECT COUNT(*) as count
			FROM `#__main_discuss_messages`

			WHERE object_type='review' AND object_id='".$review_id."'
		");
		while ($obj=$_db->fetchAssoc($r))
		{
			$count = $obj['count'];
		}

		return $count;
	}

	static function showReviewTabs($review_id,$review_title,$active_tab=false)
	{
			if(self::isArticleDiscuss($review_id))
		{
		$var['active']=$active_tab;
		$var['objects']['text']['title']=sys::translate('main::text');
		$var['objects']['text']['url']=self::getReviewUrl($review_id);
		$var['objects']['text']['alt']=$review_title;

		$var['objects']['discuss']['title']=sys::translate('main::discuss');
		$var['objects']['discuss']['url']=$var['objects']['text']['url']."#discuss";
   	  //$var['objects']['discuss']['url']=self::getArticleDiscussUrl($article_id)."#discuss";
		$var['objects']['discuss']['alt']=$review_title.'::'.sys::translate('main::discuss');

		return sys::parseTpl('main::tabs',$var);
		}
	}
}
?>