<?php

class main_logging {
	
	public static $access_ip = '92.255.216.255';
	public static $logging_path = '/logs/';
	public static $use_logging = true;
	
	public static function init(){
		if($_SERVER['REMOTE_ADDR'] == self::$access_ip && self::$use_logging)
			return true;
		return false;
	}
	
	public static function write_query($query,$params = array()){
		if(!self::init())
			return false;
//		if($_SERVER['REMOTE_ADDR'])
		$fh = fopen($_SERVER['DOCUMENT_ROOT'].self::$logging_file,'w');
		fwrite($fh,'-----------------------------\n');
		fwrite($fh,'Time: '.date('Y-m-d H:i:s').'\n');
		if(isset($params['library']))
			fwrite($fh,'Library: '.$params['library'].'\n');
		if(isset($params['function']))
			fwrite($fh,'Function: '.$params['function'].'\n');
		if(isset($params['str']))
			fwrite($fh,'Str: '.$params['str'].'\n');
		fwrite($fh,'Query: '.$query.'\n');
		fwrite($fh,'-----------------------------\n');
		fclose($fh);

	}
	
	public static function write_megakino_queries($method,$request,$response){
		$log_name = self::get_log_name();
		$path = $_SERVER['DOCUMENT_ROOT'].self::$logging_path.$log_name;
		$fh = fopen($path,'a');
		fwrite($fh,"-----------------------------\n");
		fwrite($fh,"Time: ".date('Y-m-d H:i:s')."\n");
//		if(isset($params['library']))
		fwrite($fh,"Method: ".$method."\n");
		fwrite($fh,"Request: ".$request."\n");
		fwrite($fh,"Response: ".$response."\n");
		fwrite($fh,"-----------------------------\n");
		fclose($fh);
	}
	
	public static function get_log_name(){
		$name = date('m').'_'.date('Y').'.log';
		return $name;
	}
}

?>