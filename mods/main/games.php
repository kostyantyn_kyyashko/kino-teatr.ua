<?
function main_games()
{
	global $_db, $_cfg;

//	if(!sys::isDebugIP()) return 404;

/*	$obj["title"] = "Goodgame Empire: Four Kingdoms";
	$obj["game_type"] = "Multiplayer Empire Mobile Game";
	$obj["url"] = "game.phtml?game=1";
	$obj["image"] = "http://media.goodgamestudios.com/www/publishers/images/e4k_180x135.jpg";
	$obj["intro"] = "<b>Category:</b> Android, iOS, Action, Multiplayer, Strategy, Roleplay, Adventure, Game for Boys, Simulation, Middle Ages<br>
					 <b>Description:</b> Prepare for battle! Construct fortresses to last for eternity, capture outposts and establish a flourishing economy. Send out your armies to reduce the castles of enemy players to rubble. Build your mighty empire and compete with thousands of other players. Play now in the huge and visually unique world of Empire: Four Kingdoms!";
	$result['objects'][] = $obj;
*/	
	$obj["title"] = "Goodgame Empire";
	$obj["game_type"] = "Multiplayer Empire Game";
	$obj["url"] = "game.phtml?game=2";
	$obj["image"] = "http://media.goodgamestudios.com/www/publishers/images/empire180x135.jpg";
	$obj["intro"] = "<b>Категория:</b> Action, Multiplayer, Strategy, Roleplay, Adventure, Game for Boys, Simulation, Middle Ages <br>
					 <b>Описание:</b> Строй свою собственную империю, расширяй ее и защищай от других игроков. Более 12-ти миллионов игроков по всему миру играют в Goodgame Empire. В 2012-м году, стратегическая онлайн-игра стала победителем присуждаемой общественностью награды European Games Award в категории «лучшая браузерная игра». Предусмотрительные действия, правильно выбранная боевая стратегия и сильные альянсы с другими игроками помогут росту и процветанию твоей империи. Регулярно выпускаемые обновления, расширения и события гарантируют динамический игровой опыт на протяжении долгих месяцев. Присоединяйся к этому огромному сообществу и правь собственной империей в игре Goodgame Empire.";	
	$result['objects'][] = $obj;
	
	$obj["title"] = "Goodgame Big Farm";
	$obj["game_type"] = "Multiplayer Farm Game";
	$obj["url"] = "game.phtml?game=3";
	$obj["image"] = "http://media.goodgamestudios.com/www/publishers/images/goodgame-bigfarm-publishers-thumb.jpg";
	$obj["intro"] = "<b>Категория:</b> Simulation, Multiplayer, Game for Girls, Game for Boys, Roleplay, Time Management, Fun, Farm Game, Strategy <br>
					 <b>Описание:</b> Дядя Джордж оставил тебе свою ферму, но, к сожалению, она не в очень хорошем состоянии. Но благодаря твоей деловой хватке и помощи соседей, друзей и родных ты в состоянии превратить захиревшее хозяйство в прекрасную и цветущую ферму";
	$result['objects'][] = $obj;
	
	$obj["title"] = "Goodgame Poker";
	$obj["game_type"] = "Multiplayer Poker Game";
	$obj["url"] = "game.phtml?game=4";
	$obj["image"] = "http://media.goodgamestudios.com/www/publishers/images/goodgamepoker2-thumb.jpg";
	$obj["intro"] = "<b>Категория:</b> Cards, Multiplayer, Action, Social Games, Texas Hold’em<br>
					 <b>Описание:</b> Увлекательная карточная онлайн игра, в которой вас ожидает невероятный мир покера. В игре вы не только сможете поиграть в свой любимый покер с игроками со всего мира, но и научитесь играть как самый настоящий профессионал в техасский холдем. Множество настраиваемых аватаров, тонны уникальных предметов, огромное количество разнообразных турниров, возможность играть с игроками из других стран, отличное обучающее вступление, всё это и многое-многое другое ждёт вас в браузерной онлайн игре \"Goodgame Poker\".";
	$result['objects'][] = $obj;
	
	$meta['title']=sys::translate('main::games_online');
	$result['meta']=sys::parseModTpl('main::games','page',$meta);
	
	return $result;
}
?>