<?
function main_film_photo()
{
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('photo_id','int');
	
	if(!$_GET['photo_id'])
		return 404;

		$q="
			SELECT 
				pht.film_id,
				pht.image,
				flm_lng.title AS `film`
			FROM `#__main_films_photos` AS `pht`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=pht.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE pht.id=".intval($_GET['photo_id'])."
			AND pht.public=1
		";
	
	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);
	
	
	if(!$result)
		return 404;
		
	main::countShow($result['film_id'],'film');
		
	$meta['film']=$result['film'];	
	$result['meta']=sys::parseModTpl('main::film_photo','page',$meta);
	
	$result['image']=$_cfg['main::films_url'].$result['image'];
	
	return $result;
}
?>