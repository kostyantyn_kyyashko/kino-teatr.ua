<?
function main_getFilm()
{
	$ips=file(_SECURE_DIR.'export.txt');
	foreach ($ips as $ip)
	{
		$ip=explode('#',$ip);
		$ips2[]=trim($ip[0]);
	}

/*	if(!in_array($_SERVER['REMOTE_ADDR'],$ips2))
		return 404; */
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	$result=array();


	$film = $_GET['film'];

	$film = str_replace('+', ' ', $film);
	$film = str_replace('&1;', "?", $film);
	$film_parts = explode('?', $film);



	//Показы-----------------------------------------------------------------
	$qf = "
			SELECT
			flm.id, flm.title_orig, flm.ukraine_premiere, flm.duration, flm.year,
			ru.title as title_ru, ru.intro as intro_ru, ru.text as text_ru,
			ua.title as title_ua, ua.intro as intro_ua, ua.text as text_ua
		FROM
			`#__main_films` `flm`
		LEFT JOIN
			`#__main_films_lng` `ru`
		ON
			flm.id = ru.record_id
		AND
			ru.lang_id = 1
		LEFT JOIN
			`#__main_films_lng` `ua`
		ON
			flm.id = ua.record_id
		AND
			ua.lang_id = 3
		WHERE   ";

		if (count($film_parts)>1)
		{
			$qf .= "flm.title_orig LIKE '".$film_parts[0]."%'
			AND
				flm.title_orig LIKE '%".$film_parts[1]."'
			";
		} else {
			$qf .= "( flm.title_orig='".$film."' OR flm.title_orig LIKE '".$film."%')";
		}

		$qf .= ' ORDER BY flm.year DESC LIMIT 0,1';

	$r=$_db->query($qf);

	$var['films']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		if ($obj['id'])
		{			$id = 	$obj['id'];
		} else {			$id = 0;
		}
		$var['films'][1]=$obj;
	}

	if ($id)
	{		$r=$_db->query("
			SELECT
				image
			FROM
				`#__main_films_photos`
			WHERE
				film_id='".$id."'
			ORDER BY order_number
			LIMIT 1
		");

		while ($obj=$_db->fetchAssoc($r))
		{
			if ($obj['image'])
			{
				$image = $obj['image'];
			}
		}
	}

	if ($id)
	{
		$r=$_db->query("
			SELECT
				image
			FROM
				`#__main_films_posters`
			WHERE
				film_id='".$id."'
			ORDER BY order_number
			LIMIT 1
		");

		while ($obj=$_db->fetchAssoc($r))
		{
			if ($obj['image'])
			{
				$poster = $obj['image'];
			}
		}
	}


	if ($id)
	{
		$q="
				SELECT
					id
				FROM `#__main_films_trailers`
				WHERE
					film_id='".$id."'
				ORDER BY order_number
				LIMIT 1
			";





		$t=$_db->query($q);

		while ($obj=$_db->fetchAssoc($t))
		{
			if ($obj['id'])
			{
				$trailer = $obj['id'];
			}
		}


	}

	if ($id)
	{
		$countries = main_films::getFilmCountriesIds($id);

		$country = implode(',', $countries);

	}


	if ($id)
	{		$actorsru = main_films::getFilmActorsIdsRu($id);

		$actorru = implode(', ', $actorsru);

		$actorsua = main_films::getFilmActorsIdsUa($id);

		$actorua = implode(', ', $actorsua);

		$directorsru = main_films::getFilmDirectorsIdsRu($id);

		$directorru = implode(', ', $directorsru);

		$directorsua = main_films::getFilmDirectorsIdsUa($id);

		$directorua = implode(', ', $directorsua);

		$genres = main_films::getFilmGenresIds($id);

		$genre = implode(',', $genres);


	}





	//=======================================================================

	sys::setTpl();
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header("Content-type: text/xml; charset=cp-1251");
	echo'<?xml version="1.0"?> '
?>
<films>
	<?foreach ($var['films'] as $film):?>
		<event>
			<title_orig><?=$film['title_orig']?></title_orig>
			<ukraine_premiere><?=$film['ukraine_premiere']?></ukraine_premiere>
			<duration><?=$film['duration']?></duration>
			<year><?=$film['year']?></year>
			<title_ru><?=$film['title_ru']?></title_ru>
			<intro_ru><![CDATA[<?=$film['intro_ru']?>]]></intro_ru>
			<text_ru><![CDATA[<?=$film['text_ru']?>]]></text_ru>
			<title_ua><?=$film['title_ua']?></title_ua>
			<intro_ua><![CDATA[<?=$film['intro_ua']?>]]></intro_ua>
			<text_ua><![CDATA[<?=$film['text_ua']?>]]></text_ua>
			<photo><?=$image?></photo>
			<poster><?=$poster?></poster>
			<trailer><?=$trailer?></trailer>
			<country><?=$country?></country>
			<actors_ru><?=$actorru?></actors_ru>
			<actors_ua><?=$actorua?></actors_ua>
			<directors_ru><?=$directorru?></directors_ru>
			<directors_ua><?=$directorua?></directors_ua>
			<genre><?=$genre?></genre>
		</event>

	<?endforeach;?>
</films>
<?
}
