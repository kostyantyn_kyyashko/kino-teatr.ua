<?

function main_user_profile_delivery() {
    
	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;
    
	define('_NOINDEX','1');
	sys::useLib('main::users');

	if($_user['id']==2)
		return 401;
    
	$result['meta'] = sys::parseModTpl('main::user_profile_delivery','page');
    
    $r = $_db->query("
        SELECT
            *
        FROM
            #__main_users_objects_list
        ORDER BY
            weight
    ");
    
    $ids_str = '';
    $ids_list = array();
    $_objs = array();
    
    while ($_obj = $_db->fetchAssoc($r)) {
        $ids_str .= (string)$_obj['id'].',';
        $ids_list[] = $_obj['id'];
        $_objs[] = $_obj;
    }
    
    if ($ids_str) $ids_str = 'AND object_id IN ('.substr($ids_str, 0, strlen($ids_str) - 1).')';
    $r = $_db->query(sprintf("
        SELECT
            *
        FROM
            #__main_users_objects_subscribe
        WHERE
            user_id=%s
            %s
    ", $_user['id'], $ids_str));
    $user_objects = array();
    while ($info = $_db->fetchAssoc($r))
        $user_objects[] = $info['object_id'];
    
    $datetime = new DateTime();
    foreach (array_diff($ids_list, $user_objects) as $id) {
        $query = sprintf("
            INSERT INTO
                #__main_users_objects_subscribe
            (user_id, object_id, w1, w2, w3, w4, w5, w6, w7, last_run)
            VALUES
            (%d, %d, 0, 0, 0, 0, 0, 0, 0, '%s')",
                $_user['id'],
                $id,
                $datetime->format('Y-m-d H:i:s')
            );
        $r = $_db->query($query);
    }
    
    $r = $_db->query(sprintf("
        SELECT
            *
        FROM
            #__main_users_objects_subscribe
        WHERE
            user_id=%s
    ", $_user['id']));
    $shed = array();
    while ($info = $_db->fetchAssoc($r))
        $shed[$info['object_id']] = $info;
    
    reset($_objs);
    foreach ($_objs as $key=>$_obj) {
        
		$_obj['title']=$_db->getValue('main_users_objects_list','title',$_obj['id'],true);
        $csh = $shed[$_obj['id']];
        if ($csh) {
            unset($_obj['weight']);
            $sh = array(
                'w1' => $csh['w1'] ? ' checked="checked"' : '',
                'w2' => $csh['w2'] ? ' checked="checked"' : '',
                'w3' => $csh['w3'] ? ' checked="checked"' : '',
                'w4' => $csh['w4'] ? ' checked="checked"' : '',
                'w5' => $csh['w5'] ? ' checked="checked"' : '',
                'w6' => $csh['w6'] ? ' checked="checked"' : '',
                'w7' => $csh['w7'] ? ' checked="checked"' : ''
            );
            $_obj['sheduler'] = $sh;
            $_objs[$key]['sheduler'] = $sh;
            $result['objects'][]=$_obj;
        }
        
    }
        
    $post = array();
    reset($_POST);
    foreach ($_POST as $key=>$val) {
        $name = explode('_', $key);
        if (count($name) == 3 && $name[0] == 'obj') {
            if (preg_match('/^[0-9]+$/', $name[1], $match) && preg_match('/^w[0-9]+$/', $name[2])) {
                $object_id = (int)$name[1];
                $field = $name[2];
                $post[$object_id][] = $field;
            }
        }
    }
	
	if(isset($_POST["cmd_save"]))
	{
	
		    if ($post) // flags present
		    {
		        reset($_objs);
		        foreach ($_objs as $val) {
		            $query = sprintf("
		                UPDATE
		                    #__main_users_objects_subscribe
		                SET
		                    w1 = %d,
		                    w2 = %d,
		                    w3 = %d,
		                    w4 = %d,
		                    w5 = %d,
		                    w6 = %d,
		                    w7 = %d
		                WHERE
		                    user_id = %d AND
		                    object_id = %d
		            ",
		                (int)in_array('w1', $post[$val['id']]),
		                (int)in_array('w2', $post[$val['id']]),
		                (int)in_array('w3', $post[$val['id']]),
		                (int)in_array('w4', $post[$val['id']]),
		                (int)in_array('w5', $post[$val['id']]),
		                (int)in_array('w6', $post[$val['id']]),
		                (int)in_array('w7', $post[$val['id']]),
		                $_user['id'],
		                $val['id']
		            );
		            $r = $_db->query($query);
		        }
		    }
		    else	// flags absent 
		    {
		            $query = sprintf("
		                UPDATE
		                    #__main_users_objects_subscribe
		                SET
		                    w1 = 0,
		                    w2 = 0,
		                    w3 = 0,
		                    w4 = 0,
		                    w5 = 0,
		                    w6 = 0,
		                    w7 = 0
		                WHERE
		                    user_id = %d
		            ", $_user['id']);
		            $_db->query($query);
		    }

		    sys::redirect(main_users::getProfileDeliveryUrl(),false);
	}    
	return $result;
    
}

?>