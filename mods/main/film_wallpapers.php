<?
function main_film_wallpapers()
{
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');
	sys::filterGet('wallpaper_id','int');
	sys::filterGet('page','int');

	if(!$_GET['film_id'] && !$_GET['wallpaper_id'])
		return 404;

	$cache_name=__FUNCTION__.'_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,_HOUR))
		return unserialize($cache);
		
		$q="
			SELECT
				wlp.film_id,
				wlp.id AS `wallpaper_id`,
				wlp.image,
				wlp.image_1024x768,
				wlp.image_1280x800,
				wlp.image_1280x1024,
				wlp.image_1680x1050,
				wlp.user_id,
				wlp.shows,
				wlp.order_number,
				wlp.public,
				flm_lng.title AS `film`
			FROM `#__main_films_wallpapers` AS `wlp`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=wlp.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		";

	if($_GET['wallpaper_id'])
	{
		$q.="
			WHERE wlp.id=".intval($_GET['wallpaper_id'])."
		";
	}
	elseif($_GET['film_id'])
	{
		if($_GET['page'])
			$limit=intval($_cfg['main::images_on_page']*(intval($_GET['page'])-1));
		else
			$limit=0;

		$q.="
			WHERE wlp.film_id='".intval($_GET['film_id'])."'
			AND wlp.public=1
			ORDER BY wlp.order_number DESC
			LIMIT ".$limit.",1
		";
	}

	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);

	$wall_url = sys::getHumanUrl($result['film_id'],$result['film'],"film_wallpapers");

	if(!$result)
		return 404;

	if(!$result['public'] && !sys::checkAccess('main::films_wallpapers_confirm'))
		return 404;

	define('_CANONIC', main_films::getFilmWallpapersUrl(intval($result['film_id'])));

	main::countShow($result['film_id'],'film');

	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::films_wallpapers_delete') || $result['user_id']==$_user['id'])
		{
			main_films::deletewallpaper($result['wallpaper_id']);
			sys::redirect(main_films::getFilmwallpapersUrl($result['film_id']),false);
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::films_wallpapers_confirm'))
	{
		main_films::confirmwallpaper($result['wallpaper_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}

	if($_cookie)
	{
		$_db->query("
			UPDATE `#__main_films_wallpapers`
			SET `shows`=(`shows`+1)
			WHERE `id`=".doubleval($result['wallpaper_id'])."
		");
	}
	$meta['film']=$result['film'];
	$meta['wallpaperid']=$result['wallpaper_id'];
	$result['title'] = sys::translate('main::film_wallpapers').' '.$result['film'];
	$meta['persons']=false;
	$result['persons']=main_films::getwallpaperPersons($result['wallpaper_id']);

// inserted by Mike begin
  	    $image=$result['image']; 
		if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$result['image'] = $image;
// inserted by Mike end
/*		// comments by Mike
		if($result['image'])
			$image=$_cfg['main::films_url'].$result['image'];
		else
         	$image = 'blank_news_img.jpg';
		$result['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=10';
*/
	if($result['image_1024x768'])
		$result['image_1024x768']=$_cfg['main::films_url'].$result['image_1024x768'];

	if($result['image_1280x800'])
		$result['image_1280x800']=$_cfg['main::films_url'].$result['image_1280x800'];

	if($result['image_1280x1024'])
		$result['image_1280x1024']=$_cfg['main::films_url'].$result['image_1280x1024'];

	if($result['image_1680x1050'])
		$result['image_1680x1050']=$_cfg['main::films_url'].$result['image_1680x1050'];

	$result['next_url']=false;
	$result['prev_url']=false;
	$result['delete_wallpaper']=false;
	$result['edit_url']=false;
	$result['add_url']=false;
	$result['alt']=false;

	foreach ($result['persons'] as $person)
	{
		$result['alt'].=$person['fio'].', ';
	}

	if($result['alt'])
	{
		$meta['persons']=sys::cutStrRight($result['alt'],2);
		$result['alt']=sys::cutStrRight($result['alt'],2);
		$result['alt']=$result['alt'].' '.sys::translate('main::in_film').' &quot;'.htmlspecialchars($result['film']).'&quot';
	}

	$result['meta']=sys::parseModTpl('main::film_wallpapers','page',$meta);

	if($next_id=main_films::getNextwallpaperId($result['film_id'],$result['order_number']))
		$result['next_url']=main_films::getwallpapersUrl($next_id);
	if($prev_id=main_films::getPrevwallpaperId($result['film_id'],$result['order_number']))
		$result['prev_url']=main_films::getwallpapersUrl($prev_id);

	if(sys::checkAccess('main::films_wallpapers_edit') || $result['user_id']==$_user['id'])
		$result['edit_url']=main_films::getwallpaperEditUrl($result['wallpaper_id']);

	if(sys::checkAccess('main::films_wallpapers_add'))
		$result['add_url']=main_films::getwallpaperAddUrl($result['film_id']);

	if(sys::checkAccess('main::films_wallpapers_delete') || $result['user_id']==$_user['id'])
		$result['delete_wallpaper']=true;
	//=============================================================================

	//Получить инфу об остальных фотках-----------------------------------------------
	$q="
		SELECT `id`,`image`
		FROM `#__main_films_wallpapers`
		WHERE `film_id`=".$result['film_id']."
		AND `public`=1
		ORDER BY `order_number` DESC
	";
	$result['pages']=sys_pages::pocess($q,$_cfg['main::images_on_page'],false,'?mod=main&act=film_wallpapers&film_id='.$result['film_id'].'&page=%page%');
	$r=$_db->query($q);
	$result['previews']=array();
	$i=0;
	while ($obj=$_db->fetchAssoc($r))
	{
  	    $image="x3_".$obj['image']; 
		if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$obj['image'] = $image;

		if ($image_info[1]<145)
		{
			$pad = round((145-$image_info[1])/2);
		} else {
			$pad = 0;
		}

		$obj['padding']=$pad;
		$obj['break']=$i%4;
		$obj['i']=$i;
		$obj['url']=$wall_url."?wallpaper_id=".$obj['id'];
		if($_GET['page'])
			$obj['url'].='&page='.$_GET['page'];
		$result['previews'][]=$obj;
		$i++;
	}
	//============================================================

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>