<?
function main_import()
{
	exit();
	global $_db, $_cfg, $_err, $_user, $_langs;
	sys::filterGet('mode');
	
	$result=array(0);
	
	$db_host='localhost';
	$db_name='kino_old';
	$db_name2='kino_old_new';
	$db_user='root';
	$db_pass='ZaoTioRaLaMK3';
	
	
	$img_dir=_PUBLIC_DIR.'img/';
	set_time_limit(0);
	//Жанры
	if($_GET['mode']=='genres' || $_GET['mode']=='all')
	{
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT 
				`id`,
				`name`,
				`pos`
			FROM `genre`
		");
		$_db->connect();
		while ($obj=$_db->fetchAssoc($r))
		{	
			$_db->query("
				INSERT INTO `#__main_genres`
				(
					`id`,
					`name`,
					`order_number`
				)	
				VALUES
				(
					'".intval($obj['id'])."',
					'".mysql_real_escape_string($obj['name'])."',
					'".intval($obj['pos'])."'
				)
			");
			foreach ($_langs as $lang_id=>$lang)
			{
				$_db->query("
					INSERT INTO `#__main_genres_lng`
					(
						`record_id`,
						`lang_id`,
						`title`
					)
					VALUES
					(
						'".intval($obj['id'])."',
						'".intval($lang_id)."',
						'".mysql_real_escape_string($obj['name'])."'
					)
				");
			}
			
		}
		
	}
	
	//Страны
	if($_GET['mode']=='countries' || $_GET['mode']=='all')
	{
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT 
				`id`,
				`name`,
				`pos`
			FROM `country`
		");
		$_db->connect();
		while ($obj=$_db->fetchAssoc($r))
		{	
			$_db->query("
				INSERT INTO `#__main_countries`
				(
					`id`,
					`name`,
					`order_number`
				)	
				VALUES
				(
					'".intval($obj['id'])."',
					'".mysql_real_escape_string($obj['name'])."',
					'".intval($obj['pos'])."'
				)
			");
			foreach ($_langs as $lang_id=>$lang)
			{
				$_db->query("
					INSERT INTO `#__main_countries_lng`
					(
						`record_id`,
						`lang_id`,
						`title`,
						`title_where`,
						`title_what`
					)
					VALUES
					(
						'".intval($obj['id'])."',
						'".intval($lang_id)."',
						'".mysql_real_escape_string($obj['name'])."',
						'".mysql_real_escape_string($obj['name'])."',
						'".mysql_real_escape_string($obj['name'])."'
					)
				");
			}
			
		}
		
	}
	
	
	//Города
	if($_GET['mode']=='cities' || $_GET['mode']=='all')
	{
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT 
				cit.id,
				cit.pos,
				lng_rus.name AS `name_rus`,
				lng_ukr.name AS `name_ukr`
			FROM 
				`city` AS `cit`
				
			LEFT JOIN
				`city_desc` AS `lng_rus`
			ON
				lng_rus.lid=1
			AND
				lng_rus.cid=cit.id
				
			LEFT JOIN
				`city_desc` AS `lng_ukr`
			ON
				lng_ukr.lid=2
			AND
				lng_ukr.cid=cit.id
		");
		$_db->connect();
		while ($obj=$_db->fetchAssoc($r))
		{	
			$_db->query("
				INSERT INTO `#__main_countries_cities`
				(
					`id`,
					`name`,
					`country_id`,
					`order_number`
				)	
				VALUES
				(
					'".intval($obj['id'])."',
					'".mysql_real_escape_string($obj['name_rus'])."',
					'29',
					'".intval($obj['pos'])."'
				)
			");
		
			$_db->query("
				INSERT INTO `#__main_countries_cities_lng`
				(
					`record_id`,
					`lang_id`,
					`title`,
					`title_where`,
					`title_what`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'1',
					'".mysql_real_escape_string($obj['name_rus'])."',
					'".mysql_real_escape_string($obj['name_rus'])."',
					'".mysql_real_escape_string($obj['name_rus'])."'
				)
			");
			
			$_db->query("
				INSERT INTO `#__main_countries_cities_lng`
				(
					`record_id`,
					`lang_id`,
					`title`,
					`title_where`,
					`title_what`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'3',
					'".mysql_real_escape_string($obj['name_ukr'])."',
					'".mysql_real_escape_string($obj['name_ukr'])."',
					'".mysql_real_escape_string($obj['name_ukr'])."'
				)
			");
			
			
		}
		
	}
	
	
	//Дистрибюторы
	if($_GET['mode']=='distributors' || $_GET['mode']=='all')
	{
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT 
				`id`,
				`name`,
				`url`,
				`comments`
			FROM `distributors`
		");
		$_db->connect();
		while ($obj=$_db->fetchAssoc($r))
		{	
			if($obj['url'])
				$obj['url']='http://'.$obj['url'];
			$_db->query("
				INSERT INTO `#__main_distributors`
				(
					`id`,
					`name`,
					`site`,
					`info`
				)	
				VALUES
				(
					'".intval($obj['id'])."',
					'".mysql_real_escape_string($obj['name'])."',
					'".mysql_real_escape_string($obj['url'])."',
					'".mysql_real_escape_string($obj['comments'])."'
				)
			");
			foreach ($_langs as $lang_id=>$lang)
			{
				$_db->query("
					INSERT INTO `#__main_distributors_lng`
					(
						`record_id`,
						`lang_id`,
						`title`
					)
					VALUES
					(
						'".intval($obj['id'])."',
						'".intval($lang_id)."',
						'".mysql_real_escape_string($obj['name'])."'
					)
				");
			}
			
		}
		
	}
	
	//Юзеры 7=107
	if($_GET['mode']=='users' || $_GET['mode']=='all')
	{
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT 
				`id`,
				`name`,
				`email`,
				`info`
			FROM `author`
		");
		$_db->connect();
		while ($obj=$_db->fetchAssoc($r))
		{	
			if($obj['id']==7)
				$obj['id']=107;
			if(!$obj['email'])
				$obj['email']='user_'.$obj['id'];
			$_db->query("
				INSERT INTO `#__sys_users`
				(
					`id`,
					`date_reg`,
					`login`,
					`password`,
					`email`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'".gmdate('Y-m-d H:i:s')."',
					'".mysql_real_escape_string($obj['name'])."',
					'".md5(sys::generatePass())."',
					'".$obj['email']."'
				)
			");
			
			$_db->query("
				INSERT INTO `#__sys_users_data`
				(
					`user_id`,
					`main_info`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'".mysql_real_escape_string($obj['info'])."'
				)
			");
			
			$_db->query("
				INSERT INTO `#__sys_users_groups`
				(
					`user_id`,
					`group_id`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'8'
				)
			");
			
		}
		
	}
	
	//Разделы новостей
	if($_GET['mode']=='news' || $_GET['mode']=='all')
	{
		sys::useLib('main::news');
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT 
				sec.id,
				sec.pos,
				lng_rus.name AS `name_rus`,
				lng_ukr.name AS `name_ukr`
			FROM 
				`news_section` AS `sec`
				
			LEFT JOIN
				`news_section_desc` AS `lng_rus`
			ON
				lng_rus.lang_id=1
			AND
				lng_rus.section_id=sec.id
				
			LEFT JOIN
				`news_section_desc` AS `lng_ukr`
			ON
				lng_ukr.lang_id=2
			AND
				lng_ukr.section_id=sec.id
		");
		$_db->connect();
		while ($obj=$_db->fetchAssoc($r))
		{	
			$_db->query("
				INSERT INTO `#__main_news`
				(
					`id`,
					`name`,
					`order_number`
				)	
				VALUES
				(
					'".intval($obj['id'])."',
					'".mysql_real_escape_string($obj['name_rus'])."',
					'".intval($obj['pos'])."'
				)
			");
		
			$_db->query("
				INSERT INTO `#__main_news_lng`
				(
					`record_id`,
					`lang_id`,
					`title`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'1',
					'".mysql_real_escape_string($obj['name_rus'])."'
				)
			");
			
			$_db->query("
				INSERT INTO `#__main_news_lng`
				(
					`record_id`,
					`lang_id`,
					`title`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'3',
					'".mysql_real_escape_string($obj['name_ukr'])."'
				)
			");
			
		}
		
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT
				nws.id,
				nws.date,
				nws.visible,
				nws.img,
				nws.cid,
				nws.section_id,
				nws.image,
				nws.image_text,
				nws.exclusive,
				nws.source,
				nws.source_url,
				lng_rus.title AS `title_rus`,
				lng_ukr.title AS `title_ukr`,
				lng_rus.announce AS `announce_rus`,
				lng_ukr.announce AS `announce_ukr`,
				lng_rus.text AS `text_rus`,
				lng_ukr.text AS `text_ukr`
			FROM
				`news` AS `nws`
			
			LEFT JOIN
				`news_desc` AS `lng_rus`
			ON
				lng_rus.lid=1
			AND
				lng_rus.nid=nws.id
				
			LEFT JOIN
				`news_desc` AS `lng_ukr`
			ON
				lng_ukr.lid=2
			AND
				lng_ukr.nid=nws.id
				
		");
		
		$_db->connect();
		while($obj=$_db->fetchAssoc($r))
		{
			if(!$obj['section_id'])
				continue;
			if($obj['source_url'] && strpos($obj['source_url'],'http')===false)
				$obj['source_url']='http://'.$obj['source_url'];
				
			$_db->query("
				INSERT INTO `#__main_news_articles`
				(
					`id`,
					`news_id`,
					`user_id`,
					`city_id`,
					`name`,
					`date`,
					`small_image`,
					`image`,
					`source_url`,
					`exclusive`,
					`public`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'".intval($obj['section_id'])."',
					'7',
					'".intval($obj['cid'])."',
					'".mysql_real_escape_string($obj['title_rus'])."',
					'".mysql_real_escape_string($obj['date'])."',
					'".mysql_real_escape_string($obj['img'])."',
					'".mysql_real_escape_string($obj['image'])."',
					'".mysql_real_escape_string($obj['source_url'])."',
					'".mysql_real_escape_string($obj['exclusive'])."',
					'".mysql_real_escape_string($obj['visible'])."'
				)
			");
			
			$_db->query("
				INSERT INTO `#__main_news_articles_lng`
				(
					`record_id`,
					`lang_id`,
					`title`,
					`intro`,
					`source`,
					`alt`,
					`text`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'1',
					'".mysql_real_escape_string($obj['title_rus'])."',
					'".mysql_real_escape_string(strip_tags(mb_substr($obj['announce_rus'],0,155)))."',
					'".mysql_real_escape_string($obj['source'])."',
					'".mysql_real_escape_string($obj['image_text'])."',
					'".mysql_real_escape_string($obj['text_rus'])."'
					
				)
			");
			
			$_db->query("
				INSERT INTO `#__main_news_articles_lng`
				(
					`record_id`,
					`lang_id`,
					`title`,
					`intro`,
					`source`,
					`alt`,
					`text`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'3',
					'".mysql_real_escape_string($obj['title_ukr'])."',
					'".mysql_real_escape_string(strip_tags($obj['announce_ukr']))."',
					'".mysql_real_escape_string($obj['source'])."',
					'".mysql_real_escape_string($obj['image_text'])."',
					'".mysql_real_escape_string($obj['text_ukr'])."'
					
				)
			");
			
			$_db->query("
				INSERT INTO `#__main_news_index`
				(
					`article_id`,
					`lang_id`,
					`text`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'1',
					'".mysql_real_escape_string($obj['title_rus']).strip_tags(mysql_real_escape_string($obj['text_rus']))."'
					
				)
			");
			
			$_db->query("
				INSERT INTO `#__main_news_index`
				(
					`article_id`,
					`lang_id`,
					`text`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'3',
					'".mysql_real_escape_string($obj['title_ukr']).strip_tags(mysql_real_escape_string($obj['text_ukr']))."'
					
				)
			");
			if(is_file($img_dir.'news/'.$obj['image']))
			{
				copy($img_dir.'news/'.$obj['image'],$_cfg['main::news_dir'].$obj['image']);
				main_news::resizeArticleImage($obj['image']);
			}
			if(is_file($img_dir.'news/'.$obj['img']))
				copy($img_dir.'news/'.$obj['img'],$_cfg['main::news_dir'].$obj['img']);
			
		}
		
		
		
	}

	
	//Фильмы
	if($_GET['mode']=='films' || $_GET['mode']=='all')
	{
		sys::useLib('main::films');
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT 
				flm.id,
				flm.name_original,
				flm.name_alt,
				flm.made,
				flm.homepage,
				flm.duration,
				flm.age_limit,
				flm.budget,
				flm.date,
				flm.date_world,
				lng_rus.name AS `name_rus`,
				lng_ukr.name AS `name_ukr`,
				lng_rus.text AS `text_rus`,
				lng_ukr.text AS `text_ukr`,
				lng_rus.press_release AS `press_release_rus`,
				lng_ukr.press_release AS `press_release_ukr` 
				
			FROM
				`film` AS `flm`
				
			LEFT JOIN
				`film_desc` AS `lng_rus`
			ON
				lng_rus.fid=flm.id
			AND
				lng_rus.lid=1
				
			LEFT JOIN
				`film_desc` AS `lng_ukr`
			ON
				lng_ukr.fid=flm.id
			AND
				lng_ukr.lid=2
				
			/*ORDER BY RAND() LIMIT 0,10*/
		");
		
		while ($obj=$_db->fetchAssoc($r)) 
		{
			$obj['name']=$obj['name_original'];
			if(!$obj['name'])
				$obj['name']=$obj['name_alt'];
			if(!$obj['name'])
				$obj['name']=$obj['name_rus'];
			if(!$obj['name'])
				$obj['name']=$obj['name_ukr'];
				
			if(!$obj['name_ukr'])
				$obj['name_ukr']=$obj['name'];
				
			if(!$obj['name_rus'])
				$obj['name_rus']=$obj['name'];
			//Вставка основной инфы
			$_db->connect();
			$_db->query("
				INSERT INTO `#__main_films`
				(
					`id`,
					`name`,
					`title_orig`,
					`title_alt`,
					`world_premiere`,
					`ukraine_premiere`,
					`duration`,
					`year`,
					`age_limit`,
					`budget`,
					`budget_currency`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'".mysql_real_escape_string($obj['name'])."',
					'".mysql_real_escape_string($obj['name_original'])."',
					'".mysql_real_escape_string($obj['name_alt'])."',
					'".mysql_real_escape_string($obj['date_world'])."',
					'".mysql_real_escape_string($obj['date'])."',
					'".intval($obj['duration'])."',
					'".intval($obj['made'])."',
					'".intval($obj['age_limit'])."',
					'".floatval($obj['budget'])."',
					'usd'
				)
			");
			
			$_db->query("
				INSERT INTO `#__main_films_lng`
				(
					`record_id`,
					`lang_id`,
					`title`,
					`intro`,
					`text`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'1',
					'".mysql_real_escape_string($obj['name_rus'])."',
					'".mysql_real_escape_string($obj['press_release_rus'])."',
					'".mysql_real_escape_string($obj['text_rus'])."'
				)
			");
			
			$_db->query("
				INSERT INTO `#__main_films_lng`
				(
					`record_id`,
					`lang_id`,
					`title`,
					`intro`,
					`text`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'3',
					'".mysql_real_escape_string($obj['name_ukr'])."',
					'".mysql_real_escape_string($obj['press_release_ukr'])."',
					'".mysql_real_escape_string($obj['text_ukr'])."'
				)
			");
			
			
			
			//Страны
			$_db->connect($db_host, $db_user, $db_pass, $db_name);
			$r2=$_db->query("
				SELECT
					`cid`
				FROM
					`film_country`
				WHERE
					`fid`=".intval($obj['id'])."
			");
			
			$_db->connect();
			while (list($cid)=$_db->fetchArray($r2)) 
			{
				$_db->query("
					INSERT INTO #__main_films_countries
					(
						`film_id`,
						`country_id`
					)
					VALUES
					(
						'".intval($obj['id'])."',
						'".intval($cid)."'
					)
					
				");
			}
			
			//Дистрибюторы
			$_db->connect($db_host, $db_user, $db_pass, $db_name);
			$r2=$_db->query("
				SELECT
					`did`
				FROM
					`film_distributors`
				WHERE
					`fid`=".intval($obj['id'])."
			");
			
			$_db->connect();
			while (list($did)=$_db->fetchArray($r2)) 
			{
				$_db->query("
					INSERT INTO #__main_films_distributors
					(
						`film_id`,
						`distributor_id`
					)
					VALUES
					(
						'".intval($obj['id'])."',
						'".intval($did)."'
					)
					
				");
			}
			
			//Жанры
			$_db->connect($db_host, $db_user, $db_pass, $db_name);
			$r2=$_db->query("
				SELECT
					`gid`
				FROM
					`film_genre`
				WHERE
					`fid`=".intval($obj['id'])."
			");
			
			$_db->connect();
			while (list($gid)=$_db->fetchArray($r2)) 
			{
				$_db->query("
					INSERT INTO #__main_films_genres
					(
						`film_id`,
						`genre_id`
					)
					VALUES
					(
						'".intval($obj['id'])."',
						'".intval($gid)."'
					)
					
				");
			}
			
			//Фотки
			$_db->connect($db_host, $db_user, $db_pass, $db_name);
			$r2=$_db->query("
				SELECT 
					`id`,
					`pos`,
					`img`
				FROM
					`film_picture`
				WHERE
					`fid`=".intval($obj['id'])."
			");
			
			$_db->connect();
			while ($photo=$_db->fetchAssoc($r2))
			{
				$_db->query("
					INSERT INTO `#__main_films_photos`
					(
						`id`,
						`film_id`,
						`user_id`,
						`image`,
						`order_number`,
						`date`
					)
					VALUES
					(
						'".intval($photo['id'])."',
						'".intval($obj['id'])."',
						'7',
						'".mysql_real_escape_string($photo['img'])."',
						'".intval($photo['pos'])."',
						'".gmdate('Y-m-d H:i:s')."'
					)
				");
				if(is_file($img_dir.'film/'.$photo['img']))
				{
					copy($img_dir.'film/'.$photo['img'],$_cfg['main::films_dir'].$photo['img']);
					main_films::resizePhotoImage($photo['img']);
				}
			}
			
			//Постеры
			$_db->connect($db_host, $db_user, $db_pass, $db_name);
			$r2=$_db->query("
				SELECT 
					`id`,
					`pos`,
					`img`,
					`img_s`
				FROM
					`poster`
				WHERE
					`fid`=".intval($obj['id'])."
			");
			
			$_db->connect();
			while ($poster=$_db->fetchAssoc($r2))
			{
				if(!$poster['img'])
					$poster['img']=$poster['img_s'];
					
				
				$_db->query("
					INSERT INTO `#__main_films_posters`
					(
						`id`,
						`film_id`,
						`user_id`,
						`image`,
						`order_number`,
						`date`
					)
					VALUES
					(
						'".intval($poster['id'])."',
						'".intval($obj['id'])."',
						'7',
						'".mysql_real_escape_string('poster_'.$poster['img'])."',
						'".intval($poster['pos'])."',
						'".gmdate('Y-m-d H:i:s')."'
					)
				");
				if(is_file($img_dir.'poster/'.$poster['img']))
				{
					copy($img_dir.'poster/'.$poster['img'],$_cfg['main::films_dir'].'poster_'.$poster['img']);
					main_films::resizePosterImage('poster_'.$poster['img']);
				}
			}
			
			//Трейлеры
			$_db->connect($db_host, $db_user, $db_pass, $db_name);
			$r2=$_db->query("
				SELECT 
					`id`,
					`pos`,
					`url`
				FROM
					`trailer`
				WHERE
					`fid`=".intval($obj['id'])."
			");
			
			$_db->connect();
			while ($trailer=$_db->fetchAssoc($r2))
			{
				if(strpos($trailer['url'],'http')===0 || strpos($trailer['url'],'ftp')===0)
				{
					$trailer['file']=false;
				}
				else 
				{
					$trailer['file']=basename($trailer['url']);
					$trailer['url']=false;
				}
					
				
				$_db->query("
					INSERT INTO `#__main_films_trailers`
					(
						`id`,
						`film_id`,
						`user_id`,
						`file`,
						`url`,
						`order_number`,
						`date`
					)
					VALUES
					(
						'".intval($trailer['id'])."',
						'".intval($obj['id'])."',
						'7',
						'".mysql_real_escape_string($trailer['file'])."',
						'".mysql_real_escape_string($trailer['url'])."',
						'".intval($trailer['pos'])."',
						'".gmdate('Y-m-d H:i:s')."'
					)
				");
			}
			
		}
	}
	
	//Рецензии
	if($_GET['mode']=='reviews' || $_GET['mode']=='all')
	{
		sys::useLib('main::reviews');
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT 
				`id`,
				`aid`,
				`fid`,
				`name`,
				`annonce`,
				`text`,
				`date`,
				`img`
			FROM `review`
			/*ORDER BY RAND() LIMIT 0,10*/
		");
		$_db->connect();
		while ($obj=$_db->fetchAssoc($r))
		{	
			if($obj['aid']==7)
				$obj['aid']=107;
			$_db->query("
				INSERT INTO `#__main_reviews`
				(
					`id`,
					`film_id`,
					`user_id`,
					`date`,
					`title`,
					`image`,
					`intro`,
					`text`
				)	
				VALUES
				(
					'".intval($obj['id'])."',
					'".intval($obj['fid'])."',
					'".intval($obj['aid'])."',
					'".mysql_real_escape_string($obj['date'])."',
					'".mysql_real_escape_string($obj['name'])."',
					'".mysql_real_escape_string($obj['img'])."',
					'".mysql_real_escape_string(strip_tags($obj['annonce']))."',
					'".mysql_real_escape_string($obj['text'])."'
				)
			");
			
			if(is_file($img_dir.'review/'.$obj['img']))
			{
				copy($img_dir.'review/'.$obj['img'],$_cfg['main::reviews_dir'].$obj['img']);
				main_reviews::resizeReviewImage($obj['img']);
			}
			
		}
	}
	
	//Кинотеатры
	if($_GET['mode']=='cinemas' || $_GET['mode']=='all')
	{
		sys::useLib('main::cinemas');
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT 
				kin.id,
				kin.cid,
				kin.pos,
				kin.telephone,
				kin.map_src,
				kin.url,
				lng_rus.name AS `name_rus`,
				lng_ukr.name AS `name_ukr`,
				lng_rus.address AS `address_rus`,
				lng_ukr.address AS `address_ukr`,
				lng_rus.about AS `about_rus`,
				lng_ukr.about AS `about_ukr`
			FROM
				`kinoteatr` AS `kin`
				
			LEFT JOIN
				`kinoteatr_desc` AS `lng_rus`
			ON
				lng_rus.kid=kin.id
			AND
				lng_rus.lid=1
				
			LEFT JOIN
				`kinoteatr_desc` AS `lng_ukr`
			ON
				lng_ukr.kid=kin.id
			AND
				lng_ukr.lid=2
		");
		while ($obj=$_db->fetchAssoc($r)) 
		{
			$_db->connect();
			if($obj['map_src'])
				$obj['map']='map_'.$obj['map_src'];
			else 
				$obj['map']=false;
			
			$_db->query("
				INSERT INTO `#__main_cinemas`
				(
					`id`,
					`city_id`,
					`name`,
					`site`,
					`map`,
					`phone`,
					`order_number`
				)	
				VALUES
				(
					'".intval($obj['id'])."',
					'".intval($obj['cid'])."',
					'".mysql_real_escape_string($obj['name_rus'])."',
					'".mysql_real_escape_string($obj['url'])."',
					'".mysql_real_escape_string($obj['map'])."',
					'".mysql_real_escape_string($obj['telephone'])."',
					'".intval($obj['pos'])."'
				)
			");
		
			$_db->query("
				INSERT INTO `#__main_cinemas_lng`
				(
					`record_id`,
					`lang_id`,
					`title`,
					`address`,
					`text`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'1',
					'".mysql_real_escape_string($obj['name_rus'])."',
					'".mysql_real_escape_string($obj['address_rus'])."',
					'".mysql_real_escape_string($obj['about_rus'])."'
				)
			");
			
			$_db->query("
				INSERT INTO `#__main_cinemas_lng`
				(
					`record_id`,
					`lang_id`,
					`title`,
					`address`,
					`text`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'3',
					'".mysql_real_escape_string($obj['name_ukr'])."',
					'".mysql_real_escape_string($obj['address_ukr'])."',
					'".mysql_real_escape_string($obj['about_ukr'])."'
				)
			");
			if(is_file($img_dir.'mapa/'.$obj['map_src']))
				copy($img_dir.'mapa/'.$obj['map_src'],$_cfg['main::cinemas_dir'].'map_'.$obj['map_src']);
				
			$_db->connect($db_host, $db_user, $db_pass, $db_name);
			$r2=$_db->query("
				SELECT 
					`id`,
					`pos`,
					`img_src`
				FROM
					`kinoteatr_picture`
				WHERE
					`kid`=".intval($obj['id'])."
			");
			$_db->connect();
			while($picture=$_db->fetchAssoc($r2))
			{
				$_db->query("
					INSERT INTO `#__main_cinemas_photos`
					(
						`id`,
						`cinema_id`,
						`user_id`,
						`image`,
						`order_number`
					)
					VALUES
					(
						'".intval($picture['id'])."',
						'".intval($obj['id'])."',
						'7',
						'".mysql_real_escape_string($picture['img_src'])."',
						'".intval($picture['pos'])."'
					)
				");
				if(is_file($img_dir.'cinema/'.$picture['img_src']))
				{
					copy($img_dir.'cinema/'.$picture['img_src'],$_cfg['main::cinemas_dir'].$picture['img_src']);
					main_cinemas::resizePhotoImage($picture['img_src']);
				}
			}
			
			$_db->connect($db_host, $db_user, $db_pass, $db_name);
			$r2=$_db->query("
				SELECT 
					hal.id,
					hal.pos,
					hal.scheme,
					lng_rus.name AS `name_rus`,
					lng_ukr.name AS `name_ukr`
				FROM
					`hall` AS `hal`
					
				LEFT JOIN
					`hall_desc` AS `lng_rus`
				ON
					lng_rus.hid=hal.id
				AND
					lng_rus.lid=1
					
				LEFT JOIN
					`hall_desc` AS `lng_ukr`
				ON
					lng_ukr.hid=hal.id
				AND
					lng_ukr.lid=2
					
				WHERE hal.kid=".intval($obj['id'])."
					
			");
			
			$_db->connect();
			while($hall=$_db->fetchAssoc($r2))
			{
				if($hall['name_rus'])
					$hall['name']=$hall['name_rus'];
				else 
					$hall['name']='Зал';
					
				$_db->query("
					INSERT INTO `#__main_cinemas_halls`
					(
						`id`,
						`cinema_id`,
						`name`,
						`scheme`,
						`order_number`
					)
					VALUES
					(
						'".intval($hall['id'])."',
						'".intval($obj['id'])."',
						'".mysql_real_escape_string($hall['name'])."',
						'".mysql_real_escape_string($hall['scheme'])."',
						'".intval($hall['pos'])."'
					)
				");
				
				$_db->query("
					INSERT INTO `#__main_cinemas_halls_lng`
					(
						`record_id`,
						`lang_id`,
						`title`
					)
					VALUES
					(
						'".intval($hall['id'])."',
						'1',
						'".mysql_real_escape_string($hall['name_rus'])."'
					)
				");
				
				$_db->query("
					INSERT INTO `#__main_cinemas_halls_lng`
					(
						`record_id`,
						`lang_id`,
						`title`
					)
					VALUES
					(
						'".intval($hall['id'])."',
						'3',
						'".mysql_real_escape_string($hall['name_ukr'])."'
					)
				");
				
				if(is_file($img_dir.'scheme/'.$hall['scheme']))
					copy($img_dir.'scheme/'.$hall['scheme'],$_cfg['main::cinemas_dir'].$hall['scheme']);
			}
		}
	}
	
	//Перосоны
	if($_GET['mode']=='persons' || $_GET['mode']=='all')
	{
		sys::useLib('main::persons');
		$_db->connect($db_host, $db_user, $db_pass, $db_name2);
		$r=$_db->query("
			SELECT 
				prs.id,
				prs.name,
				prs.fio_orig,
				prs.birth_date,
				prs.photo,
				lng_rus.fio AS `fio_rus`,
				lng_ukr.fio AS `fio_ukr`,
				lng_rus.biography AS `biography_rus`,
				lng_ukr.biography AS `biography_ukr`
			FROM
				`kino_site_persons` AS `prs`
				
			LEFT JOIN
				`kino_site_persons_translate` AS `lng_rus`
			ON
				lng_rus.record_id=prs.id
			AND
				lng_rus.lang_id=1
				
			LEFT JOIN
				`kino_site_persons_translate` AS `lng_ukr`
			ON
				lng_ukr.record_id=prs.id
			AND
				lng_ukr.lang_id=4
		");
		
		
		while ($obj=$_db->fetchAssoc($r))
		{
			$_db->connect();
			$fio=explode(',',$obj['name']);
			$obj['lastname']=$fio[0];
			$obj['firstname']=$fio[1];
			
			$fio_rus=explode(' ',$obj['fio_rus']);
			$obj['firstname_rus']=$fio_rus[0];
			$obj['lastname_rus']='';
			for($i=1; $i<count($fio_rus);$i++)
			{
				$obj['lastname_rus'].=$fio_rus[$i].' ';
			}
			$obj['lastname_rus']=trim($obj['lastname_rus']);
			
			
			$fio_ukr=explode(' ',$obj['fio_ukr']);
			$obj['firstname_ukr']=$fio_ukr[0];
			$obj['lastname_ukr']='';
			for($i=1; $i<count($fio_ukr);$i++)
			{
				$obj['lastname_ukr'].=$fio_ukr[$i].' ';
			}
			$obj['lastname_ukr']=trim($obj['lastname_ukr']);
			if($obj['birth_date']=='0000-00-00')
				$obj['birth_date']=false;
			
			$_db->query("
				INSERT INTO `#__main_persons`
				(
					`id`,
					`name`,
					`lastname_orig`,
					`firstname_orig`,
					`birthdate`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'".mysql_real_escape_string($obj['name'])."',
					'".mysql_real_escape_string($obj['lastname'])."',
					'".mysql_real_escape_string($obj['firstname'])."',
					'".mysql_real_escape_string($obj['birth_date'])."'
				)
			");
			
			$_db->query("
				INSERT INTO `#__main_persons_lng`
				(
					`record_id`,
					`lang_id`,
					`lastname`,
					`firstname`,
					`biography`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'1',
					'".mysql_real_escape_string($obj['lastname_rus'])."',
					'".mysql_real_escape_string($obj['firstname_rus'])."',
					'".mysql_real_escape_string(sys::parseBBCode($obj['biography_rus']))."'
				)
				
			");
			
			$_db->query("
				INSERT INTO `#__main_persons_lng`
				(
					`record_id`,
					`lang_id`,
					`lastname`,
					`firstname`,
					`biography`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'3',
					'".mysql_real_escape_string($obj['lastname_ukr'])."',
					'".mysql_real_escape_string($obj['firstname_ukr'])."',
					'".mysql_real_escape_string(sys::parseBBCode($obj['biography_ukr']))."'
				)
				
			");
			
			if(is_file($img_dir.'persons/'.$obj['photo']))
			{
				copy($img_dir.'persons/'.$obj['photo'],$_cfg['main::persons_dir'].$obj['photo']);
				main_persons::resizePhotoImage($obj['photo']);
			}
			
			$_db->query("
				INSERT INTO `#__main_persons_photos`
				(
					`person_id`,
					`user_id`,
					`image`,
					`order_number`
				)
				VALUES
				(
					'".intval($obj['id'])."',
					'7',
					'".$obj['photo']."',
					'1'
				)
			");
			
			
		}
	}
	
	
	
	
	return $result;
	
}
?>