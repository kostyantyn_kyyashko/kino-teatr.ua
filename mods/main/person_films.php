<?
function main_person_films()
{
	sys::useLib('main::persons');
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('person_id','int');

	define('_CANONICAL',main_persons::getPersonFilmsUrl($_GET['person_id']),true);

	if(!$_GET['person_id'])
		return 404;

	$result['person']=main_persons::getPersonFio($_GET['person_id']);
	$result['person_id']=doubleval($_GET['person_id']);
	if(!$result['person'])
		return 404;
	$result['title'] = sys::translate('main::filmography').' '.$result['person'];

	$meta['person']=$result['person'];
	$result['meta']=sys::parseModTpl('main::person_films','page',$meta);

	$r=$_db->query("
		SELECT
			prf_lng.title AS `profession`,
			flm_prs.profession_id,
			flm_prs_lng.role,
			flm.year,
			flm.title_orig,
			flm.id AS `film_id`,
			flm_lng.title AS `film`
		FROM
			`#__main_films_persons` AS `flm_prs`

		LEFT JOIN
			`#__main_professions` AS `prf`
		ON
			prf.id=flm_prs.profession_id

		LEFT JOIN
			`#__main_professions_lng` AS `prf_lng`
		ON
			prf_lng.record_id=flm_prs.profession_id
		AND
			prf_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN
			`#__main_films` AS `flm`
		ON
			flm.id=flm_prs.film_id

		LEFT JOIN
			`#__main_films_lng` AS `flm_lng`
		ON
			flm_lng.record_id=flm_prs.film_id
		AND
			flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN
			`#__main_films_persons_lng` AS `flm_prs_lng`
		ON
			flm_prs_lng.record_id=flm_prs.id
		AND
			flm_prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE
			flm_prs.person_id=".intval($_GET['person_id'])."

		ORDER BY prf.order_number, flm.year DESC

	");

	$result['professions']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['film']=htmlspecialchars($obj['film']);
		$obj['profession']=htmlspecialchars($obj['profession']);
		$obj['role']=htmlspecialchars($obj['role']);
		$obj['film_url']=main_films::getFilmUrl($obj['film_id']);
		$result['professions'][$obj['profession_id']]['title']=$obj['profession'];
		$result['professions'][$obj['profession_id']]['objects'][]=$obj;
	}

	if(!$result['professions'])
		return 404;

	main::countShow($_GET['person_id'],'person');


	return $result;
}
?>