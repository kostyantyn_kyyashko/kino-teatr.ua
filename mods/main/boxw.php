<?
function main_boxw()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::boxw');
	sys::useLib('main::users');
	sys::useLib('sys::pages');

	sys::filterGet('month');
	sys::filterGet('year');
	sys::filterGet('boxw_id');




		$lang=$_cfg['sys::lang'];




	$cache_name='main_boxw_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::boxw_page_cache_period']*60))
		return unserialize($cache);


	$result['section']=$meta['section']=sys::translate('main::all_sections');


	if(isset($_POST['_task']) && $_POST['_task'])
	{
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete' && sys::checkAccess('main::boxw_delete'))
			main_boxw::deleteArticle($task[1]);
	}

	$result['date']=$meta['date']=mb_strtolower(sys::translate('main::month_'.intval($_GET['month']))).' '.$_GET['year'];
	$meta['type']=sys::translate('main::world_box_now');
	$result['meta']=sys::parseModTpl('main::box','page',$meta);


	$result['title']=sys::translate('main::boxw_cinema');



	$month_begin=date('Y-m-d H:i:s',main_boxw::getMonthBegin($_GET['month'],$_GET['year']));
	$month_end=date('Y-m-d H:i:s',main_boxw::getMonthEnd($_GET['month'],$_GET['year']));
	$q="
		SELECT
			art.id,
			art.date,
			art.real_id,
			art.date_w_s,
			art.date_w_f,
			art.user_id,
			art.small_image AS `image`,
			art_lng.title,
			art_lng.intro
		FROM `#__main_boxw_articles` AS `art`

		LEFT JOIN
			`#__main_boxw_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE
			art.public=1

	";
	if($_GET['boxw_id'])
	{
		$q.="
			AND art.boxw_id=".intval($_GET['boxw_id'])."
		";
	}
	$q.="
		ORDER BY art.date DESC
	";

	$result['pages']=sys_pages::pocess($q,$_cfg['main::news_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{


		$q2="
			SELECT
				SUM(money) as money
			FROM `#__main_boxw_positions`
			WHERE
				boxw_id='".$obj['id']."'
		";

		$r2=$_db->query($q2);
		while($obj2=$_db->fetchAssoc($r2))
		{
			$obj['money'] = number_format($obj2['money'],0, ",", " ");
		}

		$obj['date']=sys::db2Date($obj['date'],true,$_cfg['sys::date_format']);
		if($obj['image'])
			$obj['image']=$_cfg['main::boxw_url'].$obj['image'];
		else
			$obj['image']=main::getNoImage('x1');
		$obj['title']=htmlspecialchars($obj['title']).' за '.date('d', strtotime($obj['date_w_s'])).' &mdash; '.sys::russianDate($obj['date_w_f']);
		$obj['url']=main_boxw::getArticleUrl($obj['id']);

		if(sys::checkAccess('main::boxw_edit') || $_user['id']==$obj['user_id'])
			$obj['edit_url']=main_boxw::getArticleEditUrl($obj['id']);
		else
			$obj['edit_url']=false;

		if(sys::checkAccess('main::boxw_delete') || $_user['id']==$obj['user_id'])
			$obj['delete']=true;
		else
			$obj['delete']=false;
		$result['objects'][]=$obj;
	}




	if(!$result['objects']/* && !main_boxw::boxwReirect($_GET['year'],$_GET['month'],$_GET['boxw_id'])*/)
		return 404;


	$result['add_url']=false;


		$result['ukraine']=sys::rewriteUrl('?mod=main&act=box');
		$result['world']=sys::rewriteUrl('?mod=main&act=boxw');


	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>