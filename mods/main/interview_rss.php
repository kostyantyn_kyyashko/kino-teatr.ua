<?
function main_interview_rss()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::interview');
	sys::setTpl();

	$cache_name='interview_rss_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::RSS_feed_cache_period']*60))
	return unserialize($cache);
	$result=array();

	$r=$_db->query("
		SELECT
			art.id,
			art.date,
			art.user_id,
			art.exclusive,
			art.small_image AS `image`,
			art_lng.title,
			art_lng.intro,
			art_lng.text,
			nws_lng.title AS `category`,
			usr.login
		FROM `#__main_interview_articles` AS `art`

		LEFT JOIN `#__main_interview_articles_lng` AS `art_lng`
		ON art_lng.record_id=art.id
		AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_interview_lng` AS `nws_lng`
		ON nws_lng.record_id=art.interview_id
		AND nws_lng.lang_id=".$_cfg['sys::lang_id']."
		
		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=art.user_id

		WHERE art.public=1
		AND art.interview_id!=9
		AND art.date<'".gmdate('Y-m-d H:i:s')."'

		ORDER BY art.date DESC

		LIMIT 0,10
	");

	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=date('r',sys::db2Timestamp($obj['date']));
		if($obj['image'] && is_file($_cfg['main::interview_dir'].$obj['image']))
		{
			$obj['image_src']=$_cfg['main::interview_url'].$obj['image'];
			$obj['image_mime']=getimagesize($_cfg['main::interview_dir'].$obj['image']) ;
			$obj['image_mime']=$obj['image_mime']['mime'];
			$obj['image_size']=filesize($_cfg['main::interview_dir'].$obj['image']);
		}
		else
			$obj['image']=false;
		$obj['title']=htmlspecialchars(stripslashes(strip_tags($obj['title'])));
		$obj['url']=main_interview::getArticleUrl($obj['id']);
		$obj['intro']=strip_tags($obj['intro']);
		$obj['text']=$obj['text'];
		$result['objects'][]=$obj;
	}
	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>