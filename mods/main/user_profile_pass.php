<?
function main_user_profile_pass()
{
	define('_NOINDEX','1');
	sys::useLib('main::users');
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useJs('sys::gui');
	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;
	if($_user['id']==2)
		return 401;

	$result['meta']=sys::parseModTpl('main::user_profile_pass','page');

	//old_password
	$fields['old_password']['input']='password';
	$fields['old_password']['type']='text';
	$fields['old_password']['req']=true;
	$fields['old_password']['title']=sys::translate('main::old_password');

	//password
	$fields['password']['input']='password';
	$fields['password']['type']='text';
	$fields['password']['req']=true;
	$fields['password']['max_chars']=16;
	$fields['password']['min_chars']=6;
	$fields['password']['title']=sys::translate('main::new_password');

	//password3
	$fields['password2']['input']='password';
	$fields['password2']['type']='text';
	$fields['password2']['req']=true;
	$fields['password2']['max_chars']=16;
	$fields['password2']['min_chars']=6;
	$fields['password2']['title']=sys::translate('main::new_password_again');

	$fields=sys_form::parseFields($fields, $_POST, false);


	$result['fields']=$fields;
	if(isset($_POST['cmd_save']))
	{
		if(!$_err=sys_check::checkValues($fields,$_POST))
		{
			if(!$_err && $_POST['password']!=$_POST['password2'])
				$_err=sys::translate('main::passwords_mismatch');
			if(!$_err && md5($_POST['old_password'])!=$_user['password'])
				$_err=sys::translate('main::wrong_old_password');
			if(!$_err)
			{
				$_db->setValue('sys_users','password',md5($_POST['password']),$_user['id']);
				$_SESSION['sys::user_password']=$_user['password']=md5($_POST['password']);

			}
		}
	}

	return $result;
}
?>