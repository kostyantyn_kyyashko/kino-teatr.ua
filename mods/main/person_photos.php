<?
function main_person_photos()
{
	sys::useLib('main::persons');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('person_id','int');
	sys::filterGet('photo_id','int');
	sys::filterGet('page','int');

	if(!$_GET['person_id'])	return 404;

		$q="
			SELECT
				pht.person_id,
				pht.id AS `photo_id`,
				pht.image,
				pht.user_id,
				pht.shows,
				pht.order_number,
				pht.public,
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `person`
			FROM `#__main_persons_photos` AS `pht`
			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=pht.person_id
			AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		";

	if($_GET['photo_id'])
	{
		$q.="
			WHERE pht.id=".intval($_GET['photo_id'])."
		";
	}
	elseif($_GET['person_id'])
	{
		if($_GET['page'])
			$limit=intval($_cfg['main::images_on_page']*(intval($_GET['page'])-1));
		else
			$limit=0;
		$q.="
			WHERE pht.person_id='".intval($_GET['person_id'])."'
			AND pht.public=1
			ORDER BY pht.order_number ASC
			LIMIT ".$limit.",1
		";
	}

	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);

	if(!$result)
		return 404;

	if(!$result['public'] && !sys::checkAccess('main::persons_photos_confirm'))
		return 404;

	define('_CANONIC', main_persons::getPersonPhotosUrl(intval($result['person_id'])));

	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::persons_photos_delete') || $result['user_id']==$_user['id'])
		{
			main_persons::deletePhoto($result['photo_id']);
			sys::redirect(main_persons::getPersonPhotosUrl($result['person_id']),false);
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::persons_photos_confirm'))
	{
		main_persons::confirmPhoto($result['photo_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}

	if($_cookie)
	{
		$_db->query("
			UPDATE `#__main_persons_photos`
			SET `shows`=(`shows`+1)
			WHERE `id`=".doubleval($_GET['photo_id'])."
		");
	}
	$meta['person']=$result['person'];
	$meta['photoid']=doubleval($_GET['photo_id']);
	$result['title'] = sys::translate('main::photos').' '.$result['person'];

	$result['image']=$_cfg['main::persons_url'].$result['image'];
	$result['next_url']=false;
	$result['prev_url']=false;
	$result['delete_photo']=false;
	$result['edit_url']=false;
	$result['add_url']=false;
	$result['url']=main_persons::getPhotoUrl($_GET['photo_id']);
	$result['alt']=$result['person'];

	$result['meta']=sys::parseModTpl('main::person_photos'.($meta['photoid']?'_selected':''),'page',$meta);

	if($next_id=main_persons::getNextPhotoId($result['person_id'],$result['order_number']))
		$result['next_url']=main_persons::getPhotosUrl($next_id);
	if($prev_id=main_persons::getPrevPhotoId($result['person_id'],$result['order_number']))
		$result['prev_url']=main_persons::getPhotosUrl($prev_id);

	if(sys::checkAccess('main::persons_photos_edit') || $result['user_id']==$_user['id'])
		$result['edit_url']=main_persons::getPhotoEditUrl($result['photo_id']);

	if(sys::checkAccess('main::persons_photos_add'))
		$result['add_url']=main_persons::getPhotoAddUrl($result['person_id']);

	if(sys::checkAccess('main::persons_photos_delete') || $result['user_id']==$_user['id'])
		$result['delete_photo']=true;
	//=============================================================================

	$page_url = main_persons::getPersonPhotoUrl($_GET['person_id']);
	$pu = $page_url."?photo_id=".intval($_GET["photo_id"]);
	if ($_GET['page'])	$qp = 'page='.$_GET['page'];
	else  $qp="";

	//Получить инфу об остальных фотках-----------------------------------------------
	$q="
		SELECT `id`,`image`
		FROM `#__main_persons_photos`
		WHERE `person_id`=".$result['person_id']."
		AND `public`=1
		ORDER BY `order_number` ASC
	";
	// Заменить старые ссылки на страницы на новые
	$result['pages']=sys_pages::pocess($q,$_cfg['main::images_on_page'],false);
	$result['pages']['first_page'] = sys_pages::setPageUrl($result['pages']['first_page'], $pu);
	$result['pages']['prev_page']  = sys_pages::setPageUrl($result['pages']['prev_page'],  $pu);
	$result['pages']['next_page']  = sys_pages::setPageUrl($result['pages']['next_page'],  $pu);
	$result['pages']['last_page']  = sys_pages::setPageUrl($result['pages']['last_page'],  $pu);
	foreach ($result['pages']['pages'] as $i=>$p)
	 $result['pages']['pages'][$i] = sys_pages::setPageUrl($result['pages']['pages'][$i],  $pu);

	$r=$_db->query($q.$result['pages']['limit']);
	$result['previews']=array();
	$result['photo_id']=$_GET['photo_id'];
	$i=1;
	while ($obj=$_db->fetchAssoc($r))
	{

  	    $image="x2_".$obj['image'];
		if(!file_exists($_cfg['main::persons_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::persons_url'].$image;
		$obj['image'] = $image;

		$obj['padding']=$pad;

		$obj['url']=$page_url."?photo_id=".doubleval($obj['id']);
		if($qp) $obj['url'] .= ((strpos($obj['url'],"?")===false)?"?":"&").$qp;		
		
		$obj['break']=$i%4;
		$obj['i']=$i;
		$result['previews'][]=$obj;
		$i++;
	}
	//============================================================
	main::countShow($result['person_id'],'person');
	return $result;
}
?>
