<?
function main_person_trailers()
{
	sys::useLib('main::persons');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('person_id','int');
	sys::filterGet('trailer_id','int');
	sys::filterGet('page','int');

	define('_CANONICAL',main_persons::getPersonTrailersUrl($_GET['person_id']),true);

	if(!$_GET['person_id'])
		return 404;
/*
	if(!$_GET['trailer_id'])
	{
		if($_GET['page'])
			$limit=intval($_cfg['main::images_on_page']*(intval($_GET['page'])-1));
		else
			$limit=0;
		$q="
			SELECT `trailer_id` FROM `#__main_films_trailers_persons`
			WHERE `person_id`=".intval($_GET['person_id'])."
			ORDER BY `trailer_id` DESC
			LIMIT ".$limit.",1
		";
		$r=$_db->query($q);
		list($_GET['trailer_id'])=$_db->fetchArray($r);
	}

	if(!$_GET['trailer_id'])
		return 404;
*/
	if($_cookie)
	{
		$_db->query("
			UPDATE `#__main_films_trailers`
			SET `shows`=(`shows`+1)
			WHERE `id`=".doubleval($_GET['trailer_id'])."
		");
	}
	
	$r=$_db->query("
		SELECT
			trl.id AS `trailer_id`,
			trl.image,
			trl.file,
			trl.url,
			trl.width,
			trl.height,
			trl.shows,
			trl.language,
			trl_lng.title, 
			flm_lng.title AS `film`,
			flm_lng.record_id AS `film_id`
		FROM
			`#__main_films_trailers` AS `trl`

		LEFT JOIN `#__main_films_lng` AS flm_lng
		ON flm_lng.record_id=trl.film_id		
		AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		
		LEFT JOIN `#__main_films_trailers_lng` AS `trl_lng`
		ON trl_lng.record_id=trl.id
		AND trl_lng.lang_id='".$_cfg['sys::lang_id']."'
		
		WHERE trl.id=".intval($_GET['trailer_id'])."
		AND trl.public=1
	");
	
	$result=$_db->fetchAssoc($r);
//	if(!$result)
//		return 404;

	$result['person']=main_persons::getPersonFio($_GET['person_id']);

	$meta['person']=$result['person'];
	$meta['id']=$_GET["trailer_id"];
	$meta['film']=$result['film'];
	$result['meta']=sys::parseModTpl('main::person_trailers'.($_GET["trailer_id"]?'_selected':''),'page',$meta);
	$result['image']=$result['image'];

	if($result['file'])
		$result['file']=$_cfg['main::films_url'].$result['file'];
	else
		$result['file']=$result['url'];

	$result['next_url']=false;
	$result['prev_url']=false;
	$result['url']=main_films::getFilmTrailerUrl($result['trailer_id']);
	$result['alt']=$result['person'].' '.sys::translate('main::trailer_from_film').' &quot;'.$result['film'].'&quot;';
	$result['person_url']=main_persons::getPersonUrl($_GET['person_id']);
	$result['film_url']=main_films::getFilmUrl($result['film_id']);
	if(!$result['width'])
		$result['width']=495;
	if(!$result['height'])
		$result['height']=372;

	if($result['width']>495)
	{
		$result['width']=495;
		$result['height']=372;
	}


	if($next_id=main_persons::getNexttrailerId($_GET['person_id'],$_GET['trailer_id']))
		$result['next_url']=main_persons::gettrailersUrl($next_id,$_GET['person_id']);
	if($prev_id=main_persons::getPrevtrailerId($_GET['person_id'],$_GET['trailer_id']))
		$result['prev_url']=main_persons::gettrailersUrl($prev_id,$_GET['person_id']);

	$page_url = main_persons::getPersonTrailerUrl(intval($_GET['person_id']));		
	$pu = $page_url."?trailer_id=".intval($_GET["trailer_id"]);
	if ($_GET['page'])	$qp = 'page='.$_GET['page'];
	else  $qp="";

	//Получить инфу об остальных трейлерах-----------------------------------------------
	$q="
		SELECT 
			trl.id,
			trl.image, 
			lng.title, 
			trl.language,
			trl.duration
		FROM `#__main_films_trailers_persons` AS `trl_prs`

		LEFT JOIN `#__main_films_trailers` AS `trl`
		ON trl.id=trl_prs.trailer_id

		LEFT JOIN `#__main_films_trailers_lng` AS `lng`
		ON lng.record_id=trl_prs.trailer_id
		AND lng.lang_id='".$_cfg['sys::lang_id']."'

		WHERE trl_prs.person_id=".$_GET['person_id']."
		AND trl.public=1
		ORDER BY trl.id DESC
	";

	// Заменить старые ссылки на страницы на новые
	$result['pages']=sys_pages::pocess($q,$_cfg['main::images_on_page'],false);
	$result['pages']['first_page'] = sys_pages::setPageUrl($result['pages']['first_page'], $pu);
	$result['pages']['prev_page']  = sys_pages::setPageUrl($result['pages']['prev_page'],  $pu);
	$result['pages']['next_page']  = sys_pages::setPageUrl($result['pages']['next_page'],  $pu);
	$result['pages']['last_page']  = sys_pages::setPageUrl($result['pages']['last_page'],  $pu);
	foreach ($result['pages']['pages'] as $i=>$p)
	 $result['pages']['pages'][$i] = sys_pages::setPageUrl($result['pages']['pages'][$i],  $pu);

	$r=$_db->query($q.$result['pages']['limit']);
	$result['previews']=array();
	$result['trailer_id']=$_REQUEST['trailer_id'];
	$i=1;
	while ($obj=$_db->fetchAssoc($r))
	{
  	    $image="x3_".$obj['image'];
		if(!file_exists($_cfg['main::films_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$obj['image'] = $image;
	
		$obj['url']=$page_url."?trailer_id=".doubleval($obj['id']);
		
		if($obj['file'])
			$obj['size']=sys::convertFileSize(filesize($_cfg['main::films_url'].$obj['file']));
		else
			$obj['size']=false;

		if($qp) $obj['url'] .= ((strpos($obj['url'],"?")===false)?"?":"&").$qp;
//		$obj['url']=sys::rewriteUrl($obj['url']);
		if($obj['duration']>60)
		{
			$obj['duration']=(floor($obj['duration']/60)).':'.($obj['duration']%60).' '.sys::translate('main::min');
		}
		elseif($obj['duration'])
			$obj['duration'].=' '.sys::translate('main::sec');
		else
			$obj['duration']=false;
		$result['previews'][]=$obj;
	}
	//============================================================

	main::countShow($_GET['person_id'],'person');
	return $result;
}
?> 