<?
function main_ajx_partner()
{
	sys::setTpl();
	global $_db, $_cfg, $_err, $_user, $_cookie;

	$data["partner_id"] = 0;
	$data["partner_link"] = "";
	$data["partner_name"] = "";
	
	if(isset($_POST['partner_id'])) 
	{
		$id = intval($_POST['partner_id']);
	
		$rec = $_db->getRecord("partners_links", $id);		
		if($rec) $data = $rec;
	}		
	echo json_encode($data);
}
?>