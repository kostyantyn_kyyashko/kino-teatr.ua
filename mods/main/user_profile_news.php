<?
function main_user_profile_news()
{
	define('_NOINDEX','1');
	sys::useLib('main::users');
	sys::useLib('sys::form');
	sys::useJs('sys::gui');
	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;
	if($_user['id']==2)
		return 401;

	$result['meta']=sys::parseModTpl('main::user_profile_news','page');

	$r=$_db->query("
		SELECT
			nws.id,
			nws_lng.title,
			usr_nws.news_id AS `checked`
		FROM
			#__main_news AS `nws`

		LEFT JOIN
			`#__main_news_lng` AS `nws_lng`
		ON
			nws_lng.record_id=nws.id
		AND
			nws_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN
			`#__main_users_news_subscribe` AS `usr_nws`
		ON
			usr_nws.news_id=nws.id
		AND
			usr_nws.user_id=".intval($_user['id'])."
        WHERE nws.showtab=1
		ORDER BY nws.order_number
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$result['objects'][]=$obj;
	}

	if(isset($_POST['cmd_save']))
	{
		sys::printR($_POST);
		$_db->delete('main_users_news_subscribe',"`user_id`=".intval($_user['id'])."");
		foreach ($_POST['news'] as $news_id=>$val)
		{
			if(!$_db->query("
				INSERT INTO `#__main_users_news_subscribe`
				(
					`user_id`,
					`news_id`,
					`date`
				)
				VALUES
				(
					'".intval($_user['id'])."',
					'".intval($news_id)."',
					'".gmdate('Y-m-d H:i:s')."'
				)
			"))
			{
				break;
				$_err=sys::translate('main::db_err');
			}
		}
		if(!$_err)
			sys::redirect(main_users::getProfileNewsUrl(),false);
	}

	return $result;
}
?>