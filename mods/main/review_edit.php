<?
function main_review_edit()
{
	sys::useLib('main::reviews');
	sys::useLib('main::films');
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	global $_db, $_cfg, $_user, $_err;
	sys::filterGet('review_id','int');
	sys::filterGet('film_id','int');
	sys::filterGet('vip','int');
	
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		

	if(!$_GET['review_id'] && !$_GET['film_id'])
		return 404;
	
	$tbl='main_reviews';
	$default['date']=strftime($_cfg['sys::date_time_format']);
	$default['user_id']=$_user['id'];
	
	if($_GET['review_id'])
	{
		$record=$_db->getRecord($tbl,$_GET['review_id']);
		if(!$record)
			return 404;
		$record['date']=sys::db2Date($record['date'],true);
	}
	else 
	{
		$record=false;
	}
	
	if(!$_GET['film_id'])
		$_GET['film_id']=$record['film_id'];
	
	$values=sys::setValues($record, $default);
	
	if($_GET['review_id'] && !sys::checkAccess('main::reviews_edit') && $_user['id']==$record['user_id'])
		return 403;
	
	elseif(!sys::checkAccess('main::reviews_add'))
		return 403;
	//===========================================================================================================//
	
	
	//Поля и форма---------------------------------------------------------------------------------------------
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="55"';
	
	//date
	$fields['date']['input']='datebox';
	$fields['date']['type']='date';
	$fields['date']['req']=true;
	$fields['date']['table']=$tbl;
	$fields['date']['format']=$_cfg['sys::date_time_format'];
	
	//image
	$fields['image']['input']='filebox';
	$fields['image']['type']='file';
	$fields['image']['file::dir']=$_cfg['main::reviews_dir'];
	$fields['image']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image']['title']=sys::translate('main::image');
	$fields['image']['preview']=true;
	$fields['image']['preview::prefix']='x2_';
	$fields['image']['req']=true;
	
	//intro
	$fields['intro']['input']='textarea';
	$fields['intro']['type']='html';
	$fields['intro']['req']=true;
	$fields['intro']['table']=$tbl;
	$fields['intro']['html_params']='style="width:100%; height:200px"';
	//$fields['intro']['height']='300px';
	$fields['intro']['title']=sys::translate('main::intro');
	//$fields['intro']['fck::ToolbarSets']='User';
	
	//text
	$fields['text']['input']='textarea';
	//$fields['text']['input']='fck';
	$fields['text']['type']='html';
	$fields['text']['table']=$tbl;
	$fields['text']['req']=true;
	$fields['text']['html_params']='style="width:100%; height:350px"';
	$fields['text']['height']='700px';
	$fields['text']['fck::ToolbarSets']='User';
	
	//mark
	$fields['mark']['input']='textbox';
	$fields['mark']['type']='int';
	$fields['mark']['req']=true;
	$fields['mark']['min_value']=1;
	$fields['mark']['max_value']=10;
	$fields['mark']['table']=$tbl;
	$fields['mark']['html_params']='size="2" maxlength="55"';
	$fields['mark']['title']=sys::translate('main::film_mark');
	
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------

	if(isset($_POST['cmd_cancel']))
	{
		sys::redirect(main_films::getFilmUrl($_GET['film_id']),false);
	}
	if(isset($_POST['cmd_save']))
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			// вырезать теги
			foreach ($values as $key => $value)
			{
				$value = preg_replace('#<(br|p)\s*/?>#i', "\n", $value); // заменить <br> на перенос строки
				$pattern = '#/\*[^\/*]+\*/#i';
				$replacement = '';
				$values[$key] = stripslashes(preg_replace($pattern, $replacement, strip_tags($value)));
				
				if( (strpos($key,"text")===0) || (strpos($key,"intro")===0) ) $values[$key] = "<p>".nl2br($values[$key])."</p>";
			}	

			$values['date']=sys::date2Db($values['date'],true);
			//Вставка данных
			if(!$_GET['review_id'])
			{
				$extra['user_id']=$_user['id'];
				$extra['film_id']=$_GET['film_id'];
				$extra['public']=0;
				if(!$_GET['review_id']=$_db->insertArray($tbl,$values,$fields,$extra))
					$_err=sys::translate('sys::db_error').$_db->err;
				else 
					sys::sendMail(false,false,$_db->getValue('sys_users','email',7),'Root','Добавлена новая рецензия',_ROOT_URL.'backend/?mod=main&act=review_edit&id='.$_GET['review_id']);
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['review_id'],$extra))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['date']=sys::db2Date($values['date'],true);
		}
		if(!$_err)
		{
			//Загрузить и отресайзить картинку	
			//Загрузить и отресайзить картинку	
			if($_FILES['image']['name'])
				$values['image']=main_reviews::uploadReviewImage($_FILES['image'],$_GET['review_id'],$values['image']);
			sys::redirect(main_films::getFilmUrl($_GET['film_id']),false);
		}
	}
	//==========================================================================================================//
	
	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			$_db->setValue('main_reviews','image',false,$_GET['id']);
			main_reviews::deleteReviewImage($values['image']);
			$values['image']=false;
		}
	}
	//==================================================================================================
	
	$result['fields']=sys_form::parseFields($fields,$values,false);
	$result['page']=sys::parseModTpl('main::review_edit','page');
	
	return $result;
}
?>