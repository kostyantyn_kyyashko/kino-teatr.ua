<?
function main_cinema_news()
{
	//error_reporting(E_ALL);
	sys::useLib('main::cinemas');
	sys::useLib('main::news');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('cinema_id','int');

	if(!$_GET['cinema_id'])
		return 404;

		
	$result['cinema']=$_db->getValue('main_cinemas','title',intval($_GET['cinema_id']),true);
	if(!$result['cinema'])
		return 404;

	$meta['cinema']=$result['cinema'];
	$result['title'] = sys::translate('main::news_about_cinema').' '.$result['cinema'];
	$result['meta']=sys::parseModTpl('main::cinema_news','page',$meta);

	$q="
		SELECT
			art.id,
			art.date,
			art.small_image AS `image`,
			art.image AS `big_image`,
			art.exclusive,
			art_lng.title,
			art_lng.intro
		FROM
			`#__main_news_articles_cinemas` AS `art_cnm`

		LEFT JOIN
			`#__main_news_articles` AS `art`
		ON
			art.id=art_cnm.article_id

		LEFT JOIN
			`#__main_news_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE
			art_cnm.cinema_id=".intval($_GET['cinema_id'])."
		AND
			art.public=1

		ORDER BY
			art.date DESC
	";

	$result['pages']=sys_pages::pocess($q,$_cfg['main::news_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::russianDate($obj['date']);

		if($obj['image'] || $obj['big_image'])
		{
  	    	$image=$obj['image'];	
  	    	list($wid, $hei, $type) = getimagesize('./public/main/news/'.$image);
			if($wid<213 && $obj['big_image']) $image = $obj['big_image'];

			if(!file_exists('./public/main/news/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
			 else $image=$_cfg['main::news_url'].$image;
			$obj['image'] = $image;
		}
		else 
		 $image = $_cfg['sys::root_url'].'blank_news_img.jpg';

		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_news::getArticleUrl($obj['id']);
		$result['objects'][]=$obj;
	}

	return $result;
}
?>