<?
function main_ajx_film_rating_values()
{
	sys::setTpl();
	sys::useLib('main::films');
	global $_user, $_db, $_cfg;

	if(!$_POST['object_id']) return false;

	$film = $_db->getRecord("main_films", intval($_POST['object_id']));

	$result['pre_sum'] 		= number_format($film['pre_rating'], 1, '.', '');
	$result['sum'] 			= number_format($film['rating'], 1, '.', '');
	$result['pre_votes'] 	= $film['pre_votes'];
	$result['votes'] 		= $film['votes'];	
	$result["expect_yes"] 	= $film["expect_yes"];
	$result["expect_no"] 	= $film["expect_no"];
		
	echo json_encode($result);
}
?>