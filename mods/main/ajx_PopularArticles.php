<?
function main_ajx_PopularArticles()
{
	sys::setTpl();
	sys::filterGet('keywords');
	sys::filterGet('field');
	sys::filterGet('mode');
	sys::useLib('main::articles');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	echo main_articles::showLastPopularArticles();
}
?>