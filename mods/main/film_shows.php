<?
function main_film_shows()
{
	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('main::cards');
	sys::useLib('sys::form');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');
	sys::filterGet('date','text',strftime($_cfg['sys::date_format']));
	sys::filterGet('city_id','int',$_cfg['main::city_id']);	
	
	define('_CANONICAL',main_films::getFilmShowsUrl($_GET['film_id']),true);

	if ($_GET['city_id']=='1')
	{
		define('_CANONICAL',$_cfg['sys::root_url'].$_cfg['sys::lang'].'/main/film_shows/film_id/'.$_GET['film_id'].'.phtml',true);
    }


	$cache_name='main_film_shows_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
//	if($_SERVER['HTTP_HOST'] == 'kino-teatr.ua'){
//		if($cache=sys::getCache($cache_name,$_cfg['main::film_shows_page_cache_period']*_HOUR))
//			return unserialize($cache);
//	}
	
	if(!$_GET['film_id'])
		return 404;

	$result['film']=$_db->getValue('main_films','title',intval($_GET['film_id']),true);
	$result['serial']=$_db->getValue('main_films','serial',intval($_GET['film_id']),false);
	$meta['film']=$result['film'];
	$result['title'] = sys::translate('main::filmshows').' '.$result['film'];
	$result['meta']=sys::parseModTpl('main::film_shows','page',$meta);
	if(!$result['film'])
		return 404;

	main::countShow($_GET['film_id'],'film');

	$q = "
		SELECT
			shw.begin,
			shw.end,
			shw.id AS `show_id`,
			shw.partner_id,
			hls_lng.title AS `hall`,
			cnm_lng.title AS `cinema`,
			cnm.site,
			cnm.ticket_url,
			hls.cinema_id,
			hls.id AS `hall_id`,
			hls.scheme,
			hls.`3d`,
			flm.`3d` as `film_3d`,
			shw.hall_id,
			ev.event_id,
			ev.site_id
		FROM `#__main_shows` AS `shw`

		LEFT JOIN `#__main_cinemas_halls` AS `hls`
		ON hls.id=shw.hall_id

		LEFT JOIN `#__main_cinemas` AS `cnm`
		ON cnm.id=hls.cinema_id

		LEFT JOIN `#__main_films` AS `flm`
		ON flm.id=shw.film_id
		
		LEFT JOIN `#__main_shows_eventmap` AS `ev`
		ON ev.show_id=shw.id

		LEFT JOIN `#__main_cinemas_lng` AS `cnm_lng`
		ON cnm_lng.record_id=cnm.id
		AND cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng`
		ON hls_lng.record_id=hls.id
		AND hls_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE shw.end>='".mysql_real_escape_string(date('Y-m-d H:i:s', strtotime($_GET['date'])))."'
		AND shw.film_id=".$_GET['film_id']." AND cnm.city_id=".$_cfg['main::city_id']."
		ORDER BY cnm.order_number, shw.hall_id, shw.begin
	";
	
/*
if(sys::isDebugIP()) 	$q = "
		SELECT
			shw.begin,
			shw.end,
			shw.id AS `show_id`,
			hls_lng.title AS `hall`,
			cnm_lng.title AS `cinema`,
			cnm.site,
			cnm.ticket_url,
			hls.cinema_id,
			hls.id AS `hall_id`,
			hls.scheme,
			hls.`3d`,
			flm.`3d` as `film_3d`,
			shw.hall_id
		FROM `#__main_shows` AS `shw`

		LEFT JOIN `#__main_cinemas_halls` AS `hls`
		ON hls.id=shw.hall_id

		LEFT JOIN `#__main_cinemas` AS `cnm`
		ON cnm.id=hls.cinema_id

		LEFT JOIN `#__main_films` AS `flm`
		ON flm.id=shw.film_id

		LEFT JOIN `#__main_cinemas_lng` AS `cnm_lng`
		ON cnm_lng.record_id=cnm.id
		AND cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng`
		ON hls_lng.record_id=hls.id
		AND hls_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE shw.end>='2016-10-27 00:00:00' AND shw.end<='2016-11-03 00:00:00'
		AND shw.film_id=".$_GET['film_id']." AND cnm.city_id=".$_cfg['main::city_id']."
		ORDER BY cnm.order_number, shw.hall_id, shw.begin
	";
*/
	$r=$_db->query($q);



//	if(sys::isDebugIP()) $_db->printR($q);
//	echo (sys::date2Db($_GET['date']));
//	echo (date('Y-m-d H:i:s', strtotime($_GET['date'])));
//	exit;


	$result['cinemas']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		// Получить массив дат от begin до end
		$a_dt_begin = explode("-", $obj['begin']);
		$a_dt_end = explode("-", $obj['end']);

		//$dt_begin = mktime(0,0,0, month, 		  	day, 			year);
		$dt_begin   = mktime(0,0,0,	$a_dt_begin[1], $a_dt_begin[2], $a_dt_begin[0]);
		$dt_end     = mktime(0,0,0,	$a_dt_end[1], 	$a_dt_end[2], 	$a_dt_end[0]);
		$today 		= mktime(0,0,0, date("m"),		date("d"),		date("Y"));
		$now 		= mktime(date("H"), date("i")+15, 0, date("m"), date("d"), date("Y"));

		$q = "
			SELECT tms.time, tms.prices, tms_lng.note, tms.3d as seans_3d,tms.sale_id,tms.sale_status, tms.id as time_id
			FROM `#__main_shows_times` AS `tms`
			LEFT JOIN `#__main_shows_times_lng` AS `tms_lng`
			ON tms_lng.record_id=tms.id
			AND tms_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE tms.show_id=".intval($obj['show_id'])." ORDER BY tms.time
		";
//if(sys::isDebugIP()) $_db->printR($q);
		$r2=$_db->query($q);
		$obj['prices']=array();
//		$a_times = array();

		while ($time=$_db->fetchAssoc($r2))
		{
			$dt_seans = explode(":",$time['time']);
			$dt_seans = mktime(isset($dt_seans[0])?$dt_seans[0]:0,isset($dt_seans[1])?$dt_seans[1]:0,0, date("m"),		date("d"),		date("Y"));
//$time['past_dt_seans'] = date("d.m.Y H:i", $dt_seans);
//$time['past_dt_end'] = date("d.m.Y H:i",$dt_end);
//$time['past_dt_now'] = date("d.m.Y H:i",$now);
			$time['past'] = (($dt_seans<$now) && ($today>=$dt_end));
//			if(sys::date2Db($_GET['date'])==date('Y-m-d').' 00:00:00')
//			{
//				$timestamp=sys::date2Timestamp(date('Y-m-d').' '.$time['time'],'%Y-%m-%d %H:%M:%S');
//			}
			$time['time']=sys::cutStrRight($time['time'],3);
//			$a_times[] = $time;

			if($time['sale_id']!=0)
			{
				$time['time'] = '<a href="http://bilet.kino-teatr.ua/showtime?theater='.$time['sale_status'].'&showtime='.$time['sale_id'].'&agent=kino-teatr" class="vkino-link">'.$time['time'].'</a>';
			}
			elseif($time['sale_status']!='' and $time['sale_id']==0)
			{
				$temp_st=explode('|',$time['sale_status']);
				if($temp_st[0]=='imax' and $temp_st[1]!='')
				{
					$time['time'] = '<a rel="nofollow" href="'.$temp_st[1].'" onclick="goPartner(this, 1);" class="mult-link">'.$time['time'].'</a>';
				}
				if($temp_st[0]=='mult' and $temp_st[1]!='')
				{
					$time['time'] = '<a rel="nofollow" href="'.$temp_st[1].'" onclick="goPartner(this, 2);" class="mult-link">'.$time['time'].'</a>';
				}
				if($temp_st[0]=='mega')
				{
					$time['time'] = ''.$time['time'].'';
				}
			}
			if($obj['partner_id'] == 8){
				if($_user['id']==2){
					$url = "/eventmap.php?lang_id=".intval($_cfg['sys::lang_id']).'&unauthorized=1';
					$time['time'] = '<a href="'.$url.'" class="megakino-link" onclick="show_unauthorized(this);return false;" style="font-size:12px;">'.$time['time'].'</a>';

				}
                else {
                    $temp_st=explode('|',$time['sale_status']);
                    $site_id='';
                    $event_id='';
                    if($temp_st[0]=='megakino' and $temp_st[1]!='' and $temp_st[2] != '') {
                        $site_id = $temp_st[1];
                        $event_id = $temp_st[2];
                    }
                    else if ($obj['event_id'] && $obj['site_id']) {
                        $site_id = $obj['site_id'];
                        $event_id = $obj['event_id'];
                    }

                    if ($site_id && $event_id) {
					    $url = "/eventmap.php?lang_id=".intval($_cfg['sys::lang_id']).'&cinema_id='.$obj['cinema_id'].'&site_id='.$site_id.'&show_id='.$obj['show_id'].'&time_id='.$time['time_id'];
                        $time['time'] = '<a href="'.$url.'" class="megakino-link" onclick="show_eventmap(this,'.$obj['show_id'].');return false;" style="font-size:12px;">'.$time['time'].'</a>';
                    }
                }
			}
			$time['url']=main_cards::getCardUrl($time['time'],$_GET['date'],$_GET['film_id'],$obj['hall_id']);
			$prices=explode(';',$time['prices']);
			$obj['prices']=array_merge($obj['prices'],$prices);
			$obj['times'][]=$time;
		}
		$obj['prices']=array_unique($obj['prices']);
		sort($obj['prices']);

		if($obj['scheme'])
			$obj['scheme_url']=main_cinemas::getHallSchemeUrl($obj['hall_id']);
		else
			$obj['scheme_url']=false;

		$obj['begin']=sys::db2Date($obj['begin'],false, $_cfg['sys::date_format']);
		$obj['end']=sys::db2Date($obj['end'],false, $_cfg['sys::date_format']);
		$result['cinemas'][$obj['cinema_id']]['title']=$obj['cinema'];
		$result['cinemas'][$obj['cinema_id']]['id']=$obj['cinema_id'];
		$result['cinemas'][$obj['cinema_id']]['site']=$obj['site'];
		$result['cinemas'][$obj['cinema_id']]['ticket_url']=$obj['ticket_url'];
		$result['cinemas'][$obj['cinema_id']]['url']=main_cinemas::getCinemaUrl($obj['cinema_id']);
		$result['cinemas'][$obj['cinema_id']]['halls'][$obj['hall_id']]['title']=$obj['hall'];
		$result['cinemas'][$obj['cinema_id']]['halls'][$obj['hall_id']]['scheme_url']=$obj['scheme_url'];
		$result['cinemas'][$obj['cinema_id']]['halls'][$obj['hall_id']]['url']=main_cinemas::getHallUrl($obj['cinema_id'],$obj['hall_id']);
		$result['cinemas'][$obj['cinema_id']]['halls'][$obj['hall_id']]['shows'][]=$obj;
		$result['cinemas'][$obj['cinema_id']]['shows'][$obj['show_id']]=$obj;
		$result['cinemas'][$obj['cinema_id']]['count'] = count ($result['cinemas'][$obj['cinema_id']]['shows'][$obj['show_id']]['times']);
	}

//if(sys::isDebugIP()) $_db->printR($result);
	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>