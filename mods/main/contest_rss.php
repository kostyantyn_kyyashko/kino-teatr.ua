<?
function main_contest_rss()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::contest');
	sys::setTpl();

	$cache_name='contest_rss_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::RSS_feed_cache_period']*60))
	return unserialize($cache);
	$result=array();

	$r=$_db->query("
		SELECT
			art.id,
			art.date,
			art.user_id,
			art.exclusive,
			art.small_image AS `image`,
			art_lng.title,
			art_lng.intro,
			art_lng.text,
			nws_lng.title AS `category`
		FROM `#__main_contest_articles` AS `art`

		LEFT JOIN `#__main_contest_articles_lng` AS `art_lng`
		ON art_lng.record_id=art.id
		AND art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_contest_lng` AS `nws_lng`
		ON nws_lng.record_id=art.contest_id
		AND nws_lng.lang_id=".$_cfg['sys::lang_id']."

		WHERE art.public=1
		AND art.contest_id!=9
		AND art.date<'".gmdate('Y-m-d H:i:s')."'

		ORDER BY art.date DESC

		LIMIT 0,10
	");

	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=date('r',sys::db2Timestamp($obj['date']));
		if($obj['image'] && is_file($_cfg['main::contest_dir'].$obj['image']))
		{
			$obj['image_src']=$_cfg['main::contest_url'].$obj['image'];
			$obj['image_mime']=getimagesize($_cfg['main::contest_dir'].$obj['image']) ;
			$obj['image_mime']=$obj['image_mime']['mime'];
			$obj['image_size']=filesize($_cfg['main::contest_dir'].$obj['image']);
		}
		else
			$obj['image']=false;
		$obj['title']=htmlspecialchars(stripslashes(strip_tags($obj['title'])));
		$obj['url']=main_contest::getArticleUrl($obj['id']);
		$obj['intro']=strip_tags($obj['intro']);

		$result['objects'][]=$obj;
	}
	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>