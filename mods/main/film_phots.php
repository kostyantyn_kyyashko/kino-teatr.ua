<?


function main_film_phots()
{

	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int',0);
	sys::filterGet('photo_id','int',0);
	sys::filterGet('page','int',0);

	$_GET['page'] = abs($_GET['page']);
	
	$cache_name=__FUNCTION__.'_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,_HOUR))
		return unserialize($cache);

	define('_CANONIC',main_films::getFilmPhotosUrl($_GET['film_id']),true);
	define('_ALTERNATE', 'href="'.main_films::getFilmPhotosUrl($_GET['film_id'], (($_cfg["sys::lang"]==="ru")?"uk":"ru")).'" hreflang="'.(($_cfg["sys::lang"]==="ru")?"uk":"ru").'"');	
	
	if(!$_GET['film_id'] && !$_GET['photo_id'])
		return 404;


		$q="
			SELECT
				pht.film_id,
				pht.id AS `photo_id`,
				pht.image,
				pht.user_id,
				pht.shows,
				pht.order_number,
				pht.public,
				flm_lng.title AS `film`,
				flm.serial,
				flm.name,
				flm.title_orig
			FROM `#__main_films_photos` AS `pht`
			
			LEFT JOIN `#__main_films` AS `flm`			
			ON flm.id=pht.film_id			
			
			LEFT JOIN `#__main_films_lng` AS `flm_lng`			
			ON flm_lng.record_id=pht.film_id			
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		";



	if($_GET['film_id'])
	{

		if($_GET['page'])
			$limit=intval($_cfg['main::images_on_page']*(intval($_GET['page'])-1));
		else
			$limit=0;

		$q.="
			WHERE pht.film_id='".intval($_GET['film_id'])."'
			AND pht.public=1
			ORDER BY pht.order_number
			LIMIT ".$limit.",1
		";
	}
	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);

	if(!$result)
		return 404;

	if(!$result['public'] && !sys::checkAccess('main::films_photos_confirm'))
		return 404;

	main::countShow($result['film_id'],'film');

	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::films_photos_delete') || $result['user_id']==$_user['id'])
		{
			main_films::deletePhoto($result['photo_id']);
			sys::redirect(main_films::getFilmPhotosUrl($result['film_id']),false);
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::films_photos_confirm'))
	{
		main_films::confirmPhoto($result['photo_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}

	if($_cookie)
	{
		$_db->query("
			UPDATE `#__main_films_photos`
			SET `shows`=(`shows`+1)
			WHERE `id`=".doubleval($result['photo_id'])."
		");
	}
//if(sys::isDebugIP()) sys::printR($result);
	//$result['serial']=$_db->getValue('main_films','serial',intval($_GET['film_id']),false);
	$meta['film']=$result['film'];
	$result['title'] = (($result['serial'])?sys::translate('main::photos_from_serial'):sys::translate('main::photos_from_films')).' '.$result['film'];
	$result['image']=$_cfg['main::films_url_new'].$result['image'].'&w=495';
	$result['next_url']=false;
	$result['prev_url']=false;
	$result['delete_poster']=false;
	$result['edit_url']=false;
	$result['add_url']=false;
	$result['url']=main_films::getPhotosUrl($result['photo_id']);

	$result['meta']=sys::parseModTpl('main::film_photos','page',$meta);

	//=============================================================================

	//Получить инфу об остальных фотках-----------------------------------------------
	$q="
		SELECT `id`,`image`
		FROM `#__main_films_photos`
		WHERE `film_id`=".$result['film_id']."
		AND `public`=1
		ORDER BY `order_number` DESC
	";
	$result['pages']=sys_pages::pocess($q,$_cfg['main::images_on_page'],false,'?mod=main&act=film_phots&film_id='.$result['film_id'].'&page=%page%');
	$r=$_db->query($q);
	$result['previews']=array();

   	$i = 1;

	while ($obj=$_db->fetchAssoc($r))
	{
		$image_info = getImageSize($_cfg['main::films_dir'].$obj['image']);
		$obj['width']	= $image_info[0];
		$obj['height']	= $image_info[1];
		if ($i!=1)
			$obj['number']='- '.sys::translate('main::photo_number').' '.$i;

		$obj['weight']=round(filesize($_cfg['main::films_dir'].$obj['image'])/1024);

  	    $image="x3_".$obj['image']; 
		if(!file_exists($_cfg['main::films_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$obj['image'] = $image;

		$obj['url']=sys::getHumanUrl($result['film_id'],$result['name'],"film_photos")."?photo_id=".$obj['id'];

		if($_GET['page'])
			$obj['url'].='&page='.intval($_GET['page']);
				$obj['break'] = $i%3;
		$result['previews'][]=$obj;
		$i++;
	}
	//============================================================
	sys::setCache($cache_name,serialize($result));
	return $result;
}


?>