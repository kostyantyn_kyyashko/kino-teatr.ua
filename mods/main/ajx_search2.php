<?
function main_ajx_search2()
{

function dmword($string, $is_cyrillic = true)
	{
		static $codes = array(
			'A' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1)),
				'U' => array(array(0, 7, -1))),
			'B' => array(array(7, 7, 7)),
			'C' => array(array(5, 5, 5), array(4, 4, 4),
				'Z' => array(array(4, 4, 4),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'Z' => array(array(4, 4, 4))),
				'K' => array(array(5, 5, 5), array(45, 45, 45)),
				'H' => array(array(5, 5, 5), array(4, 4, 4),
					'S' => array(array(5, 54, 54)))),
			'D' => array(array(3, 3, 3),
				'T' => array(array(3, 3, 3)),
				'Z' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'Z' => array(array(4, 4, 4))),
				'R' => array(
					'S' => array(array(4, 4, 4)),
					'Z' => array(array(4, 4, 4)))),
			'E' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1)),
				'U' => array(array(1, 1, -1))),
			'F' => array(array(7, 7, 7),
				'B' => array(array(7, 7, 7))),
			'G' => array(array(5, 5, 5)),
			'H' => array(array(5, 5, -1)),
			'I' => array(array(0, -1, -1),
				'A' => array(array(1, -1, -1)),
				'E' => array(array(1, -1, -1)),
				'O' => array(array(1, -1, -1)),
				'U' => array(array(1, -1, -1))),
			'J' => array(array(4, 4, 4)),
			'K' => array(array(5, 5, 5),
				'H' => array(array(5, 5, 5)),
				'S' => array(array(5, 54, 54))),
			'L' => array(array(8, 8, 8)),
			'M' => array(array(6, 6, 6),
				'N' => array(array(66, 66, 66))),
			'N' => array(array(6, 6, 6),
				'M' => array(array(66, 66, 66))),
			'O' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1))),
			'P' => array(array(7, 7, 7),
				'F' => array(array(7, 7, 7)),
				'H' => array(array(7, 7, 7))),
			'Q' => array(array(5, 5, 5)),
			'R' => array(array(9, 9, 9),
				'Z' => array(array(94, 94, 94), array(94, 94, 94)), // special case
				'S' => array(array(94, 94, 94), array(94, 94, 94))), // special case
			'S' => array(array(4, 4, 4),
				'Z' => array(array(4, 4, 4),
					'T' => array(array(2, 43, 43)),
					'C' => array(
						'Z' => array(array(2, 4, 4)),
						'S' => array(array(2, 4, 4))),
					'D' => array(array(2, 43, 43))),
				'D' => array(array(2, 43, 43)),
				'T' => array(array(2, 43, 43),
					'R' => array(
						'Z' => array(array(2, 4, 4)),
						'S' => array(array(2, 4, 4))),
					'C' => array(
						'H' => array(array(2, 4, 4))),
					'S' => array(
						'H' => array(array(2, 4, 4)),
						'C' => array(
							'H' => array(array(2, 4, 4))))),
				'C' => array(array(2, 4, 4),
					'H' => array(array(4, 4, 4),
						'T' => array(array(2, 43, 43),
							'S' => array(
								'C' => array(
									'H' => array(array(2, 4, 4))),
								'H' => array(array(2, 4, 4))),
							'C' => array(
								'H' => array(array(2, 4, 4)))),
						'D' => array(array(2, 43, 43)))),
				'H' => array(array(4, 4, 4),
					'T' => array(array(2, 43, 43),
						'C' => array(
							'H' => array(array(2, 4, 4))),
						'S' => array(
							'H' => array(array(2, 4, 4)))),
					'C' => array(
						'H' => array(array(2, 4, 4))),
					'D' => array(array(2, 43, 43)))),
			'T' => array(array(3, 3, 3),
				'C' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4))),
				'Z' => array(array(4, 4, 4),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'Z' => array(array(4, 4, 4)),
					'H' => array(array(4, 4, 4)),
					'C' => array(
						'H' => array(array(4, 4, 4)))),
				'T' => array(
					'S' => array(array(4, 4, 4),
						'Z' => array(array(4, 4, 4)),
						'C' => array(
							'H' => array(array(4, 4, 4)))),
					'C' => array(
						'H' => array(array(4, 4, 4))),
					'Z' => array(array(4, 4, 4))),
				'H' => array(array(3, 3, 3)),
				'R' => array(
					'Z' => array(array(4, 4, 4)),
					'S' => array(array(4, 4, 4)))),
			'U' => array(array(0, -1, -1),
				'E' => array(array(0, -1, -1)),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1))),
			'V' => array(array(7, 7, 7)),
			'W' => array(array(7, 7, 7)),
			'X' => array(array(5, 54, 54)),
			'Y' => array(array(1, -1, -1)),
			'Z' => array(array(4, 4, 4),
				'D' => array(array(2, 43, 43),
					'Z' => array(array(2, 4, 4),
						'H' => array(array(2, 4, 4)))),
				'H' => array(array(4, 4, 4),
					'D' => array(array(2, 43, 43),
						'Z' => array(
							'H' => array(array(2, 4, 4))))),
				'S' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'C' => array(
						'H' => array(array(4, 4, 4))))));
		$length = strlen($string);
		$output = '';
		$i = 0;
		$previous = -1;
		while ($i < $length)
		{
			$current = $last = &$codes[$string[$i]];
			for ($j = $k = 1; $k < 7; $k++)
			{
				if (!isset($string[$i + $k]) || !isset($current[$string[$i + $k]]))
					break;
				$current = &$current[$string[$i + $k]];
				if (isset($current[0]))
				{
					$last = &$current;
					$j = $k + 1;
				}
			}
			if ($i == 0)
				$code = $last[0][0];
			elseif (!isset($string[$i + $j]) || ($codes[$string[$i + $j]][0][0] != 0))
				$code = $is_cyrillic ? (isset($last[1]) ? $last[1][2] : $last[0][2]) : $last[0][2];
			else
				$code = $is_cyrillic ? (isset($last[1]) ? $last[1][1] : $last[0][1]) : $last[0][1];
			if (($code != -1) && ($code != $previous))
				$output .= $code;
			$previous = $code;
			$i += $j;
		}
		return str_pad(substr($output, 0, 6), 6, '0');
	}

//------------------------------------------------------------------------------

	function dmstring($string)
	{
		$is_cyrillic = false;
		if (preg_match('/[А-Яа-я]/iu', $string) === 1)
		{
			$string = translit($string);
			$is_cyrillic = true;
		}







		$firstword = '';
		$lastword = '';


		$words = explode(' ', $string);


		if (preg_match('/[A-Za-z]/iu', $words[0]) !== 1)
		{
			$firstword = $words[0];
		}

		if (preg_match('/[A-Za-z]/iu', $words[count($words)-1]) !== 1)
		{
			$lastword = $words[count($words)-1];
		}






		$string = preg_replace(array('/[^\w\s]|\d/iu', '/\b[^\s]{1,1}\b/iu', '/\s{1,}/iu', '/^\s+|\s+$/iu'), array('', '', ' '), strtoupper($string));


		if ($string[0])
		{
			$matches = explode(' ', $string);
		}  else {
			$matches = array();
		}


		foreach($matches as $key => $match)
			$matches[$key] = dmword($match, $is_cyrillic);


		if ($firstword)
		{
			array_unshift($matches, $firstword);
		}

		if ($lastword)
		{
			$matches[]=$lastword;
		}




		return $matches;
	}

//------------------------------------------------------------------------------

	function translit($string)
	{
		static $ru = array(
			'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
			'Ё', 'ё', 'Ж', 'ж', 'З', 'з', 'И', 'и', 'Й', 'й', 'К', 'к',
			'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',
			'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц',
			'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ', 'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь',
			'Э', 'э', 'Ю', 'ю', 'Я', 'я'
		);
		static $en = array(
			'A', 'a', 'B', 'b', 'V', 'v', 'G', 'g', 'D', 'd', 'E', 'e',
			'E', 'e', 'Zh', 'zh', 'Z', 'z', 'I', 'i', 'J', 'j', 'K', 'k',
			'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'R', 'r',
			'S', 's', 'T', 't', 'U', 'u', 'F', 'f', 'H', 'h', 'C', 'c',
			'Ch', 'ch', 'Sh', 'sh', 'Sch', 'sch', '\'', '\'', 'Y', 'y', '\'', '\'',
			'E', 'e', 'Ju', 'ju', 'Ja', 'ja'
		);
		$string = str_replace($ru, $en, $string);
		return $string;
	}

//------------------------------------------------------------------------------



	$_GET['keywords'] = 'гейм';


        $cyrillic = 0;

		if (preg_match('/[А-Яа-я]/iu', $_GET['keywords']) === 1)
		{
			$cyrillic = 1;
		}


	$coded = dmstring($_GET['keywords']);

	$chars = str_split($coded[count($coded)-1]);

	$coded[count($coded)-1] = '';

	$coded[count($coded)-1] .= $chars[0];

	for ($i=1;$i<=(count($chars)-1);$i++)
	{
		if ($chars[$i]!='0')
		{
			$coded[count($coded)-1] .= $chars[$i];
		}
	}

	$text = implode(',', $coded);
	echo $_GET['keywords'];

	$lnk = 1;

	sys::setTpl();
	sys::filterGet('keywords');
	sys::filterGet('field');
	sys::filterGet('mode');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	$q="
		SELECT
			flm.id,
			flm.name,
			flm.title_orig,
			flm.year,
			flm.coded as `fcode`,
			lng.coded as `lcode`,
			lng.title
		FROM
			`grifix_main_films` AS `flm`
		LEFT JOIN
			`grifix_main_films_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	if($_GET['keywords']!='*')
		$q.="WHERE ";

		if ($cyrillic)
		{
			$q .= "lng.coded LIKE '".$text."%'
			OR lng.coded LIKE '%,".$text."%'";
		} else {
			$q .= "flm.coded LIKE '".$text."%'
			OR flm.coded LIKE '%,".$text."%'";
		}


	$q.=" ORDER BY flm.total_shows DESC LIMIT 50";

//	print_r ($q);


	$r=$_db->query($q);
	$list=array();
	$i=0;

	$films='';

	if (intval($_cfg['sys::lang_id'])=='1')
	{
		$lang = 'ru';
	} else {
		$lang = 'uk';
	}

	while($obj=$_db->fetchAssoc($r))
	{

		$obj['min'] = 1000;



			$keyw = strtoupper($_GET['keywords']);


			$origarray = explode(' ', strtoupper($obj['title_orig']));

			foreach ($origarray as $key=>$value)
			{

				if (levenshtein($keyw, substr($value, 0, strlen($keyw)))<$obj['min'])
					{
						$obj['min'] = 	levenshtein($keyw, substr($value, 0, strlen($keyw)));
					}

			}


			$justarray = explode(' ', strtoupper($obj['title']));

			foreach ($justarray as $key=>$value)
			{

				if (levenshtein($keyw, substr($value, 0, strlen($keyw)))<$obj['min'])
					{
						$obj['min'] = 	levenshtein($keyw, substr($value, 0, strlen($keyw)));
					}

			}





		$qw=$_db->query("
			SELECT
				pst.image as `poster`

			FROM `#__main_films_posters` AS `pst`

			WHERE pst.film_id=".intval($obj['id'])."
			AND pst.order_number=1
			LIMIT 1
		");


		$poster = '';

		while($object=$_db->fetchAssoc($qw))
		{
			$poster = $object['poster'];
		}

		if ($poster)
		{
			$obj['poster'] = _ROOT_URL.'public/main/resize.php?f='.$poster.'&w=30';
		} else {
			$obj['poster'] = _ROOT_URL.'ims/kt_logo.jpg';
		}


		$obj['name'].=' ('.$obj['title'].')';
		$obj['name']=addslashes(($obj['name']));
		$list[$obj['min']]=$obj;
		$i++;
	}

	ksort($list);

	$list = array_values($list);


		foreach ($list as $key=>$obj)
		{

		$films .= '<li id="lnk'.$lnk.'" objid="'.$obj['id'].'" type="film">
		<a class="xImage" style="width: 30px; float: left; margin-right: 10px;" href="'._ROOT_URL.$lang.'/main/film/film_id/'.$obj['id'].'.phtml">
			<img width="30" src="'.$obj["poster"].'">
		</a>
		<a href="'._ROOT_URL.$lang.'/main/film/film_id/'.$obj['id'].'.phtml" style="color: #970301 !important">'.$obj['title'].'|||'.$min.'</a>';

		$lnk++;

		if ($obj['title_orig'] || $obj['year'])
		{
			$films .= '<br><font style="font-size: 10px; color: #AAAAAA;">';

			if ($obj['title_orig'])
			{
				$films .= $obj['title_orig'].', ';
			}

			$films .= $obj['year'].'</font>';
		}

		$films .= '
			<div style="clear: both; height: 1px;"><img src="'._ROOT_URL.'ims/blank.gif"></div>
		</li>';
		}





	print_r ($films);

}
?>