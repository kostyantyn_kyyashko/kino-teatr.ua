<?
function main_person_posters()
{
	sys::useLib('main::persons');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('person_id','int');
	sys::filterGet('poster_id','int');
	sys::filterGet('page','int');

	$limit = 0;
	if(!$_GET['person_id'])	return 404;

		$q="
			SELECT
				pht.person_id,
				pht.poster_id,				
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `person`,
				pst.image,
				flm_lng.title AS `film`
				
			FROM `#__main_persons_posters` AS `pht`
			
			LEFT join `#__main_films_posters` AS `pst`
			ON pht.poster_id = pst.id
			
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=pst.film_id

			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=pht.person_id	AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			
			WHERE 1
		";

	if($_GET['poster_id'])
	{
		$q.="
			AND pht.poster_id=".intval($_GET['poster_id'])."
		";
	}
	if($_GET['person_id'])
	{
		if($_GET['page'])
			$limit=intval($_cfg['main::images_on_page']*(intval($_GET['page'])-1));
			
		$q.="
			AND pht.person_id='".intval($_GET['person_id'])."'
		";
	}

	$q.="
		LIMIT ".$limit.",1
	";
	
	//if(sys::isDebugIP()) $_db->printR($q);

	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);

	if(!$result)
		return 404;

	define('_CANONIC', main_persons::getPersonPostersUrl(intval($result['person_id'])));

	$result['title'] = sys::translate('main::posters').' '.$result['person'];
	
	$result["film"] = htmlspecialchars($result["film"]);
	
	$result['image']=$_cfg['main::films_url'].$result['image']; 
	$result['next_url']=false;
	$result['prev_url']=false;
	$result['delete_photo']=false;
	$result['edit_url']=false;
	$result['add_url']=false;
	$result['url']=$result['url']=sys::getHumanUrl($result['poster_id'],$result['film'],"film_poster");
	$result['alt']=$result['person'];

	$meta['person']=$result['person'];
	$meta['posterid']=doubleval($_GET['poster_id']);
	$meta['film']=$result["film"];
	$result['meta']=sys::parseModTpl('main::person_posters'.($meta['posterid']?'_selected':''),'page',$meta);

	//=============================================================================

	$page_url = main_persons::getPersonPostersUrl($_GET['person_id']);
	$pu = $page_url."?poster_id=".intval($_GET["poster_id"]);
	if ($_GET['page'])	$qp = 'page='.$_GET['page'];
	else  $qp="";

	//Получить инфу об остальных постерах-----------------------------------------------
	$q="
		SELECT pht.poster_id, 
			   pst.image,
			   flm_lng.title AS `film`
			   
		FROM `#__main_persons_posters` as pht

		LEFT join `#__main_films_posters` AS `pst`
		ON pht.poster_id = pst.id
			
		LEFT JOIN `#__main_films_lng` AS `flm_lng`
		ON flm_lng.record_id=pst.film_id AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		
		WHERE `person_id`=".$result['person_id']."
	";

	// Заменить старые ссылки на страницы на новые
	$result['pages']=sys_pages::pocess($q,$_cfg['main::images_on_page'],false);
	$result['pages']['first_page'] = sys_pages::setPageUrl($result['pages']['first_page'], $pu);
	$result['pages']['prev_page']  = sys_pages::setPageUrl($result['pages']['prev_page'],  $pu);
	$result['pages']['next_page']  = sys_pages::setPageUrl($result['pages']['next_page'],  $pu);
	$result['pages']['last_page']  = sys_pages::setPageUrl($result['pages']['last_page'],  $pu);
	foreach ($result['pages']['pages'] as $i=>$p)
	 $result['pages']['pages'][$i] = sys_pages::setPageUrl($result['pages']['pages'][$i],  $pu);

	$r=$_db->query($q.$result['pages']['limit']);
	$result['previews']=array();
	$result['poster_id']=$_GET['poster_id'];
	$i=1;
	while ($obj=$_db->fetchAssoc($r))
	{

  	    $image="x2_".$obj['image'];
  	    
		if(!file_exists($_cfg['main::films_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$obj['image'] = $image;

		$obj["film"] = htmlspecialchars($obj["film"]);

		$obj['url']=$page_url."?poster_id=".doubleval($obj['poster_id']);
		if($qp) $obj['url'] .= ((strpos($obj['url'],"?")===false)?"?":"&").$qp;		
		
		$obj['break']=$i%4;
		$obj['i']=$i;
		$result['previews'][]=$obj;
		$i++;
	}
	//============================================================
	
	//if(sys::isDebugIP()) $_db->printR($result['meta']);
	
	main::countShow($result['person_id'],'person');
	return $result;
}
?>
