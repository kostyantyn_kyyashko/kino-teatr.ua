<?
function main_person_discuss()
{
	sys::useLib('main::persons');
	sys::useLib('main::discuss');
	sys::useLib('main::users');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('person_id','int');

	define('_CANONICAL',main_persons::getPersonDiscussUrl($_GET['person_id']),true);

	if(!$_GET['person_id'])
		return 404;

	$result['person']=main_persons::getPersonFio($_GET['person_id']);
	$meta['person']=$result['person'];
	$result['meta']=sys::parseModTpl('main::person_discuss','page',$meta);
	if(!$result['person'])
		return 404;

	main::countShow($_GET['person_id'],'person');

	sys::jsInclude('sys::bb');
	return $result;
}
?>