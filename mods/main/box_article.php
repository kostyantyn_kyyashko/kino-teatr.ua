<?
function main_box_article()
{
	sys::useLib('main::box');
	sys::useLib('main::films');
	sys::useLib('main::users');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('box_id','int');

	if(!$_GET['article_id'])
		return 404;

	$r=$_db->query("
		SELECT
			art.date,
			art.image,
			art.box_id,
			art.id,
			art.user_id,
			art.comments,
			art.public,
			art_lng.title,
			art_lng.intro,
			art_lng.text,
			usr.login AS `user`
		FROM `#__main_box_articles` AS `art`

		LEFT JOIN
			`#__main_box_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN
			`#__sys_users` AS `usr`
		ON
			usr.id=art.user_id

		WHERE art.id=".intval($_GET['article_id'])."
	");
	$result=$_db->fetchAssoc($r);


	$r=$_db->query("
		SELECT
			SUM(money) as last_week, SUM(visitors) as last_week_visitors
		FROM
			`#__main_box_positions`
		WHERE
			box_id='".intval($_GET['article_id']-1)."'
	");

		while ($obj=$_db->fetchAssoc($r))
		{
			$last_week = $obj['last_week'];
			$last_week_visitors = $obj['last_week_visitors'];
		}

		$result['last_week'] = number_format($last_week,0, ",", " ");
		$result['last_week_visitors'] = number_format($last_week_visitors,0, ",", " ");

	$r=$_db->query("
		SELECT
			date_w_f as date,
			date_w_s,
			date_w_f
		FROM
			`#__main_box_articles`
		WHERE
			id='".intval($_GET['article_id'])."'
	");
		while ($obj=$_db->fetchAssoc($r))
		{
			$peri=' за '.date('d', strtotime($obj['date_w_s'].' +1 days')).' - '.sys::russianDate($obj['date_w_f'].' +1 days');
			$findate = $obj['date'];
		}

	$r=$_db->query("
		SELECT
			box.box_id,
			box.film_id,
			box.position,
			box.money,
			box.visitors,
			box.visitors_total,
			box.screens,
			box.total,
			flm.year,
			flm.title_orig,
			flm.ukraine_premiere,
			lng.title,
			dst.title as dist
		FROM
			`#__main_box_positions` AS `box`
		LEFT JOIN
			`#__main_films` AS `flm`
		ON
			flm.id=box.film_id
		LEFT JOIN
			`#__main_films_lng` AS `lng`
		ON
			box.film_id=lng.record_id
		AND
			lng.lang_id=".$_cfg['sys::lang_id']."
		LEFT JOIN
			`#__main_films_distributors` AS `dstr`
		ON
			box.film_id=dstr.film_id
		LEFT JOIN
			`#__main_distributors_lng` AS `dst`
		ON
			dstr.distributor_id=dst.record_id
		AND
			dst.lang_id=".$_cfg['sys::lang_id']."
		WHERE
			box.box_id='".intval($_GET['article_id'])."'
		ORDER BY
			box.position

	");

//if(sys::isDebugIP()) $_db->printR($findate);

	$total_week = 0;
	$total_visitors = 0;

		while ($obj=$_db->fetchAssoc($r))
		{
				$obj['week']=$box_id;
				$obj['pos']=$obj['position'];

				if ($obj['position']=='1')
				{
					$filmid = $obj['film_id'];
				}

				$obj['film_id']=$obj['film_id'];
				$obj['title-r']=$obj['title'];
				$obj['title-o']=$obj['title_orig'];
				$obj['year']=$obj['year'];
				$obj['date']=$obj['ukraine_premiere'];
				$obj['screens']=$obj['screens'];
				$obj['visitors_']=number_format($obj['visitors'],0, ",", " ");
				$obj['visitors_total']=number_format($obj['visitors_total'],0, ",", " ");
				$obj['dist']=$obj['dist'];
				$obj['usd']=number_format($obj['money'],0, ",", " ");
				$obj['total']=number_format($obj['total'],0, ",", " ");


				$total_week += $obj['money'];
				$total_visitors += $obj['visitors'];

				$now = strtotime($findate)+86400;
				$selected_date = strtotime($obj['ukraine_premiere']);
				//if(sys::isDebugIP()) $_db->printR($now - $selected_date);
				$seconds_diff = max(0, $now - $selected_date);
				$days_diff = $seconds_diff / (60*60*24);

				$obj['days']=round($days_diff);

				$rw=$_db->query("
					SELECT
						*
					FROM
						`#__main_box_positions`
					WHERE
						film_id='".$obj['film_id']."'
					AND
						box_id='".intval($obj['box_id']-1)."'
				");

					while ($object=$_db->fetchAssoc($rw))
					{

							if ($obj['money']>=$object['money'])
							{
								$obj['last_money'] = '+'.round($obj['money']/$object['money']*100-100);
							} else {
								$obj['last_money'] = round($obj['money']/$object['money']*100-100);
							}

						$obj['last_pos'] = $object['position'];
						if (($obj['screens']-$object['screens'])!=0)
						{
							$obj['last_screens'] = '<br>('.($obj['screens']-$object['screens']).')';
						} else {
							$obj['last_screens'] = '';
						}
					}

				if (!$obj['last_pos'])
					$obj['last_pos'] = '-';

				if (!$obj['last_money'])
					$obj['last_money'] = '-';

				$result['films'][$obj['film_id']]=$obj;
		}

//if(sys::isDebugIP()) $_db->printR($result);

	$result['total_week'] = number_format($total_week,0, ",", " ");
	$result['total_visitors'] = number_format($total_visitors,0, ",", " ");

	if ($total_week>=$last_week)
	{
		$week_change = '+'.round($total_week/$last_week*100-100);
	} else {
		$week_change = round($total_week/$last_week*100-100);
	}
	$result['week_change'] = $week_change;
	
	if ($total_visitors>=$last_week_visitors)
	{
		$week_visitors_change = '+'.abs((round($total_visitors/$last_week_visitors*100-100)));
	} else {
		$week_visitors_change = round($total_visitors/$last_week_visitors*100-100);
	}
	$result['week_visitors_change'] = $week_visitors_change;


	if(!$result)
		return 404;

	if(!$result['public'] && !sys::checkAccess('main::box_confirm'))
		return 404;




	main::countShow($_GET['article_id'],'article');




	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::box_delete') || $result['user_id']==$_user['id'])
		{
			main_box::deleteArticle($_GET['article_id']);
			sys::redirect('?mod=main&act=box');
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::box_confirm'))
	{
		main_box::confirmArticle($_GET['article_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}


	$meta['article']=$result['title'];
	$meta['type']=sys::translate('main::ukraine_box_now');
	$meta['period']=$peri;
	$result['meta']=sys::parseModTpl('main::box_article','page',$meta);

   	$poster=main_films::getFilmFirstPoster($filmid);
	if ($poster['image'])
	{
  	    $image="x2_".$poster['image'];	
		if(!file_exists($_cfg['main::films_dir'].$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$result['image'] = $image;
	} else {
		$result['image'] = $_cfg['sys::root_url'].'blank_news_img.jpg';
	}

//           	if ($poster)
//           	{
//				$image=$_cfg['main::films_url'].$poster['image'];
//           	} else {
//         		$image = 'blank_news_img.jpg';
//           	}
//		$result['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=6';



	$result['alt']=htmlspecialchars($result['alt']);
	$result['source']=htmlspecialchars($result['source']);

	$date=sys::db2Timestamp($result['date']);
	$result['year']=date('Y',$date);
	$result['month']=date('m',$date);

 	$result['date']=sys::russianDate($result['date']);
	$result['user_url']=main_users::getUserUrl($result['user_id']);
	$result['edit_url']=false;
	$result['delete']=false;


	if(sys::checkAccess('main::box_edit') || $result['user_id']==$_user['id'])
		$result['edit_url']=main_box::getArticleEditUrl($_GET['article_id']);

	if(sys::checkAccess('main::box_delete') || $result['user_id']==$_user['id'])
		$result['delete']=true;


	return $result;


}
?>