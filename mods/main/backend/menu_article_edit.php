<?
function main_menu_article_edit()
{
	if(!sys::checkAccess('main::menu'))
		return 403;

	global $_db, $_err, $_cfg, $_user;

	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::menu');
	sys::useLib('main::persons');
	sys::useLib('main::films');

	sys::filterGet('id','int');
	sys::filterGet('menu_id','int',0);

	if(!isset($_POST['_task']))
		$_POST['_task']=false;


	$tbl='main_menu_articles';
	$default['menu_id']=$_GET['menu_id'];
	$default['public']=true;
	$default['date']=strftime($_cfg['sys::date_time_format']);
	$default['dateend']=strftime($_cfg['sys::date_time_format']);
	$default['persons']=array();
	$default['films']=array();

	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id'],true);
		if(!$record)
			return 404;
		$title=$record['name'];
		$record['persons']=main_menu::getArticlePersonsIds($_GET['id']);
		$record['films']=main_menu::getArticleFilmsIds($_GET['id']);
		$record['date']=sys::db2Date($record['date'],true,$_cfg['sys::date_time_format']);
		$record['dateend']=sys::db2Date($record['dateend'],true,$_cfg['sys::date_time_format']);
	}
	else
	{
		$record=false;
		$title=sys::translate('main::new_article');
	}

	$values=sys::setValues($record, $default);
	$values['persons']=main_persons::getPersonsNames($values['persons']);
	$values['films']=main_films::getFilmsNames($values['films']);
	//===========================================================================================================//


	//Поля и форма----------------------------------------------------------------------------------------------

	//menu_id
	$fields['parent_id']['input']='selectbox';
	$fields['parent_id']['type']='int';
	$fields['parent_id']['table']=$tbl;
	$fields['parent_id']['title']=sys::translate('main::parent');
	$r=$_db->query("SELECT `id`, `name` FROM `#__main_menu_articles` ORDER BY `id`");
	$fields['parent_id']['values'][0]='Корень';
	while ($section=$_db->fetchAssoc($r))
	{
		$fields['parent_id']['values'][$section['id']]=$section['name'];
	}

	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="55"';

	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="55"';

	//name
	$fields['link']['input']='textbox';
	$fields['link']['type']='text';
	$fields['link']['req']=true;
	$fields['link']['table']=$tbl;
	$fields['link']['title']=sys::translate('main::link');
	$fields['link']['html_params']='style="width:100%" maxlength="55"';

	//name
	$fields['class']['input']='textbox';
	$fields['class']['type']='text';
	$fields['class']['req']=true;
	$fields['class']['table']=$tbl;
	$fields['class']['html_params']='style="width:100%" maxlength="55"';

	//public
	$fields['public']['input']='checkbox';
	$fields['public']['type']='bool';
	$fields['public']['table']=$tbl;

	//=========================================================================================================//

	//Проверка и сохранение данных--------------------------------------------------------------------------
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{


		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['index']=strip_tags($values['text']);
			$values['date']=sys::date2Db($values['date'],true,$_cfg['sys::date_time_format']);
			$values['dateend']=sys::date2Db($values['dateend'],true,$_cfg['sys::date_time_format']);

			//Вставка данных
			if(!$_GET['id'])
			{
				$values['user_id']=$_user['id'];
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['date']=sys::db2Date($values['date'],true,$_cfg['sys::date_time_format']);
			$values['dateend']=sys::db2Date($values['dateend'],true,$_cfg['sys::date_time_format']);
		}
		if(!$_err)
		{
			//Загрузить и отресайзить картинку
			if($_FILES['image']['name'])
				$values['image']=main_menu::uploadArticleImage($_FILES['image'],$_GET['id'],$values['image']);

			if($_FILES['small_image']['name'])
				$values['small_image']=main_menu::uploadArticleSmallImage($_FILES['small_image'],$_GET['id'],$values['small_image']);

			main_menu::linkArticlePersons($_GET['id'],$values['persons']);
			main_menu::linkArticleFilms($_GET['id'],$values['films']);
			main_menu::setArticleIndex($_GET['id'],$values);
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//

	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			$_db->setValue('main_menu_articles','image',false,$_GET['id']);
			main_menu::deleteArticleImage($values['image']);
			$values['image']=false;
		}

		if($task[1]=='small_image')
		{
			$_db->setValue('main_menu_articles','small_image',false,$_GET['id']);
			if(is_file($_cfg['main::menu_dir'].$values['small_image']))
				unlink($_cfg['main::menu_dir'].$values['small_image']);
			$values['small_image']=false;
		}
	}
	//==================================================================================================

	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//

	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================

	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>