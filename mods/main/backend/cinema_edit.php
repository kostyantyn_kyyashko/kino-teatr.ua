<?
function main_cinema_edit()
{
	if(!sys::checkAccess('main::cinemas'))
		return 403;

	global $_db, $_err, $_cfg;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::cinemas');

	sys::filterGet('id','int');

	if(!isset($_POST['_task']))
		$_POST['_task']=false;

	$tbl='main_cinemas';
	$default=array();
	$default['user_id']=false;
	$default['order_number']=$_db->getMaxOrderNumber($tbl);

	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,intval($_GET['id']),true);

		$record['notice_begin']=date('d.m.Y',strtotime($record['notice_begin']));
		$record['notice_end']=date('d.m.Y',strtotime($record['notice_end']));
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else
	{
		$record=false;
		$title=sys::translate('main::new_cinema');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//

	//Поля и форма----------------------------------------------------------------------------------------------

	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';

	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="255"';

	//user_id
	$fields['user_id']['input']='searchbox';
	$fields['user_id']['type']='int';
	if(!$values['user_id'])
		$fields['user_id']['text']=sys::translate('sys::not_chosed');
	else
	{
		$fields['user_id']['text']=$_db->getValue('sys_users','login',$values['user_id']);
	}
	$fields['user_id']['table']=$tbl;
	$fields['user_id']['title']=sys::translate('main::user');
	$fields['user_id']['onkeyup']="searchInList(this,'2','user_id','?mod=sys&act=ajx_users_list&mode=value');";

	//city_id
	$fields['city_id']['input']='selectbox';
	$fields['city_id']['type']='int';
	$fields['city_id']['req']=true;
	$fields['city_id']['table']=$tbl;
	$fields['city_id']['title']=sys::translate('main::city');
	$r=$_db->query("SELECT `id`, `name` FROM `#__main_countries_cities` ORDER BY `order_number`");
	while ($city=$_db->fetchAssoc($r))
	{
		$fields['city_id']['values'][$city['id']]=$city['name'];
	}
	$fields['city_id']['empty']='';

	//address
	$fields['address']['input']='textarea';
	$fields['address']['type']='html';
	$fields['address']['table']=$tbl;
	$fields['address']['multi_lang']=true;
	$fields['address']['html_params']='style="width:100%; height:150px"';
	$fields['address']['title']=sys::translate('main::address');

	//phone
	$fields['phone']['input']='textbox';
	$fields['phone']['type']='text';
	$fields['phone']['table']=$tbl;
	$fields['phone']['html_params']='style="width:100%" maxlength="255"';
	$fields['phone']['title']=sys::translate('main::phone');

	//site
	$fields['site']['input']='textbox';
	$fields['site']['type']='text';
	$fields['site']['table']=$tbl;
	$fields['site']['html_params']='style="width:100%" maxlength="255"';

	//site
	$fields['ticket_url']['input']='textbox';
	$fields['ticket_url']['type']='text';
	$fields['ticket_url']['table']=$tbl;
	$fields['ticket_url']['html_params']='style="width:100%" maxlength="255"';
	$fields['ticket_url']['title']=sys::translate('main::ticket_url');

	//site
	$fields['latitude']['input']='textbox';
	$fields['latitude']['type']='text';
	$fields['latitude']['table']=$tbl;
	$fields['latitude']['html_params']='style="width:100%" maxlength="255"';

	//site
	$fields['longitude']['input']='textbox';
	$fields['longitude']['type']='text';
	$fields['longitude']['table']=$tbl;
	$fields['longitude']['html_params']='style="width:100%" maxlength="255"';

	//map
	$fields['map']['input']='filebox';
	$fields['map']['type']='file';
	$fields['map']['file::dir']=$_cfg['main::cinemas_dir'];
	$fields['map']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['map']['preview']=true;
	$fields['map']['title']=sys::translate('main::map');

	//intro
	$fields['seosm']['input']='fck';
	$fields['seosm']['type']='html';
	$fields['seosm']['table']=$tbl;
	$fields['seosm']['multi_lang']=true;
	$fields['seosm']['html_params']='style="width:100%; height:50px"';
	$fields['seosm']['height']='300px';
	$fields['seosm']['title']='Короткий SEO';

	//intro
	$fields['seofull']['input']='fck';
	$fields['seofull']['type']='html';
	$fields['seofull']['table']=$tbl;
	$fields['seofull']['multi_lang']=true;
	$fields['seofull']['html_params']='style="width:100%; height:50px"';
	$fields['seofull']['height']='300px';
	$fields['seofull']['title']='Полный текст SEO';

	//mapia_code
	$fields['mapia_code']['input']='textarea';
	$fields['mapia_code']['type']='text';
	$fields['mapia_code']['table']=$tbl;
	$fields['mapia_code']['html_params']='style="width:100%; height:50px"';
	$fields['mapia_code']['title']=sys::translate('main::mapia_code');

	//notice
	$fields['notice']['input']='fck';
	$fields['notice']['type']='html';
	$fields['notice']['table']=$tbl;
	$fields['notice']['multi_lang']=true;
	$fields['notice']['html_params']='style="width:100%; height:50px"';
	$fields['notice']['height']='300px';
	$fields['notice']['title']=sys::translate('main::notice');

	//notice_begin
	$fields['notice_begin']['input']='datebox';
	$fields['notice_begin']['table']=$tbl;
	$fields['notice_begin']['title']=sys::translate('main::notice_begin');

	//notice_end
	$fields['notice_end']['input']='datebox';
	$fields['notice_end']['table']=$tbl;
	$fields['notice_end']['title']=sys::translate('main::notice_end');

	//text
	$fields['text']['input']='fck';
	$fields['text']['type']='html';
	$fields['text']['table']=$tbl;
	$fields['text']['multi_lang']=true;
	$fields['text']['html_params']='style="width:100%; height:50px"';
	$fields['text']['height']='300px';

	//first_screen
	$fields['first_screen']['input']='checkbox';
	$fields['first_screen']['type']='bool';
	$fields['first_screen']['table']=$tbl;
	$fields['first_screen']['title']=sys::translate('main::first_screen');

	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
		//banner
	$fields['banner']['input']='textarea';
	$fields['banner']['type']='html';
	$fields['banner']['table']=$tbl;
	$fields['banner']['html_params']='style="width:100%; height:50px"';
	$fields['banner']['title']=sys::translate('main::banner');
	//=========================================================================================================//

	//Проверка и сохранение данных--------------------------------------------------------------------------
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{


		$record['notice_begin']=date('Y-m-d',strtotime($record['notice_begin']));
		$record['notice_end']=date('Y-m-d',strtotime($record['notice_end']));



		if(!$_err=sys_check::checkValues($fields,$values))
		{


			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}

		}


		if(!$_err)
		{
			if($_FILES['map']['name'])
				$values['map']=main_cinemas::uploadCinemaMap($_FILES['map'],$_GET['id'],$values['map']);
			sys_gui::afterSave();
		}


	}
	//==========================================================================================================//

	//Удаление карты
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='map')
		{
			$_db->setValue('main_cinemas','map',false,$_GET['id']);
			main_cinemas::deleteMapImage($values['map']);
			$values['map']=false;
		}
	}
	//==================================================================================================

	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//

	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================

	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	if($_GET['id'])
	{
		$panel['class']='blank';
		$panel['text']=main_cinemas::showCinemaAdminTabs($_GET['id'],'info');
		$result['panels'][]=$panel;
	}
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>