<?
function main_anounce_article_edit()
{
	if(!sys::checkAccess('main::anounce'))
		return 403;

	global $_db, $_err, $_cfg, $_user;

	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::anounce');
	sys::useLib('main::persons');
	sys::useLib('main::films');

	sys::filterGet('id','int');
	sys::filterGet('anounce_id','int',0);

	if(!isset($_POST['_task']))
		$_POST['_task']=false;


	$tbl='main_anounce_articles';
	$default['anounce_id']=$_GET['anounce_id'];
	$default['public']=true;
	$default['date']=strftime($_cfg['sys::date_time_format']);
	$default['persons']=array();
	$default['films']=array();

	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id'],true);
		if(!$record)
			return 404;
		$title=$record['name'];
		$record['persons']=main_anounce::getArticlePersonsIds($_GET['id']);
		$record['films']=main_anounce::getArticleFilmsIds($_GET['id']);
		$record['date']=sys::db2Date($record['date'],true,$_cfg['sys::date_time_format']);
	}
	else
	{
		$record=false;
		$title=sys::translate('main::new_article');
	}

	$values=sys::setValues($record, $default);
	$values['persons']=main_persons::getPersonsNames($values['persons']);
	$values['films']=main_films::getFilmsNames($values['films']);
	//===========================================================================================================//


	//Поля и форма----------------------------------------------------------------------------------------------

	//anounce_id
	$fields['anounce_id']['input']='selectbox';
	$fields['anounce_id']['type']='int';
	$fields['anounce_id']['table']=$tbl;
	$fields['anounce_id']['title']=sys::translate('main::section');
	$r=$_db->query("SELECT `id`, `name` FROM `#__main_anounce` ORDER BY `order_number`");
	while ($section=$_db->fetchAssoc($r))
	{
		$fields['anounce_id']['values'][$section['id']]=$section['name'];
	}

	//user_id
	$fields['user_id']['input']='hidden';
	$fields['user_id']['type']='int';
	$fields['user_id']['table']=$tbl;

	//city_id
	$fields['city_id']['input']='selectbox';
	$fields['city_id']['type']='int';
	$fields['city_id']['table']=$tbl;
	$fields['city_id']['title']=sys::translate('main::city');
	$r=$_db->query("SELECT `id`, `name` FROM `#__main_countries_cities` ORDER BY `order_number`");
	$fields['city_id']['values'][0]=sys::translate('main::all_cities');
	while ($city=$_db->fetchAssoc($r))
	{
		$fields['city_id']['values'][$city['id']]=$city['name'];
	}


	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="55"';

	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="55"';

	//date
	$fields['date']['input']='datebox';
	$fields['date']['type']='date';
	$fields['date']['req']=true;
	$fields['date']['table']=$tbl;
	$fields['date']['format']=$_cfg['sys::date_time_format'];

	//small_image
	$fields['small_image']['input']='filebox';
	$fields['small_image']['type']='file';
	$fields['small_image']['file::dir']=$_cfg['main::anounce_dir'];
	$fields['small_image']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['small_image']['title']=sys::translate('main::small_image');
	$fields['small_image']['preview']=true;

	//image
	$fields['image']['input']='filebox';
	$fields['image']['type']='file';
	$fields['image']['file::dir']=$_cfg['main::anounce_dir'];
	$fields['image']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image']['title']=sys::translate('main::image');
	$fields['image']['preview']=true;
	$fields['image']['preview::prefix']='x2_';

	//intro
	$fields['intro']['input']='textarea';
	$fields['intro']['type']='html';
	$fields['intro']['table']=$tbl;
	$fields['intro']['multi_lang']=true;
	$fields['intro']['html_params']='style="width:100%; height:50px"';
	$fields['intro']['height']='300px';
	$fields['intro']['title']=sys::translate('main::intro');

	//source_url
	$fields['source_url']['input']='textbox';
	$fields['source_url']['type']='text';
	$fields['source_url']['table']=$tbl;
	$fields['source_url']['html_params']='style="width:100%" maxlength="255"';
	$fields['source_url']['title']=sys::translate('main::source_url');

	//source_url
	$fields['ua_url']['input']='textbox';
	$fields['ua_url']['type']='text';
	$fields['ua_url']['table']=$tbl;
	$fields['ua_url']['html_params']='style="width:100%" ';
	$fields['ua_url']['title']='Ссылка (украинская)';

	//films
	$fields['film']['input']='textbox';
	$fields['film']['type']='text';
	$fields['film']['title']=sys::translate('main::film').' ID';
	$fields['film']['table']=$tbl;
	$fields['film']['req']=false;

	//exclusive
	$fields['exclusive']['input']='checkbox';
	$fields['exclusive']['type']='bool';
	$fields['exclusive']['table']=$tbl;
	$fields['exclusive']['title']=sys::translate('main::exclusive');

	//public
	$fields['public']['input']='checkbox';
	$fields['public']['type']='bool';
	$fields['public']['table']=$tbl;

	//=========================================================================================================//

	//Проверка и сохранение данных--------------------------------------------------------------------------
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['index']=strip_tags($values['text']);
			$values['date']=sys::date2Db($values['date'],true,$_cfg['sys::date_time_format']);

			//Вставка данных
			if(!$_GET['id'])
			{
				$values['user_id']=$_user['id'];
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['date']=sys::db2Date($values['date'],true,$_cfg['sys::date_time_format']);
		}
		if(!$_err)
		{
			//Загрузить и отресайзить картинку
			if($_FILES['image']['name'])
				$values['image']=main_anounce::uploadArticleImage($_FILES['image'],$_GET['id'],$values['image']);

			if($_FILES['small_image']['name'])
				$values['small_image']=main_anounce::uploadArticleSmallImage($_FILES['small_image'],$_GET['id'],$values['small_image']);

			main_anounce::linkArticlePersons($_GET['id'],$values['persons']);
			main_anounce::linkArticleFilms($_GET['id'],$values['films']);
			main_anounce::setArticleIndex($_GET['id'],$values);
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//

	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			$_db->setValue('main_anounce_articles','image',false,$_GET['id']);
			main_anounce::deleteArticleImage($values['image']);
			$values['image']=false;
		}

		if($task[1]=='small_image')
		{
			$_db->setValue('main_anounce_articles','small_image',false,$_GET['id']);
			if(is_file($_cfg['main::anounce_dir'].$values['small_image']))
				unlink($_cfg['main::anounce_dir'].$values['small_image']);
			$values['small_image']=false;
		}
	}
	//==================================================================================================

	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//

	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================

	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>