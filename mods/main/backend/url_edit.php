<?
function main_url_edit()
{
	if(!sys::checkAccess('content::content'))
		return 403;
		
	global $_db, $_err, $_user;
		
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('content::objects');
	
	sys::filterGet('id','int');
	sys::filterGet('parent_id','int',0);
	sys::filterGet('no_close','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl_obj='content_objects';
	$type_id=1;
	$default=content_objects::getDefaultValues($type_id,$_GET['parent_id']);
	$type=$_db->getRecord('content_types',$type_id);
	
	
	if($_GET['id'])
	{
		$record=content_objects::getRecord($_GET['id'],$type_id);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_'.$type['name']);
	}
	
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	if(!$_GET['id'])
	{
		$fields=content_objects::getHiddenFields();
	}
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl_obj;
	$fields['name']['max_chars']=255;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl_obj;
	$fields['title']['max_chars']=255;
	$fields['title']['html_params']='style="width:100%" maxlength="255"';
	
	//menu
	$fields['menu']['input']='checkbox';
	$fields['menu']['type']='bool';
	$fields['menu']['table']=$tbl_obj;
	$fields['menu']['title']=sys::translate('content::show_in_menu');
	
	//short_title
	$fields['short_title']['input']='textbox';
	$fields['short_title']['type']='text';
	$fields['short_title']['multi_lang']=true;
	$fields['short_title']['table']=$tbl_obj;
	$fields['short_title']['max_chars']=55;
	$fields['short_title']['html_params']='size="55" style="maxlength="55"';
	$fields['short_title']['title']=sys::translate('content::short_title');
	$fields['short_title']['info']=sys::getHlp('content::short_title');
	
	//url
	$fields['url']['input']='textbox';
	$fields['url']['type']='text';
	$fields['url']['req']=true;
	$fields['url']['table']=$tbl_obj;
	$fields['url']['max_chars']=255;
	$fields['url']['html_params']='style="width:100%" maxlength="255"';
	
	//public
	$fields['public']['input']='checkbox';
	$fields['public']['type']='bool';
	$fields['public']['table']=$tbl_obj;
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=content_objects::insertObject($type_id,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!content_objects::updateObject($type_id,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	if(!$_GET['no_close'])
		$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>