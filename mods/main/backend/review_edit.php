<?
function main_review_edit()
{
	if(!sys::checkAccess('main::reviews'))
		return 403;
		
	global $_db, $_err, $_cfg, $_user;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::reviews');
	sys::useLib('main::films');
	sys::useLib('main::spec_themes');
	sys::useLib('main::genres');
	$text_type = "reviews";
		
	sys::filterGet('id','int');
	sys::filterGet('film_id','int',0);
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='main_reviews';
	$default['film_id']=$_GET['film_id'];
	$default['public']=true;
	$default['date']=strftime($_cfg['sys::date_time_format']);
	$default['spec_themes']=array();
	$default['genres']=array();
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$title=$record['title'];
		$record['date']=sys::db2Date($record['date'],true);
		$record['spec_themes']=main_reviews::getArticleSpecThemesIds($_GET['id']);
		$record['genres']=main_genres::getTextGenresIds($_GET['id'], $text_type);
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_review');
	}
	
	$values=sys::setValues($record, $default);
	$values['spec_themes']=main_spec_themes::getSpecThemesNames($values['spec_themes']);
	$values['genres']=main_genres::getGenresNames($values['genres']);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//user_id
	$fields['user_id']['input']='hidden';
	$fields['user_id']['type']='int';
	$fields['user_id']['table']=$tbl;
	
	//film_id
	$fields['film_id']['input']='searchbox';
	$fields['film_id']['type']='int';
	if(!$values['film_id'])
		$fields['film_id']['text']=sys::translate('sys::not_chosed');
	else
	{
		$fields['film_id']['text']=$_db->getValue('main_films','name',$values['film_id']);
	}
	$fields['film_id']['table']=$tbl;
	$fields['film_id']['title']=sys::translate('main::film');
	$fields['film_id']['req']=true;
	$fields['film_id']['onkeyup']="searchInList(this,'2','film_id','?mod=main&act=ajx_films_list&mode=value');";
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="100"';
	
	//date
	$fields['date']['input']='datebox';
	$fields['date']['type']='date';
	$fields['date']['req']=true;
	$fields['date']['table']=$tbl;
	$fields['date']['format']=$_cfg['sys::date_time_format'];
	
	//image
	$fields['image']['input']='filebox';
	$fields['image']['type']='file';
	$fields['image']['file::dir']=$_cfg['main::reviews_dir'];
	$fields['image']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image']['title']=sys::translate('main::image');
	$fields['image']['preview']=true;
	$fields['image']['preview::prefix']='x2_';
	
	//intro
	$fields['intro']['input']='fck';
	$fields['intro']['type']='html';
	$fields['intro']['table']=$tbl;
	$fields['intro']['html_params']='style="width:100%; height:50px"';
	$fields['intro']['height']='300px';
	$fields['intro']['title']=sys::translate('main::intro');
	
	//text
	$fields['text']['input']='fck';
	$fields['text']['type']='html';
	$fields['text']['table']=$tbl;
	$fields['text']['html_params']='style="width:100%; height:50px"';
	$fields['text']['height']='300px';

	//spec_themes
	$fields['spec_themes']['input']='multisearchbox';
	$fields['spec_themes']['type']='text';
	$fields['spec_themes']['onkeyup']="searchInList(this,'2','spec_themes','?mod=main&act=ajx_spec_themes_list&mode=option');";
	$fields['spec_themes']['title']=sys::translate('main::spec_themes');
	
	//genres
	$fields['genres']['input']='multisearchbox';
	$fields['genres']['type']='text';
	$fields['genres']['onkeyup']="searchInList(this,'2','genres','?mod=main&act=ajx_genres_list&mode=option');";
	$fields['genres']['title']=sys::translate('main::genres');
	
	//mark
	$fields['mark']['input']='textbox';
	$fields['mark']['type']='int';
	$fields['mark']['req']=true;
	$fields['mark']['min_value']=1;
	$fields['mark']['max_value']=10;
	$fields['mark']['table']=$tbl;
	$fields['mark']['html_params']='size="2" maxlength="55"';
	$fields['mark']['title']=sys::translate('main::mark');
	
	//public
	$fields['public']['input']='checkbox';
	$fields['public']['type']='bool';
	$fields['public']['table']=$tbl;
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['date']=sys::date2Db($values['date'],true);
			
			//Вставка данных
			if(!$_GET['id'])
			{
				$values['user_id']=$_user['id'];
				main_films::proMarkFilm($values['film_id'],$values['mark']);
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				main_films::proMarkFilm($values['film_id'],$values['mark'],$_GET['id']);
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['date']=sys::db2Date($values['date'],true);
		}
		if(!$_err)
		{
			//Загрузить и отресайзить картинку	
			if($_FILES['image']['name'])
				$values['image']=main_reviews::uploadReviewImage($_FILES['image'],$_GET['id'],$values['image']);
			main_reviews::linkArticleSpecThemes($_GET['id'],$values['spec_themes']);
			main_genres::linkTextsGenres($_GET['id'], $text_type, $values['genres']);
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			$_db->setValue('main_reviews','image',false,$_GET['id']);
			main_reviews::deleteReviewImage($values['image']);
			$values['image']=false;
		}
	}
	//==================================================================================================
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>