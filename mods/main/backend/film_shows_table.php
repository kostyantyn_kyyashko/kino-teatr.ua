<?
function main_film_shows_table()
{
	if(!sys::checkAccess('main::films'))
		return 403;
		
	global $_db, $_err, $_cfg;
	sys::useLib('sys::pages');
	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.desc');
	sys::filterGet('film_id','int');
		
	if(!$_GET['film_id'])
		return 404;
		
	$tbl='main_shows';
	$edit_url='?mod=main&act=show_edit';
	$win_params=sys::setWinParams('600','500');
	$add_title=sys::translate('main::add_show');
	$order_where=$where=" `film_id`=".intval($_GET['film_id'])."";
	
	$film=$_db->getValue('main_films','name',$_GET['film_id']);
	
	if(!$film)
		return 404;
	
	$title=$film.' :: '.sys::translate('main::shows');
	
	$q="SELECT 
			shw.id, 
			shw.hall_id,
			CONCAT(cnm.name,'::',hls.name,' (',cts.name,')') AS `cinema`,
			shw.begin,
			shw.end
		FROM `#__".$tbl."` AS `shw`
		
		LEFT JOIN `#__main_cinemas_halls` AS `hls`
		ON hls.id=shw.hall_id
		
		LEFT JOIN `#__main_cinemas` AS `cnm`
		ON cnm.id=hls.cinema_id
		
		LEFT JOIN `#__main_countries_cities` AS `cts`
		ON cts.id=cnm.city_id
		
		WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_shows::deleteShow($task[1]);
		
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_shows::deleteShow($key);
				}
			}
					
		}	
		
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['begin']=sys::db2Date($obj['begin'],false, $_cfg['sys::date_format']);
		$obj['end']=sys::db2Date($obj['end'],false, $_cfg['sys::date_format']);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['times']['type']='button';
	$table['fields']['times']['onclick']="window.open('?mod=main&act=show_times_table&show_id=%id%','','".$win_params."');return false";
	$table['fields']['times']['url']='?mod=main&act=show_times_table&show_id=%id%';
	$table['fields']['times']['title']=sys::translate('main::show_times');
	$table['fields']['times']['image']='main::btn.clock.gif';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['begin']['type']='text';
	$table['fields']['begin']['order']=true;
	$table['fields']['begin']['title']=sys::translate('main::begin');
	
	$table['fields']['end']['type']='text';
	$table['fields']['end']['order']=true;
	$table['fields']['end']['title']=sys::translate('main::end');
	
	$table['fields']['cinema']['type']='text';
	$table['fields']['cinema']['order']=true;
	$table['fields']['cinema']['onclick']="window.open('?mod=main&act=cinema_hall_shows_table&hall_id=%hall_id%','','".$win_params."');return false";
	$table['fields']['cinema']['url']='?mod=main&act=cinema_hall_shows_table&hall_id=%hall_id%';
	$table['fields']['cinema']['title']=sys::translate('main::cinema');
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&film_id=".$_GET['film_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=main_films::showFilmAdminTabs($_GET['film_id'],'shows');
	$result['panels'][]=$panel;
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
