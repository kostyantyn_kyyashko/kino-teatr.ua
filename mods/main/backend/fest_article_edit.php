<?
function main_fest_article_edit()
{
	if(!sys::checkAccess('main::news'))
		return 403;

	global $_db, $_err, $_cfg, $_user;

	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::fest');
	sys::useLib('main::persons');
	sys::useLib('main::films');

	sys::filterGet('id','int');
	sys::filterGet('fest_id','int',0);

	if(!isset($_POST['_task']))
		$_POST['_task']=false;


	$tbl='main_fest_articles';
	$default['fest_id']=$_GET['fest_id'];
	$default['public']=true;
	$default['date']=strftime($_cfg['sys::date_time_format']);


	if($_GET['id'])
	{
		$r=$_db->query("SELECT * FROM `#__main_fest_articles` WHERE `id`='".$_GET['id']."'");
		$record=$_db->fetchAssoc($r);


		$r=$_db->query("SELECT * FROM `#__main_fest_articles_lng` WHERE `record_id`='".$_GET['id']."' AND `lang_id`='1'");
		while ($section=$_db->fetchAssoc($r))
		{
			$record['title_ru']=$section['title'];
			$record['intro_ru']=$section['intro'];
			$record['text_ru']=$section['text'];
		}

		$r=$_db->query("SELECT * FROM `#__main_fest_articles_lng` WHERE `record_id`='".$_GET['id']."' AND `lang_id`='3'");
		while ($section=$_db->fetchAssoc($r))
		{
			$record['title_uk']=$section['title'];
			$record['intro_uk']=$section['intro'];
			$record['text_uk']=$section['text'];
		}

		for($i=1;$i<=10;$i++)
		{
			$r=$_db->query("SELECT * FROM `#__main_fest_positions` WHERE `fest_id`='".$_GET['id']."' AND `position`='".$i."'");
			while ($section=$_db->fetchAssoc($r))
			{
				$record['pos_'.$i.'_id']=$section['film_id'];
				$record['pos_'.$i.'_mon']=$section['money'];
				$record['pos_'.$i.'_scr']=$section['screens'];
				$record['pos_'.$i.'_tot']=$section['total'];
			}
		}
//		$record=$_db->getRecord($tbl,$_GET['id'],true);

		if(!$record)
			return 404;
		$title=$record['name'];
		$record['date']=sys::db2Date($record['date'],true,$_cfg['sys::date_time_format']);
		$record['date_w_s'] = date('d.m.Y H:i:s', strtotime($record['date_w_s']));
  	}
	else
	{
		$record=false;
		$title=sys::translate('main::new_article');
	}

	$values=sys::setValues($record, $default);
	$values['persons']=main_persons::getPersonsNames($values['persons']);
	$values['films']=main_films::getFilmsNames($values['films']);
	//===========================================================================================================//


	//Поля и форма----------------------------------------------------------------------------------------------


	//fest_id
	$fields['fest_id']['input']='selectbox';
	$fields['fest_id']['type']='int';
	$fields['fest_id']['table']=$tbl;
	$fields['fest_id']['title']=sys::translate('main::section');
	$r=$_db->query("SELECT `id`, `name` FROM `#__main_fest` ORDER BY `order_number`");
	while ($section=$_db->fetchAssoc($r))
	{
		$fields['fest_id']['values'][$section['id']]=$section['name'];
	}

	//user_id
	$fields['user_id']['input']='hidden';
	$fields['user_id']['type']='int';
	$fields['user_id']['table']=$tbl;


	//films
	$fields['films']['input']='searchbox';
	$fields['films']['type']='text';
	$fields['films']['onkeyup']="searchInList(this,'2','films','?mod=main&act=ajx_films_list&mode=option2');\" id=\"film_name\"";
	$fields['films']['title']='Изменить фильм';

	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100px" maxlength="55" id="film_id"';
	$fields['name']['title']='ID фильма';


	//date
	$fields['date_w_s']['input']='datebox';
	$fields['date_w_s']['type']='date';
	$fields['date_w_s']['title']='Дата и время показа';
	$fields['date_w_s']['req']=true;
	$fields['date_w_s']['table']=$tbl;
	$fields['date_w_s']['format']=$_cfg['sys::date_time_format'];





	//public
	$fields['public']['input']='checkbox';
	$fields['public']['type']='bool';
	$fields['public']['table']=$tbl;


	//date
	$fields['date']['input']='datebox';
	$fields['date']['type']='date';
	$fields['date']['title']='Дата публикации';
	$fields['date']['req']=true;
	$fields['date']['table']=$tbl;
	$fields['date']['format']=$_cfg['sys::date_time_format'];




	//=========================================================================================================//

	//Проверка и сохранение данных--------------------------------------------------------------------------
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{




		if(!$_err=sys_check::checkValues($fields,$values))
		{

			$values['date']=sys::date2Db($values['date'],true,$_cfg['sys::date_time_format']);



			$values['date_w_s']=date('Y-m-d H:i:s',strtotime($values['date_w_s']));

			if ($values['public']=='on')
			{				$values['public'] = '1';
			} else {				$values['public'] = '0';
			}


			//Вставка данных
			if(!$_GET['id'])
			{
				$values['user_id']=$_user['id'];

				$q2 = "SELECT MAX(`id`) as maxid FROM #__main_fest_articles";
				$newid=$_db->query($q2);
				while ($nid=$_db->fetchAssoc($newid))
				{
					$neid = $nid['maxid']+1;
				}


				$q="INSERT INTO `#__main_fest_articles`
				(`id`, `fest_id`, `user_id`,
				`name`, `date`, `date_w_s`,
				`public`, `comments`, `today_shows`, `total_shows`)
				VALUES ('".$neid."', '".$values['fest_id']."', '".$values['user_id']."',
				'".$values['name']."', '".$values['date']."', '".$values['date_w_s']."',
				'".$values['public']."', 0, 0, 0);
				";

				$made=$_db->query($q);


				$q = '';


			}
			else //Обновление данных
			{

				$values['user_id']=$_user['id'];

				$neid = $_GET['id'];

				$q="UPDATE `#__main_fest_articles` SET `fest_id`='".$values['fest_id']."',
				`user_id`='".$values['user_id']."',	`name`='".$values['name']."', `date`='".$values['date']."',
				`date_w_s`='".$values['date_w_s']."',
				`public`='".$values['public']."' WHERE `id`='".$_GET['id']."'";

				$made=$_db->query($q);

				$q = '';


			}

			$values['date']=sys::db2Date($values['date'],true,$_cfg['sys::date_time_format']);
			$values['date_w_s']=date('Y-m-d H:i:s',strtotime($values['date_w_s']));


		}
		if(!$_err)
		{


			sys_gui::afterSave();



		}


	}
	//==========================================================================================================//

	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			$_db->setValue('main_fest_articles','image',false,$_GET['id']);
			main_fest::deleteArticleImage($values['image']);
			$values['image']=false;
		}

		if($task[1]=='small_image')
		{
			$_db->setValue('main_fest_articles','small_image',false,$_GET['id']);
			if(is_file($_cfg['main::fest_dir'].$values['small_image']))
				unlink($_cfg['main::fest_dir'].$values['small_image']);
			$values['small_image']=false;
		}
	}
	//==================================================================================================

	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//

	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================

	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>