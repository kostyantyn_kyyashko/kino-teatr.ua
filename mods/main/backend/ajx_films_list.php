<?
function main_ajx_films_list()
{
	global $_db, $_err, $_cfg;
	sys::filterGet('keywords');
	sys::filterGet('field');
	sys::filterGet('mode');
	sys::setTpl();
	if(!$_GET['keywords'])
		return false;
	$q="
		SELECT
			flm.id,
			flm.name,
			lng.title,
			3d as film_3d,
			year
		FROM
			`#__main_films` AS `flm`
		LEFT JOIN
			`#__main_films_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	if($_GET['keywords']!='*')
		$q.="WHERE `name` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `title` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'";

	$q.=" ORDER BY flm.name limit 0,100";
	$r=$_db->query($q);
	$list=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['name'].=' ('.$obj['title'].')';
		$obj['name'].= (' '.$obj['year'].'г.');
		if($obj['film_3d']) $obj['name'].=' [3D]';
		$obj['name']=addslashes(($obj['name']));
		$list[]=$obj;
	}




	switch ($_GET['mode'])
	{
		case 'option':
			echo sys_gui::showOptionsList($list,$_GET['field']);
		break;

		case 'option2':
			echo sys_gui::showOptionsLst($list,$_GET['field']);
		break;

		case 'value':
			echo sys_gui::showValuesList($list,$_GET['field']);
		break;
	}


}
?>