<?
function main_delivery_message_edit()
{
	if(!sys::checkAccess('main::delivery_message'))
		return 403;

	global $_err, $_db;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('sys::mods');

	$tbl='delivery_message';

	//Проверка и сохранение данных---------------------------------------------
	if(isset($_POST['_task']))
	{
		if($_POST['_task']=='save')
		{
			$val['title_ru'] 	= trim($_db->processValue($_POST['title_ru'], 'string'));
			$val['title_uk'] 	= trim($_db->processValue($_POST['title_uk'], 'string'));
			$val['message_ru'] 	= trim($_db->processValue($_POST['message_ru'], 'string'));
			$val['message_uk'] 	= trim($_db->processValue($_POST['message_uk'], 'string'));
			$val['date_from']	= $_db->processValue(sys::date2Db($_POST['date_from'],false,$_cfg['sys::date_format']));
			$val['date_to']		= $_db->processValue(sys::date2Db($_POST['date_to'],false, $_cfg['sys::date_format']));
			
			$s = "";
			foreach ($val as $key=>$value) $s .= (($s?",":"")."$key=$value");
			
			$q = "UPDATE #__$tbl SET $s LIMIT 1";  // Всегда используется только первая запись !!!
			$_db->query($q);
		}	
	}
		
	$record=$_db->getRecord($tbl, "1=1");
	$record['date_from']=sys::db2Date($record['date_from'],false,$_cfg['sys::date_format']);
	$record['date_to']=sys::db2Date($record['date_to'],false,$_cfg['sys::date_format']);
	$values=$record;
	
	//============================================================================	
	//name ru
	$fields['title']['title']='Заголовок сообщения в рассылке';
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['multi_lang']=true;
	$fields['title']['req']=false;
	$fields['title']['html_params']='style="width:100%" maxlength="55"';

	//date from
	$fields['date_from']['title']=sys::translate('Когда начать включение в рассылку');
	$fields['date_from']['input']='datebox';
	$fields['date_from']['type']='date';
	$fields['date_from']['req']=true;
	$fields['date_from']['format']=$_cfg['sys::date_time_format'];

	//date to
	$fields['date_to']['title']=sys::translate('Когда закончить включение в рассылку');
	$fields['date_to']['input']='datebox';
	$fields['date_to']['type']='date';
	$fields['date_to']['req']=true;
	$fields['date_to']['format']=$_cfg['sys::date_time_format'];

	//message ru
	$fields['message']['title']='Сообщение в рассылке';
	$fields['message']['input']='textarea';
	$fields['message']['type']='html';
	$fields['message']['multi_lang']=true;
	$fields['message']['html_params']='style="width:100%; height:90px"';

	// information
	$fields['info']['title']='<b>Информация для администратора</b>';
	$fields['info']['input']='textarea';
	$fields['info']['disabled']=true;
	$fields['info']['html_params']='style="width:100%; height:60px; background-color: #FFCCCC; disabled: true"';
	
	$values['info'] = "
	 - Для того, чтобы сообщение не отправляллось достаточно оставить пустыми все заголовки или сообщения.
	 - Если заполнить только одно поле (ru или uk), то отправится то, которое заполнено, игнорируя язык получателя.
	 - Сообщение желательно набирать без HTML - тегов.
	 - Эта информация сохраняется в таблице $tbl. Используется только одна запись, без идентификации id.";
	
	//=====================================================================
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	

	//=========================================================================================================//
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	
	//=====================================================================
	//Формирвание результата----------------------------------------
	sys::setTitle(sys::translate('sys::delivery_message'));
	$result['panels'][]['text']=sys_gui::showNavPath('sys::delivery_message');
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//==============================================================
    
	return $result;
}
?>