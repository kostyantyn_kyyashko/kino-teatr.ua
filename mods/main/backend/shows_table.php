<?
function main_shows_table()
{
	if(!sys::checkAccess('main::shows'))
		return 403;
		
	global $_db, $_err, $_cfg;
	sys::useLib('sys::pages');
	sys::useLib('main::shows');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.desc');
	sys::filterGet('keywords','text');
	sys::filterGet('date','text');
	sys::filterGet('city_id','int');
		
	$tbl='main_shows';
	$edit_url='?mod=main&act=show_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('main::add_show');
	$order_where=$where=false;
	$nav_point='main::shows';
	$where="WHERE 1=1 ";
	
	if($_GET['city_id'])
	{
		$where.=" AND cnm.city_id = ".intval($_GET['city_id'])."";
	}

	if($_GET['keywords'])
	{
		$keywords=str_replace('*','%',urldecode($_GET['keywords']));	
		$where.=" 
			AND (
				cnm.name LIKE('".mysql_real_escape_string($keywords)."%')
				OR flm.name LIKE('".mysql_real_escape_string($keywords)."%')
				OR flm_lng.title LIKE('".mysql_real_escape_string($keywords)."%')
			) ";
	
	}
	
	$q="SELECT 
			shw.id,
			shw.begin,
			shw.film_id,
			shw.hall_id,
			shw.end,
			CONCAT(cnm.name,'::',hls.name,' (',cts.name,')') AS `cinema`,
			flm.name AS `film`,
			flm_lng.title,
			hls.`3d` as hls_3d, 
			flm.`3d` as flm_3d
			
		FROM `#__".$tbl."` AS `shw`
		
		LEFT JOIN `#__main_cinemas_halls` AS `hls`
			ON hls.id=shw.hall_id
			
		LEFT JOIN `#__main_cinemas` AS `cnm`
			ON cnm.id=hls.cinema_id
			
		LEFT JOIN `#__main_films` AS `flm`
			ON flm.id=shw.film_id
			
		LEFT JOIN `#__main_countries_cities` AS `cts`
			ON cts.id=cnm.city_id
			
		LEFT JOIN
			`#__main_films_lng` AS `flm_lng`
		ON 
			flm_lng.record_id=flm.id
		AND
			flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			
		".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_shows::deleteShow($task[1]);
			
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_shows::deleteShow($key);
				}
			}
					
		}	
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	
	if(isset($_GET['city_id']) && intval($_GET['city_id'])>0)
	{
		$city_id_str = "&city_id=".intval($_GET['city_id']);
		if($pages['next_page']) $pages['next_page'] .= "&city_id=$city_id_str";
		if($pages['prev_page']) $pages['prev_page'] .= "&city_id=$city_id_str";
		if($pages['first_page']) $pages['first_page'] .= "&city_id=$city_id_str";
		if($pages['last_page']) $pages['last_page'] .= "&city_id=$city_id_str";
		if (isset($pages['pages'])) 
			foreach($pages['pages'] as $i=>$p)
				$pages['pages'][$i] .= "&city_id=$city_id_str";
	}
		
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['film'].=' ('.$obj['title'].')';
		$obj['begin']=sys::db2Date($obj['begin'],false, $_cfg['sys::date_format']);
		$obj['end']=sys::db2Date($obj['end'],false, $_cfg['sys::date_format']);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['copy']['type']='button';
	$table['fields']['copy']['onclick']="window.open('".$edit_url."&amp;copy_id=%id%&amp;hls_3d=%hls_3d%&amp;flm_3d=%flm_3d%','','".$win_params."');return false";
	//$table['fields']['edit']['url']=$edit_url.'&copy_id=%id%';
	
	$table['fields']['times']['type']='button';
	$table['fields']['times']['onclick']="window.open('?mod=main&act=show_times_table&show_id=%id%','','".$win_params."');return false";
	$table['fields']['times']['url']='?mod=main&act=show_times_table&show_id=%id%';
	$table['fields']['times']['title']=sys::translate('main::show_times');
	$table['fields']['times']['image']='main::btn.clock.gif';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['begin']['type']='text';
	$table['fields']['begin']['order']=true;
	$table['fields']['begin']['title']=sys::translate('main::begin');
	
	$table['fields']['end']['type']='text';
	$table['fields']['end']['order']=true;
	$table['fields']['end']['title']=sys::translate('main::end');
	
	$table['fields']['cinema']['type']='text';
	$table['fields']['cinema']['order']=true;
	$table['fields']['cinema']['onclick']="window.open('?mod=main&act=cinema_hall_shows_table&hall_id=%hall_id%','','".$win_params."');return false";
	$table['fields']['cinema']['url']='?mod=main&act=cinema_hall_shows_table&hall_id=%hall_id%';
	$table['fields']['cinema']['title']=sys::translate('main::cinema');
	
	$table['fields']['film']['type']='text';
	$table['fields']['film']['order']=true;
	$table['fields']['film']['onclick']="window.open('?mod=main&act=film_shows_table&film_id=%film_id%','','".$win_params."');return false";
	$table['fields']['film']['url']='?mod=main&act=film_shows_table&film_id=%film_id%';
	$table['fields']['film']['title']=sys::translate('main::film');
	//======================================================================================================//
	
	//Поиск------------------------------------------------
	$search['action']='?mod=main&act=shows_table&city_id='.$_GET['city_id'];
	$search['html_params']='style="width:150px" maxlenght="255"';
	//======================================================
	
	//Селектор городов------------------------------------------------
	$city['input']='selectbox';
	$city['values']=$_db->getValues('main_countries_cities','name',false, "ORDER BY `name` DESC");
	$city['values'][0]=sys::translate('main::all_cities');
	$city['values']=array_reverse($city['values'],true);
	$city['value']=$_GET['city_id'];
	$city['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\');window.location=\'?mod=main&act=shows_table&city_id=\'+this.value"';
	$city['title']=sys::translate('main::city');
	//==============================================================

	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSearchForm($search).sys_gui::showSpacer().sys_form::parseField('group_id',$city);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
