<?
function main_film_wallpapers_table()
{
	if(!sys::checkAccess('main::films'))
		return 403;
		
	global $_db, $_err, $_cfg;
	sys::useLib('sys::pages');
	sys::useLib('main::films');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','order_number.desc');
	sys::filterGet('film_id','int');
		
	if(!$_GET['film_id'])
		return 404;
		
	$tbl='main_films_wallpapers';
	$edit_url='?mod=main&act=film_wallpaper_edit';
	$win_params=sys::setWinParams('710','500');
	$add_title=sys::translate('main::add_wallpaper');
	$order_where=" `film_id`=".intval($_GET['film_id'])."";
	$where=" wlp.film_id=".intval($_GET['film_id'])."";
	
	$film=$_db->getValue('main_films','name',$_GET['film_id']);
	
	if(!$film)
		return 404;
	
	$title=$film.' :: '.sys::translate('main::wallpapers');
	
	$q="SELECT 
			wlp.id, 
			wlp.image,
			wlp.order_number,
			wlp.user_id,
			wlp.public,
			usr.login AS `user`
		FROM `#__".$tbl."` AS `wlp`
		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=wlp.user_id
		WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_films::deleteWallpaper($task[1]);
			
		//Сдвиг вверх
		if($task[0]=='moveUp')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveDown($tbl,$task[1],$order_where);
			else
				$_db->moveUp($tbl,$task[1],$order_where);
		}
		
		//Сдвиг вниз
		if($task[0]=='moveDown')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveUp($tbl,$task[1],$order_where);
			else
				$_db->moveDown($tbl,$task[1],$order_where);
		}
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_films::deleteWallpaper($key);
				}
			}
					
		}	
		
		//Упорядочение нумерации
		if($_POST['_task']=='orderNumbers')
			$_db->orderNumbers($tbl, $order_where);
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['move_up']['type']='button';
	$table['fields']['move_up']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveUp.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_up']['image']='sys::btn.desc.gif';
	
	$table['fields']['move_down']['type']='button';
	$table['fields']['move_down']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveDown.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['move_down']['image']='sys::btn.asc.gif';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['image']['type']='image';
	$table['fields']['image']['src']=$_cfg['main::films_url'].'x1_%image%';
	$table['fields']['image']['url']=$_cfg['main::films_url'].'%image%';
	
	$table['fields']['user']['type']='text';
	$table['fields']['user']['order']=true;
	$table['fields']['user']['onclick']="window.open('?mod=sys&act=user_edit&id=%user_id%','','".$win_params."');return false";
	$table['fields']['user']['url']='?mod=sys&act=user_edit&id=%user_id%';
	
	$table['fields']['order_number']['type']='textbox';
	$table['fields']['order_number']['order']=true;
	$table['fields']['order_number']['html_params']='size="8" onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=set~'.$tbl.'~order_number~%id%~\'+this.value+\'~int\')"';
	
	$table['fields']['public']['type']='checkbox';
	$table['fields']['public']['title']=sys::translate('test::pbl');
	$table['fields']['public']['alt']='public';
	$table['fields']['public']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~public~%id%~%public%~float\')"';
	$table['fields']['public']['width']=40;
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&film_id=".$_GET['film_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=main_films::showFilmAdminTabs($_GET['film_id'],'wallpapers');
	$result['panels'][]=$panel;
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
