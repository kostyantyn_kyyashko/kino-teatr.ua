<?
function main_film_person_edit()
{
	if(!sys::checkAccess('main::films'))
		return 403;
		
	global $_db, $_err, $_charsets;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::films');
	
	sys::filterGet('id','int');
	sys::filterGet('film_id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='main_films_persons';
	$default=array();
	$default['order_number']=$_db->getMaxOrderNumber($tbl,"`film_id`=".intval($_GET['film_id'])."");
	$default['film_id']=$_GET['film_id'];
	$default['person_id']=false;
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,intval($_GET['id']),true);
		if(!$record)
			return 404;
		$title=$_db->getValue('main_persons','name',$record['person_id']);
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_person');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//film_id
	$fields['film_id']['input']='hidden';
	$fields['film_id']['type']='int';
	$fields['film_id']['req']=true;
	$fields['film_id']['table']=$tbl;
	
	//person_id
	$fields['person_id']['input']='searchbox';
	$fields['person_id']['type']='int';
	if(!$values['person_id'])
		$fields['person_id']['text']=sys::translate('sys::not_chosed');
	else
		$fields['person_id']['text']=$_db->getValue('main_persons','name',$values['person_id']);
	$fields['person_id']['req']=true;
	$fields['person_id']['table']=$tbl;
	$fields['person_id']['title']=sys::translate('main::person');
	$fields['person_id']['onkeyup']="searchInList(this,'2','person_id','?mod=main&act=ajx_persons_list&mode=value');";
	
	//profession_id
	$fields['profession_id']['input']='selectbox';
	$fields['profession_id']['type']='int';
	$fields['profession_id']['req']=true;
	$fields['profession_id']['table']=$tbl;
	$fields['profession_id']['empty']=true;
	$r=$_db->query("
		SELECT `id`, `name` FROM `#__main_professions`
		ORDER BY `order_number`
	");
	$fields['profession_id']['empty']=' ';
	while ($prof=$_db->fetchAssoc($r))
	{
		$fields['profession_id']['values'][$prof['id']]=$prof['name'];
	}
	$fields['profession_id']['title']=sys::translate('main::profession');
	
	
	//role
	$fields['role']['input']='textbox';
	$fields['role']['type']='text';
	$fields['role']['multi_lang']=true;
	$fields['role']['table']=$tbl;
	$fields['role']['html_params']='style="width:100%" maxlength="255"';
	$fields['role']['title']=sys::translate('main::role');
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>