<?
function main_film_soundtrack_edit()
{
	if(!sys::checkAccess('main::films'))
		return 403;
		
	global $_db, $_err, $_cfg, $_user;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::films');
		
	sys::filterGet('id','int');
	sys::filterGet('film_id','int',0);
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='main_films_soundtracks';
	$default['film_id']=$_GET['film_id'];
	$default['public']=true;
	$default['date']=strftime($_cfg['sys::date_time_format']);
	$default['order_number']=$_db->getMaxOrderNumber($tbl,"`film_id`=".intval($_GET['film_id'])."");
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$title=$record['name'];
		$record['date']=sys::db2Date($record['date'],true,$_cfg['sys::date_time_format']);
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_soundtrack');
	}
	
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//film_id
	$fields['film_id']['input']='hidden';
	$fields['film_id']['type']='int';
	$fields['film_id']['req']=true;
	$fields['film_id']['table']=$tbl;
	
	//user_id
	$fields['user_id']['input']='hidden';
	$fields['user_id']['type']='int';
	$fields['user_id']['table']=$tbl;
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';
	
	//file
	$fields['file']['input']='filebox';
	$fields['file']['type']='file';
	$fields['file']['file::dir']=$_cfg['main::films_dir'];
	$fields['file']['file::formats']=array('mp3');
	//$fields['file']['req']=true;
	
	//url
	$fields['url']['input']='textbox';
	$fields['url']['type']='text';
	$fields['url']['table']=$tbl;
	$fields['url']['html_params']='style="width:100%" maxlength="255"';
	
	//date
	$fields['date']['input']='datebox';
	$fields['date']['type']='date';
	$fields['date']['req']=true;
	$fields['date']['table']=$tbl;
	$fields['date']['format']=$_cfg['sys::date_time_format'];
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	//public
	$fields['public']['input']='checkbox';
	$fields['public']['type']='bool';
	$fields['public']['table']=$tbl;
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['user_id']=$_user['id'];
			$values['date']=sys::date2Db($values['date'],true,$_cfg['sys::date_time_format']);
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['date']=sys::db2Date($values['date'],true,$_cfg['sys::date_time_format']);
		}
		if(!$_err)
		{		
			//Загрузить файл
			if($_FILES['file']['name'])
				$values['file']=main_films::uploadSoundtrackFile($_FILES['file'],$_GET['id'],$values['file']);
			
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Удаление файла
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
	
		if($task[1]=='file')
		{
			$_db->setValue('main_films_soundtracks','file',false,$_GET['id']);
			if(is_file($_cfg['main::films_dir'].$values['file']))
				unlink($_cfg['main::films_dir'].$values['file']);
			$values['file']=false;
		}
	}
	//==================================================================================================
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>