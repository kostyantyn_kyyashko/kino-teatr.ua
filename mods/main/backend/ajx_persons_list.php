<?
function main_ajx_persons_list()
{
	global $_db, $_err, $_cfg;
	sys::filterGet('keywords');
	sys::filterGet('field');
	sys::filterGet('film_id');
	sys::filterGet('mode');
	sys::setTpl();
	if(!$_GET['keywords'])
		return false;
	if($_GET['film_id'])
	{
		$q="
			SELECT 
				fp.person_id AS `id`, 
				prs.name, 
				CONCAT(prs_lng.lastname,' ',prs_lng.firstname) AS `fio`   
			FROM 
				`#__main_films_persons` 
			AS 
				`fp`
			LEFT JOIN 
				`#__main_persons` AS `prs`
			ON 
				prs.id=fp.person_id
		
				
			LEFT JOIN 
				`#__main_persons_lng` as `prs_lng`
			ON
				prs_lng.record_id=prs.id
			AND
				prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."
				
			WHERE 
				fp.film_id=".intval($_GET['film_id'])."
		";
		if($_GET['keywords']!='*')
			$q.="
				AND prs.name LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
				OR prs_lng.lastname LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			";
		$q.=" GROUP BY fp.person_id ORDER BY prs.name";
	}
	else 
	{
		$q="
			SELECT 
				prs.id, 
				prs.name,
				CONCAT(prs_lng.lastname,' ',prs_lng.firstname) AS `fio`    
			FROM 
				`#__main_persons` AS `prs`
			LEFT JOIN 
				`#__main_persons_lng` as `prs_lng`
			ON
				prs_lng.record_id=prs.id
			AND
				prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		";
	
		if($_GET['keywords']!='*')
			$q.="
				WHERE `name` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
				OR prs_lng.lastname LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			";
		$q.=" ORDER BY `name` ";
		
	}
	$r=$_db->query($q);
	$list=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['name'].=' ('.$obj['fio'].')';
		$obj['name']=addslashes(($obj['name']));
		$list[]=$obj;
	}
	switch ($_GET['mode'])
	{
		case 'option':
			echo sys_gui::showOptionsList($list,$_GET['field']);
		break;
		
		case 'value':
			echo sys_gui::showValuesList($list,$_GET['field']);
		break;
	}
}
?>