<?
function main_seria_edit()
{
	if(!sys::checkAccess('main::series'))
		return 403;
	
	global $_db, $_err, $_cfg;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	
	sys::filterGet('id','int');
	sys::filterGet('film_id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='main_films_serials_series';
	$default=array();
	$default['film_id']=$_GET['film_id'];
	
	$film_name = $_db->getValue('main_films', 'title', $_GET['film_id'], true);
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,intval($_GET['id']));
		if(!$record)
			return 404;
		$title=$record["title"];
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_seria');
	}
	
	$values=sys::setValues($record, $default);

	//===========================================================================================================//
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//film_id
	$fields['film_id']['input']='hidden';
	$fields['film_id']['type']='int';
	$fields['film_id']['table']=$tbl;
	
	//date
	$fields['date']['input']='datebox';
	$fields['date']['type']='date';
	$fields['date']['name']='date';
	$fields['date']['req']=true;
	$fields['date']['table']=$tbl;
	$fields['date']['format']=$_cfg['sys::date_format'];
	$fields['date']['title']=sys::translate('main::date');
	
	//season
	$fields['season']['input']='textbox';
	$fields['season']['type']='int';
	$fields['season']['req']=true;
	$fields['season']['table']=$tbl;
	$fields['season']['title']=sys::translate('main::season');
	$fields['season']['html_params']='size="6" maxlength="3"';

	//seria
	$fields['seria']['input']='textbox';
	$fields['seria']['type']='int';
	$fields['seria']['req']=true;
	$fields['seria']['table']=$tbl;
	$fields['seria']['title']=sys::translate('main::seria');
	$fields['seria']['html_params']='size="6" maxlength="3"';

	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['title']=sys::translate('main::title');
	$fields['title']['html_params']='size="70"';

	//=========================================================================================================//
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{		
		$_err=sys_check::checkValues($fields,$values);	
		
		if(!$_err)
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title. " - " . $film_name);
	$result['panels'][0]['text']=$title. " - " . $film_name;
	$result['panels'][0]['class']='title';

	$result['panels'][]['text']=sys_gui::showButtons($buttons);
//	if($_GET['id'])
		$result['main']=sys_form::parseForm($form);
//	else 
//		$result['main']=sys_form::parseForm($form,'main::seria_add');
	//============================================================================================================
	return $result;
}
?>