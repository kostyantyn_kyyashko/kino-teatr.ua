<?
function main_ajx_anounce_put()
{
	
	global $_db, $_err, $_cfg;
	
	sys::filterGet('feed');
	sys::filterGet('on');
	sys::filterGet('id');
	sys::setTpl();
	
	if((!$_GET['feed'])||(!$_GET['id']))	die("hideMsg();");

// setup common variables
	
    switch ($_GET['feed'])
	{
		case 'news':
			sys::useLib("main::news");
			$tbl = 'main_news_articles';
			$src_dir = $_cfg['main::news_dir'];  // folder with original img files (x1_<id>.<ext>-small 	article_<id>.<ext>-big)
			$src_url_ru = main_news::getArticleUrl(intval($_GET['id']));
			$src_url_uk = main_news::getArticleUkUrl(intval($_GET['id']));
			break;
		
		case 'articles':
			sys::useLib("main::articles");
			$tbl = 'main_articles_articles';
			$src_dir = $_cfg['main::articles_dir'];  // folder with original img files (x1_<id>.<ext>-small 	article_<id>.<ext>-big)
			$src_url_ru = main_articles::getArticleUrl(intval($_GET['id']));
			$src_url_uk = main_articles::getArticleUkUrl(intval($_GET['id']));
			break;

		case 'interview':
			sys::useLib("main::interview");
			$tbl = 'main_interview_articles';
			$src_dir = $_cfg['main::interview_dir'];  // folder with original img files (x1_<id>.<ext>-small 	article_<id>.<ext>-big)
			$src_url_ru = main_interview::getArticleUrl(intval($_GET['id']));
			$src_url_uk = main_interview::getArticleUkUrl(intval($_GET['id']));
			break;
			
		case 'gossip':
			sys::useLib("main::gossip");
			$tbl = 'main_gossip_articles';
			$src_dir = $_cfg['main::gossip_dir'];  // folder with original img files (x1_<id>.<ext>-small 	article_<id>.<ext>-big)
			$src_url_ru = main_gossip::getArticleUrl(intval($_GET['id']));
			$src_url_uk = main_gossip::getArticleUkUrl(intval($_GET['id']));
			break;
		
		case 'serials':
			sys::useLib("main::serials");
			$tbl = 'main_serials_articles';
			$src_dir = $_cfg['main::serials_dir'];  // folder with original img files (x1_<id>.<ext>-small 	article_<id>.<ext>-big)
			$src_url_ru = main_serials::getArticleUrl(intval($_GET['id']));
			$src_url_uk = main_serials::getArticleUkUrl(intval($_GET['id']));
			break;
		
		default: die("hideMsg();");
	}
	
// make common select request
	
	$q = "
		SELECT
			art.id,
			art.user_id,
			art.anounce_id,
			art.name,
			art.date,
			art.small_image,
			art.image,
			art_lng.lang_id,
			art_lng.title,
			art_lng.intro,
			lng.code as lang_code
		FROM `#__$tbl` AS `art`

		LEFT JOIN
			`#__$tbl"."_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id

		LEFT JOIN
			`#__sys_langs` AS `lng`
		ON
			art_lng.lang_id=lng.id

		WHERE
			art.id=".intval($_GET['id']);

// use selected variables	
	
	$r=$_db->query($q);
	
	$tbl_anounce = "main_anounce_articles";
	
	$values = array();
	$values['anounce_id']=21; // ??? may be from `grifix_main_anounce`
	
	$fields = array();
	$fields['user_id']['table']=$tbl_anounce;
	$fields['anounce_id']['table']=$tbl_anounce;
	$fields['name']['table']=$tbl_anounce;
	$fields['date']['table']=$tbl_anounce;
	$fields['small_image']['table']=$tbl_anounce;
	$fields['image']['table']=$tbl_anounce;
	$fields['source_url']['table']=$tbl_anounce;
	$fields['ua_url']['table']=$tbl_anounce;
	$fields['film']['table']=$tbl_anounce;
	$fields['public']['table']=$tbl_anounce;
	$fields['exclusive']['table']=$tbl_anounce;
	$fields['intro']['table']=$tbl_anounce;
	$fields['title']['table']=$tbl_anounce;
	
	$fields['intro']['multi_lang']=true;
	$fields['title']['multi_lang']=true;

		
	$removed = false;
	$do_insert = false;
	while($obj=$_db->fetchAssoc($r))
	{
		$image = $obj["image"];
		$small_image = $obj["small_image"];
		$target_dir = $_cfg['main::anounce_dir'];
		
		// remove old data in any case on first iteration
		if(!$removed)
		{
			$removed=true;
			if(is_file($target_dir.$image))			unlink($target_dir.$image);		// remove big image
			if(is_file($target_dir."x2_".$image))	unlink($target_dir."x2_".$image);		// remove big image x2_
			if(is_file($target_dir.$small_image))	unlink($target_dir.$small_image);	// remove small image
			$_db->query("DELETE FROM `#__main_anounce_articles` WHERE id=".$obj["anounce_id"]);		// remove head record
			$_db->query("DELETE FROM `#__main_anounce_articles_lng` WHERE record_id=".$obj["anounce_id"]);	// remove language records
			if($_GET["on"]=="false") 
			{
				$_db->query("UPDATE `#__$tbl` SET anounce_id='0' WHERE id=".intval($_GET['id']));	// clean up link to anounce
				break; // now all done :)
			}
		}

		if($_GET["on"]=="true") ///  insert anounce
		{
			$values['user_id']=$obj["user_id"];
			$values['name']=$obj["name"];
			$values['date']=$obj["date"];
			$values['small_image']=$obj['small_image'];
			$values['image']=$obj['image'];
			$values['source_url']=$src_url_ru;
			$values['ua_url']=$src_url_uk;
			$values['film']=0;
			$values['public']=1;
			$values['exclusive']=0;
			$values['intro_'.$obj['lang_code']]=$obj['intro'];
			$values['title_'.$obj['lang_code']]=$obj['title'];
			$do_insert = true;
		}		
	}
	
	if($do_insert) 
	{
		// save database
		$new_id = $_db->insertArray($tbl_anounce,$values,$fields);
		$_db->query("UPDATE `#__$tbl` SET anounce_id='$new_id' WHERE id=".intval($_GET['id']));	// set up link to anounce
		
		// copy images
		if(is_file($src_dir.$small_image))	copy($src_dir.$small_image, $target_dir.$small_image);	// copy small image		
		sys::resizeImage($src_dir.$image, $target_dir.'x2_'.$image, 664, 350, true); // x2_ by Mike
	}
	
	print ("hideMsg();");
}
?>