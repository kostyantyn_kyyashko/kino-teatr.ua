<?
function main_cinemas_table()
{
	if(!sys::checkAccess('main::cinemas'))
		return 403;

	global $_db, $_err;
	sys::useLib('sys::pages');
	sys::useLib('main::cinemas');
	sys::useLib('sys::form');


	sys::filterGet('keywords','text');
	sys::filterGet('city_id','int');
	if($_GET['city_id'])
		sys::filterGet('order_by','text','order_number.asc');
	else
		sys::filterGet('order_by','text','id.desc');

	$tbl='main_cinemas';
	$edit_url='?mod=main&act=cinema_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('main::add_cinema');
	$order_where=$where=false;
	$nav_point='main::cinemas';
	$where="WHERE 1=1 ";

	if($_GET['keywords'])
	{
		$keywords=str_replace('*','%',urldecode($_GET['keywords']));
			$where.=" AND cnm.name LIKE('".mysql_real_escape_string($keywords)."%') ";
	}

	if($_GET['city_id'])
	{
		$where.=" AND cnm.city_id = ".intval($_GET['city_id'])."";
		$order_where="`city_id` = ".intval($_GET['city_id'])."";
	}

	$q="SELECT
			cnm.id,
			CONCAT(cnm.name,' (',cts.name,')') AS `name`,
			cnm.order_number,
			cnm.first_screen
		FROM `#__".$tbl."` AS `cnm`
		LEFT JOIN `#__main_countries_cities` AS `cts`
		ON cts.id=cnm.city_id
		".$where;
	//===============================================================================================//

	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_cinemas::deletecinema($task[1]);

		//Сдвиг вверх
		if($task[0]=='moveUp')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveDown($tbl,$task[1],$order_where);
			else
				$_db->moveUp($tbl,$task[1],$order_where);
		}

		//Сдвиг вниз
		if($task[0]=='moveDown')
		{
			$order_by=explode('.',$_GET['order_by']);
			if($order_by[0]=='order_number' && $order_by[1]=='desc')
				$_db->moveUp($tbl,$task[1],$order_where);
			else
				$_db->moveDown($tbl,$task[1],$order_where);
		}

		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_cinemas::deletecinema($key);
				}
			}

		}

		//Упорядочение нумерации
		if($_POST['_task']=='orderNumbers')
			$_db->orderNumbers($tbl, $order_where);
	}
	//================================================================================================//

	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	
	if(isset($_GET['city_id']) && intval($_GET['city_id'])>0)
	{
		$city_id_str = "&city_id=".intval($_GET['city_id']);
		if($pages['next_page']) $pages['next_page'] .= "&city_id=$city_id_str";
		if($pages['prev_page']) $pages['prev_page'] .= "&city_id=$city_id_str";
		if($pages['first_page']) $pages['first_page'] .= "&city_id=$city_id_str";
		if($pages['last_page']) $pages['last_page'] .= "&city_id=$city_id_str";
		if (isset($pages['pages'])) 
			foreach($pages['pages'] as $i=>$p)
				$pages['pages'][$i] .= "&city_id=$city_id_str";
	}
		
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);

	while($obj=$_db->fetchAssoc($r))
	{
		$table['records'][$obj['id']]=$obj;
	}

	$table['checkbox']=true;

	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";

	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';

	if($_GET['city_id'])
	{
		$table['fields']['move_up']['type']='button';
		$table['fields']['move_up']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveUp.%id%'; document.forms['table'].submit(); return false;";
		$table['fields']['move_up']['image']='sys::btn.desc.gif';

		$table['fields']['move_down']['type']='button';
		$table['fields']['move_down']['onclick']="setMsg('".sys::translate('sys::saving')."'); document.forms['table'].elements._task.value='moveDown.%id%'; document.forms['table'].submit(); return false;";
		$table['fields']['move_down']['image']='sys::btn.asc.gif';
	}

	$table['fields']['photos']['type']='button';
	$table['fields']['photos']['onclick']="window.open('?mod=main&act=cinema_photos_table&cinema_id=%id%','','".$win_params."');return false";
	$table['fields']['photos']['url']='?mod=main&act=cinema_photos_table&cinema_id=%id%';
	$table['fields']['photos']['title']=sys::translate('main::photos');
	$table['fields']['photos']['image']='main::btn.photo.gif';

	$table['fields']['halls']['type']='button';
	$table['fields']['halls']['onclick']="window.open('?mod=main&act=cinema_halls_table&cinema_id=%id%','','".$win_params."');return false";
	$table['fields']['halls']['url']='?mod=main&act=cinema_halls_table&cinema_id=%id%';
	$table['fields']['halls']['title']=sys::translate('main::halls');
	$table['fields']['halls']['image']='main::btn.halls.gif';

	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;

	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;

	if($_GET['city_id'])
	{
		$table['fields']['order_number']['type']='textbox';
		$table['fields']['order_number']['order']=true;
		$table['fields']['order_number']['html_params']='size="8" onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=set~'.$tbl.'~order_number~%id%~\'+this.value+\'~int\')"';
	}

	$table['fields']['first_screen']['type']='checkbox';
	$table['fields']['first_screen']['title']=sys::translate('main::first_scr');
	$table['fields']['first_screen']['alt']=sys::translate('main::first_screen');
	$table['fields']['first_screen']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~first_screen~%id%~%first_screen%~int\')"';
	$table['fields']['first_screen']['width']=40;
	//======================================================================================================//

	//Поиск------------------------------------------------
	$search['action']='?mod=main&act=cinemas_table&city_id='.$_GET['city_id'];
	$search['html_params']='style="width:150px" maxlenght="255"';
	//======================================================

	//Селектор городов------------------------------------------------
	$city['input']='selectbox';
	$city['values']=$_db->getValues('main_countries_cities','name',false, "ORDER BY `name` DESC");
	$city['values'][0]=sys::translate('main::all_cities');
	$city['values']=array_reverse($city['values'],true);
	$city['value']=$_GET['city_id'];
	$city['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\');window.location=\'?mod=main&act=cinemas_table&city_id=\'+this.value"';
	$city['title']=sys::translate('main::city');
	//==============================================================

	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
	$buttons['new']['title']=$add_title;

	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';

	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';

	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";

	//=======================================================================================================//

	//Формирование результата------------------------------------------------------------------------------//

	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);

	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSearchForm($search).sys_gui::showSpacer().sys_form::parseField('group_id',$city);;
	$result['panels'][]['text']=sys_gui::showPages($pages);

	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
