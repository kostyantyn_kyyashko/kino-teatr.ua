<?
function main_contest_winners_table()
{
	if(!sys::checkAccess('main::contest'))
		return 403;

	global $_db, $_err;
	sys::useLib('sys::pages');
	sys::useLib('main::contest');
	sys::useLib('sys::form');

	sys::filterGet('order_by','text','date.desc');
	sys::filterGet('article_id','int');


	$tbl='main_contest_winners';
	$edit_url='?mod=main&act=contest_winners_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('main::add_contest');
	$order_where=$where='1=1';
	if($_GET['article_id'])
		$order_where=$where=" art.contest_id=".intval($_GET['article_id'])."";
	$nav_point='main::contest';

	$qw="SELECT
			count(*) as count
		FROM `#__".$tbl."` AS `art`
		WHERE ".$where;

	$rw=$_db->query($qw);

	while($objw=$_db->fetchAssoc($rw))
	{
		$count = $objw['count'];
	}

	if (!$count)
	{
		$qw="SELECT
				winners
			FROM `#__main_contest_articles` AS `art`
			WHERE art.id=".intval($_GET['article_id'])."";

		$rw=$_db->query($qw);
		// Получить заданное количество победителей, которых надо выбрать
		while($objw=$_db->fetchAssoc($rw))
		{
			$winners = $objw['winners']; // Нужно будет выбрать столько победителей
		}

		$qu="SELECT
				art.user_id,
				art.rating,
				usr.login,
				usr.email,
				usr_dat.main_first_name,
				usr_dat.main_last_name,
				usr_cfg.main_city_id as `city`
			
			FROM `#__main_contest_participants` AS `art`
			
			LEFT JOIN `#__sys_users` as `usr`
			ON art.user_id = usr.id
			
			LEFT JOIN `#__sys_users_data` as `usr_dat`
			ON art.user_id = usr_dat.user_id
			
			LEFT JOIN `#__sys_users_cfg` as `usr_cfg`
			ON art.user_id = usr_cfg.user_id
			
			WHERE
				art.contest_id = ".intval($_GET['article_id'])."

			ORDER BY art.rating desc, RAND()
			LIMIT ".$winners."
			";

		$ru=$_db->query($qu);

		while($obju=$_db->fetchAssoc($ru))
		{
//			$qp="
//				INSERT INTO `#__main_contest_winners`
//				(`contest_id`, `user_id`, `username`, `city`, `first_name`, `last_name`, `email`, `rating`)
//				VALUES (".$_GET['article_id'].", ".$obju['user_id'].", '".$obju['login']."', ".$obju['city'].", '".$obju['main_first_name']."', '".$obju['main_last_name']."', '".$obju['email']."', ".$obju['rating'].")
//			";
			$qp="
				INSERT INTO `#__main_contest_winners`
				(`contest_id`, `user_id`) VALUES (".$_GET['article_id'].", ".$obju['user_id'].")";

			$rp=$_db->query($qp);
		}
	}



	$q="SELECT
			art.user_id,
			art.contest_id,
			usr.email,
			usr_dat.main_first_name as first_name,
			usr_dat.main_last_name as last_name,
			usr_dat.main_nick_name as username,
			usr_dat.main_nick_name as login
		FROM `#__".$tbl."` AS `art`
			
		LEFT JOIN `#__sys_users` as `usr`
		ON art.user_id = usr.id
		
		LEFT JOIN `#__sys_users_data` as `usr_dat`
		ON art.user_id = usr_dat.user_id

		WHERE ".$where;



	//===============================================================================================//

	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_contest::deleteWinner($task[1], $_GET['article_id']);

		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_contest::deleteArticle($key);
				}
			}

		}
	}
	//================================================================================================//

	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);

	$r=$_db->query($q.$pages['limit']);



	while($obj=$_db->fetchAssoc($r))
	{
		$table['records'][$obj['user_id']]=$obj;
	}

	$table['checkbox']=true;

	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%user_id%'; document.forms['table'].submit();} return false;";

	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&user_id=%user_id%&contest_id=%contest_id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';

	$table['fields']['first_name']['type']='text';
	$table['fields']['last_name']['type']='text';
	$table['fields']['username']['type']='text';
	$table['fields']['email']['type']='text';

	//======================================================================================================//

	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&contest_id=".$_GET['article_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;

	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";

	//=======================================================================================================//

	//Селектор разделов------------------------------------------------
	$section['input']='hidden';
	//==============================================================

	//Формирование результата------------------------------------------------------------------------------//

	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);

	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSpacer().sys_form::parseField('contest_id',$section);;
	$result['panels'][]['text']=sys_gui::showPages($pages);

	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
