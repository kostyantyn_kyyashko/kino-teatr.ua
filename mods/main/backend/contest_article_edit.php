<?
function main_contest_article_edit()
{
	if(!sys::checkAccess('main::contest'))
		return 403;

	global $_db, $_err, $_cfg, $_user;

	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::contest');
	sys::useLib('main::persons');
	sys::useLib('main::films');

	sys::filterGet('id','int');
	sys::filterGet('contest_id','int',0);
	sys::filterGet('cpy_id','int');

	if(!isset($_POST['_task']))
		$_POST['_task']=false;


	$tbl='main_contest_articles';
	$default['contest_id']=$_GET['contest_id'];
	$default['public']=true;
	$default['date']=strftime($_cfg['sys::date_time_format']);
	$default['dateend']=strftime($_cfg['sys::date_time_format']);
	$default['persons']=array();
	$default['films']=array();

	if($_GET['cpy_id'])	// by Mike
	{
		$default=$_db->getRecord($tbl,intval($_GET['cpy_id']), true);
		$default['date']=strftime($_cfg['sys::date_time_format']);
		$default['dateend']=strftime($_cfg['sys::date_time_format']);
		$default['finished']=false;
		$default['sent']=false;
		$default['public']=false;
	}

	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id'],true);
		if(!$record)
			return 404;
		$title=$record['name'];
		$record['persons']=main_contest::getArticlePersonsIds($_GET['id']);
		$record['films']=main_contest::getArticleFilmsIds($_GET['id']);
		$record['date']=sys::db2Date($record['date'],true,$_cfg['sys::date_time_format']);
		$record['dateend']=sys::db2Date($record['dateend'],true,$_cfg['sys::date_time_format']);
	}
	else
	{
		$record=false;
		$title=sys::translate('main::new_article');
	}

	$values=sys::setValues($record, $default);
	$values['persons']=main_persons::getPersonsNames($values['persons']);
	$values['films']=main_films::getFilmsNames($values['films']);
	//===========================================================================================================//


	//Поля и форма----------------------------------------------------------------------------------------------

	//contest_id
	$fields['contest_id']['input']='selectbox';
	$fields['contest_id']['type']='int';
	$fields['contest_id']['table']=$tbl;
	$fields['contest_id']['title']=sys::translate('main::section');
	$r=$_db->query("SELECT `id`, `name` FROM `#__main_contest` ORDER BY `order_number`");
	while ($section=$_db->fetchAssoc($r))
	{
		$fields['contest_id']['values'][$section['id']]=$section['name'];
	}

	//user_id
	$fields['user_id']['input']='hidden';
	$fields['user_id']['type']='int';
	$fields['user_id']['table']=$tbl;

	//city_id
	$fields['city_id']['input']='selectbox';
	$fields['city_id']['type']='int';
	$fields['city_id']['table']=$tbl;
	$fields['city_id']['title']=sys::translate('main::city');
	$r=$_db->query("SELECT `id`, `name` FROM `#__main_countries_cities` ORDER BY `order_number`");
	$fields['city_id']['values'][0]=sys::translate('main::all_cities');
	while ($city=$_db->fetchAssoc($r))
	{
		$fields['city_id']['values'][$city['id']]=$city['name'];
	}


	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="55"';

	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="55"';

	//date
	$fields['date']['input']='datebox';
	$fields['date']['type']='date';
	$fields['date']['req']=true;
	$fields['date']['table']=$tbl;
	$fields['date']['format']=$_cfg['sys::date_time_format'];

	//dateend
	$fields['dateend']['input']='datebox';
	$fields['dateend']['type']='date';
	$fields['dateend']['req']=true;
	$fields['dateend']['table']=$tbl;
	$fields['dateend']['format']=$_cfg['sys::date_time_format'];

	//name
	$fields['winners']['input']='textbox';
	$fields['winners']['type']='text';
	$fields['winners']['req']=true;
	$fields['winners']['table']=$tbl;
	$fields['winners']['html_params']='style="width:20%" maxlength="55"';

	//image
	$fields['image']['input']='filebox';
	$fields['image']['type']='file';
	$fields['image']['file::dir']=$_cfg['main::contest_dir'];
	$fields['image']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image']['title']=sys::translate('main::image');
	$fields['image']['preview']=true;
	$fields['image']['preview::prefix']='x2_';


	//intro
	$fields['intro']['input']='textarea';
	$fields['intro']['type']='html';
	$fields['intro']['table']=$tbl;
	$fields['intro']['multi_lang']=true;
	$fields['intro']['html_params']='style="width:100%; height:50px"';
	$fields['intro']['height']='300px';
	$fields['intro']['title']=sys::translate('main::intro');

	//text
	$fields['text']['input']='fck';
	$fields['text']['type']='html';
	$fields['text']['table']=$tbl;
	$fields['text']['multi_lang']=true;
	$fields['text']['html_params']='style="width:100%; height:50px"';
	$fields['text']['height']='300px';

	//текст после завершения конкурса
	$fields['finishedtext']['input']='fck';
	$fields['finishedtext']['type']='html';
	$fields['finishedtext']['table']=$tbl;
	$fields['finishedtext']['multi_lang']=true;
	$fields['finishedtext']['html_params']='style="width:100%; height:50px"';
	$fields['finishedtext']['height']='300px';

	//text
	$fields['letter']['input']='fck';
	$fields['letter']['type']='html';
	$fields['letter']['table']=$tbl;
	$fields['letter']['multi_lang']=false;
	$fields['letter']['html_params']='style="width:100%; height:50px"';
	$fields['letter']['height']='300px';

	//public
	$fields['prizes']['input']='checkbox';
	$fields['prizes']['type']='bool';
	$fields['prizes']['table']=$tbl;

	//public
	$fields['book']['input']='checkbox';
	$fields['book']['type']='bool';
	$fields['book']['table']=$tbl;

	//public
	$fields['cd']['input']='checkbox';
	$fields['cd']['type']='bool';
	$fields['cd']['table']=$tbl;

	//public
	$fields['tshirt']['input']='checkbox';
	$fields['tshirt']['type']='bool';
	$fields['tshirt']['table']=$tbl;

	//public
	$fields['tickets']['input']='checkbox';
	$fields['tickets']['type']='bool';
	$fields['tickets']['table']=$tbl;

	//public
	$fields['public']['input']='checkbox';
	$fields['public']['type']='bool';
	$fields['public']['table']=$tbl;

	//public
	$fields['finished']['input']='checkbox';
	$fields['finished']['type']='bool';
	$fields['finished']['table']=$tbl;

	//public
	$fields['sent']['input']='checkbox';
	$fields['sent']['type']='bool';
	$fields['sent']['table']=$tbl;

	//=========================================================================================================//

	//Проверка и сохранение данных--------------------------------------------------------------------------
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['index']=strip_tags($values['text']);
			$values['date']=sys::date2Db($values['date'],true,$_cfg['sys::date_time_format']);
			$values['dateend']=sys::date2Db($values['dateend'],true,$_cfg['sys::date_time_format']);

			//Вставка данных
			if(!$_GET['id'])
			{
				$values['user_id']=$_user['id'];
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['date']=sys::db2Date($values['date'],true,$_cfg['sys::date_time_format']);
			$values['dateend']=sys::db2Date($values['dateend'],true,$_cfg['sys::date_time_format']);
		}
		if(!$_err)
		{
			//Загрузить и отресайзить картинку
			if($_FILES['image']['name'])
				$values['image']=main_contest::uploadArticleImage($_FILES['image'],$_GET['id'],$values['image']);

			if($_FILES['small_image']['name'])
				$values['small_image']=main_contest::uploadArticleSmallImage($_FILES['small_image'],$_GET['id'],$values['small_image']);

			main_contest::linkArticlePersons($_GET['id'],$values['persons']);
			main_contest::linkArticleFilms($_GET['id'],$values['films']);
			main_contest::setArticleIndex($_GET['id'],$values);
			if($_GET['cpy_id'])	// by Mike
			{
				$q = "DELETE FROM `#__main_contest_questions` WHERE `contest_id`=".$_GET['id'];
				$_db->query($q);
				$q = "SELECT id FROM `#__main_contest_questions` WHERE `contest_id`=".$_GET['cpy_id'];
				$r=$_db->query($q);
				$new_contest_id = $_GET['id'];
				$new_records=array();
				while($rec=$_db->fetchAssoc($r))
				{
					$q = "INSERT INTO `#__main_contest_questions` SELECT 
					'0' as id, 
					'$new_contest_id' as contest_id, 
					question, 
					answer_1, 
					answer_2, 
					answer_3, 
					answer_4, 
					answer_5, 
					correct_answer,
					question_ua, 
					answer_1_ua, 
					answer_2_ua, 
					answer_3_ua, 
					answer_4_ua, 
					answer_5_ua
					FROM `#__main_contest_questions` WHERE `id`=".$rec["id"];
					$_db->query($q);
				}
			}
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//

	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			$_db->setValue('main_contest_articles','image',false,$_GET['id']);
			main_contest::deleteArticleImage($values['image']);
			$values['image']=false;
		}

		if($task[1]=='small_image')
		{
			$_db->setValue('main_contest_articles','small_image',false,$_GET['id']);
			if(is_file($_cfg['main::contest_dir'].$values['small_image']))
				unlink($_cfg['main::contest_dir'].$values['small_image']);
			$values['small_image']=false;
		}
	}
	//==================================================================================================

	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//

	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================

	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>