<?
function main_film_wallpaper_edit()
{
	if(!sys::checkAccess('main::films'))
		return 403;
		
	global $_db, $_err, $_cfg, $_user;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::films');
	sys::useLib('main::persons');
		
	sys::filterGet('id','int');
	sys::filterGet('film_id','int',0);
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='main_films_wallpapers';
	$default['film_id']=$_GET['film_id'];
	$default['public']=true;
	$default['date']=strftime($_cfg['sys::date_time_format']);
	$default['order_number']=$_db->getMaxOrderNumber($tbl,"`film_id`=".intval($_GET['film_id'])."");
	$default['persons']=array();
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$title=$record['image'];
		$record['persons']=main_films::getwallpaperPersonsIds($_GET['id']);
		$record['date']=sys::db2Date($record['date'],true,$_cfg['sys::date_time_format']);
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_wallpaper');
	}
	
	$values=sys::setValues($record, $default);
	$values['persons']=main_persons::getPersonsNames($values['persons']);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//film_id
	$fields['film_id']['input']='hidden';
	$fields['film_id']['type']='int';
	$fields['film_id']['req']=true;
	$fields['film_id']['table']=$tbl;
	
	//user_id
	$fields['user_id']['input']='hidden';
	$fields['user_id']['type']='int';
	$fields['user_id']['table']=$tbl;
	
	//image
	$fields['image']['input']='filebox';
	$fields['image']['type']='file';
	$fields['image']['file::dir']=$_cfg['main::films_dir'];
	$fields['image']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image']['title']=sys::translate('main::image');
	$fields['image']['preview']=true;
	$fields['image']['req']=true;
	$fields['image']['preview::prefix']='x2_';
	
	//image_1024x768
	$fields['image_1024x768']['input']='filebox';
	$fields['image_1024x768']['type']='file';
	$fields['image_1024x768']['file::dir']=$_cfg['main::films_dir'];
	$fields['image_1024x768']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image_1024x768']['title']=sys::translate('main::image_1024x768');
	
	//image_1280x800
	$fields['image_1280x800']['input']='filebox';
	$fields['image_1280x800']['type']='file';
	$fields['image_1280x800']['file::dir']=$_cfg['main::films_dir'];
	$fields['image_1280x800']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image_1280x800']['title']=sys::translate('main::image_1280x800');
	
	//image_1280x1024
	$fields['image_1280x1024']['input']='filebox';
	$fields['image_1280x1024']['type']='file';
	$fields['image_1280x1024']['file::dir']=$_cfg['main::films_dir'];
	$fields['image_1280x1024']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image_1280x1024']['title']=sys::translate('main::image_1280x1024');
	
	//image_1280x1024
	$fields['image_1680x1050']['input']='filebox';
	$fields['image_1680x1050']['type']='file';
	$fields['image_1680x1050']['file::dir']=$_cfg['main::films_dir'];
	$fields['image_1680x1050']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image_1680x1050']['title']=sys::translate('main::image_1680x1050');
	
	//date
	$fields['date']['input']='datebox';
	$fields['date']['type']='date';
	$fields['date']['req']=true;
	$fields['date']['table']=$tbl;
	$fields['date']['format']=$_cfg['sys::date_time_format'];
	
	//persons
	$fields['persons']['input']='multisearchbox';
	$fields['persons']['type']='text';
	$fields['persons']['onkeyup']="searchInList(this,'2','persons','?mod=main&act=ajx_persons_list&mode=option&film_id=".$values['film_id']."');";
	$fields['persons']['title']=sys::translate('main::persons');
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	//public
	$fields['public']['input']='checkbox';
	$fields['public']['type']='bool';
	$fields['public']['table']=$tbl;
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['user_id']=$_user['id'];
			$values['date']=sys::date2Db($values['date'],true,$_cfg['sys::date_time_format']);
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['date']=sys::db2Date($values['date'],true,$_cfg['sys::date_time_format']);
		}
		if(!$_err)
		{
			//Загрузить и отресайзить картинку	
			if($_FILES['image']['name'])
				$values['image']=main_films::uploadwallpaperImage($_FILES['image'],$_GET['id'],$values['image']);
				
			//Загрузить обои
			if($_FILES['image_1024x768']['name'])
				$values['image_1024x768']=main_films::uploadwallpaperFile($_FILES['image_1024x768'],$_GET['id'],$values['image_1024x768'],'1024x768');
				
			if($_FILES['image_1280x800']['name'])
				$values['image_1280x800']=main_films::uploadwallpaperFile($_FILES['image_1280x800'],$_GET['id'],$values['image_1280x800'],'1280x800');
				
			if($_FILES['image_1280x1024']['name'])
				$values['image_1280x1024']=main_films::uploadwallpaperFile($_FILES['image_1280x1024'],$_GET['id'],$values['image_1280x1024'],'1280x1024');
				
			if($_FILES['image_1680x1050']['name'])
				$values['image_1680x1050']=main_films::uploadwallpaperFile($_FILES['image_1680x1050'],$_GET['id'],$values['image_1680x1050'],'1680x1050');
			
			main_films::linkWallpaperPersons($_GET['id'],$values['persons']);
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Удаление файла
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			$_db->setValue('main_films_wallpapers','image',false,$_GET['id']);
			main_films::deletewallpaperImage($values['image']);
			$values['image']=false;
		}
		
		if($task[1]=='image_1024x768')
		{
			$_db->setValue('main_films_wallpapers','image_1024x768',false,$_GET['id']);
			if(is_file($_cfg['main::films_dir'].$values['image_1024x768']))
				unlink($_cfg['main::films_dir'].$values['image_1024x768']);
			$values['image_1024x768']=false;
		}
		
		if($task[1]=='image_1280x800')
		{
			$_db->setValue('main_films_wallpapers','image_1280x800',false,$_GET['id']);
			if(is_file($_cfg['main::films_dir'].$values['image_1280x800']))
				unlink($_cfg['main::films_dir'].$values['image_1280x800']);
			$values['image_1280x800']=false;
		}
		
		if($task[1]=='image_1280x1024')
		{
			$_db->setValue('main_films_wallpapers','image_1280x1024',false,$_GET['id']);
			if(is_file($_cfg['main::films_dir'].$values['image_1280x1024']))
				unlink($_cfg['main::films_dir'].$values['image_1280x1024']);
			$values['image_1280x1024']=false;
		}
		
		if($task[1]=='image_1680x1050')
		{
			$_db->setValue('main_films_wallpapers','image_1680x1050',false,$_GET['id']);
			if(is_file($_cfg['main::films_dir'].$values['image_1680x1050']))
				unlink($_cfg['main::films_dir'].$values['image_1680x1050']);
			$values['image_1680x1050']=false;
		}
		
	}
	//==================================================================================================
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>