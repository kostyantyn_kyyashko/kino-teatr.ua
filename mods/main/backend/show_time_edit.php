<?

function main_show_time_edit()
{
	if(!sys::checkAccess('main::shows'))
		return 403;
		
	global $_db, $_err, $_charsets;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::shows');

	if(!isset($_POST['_task']))
		$_POST['_task']=false;
			
	sys::filterGet('id','int');
	sys::filterGet('show_id','int');

	$tbl='main_shows_times';
	$default=array();
	$default['show_id']=$_GET['show_id'];
	
	$record=false;
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,intval($_GET['id']),true);
		if(!$record)
			return 404;
		$title=$record['time'];
		
		if(!$_POST['_task']) $_POST['3d']=($record['3D']>0)?"on":"";
		$_POST['may3d']=$record['may3D'];
	}
	else 
	{
		$title=sys::translate('main::new_show_time');
		$q = "SELECT `show`.`id` as `show_id`, (`film`.`3d`>0 and `hall`.`3d`>0) as may3d 
				FROM `#__main_shows` AS `show`
				LEFT JOIN `#__main_cinemas_halls` AS `hall`
				ON `hall`.`id`=`show`.`hall_id`
				LEFT JOIN `#__main_films` AS `film`
				ON `film`.`id`=`show`.`film_id`
				WHERE `show`.`id`=".$_GET['show_id'];
		$r=$_db->Query($q);
		if($record = $_db->fetchArray($r))
			$_POST['3d']=(($_POST['may3d']=$record['may3d'])>0)?"on":"";
	}
	
	$values=sys::setValues($record, $default);
	
	// если указан id, то идет редактирование и строка в таблице содержит информацию о сеансе
	// если id нет, а есть show_id, то значит надо считать комбинацию из зала и фильма
	
	$values["3d"] = isset($_POST['3d'])?intval($_POST['3d']=="on"):0;
	$values["may3d"] = isset($_POST['may3d'])?intval($_POST['may3d']):0;
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//show_id
	$fields['show_id']['input']='hidden';
	$fields['show_id']['type']='int';
	$fields['show_id']['req']=true;
	$fields['show_id']['table']=$tbl;
	
	//time
	$fields['time']['input']='textbox';
	$fields['time']['type']='time';
	$fields['time']['req']=true;
	$fields['time']['table']=$tbl;
	$fields['time']['html_params']='size="8" maxlength="8"';
	$fields['time']['title']=sys::translate('main::time');
	
	//prices
	$fields['prices']['input']='textbox';
	$fields['prices']['type']='text';
	$fields['prices']['req']=true;
	$fields['prices']['table']=$tbl;
	$fields['prices']['html_params']='style="width:100%" maxlength="255"';
	$fields['prices']['title']=sys::translate('main::prices');

	//may3D
	$fields['may3d']['input']='hidden';
	$fields['may3d']['type']='int';
	$fields['may3d']['req']=true;
	$fields['may3d']['table']=$tbl;
	$fields['may3d']['value']=intval($values["may3d"]);

	//3d
	$fields['3d']['input']='checkbox';
	$fields['3d']['type']='bool';
	$fields['3d']['table']=$tbl;
	$fields['3d']['title']=sys::translate('main::3d');
	$fields['3d']['html_params']=intval($values["may3d"])?"":"disabled";
	$fields['3d']['value']=intval(($values["may3d"]>0) && ($values["3d"]>0));	

	//note
	$fields['note']['input']='textbox';
	$fields['note']['type']='text';
	$fields['note']['multi_lang']=true;
	$fields['note']['table']=$tbl;
	$fields['note']['html_params']='style="width:100%" maxlength="255"';
	$fields['note']['title']=sys::translate('main::note');
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close') 
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);  
	//============================================================================================================
	return $result;
}
?>