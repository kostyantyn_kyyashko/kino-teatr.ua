<?
function main_distributors_table()
{
	if(!sys::checkAccess('main::distributors'))
		return 403;
		
	global $_db, $_err;
	sys::useLib('sys::pages');
	sys::useLib('main::distributors');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.desc');
	sys::filterGet('keywords','text');
		
	$tbl='main_distributors';
	$edit_url='?mod=main&act=distributor_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('main::add_distributor');
	$order_where=$where=false;
	$nav_point='main::distributors';
	$where="WHERE 1=1 ";
	
	if($_GET['keywords'])
	{
		$keywords=str_replace('*','%',urldecode($_GET['keywords']));
			$where.=" AND `name` LIKE('".mysql_real_escape_string($keywords)."%') ";
	}
	
	$q="SELECT 
			`id`, 
			`name`,
			`info`,
			`site`
		FROM `#__".$tbl."`".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_distributors::deletedistributor($task[1]);
			
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_distributors::deletedistributor($key);
				}
			}
					
		}	
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;
	
	$table['fields']['info']['type']='text';
	
	$table['fields']['site']['type']='text';
	$table['fields']['site']['url']='%site%';
	//======================================================================================================//
	
	//Поиск------------------------------------------------
	$search['action']='?mod=main&act=distributors_table';
	$search['html_params']='style="width:150px" maxlenght="255"';
	//======================================================
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSearchForm($search);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
