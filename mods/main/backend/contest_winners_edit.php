<?
function main_contest_winners_edit()
{
	if(!sys::checkAccess('main::contest'))
		return 403;

	global $_db, $_err, $_cfg, $_user;

	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::contest');


	sys::filterGet('user_id','int');
	sys::filterGet('contest_id','int',0);

	if(!isset($_POST['_task']))
		$_POST['_task']=false;




	$tbl='main_contest_winners';
	$default['contest_id']=$_GET['contest_id'];




	if($_GET['user_id'])
	{



		$rw=$_db->query("
				SELECT
					*
				FROM `#__main_contest_winners`
				WHERE contest_id=".intval($_GET['contest_id'])."
				AND user_id = ".$_GET['user_id']."
			");




		$record=$_db->fetchAssoc($rw);


//		$record=$_db->getRecord($tbl,$_GET['user_id'],true);
		if(!$record)
			return 404;



		$title=$record['username'];
	}
	else
	{
		$record=false;
		$title=sys::translate('main::new_article');
	}



	$values=sys::setValues($record, $default);

	//===========================================================================================================//


	//Поля и форма----------------------------------------------------------------------------------------------

	//question
	$fields['user_id']['input']='textbox';
	$fields['user_id']['type']='text';
	$fields['user_id']['req']=true;
	$fields['user_id']['table']=$tbl;
	$fields['user_id']['html_params']='style="width:20%" maxlength="55"';

	//answer
	$fields['username']['input']='textbox';
	$fields['username']['type']='text';
	$fields['username']['req']=false;
	$fields['username']['table']=$tbl;
	$fields['username']['html_params']='style="width:100%" maxlength="55"';

	//answer
	$fields['city']['input']='textbox';
	$fields['city']['type']='text';
	$fields['city']['req']=false;
	$fields['city']['table']=$tbl;
	$fields['city']['html_params']='style="width:10%" maxlength="55"';

	//answer
	$fields['first_name']['input']='textbox';
	$fields['first_name']['type']='text';
	$fields['first_name']['req']=false;
	$fields['first_name']['table']=$tbl;
	$fields['first_name']['html_params']='style="width:100%" maxlength="55"';

	//answer
	$fields['last_name']['input']='textbox';
	$fields['last_name']['type']='text';
	$fields['last_name']['req']=false;
	$fields['last_name']['table']=$tbl;
	$fields['last_name']['html_params']='style="width:100%" maxlength="55"';

	//answer
	$fields['email']['input']='textbox';
	$fields['email']['type']='text';
	$fields['email']['req']=false;
	$fields['email']['table']=$tbl;
	$fields['email']['html_params']='style="width:100%" maxlength="55"';

	//answer
	$fields['rating']['input']='textbox';
	$fields['rating']['type']='text';
	$fields['rating']['req']=true;
	$fields['rating']['table']=$tbl;
	$fields['rating']['html_params']='style="width:20%" maxlength="55"';

	//=========================================================================================================//

	//Проверка и сохранение данных--------------------------------------------------------------------------
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{


		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['index']=strip_tags($values['text']);

			//Вставка данных
			if(!$_GET['user_id'])
			{

				$qp="
				INSERT INTO `#__main_contest_winners`
				(`contest_id`, `user_id`, `username`, `city`, `first_name`, `last_name`, `email`, `rating`)
				VALUES (".$_GET['contest_id'].", '".$values['user_id']."', '".$values['username']."', ".$values['city'].", '".$values['first_name']."', '".$values['last_name']."', '".$values['email']."', ".$values['rating'].")
				";

				$rp=$_db->query($qp);

			}
			else //Обновление данных
			{
				$qp="
				UPDATE `#__main_contest_winners`
				SET
					`username`='".$values['username']."',
					`city`=".$values['city'].",
					`first_name`='".$values['first_name']."',
					`last_name`='".$values['last_name']."',
					`email`='".$values['email']."',
					`rating`=".$values['rating']."
				WHERE
					`contest_id`=".$_GET['contest_id']."
				AND
					`user_id` = ".$values['user_id']."
				";

				$rp=$_db->query($qp);

			}



		}
		if(!$_err)
		{
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//

	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			$_db->setValue('main_contest_articles','image',false,$_GET['id']);
			main_contest::deleteArticleImage($values['image']);
			$values['image']=false;
		}

		if($task[1]=='small_image')
		{
			$_db->setValue('main_contest_articles','small_image',false,$_GET['id']);
			if(is_file($_cfg['main::contest_dir'].$values['small_image']))
				unlink($_cfg['main::contest_dir'].$values['small_image']);
			$values['small_image']=false;
		}
	}
	//==================================================================================================

	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//

	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================

	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>