<?
function main_shows_control()
{
	if(!sys::checkAccess('main::shows'))
		return 403;
		
	global $_db, $_err, $_cfg;
	sys::useLib('sys::pages');
	sys::useLib('main::cinemas');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.desc');
	sys::filterGet('date','text',strftime($_cfg['sys::date_format']));
	
	$tbl='main_cinemas_halls';
	$edit_url='?mod=main&act=cinema_hall_edit';
	$win_params=sys::setWinParams('790','590');
	$order_where=$where=false;
	$nav_point='main::shows_control';
	//$where="WHERE `num`=0 ";
	
	if($_GET['date'])
	{
		$date=sys::date2Db($_GET['date'], false, $_cfg['sys::date_format']);
	}
	
	$q="SELECT 
			hls.id, 
			CONCAT(cnm.name,'::',hls.name,' (',cts.name,')') AS `name`,
			(
				SELECT COUNT(shw.id) FROM `#__main_shows` AS `shw`
				WHERE '".mysql_real_escape_string($date)."' BETWEEN shw.begin AND shw.end
				AND shw.hall_id=hls.id
			) AS `num`
		FROM `#__".$tbl."` AS `hls`
		
		LEFT JOIN `#__main_cinemas` AS `cnm`
		ON cnm.id=hls.cinema_id
		
		LEFT JOIN `#__main_countries_cities` AS `cts`
		ON cts.id=cnm.city_id
		
		".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
			
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by);
	
	while($obj=$_db->fetchAssoc($r))
	{
		if(!$obj['num'])
			$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['shows']['type']='button';
	$table['fields']['shows']['onclick']="window.open('?mod=main&act=cinema_hall_shows_table&hall_id=%id%','','".$win_params."');return false";
	$table['fields']['shows']['url']='?mod=main&act=cinema_hall_shows_table&hall_id=%id%';
	$table['fields']['shows']['title']=sys::translate('main::shows');
	$table['fields']['shows']['image']='main::btn.show.gif';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys::parseTpl('main::shows_control_search');
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
