<?
function main_question_edit()
{
	if(!sys::checkAccess('main::questions'))
		return 403;
		
	global $_db, $_err, $_charsets;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::questions');
	
	sys::filterGet('id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='main_questions';
	$default=array();
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,intval($_GET['id']),true);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_question');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';
	
	//question
	$fields['question']['input']='textbox';
	$fields['question']['type']='text';
	$fields['question']['req']=true;
	$fields['question']['multi_lang']=true;
	$fields['question']['table']=$tbl;
	$fields['question']['html_params']='style="width:100%" maxlength="255"';
	$fields['question']['title']=sys::translate('main::question');
	
	//answers
	$fields['answers']['input']='textarea';
	$fields['answers']['type']='text';
	$fields['answers']['table']=$tbl;
	$fields['answers']['multi_lang']=true;
	$fields['answers']['html_params']='style="width:100%; height:150px"';
	$fields['answers']['req']=true;
	$fields['answers']['title']=sys::translate('main::answers');
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>