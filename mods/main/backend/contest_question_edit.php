<?
function main_contest_question_edit()
{
	if(!sys::checkAccess('main::contest'))
		return 403;

	global $_db, $_err, $_cfg, $_user;

	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::contest');


	sys::filterGet('id','int');
	sys::filterGet('contest_id','int',0);

	if(!isset($_POST['_task']))
		$_POST['_task']=false;


	$tbl='main_contest_questions';
	$default['contest_id']=$_GET['contest_id'];

	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id'],true);
		if(!$record)
			return 404;
		$title=$record['question'];
	}
	else
	{
		$record=false;
		$title=sys::translate('main::new_article');
	}

	$values=sys::setValues($record, $default);

	//===========================================================================================================//


	//Поля и форма----------------------------------------------------------------------------------------------

	//contest_id
	$fields['contest_id']['input']='selectbox';
	$fields['contest_id']['type']='int';
	$fields['contest_id']['table']=$tbl;
	$fields['contest_id']['title']=sys::translate('main::contest');
	$r=$_db->query("SELECT `id`, `name` FROM `#__main_contest_articles` ORDER BY `date` DESC");
	while ($section=$_db->fetchAssoc($r))
	{
		$fields['contest_id']['values'][$section['id']]=$section['name'];
	}

	//question
	$fields['question']['input']='textbox';
	$fields['question']['type']='text';
	$fields['question']['req']=true;
	$fields['question']['table']=$tbl;
	$fields['question']['html_params']='style="width:100%" ';

	//question
	$fields['question_ua']['input']='textbox';
	$fields['question_ua']['type']='text';
	$fields['question_ua']['req']=true;
	$fields['question_ua']['table']=$tbl;
	$fields['question_ua']['html_params']='style="width:100%" ';

	//answer
	$fields['answer_1']['input']='textbox';
	$fields['answer_1']['type']='text';
	$fields['answer_1']['req']=false;
	$fields['answer_1']['table']=$tbl;
	$fields['answer_1']['html_params']='style="width:100%" ';

	//answer
	$fields['answer_1_ua']['input']='textbox';
	$fields['answer_1_ua']['type']='text';
	$fields['answer_1_ua']['req']=false;
	$fields['answer_1_ua']['table']=$tbl;
	$fields['answer_1_ua']['html_params']='style="width:100%" ';

	//answer
	$fields['answer_2']['input']='textbox';
	$fields['answer_2']['type']='text';
	$fields['answer_2']['req']=false;
	$fields['answer_2']['table']=$tbl;
	$fields['answer_2']['html_params']='style="width:100%" ';

	//answer
	$fields['answer_2_ua']['input']='textbox';
	$fields['answer_2_ua']['type']='text';
	$fields['answer_2_ua']['req']=false;
	$fields['answer_2_ua']['table']=$tbl;
	$fields['answer_2_ua']['html_params']='style="width:100%" ';

	//answer
	$fields['answer_3']['input']='textbox';
	$fields['answer_3']['type']='text';
	$fields['answer_3']['req']=false;
	$fields['answer_3']['table']=$tbl;
	$fields['answer_3']['html_params']='style="width:100%" ';

	//answer
	$fields['answer_3_ua']['input']='textbox';
	$fields['answer_3_ua']['type']='text';
	$fields['answer_3_ua']['req']=false;
	$fields['answer_3_ua']['table']=$tbl;
	$fields['answer_3_ua']['html_params']='style="width:100%" ';

	//answer
	$fields['answer_4']['input']='textbox';
	$fields['answer_4']['type']='text';
	$fields['answer_4']['req']=false;
	$fields['answer_4']['table']=$tbl;
	$fields['answer_4']['html_params']='style="width:100%" ';

	//answer
	$fields['answer_4_ua']['input']='textbox';
	$fields['answer_4_ua']['type']='text';
	$fields['answer_4_ua']['req']=false;
	$fields['answer_4_ua']['table']=$tbl;
	$fields['answer_4_ua']['html_params']='style="width:100%" ';

	//answer
	$fields['answer_5']['input']='textbox';
	$fields['answer_5']['type']='text';
	$fields['answer_5']['req']=false;
	$fields['answer_5']['table']=$tbl;
	$fields['answer_5']['html_params']='style="width:100%" ';

	//answer
	$fields['answer_5_ua']['input']='textbox';
	$fields['answer_5_ua']['type']='text';
	$fields['answer_5_ua']['req']=false;
	$fields['answer_5_ua']['table']=$tbl;
	$fields['answer_5_ua']['html_params']='style="width:100%" ';


	//answer
	$fields['correct_answer']['input']='textbox';
	$fields['correct_answer']['type']='text';
	$fields['correct_answer']['req']=true;
	$fields['correct_answer']['table']=$tbl;
	$fields['correct_answer']['html_params']='style="width:20%" ';

	//=========================================================================================================//

	//Проверка и сохранение данных--------------------------------------------------------------------------
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{


		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['index']=strip_tags($values['text']);
			$values['date']=sys::date2Db($values['date'],true,$_cfg['sys::date_time_format']);
			$values['dateend']=sys::date2Db($values['dateend'],true,$_cfg['sys::date_time_format']);

			//Вставка данных
			if(!$_GET['id'])
			{
				$values['user_id']=$_user['id'];
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['date']=sys::db2Date($values['date'],true,$_cfg['sys::date_time_format']);
			$values['dateend']=sys::db2Date($values['dateend'],true,$_cfg['sys::date_time_format']);
		}
		if(!$_err)
		{
			//Загрузить и отресайзить картинку
			if($_FILES['image']['name'])
				$values['image']=main_contest::uploadArticleImage($_FILES['image'],$_GET['id'],$values['image']);

			if($_FILES['small_image']['name'])
				$values['small_image']=main_contest::uploadArticleSmallImage($_FILES['small_image'],$_GET['id'],$values['small_image']);

			main_contest::linkArticlePersons($_GET['id'],$values['persons']);
			main_contest::linkArticleFilms($_GET['id'],$values['films']);
			main_contest::setArticleIndex($_GET['id'],$values);
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//

	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			$_db->setValue('main_contest_articles','image',false,$_GET['id']);
			main_contest::deleteArticleImage($values['image']);
			$values['image']=false;
		}

		if($task[1]=='small_image')
		{
			$_db->setValue('main_contest_articles','small_image',false,$_GET['id']);
			if(is_file($_cfg['main::contest_dir'].$values['small_image']))
				unlink($_cfg['main::contest_dir'].$values['small_image']);
			$values['small_image']=false;
		}
	}
	//==================================================================================================

	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//

	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================

	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>