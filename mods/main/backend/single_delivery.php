<?
function main_single_delivery()
{
	if(!sys::checkAccess('main::distributors'))
		return 403;
	
	$nav_point='main::single_delivery';
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
    $result['main']=sys::parseTpl('main::single_delivery',$params);
    
	return $result;
    
}


?>
