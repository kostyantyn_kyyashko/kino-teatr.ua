<?
function main_contest_edit()
{
	if(!sys::checkAccess('main::contest'))
		return 403;

	global $_db, $_err, $_charsets;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::contest');

	sys::filterGet('id','int');

	if(!isset($_POST['_task']))
		$_POST['_task']=false;

	$tbl='main_contest';
	$default=array();
	$default['order_number']=$_db->getMaxOrderNumber($tbl);

	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,intval($_GET['id']),true);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else
	{
		$record=false;
		$title=sys::translate('main::new_contest');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//

	//Поля и форма----------------------------------------------------------------------------------------------

	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';

	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="255"';

	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	//=========================================================================================================//

	//Проверка и сохранение данных--------------------------------------------------------------------------
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//

	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//

	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================

	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	if($_GET['id'])
	{
		$panel['class']='blank';
		$panel['text']=main_contest::showcontestAdminTabs($_GET['id'],'info');
		$result['panels'][]=$panel;
	}
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>