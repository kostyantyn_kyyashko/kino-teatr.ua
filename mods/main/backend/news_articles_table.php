<?
function main_news_articles_table()
{
	if(!sys::checkAccess('main::news'))
		return 403;
		
	global $_db, $_err;
	sys::useLib('sys::pages');
	sys::useLib('main::news');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','date.desc');
	sys::filterGet('news_id','int');
		
	if(!$_GET['news_id'])
		return 404;
		
	$tbl='main_news_articles';
	$edit_url='?mod=main&act=news_article_edit';
	$win_params=sys::setWinParams('700','600');
	$add_title=sys::translate('main::add_article');
	$order_where=$where=" art.news_id=".intval($_GET['news_id'])."";
	
	$news=$_db->getValue('main_news','name',$_GET['news_id']);
	
	if(!$news)
		return 404;
	
	$title=$news.' :: '.sys::translate('main::articles');
	
	$q="SELECT 
			art.id, 
			art.user_id,
			usr.login AS `user`,
			art.name,
			art.date,
			art.public,
			art.exclusive
		FROM `#__".$tbl."` AS `art`
		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=art.user_id
		WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_news::deleteArticle($task[1]);
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_news::deleteArticle($key);
				}
			}
					
		}	
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;
	
	$table['fields']['user']['type']='text';
	$table['fields']['user']['order']=true;
	$table['fields']['user']['width']=150;
	$table['fields']['user']['onclick']="window.open('?mod=sys&act=user_edit&id=%user_id%','','".$win_params."');return false";
	$table['fields']['user']['url']='?mod=sys&act=user_edit&id=%user_id%';
	
	$table['fields']['date']['type']='text';
	$table['fields']['date']['order']=true;
	$table['fields']['date']['width']=150;
	
	$table['fields']['public']['type']='checkbox';
	$table['fields']['public']['title']=sys::translate('sys::pbl');
	$table['fields']['public']['alt']=sys::translate('sys::public');
	$table['fields']['public']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~public~%id%~%public%\')"';
	$table['fields']['public']['width']=40;
	
	$table['fields']['exclusive']['type']='checkbox';
	$table['fields']['exclusive']['title']=sys::translate('main::exc');
	$table['fields']['exclusive']['alt']=sys::translate('main::exclusive');
	$table['fields']['exclusive']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~exclusive~%id%~%exclusive%\')"';
	$table['fields']['exclusive']['width']=40;
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&news_id=".$_GET['news_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=main_news::showNewsAdminTabs($_GET['news_id'],'articles');
	$result['panels'][]=$panel;
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
