<?
function main_contest_questions_table()
{
	if(!sys::checkAccess('main::contest'))
		return 403;

	global $_db, $_err;
	sys::useLib('sys::pages');
	sys::useLib('main::contest');
	sys::useLib('sys::form');

	sys::filterGet('order_by','text','date.desc');
	sys::filterGet('article_id','int');


	$tbl='main_contest_questions';
	$edit_url='?mod=main&act=contest_question_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('main::add_contest');
	$order_where=$where='1=1';
	if($_GET['article_id'])
		$order_where=$where=" art.contest_id=".intval($_GET['article_id'])."";
	$nav_point='main::contest';

	$q="SELECT
			art.id,
			art.question
		FROM `#__".$tbl."` AS `art`
		WHERE ".$where;

	//===============================================================================================//

	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_contest::deleteQuestion($task[1], $_GET['article_id']);

	}
	//================================================================================================//

	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$r=$_db->query($q.$pages['limit']);

	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true);
		$table['records'][$obj['id']]=$obj;
	}

	$table['checkbox']=true;

	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";

	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';

	$table['fields']['question']['type']='text';

	//======================================================================================================//

	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
	$buttons['new']['title']=$add_title;

	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';

	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';

	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";

	//=======================================================================================================//

	//Селектор разделов------------------------------------------------
	$section['input']='selectbox';
	$section['values']=$_db->getValues('main_contest','name',false, "ORDER BY `name` DESC");
	$section['values'][0]=sys::translate('main::all_sections');
	$section['values']=array_reverse($section['values'],true);
	$section['value']=$_GET['contest_id'];
	$section['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\');window.location=\'?mod=main&act=contest_table&contest_id=\'+this.value"';
	$section['title']=sys::translate('main::section');
	//==============================================================

	//Формирование результата------------------------------------------------------------------------------//

	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);

	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSpacer().sys_form::parseField('contest_id',$section);;
	$result['panels'][]['text']=sys_gui::showPages($pages);

	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
