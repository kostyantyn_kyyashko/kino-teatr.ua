<?
function main_reviews_table()
{
	if(!sys::checkAccess('main::reviews'))
		return 403;
		
	global $_db, $_err, $_cfg;
	sys::useLib('sys::pages');
	sys::useLib('main::reviews');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.desc');
	sys::filterGet('keywords','text');
		
	$tbl='main_reviews';
	$edit_url='?mod=main&act=review_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('main::add_review');
	$order_where=$where=false;
	$nav_point='main::reviews';
	$where="WHERE 1=1 ";
	
	if($_GET['keywords'])
	{
		$keywords=str_replace('*','%',urldecode($_GET['keywords']));
			$where.=" 
				AND
				( 
					rvs.title LIKE('".mysql_real_escape_string($keywords)."%') 
					OR usr.login LIKE('".mysql_real_escape_string($keywords)."%') 
					OR flm.name LIKE('".mysql_real_escape_string($keywords)."%') 
				)";
	}
	
	$q="SELECT 
			rvs.id, 
			rvs.user_id,
			rvs.film_id,
			rvs.title,
			rvs.date,
			rvs.public,
			rvs.thanks,
			usr.login AS `user`,
			flm.name AS `film`
		FROM `#__".$tbl."` AS `rvs`
		
		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=rvs.user_id
		
		LEFT JOIN `#__main_films` AS `flm`
		ON flm.id=rvs.film_id
		
		".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_reviews::deleteReview($task[1]);
			
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_reviews::deleteReview($key);
				}
			}
					
		}	
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],false,$_cfg['sys::date_format']);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	
	$table['fields']['thanks']['type']='text';
	$table['fields']['thanks']['order']=true;
	$table['fields']['thanks']['title']=sys::translate('main::thnk');
	$table['fields']['thanks']['width']=50;
	
	$table['fields']['title']['type']='text';
	$table['fields']['title']['order']=true;
	
	$table['fields']['film']['type']='text';
	$table['fields']['film']['order']=true;
	$table['fields']['film']['onclick']="window.open('?mod=main&act=film_edit&id=%film_id%','','".$win_params."');return false";
	$table['fields']['film']['url']='?mod=main&act=film_edit&id=%film_id%';
	$table['fields']['film']['title']=sys::translate('main::film');
	
	$table['fields']['user']['type']='text';
	$table['fields']['user']['order']=true;
	$table['fields']['user']['onclick']="window.open('?mod=sys&act=user_edit&id=%user_id%','','".$win_params."');return false";
	$table['fields']['user']['url']='?mod=sys&act=user_edit&id=%user_id%';
	$table['fields']['user']['width']=150;
	
	$table['fields']['public']['type']='checkbox';
	$table['fields']['public']['title']=sys::translate('sys::pbl');
	$table['fields']['public']['alt']=sys::translate('sys::public');
	$table['fields']['public']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~public~%id%~%public%\')"';
	$table['fields']['public']['width']=40;
	//======================================================================================================//
	
	//Поиск------------------------------------------------
	$search['action']='?mod=main&act=reviews_table';
	$search['html_params']='style="width:150px" maxlenght="255"';
	//======================================================
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSearchForm($search);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
