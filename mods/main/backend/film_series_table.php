<?
function main_film_series_table()
{
	if(!sys::checkAccess('main::films'))
		return 403;
		
	global $_db, $_err, $_cfg;
	sys::useLib('sys::pages');
	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.desc');
	sys::filterGet('film_id','int');
		
	if(!$_GET['film_id'])
		return 404;
		
	$tbl='main_films_serials_series';
	$edit_url='?mod=main&act=seria_edit';
	$win_params=sys::setWinParams('600','500');
	$add_title=sys::translate('main::add_seria');
	$where=" `film_id`=".intval($_GET['film_id'])."";
	
	$film=$_db->getValue('main_films','name',$_GET['film_id']);
	
	if(!$film)
		return 404;
	
	$title=$film.' :: '.sys::translate('main::series');
	
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
		{
			$_db->delete($tbl,"id=".intval($task[1]));
		}
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
			$_db->delete($tbl,"id=".intval($key));
				}
			}
					
		}	
		//die;
	}
	//================================================================================================//
	
	$q="SELECT id, date, season, seria, title FROM `#__".$tbl."` WHERE ".$where;

	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	//$order_by=sys::parseOrderBy($_GET['order_by']);
	$order_by= " ORDER BY season DESC, CONCAT(SUBSTRING(date,7,4),SUBSTRING(date,4,2),SUBSTRING(date,1,2)) DESC";
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&film_id=".$_GET['film_id']. "&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url."&film_id=".$_GET['film_id']. "&id=%id%";
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['date']['type']='text';
	$table['fields']['date']['order']=false;
	$table['fields']['date']['title']=sys::translate('main::date');	
	
	$table['fields']['season']['type']='text';
	$table['fields']['season']['order']=false;
	$table['fields']['season']['title']=sys::translate('main::season');	
	
	$table['fields']['seria']['type']='text';
	$table['fields']['seria']['order']=false;
	$table['fields']['seria']['title']=sys::translate('main::seria');	
	
	$table['fields']['title']['type']='text';
	$table['fields']['title']['order']=false;
	$table['fields']['title']['title']=sys::translate('main::title');	
	
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&film_id=".$_GET['film_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=main_films::showFilmAdminTabs($_GET['film_id'],'series');
	$result['panels'][]=$panel;
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
