<?
function main_film_edit()
{
function dmword($string, $is_cyrillic = true)
	{
		static $codes = array(
			'A' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1)),
				'U' => array(array(0, 7, -1))),
			'B' => array(array(7, 7, 7)),
			'C' => array(array(5, 5, 5), array(4, 4, 4),
				'Z' => array(array(4, 4, 4),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'Z' => array(array(4, 4, 4))),
				'K' => array(array(5, 5, 5), array(45, 45, 45)),
				'H' => array(array(5, 5, 5), array(4, 4, 4),
					'S' => array(array(5, 54, 54)))),
			'D' => array(array(3, 3, 3),
				'T' => array(array(3, 3, 3)),
				'Z' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'Z' => array(array(4, 4, 4))),
				'R' => array(
					'S' => array(array(4, 4, 4)),
					'Z' => array(array(4, 4, 4)))),
			'E' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1)),
				'U' => array(array(1, 1, -1))),
			'F' => array(array(7, 7, 7),
				'B' => array(array(7, 7, 7))),
			'G' => array(array(5, 5, 5)),
			'H' => array(array(5, 5, -1)),
			'I' => array(array(0, -1, -1),
				'A' => array(array(1, -1, -1)),
				'E' => array(array(1, -1, -1)),
				'O' => array(array(1, -1, -1)),
				'U' => array(array(1, -1, -1))),
			'J' => array(array(4, 4, 4)),
			'K' => array(array(5, 5, 5),
				'H' => array(array(5, 5, 5)),
				'S' => array(array(5, 54, 54))),
			'L' => array(array(8, 8, 8)),
			'M' => array(array(6, 6, 6),
				'N' => array(array(66, 66, 66))),
			'N' => array(array(6, 6, 6),
				'M' => array(array(66, 66, 66))),
			'O' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1))),
			'P' => array(array(7, 7, 7),
				'F' => array(array(7, 7, 7)),
				'H' => array(array(7, 7, 7))),
			'Q' => array(array(5, 5, 5)),
			'R' => array(array(9, 9, 9),
				'Z' => array(array(94, 94, 94), array(94, 94, 94)), // special case
				'S' => array(array(94, 94, 94), array(94, 94, 94))), // special case
			'S' => array(array(4, 4, 4),
				'Z' => array(array(4, 4, 4),
					'T' => array(array(2, 43, 43)),
					'C' => array(
						'Z' => array(array(2, 4, 4)),
						'S' => array(array(2, 4, 4))),
					'D' => array(array(2, 43, 43))),
				'D' => array(array(2, 43, 43)),
				'T' => array(array(2, 43, 43),
					'R' => array(
						'Z' => array(array(2, 4, 4)),
						'S' => array(array(2, 4, 4))),
					'C' => array(
						'H' => array(array(2, 4, 4))),
					'S' => array(
						'H' => array(array(2, 4, 4)),
						'C' => array(
							'H' => array(array(2, 4, 4))))),
				'C' => array(array(2, 4, 4),
					'H' => array(array(4, 4, 4),
						'T' => array(array(2, 43, 43),
							'S' => array(
								'C' => array(
									'H' => array(array(2, 4, 4))),
								'H' => array(array(2, 4, 4))),
							'C' => array(
								'H' => array(array(2, 4, 4)))),
						'D' => array(array(2, 43, 43)))),
				'H' => array(array(4, 4, 4),
					'T' => array(array(2, 43, 43),
						'C' => array(
							'H' => array(array(2, 4, 4))),
						'S' => array(
							'H' => array(array(2, 4, 4)))),
					'C' => array(
						'H' => array(array(2, 4, 4))),
					'D' => array(array(2, 43, 43)))),
			'T' => array(array(3, 3, 3),
				'C' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4))),
				'Z' => array(array(4, 4, 4),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'Z' => array(array(4, 4, 4)),
					'H' => array(array(4, 4, 4)),
					'C' => array(
						'H' => array(array(4, 4, 4)))),
				'T' => array(
					'S' => array(array(4, 4, 4),
						'Z' => array(array(4, 4, 4)),
						'C' => array(
							'H' => array(array(4, 4, 4)))),
					'C' => array(
						'H' => array(array(4, 4, 4))),
					'Z' => array(array(4, 4, 4))),
				'H' => array(array(3, 3, 3)),
				'R' => array(
					'Z' => array(array(4, 4, 4)),
					'S' => array(array(4, 4, 4)))),
			'U' => array(array(0, -1, -1),
				'E' => array(array(0, -1, -1)),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1))),
			'V' => array(array(7, 7, 7)),
			'W' => array(array(7, 7, 7)),
			'X' => array(array(5, 54, 54)),
			'Y' => array(array(1, -1, -1)),
			'Z' => array(array(4, 4, 4),
				'D' => array(array(2, 43, 43),
					'Z' => array(array(2, 4, 4),
						'H' => array(array(2, 4, 4)))),
				'H' => array(array(4, 4, 4),
					'D' => array(array(2, 43, 43),
						'Z' => array(
							'H' => array(array(2, 4, 4))))),
				'S' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'C' => array(
						'H' => array(array(4, 4, 4))))));
		$length = strlen($string);
		$output = '';
		$i = 0;
		$previous = -1;
		while ($i < $length)
		{
			$current = $last = &$codes[$string[$i]];
			for ($j = $k = 1; $k < 7; $k++)
			{
				if (!isset($string[$i + $k]) || !isset($current[$string[$i + $k]]))
					break;
				$current = &$current[$string[$i + $k]];
				if (isset($current[0]))
				{
					$last = &$current;
					$j = $k + 1;
				}
			}
			if ($i == 0)
				$code = $last[0][0];
			elseif (!isset($string[$i + $j]) || ($codes[$string[$i + $j]][0][0] != 0))
				$code = $is_cyrillic ? (isset($last[1]) ? $last[1][2] : $last[0][2]) : $last[0][2];
			else
				$code = $is_cyrillic ? (isset($last[1]) ? $last[1][1] : $last[0][1]) : $last[0][1];
			if (($code != -1) && ($code != $previous))
				$output .= $code;
			$previous = $code;
			$i += $j;
		}
		return str_pad(substr($output, 0, 6), 6, '0');
	}

//------------------------------------------------------------------------------

	function dmstring($string)
	{
		$is_cyrillic = false;
		if (preg_match('/[А-Яа-я]/iu', $string) === 1)
		{
			$string = translit($string);
			$is_cyrillic = true;
		}

		$firstword = '';
		$lastword = '';
		$words = explode(' ', $string);

		if (preg_match('/[A-Za-z]/iu', $words[0]) !== 1)
		{
			$firstword = $words[0];
		}

		if (preg_match('/[A-Za-z]/iu', $words[count($words)-1]) !== 1)
		{
			$lastword = $words[count($words)-1];
		}

		$string = preg_replace(array('/[^\w\s]|\d/iu', '/\b[^\s]{1,1}\b/iu', '/\s{1,}/iu', '/^\s+|\s+$/iu'), array('', '', ' '), strtoupper($string));

		if ($string[0])
		{
			$matches = explode(' ', $string);
		}  else {
			$matches = array();
		}

		foreach($matches as $key => $match)
			$matches[$key] = dmword($match, $is_cyrillic);

		if ($firstword)
		{
			array_unshift($matches, $firstword);
		}

		if ($lastword)
		{
			$matches[]=$lastword;
		}

		return $matches;
	}



	function translit($string)
	{
		static $ru = array(
			'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
			'Ё', 'ё', 'Ж', 'ж', 'З', 'з', 'И', 'и', 'Й', 'й', 'К', 'к',
			'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',
			'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц',
			'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ', 'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь',
			'Э', 'э', 'Ю', 'ю', 'Я', 'я'
		);
		static $en = array(
			'A', 'a', 'B', 'b', 'V', 'v', 'G', 'g', 'D', 'd', 'E', 'e',
			'E', 'e', 'Zh', 'zh', 'Z', 'z', 'I', 'i', 'J', 'j', 'K', 'k',
			'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'R', 'r',
			'S', 's', 'T', 't', 'U', 'u', 'F', 'f', 'H', 'h', 'C', 'c',
			'Ch', 'ch', 'Sh', 'sh', 'Sch', 'sch', '\'', '\'', 'Y', 'y', '\'', '\'',
			'E', 'e', 'Ju', 'ju', 'Ja', 'ja'
		);
		$string = str_replace($ru, $en, $string);
		return $string;
	}


	if(!sys::checkAccess('main::films'))
		return 403;

	global $_db, $_err, $_cfg, $_user;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::films');
	sys::useLib('main::genres');
	sys::useLib('main::countries');
	sys::useLib('main::studios');
	sys::useLib('main::distributors');

	sys::filterGet('id','int');

	if(!isset($_POST['_task']))
		$_POST['_task']=false;

	$tbl='main_films';
	$default=array();
	$default['genres']=array();
	$default['studios']=array();
	$default['distributors']=array();
	$default['countries']=array();

	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,intval($_GET['id']),true);
		$record['world_premiere']=sys::db2Date($record['world_premiere'],false,$_cfg['sys::date_format']);
		$record['ukraine_premiere']=sys::db2Date($record['ukraine_premiere'],false,$_cfg['sys::date_format']);
		$record['imax_premiere']=sys::db2Date($record['imax_premiere'],false,$_cfg['sys::date_format']);
		$record['genres']=main_films::getFilmGenresIds($_GET['id']);
		$record['studios']=main_films::getFilmStudiosIds($_GET['id']);
		$record['distributors']=main_films::getFilmDistributorsIds($_GET['id']);
		$record['countries']=main_films::getFilmCountriesIds($_GET['id']);
		$imdb_id=$_db->getRecord('main_films_imdb_to_kt','kt_id='.intval($_GET['id']),false);
		$record['imdb_id']=is_array($imdb_id)?$imdb_id["imdb_id"]:"";
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else
	{
		$record=false;
		$title=sys::translate('main::new_film');
	}
	$values=sys::setValues($record, $default);
	$values['genres']=main_genres::getGenresNames($values['genres']);
	$values['studios']=main_studios::getStudiosNames($values['studios']);
	$values['distributors']=main_distributors::getDistributorsNames($values['distributors']);
	$values['countries']=main_countries::getCountriesNames($values['countries']);

	//===========================================================================================================//

	//Поля и форма----------------------------------------------------------------------------------------------

	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';

	//title_orig
	$fields['title_orig']['input']='textbox';
	$fields['title_orig']['type']='text';
	$fields['title_orig']['req']=true;
	$fields['title_orig']['table']=$tbl;
	$fields['title_orig']['html_params']='style="width:100%" maxlength="255"';
	$fields['title_orig']['title']=sys::translate('main::title_orig');

	//title_alt
	$fields['title_alt']['input']='textbox';
	$fields['title_alt']['type']='text';
	$fields['title_alt']['table']=$tbl;
	$fields['title_alt']['html_params']='style="width:100%" maxlength="255"';
	$fields['title_alt']['title']=sys::translate('main::title_alt');

	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="255"';

	//world_premiere
	$fields['world_premiere']['input']='datebox';
	$fields['world_premiere']['type']='date';
	$fields['world_premiere']['table']=$tbl;
	$fields['world_premiere']['format']=$_cfg['sys::date_format'];
	$fields['world_premiere']['title']=sys::translate('main::world_premiere');

	//ukraine_premiere
	$fields['ukraine_premiere']['input']='datebox';
	$fields['ukraine_premiere']['type']='date';
	$fields['ukraine_premiere']['table']=$tbl;
	$fields['ukraine_premiere']['format']=$_cfg['sys::date_format'];
	$fields['ukraine_premiere']['title']=sys::translate('main::ukraine_premiere');

	//ukraine_premiere
	$fields['imax_premiere']['input']='datebox';
	$fields['imax_premiere']['type']='date';
	$fields['imax_premiere']['table']=$tbl;
	$fields['imax_premiere']['format']=$_cfg['sys::date_format'];
	$fields['imax_premiere']['title']=sys::translate('main::imax_premiere');


	//budget
	$fields['imax_id']['input']='textbox';
	$fields['imax_id']['type']='text';
	$fields['imax_id']['table']=$tbl;
	$fields['imax_id']['html_params']='size="25"';
	$fields['imax_id']['title']='IMAX ID';

	//duration
	$fields['duration']['input']='textbox';
	$fields['duration']['type']='int';
	//$fields['duration']['req']=true;
	$fields['duration']['table']=$tbl;
	$fields['duration']['html_params']='size="3" maxlength="3"';
	$fields['duration']['title']=sys::translate('main::duration');

	//year
	$fields['year']['input']='textbox';
	$fields['year']['type']='int';
	//$fields['year']['req']=true;
	$fields['year']['table']=$tbl;
	$fields['year']['html_params']='size="4" maxlength="4"';
	$fields['year']['title']=sys::translate('main::year');

	//age_limit
	$fields['age_limit']['input']='textbox';
	$fields['age_limit']['type']='int';
	$fields['age_limit']['table']=$tbl;
	$fields['age_limit']['html_params']='size="2" maxlength="2"';
	$fields['age_limit']['title']=sys::translate('main::age_limit');

	//budget
	$fields['budget']['input']='textbox';
	//$fields['budget']['type']='int';
	$fields['budget']['table']=$tbl;
	$fields['budget']['html_params']='size="8" maxlength="8"';
	$fields['budget']['title']=sys::translate('main::budget');

	//budget_currency
	$fields['budget_currency']['input']='selectbox';
	$fields['budget_currency']['type']='text';
	$fields['budget_currency']['table']=$tbl;
	$fields['budget_currency']['values']=$_cfg['main::currency'];
	$fields['budget_currency']['title']=sys::translate('main::budget_currency');

	//imdb_id
	$fields['imdb_id']['type']='text';
	$fields['imdb_id']['title']=sys::translate('IMDB ID');
	$fields['imdb_id']['html_params']='size="12" readonly';
	
	//album_poster
	$fields['album_poster']['input']='filebox';
	$fields['album_poster']['type']='file';
	$fields['album_poster']['file::dir']=$_cfg['main::films_dir'];
	$fields['album_poster']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['album_poster']['title']=sys::translate('main::album_poster');
	$fields['album_poster']['preview']=true;
	$fields['album_poster']['title']=sys::translate('main::album_poster');

	//public
	$fields['children']['input']='checkbox';
	$fields['children']['type']='bool';
	$fields['children']['table']=$tbl;

	$fields['3d']['input']='checkbox';
	$fields['3d']['type']='bool';
	$fields['3d']['table']=$tbl;
	$fields['3d']['title']='3D';

	$fields['serial']['input']='checkbox';
	$fields['serial']['type']='bool';
	$fields['serial']['table']=$tbl;
	
	// link - donor of serial (http://www.serialdate.ru/serial.php?id=105)
	$fields['serial_link']['input']='textbox';
	$fields['serial_link']['type']='text';
	$fields['serial_link']['table']=$tbl;
	$fields['serial_link']['html_params']='style="width:100%" maxlength="1024"';
	$fields['serial_link']['req']=false;

	//intro
	$fields['intro']['input']='fck';
	$fields['intro']['type']='html';
	$fields['intro']['table']=$tbl;
	$fields['intro']['multi_lang']=true;
	$fields['intro']['html_params']='style="width:100%; height:50px"';
	$fields['intro']['height']='300px';
	$fields['intro']['title']=sys::translate('main::intro');

	//text
	$fields['text']['input']='fck';
	$fields['text']['type']='html';
	$fields['text']['table']=$tbl;
	$fields['text']['multi_lang']=true;
	$fields['text']['html_params']='style="width:100%; height:50px"';
	$fields['text']['height']='300px';

	//intro
	$fields['seosm']['input']='fck';
	$fields['seosm']['type']='html';
	$fields['seosm']['table']=$tbl;
	$fields['seosm']['multi_lang']=true;
	$fields['seosm']['html_params']='style="width:100%; height:50px"';
	$fields['seosm']['height']='300px';
	$fields['seosm']['title']='Короткий SEO';

	//intro
	$fields['seofull']['input']='fck';
	$fields['seofull']['type']='html';
	$fields['seofull']['table']=$tbl;
	$fields['seofull']['multi_lang']=true;
	$fields['seofull']['html_params']='style="width:100%; height:50px"';
	$fields['seofull']['height']='300px';
	$fields['seofull']['title']='Полный текст SEO';

	//genres
	$fields['genres']['input']='multisearchbox';
	$fields['genres']['type']='text';
	//$fields['genres']['req']=true;
	$fields['genres']['onkeyup']="searchInList(this,'2','genres','?mod=main&act=ajx_genres_list&mode=option');";
	$fields['genres']['title']=sys::translate('main::genres');

	//studios
	$fields['studios']['input']='multisearchbox';
	$fields['studios']['type']='text';
	//$fields['studios']['req']=true;
	$fields['studios']['onkeyup']="searchInList(this,'2','studios','?mod=main&act=ajx_studios_list&mode=option');";
	$fields['studios']['title']=sys::translate('main::studios');

	//countries
	$fields['countries']['input']='multisearchbox';
	$fields['countries']['type']='text';
	//$fields['countries']['req']=true;
	$fields['countries']['onkeyup']="searchInList(this,'2','countries','?mod=main&act=ajx_countries_list&mode=option');";
	$fields['countries']['title']=sys::translate('main::countries');

	//distributors
	$fields['distributors']['input']='multisearchbox';
	$fields['distributors']['type']='text';
	//$fields['distributors']['req']=true;
	$fields['distributors']['onkeyup']="searchInList(this,'2','distributors','?mod=main&act=ajx_distributors_list&mode=option');";
	$fields['distributors']['title']=sys::translate('main::distributors');

	//yakaboo_url
	$fields['yakaboo_url']['input']='textbox';
	$fields['yakaboo_url']['type']='text';
	$fields['yakaboo_url']['title']=sys::translate('main::yakaboo_url');
	$fields['yakaboo_url']['table']=$tbl;
	$fields['yakaboo_url']['html_params']='style="width:100%" maxlength="255"';

	//banner
	$fields['banner']['input']='textarea';
	$fields['banner']['type']='html';
	$fields['banner']['table']=$tbl;
	$fields['banner']['html_params']='style="width:100%; height:50px"';
	$fields['banner']['title']=sys::translate('main::banner');

	//=========================================================================================================//

	//Проверка и сохранение данных--------------------------------------------------------------------------
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['world_premiere']=sys::date2Db($values['world_premiere'],false,$_cfg['sys::date_format']);
			$values['ukraine_premiere']=sys::date2Db($values['ukraine_premiere'],false,$_cfg['sys::date_format']);
			$values['imax_premiere']=sys::date2Db($values['imax_premiere'],false,$_cfg['sys::date_format']);
			$values['coded']=implode(',',dmstring($values['title_orig']));
			$values['budget'] = str_replace(",", ".", $values['budget']);

			$fields['coded']['input'] = 'textbox';
			$fields['coded']['type'] = 'text';
			$fields['coded']['table'] = 'main_films';

			//Вставка данных
			if(!$_GET['id'])
			{
                $datetime = new DateTime();
                $values['date'] = $datetime->format('Y-m-d H:i:s');
            	$fields['date']['input']='datebox';
            	$fields['date']['type']='date';
            	$fields['date']['table']=$tbl;
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}

			$tru = $values['title_ru'];
			$tuk = $values['title_uk'];


			$qw=$_db->query("
				UPDATE `#__main_films_lng`
				SET coded = '".implode(',',dmstring($tru))."'
				WHERE record_id = '".$_GET['id']."' AND lang_id = 1
			");

			$qw=$_db->query("
				UPDATE `#__main_films_lng`
				SET coded = '".implode(',',dmstring($tuk))."'
				WHERE record_id = '".$_GET['id']."' AND lang_id = 3
			");

			// удалить запись об imdb
			//$_db->query("DELETE FROM `#__main_films_imdb_to_kt` WHERE kt_id = '".doubleval($_GET['id'])."'");
			// вставить запись об imdb
			//$_db->query("INSERT INTO `#__main_films_imdb_to_kt` (kt_id,imdb_id) VALUES ('".doubleval($_GET['id'])."','".$values['imdb_id']."')");
			
/*			$flds['coded']['input'] = 'textbox';
			$flds['coded']['type'] = 'text';
			$flds['coded']['table'] = 'main_films';
			$flds['coded']['multi_lang'] = '1';

			$vales['coded_ru']=implode(',',dmstring($tru));
			$vales['coded_uk']=implode(',',dmstring($tuk));

			$_db->updateArray($tbl,$vales,$flds,$_GET['id']);
*/
			$values['world_premiere']=sys::db2Date($values['world_premiere'],false, $_cfg['sys::date_format']);
			$values['ukraine_premiere']=sys::db2Date($values['ukraine_premiere'],false, $_cfg['sys::date_format']);
			$values['imax_premiere']=sys::db2Date($values['imax_premiere'],false, $_cfg['sys::date_format']);
		}

		if(!$_err)
		{
			if($_FILES['album_poster']['name'])
				$values['album_poster']=main_films::uploadAlbumPoster($_FILES['album_poster'],$_GET['id'],$values['album_poster']);
			main_films::linkFilmGenres($_GET['id'],$values['genres']);
			main_films::linkFilmStudios($_GET['id'],$values['studios']);
			main_films::linkFilmCountries($_GET['id'],$values['countries']);
			main_films::linkFilmDistributors($_GET['id'],$values['distributors']);
			sys_gui::afterSave();
//			if(sys::isDebugIP()) {				 
//				 sys::printR($_cfg);
				 $href = main_films::getFilmUrl($_GET['id']);
				 $message = "Добавлен (или изменен) фильм <a href=$href>{$values['name']}</a> \r\nРедактор: {$_user["login"]} ({$_user["main_first_name"]} {$_user["main_last_name"]})";					 
			 	 sys::addMail('noreply@kino-teatr.ua', false, $_cfg["sys::from_email"], $_cfg["sys::from_email"], 'Изменение или добавление фильма через админку', false, $message, false, mb_detect_encoding($message), false, 'sendMailObject');
//			}
		}
	}
	//==========================================================================================================//

	//Удаление постера альбома
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='album_poster')
		{
			$_db->setValue('main_films','album_poster',false,$_GET['id']);
			if(is_file($_cfg['main::films_dir'].$values['album_poster']))
				unlink($_cfg['main::films_dir'].$values['album_poster']);
			$values['album_poster']=false;
		}
	}
	//==================================================================================================

	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//

	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================

	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	if($_GET['id'])
	{
		$panel['class']='blank';
		$panel['text']=main_films::showFilmAdminTabs($_GET['id'],'info');
		$result['panels'][]=$panel;
	}
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>