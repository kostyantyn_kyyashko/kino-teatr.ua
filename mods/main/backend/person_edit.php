<?
function main_person_edit()
{
	if(!sys::checkAccess('main::persons'))
		return 403;
	
	global $_db, $_err, $_cfg;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::persons');
	
	sys::filterGet('id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='main_persons';
	$default=array();
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,intval($_GET['id']),true);
		if(!$record)
			return 404;
		$title=$record['name'];
		$record['birthdate']=sys::db2Date($record['birthdate'],false,$_cfg['sys::date_format']);
		$record['deathdate']=sys::db2Date($record['deathdate'],false,$_cfg['sys::date_format']);
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_person');
	}
    
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';
	
	//lastname_orig
	$fields['lastname_orig']['input']='textbox';
	$fields['lastname_orig']['type']='text';
	$fields['lastname_orig']['req']=true;
	$fields['lastname_orig']['table']=$tbl;
	$fields['lastname_orig']['html_params']='style="width:100%" maxlength="255"';
	$fields['lastname_orig']['title']=sys::translate('main::lastname_orig');
	
	//firstname_orig
	$fields['firstname_orig']['input']='textbox';
	$fields['firstname_orig']['type']='text';
	$fields['firstname_orig']['req']=true;
	$fields['firstname_orig']['table']=$tbl;
	$fields['firstname_orig']['html_params']='style="width:100%" maxlength="255"';
	$fields['firstname_orig']['title']=sys::translate('main::firstname_orig');
	
	//lastname
	$fields['lastname']['input']='textbox';
	$fields['lastname']['type']='text';
	$fields['lastname']['req']=true;
	$fields['lastname']['multi_lang']=true;
	$fields['lastname']['table']=$tbl;
	$fields['lastname']['html_params']='style="width:100%" maxlength="255"';
	$fields['lastname']['title']=sys::translate('main::lastname');
	
	//firstname
	$fields['firstname']['input']='textbox';
	$fields['firstname']['type']='text';
	$fields['firstname']['req']=true;
	$fields['firstname']['multi_lang']=true;
	$fields['firstname']['table']=$tbl;
	$fields['firstname']['html_params']='style="width:100%" maxlength="255"';
	$fields['firstname']['title']=sys::translate('main::firstname');
	
	//birthdate
	$fields['birthdate']['input']='datebox';
	$fields['birthdate']['type']='date';
	$fields['birthdate']['table']=$tbl;
	$fields['birthdate']['format']=$_cfg['sys::date_format'];
	$fields['birthdate']['title']=sys::translate('main::birthdate');
	
	//deathdate
	$fields['deathdate']['input']='datebox';
	$fields['deathdate']['type']='date';
	$fields['deathdate']['table']=$tbl;
	$fields['deathdate']['format']=$_cfg['sys::date_format'];
	$fields['deathdate']['title']=sys::translate('main::deathdate');
	
	//biography
	$fields['biography']['input']='fck';
	$fields['biography']['type']='html';
	$fields['biography']['table']=$tbl;
	$fields['biography']['multi_lang']=true;
	$fields['biography']['html_params']='style="width:100%; height:50px"';
	$fields['biography']['height']='300px';
	$fields['biography']['title']=sys::translate('main::biography');
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['birthdate']=sys::date2Db($values['birthdate'],false,$_cfg['sys::date_format']);
			$values['deathdate']=sys::date2Db($values['deathdate'],false,$_cfg['sys::date_format']);
			//Вставка данных
			if(!$_GET['id'])
			{
                $datetime = new DateTime();
                $values['date'] = $datetime->format('Y-m-d H:i:s');
            	$fields['date']['input']='datebox';
            	$fields['date']['type']='date';
            	$fields['date']['table']=$tbl;
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['birthdate']=sys::db2Date($values['birthdate'],false, $_cfg['sys::date_format']);
			$values['deathdate']=sys::db2Date($values['deathdate'],false, $_cfg['sys::date_format']);
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	if($_GET['id'])
	{
		$panel['class']='blank';
		$panel['text']=main_persons::showPersonAdminTabs($_GET['id'],'info');
		$result['panels'][]=$panel;
	}
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>