<?
function main_cinema_hall_edit()
{
	if(!sys::checkAccess('main::cinemas'))
		return 403;

	global $_db, $_err, $_cfg;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::cinemas');

	sys::filterGet('id','int');
	sys::filterGet('cinema_id','int');

	if(!isset($_POST['_task']))
		$_POST['_task']=false;

	$tbl='main_cinemas_halls';
	$default=array();
	$default['order_number']=$_db->getMaxOrderNumber($tbl,"`cinema_id`=".intval($_GET['cinema_id'])."");
	$default['cinema_id']=$_GET['cinema_id'];

	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,intval($_GET['id']),true);
		if(!$record)
			return 404;
		$title=main_cinemas::getHallName($_GET['id']);
	}
	else
	{
		$record=false;
		$title=sys::translate('main::new_hall');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//

	//Поля и форма----------------------------------------------------------------------------------------------

	//cinema_id
	$fields['cinema_id']['input']='hidden';
	$fields['cinema_id']['type']='int';
	$fields['cinema_id']['req']=true;
	$fields['cinema_id']['table']=$tbl;

	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';

	//public
	$fields['3d']['input']='checkbox';
	$fields['3d']['type']='bool';
	$fields['3d']['table']=$tbl;
	$fields['3d']['title']='3D';

	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	//$fields['title']['req']=true;
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="255"';

	//scheme
	$fields['scheme']['input']='filebox';
	$fields['scheme']['type']='file';
	$fields['scheme']['file::dir']=$_cfg['main::cinemas_dir'];
	$fields['scheme']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['scheme']['preview']=true;
	$fields['scheme']['title']=sys::translate('main::scheme');



	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	//=========================================================================================================//

	//Проверка и сохранение данных--------------------------------------------------------------------------
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{
			if($_FILES['scheme']['name'])
				$values['scheme']=main_cinemas::uploadHallScheme($_FILES['scheme'],$_GET['id'],$values['scheme']);
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//

	//Удаление схемы
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='scheme')
		{
			$_db->setValue('main_cinemas_halls','scheme',false,$_GET['id']);
			if(is_file($_cfg['main::cinemas_dir'].$values['scheme']))
				unlink($_cfg['main::cinemas_dir'].$values['scheme']);
			$values['scheme']=false;
		}
	}
	//==================================================================================================

	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//

	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================

	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	if($_GET['id'])
	{
		$panel['class']='blank';
		$panel['text']=main_cinemas::showHallTabs($_GET['id'],'info');
		$result['panels'][]=$panel;
	}
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>