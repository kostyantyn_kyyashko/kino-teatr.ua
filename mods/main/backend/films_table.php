<?
function main_films_table()
{
	if(!sys::checkAccess('main::films'))
		return 403;
		
	global $_db, $_err, $_cfg;
	sys::useLib('sys::pages');
	sys::useLib('main::films');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.desc');
	sys::filterGet('keywords','text');
	sys::filterGet('distributor_id');
		
	$tbl='main_films';
	$edit_url='?mod=main&act=film_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('main::add_film');
	$order_where=$where=false;
	$nav_point='main::films';
	$where="WHERE 1=1 ";
	
	if($_GET['keywords'])
	{
		$keywords=str_replace('*','%',urldecode($_GET['keywords']));
			$where.=" AND flm.name LIKE('".mysql_real_escape_string($keywords)."%')
				OR lng.title  LIKE('".mysql_real_escape_string($keywords)."%')";
	}
	
	if($_GET['distributor_id'])
	{
		$where.="AND d.distributor_id='".intval($_GET['distributor_id'])."'";	
		
	}
	
	$q="SELECT 
			flm.id, 
			flm.name,
			flm.ukraine_premiere,
			lng.title
		FROM 
			`#__".$tbl."` AS `flm`
		LEFT JOIN
			`#__main_films_lng` AS `lng`
		ON 
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		";
	if($_GET['distributor_id'])
	{
		$q.="
			LEFT JOIN `#__main_films_distributors` as `d`
			ON d.film_id=flm.id
			AND d.distributor_id='".intval($_GET['distributor_id'])."'
		";
	}
	
	$q.=$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_films::deleteFilm($task[1]);
			
		if($task[0]=='recount')
		{
			main_films::countFilmPreRating($task[1]);
			main_films::countFilmProRating($task[1]);
			main_films::countFilmRating($task[1]);
		}
			
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_films::deleteFilm($key);
				}
			}
					
		}	
		
		//Пересчет отмеченных
		if($_POST['_task']=='recountChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_films::countFilmPreRating($key);
					main_films::countFilmProRating($key);
					main_films::countFilmRating($key);
				}
			}
					
		}
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['name'].=' ('.$obj['title'].')';
		$obj['ukraine_premiere']=sys::db2Date($obj['ukraine_premiere'],false,$_cfg['sys::date_format']);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['recount']['type']='button';
	$table['fields']['recount']['onclick']="setMsg('".sys::translate('main::recounting')."'); document.forms['table'].elements._task.value='recount.%id%'; document.forms['table'].submit(); return false;";
	$table['fields']['recount']['image']='main::btn.recount.gif';
	$table['fields']['recount']['title']=sys::translate('main::recount_rating');
	
	$table['fields']['persons']['type']='button';
	$table['fields']['persons']['onclick']="window.open('?mod=main&act=film_persons_table&film_id=%id%','','".$win_params."');return false";
	$table['fields']['persons']['url']='?mod=main&act=film_persons_table&film_id=%id%';
	$table['fields']['persons']['title']=sys::translate('main::persons');
	$table['fields']['persons']['image']='main::btn.person.gif';
	
	$table['fields']['trailers']['type']='button';
	$table['fields']['trailers']['onclick']="window.open('?mod=main&act=film_trailers_table&film_id=%id%','','".$win_params."');return false";
	$table['fields']['trailers']['url']='?mod=main&act=film_trailers_table&film_id=%id%';
	$table['fields']['trailers']['title']=sys::translate('main::trailers');
	$table['fields']['trailers']['image']='main::btn.video.gif';
	
	$table['fields']['photos']['type']='button';
	$table['fields']['photos']['onclick']="window.open('?mod=main&act=film_photos_table&film_id=%id%','','".$win_params."');return false";
	$table['fields']['photos']['url']='?mod=main&act=film_photos_table&film_id=%id%';
	$table['fields']['photos']['title']=sys::translate('main::photos');
	$table['fields']['photos']['image']='main::btn.photo.gif';
	
	$table['fields']['posters']['type']='button';
	$table['fields']['posters']['onclick']="window.open('?mod=main&act=film_posters_table&film_id=%id%','','".$win_params."');return false";
	$table['fields']['posters']['url']='?mod=main&act=film_posters_table&film_id=%id%';
	$table['fields']['posters']['title']=sys::translate('main::posters');
	$table['fields']['posters']['image']='main::btn.poster.gif';
	
	$table['fields']['wallpapers']['type']='button';
	$table['fields']['wallpapers']['onclick']="window.open('?mod=main&act=film_wallpapers_table&film_id=%id%','','".$win_params."');return false";
	$table['fields']['wallpapers']['url']='?mod=main&act=film_wallpapers_table&film_id=%id%';
	$table['fields']['wallpapers']['title']=sys::translate('main::wallpapers');
	$table['fields']['wallpapers']['image']='main::btn.wallpaper.gif';
	
	$table['fields']['soundtracks']['type']='button';
	$table['fields']['soundtracks']['onclick']="window.open('?mod=main&act=film_soundtracks_table&film_id=%id%','','".$win_params."');return false";
	$table['fields']['soundtracks']['url']='?mod=main&act=film_soundtracks_table&film_id=%id%';
	$table['fields']['soundtracks']['title']=sys::translate('main::soundtracks');
	$table['fields']['soundtracks']['image']='main::btn.sound.gif';
	
	$table['fields']['links']['type']='button';
	$table['fields']['links']['onclick']="window.open('?mod=main&act=film_links_table&film_id=%id%','','".$win_params."');return false";
	$table['fields']['links']['url']='?mod=main&act=film_links_table&film_id=%id%';
	$table['fields']['links']['title']=sys::translate('main::links');
	$table['fields']['links']['image']='sys::btn.link.gif';
	
	$table['fields']['awards']['type']='button';
	$table['fields']['awards']['onclick']="window.open('?mod=main&act=film_awards_table&film_id=%id%','','".$win_params."');return false";
	$table['fields']['awards']['url']='?mod=main&act=film_awards_table&film_id=%id%';
	$table['fields']['awards']['title']=sys::translate('main::awards');
	$table['fields']['awards']['image']='main::btn.award.gif';
	
	$table['fields']['shows']['type']='button';
	$table['fields']['shows']['onclick']="window.open('?mod=main&act=film_shows_table&film_id=%id%','','".$win_params."');return false";
	$table['fields']['shows']['url']='?mod=main&act=film_shows_table&film_id=%id%';
	$table['fields']['shows']['title']=sys::translate('main::shows');
	$table['fields']['shows']['image']='main::btn.show.gif';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;
	
	$table['fields']['ukraine_premiere']['type']='text';
	$table['fields']['ukraine_premiere']['order']=true;
	$table['fields']['ukraine_premiere']['title']=sys::translate('main::ukraine_premiere');
	$table['fields']['ukraine_premiere']['width']=200;
	//======================================================================================================//
	
	//Поиск------------------------------------------------
	$search['action']='?mod=main&act=films_table';
	$search['html_params']='style="width:150px" maxlenght="255"';
	//======================================================
	
	//Селектор дистрибюторов------------------------------------------------
	$distributor['input']='selectbox';
	$distributor['values']=$_db->getValues('main_distributors','name',false, "ORDER BY `name` DESC");
	$distributor['values'][0]=sys::translate('main::all_distributors');
	$distributor['values']=array_reverse($distributor['values'],true);
	$distributor['value']=$_GET['distributor_id'];
	$distributor['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\');window.location=\'?mod=main&act=films_table&distributor_id=\'+this.value"';
	$distributor['title']=sys::translate('main::distributor');
	//==============================================================
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['recount_checked']['onclick']="setMsg('".sys::translate('main::recounting')."'); document.forms['table'].elements._task.value='recountChecked'; document.forms['table'].submit(); return false;";
	$buttons['recount_checked']['image']='main::btn.recount.gif';
	$buttons['recount_checked']['title']=sys::translate('main::recount_checked');
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSearchForm($search).sys_gui::showSpacer().sys_form::parseField('distributor_id',$distributor);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
