<?
function main_users_delivery()
{
	if(!sys::checkAccess('main::distributors'))
		return 403;
	
    global $_db;
    
    $r = $_db->query("SELECT * FROM #__main_users_objects_list ORDER BY weight");
    $_objs = array();
    while ($_obj = $_db->fetchAssoc($r)) {
		$_obj['title']=$_db->getValue('main_users_objects_list','title',$_obj['id'],true);
        $_objs[] = $_obj;
    }
    
    reset($_objs);
    $objects = array();
    foreach ($_objs as $_obj) {
        $_obj['w1'] = get_count_week($_obj['id'], 1);
        $_obj['w2'] = get_count_week($_obj['id'], 2);
        $_obj['w3'] = get_count_week($_obj['id'], 3);
        $_obj['w4'] = get_count_week($_obj['id'], 4);
        $_obj['w5'] = get_count_week($_obj['id'], 5);
        $_obj['w6'] = get_count_week($_obj['id'], 6);
        $_obj['w7'] = get_count_week($_obj['id'], 7);
        $query = sprintf("
            SELECT
                COUNT(DISTINCT(user_id)) AS count
            FROM
                #__main_users_objects_subscribe
            WHERE
                object_id=%d AND
                (w1=1 OR w2=1 OR w3=1 OR w4=1 OR w5=1 OR w6=1 OR w7=1)
        ", $_obj['id']);
        $r = $_db->query($query);
        $info = $_db->fetchAssoc($r);
        $_obj['users'] = $info['count'];
        $params['objects'][] = $_obj;
    }
    
    $query = sprintf("
        SELECT
            COUNT(DISTINCT(user_id)) AS count
        FROM
            #__main_users_objects_subscribe
    ", $_obj['id']);
    $r = $_db->query($query);
    $info = $_db->fetchAssoc($r);
    $params['users'] = $info['count'];
    
    $params['u1'] = get_count_users(1);
    $params['u2'] = get_count_users(2);
    $params['u3'] = get_count_users(3);
    $params['u4'] = get_count_users(4);
    $params['u5'] = get_count_users(5);
    $params['u6'] = get_count_users(6);
    $params['u7'] = get_count_users(7);
    
    
    $params['cinema_subscribers'] = get_cinema_subscribers();
    
//if(sys::isDebugIP()) sys::printR($params);    
	$nav_point='main::users_delivery';
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);	
    $result['main']=sys::parseTpl('main::delivery',$params);
	return $result;
    
}


function get_count_week($id, $week) {
    global $_db;
    $query = sprintf("
        SELECT
            COUNT(id) AS count
        FROM
            #__main_users_objects_subscribe
        WHERE
            object_id=%d AND
            w%d = 1
    ", $id, $week);
    $r = $_db->query($query);
    $info = $_db->fetchAssoc($r);
    return $info['count'];
}


function get_count_users($week) {
    global $_db;
    $query = sprintf("
        SELECT
            COUNT(DISTINCT(user_id)) AS count
        FROM
            #__main_users_objects_subscribe
        WHERE
            w%d = 1
    ", $week);
    $r = $_db->query($query);
    $info = $_db->fetchAssoc($r);
    return $info['count'];
}


function get_cinema_subscribers() {
    global $_db;
    $query = "
        SELECT
            COUNT(DISTINCT(user_id)) AS count
        FROM
            #__main_users_cinema_subscribe
    ";
    $r = $_db->query($query);
    $info = $_db->fetchAssoc($r);
    return $info['count'];
}

?>
