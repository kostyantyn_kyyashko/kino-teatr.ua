<?
function main_menu_table()
{
	if(!sys::checkAccess('main::menu'))
		return 403;

	global $_db, $_err;
	sys::useLib('sys::pages');
	sys::useLib('main::menu');
	sys::useLib('sys::form');

	sys::filterGet('order_by','text','date.desc');
	sys::filterGet('menu_id','int');

	$tbl='main_menu_articles';
	$edit_url='?mod=main&act=menu_article_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('main::add_menu');
	$order_where=$where='1=1';
	$nav_point='main::menu';

	$q="SELECT
			art.id,
			art.parent_id,
			art.name,
			art.link,
			art.public
		FROM `#__".$tbl."` AS `art`
		WHERE ".$where;


	//===============================================================================================//

	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_menu::deleteArticle($task[1]);


		if($task[0]=='send')
		{

			$qu="SELECT
				*
			FROM `#__main_menu_articles`
			WHERE id=".$task[1]."
			";


			$ru=$_db->query($qu);

			while($obju=$_db->fetchAssoc($ru))
			{
				$letter = $obju['letter'];
			}



			$qu="SELECT
				*
			FROM `#__main_menu_winners`
			WHERE menu_id=".$task[1]."
			";


			$ru=$_db->query($qu);

			while($obju=$_db->fetchAssoc($ru))
			{
				sys::sendmenuMail('info@kino-teatr.ua','Kino-Teatr.ua',$obju['email'],$obju['username'],'Kino-Teatr.ua','',$letter);
			}

			$qu="UPDATE `#__main_menu_articles`
			SET
				`sent` = `
			WHERE id=".$task[1]."
			";


			$ru=$_db->query($qu);
		}

		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_menu::deleteArticle($key);
				}
			}

		}
	}


	//================================================================================================//

	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$pages['limit']);

	while($obj=$_db->fetchAssoc($r))
	{
		$table['records'][$obj['id']]=$obj;
	}

	$table['checkbox']=true;

	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";

	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';

	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;

	$table['fields']['public']['type']='checkbox';
	$table['fields']['public']['title']=sys::translate('sys::pbl');
	$table['fields']['public']['alt']=sys::translate('sys::public');
	$table['fields']['public']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~public~%id%~%public%\')"';
	$table['fields']['public']['width']=40;

	//======================================================================================================//

	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
	$buttons['new']['title']=$add_title;

	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';

	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';

	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";

	//=======================================================================================================//

	//Селектор разделов------------------------------------------------
	$section['input']='selectbox';
	$section['values']=$_db->getValues('main_menu','name',false, "ORDER BY `name` DESC");
	$section['values'][0]=sys::translate('main::all_sections');
	$section['values']=array_reverse($section['values'],true);
	$section['value']=$_GET['menu_id'];
	$section['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\');window.location=\'?mod=main&act=menu_table&menu_id=\'+this.value"';
	$section['title']=sys::translate('main::section');
	//==============================================================

	//Формирование результата------------------------------------------------------------------------------//

	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);

	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSpacer().sys_form::parseField('menu_id',$section);;
	$result['panels'][]['text']=sys_gui::showPages($pages);

	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
