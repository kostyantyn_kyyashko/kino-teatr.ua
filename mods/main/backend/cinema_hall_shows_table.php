<?
function main_cinema_hall_shows_table()
{
	if(!sys::checkAccess('main::cinemas'))
		return 403;
		
	global $_db, $_err, $_cfg;
	sys::useLib('sys::pages');
	sys::useLib('main::cinemas');
	sys::useLib('main::shows');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.desc');
	sys::filterGet('hall_id','int');
		
	if(!$_GET['hall_id'])
		return 404;
		
	$tbl='main_shows';
	$edit_url='?mod=main&act=show_edit';
	$win_params=sys::setWinParams('700','600');
	$add_title=sys::translate('main::add_show');
	$order_where=$where=" `hall_id`=".intval($_GET['hall_id'])."";
	
	$title=main_cinemas::getHallName($_GET['hall_id']);
	
	if(!$title)
		return 404;
	
	$title=$title.' :: '.sys::translate('main::shows');
	
	$q="SELECT 
			shw.id, 
			shw.film_id,
			flm.name AS `film`,
			shw.begin,
			shw.end
		FROM `#__".$tbl."` AS `shw`
		
		LEFT JOIN `#__main_films` AS `flm`
		ON flm.id=shw.film_id
		
		WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_shows::deleteShow($task[1]);
		
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_shows::deleteShow($key);
				}
			}
					
		}	
		
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['begin']=sys::db2Date($obj['begin'],false, $_cfg['sys::date_format']);
		$obj['end']=sys::db2Date($obj['end'],false, $_cfg['sys::date_format']);
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['times']['type']='button';
	$table['fields']['times']['onclick']="window.open('?mod=main&act=show_times_table&show_id=%id%','','".$win_params."');return false";
	$table['fields']['times']['url']='?mod=main&act=show_times_table&show_id=%id%';
	$table['fields']['times']['title']=sys::translate('main::show_times');
	$table['fields']['times']['image']='main::btn.clock.gif';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['begin']['type']='text';
	$table['fields']['begin']['order']=true;
	$table['fields']['begin']['title']=sys::translate('main::begin');
	
	$table['fields']['end']['type']='text';
	$table['fields']['end']['order']=true;
	$table['fields']['end']['title']=sys::translate('main::end');
	
	$table['fields']['film']['type']='text';
	$table['fields']['film']['order']=true;
	$table['fields']['film']['onclick']="window.open('?mod=main&act=film_shows_table&film_id=%film_id%','','".$win_params."');return false";
	$table['fields']['film']['url']='?mod=main&act=film_shows_table&film_id=%film_id%';
	$table['fields']['film']['title']=sys::translate('main::film');
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&hall_id=".$_GET['hall_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=main_cinemas::showHallTabs($_GET['hall_id'],'shows');
	$result['panels'][]=$panel;
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
