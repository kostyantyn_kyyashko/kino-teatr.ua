<?
function main_ajx_countries_list()
{
	global $_db, $_err;
	sys::filterGet('keywords');
	sys::filterGet('field');
	sys::filterGet('mode');
	sys::setTpl();
	if(!$_GET['keywords'])
		return false;
	
	$q="
		SELECT `id`, `name` 
		FROM `#__main_countries`
	";

	if($_GET['keywords']!='*')
		$q.="WHERE `name` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'";
	
	$q.=" ORDER BY `name` ";
		
	$r=$_db->query($q);
	
	$list=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['name']=addslashes(($obj['name']));
		$list[]=$obj;
	}
	
	switch($_GET['mode'])
	{
		case 'option':
			echo sys_gui::showOptionsList($list,$_GET['field']);
		break;
		
		case 'value':
			echo sys_gui::showValuesList($list,$_GET['field']);
		break;
	}
}
?>