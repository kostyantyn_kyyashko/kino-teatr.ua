<?
function main_country_city_edit()
{
	if(!sys::checkAccess('main::countries'))
		return 403;
		
	global $_db, $_err, $_charsets;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::countries');
	
	sys::filterGet('id','int');
	sys::filterGet('country_id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='main_countries_cities';
	$default=array();
	$default['order_number']=$_db->getMaxOrderNumber($tbl);
	$default['country_id']=$_GET['country_id'];
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,intval($_GET['id']),true);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_city');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//country_id
	$fields['country_id']['input']='hidden';
	$fields['country_id']['type']='int';
	$fields['country_id']['req']=true;
	$fields['country_id']['table']=$tbl;
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="255"';
	
	//title_where
	$fields['title_where']['input']='textbox';
	$fields['title_where']['type']='text';
	$fields['title_where']['req']=true;
	$fields['title_where']['multi_lang']=true;
	$fields['title_where']['table']=$tbl;
	$fields['title_where']['html_params']='style="width:100%" maxlength="255"';
	$fields['title_where']['title']=sys::translate('main::in_city');
	
	//title_what
	$fields['title_what']['input']='textbox';
	$fields['title_what']['type']='text';
	$fields['title_what']['req']=true;
	$fields['title_what']['multi_lang']=true;
	$fields['title_what']['table']=$tbl;
	$fields['title_what']['html_params']='style="width:100%" maxlength="255"';
	$fields['title_what']['title']=sys::translate('main::city_e');
	
	//code
	$fields['code']['input']='textbox';
	$fields['code']['type']='text';
	//$fields['code']['req']=true;
	$fields['code']['table']=$tbl;
	$fields['code']['html_params']='size="3" maxlength="3"';
	$fields['code']['title']=sys::translate('main::city_code');
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>