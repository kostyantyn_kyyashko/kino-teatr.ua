<?
function main_ajx_halls_list()
{
	global $_db, $_err;
	sys::filterGet('keywords');
	sys::filterGet('field');
	sys::filterGet('mode');
	sys::setTpl();
	if(!$_GET['keywords'])
		return false;
	$q="
		SELECT hls.id, CONCAT(cnm.name,'::',hls.name,' (',cts.name,')') AS `name`, hls.3d as hall_3d 
		FROM `#__main_cinemas_halls` AS `hls`
		
		LEFT JOIN `#__main_cinemas` AS `cnm`
		ON cnm.id=hls.cinema_id
		
		LEFT JOIN `#__main_countries_cities` AS `cts`
		ON cts.id=cnm.city_id
		
	";

	if($_GET['keywords']!='*')
		$q.="WHERE cnm.name LIKE '".mysql_real_escape_string($_GET['keywords'])."%'";
	$q.=" ORDER BY cnm.id  limit 0,100";
	$r=$_db->query($q);
	$list=array();
	while($obj=$_db->fetchAssoc($r))
	{
		if($obj['hall_3d']) $obj['name'].=' [3D]';
		$obj['name']=addslashes(($obj['name']));
		$list[]=$obj;
	}
	
	switch ($_GET['mode'])
	{
		case 'option':
			echo sys_gui::showOptionsList($list,$_GET['field']);
		break;
		
		case 'value':
			echo sys_gui::showValuesList($list,$_GET['field']);
		break;
	}
}
?>