<?
function main_film_award_edit()
{
	if(!sys::checkAccess('main::films'))
		return 403;
		
	global $_db, $_err, $_cfg, $_user;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::films');
	sys::useLib('main::persons');
	sys::useLib('main::awards');
		
	sys::filterGet('id','int');
	sys::filterGet('film_id','int',0);
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='main_films_awards';
	$default['film_id']=$_GET['film_id'];
	$default['public']=true;
	$default['order_number']=$_db->getMaxOrderNumber($tbl,"`film_id`=".intval($_GET['film_id'])."");
	$default['persons']=array();
	$default['award_id']=false;
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id']);
		if(!$record)
			return 404;
		$title=main_awards::getAwardName($record['award_id']);
		$record['persons']=main_films::getAwardPersonsIds($_GET['id']);
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_award');
	}
	
	$values=sys::setValues($record, $default);
	$values['persons']=main_persons::getPersonsNames($values['persons']);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//film_id
	$fields['film_id']['input']='hidden';
	$fields['film_id']['type']='int';
	$fields['film_id']['req']=true;
	$fields['film_id']['table']=$tbl;
	
	//result
	$fields['result']['input']='selectbox';
	$fields['result']['type']='text';
	$fields['result']['values']['win']=sys::translate('main::win');
	$fields['result']['values']['nom']=sys::translate('main::nomination');
	$fields['result']['table']=$tbl;
	$fields['result']['title']=sys::translate('main::result');
	$fields['result']['empty']='';
	
	//award_id
	$fields['award_id']['input']='searchbox';
	$fields['award_id']['type']='int';
	if(!$values['award_id'])
		$fields['award_id']['text']=sys::translate('sys::not_chosed');
	else
	{
		$fields['award_id']['text']=$title;
	}
	$fields['award_id']['req']=true;
	$fields['award_id']['table']=$tbl;
	$fields['award_id']['title']=sys::translate('main::award');
	$fields['award_id']['onkeyup']="searchInList(this,'2','award_id','?mod=main&act=ajx_awards_list&mode=value');";
	
	//year
	$fields['year']['input']='textbox';
	$fields['year']['type']='int';
	$fields['year']['req']=true;
	$fields['year']['table']=$tbl;
	$fields['year']['html_params']='size="4" maxlength="4"';
	$fields['year']['title']=sys::translate('main::year');
	
	//persons
	$fields['persons']['input']='multisearchbox';
	$fields['persons']['type']='text';
	$fields['persons']['onkeyup']="searchInList(this,'2','persons','?mod=main&act=ajx_persons_list&mode=option&film_id=".$values['film_id']."');";
	$fields['persons']['title']=sys::translate('main::persons');
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['user_id']=$_user['id'];
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			main_films::linkAwardPersons($_GET['id'],$values['persons']);
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			$_db->setValue('main_films_awards','image',false,$_GET['id']);
			main_films::deleteawardImage($values['image']);
			$values['image']=false;
		}
	}
	//==================================================================================================
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>