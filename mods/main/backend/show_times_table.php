<?
function main_show_times_table()
{
	if(!sys::checkAccess('main::shows'))
		return 403;
		
	global $_db, $_err;
	sys::useLib('sys::pages');
	sys::useLib('main::shows');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','time.asc');
	sys::filterGet('show_id','int');
		
	if(!$_GET['show_id'])
		return 404;
		
	$tbl='main_shows_times';
	$edit_url='?mod=main&act=show_time_edit';
	$win_params=sys::setWinParams('600','500');
	$add_title=sys::translate('main::add_show_time');
	$order_where=$where=" `show_id`=".intval($_GET['show_id'])."";
	
	$show=$_db->getValue('main_shows','id',$_GET['show_id']);
	
	$title=main_shows::getShowName($_GET['show_id']);
	
	if(!$title)
		return 404;
		
	$title=$title.' :: '.sys::translate('main::show_times');
	
	$q="SELECT 
			`id`, 
			`time`,
			`prices`
		FROM `#__".$tbl."`
		WHERE ".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_shows::deleteTime($task[1]);
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_shows::deleteTime($key);
				}
			}
					
		}	
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['time']['type']='text';
	$table['fields']['time']['order']=true;
	$table['fields']['time']['width']=20;
	$table['fields']['time']['title']=sys::translate('main::time');
	
	$table['fields']['prices']['type']='text';
	$table['fields']['prices']['title']=sys::translate('main::prices');
	//======================================================================================================//
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."&show_id=".$_GET['show_id']."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	
	$panel['class']='blank';
	$panel['text']=main_shows::showShowTabs($_GET['show_id'],'times');
	$result['panels'][]=$panel;
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
