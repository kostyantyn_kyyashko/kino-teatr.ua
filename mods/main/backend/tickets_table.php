<?

function ticket_show(){

	sys::useLib('main::cinemas');
	sys::useLib('main::films');
	sys::useLib('main::orders');
	sys::useLib('sys::pages');
	sys::useLib('sys::form');
	global $_db, $_cfg;
	$id = (int)$_REQUEST['id'];

	$lang_id = intval($_cfg['sys::lang_id']);
	
	
	$q="
			SELECT o.info
			FROM `#__main_users_orders` AS `o`
			WHERE o.id = $id";
			
		$result=array();
	
		$r=$_db->query($q);
		$obj=$_db->fetchAssoc($r);
		$buffer = json_decode($obj['info'],true);
		$table = array();
		$counter = 0;
		foreach($buffer['tickets'] as $item){
			$item['price'] /=100;
			$table['records'][$counter]=$item;
			$counter++;
		}
		
	
	$table['fields']['rowNumber']['type']='text';
	$table['fields']['rowNumber']['title']='Ряд';	
	
	$table['fields']['placeNumber']['type']='text';
	$table['fields']['placeNumber']['title']='Место';
	
	$table['fields']['price']['type']='text';
	$table['fields']['price']['title']='Цена';
	
	$table['fields']['hallName']['type']='text';
	$table['fields']['hallName']['title']='Зал';
	
	$table['fields']['showName']['type']='text';
	$table['fields']['showName']['title']='Фильм';
	
	$table['fields']['eventDate']['type']='text';
	$table['fields']['eventDate']['title']='Дата';
		
	//Формирование результата------------------------------------------------------------------------------//
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}





function main_tickets_table()
{
	if(!sys::checkAccess('main::tickets'))
		return 403;
//	error_reporting(E_ALL);
	if(isset($_REQUEST['show_ticket'])){
		$result = ticket_show();
		return $result;
	}

	sys::useLib('main::cinemas');
	sys::useLib('main::films');
	sys::useLib('main::orders');
	sys::useLib('sys::pages');
	sys::useLib('sys::form');
	global $_db, $_cfg;
	
	$nav_point='main::tickets';

	sys::filterGet('keywords','text');
	sys::filterGet('city_id','int');
	if($_GET['city_id'])
		sys::filterGet('order_by','text','order_number.asc');
	else
		sys::filterGet('order_by','text','id.desc');

	$lang_id = intval($_cfg['sys::lang_id']);
	$where="WHERE 1=1 ";
	
	if($_GET['keywords'])
	{
		$keywords=str_replace('*','%',urldecode($_GET['keywords']));
			$where.=" AND flm_lng.title LIKE('".mysql_real_escape_string($keywords)."%')
				OR cl.title  LIKE('".mysql_real_escape_string($keywords)."%')";
	}
	
	if(empty($_GET['type']) || $_GET['type'] == 'sold'){
		$data_field = 'o.data_buy';
		$where .= ' AND o.status = 1';
	}
	else{
		$data_field = 'o.data_fail';
		$where .= ' AND o.status = 0';
	}
	
	if($_GET['date-start'] && $_GET['date-end']){
		list($month,$day,$year) = explode('/',$_GET['date-start']);
		$data_start = $year.'-'.$month.'-'.$day;
		list($month,$day,$year) = explode('/',$_GET['date-end']);
		$data_end = $year.'-'.$month.'-'.$day;
		$where.=" AND $data_field BETWEEN '$data_start' AND '$data_end'";
	}
	elseif($_GET['date-start']){
		list($month,$day,$year) = explode('/',$_GET['date-start']);
		$data_start = $year.'-'.$month.'-'.$day;
		$where.=" AND $data_field > '$data_start'";
	}
	elseif($_GET['date-end']){
		list($month,$day,$year) = explode('/',$_GET['date-end']);
		$data_end = $year.'-'.$month.'-'.$day;
		$where.=" AND $data_field < '$data_end'";
	}
	
	$q="
			SELECT o.id, o.data_order, o.data_buy, o.data_fail, o.`sum`, hls_lng.title AS 'hall_name', hls.name AS 'hall', o.cinema_id, o.film_id, o.time_show, o.info, o.`sum`, cl.title AS cinema_name, flm_lng.title AS film_name, o.megakino_id, o.site_id, o.pmt_id, o.user_id, u.login
			FROM `#__main_users_orders` AS `o`
			LEFT JOIN `#__main_cinemas_halls` AS `hls` ON hls.id=o.hall_id
			LEFT JOIN `#__sys_users` AS `u` ON u.id=o.user_id
			LEFT JOIN `#__main_cinemas_halls_lng` AS `hls_lng` ON hls_lng.record_id=hls.id AND hls_lng.lang_id=$lang_id
			LEFT JOIN `#__main_films` AS `flm` ON flm.id=o.film_id
			LEFT JOIN `#__main_films_lng` AS `flm_lng` ON flm_lng.record_id=o.film_id AND flm_lng.lang_id=$lang_id
			LEFT JOIN `#__main_cinemas` AS `c` ON c.id=o.cinema_id
			LEFT JOIN `#__main_cinemas_lng` AS `cl` ON cl.record_id = c.id AND cl.lang_id = $lang_id
			$where
			AND o.type = 'buy'";
			
//	echo $q.'<br>';exit;
	
			
	if(isset($_REQUEST['export_csv'])){
		export_csv($q);
		exit;
	}
			
		$result=array();
	
		$result['pages']=sys_pages::pocess($q);
	

		foreach(array("first_page", "prev_page", "next_page", "last_page") as $key=>$keyval)
		{
			if(isset($result['pages'][$keyval]) && $result['pages'][$keyval])
				$result['pages'][$keyval] = $result['pages'][$keyval];
		}
		
		for($page=1; $page<=$result['pages']['last_page_number']; $page++)
		 if(isset($result['pages']['pages'][$page]))
			$result['pages']['pages'][$page] = $result['pages']['pages'][$page];
			
		$order_by=sys::parseOrderBy($_GET['order_by']);
		$r=$_db->query($q.$order_by.$result['pages']['limit']);
		
		$counter = 0;
		while ($obj=$_db->fetchArray($r))
		{
			$cinema_url=main_cinemas::getCinemaUrl($obj['cinema_id']);
			$obj['cinema_name']='<a href="'.$cinema_url.'" target="_blank">'.$obj['cinema_name'].'</a>';
			$user_url = '?mod=sys&act=user_edit?id='.$obj['user_id'];
			$obj['username']='<a href="'.$user_url.'" target="_blank">'.$obj['login'].'</a>';
			$film_url=main_films::getFilmUrl($obj['film_id']);
			$obj['film_name']='<a href="'.$film_url.'" target="_blank">'.$obj['film_name'].'</a>';
			$buffer = json_decode($obj['info'],true);
			$obj['tickets_count'] = count($buffer['tickets']);
			$obj['event_time']=main_orders::getDate($obj['info']);
			$obj['time_show']=$buffer['tickets'][0]['eventDate'];
			$table['records'][$obj['id']]=$obj;
			$counter++;
		}
		
	
	$table['fields']['megakino_id']['type']='text';
	$table['fields']['megakino_id']['order']=true;
	$table['fields']['megakino_id']['title']='№ заказа';	
	
	if(empty($_GET['type']) || $_GET['type'] == 'sold'){
		$table['fields']['data_buy']['type']='text';
		$table['fields']['data_buy']['order']=true;
		$table['fields']['data_buy']['title']='Дата покупки';
	}
	else{
		$table['fields']['data_fail']['type']='text';
		$table['fields']['data_fail']['order']=true;
		$table['fields']['data_fail']['title']='Дата возврата';
	}
	
	$table['fields']['time_show']['type']='text';
//	$table['fields']['time_show']['order']=true;
	$table['fields']['time_show']['title']='Сеанс';
	
	$table['fields']['film_name']['type']='custom';
	$table['fields']['film_name']['order']=true;
	$table['fields']['film_name']['title']='Фильм';
	
	$table['fields']['cinema_name']['type']='custom';
	$table['fields']['cinema_name']['order']=true;
	$table['fields']['cinema_name']['title']='Кинотеатр';
	
	$table['fields']['username']['type']='custom';
	$table['fields']['username']['title']='Пользователь';
	
	$table['fields']['tickets_count']['type']='text';
	$table['fields']['tickets_count']['title']='Билетов';
	
	$table['fields']['sum']['type']='text';
	$table['fields']['sum']['order']=true;
	$table['fields']['sum']['title']='Сумма';
	
	
	
	$table['fields']['info']['type']='button';
	$table['fields']['info']['onclick']="window.open('?mod=main&act=tickets_table&show_ticket=1&id=%id%','','width=790,height=590,directories=no,toolbar=no,location=no,resizable=yes,scrollbars=yes,menubar=no,dependent=yes,left=200,top=200');return false";
	$table['fields']['info']['url']='?mod=main&act=tickets_table&show_ticket=1&id=%id%';
	$table['fields']['info']['title']='Информация о сеансе';
	$table['fields']['info']['image']='main::btn.halls.gif';
	
	//Поиск------------------------------------------------
	$search['action']='?mod=main&act=tickets_table';
	$search['html_params']='style="width:150px" maxlenght="255"';
		
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	$result['panels'][]['text']=sys_gui::showSearchFormWithDatepickers($search);
	$result['panels'][]['text']=sys_gui::showPages($result['pages']);
	$show_table=' <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>';
	$show_table.='<script src="//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/jquery-ui.min.js"></script>';
	$show_table.='<script type="text/javascript" src="/mods/main/skins/default/js/widgets/datepicker.js"></script>';
	$show_table.='<script type="text/javascript" src="/mods/main/skins/default/js/datepicker.js"></script>';
	$show_table.='<link rel="stylesheet" type="text/css" media="screen" href="/mods/main/skins/default/css/jquery.ui.all.css">';
	$show_table.=sys_gui::showTable($table);
	$show_table.='<table style="margin-top: 15px;">';
	$link=$_SERVER['REQUEST_URI'].'&export_csv=1';
	$show_table.='<tr><td><a href="'.$link.'">Экспорт в xls</a></td></tr>';
	$show_table.='</table>';
	$result['main']=$show_table;
	//=========================================================================================================//
	return $result;
	
}

function export_csv($q){
	
	if(!sys::checkAccess('main::tickets'))
		return 403;
//	error_reporting(E_ALL);
	if(isset($_REQUEST['show_ticket'])){
		$result = ticket_show();
		return $result;
	}

	sys::useLib('main::cinemas');
	sys::useLib('main::films');
	sys::useLib('main::orders');
	sys::useLib('sys::pages');
	sys::useLib('sys::form');
	global $_db, $_cfg;
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by);
	require_once($_SERVER['DOCUMENT_ROOT'].'/mods/main/libs/phpexcel/Classes/PHPExcel.php');
	
	// Создаем объект класса PHPExcel
	$pExcel = new PHPExcel();
	// Устанавливаем индекс активного листа
	$pExcel->setActiveSheetIndex(0);
	// Получаем активный лист
	$aSheet = $pExcel->getActiveSheet();
	// Подписываем лист
	// Ориентация страницы и  размер листа
	$aSheet->getPageSetup()
       ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
	$aSheet->getPageSetup()
       ->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
	   
// Поля документа
	$aSheet->getPageMargins()->setTop(1);
	$aSheet->getPageMargins()->setRight(0.75);
	$aSheet->getPageMargins()->setLeft(0.75);
	$aSheet->getPageMargins()->setBottom(1);
// Название листа
	$aSheet->setTitle('Отчет по продаже билетов');
// Шапка и футер (при печати)
	$aSheet->getHeaderFooter()
       ->setOddHeader('Отчет по продаже билетов');
	$aSheet->getHeaderFooter()
       ->setOddFooter('&L&B'.$aSheet->getTitle().'&RСтраница &P из &N');
// Настройки шрифта
	$pExcel->getDefaultStyle()->getFont()->setName('Arial');
	$pExcel->getDefaultStyle()->getFont()->setSize(12);
	
$aSheet->getColumnDimension('A')->setWidth(10);
$aSheet->getColumnDimension('B')->setWidth(18);
$aSheet->getColumnDimension('C')->setWidth(10);
$aSheet->getColumnDimension('D')->setWidth(30);
$aSheet->getColumnDimension('E')->setWidth(15);
$aSheet->getColumnDimension('F')->setWidth(10);
$aSheet->getColumnDimension('G')->setWidth(10);
	
$aSheet->setCellValue('A1','№');
$aSheet->setCellValue('B1','Дата');
$aSheet->setCellValue('C1','Сеанс');
$aSheet->setCellValue('D1','Фильм');
$aSheet->setCellValue('E1','Кинотеатр');
$aSheet->setCellValue('F1','Билетов');
$aSheet->setCellValue('G1','Сумма');

		
		$counter = 2;
		while ($obj=$_db->fetchArray($r))
		{
			$film_url=main_films::getFilmUrl($obj['film_id']);
//			$obj['film_name']='<a href="'.$film_url.'" target="_blank">'.$obj['film_name'].'</a>';
			$buffer = json_decode($obj['info'],true);
			$obj['tickets_count'] = count($buffer['tickets']);
			$obj['event_time']=main_orders::getDate($obj['info']);
			$table['records'][$obj['id']]=$obj;
			$aSheet->setCellValue('A'.$counter, $obj['megakino_id']);
    		$aSheet->setCellValue('B'.$counter, $obj['data_buy']);
    		$aSheet->setCellValue('C'.$counter, $obj['time_show']);
    		$aSheet->setCellValue('D'.$counter, $obj['film_name']);
    		$aSheet->setCellValue('E'.$counter, $obj['cinema_name']);
    		$aSheet->setCellValue('F'.$counter, $obj['tickets_count']);
    		$aSheet->setCellValue('G'.$counter, $obj['sum']);
			$aSheet->getStyle('A'.$counter)->getAlignment()->setHorizontal(
    		PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$aSheet->getStyle('F'.$counter)->getAlignment()->setHorizontal(
    		PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$aSheet->getStyle('G'.$counter)->getAlignment()->setHorizontal(
    		PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
//    		$i++;
			$counter++;
		}
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Отчет о продаже билетов.xlsx"');
	header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	$objWriter = new PHPExcel_Writer_Excel2007($pExcel);
	$objWriter->save('php://output');
	exit;
	$csv = 'number;data_buy;time_show;film_name;cinema_name;tickets;sum; \r\n';
	foreach($table['records'] as $item){
		$csv.= $item['megakino_id'].';'.$item['data_buy'].';'.$item['data_show'].';'.$item['film_name'].';'.$item['cinema_name'].';'.$item['tickets_count'].';'.$item['sum'].' \r\n';
	}
	echo $csv;
	//=========================================================================================================//
	
}


?>
