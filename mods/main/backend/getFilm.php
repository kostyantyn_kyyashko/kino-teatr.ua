<?
function main_yandex_export()
{
	$ips=file(_SECURE_DIR.'export.txt');
	foreach ($ips as $ip)
	{
		$ip=explode('#',$ip);
		$ips2[]=trim($ip[0]);
	}
/*
	if(!in_array($_SERVER['REMOTE_ADDR'],$ips2))
		return 404; */
	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('main::persons');
	sys::useLib('main::countries');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	$result=array();



	//Показы-----------------------------------------------------------------
	$r=$_db->query("
			SELECT
			shw.begin,
			shw.end,
			shw.film_id,
			shw.id,
			hls.cinema_id
		FROM
			`#__main_shows` AS `shw`
		LEFT JOIN
			`#__main_cinemas_halls` AS `hls`
		ON
			hls.id=shw.hall_id
		WHERE
			shw.end>='".mysql_real_escape_string(date('Y-m-d'))."'
		ORDER BY
			shw.end ASC
	");

	$var['shows']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$films[$obj['film_id']]=$obj['film_id'];
		$cinemas[$obj['cinema_id']]=$obj['cinema_id'];
		$shows[$obj['id']]=$obj['id'];

		$obj['times']=array();
		$var['shows'][$obj['id']]=$obj;
	}

	//=======================================================================


	//Часы показов--------------------------------------------------------

	$r=$_db->query("
		SELECT
			tms.time,
			tms.prices,
			tms_lng.note,
			tms.show_id
		FROM
			`#__main_shows_times` AS `tms`
		LEFT JOIN
			`#__main_shows_times_lng` AS `tms_lng`
		ON
			tms_lng.record_id=tms.id
		AND
			tms_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			tms.show_id IN(".implode(',',$shows).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$var['shows'][$obj['show_id']]['times'][]=sys::cutStrRight($obj['time'],3);
	}
	//=========================================================================

	//Фильмы------------------------------------------------------------------
	$r=$_db->query("
		SELECT
			flm.id,
			lng.title,
			flm.title_orig,
			flm.title_alt,
			flm.duration,
			flm.year,
			flm.comments,
			flm.ukraine_premiere,
			flm.age_limit,
			flm.budget,
			flm.budget_currency,
			lng.intro,
			lng.text,
			flm.rating,
			flm.votes,
			flm.pro_rating,
			flm.pro_votes
		FROM
			`#__main_films` AS `flm`
		LEFT JOIN
			`#__main_films_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=1
		WHERE
			flm.id IN(".implode(',',$films).")

	");
	$var['films']=array();
	{
		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['title']=htmlspecialchars($obj['title']);
			$obj['title_alt']=htmlspecialchars($obj['title_alt']);
			$obj['title_orig']=htmlspecialchars($obj['title_orig']);
			$obj['photos']=array();
			$obj['trailers']=array();
			$obj['poster']=false;
			$obj['genres']=array();
			$obj['countries']=array();
			$obj['links']=array();
			$obj['directors']=array();
			$obj['comments_url']=main_films::getFilmDiscussUrl($obj['id']);
			$obj['cast']=array();
			if($obj['year']==0)
				$obj['year']=false;
			if(in_array($obj['ukraine_premiere'],array('0000-00-00','1970-01-01')))
				$obj['ukraine_premiere']=false;

			$var['films'][$obj['id']]=$obj;
		}
	}
	//============================================================================

	//Трейлеры фильмов-------------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`file`,
			`url`,
			`language`
		FROM
			`#__main_films_trailers`
		WHERE
			`film_id` IN(".implode(',',$films).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		if($obj['file'])
			$obj['url']=$_cfg['main::films_url'].$obj['file'];
		$obj['url']=htmlspecialchars($obj['url']);
		$var['films'][$obj['film_id']]['trailers'][]=$obj;
	}

	//=======================================================================

	//Постеры фильмов-------------------------------------------------
	$r=$_db->query("
		SELECT
			`film_id`,
			`image`,
			`order_number`
		FROM
			`#__main_films_posters`
		WHERE
			`film_id` IN(".implode(',',$films).")
		AND
			`order_number`=1
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$var['films'][$obj['film_id']]['poster']=$_cfg['main::films_url'].$obj['image'];
	}
	//================================================================

	//Перcоны фильмов------------------------------------------------------
	$r=$_db->query("
		SELECT
			fp.film_id,
			fp.profession_id,
			CONCAT(lng.firstname,' ',lng.lastname) AS `title`
		FROM
			`#__main_films_persons` AS `fp`
		LEFT JOIN
			`#__main_persons_lng` AS `lng`
		ON
			lng.record_id=fp.person_id
		AND
			lng.lang_id=1
		WHERE
			fp.film_id IN(".implode(',',$films).")
		AND
			fp.profession_id IN(1,2)
	");
	$persons=array();
	while($obj=$_db->fetchAssoc($r))
	{
		if($obj['profession_id']==1)
			$var['films'][$obj['film_id']]['directors'][]=$obj['title'];
		else
			$var['films'][$obj['film_id']]['cast'][]=$obj['title'];
	}
	//==============================================================

	//Жанры фильмов--------------------------------------
	$r=$_db->query("
		SELECT
			rels.film_id,
			lng.title
		FROM
			`#__main_films_genres` AS `rels`
		LEFT JOIN
			`#__main_genres_lng` AS `lng`
		ON
			lng.record_id=rels.genre_id
		AND
			lng.lang_id=1

		WHERE
			rels.film_id IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$var['films'][$obj['film_id']]['genres'][]=$obj['title'];
	}
	//================================================



	//Страны фильмов--------------------------------------
	$r=$_db->query("
		SELECT
			rels.film_id,
			lng.title
		FROM
			`#__main_films_countries` AS `rels`
		LEFT JOIN
			`#__main_countries_lng` AS `lng`
		ON
			lng.record_id=rels.country_id
		AND
			lng.lang_id=1
		WHERE
			rels.film_id IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$var['films'][$obj['film_id']]['countries'][]=$obj['title'];
	}
	//================================================


	//Кинотеатры-----------------------------------------------------
	$r=$_db->query("
		SELECT
			cnm.id,
			cnm.city_id,
			cnm.site,
			lng.title,
			cnm.site,
			cnm.phone,
			cnm.comments,
			lng.address,
			lng.text,
			cit.code
		FROM
			`#__main_cinemas` AS `cnm`
		LEFT JOIN
			`#__main_cinemas_lng` AS `lng`
		ON
			lng.record_id=cnm.id
		AND
			lng.lang_id=1
		LEFT JOIN
			`#__main_countries_cities` AS `cit`
		ON
			cit.id=cnm.city_id
		WHERE cnm.id IN(".implode(',',$cinemas).")
	");
	$result['cinemas']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['address']=htmlspecialchars($obj['address']);
		$obj['phone']=htmlspecialchars($obj['phone']);
		$obj['site']=htmlspecialchars($obj['site']);
		$obj['halls']=array();
		$obj['photos']=array();
		$var['cinemas'][$obj['id']]=$obj;
	}
	//============================================================

	//Фотки кинотеатра------------------------------------------------
	/*
	$r=$_db->query("
		SELECT
			`cinema_id`,
			`image`
		FROM
			`#__main_cinemas_photos`
		WHERE
			`cinema_id` IN(".implode(',',$cinemas).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['src']=$_cfg['main::cinemas_url'].$obj['image'];
		$result['cinemas'][$obj['cinema_id']]['photos'][]=$obj;
	}
	*/
	//=============================================================

	sys::setTpl();
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header("Content-type: text/xml; charset=cp-1251");
	echo'<?xml version="1.0"?> '
?>
<schedule>
	<?foreach ($var['films'] as $film):?>
		<event id="<?=$film['id']?>">
			<type>cinema</type>
			<title><?=$film['title']?></title>
			<original><?=$film['title_orig']?></original>
			<aka><?=$film['title_alt']?></aka>
			<runtime><?=$film['duration']?></runtime>
			<country><?if(isset($film['countries'][0])):?><?=$film['countries'][0]?><?endif?></country>
			<year><?=$film['year']?></year>
			<release_date><?=$film['ukraine_premiere']?></release_date>
			<genre><?=implode(', ',$film['genres'])?></genre>
			<director><?=implode(', ',$film['directors'])?></director>
			<cast><?=implode(', ',$film['cast'])?></cast>
			<image><?=$film['poster']?></image>
			<?if($film['trailers']):?>
				<trailers>
					<?foreach ($film['trailers'] as $trailer):?>
					   <trailer_url language="<?=$trailer['language']?>">
					       <?=$trailer['url']?>
					   </trailer_url>
				   <?endforeach;?>
				</trailers>
			<?endif?>
			<review>
				<text><?=htmlspecialchars(strip_tags($film['intro']))?></text>
			</review>
			<users count="<?=$film['comments']?>"><?=$film['comments_url']?></users>
			<type-name>movie</type-name>
			<club_event>0</club_event>
		</event>
	<?endforeach;?>


	<?foreach ($var['cinemas'] as $cinema):?>
		<place id="<?=$cinema['id']?>">
			<title><?=$cinema['title']?></title>
			<city><?=$cinema['code']?></city>
			<address><?=$cinema['address']?></address>
			<phone><?=$cinema['phone']?></phone>
			<type>cinema</type>
			<url><?=$cinema['site']?></url>
			<review>
				<text><?=htmlspecialchars(strip_tags($cinema['text']))?></text>
				<url><?=main_cinemas::getCinemaUrl($cinema['id'])?></url>
			</review>
			<users count="<?=$cinema['comments']?>"><?=main_cinemas::getCinemaDiscussUrl($cinema['id'])?></users>
		</place>
	<?endforeach;?>


	<?foreach ($var['shows'] as $show):?>
		<section id="<?=$show['id']?>" date="<?=$show['begin']?>" date_finish="<?=$show['end']?>" time="<?=implode(',',$show['times'])?>">
			<event id="<?=$show['film_id']?>" />
			<place id="<?=$show['cinema_id']?>" />

		</section>
	<?endforeach;?>

</schedule>
<?
}
