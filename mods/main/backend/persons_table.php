<?
function main_persons_table()
{
	if(!sys::checkAccess('main::persons'))
		return 403;
		
	global $_db, $_err, $_cfg;
	sys::useLib('sys::pages');
	sys::useLib('main::persons');
	sys::useLib('sys::form');
	
	sys::filterGet('order_by','text','id.desc');
	sys::filterGet('keywords','text');
		
	$tbl='main_persons';
	$edit_url='?mod=main&act=person_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('main::add_person');
	$order_where=$where=false;
	$nav_point='main::persons';
	$where="WHERE 1=1 ";
	
	if($_GET['keywords'])
	{
		$keywords=str_replace('*','%',urldecode($_GET['keywords']));
			$where.=" 
				AND (
					prs.name LIKE('".mysql_real_escape_string($keywords)."%')
					OR
					prs_lng.lastname LIKE '".mysql_real_escape_string($keywords)."%'
				) 
			";
	}
	
	$q="SELECT 
			prs.id, 
			prs.name,
			CONCAT(prs_lng.lastname,' ',prs_lng.firstname) AS `fio`  
		FROM 
			`#__".$tbl."` AS `prs`
		LEFT JOIN 
			`#__main_persons_lng` as `prs_lng`
		ON
			prs_lng.record_id=prs.id
		AND
			prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		".$where;
	//===============================================================================================//
	
	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_persons::deletePerson($task[1]);
			
		
		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else 
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_persons::deletePerson($key);
				}
			}
					
		}	
	}
	//================================================================================================//
	
	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);
	
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['name'].=' ('.$obj['fio'].')';
		$table['records'][$obj['id']]=$obj;
	}
	
	$table['checkbox']=true;
	
	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";
	
	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';
	
	$table['fields']['photos']['type']='button';
	$table['fields']['photos']['onclick']="window.open('?mod=main&act=person_photos_table&person_id=%id%','','".$win_params."');return false";
	$table['fields']['photos']['url']='?mod=main&act=person_photos_table&person_id=%id%';
	$table['fields']['photos']['title']=sys::translate('main::photos');
	$table['fields']['photos']['image']='main::btn.photo.gif';
	
	$table['fields']['links']['type']='button';
	$table['fields']['links']['onclick']="window.open('?mod=main&act=person_links_table&person_id=%id%','','".$win_params."');return false";
	$table['fields']['links']['url']='?mod=main&act=person_links_table&person_id=%id%';
	$table['fields']['links']['title']=sys::translate('main::links');
	$table['fields']['links']['image']='sys::btn.link.gif';
	
	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;
	
	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;
	//======================================================================================================//
	
	//Поиск------------------------------------------------
	$search['action']='?mod=main&act=persons_table';
	$search['html_params']='style="width:150px" maxlenght="255"';
	//======================================================
	
	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
	$buttons['new']['title']=$add_title;
	
	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';
	
	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";
	
	//=======================================================================================================//
	
	//Формирование результата------------------------------------------------------------------------------//
	
	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);
	
	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSearchForm($search);
	$result['panels'][]['text']=sys_gui::showPages($pages);
	
	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
