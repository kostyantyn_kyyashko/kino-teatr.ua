<?
function main_gossip_table()
{
	if(!sys::checkAccess('main::gossip'))
		return 403;

	global $_db, $_err;
	sys::useLib('sys::pages');
	sys::useLib('main::gossip');
	sys::useLib('sys::form');

	sys::filterGet('order_by','text','date.desc');
	sys::filterGet('gossip_id','int');

	$tbl='main_gossip_articles';
	$edit_url='?mod=main&act=gossip_article_edit';
	$win_params=sys::setWinParams('790','590');
	$add_title=sys::translate('main::add_gossip');
	$order_where=$where='1=1';
	if($_GET['gossip_id'])
		$order_where=$where=" art.gossip_id=".intval($_GET['gossip_id'])."";
	$nav_point='main::gossip';

	$q="SELECT
			art.id,
			art.user_id,
			usr.login AS `user`,
			art.name,
			art.date,
			art.public,
			art.exclusive,
			(art.anounce_id>0) AS anounce,
			nws.name AS `section`
		FROM `#__".$tbl."` AS `art`
		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=art.user_id
		LEFT JOIN `#__main_gossip` AS `nws`
		ON art.gossip_id=nws.id
		WHERE ".$where;
	//===============================================================================================//

	//Обработка действия-----------------------------------------------------------------------------//
	if(isset($_POST['_task']))
	{
		//Удаление
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete')
			main_gossip::deleteArticle($task[1]);

		//Удаление отмеченных
		if($_POST['_task']=='deleteChecked')
		{
			if(!isset($_POST['_chk']))
				$_err=sys::translate('sys::you_not_checked_any_record');
			else
			{
				foreach ($_POST['_chk'] as $key=>$val)
				{
					main_gossip::deleteArticle($key);
				}
			}

		}
	}
	//================================================================================================//

	//Таблица--------------------------------------------------------------------------------------//
	$pages=sys_pages::pocess($q);
	$order_by=sys::parseOrderBy($_GET['order_by']);
	$r=$_db->query($q.$order_by.$pages['limit']);

	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true);
		$table['records'][$obj['id']]=$obj;
	}

	$table['checkbox']=true;

	$table['fields']['delete']['type']='button';
	$table['fields']['delete']['onclick']="if(confirm('".sys::translate('sys::delete')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='delete.%id%'; document.forms['table'].submit();} return false;";

	$table['fields']['edit']['type']='button';
	$table['fields']['edit']['onclick']="window.open('".$edit_url."&id=%id%','','".$win_params."');return false";
	$table['fields']['edit']['url']=$edit_url.'&id=%id%';

	$table['fields']['id']['type']='text';
	$table['fields']['id']['order']=true;

	$table['fields']['name']['type']='text';
	$table['fields']['name']['order']=true;

	$table['fields']['section']['type']='text';
	$table['fields']['section']['order']=true;
	$table['fields']['section']['title']=sys::translate('main::section');

	$table['fields']['user']['type']='text';
	$table['fields']['user']['order']=true;
	$table['fields']['user']['width']=150;
	$table['fields']['user']['onclick']="window.open('?mod=sys&act=user_edit&id=%user_id%','','".$win_params."');return false";
	$table['fields']['user']['url']='?mod=sys&act=user_edit&id=%user_id%';

	$table['fields']['date']['type']='text';
	$table['fields']['date']['order']=true;
	$table['fields']['date']['width']=150;

	$table['fields']['public']['type']='checkbox';
	$table['fields']['public']['title']=sys::translate('sys::pbl');
	$table['fields']['public']['alt']=sys::translate('sys::public');
	$table['fields']['public']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~public~%id%~%public%\')"';
	$table['fields']['public']['width']=40;

	$table['fields']['exclusive']['type']='checkbox';
	$table['fields']['exclusive']['title']=sys::translate('main::exc');
	$table['fields']['exclusive']['alt']=sys::translate('main::exclusive');
	$table['fields']['exclusive']['html_params']='onchange="setMsg(\''.sys::translate('sys::saving').'\'); ajaxRequest(\'?mod=sys&act=ajx_default&task=switch~'.$tbl.'~exclusive~%id%~%exclusive%\')"';
	$table['fields']['exclusive']['width']=40;

	$table['fields']['anounce']['type']='checkbox';
	$table['fields']['anounce']['title']=sys::translate('main::anounce');
	$table['fields']['anounce']['alt']=sys::translate('main::anounce');
	$table['fields']['anounce']['html_params']='onchange="setMsg(\''.sys::translate('main::anouncing').'\'); ajaxRequest(\'?mod=main&act=ajx_anounce_put&id=%id%&feed=gossip&on=\'+this.checked); return 0;"';
	$table['fields']['anounce']['width']=40;
	//======================================================================================================//

	//Кнопки------------------------------------------------------------------------------------------------//
	$buttons['new']['onclick']="window.open('".$edit_url."','','".$win_params."');";
	$buttons['new']['title']=$add_title;

	$buttons['delete_checked']['onclick']="if(confirm('".sys::translate('sys::delete_checked')."? ".sys::translate('sys::are_you_sure')."')){setMsg('".sys::translate('sys::deleting')."'); document.forms['table'].elements._task.value='deleteChecked'; document.forms['table'].submit();} return false;";
	$buttons['delete_checked']['image']='sys::btn.delete.gif';

	$buttons['order_numbers']['onclick']="setMsg('".sys::translate('sys::saving')."');document.forms['table'].elements._task.value='orderNumbers'; document.forms['table'].submit();";
	$buttons['order_numbers']['image']='sys::btn.renumber.gif';

	$buttons['refresh']['onclick']="setMsg('".sys::translate('sys::loading')."');window.location='".$_SERVER['REQUEST_URI']."'";

	//=======================================================================================================//

	//Селектор разделов------------------------------------------------
	$section['input']='selectbox';
	$section['values']=$_db->getValues('main_gossip','name',false, "ORDER BY `name` DESC");
	$section['values'][0]=sys::translate('main::all_sections');
	$section['values']=array_reverse($section['values'],true);
	$section['value']=$_GET['gossip_id'];
	$section['html_params']='onchange="setMsg(\''.sys::translate('sys::loading').'\');window.location=\'?mod=main&act=gossip_table&gossip_id=\'+this.value"';
	$section['title']=sys::translate('main::section');
	//==============================================================

	//Формирование результата------------------------------------------------------------------------------//

	sys::setTitle(sys::translate($nav_point));
	$result['panels'][0]['text']=sys_gui::showNavPath($nav_point);

	$result['panels'][]['text']=sys_gui::showButtons($buttons).sys_gui::showSpacer().sys_form::parseField('gossip_id',$section);;
	$result['panels'][]['text']=sys_gui::showPages($pages);

	$result['main']=sys_gui::showTable($table);
	//=========================================================================================================//
	return $result;
}
?>
