<?
function main_film_trailer_edit()
{
	if(!sys::checkAccess('main::films'))
		return 403;
		
	global $_db, $_err, $_cfg, $_user;
	
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::films');
	sys::useLib('main::persons');
		
	sys::filterGet('id','int');
	sys::filterGet('film_id','int',0);
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
		
	$tbl='main_films_trailers';
	$default['film_id']=$_GET['film_id'];
	$default['public']=true;
	$default['date']=strftime($_cfg['sys::date_time_format']);
	$default['order_number']=$_db->getMaxOrderNumber($tbl,"`film_id`=".intval($_GET['film_id'])."");
	$default['persons']=array();
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id'],true);
		if(!$record)
			return 404;
		$title=$record['image'];
		$record['persons']=main_films::getTrailerPersonsIds($_GET['id']);
		$record['date']=sys::db2Date($record['date'],true,$_cfg['sys::date_time_format']);
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_trailer');
	}
	
	$values=sys::setValues($record, $default);
	$values['persons']=main_persons::getPersonsNames($values['persons']);
	//===========================================================================================================//
	
	
	//Поля и форма----------------------------------------------------------------------------------------------
	//film_id
	$fields['film_id']['input']='hidden';
	$fields['film_id']['type']='int';
	$fields['film_id']['req']=true;
	$fields['film_id']['table']=$tbl;
	
	//user_id
	$fields['user_id']['input']='hidden';
	$fields['user_id']['type']='int';
	$fields['user_id']['table']=$tbl;
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='size="55" maxlength="55"';
	$fields['title']['multi_lang']=true;
	
	//language
	$fields['language']['input']='textbox';
	$fields['language']['type']='text';
	$fields['language']['table']=$tbl;
	$fields['language']['html_params']='size="55" maxlength="55"';
	
	//duration
	$fields['duration']['input']='textbox';
	$fields['duration']['type']='int';
	$fields['duration']['table']=$tbl;
	$fields['duration']['html_params']='size="4" maxlength="4"';
	$fields['duration']['title']=sys::translate('main::duration_sec');
	
	//tab_num
	$fields['tab_num']['input']='selectbox';
	$fields['tab_num']['type']='int';
	$fields['tab_num']['table']=$tbl;
	$fields['tab_num']['title']=sys::translate('main::rol_type');
	$fields['tab_num']['values'][1]=sys::translate('main::trailers');
	$fields['tab_num']['values'][2]=sys::translate('main::tizers');
	$fields['tab_num']['values'][3]=sys::translate('main::fragments');
	$fields['tab_num']['values'][4]=sys::translate('main::semkis');
	$fields['tab_num']['values'][5]=sys::translate('main::tvs');
	$fields['tab_num']['values'][6]=sys::translate('main::intervius');

	//image
	$fields['image']['input']='filebox';
	$fields['image']['type']='file';
	$fields['image']['file::dir']=$_cfg['main::films_dir'];
	$fields['image']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image']['title']=sys::translate('main::image');
	$fields['image']['preview']=true;
	$fields['image']['req']=true;
	$fields['image']['preview::prefix']='x2_';
	
	//url
	$fields['url']['input']='textbox';
	$fields['url']['type']='text';
	$fields['url']['table']=$tbl;
	$fields['url']['html_params']='style="width:100%" maxlength="255"';
	
	//file
	$fields['file']['input']='filebox';
	$fields['file']['type']='file';
	$fields['file']['file::dir']=$_cfg['main::films_dir'];
	$fields['file']['file::formats']=array('mov','flv','mp4'); // By Mike
	$fields['file']['req']=true;
	

	
	//width
	$fields['width']['input']='textbox';
	$fields['width']['type']='int';
	$fields['width']['table']=$tbl;
	$fields['width']['html_params']='size="3" maxlength="3"';
	
	//height
	$fields['height']['input']='textbox';
	$fields['height']['type']='int';
	$fields['height']['table']=$tbl;
	$fields['height']['html_params']='size="3" maxlength="3"';
	
	//file_3gp
	$fields['file_3gp']['input']='filebox';
	$fields['file_3gp']['type']='file';
	$fields['file_3gp']['file::dir']=$_cfg['main::films_dir'];
	$fields['file_3gp']['file::formats']=array('3gp');
	$fields['file_3gp']['title']=sys::translate('main::file_3gp');
	
	//date
	$fields['date']['input']='datebox';
	$fields['date']['type']='date';
	$fields['date']['req']=true;
	$fields['date']['table']=$tbl;
	$fields['date']['format']=$_cfg['sys::date_time_format'];
	
	//persons
	$fields['persons']['input']='multisearchbox';
	$fields['persons']['type']='text';
	$fields['persons']['onkeyup']="searchInList(this,'2','persons','?mod=main&act=ajx_persons_list&mode=option&film_id=".$values['film_id']."');";
	$fields['persons']['title']=sys::translate('main::persons');
	
	//order_number
	$fields['order_number']['input']='textbox';
	$fields['order_number']['type']='int';
	$fields['order_number']['req']=true;
	$fields['order_number']['table']=$tbl;
	$fields['order_number']['html_params']='size="6" maxlength="11"';
	
	//public
	$fields['public']['input']='checkbox';
	$fields['public']['type']='bool';
	$fields['public']['table']=$tbl;
	
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['user_id']=$_user['id'];
			$values['date']=sys::date2Db($values['date'],true,$_cfg['sys::date_time_format']);
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['date']=sys::db2Date($values['date'],true,$_cfg['sys::date_time_format']);
		}
		if(!$_err)
		{
			//Загрузить и отресайзить картинку	
			if($_FILES['image']['name'])
				$values['image']=main_films::uploadTrailerImage($_FILES['image'],$_GET['id'],$values['image']);
				
			//Загрузить файл
			if($_FILES['file']['name'])
				$values['file']=main_films::uploadTrailerFile($_FILES['file'],$_GET['id'],$values['file']);
			
			//Загрузить файл 3gp 
			if($_FILES['file_3gp']['name'])
				$values['file_3gp']=main_films::uploadTrailerFile3gp($_FILES['file_3gp'],$_GET['id'],$values['file_3gp']);
			
			main_films::linkTrailerPersons($_GET['id'],$values['persons']);
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
			
	//Удаление файла 
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			$_db->setValue('main_films_trailers','image',false,$_GET['id']);
			main_films::deletetrailerImage($values['image']);
			$values['image']=false;
		}
		
		if($task[1]=='file')
		{
			$_db->setValue('main_films_trailers','file',false,$_GET['id']);
			if(is_file($_cfg['main::films_dir'].$values['file']))
				unlink($_cfg['main::films_dir'].$values['file']);
			$values['file']=false;
		}
		
		if($task[1]=='file_3gp')
		{
			$_db->setValue('main_films_trailers','file',false,$_GET['id']);
			if(is_file($_cfg['main::films_dir'].$values['file_3gp']))
				unlink($_cfg['main::films_dir'].$values['file_3gp']);
			$values['file_3gp']=false;
		}
	}
	//==================================================================================================
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>