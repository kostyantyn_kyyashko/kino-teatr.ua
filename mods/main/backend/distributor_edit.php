<?
function main_distributor_edit()
{
	if(!sys::checkAccess('main::distributors'))
		return 403;
		
	global $_db, $_err, $_charsets;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::distributors');
	
	sys::filterGet('id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='main_distributors';
	$default=array();
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,intval($_GET['id']),true);
		if(!$record)
			return 404;
		$title=$record['name'];
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_distributor');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="255"';
	
	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="255"';
	
	//site
	$fields['site']['input']='textbox';
	$fields['site']['type']='text';
	$fields['site']['table']=$tbl;
	$fields['site']['html_params']='style="width:100%" maxlength="255"';
	
	//info
	$fields['info']['input']='textarea';
	$fields['info']['type']='text';
	$fields['info']['table']=$tbl;
	$fields['info']['html_params']='style="width:100%; height:250px"';
	$fields['info']['title']=sys::translate('sys::info');
	//=========================================================================================================//
	
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
		}
		if(!$_err)
		{	
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>