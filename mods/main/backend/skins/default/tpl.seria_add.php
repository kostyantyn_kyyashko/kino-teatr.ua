<?sys::useJs('sys::form');?>
<script type="text/javascript">
	function addTimeRow(obj)
	{	
		form=obj.parentNode.parentNode.parentNode.parentNode; 
		form.appendChild(document.createElement("BR"));
		table=obj.parentNode.parentNode.parentNode;
		new_table=table.cloneNode(true);
		form.appendChild(new_table);	
	}
</script>
<form <?if($var['id']):?> id="<?=$var['id']?>"<?endif?> onsubmit="return checkForm(this)" method="<?=$var['method']?>" action="<?=$var['action']?>" <?=$var['html_params']?>>
	<input type="hidden" name="_task">
	<?=sys::parseTpl('sys::filelds.default',$var)?>
			<table class="table">
				<tr>
					<th><?=sys::translate('main::date')?></th>
					<th><?=sys::translate('main::season')?></th>
					<th><?=sys::translate('main::seria')?></th>
					<th><?=sys::translate('main::title')?></th>
					<th>&nbsp;</th>
				</tr>
				<tr>
					<td width=70>
						<input style="width:100%" maxlength="255" type="text" name="date[]">
					</td>
					<td width=70>
						<input style="width:100%" maxlength="255" type="text" name="season[]">
					</td>					
					<td width=70>
						<input style="width:100%" maxlength="255" type="text" name="seria[]">
					</td>					
					<td>
						<input style="width:100%" maxlength="255" type="text" name="title[]">
					</td>					
					<td style="width:1px">
						<img title="<?=sys::translate('sys::add');?>" alt="<?=sys::translate('sys::add');?>" onclick="addTimeRow(this); return false;" align="absmiddle" class="image" alt="" src="<?=sys_gui::getIconSrc('sys::btn.add.gif')?>"></button>
					</td>
				</tr>
			</table>
	
</form>