<?
	$default_show_3dbox = false;
	if(!$_POST["_task"])
	{
		if(!isset($_POST['_film_3d']) && isset($_GET['flm_3d'])) $_POST['_film_3d']=$_GET['flm_3d'];
		if(!isset($_POST['_hall_3d']) && isset($_GET['hls_3d'])) $_POST['_hall_3d']=$_GET['hls_3d'];
		$default_show_3dbox = ($_POST['_film_3d'] && $_POST['_hall_3d'])?"visible":"hidden";
	}
	$hide_3d = intval($_POST['_film_3d'] && $_POST['_hall_3d']);
?>
<?sys::useJs('sys::form');?>
<script type="text/javascript">
	function addTimeRow(obj)
	{	
		form=obj.parentNode.parentNode.parentNode.parentNode; 
		form.appendChild(document.createElement("BR"));
		table=obj.parentNode.parentNode.parentNode;
		new_table=table.cloneNode(true);
		form.appendChild(new_table);	
	}
	function addTimeRowCheck(box)
	{
		box.previousSibling.previousSibling.value=box.checked?'on':'off';
	}
	function theUserCallBackFunction()
	{
		var arr = new Array();
		for(var i=0;i<arguments.length;i++) {
			if(arguments[i]=='') continue;
			var a = arguments[i].split("=");
			arr[a[0]] = a[1];
		}
		 var id = arr['caller_identification'];
		 var box = document.getElementsByClassName('listbox');
		 switch(id){
		 	case 'film_id': SelectedList(arr,box[0],id,'film_3d');break;
		 	case 'hall_id': SelectedList(arr,box[1],id,'hall_3d');break;
		 	default: return;
		 }
	}		
	function SelectedList(arr, el, id, name_3d)
	{
		var img=insertImage(box[0], name_3d+'_img', arr[name_3d]);
		try{
			var v=document.getElementById('_'+name_3d);
			v.value=arr[name_3d];
		}catch(e){}
		DealChecks();
	}
	function DealChecks()
	{
		var img1 = null;
		try{
			img1 = document.getElementById('film_3d_img');
		}catch(e){img1 = null;}

		var img2 = null;
		try{
			img2 = document.getElementById('hall_3d_img');
		}catch(e){img2 = null;}

		var is_3d = (img1 && img2 && (img1.style.visibility!='hidden') && (img2.style.visibility!='hidden'));
		var form = document.getElementById('form');
		var checkboxes = form.getElementsByTagName('input');
		for (var i = 0; i < checkboxes.length; i++) {
		  var t=checkboxes[i];
		  if(t.name=='3d[]')t.value=is_3d?"on":"off";
		  if(t.type=='checkbox'){
		  	t.checked=is_3d;
		  	t.style.visibility=is_3d?"":'hidden';
		  }		  
		}		
	}
 
	var _3dImageTimerId = setInterval(function() 
	{ 
		 var box = document.getElementsByClassName('listbox');
		 var v1 = insertImage(box[0], 'film_3d_img', '<?=intval($_POST['_film_3d'])?>');
		 var v2 = insertImage(box[1], 'hall_3d_img', '<?=intval($_POST['_hall_3d'])?>');
		 if(v1&&v2) clearInterval(_3dImageTimerId);
	}, 1000)	

	function insertImage(el, id, hide)
	{
		var img = null;
		try{
			img = document.getElementById(id);
		}catch(e){img = null;}
		if(!img)
		{
			img=el.appendChild(document.createElement("img"));
			img.setAttribute("src", '<?=$skin_url?>images/3dglass.jpg');
			img.setAttribute("id", id);
		}
		if(img) img.style.visibility=((hide<1)?'hidden':'');
		return img;
	}
</script>
<form <?if($var['id']):?> id="<?=$var['id']?>"<?endif?> onsubmit="return checkForm(this)" method="<?=$var['method']?>" action="<?=$var['action']?>" <?=$var['html_params']?>>
	<input type="hidden" name="_task">
	<input type="hidden" name="_film_3d" id="_film_3d" value="<?=($_POST["_film_3d"])?>">
	<input type="hidden" name="_hall_3d" id="_hall_3d" value="<?=($_POST["_hall_3d"])?>">
	<?=sys::parseTpl('sys::filelds.default',$var)?>
		<?if(isset($_POST['time'])):?>
			<?for ($i=0; $i<count($_POST['time']); $i++):?>
				<table class="table">
					<tr>
						<th><?=sys::translate('main::time')?></th>
						<th><?=sys::translate('main::prices')?></th>
						<th><?=sys::translate('main::3d')?></th>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<td style="width:1px">
							<input value="<?=$_POST['time'][$i]?>" size="5" maxlength="5" type="text" name="time[]">
						</td>
						<td>
							<input value="<?=$_POST['prices'][$i]?>" style="width:100%" maxlength="255" type="text" name="prices[]">
						</td>
						<td>
							<input value="<?=($_POST['3d'][$i]=="on")?"on":"off"?>" type="hidden" name="3d[]">
							<input 
								<?=($_POST['3d'][$i]=="on")?"checked":""?> 
								style="visibility:<?=(($hide_3d<1)?"hidden":"block")?>"
								type="checkbox" 
								onchange="addTimeRowCheck(this)" 
							    name="checkbox3d"
							>
						</td>
						<td style="width:1px">
							<img title="<?=sys::translate('sys::add');?>" alt="<?=sys::translate('sys::add');?>" onclick="addTimeRow(this); return false;" align="absmiddle" class="image" alt="" src="<?=sys_gui::getIconSrc('sys::btn.add.gif')?>"></button>
						</td>
					</tr>
					<?foreach ($_langs as $id=>$lang):?>
					<tr>
						<th colspan="4"><?=sys::translate('main::note')?> (<?=$lang['title']?>)</th>
					</tr>
					<tr>
						<td colspan="4"><input value="<?=$_POST['note_'.$lang['code']][$i]?>" type="text" style="width:100%" name="note_<?=$lang['code']?>[]"></td>
					</tr>
					<?endforeach;?>
				</table>
			<?endfor?>
		<?else:?>
			<table class="table">
				<tr>
					<th><?=sys::translate('main::time')?></th>
					<th><?=sys::translate('main::prices')?></th>
					<th><?=sys::translate('main::3d')?></th>
					<th>&nbsp;</th>
				</tr>
				<tr>
					<td style="width:1px">
						<input size="5" maxlength="5" type="text" name="time[]">
					</td>
					<td>
						<input style="width:100%" maxlength="255" type="text" name="prices[]">
					</td>					
					<td>
						<input value="on" type="hidden" name="3d[]">
						<input checked type="checkbox" onchange="addTimeRowCheck(this)" name="checkbox3d" style="visibility:<?=$default_show_3dbox?>">
					</td>
					<td style="width:1px">
						<img title="<?=sys::translate('sys::add');?>" alt="<?=sys::translate('sys::add');?>" onclick="addTimeRow(this); return false;" align="absmiddle" class="image" alt="" src="<?=sys_gui::getIconSrc('sys::btn.add.gif')?>"></button>
					</td>
				</tr>
				<?foreach ($_langs as $id=>$lang):?>
				<tr>
					<th colspan="4"><?=sys::translate('main::note')?> (<?=$lang['title']?>)</th>
				</tr>
				<tr>
					<td colspan="4"><input type="text" style="width:100%" name="note_<?=$lang['code']?>[]"></td>
				</tr>
				<?endforeach;?>
			</table>
		<?endif;?>
	
</form>