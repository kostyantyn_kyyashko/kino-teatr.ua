<?
function main_articles_article_edit()
{
	if(!sys::checkAccess('main::articles'))
		return 403;

	global $_db, $_err, $_cfg, $_user;

	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::articles');
	sys::useLib('main::persons');
	sys::useLib('main::spec_themes');
	sys::useLib('main::films');
	sys::useLib('main::genres');
	$text_type = "articles";

	sys::filterGet('id','int');
	sys::filterGet('articles_id','int',0);

	if(!isset($_POST['_task']))
		$_POST['_task']=false;


	$tbl='main_articles_articles';
	$default['articles_id']=$_GET['articles_id'];
	$default['public']=true;
	$default['date']=strftime($_cfg['sys::date_time_format']);
	$default['persons']=array();
	$default['spec_themes']=array();	
	$default['films']=array();
	$default['genres']=array();

	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,$_GET['id'],true);
		if(!$record)
			return 404;
		$title=$record['name'];
		$record['persons']=main_articles::getArticlePersonsIds($_GET['id']);
		$record['films']=main_articles::getArticleFilmsIds($_GET['id']);
		$record['date']=sys::db2Date($record['date'],true,$_cfg['sys::date_time_format']);
		$record['spec_themes']=main_articles::getArticleSpecThemesIds($_GET['id']);
		$record['genres']=main_genres::getTextGenresIds($_GET['id'], $text_type);
	}
	else
	{
		$record=false;
		$title=sys::translate('main::new_article');
	}

	$values=sys::setValues($record, $default);
	$values['persons']=main_persons::getPersonsNames($values['persons']);
	$values['films']=main_films::getFilmsNames($values['films']);
	$values['spec_themes']=main_spec_themes::getSpecThemesNames($values['spec_themes']);
	$values['genres']=main_genres::getGenresNames($values['genres']);
	//===========================================================================================================//


	//Поля и форма----------------------------------------------------------------------------------------------

	//articles_id
	$fields['articles_id']['input']='selectbox';
	$fields['articles_id']['type']='int';
	$fields['articles_id']['table']=$tbl;
	$fields['articles_id']['title']=sys::translate('main::section');
	$r=$_db->query("SELECT `id`, `name` FROM `#__main_articles` ORDER BY `order_number`");
	while ($section=$_db->fetchAssoc($r))
	{
		$fields['articles_id']['values'][$section['id']]=$section['name'];
	}

	//user_id
	$fields['user_id']['input']='hidden';
	$fields['user_id']['type']='int';
	$fields['user_id']['table']=$tbl;

	//city_id
	$fields['city_id']['input']='selectbox';
	$fields['city_id']['type']='int';
	$fields['city_id']['table']=$tbl;
	$fields['city_id']['title']=sys::translate('main::city');
	$r=$_db->query("SELECT `id`, `name` FROM `#__main_countries_cities` ORDER BY `order_number`");
	$fields['city_id']['values'][0]=sys::translate('main::all_cities');
	while ($city=$_db->fetchAssoc($r))
	{
		$fields['city_id']['values'][$city['id']]=$city['name'];
	}


	//name
	$fields['name']['input']='textbox';
	$fields['name']['type']='text';
	$fields['name']['req']=true;
	$fields['name']['table']=$tbl;
	$fields['name']['html_params']='style="width:100%" maxlength="55"';

	//title
	$fields['title']['input']='textbox';
	$fields['title']['type']='text';
	$fields['title']['req']=true;
	$fields['title']['multi_lang']=true;
	$fields['title']['table']=$tbl;
	$fields['title']['html_params']='style="width:100%" maxlength="55"';

	//date
	$fields['date']['input']='datebox';
	$fields['date']['type']='date';
	$fields['date']['req']=true;
	$fields['date']['table']=$tbl;
	$fields['date']['format']=$_cfg['sys::date_time_format'];

	//small_image
	$fields['small_image']['input']='filebox';
	$fields['small_image']['type']='file';
	$fields['small_image']['file::dir']=$_cfg['main::articles_dir'];
	$fields['small_image']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['small_image']['title']=sys::translate('main::small_image');
	$fields['small_image']['preview']=true;

	//image
	$fields['image']['input']='filebox';
	$fields['image']['type']='file';
	$fields['image']['file::dir']=$_cfg['main::articles_dir'];
	$fields['image']['file::formats']=array('gif','jpg','png','jpeg');
	$fields['image']['title']=sys::translate('main::image');
	$fields['image']['preview']=true;
	$fields['image']['preview::prefix']='x2_';

	//alt
	$fields['alt']['input']='textbox';
	$fields['alt']['type']='text';
	$fields['alt']['multi_lang']=true;
	$fields['alt']['table']=$tbl;
	$fields['alt']['html_params']='style="width:100%" maxlength="55"';
	$fields['alt']['title']=sys::translate('main::image_alt');

	//intro
	$fields['intro']['input']='textarea';
	$fields['intro']['type']='html';
	$fields['intro']['table']=$tbl;
	$fields['intro']['multi_lang']=true;
	$fields['intro']['html_params']='style="width:100%; height:50px"';
	$fields['intro']['height']='300px';
	$fields['intro']['title']=sys::translate('main::intro');

	//text
	$fields['text']['input']='fck';
	$fields['text']['type']='html';
	$fields['text']['table']=$tbl;
	$fields['text']['multi_lang']=true;
	$fields['text']['html_params']='style="width:100%; height:50px"';
	$fields['text']['height']='300px';

	//title
	$fields['tags']['input']='textbox';
	$fields['tags']['type']='text';
	$fields['tags']['req']=false;
	$fields['tags']['multi_lang']=false;
	$fields['tags']['table']=$tbl;
	$fields['tags']['html_params']='style="width:100%" ';
	$fields['tags']['title']=sys::translate('main::tags');

	//title
	$fields['tags_ua']['input']='textbox';
	$fields['tags_ua']['type']='text';
	$fields['tags_ua']['req']=false;
	$fields['tags_ua']['multi_lang']=false;
	$fields['tags_ua']['table']=$tbl;
	$fields['tags_ua']['html_params']='style="width:100%" ';
	$fields['tags_ua']['title']='Теги (укр)';

	//source
	$fields['source']['input']='textbox';
	$fields['source']['type']='text';
	$fields['source']['multi_lang']=true;
	$fields['source']['table']=$tbl;
	$fields['source']['html_params']='style="width:100%" maxlength="55"';
	$fields['source']['title']=sys::translate('main::source');

	//source_url
	$fields['source_url']['input']='textbox';
	$fields['source_url']['type']='text';
	$fields['source_url']['table']=$tbl;
	$fields['source_url']['html_params']='style="width:100%" maxlength="55"';
	$fields['source_url']['title']=sys::translate('main::source_url');

	//films
	$fields['films']['input']='multisearchbox';
	$fields['films']['type']='text';
	$fields['films']['onkeyup']="searchInList(this,'2','films','?mod=main&act=ajx_films_list&mode=option');";
	$fields['films']['title']=sys::translate('main::films');

	//persons
	$fields['persons']['input']='multisearchbox';
	$fields['persons']['type']='text';
	$fields['persons']['onkeyup']="searchInList(this,'2','persons','?mod=main&act=ajx_persons_list&mode=option');";
	$fields['persons']['title']=sys::translate('main::persons');

	//spec_themes
	$fields['spec_themes']['input']='multisearchbox';
	$fields['spec_themes']['type']='text';
	$fields['spec_themes']['onkeyup']="searchInList(this,'2','spec_themes','?mod=main&act=ajx_spec_themes_list&mode=option');";
	$fields['spec_themes']['title']=sys::translate('main::spec_themes');

	//genres
	$fields['genres']['input']='multisearchbox';
	$fields['genres']['type']='text';
	$fields['genres']['onkeyup']="searchInList(this,'2','genres','?mod=main&act=ajx_genres_list&mode=option');";
	$fields['genres']['title']=sys::translate('main::genres');
	
	//exclusive
	$fields['exclusive']['input']='checkbox';
	$fields['exclusive']['type']='bool';
	$fields['exclusive']['table']=$tbl;
	$fields['exclusive']['title']=sys::translate('main::exclusive');

	//public
	$fields['public']['input']='checkbox';
	$fields['public']['type']='bool';
	$fields['public']['table']=$tbl;

	//=========================================================================================================//

	//Проверка и сохранение данных--------------------------------------------------------------------------
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		if(!$_err=sys_check::checkValues($fields,$values))
		{
			$values['index']=strip_tags($values['text']);
			$values['date']=sys::date2Db($values['date'],true,$_cfg['sys::date_time_format']);

			//Вставка данных
			if(!$_GET['id'])
			{
				$values['user_id']=$_user['id'];
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['date']=sys::db2Date($values['date'],true,$_cfg['sys::date_time_format']);
		}
		if(!$_err)
		{
			//Загрузить и отресайзить картинку
			if($_FILES['image']['name'])
				$values['image']=main_articles::uploadArticleImage($_FILES['image'],$_GET['id'],$values['image']);

			if($_FILES['small_image']['name'])
				$values['small_image']=main_articles::uploadArticleSmallImage($_FILES['small_image'],$_GET['id'],$values['small_image']);

			main_articles::linkArticlePersons($_GET['id'],$values['persons']);
			main_articles::linkArticleSpecThemes($_GET['id'],$values['spec_themes']);
			main_articles::linkArticleFilms($_GET['id'],$values['films']);
			main_articles::setArticleIndex($_GET['id'],$values);
			main_genres::linkTextsGenres($_GET['id'], $text_type, $values['genres']);
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//

	//Удаление картики
	if(strpos($_POST['_task'],'delete_file')===0)
	{
		$task=explode('.',$_POST['_task']);
		if($task[1]=='image')
		{
			$_db->setValue('main_articles_articles','image',false,$_GET['id']);
			main_articles::deleteArticleImage($values['image']);
			$values['image']=false;
		}

		if($task[1]=='small_image')
		{
			$_db->setValue('main_articles_articles','small_image',false,$_GET['id']);
			if(is_file($_cfg['main::articles_dir'].$values['small_image']))
				unlink($_cfg['main::articles_dir'].$values['small_image']);
			$values['small_image']=false;
		}
	}
	//==================================================================================================

	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//

	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	$form['html_params']='enctype="multipart/form-data"';
	//=====================================================================

	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	$result['panels'][1]['text']=sys_gui::showButtons($buttons);
	$result['main']=sys_form::parseForm($form);
	//============================================================================================================
	return $result;
}
?>