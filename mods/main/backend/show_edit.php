<?
function main_show_edit()
{
	if(!sys::checkAccess('main::shows'))
		return 403;
	
	global $_db, $_err, $_cfg;
	sys::useLib('sys::form');
	sys::useLib('sys::check');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	
	sys::filterGet('id','int');
	sys::filterGet('copy_id','int');
	sys::filterGet('film_id','int');
	sys::filterGet('hall_id','int');
		
	if(!isset($_POST['_task']))
		$_POST['_task']=false;
		
	$tbl='main_shows';
	$default=array();
	$default['film_id']=$_GET['film_id'];
	$default['hall_id']=$_GET['hall_id'];
	
	if($_GET['copy_id'])
	{
		$default=$_db->getRecord($tbl,intval($_GET['copy_id']));
		$default['begin']=sys::db2Date($default['begin'],false,$_cfg['sys::date_format']);
		$default['end']=sys::db2Date($default['end'],false,$_cfg['sys::date_format']);
	}
	
	if($_GET['id'])
	{
		$record=$_db->getRecord($tbl,intval($_GET['id']));
		$record['begin']=sys::db2Date($record['begin'],false,$_cfg['sys::date_format']);
		$record['end']=sys::db2Date($record['end'],false,$_cfg['sys::date_format']);
		if(!$record)
			return 404;
		$title=main_shows::getShowName($_GET['id']);
	}
	else 
	{
		$record=false;
		$title=sys::translate('main::new_show');
	}
	$values=sys::setValues($record, $default);
	//===========================================================================================================//
	
	//Поля и форма----------------------------------------------------------------------------------------------
	
	//film_id
	$fields['film_id']['input']='searchbox';
	$fields['film_id']['type']='int';
	if(!$values['film_id'])
		$fields['film_id']['text']=sys::translate('sys::not_chosed');
	else
	{
		$fields['film_id']['text']=$_db->getValue('main_films','name',$values['film_id']);
	}
	$fields['film_id']['table']=$tbl;
	$fields['film_id']['title']=sys::translate('main::film');
	$fields['film_id']['req']=true;
	$fields['film_id']['onkeyup']="searchInList(this,'2','film_id','?mod=main&act=ajx_films_list&mode=value');";
	
	//hall_id
	$fields['hall_id']['input']='searchbox';
	$fields['hall_id']['type']='int';
	$fields['hall_id']['req']=true;
	if(!$values['hall_id'])
		$fields['hall_id']['text']=sys::translate('sys::not_chosed');
	else
	{
		$fields['hall_id']['text']=main_cinemas::getHallName($values['hall_id']);
	}
	$fields['hall_id']['table']=$tbl;
	$fields['hall_id']['title']=sys::translate('main::hall');
	$fields['hall_id']['onkeyup']="searchInList(this,'2','hall_id','?mod=main&act=ajx_halls_list&mode=value');";
	
	//begin
	$fields['begin']['input']='datebox';
	$fields['begin']['type']='date';
	$fields['begin']['req']=true;
	$fields['begin']['table']=$tbl;
	$fields['begin']['format']=$_cfg['sys::date_format'];
	$fields['begin']['title']=sys::translate('main::begin');
	
	//end
	$fields['end']['input']='datebox';
	$fields['end']['type']='date';
	$fields['end']['req']=true;
	$fields['end']['table']=$tbl;
	$fields['end']['format']=$_cfg['sys::date_format'];
	$fields['end']['title']=sys::translate('main::end');

	//=========================================================================================================//
	//Проверка и сохранение данных--------------------------------------------------------------------------	
	if($_POST['_task']=='save' || $_POST['_task']=='save_and_close')
	{
		$_err=sys_check::checkValues($fields,$values);	
		
		if(!$_err && !main_shows::checkShowDates($values['begin'],$values['end'],$values['film_id'],$values['hall_id'],$_GET['id']))
			$_err=sys::translate('main::invalid_show_dates');
		if(!$_err && !$_GET['id'])
			$_err=main_shows::checkTimes($_GET['id']);
		if(!$_err)
		{
			$values['begin']=sys::date2Db($values['begin'],false,$_cfg['sys::date_format']);
			$values['end']=sys::date2Db($values['end'],false, $_cfg['sys::date_format']);
			//Вставка данных
			if(!$_GET['id'])
			{
				if(!$_GET['id']=$_db->insertArray($tbl,$values,$fields))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			else //Обновление данных
			{
				if(!$_db->updateArray($tbl,$values,$fields,$_GET['id']))
					$_err=sys::translate('sys::db_error').$_db->err;
			}
			$values['begin']=sys::db2Date($values['begin'],false, $_cfg['sys::date_format']);
			$values['end']=sys::db2Date($values['end'],false, $_cfg['sys::date_format']);
		}
		if(!$_err)
		{	
			if(isset($_POST['time']))
			{				
				$_POST["may3d"] = $_POST["_film_3d"] && $_POST["_hall_3d"];
				main_shows::insertTimes($_GET['id']);
			}
			sys_gui::afterSave();
		}
	}
	//==========================================================================================================//
	
	//Кнопки---------------------------------------------------------------------------------------------------
	$buttons['save']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."');document.forms['form'].elements['_task'].value='save'; document.forms['form'].submit();} return false;";	
	$buttons['save_and_close']['onclick']="if(checkForm(document.forms['form'])){setMsg('".sys::translate('sys::saving')."'); document.forms['form'].elements['_task'].value='save_and_close'; document.forms['form'].submit();} return false;";
	//=========================================================================================================//
	
	//Форма----------------------------------------------------------
	$form['fields']=$fields;
	$form['values']=$values;
	//=====================================================================
	
	//Формирование результата------------------------------------------------------------------------------------
	sys::setTitle($title);
	$result['panels'][0]['text']=$title;
	$result['panels'][0]['class']='title';
	if($_GET['id'])
	{
		$panel['class']='blank';
		$panel['text']=main_shows::showShowTabs($_GET['id'],'info');
		$result['panels'][]=$panel;
	}
	$result['panels'][]['text']=sys_gui::showButtons($buttons);
	if($_GET['id'])
		$result['main']=sys_form::parseForm($form);
	else 
		$result['main']=sys_form::parseForm($form,'main::show_add');
	//============================================================================================================
	return $result;
}
?>