<?
function main_import_files()
{
	exit();
	global $_db, $_cfg, $_err, $_user, $_langs;
	sys::filterGet('mode');
	
	$result=array(0);
	
	$db_host='localhost';
	$db_name='kino_old';
	$db_name2='kino_old_new';
	$db_user='root';
	$db_pass='ZaoTioRaLaMK3';
	
	
	$img_dir=_PUBLIC_DIR.'img/';
	set_time_limit(0);
	
	
	//Разделы новостей
	if($_GET['mode']=='news' || $_GET['mode']=='all')
	{
		sys::useLib('main::news');
		
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT
				`id`,
				`img`,
				`image`
			FROM
				`news`
		");
		
		while($obj=$_db->fetchAssoc($r))
		{	
			if(is_file($img_dir.'news/'.$obj['image']))
			{
				copy($img_dir.'news/'.$obj['image'],$_cfg['main::news_dir'].$obj['image']);
				main_news::resizeArticleImage($obj['image']);
			}
			if(is_file($img_dir.'news/'.$obj['img']))
				copy($img_dir.'news/'.$obj['img'],$_cfg['main::news_dir'].$obj['img']);
			
		}
		
	}

	
	//Фильмы
	if($_GET['mode']=='films' || $_GET['mode']=='all')
	{
		sys::useLib('main::films');
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT 
				`id`
			FROM
				`film` 
		");
		
		while ($obj=$_db->fetchAssoc($r)) 
		{
			
			//Фотки
			$_db->connect($db_host, $db_user, $db_pass, $db_name);
			$r2=$_db->query("
				SELECT
					`img`
				FROM
					`film_picture`
				WHERE
					`fid`=".intval($obj['id'])."
			");
			
			while ($photo=$_db->fetchAssoc($r2))
			{
				if(is_file($img_dir.'film/'.$photo['img']))
				{
					copy($img_dir.'film/'.$photo['img'],$_cfg['main::films_dir'].$photo['img']);
					main_films::resizePhotoImage($photo['img']);
				}
			}
			
			//Постеры
			$_db->connect($db_host, $db_user, $db_pass, $db_name);
			$r2=$_db->query("
				SELECT 
					`img`,
					`img_s`
				FROM
					`poster`
				WHERE
					`fid`=".intval($obj['id'])."
			");
			
			while ($poster=$_db->fetchAssoc($r2))
			{
				if(!$poster['img'])
					$poster['img']=$poster['img_s'];
					
				if(is_file($img_dir.'poster/'.$poster['img']))
				{
					copy($img_dir.'poster/'.$poster['img'],$_cfg['main::films_dir'].'poster_'.$poster['img']);
					main_films::resizePosterImage('poster_'.$poster['img']);
				}
			}
		}
	}
	
	//Рецензии
	if($_GET['mode']=='reviews' || $_GET['mode']=='all')
	{
		sys::useLib('main::reviews');
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT 
				`img`
			FROM `review`
		");
		while ($obj=$_db->fetchAssoc($r))
		{	
			if(is_file($img_dir.'review/'.$obj['img']))
			{
				copy($img_dir.'review/'.$obj['img'],$_cfg['main::reviews_dir'].$obj['img']);
				main_reviews::resizeReviewImage($obj['img']);
			}
			
		}
	}
	
	//Кинотеатры
	if($_GET['mode']=='cinemas' || $_GET['mode']=='all')
	{
		sys::useLib('main::cinemas');
		$_db->connect($db_host, $db_user, $db_pass, $db_name);
		$r=$_db->query("
			SELECT 
				`id`,
				`map_src`
			FROM
				`kinoteatr`
		");
		while ($obj=$_db->fetchAssoc($r)) 
		{
			if(is_file($img_dir.'mapa/'.$obj['map_src']))
				copy($img_dir.'mapa/'.$obj['map_src'],$_cfg['main::cinemas_dir'].'map_'.$obj['map_src']);
				
			$_db->connect($db_host, $db_user, $db_pass, $db_name);
			$r2=$_db->query("
				SELECT 
					`img_src`
				FROM
					`kinoteatr_picture`
				WHERE
					`kid`=".intval($obj['id'])."
			");

			while($picture=$_db->fetchAssoc($r2))
			{
				if(is_file($img_dir.'cinema/'.$picture['img_src']))
				{
					copy($img_dir.'cinema/'.$picture['img_src'],$_cfg['main::cinemas_dir'].$picture['img_src']);
					main_cinemas::resizePhotoImage($picture['img_src']);
				}
			}
			
			$_db->connect($db_host, $db_user, $db_pass, $db_name);
			$r2=$_db->query("
				SELECT 
					`scheme`
				FROM
					`hall`
					
				WHERE `kid`=".intval($obj['id'])."
					
			");
			
			while($hall=$_db->fetchAssoc($r2))
			{	
				if(is_file($img_dir.'scheme/'.$hall['scheme']))
					copy($img_dir.'scheme/'.$hall['scheme'],$_cfg['main::cinemas_dir'].$hall['scheme']);
			}
		}
	}
	
	//Перосоны
	if($_GET['mode']=='persons' || $_GET['mode']=='all')
	{
		sys::useLib('main::persons');
		$_db->connect($db_host, $db_user, $db_pass, $db_name2);
		$r=$_db->query("
			SELECT 
				`photo`
			FROM
				`kino_site_persons`
		");
		
		
		while ($obj=$_db->fetchAssoc($r))
		{
			
			if(is_file($img_dir.'persons/'.$obj['photo']))
			{
				copy($img_dir.'persons/'.$obj['photo'],$_cfg['main::persons_dir'].$obj['photo']);
				main_persons::resizePhotoImage($obj['photo']);
			}
			
		}
	}
	
	$_db->connect();
	
	
	return $result;
	
}
?>