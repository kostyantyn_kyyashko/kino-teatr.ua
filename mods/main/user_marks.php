<?
function main_user_marks()
{
	return 404; // 26.11.2015 рекомендовано удалить этот файл и его шаблон
	
	sys::useLib('main::users');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('user_id','int');
	sys::filterGet('order_by','text','mark.desc');
	
	if(!$_GET['user_id'])
		return 404;
		
	$result['user']=$_db->getValue('sys_users','login',intval($_GET['user_id']));
	$meta['user']=$result['user'];	
	$result['meta']=sys::parseModTpl('main::user_marks','page',$meta);
	if(!$result['user'])
		return 404;
	
	//Определение направления сортировки
	$order_by=explode('.',$_GET['order_by']);
	$order_fields=array('film','mark');
	if($order_by[1]=='asc')
	{
		$result['order']='asc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.desc');
		}
	
	}
	else
	{ 
		$result['order']='desc';
		foreach ($order_fields as $field)
		{
			$result[$field]['order_url']=sys::rewriteUrl(sys::cutGetParams('order_by').'&order_by='.$field.'.asc');
		}
	}
	$result['order_field']=$order_by[0];

	$order_by=sys::parseOrderBy($_GET['order_by']);
	//=====================================================
	
	//Формирование запроса-------------------------------------
	$q="
		SELECT 
			rat.film_id,
			flm_lng.title AS `film`, 
			rat.mark
		FROM `#__main_films_rating` AS `rat`
		LEFT JOIN `#__main_films_lng` AS `flm_lng`
		ON flm_lng.record_id=rat.film_id
		AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";
	
	$q.="
		WHERE rat.user_id=".intval($_GET['user_id'])."
		AND rat.mark>0
	";
	
	$q.=$order_by;
	//====================================================================
	
	$result['pages']=sys_pages::pocess($q,$_cfg['main::rating_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['url']=main_films::getFilmUrl($obj['film_id']);
		$result['objects'][]=$obj;
	}
		
	return $result;
}
?>