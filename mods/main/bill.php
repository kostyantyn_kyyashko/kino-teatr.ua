<?
function main_bill()
{
	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('main::countries');
	sys::useLib('sys::form');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('date','text',date('d.m.Y'));
	sys::filterGet('order','text','cinemas');
	sys::filterGet('genre','int');
	sys::filterGet('minTime','text');
	sys::filterGet('city','text');
	sys::filterGet('city_id','int',$_cfg['main::city_id']);
	
	sys::jsInclude("main::eventmap");

	if(sys::isDebugIP()) sys::printR($_GET);
	if(sys::isDebugIP()) sys::printR($_POST);
	
	if ($_GET['city'])
	{
		$q="SELECT * FROM `#__main_countries_cities`
		WHERE translit='".$_GET['city']."'
		";
		$r=$_db->query($q);
		if($obj=$_db->fetchAssoc($r))
			$_GET['city_id'] = $obj['id'];
	}

	if(!$_GET['city_id']) $_GET['city_id']=1; // киев

	if (!$_GET['city'])
	{
		$q="SELECT translit FROM `#__main_countries_cities`
		WHERE id='".$_GET['city_id']."'
		";
		$r=$_db->query($q);
		if($obj=$_db->fetchAssoc($r))
			$_GET['city'] = $obj['translit'];
	}

	if(!$_REQUEST['minTime'] && $_GET['minTime'])
	{
		$_REQUEST['minTime'] = $_GET['minTime'];
	}

	if(!$_REQUEST['maxTime'] && $_GET['maxTime'])
	{
		$_REQUEST['maxTime'] = $_GET['maxTime'];
	}


	if ($_GET['city_id'] && $_GET['city_id']!='1')
	{
		$can = '?city_id='.$_GET['city_id'];
	}

		$lang=$_cfg['sys::lang'];
	
		if ($lang=='ru')
		{
//		define('_CANONIC',_ROOT_URL.'bill.phtml',true);
		define('_CANONIC',_ROOT_URL.(($_GET['order']=="films")?"afisha-kino-":"kinoafisha-").$_GET['city'].'.phtml',true);
		} else {
//		define('_CANONIC',_ROOT_URL.$lang.'/bill.phtml',true);
		define('_CANONIC',_ROOT_URL.$lang.(($_GET['order']=="films")?"/afisha-kino-":"/kinoafisha-").$_GET['city'].'.phtml',true);
		}

	if(isset($_POST['_task']) && $_POST['_task'])
	{
		if($_POST['_task']=='user_cinema_subscribe')
		$id_cinema = isset($_POST["id_cinema"])?intval($_POST["id_cinema"]):0;
		$act = isset($_POST["act"])?intval($_POST["act"]):0;		
		
		if($act==0)
			main_cinemas::unsubscribeCinema($_user["id"], $id_cinema);
		else	
			main_cinemas::subscribeCinema($_user["id"], $id_cinema);
		sys::redirect(_CANONIC, false);	
	}
		

	//$cache_name='main_billboard_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	//if($cache=sys::getCache($cache_name,$_cfg['main::billboard_cache_period']*_HOUR))
	//return unserialize($cache);


	if ($_REQUEST['date'])
		$_GET['date'] = $_REQUEST['date'];


	$meta['city']=$result['city']=main_countries::getCityTitle($_GET['city_id'],'what');

	$mmonth=sys::translate('main::month_'.(strftime('%m',sys::date2Timestamp($_GET['date'],'%d.%m.%Y'))+0).'_a');
    $nyear = strftime('%Y',sys::date2Timestamp($_GET['date'],'%d.%m.%Y'));

	$meta['date']=$result['date']=strftime('%d',sys::date2Timestamp($_GET['date'],'%d.%m.%Y')).' '.$mmonth.' '.$nyear;
    //echo  $meta['date'];

	if ($_GET['order']!='films')
	{
	$result['meta']=sys::parseModTpl('main::billboard','page',$meta);
	} else {
	$result['meta']=sys::parseModTpl('main::billfilms','page',$meta);
	}
	$result['city_id']=$_REQUEST['city_id'];
	if(!$result['city'])
		return 404;

		if (!$_REQUEST['minCost'])
			$_REQUEST['minCost'] = 0;

		if (!$_REQUEST['maxCost'])
			$_REQUEST['maxCost'] = 500;


		if (!$_REQUEST['minTime'])
			$_REQUEST['minTime'] = '0:00';

		if (!$_REQUEST['maxTime'])
			$_REQUEST['maxTime'] = '23:59';

	$result['bill_url']='?mod=main&act=bill&date='.$_GET['date'].'&city_id='.$_GET['city_id'];
	$result['print_url']=sys::rewriteUrl('?mod=main&act=bill_print&date='.$_GET['date'].'&order='.$_GET['order'].'&city_id='.$_GET['city_id']);
	$result['objects']=main_shows::getBillBoard($_GET['city_id'],$_GET['date'], $_GET['order'], $_REQUEST['minCost'], $_REQUEST['maxCost'], $_REQUEST['minTime'], $_REQUEST['maxTime'], $_GET['genre']);

	if ($_GET['order']=='films')
	{
		if (sys::translate('main::bill_films_'.$_GET['city_id'])!='' && sys::translate('main::bill_films_'.$_GET['city_id'])!='main::bill_films_'.$_GET['city_id'])
		{
			$result['seo'] =  sys::translate('main::bill_films_'.$_GET['city_id']);
		}
	} else {
		if (sys::translate('main::bill_cinemas_'.$_GET['city_id'])!='' && sys::translate('main::bill_cinemas_'.$_GET['city_id'])!='main::bill_cinemas_'.$_GET['city_id'])
		{
			$result['seo'] =  sys::translate('main::bill_cinemas_'.$_GET['city_id']);
		}
	}

	$result['mincost'] = $_REQUEST['minCost'];
	$result['maxcost'] = $_REQUEST['maxCost'];

	$mintimearr = explode(':',$_REQUEST['minTime']);

	$mintimemin = $mintimearr[0]*60+$mintimearr[1];

	$maxtimearr = explode(':',$_REQUEST['maxTime']);

	$maxtimemin = $maxtimearr[0]*60+$maxtimearr[1];

	$result['mintime'] = $mintimemin;
	$result['maxtime'] = $maxtimemin;
	$result['chage'] = 1;

	//sys::setCache($cache_name,serialize($result));

	return $result;
}
?>