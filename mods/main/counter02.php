<?
function main_counter02()
{
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('film_id','int');
	
	$uri=explode('_',$_SERVER['REQUEST_URI']);
	$_GET['film_id']=$uri[1];
	
	if(!$_GET['film_id'])
		return 404;
		
	$r=$_db->query("
		SELECT 
			`rating`, 
			`pre_rating`,
			`pre_votes`,
			`votes`,
			`pre_sum`,
			`sum`
		FROM 
			`#__main_films`
		WHERE 
			`id`='".intval($_GET['film_id'])."'
	");
	
	$film=$_db->fetchAssoc($r);
	
	
	if(!$film)
		return 404;

	if($film['votes']>5) // только при наличии 5 голосов после премьеры показывать реальный рейтинг	
		$film['rating']=$film['sum']/$film['votes'];
	else // до 5 голосов показывать предварительный усредненный рейтинг
		$film['rating']=($film['pre_sum']+$film['sum'])/($film['pre_votes']+$film['votes']);
	
	$votes=$film['votes']+$film['pre_votes'];
	
	if($film['rating']==0) $film['rating'] = "0 ";
	else $film['rating']= number_format($film['rating'], 1, '.', '');
	
	
	for($i=0.25; $i<11; $i=$i+0.25)
	{
		if($film['rating']<$i)
		{
			$stars='stars_'.($i-0.25).'.gif';
			break;
		}
	}
	
	$stars=imagecreatefromgif(_IMG_DIR.'counter01/'.$stars);
	$img=imagecreatefromgif(_IMG_DIR.'counter01/cnt.gif');
	
	$font=_SECURE_DIR.'fonts/TechnoNormal.ttf';	
	$color=imagecolorallocate($img,171,51,50);
	$fontSize=13; 
	$box=imagettfbbox($fontSize,0,$font,$film['rating']);
	$x=86-$box[4];
	imagettftext($img,$fontSize,0,$x,14,$color,$font,$film['rating']);
	
	$text=' '.$votes.' ';
	$color=imagecolorallocate($img,44,52,68);
	$fontSize=8; 
	$box=imagettfbbox($fontSize,0,$font,$text);
	$x=90-$box[4];
	imagettftext($img,$fontSize,0,$x,26,$color,$font,$text);
	
	
	imagecopy($img,$stars,0,26,0,0,108,13);
	header('Content-type: image/gif');
	imagegif($img);
	imagedestroy($img);
	sys::setTpl();
}
?>