<?
function main_film_awards()
{
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');

	if(!$_GET['film_id'])
		return 404;

	define('_CANONICAL',main_films::getFilmAwardsUrl($_GET['film_id']),true);

	$result['title']=$result['film']=$_db->getValue('main_films','title',intval($_GET['film_id']),true);
	$result['serial']=$_db->getValue('main_films','serial',intval($_GET['film_id']),false);
	$meta['film']=$result['film'];
	$result['meta']=sys::parseModTpl('main::film_awards','page',$meta);

	if(!$result['film'])
		return 404;

	main::countShow($_GET['film_id'],'film');

	$q = "
		SELECT
			flm_awd.id,
			flm_awd.year,
			awd_lng.title as nomination,
			awd_cat_lng.title AS `title`,
			flm_awd.result
		FROM `#__main_films_awards` AS `flm_awd`

		LEFT JOIN `#__main_awards_categories` AS `awd_cat`
		ON awd_cat.id=flm_awd.award_id

		LEFT JOIN `#__main_awards_categories_lng` AS `awd_cat_lng`
		ON awd_cat_lng.record_id=flm_awd.award_id
		AND awd_cat_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN `#__main_awards_lng` AS `awd_lng`
		ON awd_lng.record_id=awd_cat.award_id
		AND awd_lng.lang_id=".$_cfg['sys::lang_id']."

		WHERE flm_awd.film_id=".intval($_GET['film_id'])."
		ORDER BY 
        	flm_awd.year DESC, 
            awd_lng.title,
            awd_cat_lng.title,
			flm_awd.order_number
	";
	$r=$_db->query($q);
//if(sys::isDebugIP()) $_db->printR($q);
	$result['objects']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['persons']=main_films::getAwardPersons($obj['id']);
		$result['objects'][]=$obj;
	}
	if(!$result['objects'])
		return 404;

	return $result;
}
?>