<?
function main_game()
{
	global $_db, $_cfg;

	//if(!sys::isDebugIP()) return 404;
	
	$partner_id = 216138;	
	
	switch (intval($_GET["game"])) {
		case 1: $result['src'] = "http://empire.goodgamestudios.com/?w=$partner_id"; 
				$meta['name']="Goodgame Empire: Four Kingdoms";
				break;
		case 2: $result['src'] = "http://media.goodgamestudios.com/games/empire/?w=$partner_id"; 
				$meta['name']="Goodgame Empire";
				break;
		case 3: $result['src'] = "http://media.goodgamestudios.com/games/ranch/?w=$partner_id"; 
				$meta['name']="Goodgame Big Farm";
				break;
		case 4: $result['src'] = "http://media.goodgamestudios.com/games/poker/?w=$partner_id"; 
				$meta['name']="Goodgame Poker";
				break;
		default: return 404;		
	}
	
	$meta['title']=$meta['name'];
	$result['meta']=sys::parseModTpl('main::game','page',$meta);
	return $result;
}
?>