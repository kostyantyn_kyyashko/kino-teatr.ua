<?
function main_cinema()
{
	sys::useLib('main::cinemas');
	sys::useLib('main::countries');
	sys::useLib('main::users');
	sys::useLib('main::discuss');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('cinema_id','int');


	if ($_GET['page'])
	{
		define('_CANONIC', main_cinemas::getCinemaUrl($_GET['cinema_id']));
	}


	if(!$_GET['cinema_id'])
		return 404;

	if(isset($_POST['_task']) && $_POST['_task'])
	{
		if($_POST['_task']=='user_cinema_subscribe')
		{
			$id_cinema = isset($_POST["id_cinema"])?intval($_POST["id_cinema"]):0;
			$act = isset($_POST["act"])?intval($_POST["act"]):0;		
			
			if($act==0)
				main_cinemas::unsubscribeCinema($_user["id"], $id_cinema);
			else	
				main_cinemas::subscribeCinema($_user["id"], $id_cinema);
			sys::redirect(main_cinemas::getCinemaUrl($id_cinema), false);	
		}
	}	
		
	if($_REQUEST['editedtext'] && sys::checkAccess('main::cinemas'))
	{

		$r=$_db->query("
			UPDATE
				`#__main_cinemas_lng`
			SET
				text = '".$_REQUEST['editedtext']."'

			WHERE
				record_id=".intval($_GET['cinema_id'])."
			AND
				lang_id = ".$_cfg['sys::lang_id']."
		");

	}



	$result['cinema']=$_db->getValue('main_cinemas','title',intval($_GET['cinema_id']),true);
	$r=$_db->query("
		SELECT
			cnm.user_id,
			cnm.ticket_url,
			cnm_lng.title,
			cnm_lng.address,
			cnm_lng.seosm,
			cnm_lng.seofull,
			cnm.map,
			cnm.city_id,
			cit_lng.title AS `city`,
			cnm.site,
			cnm.latitude,
			cnm.longitude,
			cnm.phone,
			cnm.banner,
			cnm.mapia_code,
			cnm_lng.text

		FROM
			`#__main_cinemas` AS `cnm`

		LEFT JOIN
			`#__main_cinemas_lng` AS `cnm_lng`
		ON
			cnm_lng.record_id=cnm.id
		AND
			cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN
			`#__main_countries_cities_lng` AS `cit_lng`
		ON
			cit_lng.record_id=cnm.city_id
		AND
			cit_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		WHERE
			cnm.id=".intval($_GET['cinema_id'])."
	");
	$result=$_db->fetchAssoc($r);

	/*if($result['ticket_url'] && $_user['id']==2)
		$result['ticket_url']=sys::rewriteUrl('?mod=main&act=register');*/

	if(!$result)
		return 404;

	$meta['cinema']=$result['title'];
	$meta['city'] = $result['city'];

	if ($_GET['page'])
	{
		$meta['page'] = '. '.sys::translate('main::page').' №'.$_GET['page'];
	} else {
		$meta['page'] = '';
	}
	$result['meta']=sys::parseModTpl('main::cinema','page',$meta);
	if($result['map'])
		$result['map']=$_cfg['main::cinemas_url'].$result['map'];

	if($result['mapia_code'])
	{
		$code['city']=$result['city'];
		$code['marker_title']=sys::translate('main::cinema').' '.htmlspecialchars($result['title']).'';
		$code['size']='495x495';
		parse_str($result['mapia_code'],$mapia_code);
		$mapia_code=array_merge($mapia_code,$code);
		$params='';
		foreach ($mapia_code as $key=>$val)
		{
			$params.=$key.'='.urlencode($val).'&';
		}
		$params=sys::cutStrRight($params,1);
		$result['map']='http://mapia.com.ua/static?'.$params;
	}

	if($result['user_id']==$_user['id'] || sys::checkAccess('main::cinemas_edit'))
		$result['edit']=true;
	else
		$result['edit']=false;
		
// flora --------------------------------------
 if (intval($_GET['cinema_id'])==8)
 {
  $result['flora'] = 1;
 }
// end flora ----------------------------------

	//koments --------------------------------------------
	$q="
		SELECT
			msg.id,
			msg.date,
			msg.user_id,
			msg.user_name,
			msg.rating,
			usr.login AS `user`,
			msg.text
		FROM `#__main_discuss_messages` AS `msg`

		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=msg.user_id

		WHERE msg.object_id='".intval($_GET['cinema_id'])."'
		AND msg.object_type='cinema'

		ORDER BY msg.date DESC

		LIMIT 0,5
	";

	$r=$_db->query($q);
	$result['comments']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true);
		if($obj['user_id']==2)
		{
			$obj['user']=$obj['user_name'];
			$obj['user_url']=false;
		}
		else
			$obj['user_url']=main_users::getUserUrl($obj['user_id']);
		$obj['bb_text']=sys::parseBBCode($obj['text']);
		$result['comments'][]=$obj;
	}

	if (sys::translate('main::cinema_'.$_GET['cinema_id'])!='' && sys::translate('main::cinema_'.$_GET['cinema_id'])!='main::cinema_'.$_GET['cinema_id'])
	{
		$result['seo'] =  sys::translate('main::cinema_'.$_GET['cinema_id']);
	}

	return $result;
}
?>