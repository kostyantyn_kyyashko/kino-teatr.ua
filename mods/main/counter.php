<?
function main_counter()
{
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('film_id','int');
	
	$uri=explode('_',$_SERVER['REQUEST_URI']);
	$_GET['film_id']=$uri[1];
	
	if(!$_GET['film_id'])
		return 404;
		
	$r=$_db->query("
		SELECT 
			`rating`, 
			`pre_rating`,
			`pre_votes`,
			`votes`,
			`pre_sum`,
			`sum`
		FROM 
			`#__main_films`
		WHERE 
			`id`='".intval($_GET['film_id'])."'
	");
	
	$film=$_db->fetchAssoc($r);
	
	
	if(!$film)
		return 404;

//	$film['votes']+=$film['pre_votes'];
//	$film['rating']=($film['pre_sum']+$film['sum'])/$film['votes'];

	if($film['votes']>5) // только при наличии 5 голосов после премьеры показывать реальный рейтинг	
		$film['rating']=$film['sum']/$film['votes'];
	else // до 5 голосов показывать предварительный усредненный рейтинг
		$film['rating']=($film['pre_sum']+$film['sum'])/($film['pre_votes']+$film['votes']);
	
	
	if($film['rating']==0) $film['rating'] = "0 ";
	else $film['rating']= number_format($film['rating'], 1, '.', '');
	
	$img=imagecreatefromgif(_IMG_DIR.'counter/rating.gif');
	
	$font=_SECURE_DIR.'fonts/tahomabd.ttf';	
	
	$color=imagecolorallocate($img,44,52,68);
	$fontSize=11; 
	$text = $film['rating'];
	$box=imagettfbbox($fontSize,0,$font,$text);

	$x=108-$box[2];
	$y=21;
	imagettftext($img,$fontSize,0,$x,$y,$color,$font,$text);
	
	header('Content-type: image/gif');
	imagegif($img);
	imagedestroy($img);
	sys::setTpl();
}
?>
