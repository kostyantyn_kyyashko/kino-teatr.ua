<?
function main_imdb_parse()
{
	sys::useLib('main::films');
	sys::useLib('sys::form');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	$q="
		SELECT
			max(id) as max
		FROM grifix_main_films
	";


	$r=$_db->query($q);
	while($obj=$_db->fetchAssoc($r))
	{		$maxid = $obj['max'];
	}


	$q="
		SELECT
			*
		FROM grifix_main_films_imdb
		WHERE id >= 45000
		ORDER BY id
	";


	$r=$_db->query($q);

	$i = 0;

	while($obj=$_db->fetchAssoc($r))
	{
		$i++;

			$obj['name'] = preg_replace('/&#x([0-9a-f]{2});/e', 'chr(hexdec($1))', $obj['name']);
			$obj['ru_name'] = preg_replace('/&#x([0-9a-f]{2});/e', 'chr(hexdec($1))', $obj['ru_name']);
			$obj['ru_summary'] = preg_replace('/&#x([0-9a-f]{2});/e', 'chr(hexdec($1))', $obj['ru_summary']);

			$country = explode(',',$obj['country']);
			$genres = explode(',',$obj['genres']);

		$q1="
		SELECT
			id
		FROM grifix_main_films
		WHERE (name = '".$obj['name']."' OR title_orig = '".$obj['name']."') AND year = '".$obj['year']."'
		";


		$id = '';

		$r1=$_db->query($q1);
		while($obj1=$_db->fetchAssoc($r1))
		{
			$id = $obj1['id'];
		}

		if ($id)
		{			$r2=$_db->query('
				INSERT INTO `#__main_films_imdb_to_kt`
					(`imdb_id`, `kt_id`)
				VALUES
					("'.$obj['id'].'", "'.$id.'")
			');
		} else {			$maxid++;



			$r2=$_db->query('
				INSERT INTO `#__main_films`
					(`id`, `name`, `title_orig`, `world_premiere`, `year`)
				VALUES
					("'.$maxid.'", "'.$obj['name'].'", "'.$obj['name'].'", "'.$obj['world_premiere'].'", "'.$obj['year'].'")
			');






			foreach ($country as $key=>$value)
			{				if ($value>0)
				{
					$r2=$_db->query('
						INSERT INTO `#__main_films_countries`
							(`film_id`, `country_id`)
						VALUES
							("'.$maxid.'", "'.$value.'")
					');
				}
			}

			foreach ($genres as $key=>$value)
			{
				if ($value>0)
				{
					$r2=$_db->query('
						INSERT INTO `#__main_films_genres`
							(`film_id`, `genre_id`)
						VALUES
							("'.$maxid.'", "'.$value.'")
					');
				}
			}


			$r2=$_db->query('
				INSERT INTO `#__main_films_lng`
					(`record_id`, `lang_id`, `title`, `intro`)
				VALUES
					("'.$maxid.'", "1", "'.$obj['ru_name'].'", "'.$obj['ru_summary'].'"),
					("'.$maxid.'", "3", "'.$obj['ru_name'].'", "'.$obj['ru_summary'].'")
			');
			$r2=$_db->query('
				INSERT INTO `#__main_films_imdb_to_kt`
					(`imdb_id`, `kt_id`)
				VALUES
					("'.$obj['id'].'", "'.$maxid.'")
			');

		}

	}






	exit;
}
?>