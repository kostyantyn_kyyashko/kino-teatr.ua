<?
function main_user_profile_socials()
{
	define('_NOINDEX','1');
	sys::useLib('main::users');

	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;

	if($_user['id']==2) return 401;

	$result['meta']=sys::parseModTpl('main::user_profile_socials','page');

	$q="
		SELECT * FROM
			`#__sys_users_social_network_info`
		WHERE
			user_id=".intval($_user['id'])
	;

	$r=$_db->query($q);
		

	$result["socials"] = $_db->fetchAssoc($r);	
	
	if(empty($result["socials"])) 
		$result["socials"] = array(
			"facebook_id" => ""
		);
	
	//if(sys::isDebugIP()) $_db->printR($result);
	
	return $result;
}
?>