<?
function main_film_posters()
{
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');
	sys::filterGet('poster_id','int');
	sys::filterGet('page','int');

	if(!$_GET['film_id'] && !$_GET['poster_id'])
		return 404;

		$q="
			SELECT
				pst.film_id,
				pst.id AS `poster_id`,
				pst.image,
				pst.user_id,
				pst.shows,
				pst.order_number,
				pst.public,
				flm_lng.title AS `film`
			FROM `#__main_films_posters` AS `pst`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=pst.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		";

	if($_GET['poster_id'])
	{
		$q.="
			WHERE pst.id=".intval($_GET['poster_id'])."
		";
	}
	elseif($_GET['film_id'])
	{
		if($_GET['page'])
			$limit=intval($_cfg['main::images_on_page']*(intval($_GET['page'])-1));
		else
			$limit=0;
		$q.="
			WHERE pst.film_id='".intval($_GET['film_id'])."'
			AND pst.public=1
			ORDER BY pst.order_number ASC
			LIMIT ".$limit.",1
		";
	}

	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);

	if(!$result)
		return 404;

	if(!$result['public'] && !sys::checkAccess('main::films_posters_confirm'))
		return 404;

	main::countShow($result['film_id'],'film');

	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::films_posters_delete') || $result['user_id']==$_user['id'])
		{
			main_films::deletePoster($result['poster_id']);
			sys::redirect(main_films::getFilmPostersUrl($result['film_id']),false);
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::films_posters_confirm'))
	{
		main_films::confirmposter($result['poster_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}

	if($_cookie)
	{
		$_db->query("
			UPDATE `#__main_films_posters`
			SET `shows`=(`shows`+1)
			WHERE `id`=".doubleval($result['poster_id'])."
		");
	}
	$meta['film']=$result['film'];
	$result['image']=$_cfg['main::films_url'].'x4_'.$result['image'];
	$result['next_url']=false;
	$result['prev_url']=false;
	$result['delete_poster']=false;
	$result['edit_url']=false;
	$result['add_url']=false;
	$result['url']=main_films::getposterUrl($result['poster_id']);

	$result['meta']=sys::parseModTpl('main::film_posters','page',$meta);

	if($next_id=main_films::getNextposterId($result['film_id'],$result['order_number']))
		$result['next_url']=main_films::getpostersUrl($next_id);
	if($prev_id=main_films::getPrevposterId($result['film_id'],$result['order_number']))
		$result['prev_url']=main_films::getpostersUrl($prev_id);

	if(sys::checkAccess('main::films_posters_edit') || $result['user_id']==$_user['id'])
		$result['edit_url']=main_films::getposterEditUrl($result['poster_id']);

	if(sys::checkAccess('main::films_posters_add'))
		$result['add_url']=main_films::getposterAddUrl($result['film_id']);

	if(sys::checkAccess('main::films_posters_delete') || $result['user_id']==$_user['id'])
		$result['delete_poster']=true;
	//=============================================================================

	//Получить инфу об остальных фотках-----------------------------------------------
	$q="
		SELECT `id`,`image`
		FROM `#__main_films_posters`
		WHERE `film_id`=".$result['film_id']."
		AND `public`=1
		ORDER BY `order_number` ASC
	";
	$result['pages']=sys_pages::pocess($q,$_cfg['main::images_on_page'],false,'?mod=main&act=film_posters&film_id='.$result['film_id'].'&page=%page%');
	$r=$_db->query($q.$result['pages']['limit']);
	$result['previews']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['image']=$_cfg['main::films_url'].'x2_'.$obj['image'];
		$obj['url']=main_films::getPostersUrl($obj['id'],false);
		if($_GET['page'])
			$obj['url'].='&page='.$_GET['page'];
		$obj['url']=sys::rewriteUrl($obj['url']);
		$result['previews'][]=$obj;
	}
	//============================================================

	return $result;
}
?>