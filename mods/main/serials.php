<?
function main_serials()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::serials');
	sys::useLib('main::spec_themes');
	sys::useLib('main::users');
	sys::useLib('sys::pages');

	sys::filterGet('month');
	sys::filterGet('year');
	sys::filterGet('article_id');

	if ($_GET['page']!='')
	{
		define('_NOINDEX',1,true);
	}


		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		define('_CANONICAL',_ROOT_URL.'serials.phtml',true);
		} else {
		define('_CANONICAL',_ROOT_URL.$lang.'/serials.phtml',true);
		}

	 $cache_name='main_serials_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	 if($cache=sys::getCache($cache_name,$_cfg['main::news_page_cache_period']*60))
	 return unserialize($cache);

	if($_GET['article_id'])
		$result['section']=$meta['section']=$_db->getValue('main_serials','title',intval($_GET['article_id']),true);
	else
		$result['section']=$meta['section']=sys::translate('main::all_sections');

	if(!$result['section'])
		return 404;

	if(isset($_POST['_task']) && $_POST['_task'])
	{
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete' && sys::checkAccess('main::serials_delete'))
			main_serials::deleteArticle($task[1]);
	}

	$result['date']=$meta['date']=mb_strtolower(sys::translate('main::month_'.intval($_GET['month']))).' '.$_GET['year'];
	if ($_GET['page'])
	{
		$meta['page'] = sys::translate('main::page').' '.$_GET['page'];
	} else {
		$meta['page'] = '';
	}

	$result['meta']=sys::parseModTpl('main::serials','page',$meta);
	$result['title']=sys::translate('main::serials_cinema');
	$month_begin=date('Y-m-d H:i:s',main_serials::getMonthBegin($_GET['month'],$_GET['year']));
	$month_end=date('Y-m-d H:i:s',main_serials::getMonthEnd($_GET['month'],$_GET['year']));
	$q="
		SELECT
			art.id,
			art.date,
			art.total_shows,
			art.comments,
			art.user_id,
			art.exclusive,
			spec.spec_theme_id as spec_theme,
			art.small_image AS `image`,
			art.image AS `big_image`,
			art_lng.title,
			art_lng.intro,
			usr.login AS `user`,
			usr_data.main_nick_name
		FROM `#__main_serials_articles` AS `art`

		LEFT JOIN
			`#__main_serials_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_serials` AS `nws`
		ON art.articles_id=nws.id

		LEFT JOIN `#__main_serials_articles_spec_themes` AS `spec`
		ON art.id=spec.article_id
		
		LEFT JOIN
			`#__sys_users` AS `usr`
		ON
			usr.id=art.user_id

		LEFT JOIN
			`#__sys_users_data` AS `usr_data`
		ON
			usr.id=usr_data.user_id
				
		WHERE
			/*art.date BETWEEN '".$month_begin."' AND '".$month_end."'
			AND */(art.city_id=".intval($_cfg['main::city_id'])." OR art.city_id=0)
			AND art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND nws.showtab=1
	";
	if($_GET['article_id'])
	{
		$q.="
			AND art.serials_id=".intval($_GET['article_id'])."
		";
	}

	if($_REQUEST['tag'] && $_cfg['sys::lang_id']=='1')
	{
		$q.="
			AND (
				art.tags LIKE '%".str_replace("'","",$_REQUEST['tag'])."%'
			OR
				art.tags LIKE '%".str_replace("'","",$_REQUEST['tag'])."'
			OR
				art.tags LIKE '".str_replace("'","",$_REQUEST['tag'])."%'
			)
		";

	$result['tag']=1;
	}

	if($_REQUEST['tag'] && $_cfg['sys::lang_id']=='3') {
		$q.="
			AND (
				art.tags_ua LIKE '%".str_replace("'","",$_REQUEST['tag'])."%'
			OR
				art.tags_ua LIKE '%".str_replace("'","",$_REQUEST['tag'])."'
			OR
				art.tags_ua LIKE '".str_replace("'","",$_REQUEST['tag'])."%'
			)
		";

	$result['tag']=1;
	}

	$q.="
		ORDER BY art.date DESC
	";
//if($_SERVER['REMOTE_ADDR'] == "37.113.6.235") mail("happyinvestor@mail.ru", 3, "$q;");

	$result['pages']=sys_pages::pocess($q,$_cfg['main::news_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$image = '';
		$obj['date']=sys::russianDate($obj['date']);
		$obj['shows']=$obj['total_shows'];
		$obj['user_url']=main_users::getUserUrl($obj['user_id']);

// inserted by Mike begin
  	    $image="x1_".$obj['id'].".jpg";
		if(!file_exists('./public/main/serials/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::serials_url'].$image;
		$obj['image'] = $image;
// inserted by Mike end
		
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_serials::getArticleUrl($obj['id']);
		$obj['spec_theme_url']=main_spec_themes::getArticleUrl($obj['spec_theme']);

// comment by Mike		
/*
		if(sys::checkAccess('main::serials_edit') || $_user['id']==$obj['user_id'])
			$obj['edit_url']=main_serials::getArticleEditUrl($obj['id']);
		else
			$obj['edit_url']=false;

		if(sys::checkAccess('main::serials_delete') || $_user['id']==$obj['user_id'])
			$obj['delete']=true;
		else
			$obj['delete']=false;
*/			
		$result['objects'][]=$obj;
	}
	
	if(!$result['objects']/* && !main_news::newsReirect($_GET['year'],$_GET['month'],$_GET['news_id'])*/)
		return 404;

// comment by Mike		
/*
	if(sys::checkAccess('main::serials'))
		$result['add_url']=main_serials::getArticleAddUrl();
	else
		$result['add_url']=false;
*/

		$result['all_serials']=sys::rewriteUrl('?mod=main&act=serials');
		$result['popular_serials']=sys::rewriteUrl('?mod=main&act=popular_serials');
		$result['discussed_serials']=sys::rewriteUrl('?mod=main&act=discussed_serials');

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>