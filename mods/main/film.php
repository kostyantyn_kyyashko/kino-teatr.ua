<?
function main_film()
{

function cut_word($text, $counttext = 50, $sep = ' ') {
    $words = split($sep, $text);
    if ( count($words) > $counttext )
        $text = join($sep, array_slice($words, 0, $counttext));
    return $text;
}

	sys::useLib('main::films');
	sys::useLib('main::reviews');
	sys::useLib('main::users');
	sys::useLib('main::discuss');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('film_id','int');
	sys::filterGet('city_id','int',$_cfg['main::city_id']);

	define('_CANONIC',main_films::getFilmUrl($_GET['film_id']),true);
	define('_ALTERNATE', 'href="'.main_films::getFilmUrl($_GET['film_id'], (($_cfg["sys::lang"]==="ru")?"uk":"ru")).'" hreflang="'.(($_cfg["sys::lang"]==="ru")?"uk":"ru").'"');	
	define('_AUTH','1',true);
	
	if(!$_GET['film_id'])
			return 404;

	if($_REQUEST['editedtext'] && sys::checkAccess('main::films'))
	{

		$r=$_db->query("
			UPDATE
				`#__main_films_lng`
			SET
				intro = '".$_REQUEST['editedtext']."'

			WHERE
				record_id=".intval($_GET['film_id'])."
			AND
				lang_id = ".$_cfg['sys::lang_id']."
		");

	}
	$r=$_db->query("
		SELECT
			flm.title_orig,
			flm.title_alt,
			flm_lng.title,
			flm_lng.seosm,
			flm_lng.seofull,
			flm.year,
			flm.id,
			flm.budget,
			flm.budget_currency,
			flm.duration,
			flm.ukraine_premiere,
			flm.world_premiere,
			flm.yakaboo_url,
			flm.age_limit,
			flm_lng.intro,
			flm_lng.text,
			flm.pre_rating,
			flm.pre_votes,
			flm.rating,
			flm.votes,
			flm.pro_rating,
			flm.pro_votes,
			flm.pre_sum,
			flm.sum,
			DATE(flm.date) as dateCreated,
			flm.comments AS `comments_num`,
			flm.banner,
			trk.film_id AS `tracking`,
			flm.3d as film_3d,
			flm.serial as serial,
			dstr.id as distributor_id, 
			dstr.name as distributor,
			imdb.rating as imdb_rating,
			imdb.votes as imdb_votes
		FROM `#__main_films` AS `flm`

		LEFT JOIN `#__main_films_lng` AS `flm_lng`
		ON flm_lng.record_id=flm.id
		AND flm_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN `#__main_users_films_subscribe` AS `trk`
		ON trk.film_id=".intval($_GET['film_id'])."
		AND trk.user_id=".intval($_user['id'])."
		
		LEFT JOIN `grifix_main_films_distributors` AS `dstr_lnk` ON flm.id = dstr_lnk.film_id 
		LEFT JOIN `grifix_main_distributors` AS `dstr` ON dstr_lnk.distributor_id = dstr.id   

		LEFT JOIN `#__main_films_imdb_to_kt` AS `imdb_kt`
		ON imdb_kt.kt_id=flm.id		
		
		LEFT JOIN `#__main_films_imdb` AS `imdb`
		ON imdb_kt.imdb_id=imdb.imdb_id				
		
		WHERE flm.id=".intval($_GET['film_id'])."
	");
	
	$result=$_db->fetchAssoc($r);
	
	$r=$_db->query("
		SELECT 
			total 
		FROM `#__main_box_positions` 
		WHERE film_id=".intval($_GET['film_id'])."
		ORDER BY box_id DESC 
		LIMIT 1
	");	
		
	if(!$result) return 404; 

	$box_office_total = $_db->fetchAssoc($r);
	$result["box_office_total"] = $box_office_total?number_format(intval($box_office_total["total"]),0, ",", " "):0;
	
	main::countShow($_GET['film_id'],'film');

	$meta['film']=$result['title'];
	$meta['year']=$result['year'];
	$meta['page']=$_GET['page'];

	if (!$_GET['page'])
	{
		$result['meta']=sys::parseModTpl('main::film','page',$meta);
	} else {
		$result['meta']=sys::parseModTpl('main::film_page','page',$meta);
	}

	$result['title']=htmlspecialchars($result['title']);

	$titles = array();

	$result['title_orig']=htmlspecialchars($result['title_orig']);
	$result['title_alt']=htmlspecialchars($result['title_alt']);

	$poster=main_films::getFilmFirstPoster($_GET['film_id']);

		if($poster)
		{
// inserted by Mike begin
  	    $image="x2_".$poster['image']; 
		if(!file_exists('./public/main/films/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::films_url'].$image;
		$poster['image'] = $image;
// inserted by Mike end
/*		// comments by Mike
			$image=$_cfg['main::films_url'].$poster['image'];
*/
			$result['poster']['url']=main_films::getFilmPostersUrl($_GET['film_id']);
		} else {
         	//$image = 'blank_news_img.jpg'; by Mike
         	$image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		}

		//$result['poster']['image'] = _ROOT_URL.'public/main/rescrop_4.php?f='.str_replace(_ROOT_URL,'',$image).'&t=5';
		$result['poster']['image'] = $image;


		if(main_films::isFilmTrailers($_GET['film_id']))
		{

     		$q="
			SELECT
				trl.film_id,
				trl.id AS `trailer_id`,
				trl.image,
				trl.file,
				trl.url,
				trl.user_id,
				trl.shows,
				trl.width,
				trl.height,
				trl.order_number,
				trl.public,
				flm_lng.title AS `film`,
				flm_pst.image as `poster`,
				DATE(trl.date) as date,
				trl.duration
			FROM `#__main_films_trailers` AS `trl`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=trl.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			LEFT JOIN `#__main_films_photos` AS `flm_pst`
			ON flm_pst.film_id=trl.film_id
			WHERE trl.film_id='".intval($_GET['film_id'])."'
			AND trl.public=1
			ORDER BY trl.order_number ASC
			LIMIT 1
			";
     		
			$t=$_db->query($q);
			$trails=$_db->fetchAssoc($t);
		
			$result['file'] = _ROOT_URL.'public/main/films/'.$trails['file'];
			$result['image'] = $trails['image'];
			$result['trailer_date'] = $trails['date'];
			$result['typevideo'] = substr($trails['file'], -3);
			$result['trailer']=true;
		}

	$genres = main_films::getFilmGenresTitlesIds($_GET['film_id']);

	$gnr = array();

	foreach ($genres as $genr)
	{
		$gnr[] = "<a href='".sys::rewriteUrl('?mod=main&act=films&genre_id='.$genr['id'])."'  title='".sys::translate('main::films')." - ".$genr['title']."'>".$genr['title']."</a>";
	}

	$countries = main_films::getFilmCountriesTitlesIds($_GET['film_id']);

	$cnt = array();

	foreach ($countries as $country)
	{
		$cnt[] = "<a href='".sys::rewriteUrl('?mod=main&act=films&country='.$country['id'])."' title='".sys::translate('main::films')." ".$country['title']."'>".$country['title']."</a>";
	}

	if($result['votes']>5) // только при наличии 5 голосов после премьеры показывать реальный рейтинг	
		$result['rating']=$result['sum']/$result['votes'];
	else // до 5 голосов показывать предварительный усредненный рейтинг
		$result['rating']=($result['pre_sum']+$result['sum'])/($result['pre_votes']+$result['votes']);

	$result['genres'] = $gnr;
	$result['countries']=$cnt;
	$result['directors']=main_films::getFilmDirectors($_GET['film_id']);
	$result['actors']=main_films::getFilmActors($_GET['film_id']);
	$result['studios']=main_films::getFilmStudiosTitles($_GET['film_id']);
	$result['links']=main_films::getFilmLinks($_GET['film_id']);
	$result['rating']=main_films::formatFilmRating($result['rating']);
	$result['pre_rating']=main_films::formatFilmRating($result['pre_rating']);
	$result['pro_rating']=main_films::formatFilmRating($result['pro_rating']);
	$result['rating_url']=main_films::getFilmRatingUrl($_GET['film_id']);
	$result['discuss_url']=main_films::getFilmDiscussUrl($_GET['film_id']);
	$result['reviews_url']=main_films::getFilmReviewsUrl($_GET['film_id']);
	$result['imdb_url']=main_films::getFilmImdbUrl($result['title_orig']);
	$result['persons_url']=main_films::getFilmPersonsUrl($_GET['film_id']);


	$result['intro'] = eregi_replace("<div[^>]*>", "<div>", $result['intro']);
	$result['intro'] = eregi_replace("<b[^>]*>", "<b>", $result['intro']);
	$result['intro'] = eregi_replace("<strong[^>]*>", "<strong>", $result['intro']);
	$result['intro'] = eregi_replace("<span[^>]*>", "", $result['intro']);
	$result['intro'] = eregi_replace("</span>", "", $result['intro']);
	$search = array ("'<span[^>]*?>.*?</span>'si");                    // интерпретировать как php-код
	$replace = array ("");
	$result['intro'] = eregi_replace($search, $replace, $result['intro']);
	$result['intro'] = str_replace("<div>&nbsp;</div>","",$result['intro']);
	$result['intro'] = str_replace('<div >&nbsp;</div>',"",$result['intro']);
	$result['intro'] = str_replace("<p>&nbsp;</p>","",$result['intro']);

//	$imdb_id=$_db->getRecord('main_films_imdb_to_kt','kt_id='.intval($_GET['film_id']),false);
//	$result['imdb_id']=is_array($imdb_id)?$imdb_id["imdb_id"]:"";
	
	$result['imdb_id']="";
	$tmdb_id_rec=$_db->getRecord('main_films_tmdb_to_kt','kt_id='.intval($_GET['film_id']),false);
	if($tmdb_id_rec)
	{		
		$imdb_id=$_db->getRecord('main_films_tmdb','id='.intval($tmdb_id_rec["tmdb_id"]),false);
		$result['imdb_id']=is_array($imdb_id)?$imdb_id["imdb_id"]:"";
	}
		
	if ($_GET['city_id']=='1')
	{
		$ro=$_db->query("
			SELECT
				count(*) as count
			FROM `#__main_shows`

			WHERE
				(hall_id='34'
			OR
				hall_id='146'
			OR
				hall_id='164'
			OR
				hall_id='364'
			OR
				hall_id='369'
			)
			AND
				film_id='".$_GET['film_id']."'
			AND
				begin<='".date('Y-m-d')."'
			AND
				end>='".date('Y-m-d')."'
		");

		$imax = $_db->fetchAssoc($ro);

		if ($imax['count'])
		{
			$result['sputnik'] = 1;
			$result['sputnikcinema'] = 34;
			$result['sputniklabel'] = 'sputnik';
		}
	}
	if(doubleval($result['budget'])>0.0)
		$result['budget']=doubleval($result['budget']).' '.sys::translate('main::mil').' '.sys::translate('main::'.$result['budget_currency']);
	else 	
		$result['budget'] = 0;
		
	if($result['ukraine_premiere']!='0000-00-00')
		$result['ukraine_premiere']=sys::db2Date($result['ukraine_premiere'],false,$_cfg['sys::date_format']);
	else
		$result['ukraine_premiere']=false;

	if($result['world_premiere']!='0000-00-00')
		$result['world_premiere']=sys::db2Date($result['world_premiere'],false,$_cfg['sys::date_format']);
	else
		$result['world_premiere']=false;

	if($result['text'])
		$result['text_url']=main_films::getFilmTextUrl($_GET['film_id']);
	else
		$result['text_url']=false;


	//Отслеживание--------------------------------------------
	if(isset($_POST['cmd_tracking']))
	{
		main_films::trackingFilm($_GET['film_id']);
		sys::redirect('?mod=main&act=film&film_id='.$_GET['film_id']);
	}
	if(isset($_POST['cmd_untracking']))
	{
		main_films::untrackingFilm($_GET['film_id']);
		sys::redirect('?mod=main&act=film&film_id='.$_GET['film_id']);
	}
	//============================================================


	//Рецензии------------------------------------
	$r=$_db->query("
		SELECT
			rev.id,
			rev.title,
			rev.intro,
			rev.text,
			rev.mark,
			rev.date,
			rev.image,
			usr.login AS `user`,
			rev.user_id
		FROM
			`#__main_reviews` AS `rev`
		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=rev.user_id
		WHERE rev.film_id=".intval($_GET['film_id'])."
		AND rev.public=1
		AND rev.date<'".gmdate('Y-m-d H:i:s')."'
		ORDER BY rev.date DESC
	");
	$result['reviews']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['date']=sys::russianDate($obj['date']);
		$obj['url']=main_reviews::getReviewUrl($obj['id']);
		$obj['user_url']=main_users::getUserReviewsUrl($obj['user_id']);

		if($obj['image'])
		{
			$image=$_cfg['main::reviews_url'].$obj['image'];
        } else {
         	$image = 'blank_news_img.jpg';
		}
			$obj['image'] = _ROOT_URL.'public/main/rescrop_4.php?f='.str_replace(_ROOT_URL,'',$image).'&t=4';



		$obj['text'] = strip_tags($obj['text'], '<p><div>');
		$obj['text'] = str_replace('<p>&nbsp;</p>', '', $obj['text']);
		$obj['text'] = str_replace('&nbsp;', '', $obj['text']);

		$text = strip_tags($obj['text']);

		$obj['intro'] = cut_word($text).'...';



		$result['reviews'][]=$obj;
	}
	//================================================


	//Комментарии--------------------------------------------
	$q="
		SELECT
			msg.id,
			msg.date,
			msg.user_id,
			msg.user_name,
			msg.rating,
			usr.login AS `user`,
			msg.text,
			grp.group_id AS `star`,
			data.main_avatar AS `avatar`

		FROM `#__main_discuss_messages` AS `msg`

		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=msg.user_id

		LEFT JOIN `#__sys_users_data` AS `data`
		ON data.user_id=msg.user_id

		LEFT JOIN `#__sys_users_groups` as grp
		ON grp.user_id=msg.user_id
		AND grp.group_id=23

		WHERE msg.object_id='".intval($_GET['film_id'])."'
		AND msg.object_type='film'

		ORDER BY msg.date DESC

		LIMIT 0,5
	";

	$r=$_db->query($q);
	$result['comments']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true);
		if($obj['user_id']==2)
		{
			$obj['user']=$obj['user_name'];
			$obj['user_url']=false;
		}
		else
			$obj['user_url']=main_users::getUserUrl($obj['user_id']);
		$obj['bb_text']=sys::parseBBCode($obj['text']);
		$result['comments'][]=$obj;
	}

	//====================================================


	if(sys::checkAccess('main::news_edit'))
		$result['edit_url']='ok';

	//if(sys::isDebugIP())	$_db->printR($result);	
	
	$og=array(); // микроразметка яндекса https://help.yandex.ru/webmaster/video/open-graph-markup.xml
	if($result['trailer'])
	{
		$og[] = '<meta property="og:title" content="'.$result['title'].'"/>';
		$og[] = '<meta property="og:url" content="'._CANONIC.'"/>';
	
		$og[] = '<meta property="og:video" content="'.$result['file'].'"/>';
		$og[] = '<meta property="ya:ovs:upload_date" content="'.$result['trailer_date'].'"/>';
		$og[] = '<meta property="og:image" content="'.$_cfg['main::films_url'].$result['image'].'"/>';
		$og[] = '<meta property="og:description" content="'.strip_tags($result['intro']).'"/>';
		$og[] = '<meta property="ya:ovs:adult" content="'.(((intval($result['age_limit'])>18))?"yes":"no").'"/>';
		$og[] = '<meta property="video:duration" content="'.intval($result['duration']).'"/>';
		$og[] = '<meta property="og:type" content="video.movie"/>';
		$og[] = '<meta property="og:video:type" content="flash"/>';

		//$og[] = '<meta property="og:thumbnailUrl" content="'.$_cfg['main::films_url'].$result['image'].'"/>';
		//$og[] = '<meta property="og:uploadDate" content="'.$result['trailer_date'].'"/>';
		//$og[] = '<meta property="og:dateCreated" content="'.$result['trailer_date'].'"/>';
		
		
		$result['meta']['meta_other'] = "\r\n\r\n".implode("\r\n", $og)."\r\n\r\n";
		$result['og'] = ("".$result['meta']['meta_other']!="");
	}

	return $result;
}
?>