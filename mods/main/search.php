<?

function maxsite_str_word($text, $counttext = 10, $sep = ' ') {
	$words = split($sep, $text);
	if ( count($words) > $counttext )
		$text = join($sep, array_slice($words, 0, $counttext)).'...';
	return $text;
}


function main_search()
{
	define('_NOINDEX','1');

function dmword($string, $is_cyrillic = true)
	{
		static $codes = array(
			'A' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1)),
				'U' => array(array(0, 7, -1))),
			'B' => array(array(7, 7, 7)),
			'C' => array(array(5, 5, 5), array(4, 4, 4),
				'Z' => array(array(4, 4, 4),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'Z' => array(array(4, 4, 4))),
				'K' => array(array(5, 5, 5), array(45, 45, 45)),
				'H' => array(array(5, 5, 5), array(4, 4, 4),
					'S' => array(array(5, 54, 54)))),
			'D' => array(array(3, 3, 3),
				'T' => array(array(3, 3, 3)),
				'Z' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'Z' => array(array(4, 4, 4))),
				'R' => array(
					'S' => array(array(4, 4, 4)),
					'Z' => array(array(4, 4, 4)))),
			'E' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1)),
				'U' => array(array(1, 1, -1))),
			'F' => array(array(7, 7, 7),
				'B' => array(array(7, 7, 7))),
			'G' => array(array(5, 5, 5)),
			'H' => array(array(5, 5, -1)),
			'I' => array(array(0, -1, -1),
				'A' => array(array(1, -1, -1)),
				'E' => array(array(1, -1, -1)),
				'O' => array(array(1, -1, -1)),
				'U' => array(array(1, -1, -1))),
			'J' => array(array(4, 4, 4)),
			'K' => array(array(5, 5, 5),
				'H' => array(array(5, 5, 5)),
				'S' => array(array(5, 54, 54))),
			'L' => array(array(8, 8, 8)),
			'M' => array(array(6, 6, 6),
				'N' => array(array(66, 66, 66))),
			'N' => array(array(6, 6, 6),
				'M' => array(array(66, 66, 66))),
			'O' => array(array(0, -1, -1),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1))),
			'P' => array(array(7, 7, 7),
				'F' => array(array(7, 7, 7)),
				'H' => array(array(7, 7, 7))),
			'Q' => array(array(5, 5, 5)),
			'R' => array(array(9, 9, 9),
				'Z' => array(array(94, 94, 94), array(94, 94, 94)), // special case
				'S' => array(array(94, 94, 94), array(94, 94, 94))), // special case
			'S' => array(array(4, 4, 4),
				'Z' => array(array(4, 4, 4),
					'T' => array(array(2, 43, 43)),
					'C' => array(
						'Z' => array(array(2, 4, 4)),
						'S' => array(array(2, 4, 4))),
					'D' => array(array(2, 43, 43))),
				'D' => array(array(2, 43, 43)),
				'T' => array(array(2, 43, 43),
					'R' => array(
						'Z' => array(array(2, 4, 4)),
						'S' => array(array(2, 4, 4))),
					'C' => array(
						'H' => array(array(2, 4, 4))),
					'S' => array(
						'H' => array(array(2, 4, 4)),
						'C' => array(
							'H' => array(array(2, 4, 4))))),
				'C' => array(array(2, 4, 4),
					'H' => array(array(4, 4, 4),
						'T' => array(array(2, 43, 43),
							'S' => array(
								'C' => array(
									'H' => array(array(2, 4, 4))),
								'H' => array(array(2, 4, 4))),
							'C' => array(
								'H' => array(array(2, 4, 4)))),
						'D' => array(array(2, 43, 43)))),
				'H' => array(array(4, 4, 4),
					'T' => array(array(2, 43, 43),
						'C' => array(
							'H' => array(array(2, 4, 4))),
						'S' => array(
							'H' => array(array(2, 4, 4)))),
					'C' => array(
						'H' => array(array(2, 4, 4))),
					'D' => array(array(2, 43, 43)))),
			'T' => array(array(3, 3, 3),
				'C' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4))),
				'Z' => array(array(4, 4, 4),
					'S' => array(array(4, 4, 4))),
				'S' => array(array(4, 4, 4),
					'Z' => array(array(4, 4, 4)),
					'H' => array(array(4, 4, 4)),
					'C' => array(
						'H' => array(array(4, 4, 4)))),
				'T' => array(
					'S' => array(array(4, 4, 4),
						'Z' => array(array(4, 4, 4)),
						'C' => array(
							'H' => array(array(4, 4, 4)))),
					'C' => array(
						'H' => array(array(4, 4, 4))),
					'Z' => array(array(4, 4, 4))),
				'H' => array(array(3, 3, 3)),
				'R' => array(
					'Z' => array(array(4, 4, 4)),
					'S' => array(array(4, 4, 4)))),
			'U' => array(array(0, -1, -1),
				'E' => array(array(0, -1, -1)),
				'I' => array(array(0, 1, -1)),
				'J' => array(array(0, 1, -1)),
				'Y' => array(array(0, 1, -1))),
			'V' => array(array(7, 7, 7)),
			'W' => array(array(7, 7, 7)),
			'X' => array(array(5, 54, 54)),
			'Y' => array(array(1, -1, -1)),
			'Z' => array(array(4, 4, 4),
				'D' => array(array(2, 43, 43),
					'Z' => array(array(2, 4, 4),
						'H' => array(array(2, 4, 4)))),
				'H' => array(array(4, 4, 4),
					'D' => array(array(2, 43, 43),
						'Z' => array(
							'H' => array(array(2, 4, 4))))),
				'S' => array(array(4, 4, 4),
					'H' => array(array(4, 4, 4)),
					'C' => array(
						'H' => array(array(4, 4, 4))))));
		$length = strlen($string);
		$output = '';
		$i = 0;
		$previous = -1;
		while ($i < $length)
		{
			$current = $last = &$codes[$string[$i]];
			for ($j = $k = 1; $k < 7; $k++)
			{
				if (!isset($string[$i + $k]) || !isset($current[$string[$i + $k]]))
					break;
				$current = &$current[$string[$i + $k]];
				if (isset($current[0]))
				{
					$last = &$current;
					$j = $k + 1;
				}
			}
			if ($i == 0)
				$code = $last[0][0];
			elseif (!isset($string[$i + $j]) || ($codes[$string[$i + $j]][0][0] != 0))
				$code = $is_cyrillic ? (isset($last[1]) ? $last[1][2] : $last[0][2]) : $last[0][2];
			else
				$code = $is_cyrillic ? (isset($last[1]) ? $last[1][1] : $last[0][1]) : $last[0][1];
			if (($code != -1) && ($code != $previous))
				$output .= $code;
			$previous = $code;
			$i += $j;
		}
		return str_pad(substr($output, 0, 6), 6, '0');
	}

//------------------------------------------------------------------------------

	function dmstring($string)
	{
		$is_cyrillic = false;
		if (preg_match('/[А-Яа-я]/iu', $string) === 1)
		{
			$string = translit($string);
			$is_cyrillic = true;
		}







		$firstword = '';
		$lastword = '';


		$words = explode(' ', $string);


		if (preg_match('/[A-Za-z]/iu', $words[0]) !== 1)
		{
			$firstword = $words[0];
		}

		if (preg_match('/[A-Za-z]/iu', $words[count($words)-1]) !== 1)
		{
			$lastword = $words[count($words)-1];
		}






		$string = preg_replace(array('/[^\w\s]|\d/iu', '/\b[^\s]{1,1}\b/iu', '/\s{1,}/iu', '/^\s+|\s+$/iu'), array('', '', ' '), strtoupper($string));


		if ($string[0])
		{
			$matches = explode(' ', $string);
		}  else {
			$matches = array();
		}


		foreach($matches as $key => $match)
			$matches[$key] = dmword($match, $is_cyrillic);


		if ($firstword)
		{
			array_unshift($matches, $firstword);
		}

		if ($lastword)
		{
			$matches[]=$lastword;
		}




		return $matches;
	}

//------------------------------------------------------------------------------

	function translit($string)
	{
		static $ru = array(
			'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
			'Ё', 'ё', 'Ж', 'ж', 'З', 'з', 'И', 'и', 'Й', 'й', 'К', 'к',
			'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',
			'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц',
			'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ', 'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь',
			'Э', 'э', 'Ю', 'ю', 'Я', 'я'
		);
		static $en = array(
			'A', 'a', 'B', 'b', 'V', 'v', 'G', 'g', 'D', 'd', 'E', 'e',
			'E', 'e', 'Zh', 'zh', 'Z', 'z', 'I', 'i', 'J', 'j', 'K', 'k',
			'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'R', 'r',
			'S', 's', 'T', 't', 'U', 'u', 'F', 'f', 'H', 'h', 'C', 'c',
			'Ch', 'ch', 'Sh', 'sh', 'Sch', 'sch', '\'', '\'', 'Y', 'y', '\'', '\'',
			'E', 'e', 'Ju', 'ju', 'Ja', 'ja'
		);
		$string = str_replace($ru, $en, $string);
		return $string;
	}

//------------------------------------------------------------------------------




	sys::useLib('main::genres');
	sys::useLib('main::persons');
	sys::useLib('main::cinemas');
	sys::useLib('main::countries');
	sys::useLib('main::news');
	sys::useLib('sys::form');
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('keyword');
	$result=array(0);

	$result['meta']=sys::parseModTpl('main::search','page');

	$_GET['keywords'] = strip_tags($_GET['keywords']);
	$_GET['keywords'] = urldecode($_GET['keyword']);


        $cyrillic = 0;

		if (preg_match('/[А-Яа-я]/iu', $_GET['keywords']) === 1)
		{
			$cyrillic = 1;
		}



	$coded = dmstring($_GET['keywords']);

	$chars = str_split($coded[count($coded)-1]);

	$coded[count($coded)-1] = '';

	$coded[count($coded)-1] .= $chars[0];

	for ($i=1;$i<=(count($chars)-1);$i++)
	{
		if ($chars[$i]!='0')
		{
			$coded[count($coded)-1] .= $chars[$i];
		}
	}

	$text = implode(',', $coded);
	$text = str_replace("'","",$text);

	$q="
		SELECT
			flm.id,
			flm.name,
			flm.title_orig,
			flm.year,
			flm.rating,
			flm.votes,
			lng.title,
			lng.intro
		FROM
			`grifix_main_films` AS `flm`
		LEFT JOIN
			`grifix_main_films_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	if($_GET['keywords'])
		$q.="WHERE
			`name` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `title_orig` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `title_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `title_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `title` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `title` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `title` LIKE '".mysql_real_escape_string($_GET['keywords'])."'";

	$q.=" ORDER BY flm.total_shows DESC LIMIT 5";
	$r=$_db->query($q);

	$wherenot = '';
	$if = 0;
	while($obj=$_db->fetchAssoc($r))
	{

		$qw=$_db->query("
			SELECT
				pst.image as `poster`

			FROM `#__main_films_posters` AS `pst`

			WHERE pst.film_id=".intval($obj['id'])."
			AND pst.order_number=1
			LIMIT 1
		");

		$poster = '';

		while($object=$_db->fetchAssoc($qw))
		{
			$poster = $object['poster'];
		}

		if ($poster)
		{
			$obj['poster'] = _ROOT_URL.'public/main/films/x2_'.$poster.'';
		} else {
			$obj['poster'] = _ROOT_URL.'ims/kt_logo.jpg';
		}

		$qw=$_db->query("
			SELECT
				lng.title as `country`

			FROM `#__main_films_countries` AS `cnt`

			LEFT JOIN
				 `#__main_countries_lng` AS `lng`
			ON
				cnt.country_id=lng.record_id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE cnt.film_id=".intval($obj['id'])."
			LIMIT 1
		");

		$obj['infolink'] = main_films::getFilmUrl($obj['id']);
		$obj['showslink'] = main_films::getFilmShowsUrl($obj['id']);
		$obj['disclink'] = main_films::getFilmDiscussUrl($obj['id']);

		if(main_films::isFilmReviews($obj['id']))
		{
			$obj['revlink'] = main_films::getFilmReviewsUrl($obj['id']);
		}

		if(main_films::isFilmNews($obj['id']))
		{
			$obj['newslink'] = main_films::getFilmNewsUrl($obj['id']);
		}

		if(main_films::isFilmPersons($obj['id']))
		{
			$obj['perslink'] = main_films::getFilmPersonsUrl($obj['id']);
		}

		if(main_films::isFilmPhotos($obj['id']))
		{
			$obj['photlink'] = main_films::getFilmPhotosUrl($obj['id']);
		}

		if(main_films::isFilmWallpapers($obj['id']))
		{
			$obj['walllink'] = main_films::getFilmWallpapersUrl($obj['id']);
		}

		if(main_films::isFilmPosters($obj['id']))
		{
			$obj['postlink'] = main_films::getFilmPostersUrl($obj['id']);
		}

		if(main_films::isFilmTrailers($obj['id']))
		{
			$obj['traillink'] = main_films::getFilmTrailersUrl($obj['id']);
		}

		if(main_films::isFilmSountracks($obj['id']))
		{
			$obj['soundlink'] = main_films::getFilmSoundtracksUrl($obj['id']);
		}


		while($object=$_db->fetchAssoc($qw))
		{
			$obj['country'] = $object['country'];

		}

		$qw=$_db->query("
			SELECT
				lng.title as `genre`

			FROM `#__main_films_genres` AS `cnt`

			LEFT JOIN
				 `#__main_genres_lng` AS `lng`
			ON
				cnt.genre_id=lng.record_id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE cnt.film_id=".intval($obj['id'])."
		");

        $obje['genres'] = array();

		while($object=$_db->fetchAssoc($qw))
		{
			$obje['genres'][] = $object['genre'];
		}

		$obj['genres'] = implode(', ', $obje['genres']);


        $object['actors'] = array();
        $obje['actors'] = array();

        $pi = 0;
			$obje['actors']=main_films::getFilmActors($obj['id']);

            if (is_array($obje['actors'][0]))
            {

	            for ($i=0;$i<=1;$i++)
	            {
	            	if ($obje['actors'][$i]['url'])
	            	{
						$object['actors'][] = '<a href="'.$obje['actors'][$i]['url'].'">'.$obje['actors'][$i]['fio'].'</a>';
						$pi++;
					}
	            }

            }

		if ($pi>0)
		{
			$obj['actors'] = sys::translate('main::actors').': '.implode(', ', $object['actors']);
		}

        $object['directors'] = array();
        $obje['directors'] = array();

        $di = 0;
			$obje['directors']=main_films::getFilmDirectors($obj['id']);


            if (is_array($obje['directors'][0]))
            {

	            for ($i=0;$i<1;$i++)
	            {
						$object['directors'][] = '<a href="'.$obje['directors'][$i]['url'].'">'.$obje['directors'][$i]['fio'].'</a>';
						$di++;
	            }

            }

		if ($di>0)
		{
			$obj['directors'] = sys::translate('main::directors').': '.implode(', ', $object['directors']);
		}

		$obj['third'] = array();
		if ($obj['directors'])
		$obj['third'][] = $obj['directors'];
		if ($obj['actors'])
		$obj['third'][] = $obj['actors'];

		if ($obj['directors'] || $obj['actors'])
		$obj['third_row'] = implode(', ', $obj['third']);







		if ($obj['rating']>0)
		{
			$obj['rate'] = sys::translate('main::rating').': <strong>'.round($obj['rating'], 1).'</strong> / '.sys::translate('main::votes').': '.$obj['votes'].'<br>';
		}


		$obj['round'] = '';

		if ($obj['title_orig'])
		{
			$obj['round'] = $obj['title_orig'];
		}

		if ($obj['year']>0)
		{
			$obj['year'] = $obj['year'];
		} else {
			$obj['year']='';
		}

		$obj['second'] = array();
		if ($obj['year'])
		$obj['second'][] = $obj['year'];
		if ($obj['country'])
		$obj['second'][] = $obj['country'];
		if ($obj['genres'])
		$obj['second'][] = $obj['genres'];

		if ($obj['year'] || $obj['country'] || $obj['genres'])
		$obj['second_row'] = implode(' | ', $obj['second']).'<br><br>';



		$obj['url']=main_films::getFilmUrl($obj['id']);
		$result['films'][]=$obj;
        $wherenot .= ' AND flm.id!="'.$obj['id'].'"';
		$if++;
	}



	if ($if<5)
	{
		$q="
			SELECT
				flm.id,
				flm.name,
				flm.title_orig,
				flm.year,
				flm.rating,
				flm.votes,
				lng.title,
				lng.intro,
				flm.coded as `fcode`,
				lng.coded as `lcode`
			FROM
				`grifix_main_films` AS `flm`
			LEFT JOIN
				`grifix_main_films_lng` AS `lng`
			ON
				lng.record_id=flm.id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."
		";

		if($_GET['keywords']!='*')
		{
			$q.="WHERE ";

			if ($cyrillic)
			{
				$q .= "(lng.coded LIKE '".$text."%'
				OR lng.coded LIKE '%,".$text."%')";
			} else {
				$q .= "(flm.coded LIKE '".$text."%'
				OR flm.coded LIKE '%,".$text."%')";
			}

			$q .= $wherenot;
        }
   		$q.=" ORDER BY flm.total_shows DESC LIMIT 100";
		$r=$_db->query($q);
		$list = array();

		while($obj=$_db->fetchAssoc($r))
		{

			$obj['min'] = 1000;

			$keyw = strtoupper($_GET['keywords']);

			$origarray = explode(' ', strtoupper($obj['title_orig']));

			foreach ($origarray as $key=>$value)
			{
				if (levenshtein($keyw, substr($value, 0, strlen($keyw)))<$obj['min'])
					{
						$obj['min'] = 	levenshtein($keyw, substr($value, 0, strlen($keyw)));
					}
			}


			$justarray = explode(' ', strtoupper($obj['title']));

			foreach ($justarray as $key=>$value)
			{
				if (levenshtein($keyw, substr($value, 0, strlen($keyw)))<$obj['min'])
					{
						$obj['min'] = 	levenshtein($keyw, substr($value, 0, strlen($keyw)));
					}
			}

			$qw=$_db->query("
				SELECT
					pst.image as `poster`

				FROM `#__main_films_posters` AS `pst`

				WHERE pst.film_id=".intval($obj['id'])."
				AND pst.order_number=1
				LIMIT 1
			");

			$poster = '';

			while($object=$_db->fetchAssoc($qw))
			{
				$poster = $object['poster'];
			}

			if ($poster)
			{
				$obj['poster'] = _ROOT_URL.'public/main/resize.php?f='.$poster.'&w=60';
			} else {
				$obj['poster'] = _ROOT_URL.'ims/kt_logo.jpg';
			}

			$qw=$_db->query("
				SELECT
					lng.title as `country`

				FROM `#__main_films_countries` AS `cnt`

				LEFT JOIN
					 `#__main_countries_lng` AS `lng`
				ON
					cnt.country_id=lng.record_id
				AND
					lng.lang_id=".intval($_cfg['sys::lang_id'])."

				WHERE cnt.film_id=".intval($obj['id'])."
				LIMIT 1
			");



			while($object=$_db->fetchAssoc($qw))
			{
				$obj['country'] = $object['country'];

			}

			$qw=$_db->query("
				SELECT
					lng.title as `genre`

				FROM `#__main_films_genres` AS `cnt`

				LEFT JOIN
					 `#__main_genres_lng` AS `lng`
				ON
					cnt.genre_id=lng.record_id
				AND
					lng.lang_id=".intval($_cfg['sys::lang_id'])."

				WHERE cnt.film_id=".intval($obj['id'])."
			");

	        $obje['genres'] = array();

			while($object=$_db->fetchAssoc($qw))
			{
				$obje['genres'][] = $object['genre'];
			}

			$obj['genres'] = implode(', ', $obje['genres']);


	        $object['actors'] = array();
	        $obje['actors'] = array();

	        $pi = 0;
				$obje['actors']=main_films::getFilmActors($obj['id']);

	            if (is_array($obje['actors'][0]))
	            {

		            for ($i=0;$i<=1;$i++)
		            {
		            	if ($obje['actors'][$i]['url'])
		            	{
							$object['actors'][] = '<a href="'.$obje['actors'][$i]['url'].'">'.$obje['actors'][$i]['fio'].'</a>';
							$pi++;
						}
		            }

	            }

			if ($pi>0)
			{
				$obj['actors'] = sys::translate('main::actors').': '.implode(', ', $object['actors']);
			}

	        $object['directors'] = array();
	        $obje['directors'] = array();

	        $di = 0;
				$obje['directors']=main_films::getFilmDirectors($obj['id']);


	            if (is_array($obje['directors'][0]))
	            {

		            for ($i=0;$i<1;$i++)
		            {
							$object['directors'][] = '<a href="'.$obje['directors'][$i]['url'].'">'.$obje['directors'][$i]['fio'].'</a>';
							$di++;
		            }

	            }

			if ($di>0)
			{
				$obj['directors'] = sys::translate('main::directors').': '.implode(', ', $object['directors']);
			}

			$obj['third'] = array();
			if ($obj['directors'])
			$obj['third'][] = $obj['directors'];
			if ($obj['actors'])
			$obj['third'][] = $obj['actors'];

			if ($obj['directors'] || $obj['actors'])
			$obj['third_row'] = implode(', ', $obj['third']);







			if ($obj['rating']>0)
			{
				$obj['rate'] = sys::translate('main::rating').': <strong>'.round($obj['rating'], 1).'</strong> / '.sys::translate('main::votes').': '.$obj['votes'].'<br>';
			}


			$obj['round'] = '';

			if ($obj['title_orig'])
			{
				$obj['round'] = $obj['title_orig'];
			}

			if ($obj['year']>0)
			{
				$obj['year'] = $obj['year'];
			} else {
				$obj['year']='';
			}

			$obj['second'] = array();
			if ($obj['year'])
			$obj['second'][] = $obj['year'];
			if ($obj['country'])
			$obj['second'][] = $obj['country'];
			if ($obj['genres'])
			$obj['second'][] = $obj['genres'];

			if ($obj['year'] || $obj['country'] || $obj['genres'])
			$obj['second_row'] = implode(' | ', $obj['second']).'<br><br>';



			$obj['url']=main_films::getFilmUrl($obj['id']);

			if (is_array($list[$obj['min']]))
			{
				if (is_array($list[$obj['min'].'1']))
				{
					$list[$obj['min'].'2']=$obj;
				} else {
					$list[$obj['min'].'1']=$obj;
				}
				$near++;

			} else {
				$list[$obj['min']]=$obj;
				$near++;
			}
		}

		ksort($list);

		$list = array_values($list);

		$lis = array();

		$k=0;
		$kt = 5-$if;

		foreach ($list as $key=>$value)
		{
			$k++;

			if ($k<=$kt)
			{
				$lis[] = $value;
			}
		}

		foreach ($lis as $key=>$obj)
		{
			$result['fnear'][]=$obj;
        }


	}



	$ip=0;
	$wherenot='';




	$q="
		SELECT
			flm.id,
			flm.lastname_orig,
			flm.firstname_orig,
			flm.rating,
			flm.rating_votes,
			flm.birthdate,
			lng.biography,
			lng.lastname,
			lng.firstname
		FROM
			`grifix_main_persons` AS `flm`
		LEFT JOIN
			`grifix_main_persons_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	if($_GET['keywords'])
		$q.="WHERE
			`name` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `name` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname_orig` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `firstname_orig` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname_orig` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `lastname` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `lastname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `firstname` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `firstname` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			";

	$q.=" ORDER BY flm.total_shows DESC LIMIT 5";
	$r=$_db->query($q);


	while($obj=$_db->fetchAssoc($r))
	{

		$qw=$_db->query("
			SELECT
				pst.image as `poster`

			FROM `#__main_persons_photos` AS `pst`

			WHERE pst.person_id=".intval($obj['id'])."
			AND pst.order_number=1
			LIMIT 1
		");

		$obj['title'] = $obj['firstname'].' '.$obj['lastname'];

		$poster = '';

		while($object=$_db->fetchAssoc($qw))
		{
			$poster = $object['poster'];
		}

		if ($poster)
		{
			$obj['poster'] = _ROOT_URL.'public/main/persons/x3_'.$poster;
		} else {
			$obj['poster'] = _ROOT_URL.'images/userpic.jpg';
		}





		if ($obj['rating']>0)
		{
			$obj['rate'] = sys::translate('main::rating').': <strong>'.round($obj['rating'], 1).'</strong> / '.sys::translate('main::votes').': '.$obj['rating_votes'].'<br>';
		}


		$obj['biography'] = strip_tags(maxsite_str_word($obj['biography'], 30).'...');

		$obj['round'] = '';

		if ($obj['firstname_orig'])
		{
			$obj['round'] = $obj['firstname_orig'].' '.$obj['lastname_orig'];
		}





		$obj['url']=main_persons::getPersonUrl($obj['id']);
		$result['persons'][]=$obj;


		$ip++;
        $wherenot .= ' AND flm.id!="'.$obj['id'].'"';
	}



    if ($ip<5)
    {
		$q="
			SELECT
				flm.id,
				flm.lastname_orig,
				flm.firstname_orig,
				flm.rating,
				flm.rating_votes,
				flm.birthdate,
				lng.biography,
				lng.lastname,
				lng.firstname,
				flm.coded as `fcode`,
				lng.coded as `lcode`
			FROM
				`grifix_main_persons` AS `flm`
			LEFT JOIN
				`grifix_main_persons_lng` AS `lng`
			ON
				lng.record_id=flm.id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."
		";

		if($_GET['keywords']!='*')
		{
			$q.="WHERE ";

			if ($cyrillic)
			{
				$q .= "(lng.coded LIKE '".$text."%'
				OR lng.coded LIKE '%,".$text."%')";
			} else {
				$q .= "(flm.coded LIKE '".$text."%'
				OR flm.coded LIKE '%,".$text."%')";
			}

			$q .= $wherenot;
        }
		$q.=" ORDER BY flm.total_shows DESC LIMIT 100";

		$r=$_db->query($q);

		$list = array();

		while($obj=$_db->fetchAssoc($r))
		{

			$obj['min'] = 1000;

			$keyw = strtoupper($_GET['keywords']);

			$origarray = explode(', ', strtoupper($obj['name']));

			foreach ($origarray as $key=>$value)
			{
				if (levenshtein($keyw, substr($value, 0, strlen($keyw)))<$obj['min'])
					{
						$obj['min'] = 	levenshtein($keyw, substr($value, 0, strlen($keyw)));
					}
			}


			$justarray = array();
			$justarray[] = strtoupper($obj['lastname']);
			$justarray[] = strtoupper($obj['firstname']);

			foreach ($justarray as $key=>$value)
			{
				if (levenshtein($keyw, substr($value, 0, strlen($keyw)))<$obj['min'])
					{
						$obj['min'] = 	levenshtein($keyw, substr($value, 0, strlen($keyw)));
					}
			}

			$qw=$_db->query("
				SELECT
					pst.image as `poster`

				FROM `#__main_persons_photos` AS `pst`

				WHERE pst.person_id=".intval($obj['id'])."
				AND pst.order_number=1
				LIMIT 1
			");

			$obj['url']=main_persons::getPersonUrl($obj['id']);
			$obj['title'] = $obj['firstname'].' '.$obj['lastname'];

			$poster = '';

			while($object=$_db->fetchAssoc($qw))
			{
				$poster = $object['poster'];
			}

			/*if ($poster)
			{
				$obj['poster'] = _ROOT_URL.'public/main/resize2.php?f='.$poster.'&w=60';
			} else {
				$obj['poster'] = _ROOT_URL.'images/userpic.jpg';
			}*/
			
	if($poster)
	{
  	    $image="x2_".$poster; 
		if(!file_exists($_cfg['main::persons_dir'].$image)) $image = $_cfg['sys::root_url'].'images/userpic.jpg';
		 else $image=$_cfg['main::persons_url'].$image;
	} else {
     	$image = $_cfg['sys::root_url'].'images/userpic.jpg';
	}
	$obj['poster'] = $image;




			if ($obj['rating']>0)
			{
				$obj['rate'] = sys::translate('main::rating').': <strong>'.round($obj['rating'], 1).'</strong> / '.sys::translate('main::votes').': '.$obj['rating_votes'].'<br>';
			}


			$obj['biography'] = strip_tags(maxsite_str_word($obj['biography'], 30).'...');

			$obj['round'] = '';

			if ($obj['firstname_orig'])
			{
				$obj['round'] .= $obj['firstname_orig'].' '.$obj['lastname_orig'];
			}


			if (is_array($list[$obj['min']]))
			{
				if (is_array($list[$obj['min'].'1']))
				{
					$list[$obj['min'].'2']=$obj;
				} else {
					$list[$obj['min'].'1']=$obj;
				}
				$near++;

			} else {
				$list[$obj['min']]=$obj;
				$near++;
			}



		}

				ksort($list);

		$list = array_values($list);

		$lis = array();

		$k=0;
		$kt = 5 - $ip;

		foreach ($list as $key=>$value)
		{


			$k++;

			if ($k<=$kt)
			{
				$lis[] = $value;
			}
		}

		foreach ($lis as $key=>$obj)
		{
			$result['pnear'][]=$obj;
        }
	}



	$q="
		SELECT
			flm.id,
			flm.name,
			flm.phone,
			lng.title,
			flm.site,
			lng.address,
			ct.title as `city`
		FROM
			`grifix_main_cinemas` AS `flm`
		LEFT JOIN
			`grifix_main_cinemas_lng` AS `lng`
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		LEFT JOIN
			`grifix_main_countries_cities_lng` AS `ct`
		ON
			ct.record_id=flm.city_id
		AND
			ct.lang_id=".intval($_cfg['sys::lang_id'])."
	";

	if($_GET['keywords'])
		$q.="WHERE
			lng.`title` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR lng.`title` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR lng.`title` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			OR `name` LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR `name` LIKE '%".mysql_real_escape_string($_GET['keywords'])."'
			";

	$q.=" ORDER BY lng.title LIMIT 5";
	$r=$_db->query($q);



	while($obj=$_db->fetchAssoc($r))
	{

		$obj['round'] = '';

		if ($obj['city'])
		{
			$obj['round'] = $obj['city'];
		}


			if ($obj['address'])
         $obj['adress'] = '<strong>'.sys::translate('main::address').':</strong> '.$obj['address'].'<br>';
			if ($obj['phone'])
         $obj['phone'] = '<strong>'.sys::translate('main::phone').':</strong> '.$obj['phone'].'<br>';
			if ($obj['site'])
         $obj['website'] = '<a href="'.$obj['site'].'" target="_blank"> '.sys::translate('main::site').'</a>';

		$obj['url']=main_cinemas::getCinemaUrl($obj['id']);
		$result['cinemas'][]=$obj;
	}



	$q="
		SELECT
			art.id,
			art.date,
			art.user_id,
			art.exclusive,
			art.small_image AS `image`,
			art.image AS `big_image`,
			art_lng.title,
			art_lng.intro
		FROM `#__main_news_articles` AS `art`

		LEFT JOIN
			`#__main_news_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_news` AS `nws`
		ON art.news_id=nws.id
	";


	if($_GET['keywords'])
		$q.="WHERE
			art_lng.title LIKE '".mysql_real_escape_string($_GET['keywords'])."%'
			OR art_lng.title LIKE '%".mysql_real_escape_string($_GET['keywords'])."%'
			OR art_lng.title LIKE '%".mysql_real_escape_string($_GET['keywords'])."'

			";

	$q.="
		ORDER BY art.date DESC  LIMIT 5
	";

	$r=$_db->query($q);



	while($obj=$_db->fetchAssoc($r))
	{

		$obj['round'] = '';

		if($obj['image'])
			$image=$_cfg['main::news_url'].$obj['image'];

		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$obj['image']));

		if ($wid<120 && $obj['big_image'])
		{
		 	$image = $_cfg['main::news_url'].$obj['big_image'];
		} else if ($wid>=120)
		{
		 	$image = $_cfg['main::news_url'].$obj['image'];
		} else {
         	$image = 'blank_news_img.jpg';
		}

		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=20';


		$obj['date']=sys::russianDate($obj['date']);

		$obj['intro'] = strip_tags($obj['intro']);

		$obj['url']=main_news::getArticleUrl($obj['id']);
		$result['news'][]=$obj;
	}


	if ($_cfg['sys::lang_id']=='1')
	{
	$result['films_link'] = _ROOT_URL.'ru/main/search_films/keyword/'.mysql_real_escape_string($_GET['keywords']).'.phtml';
	$result['cinema_link'] = _ROOT_URL.'ru/main/search_cinema/keyword/'.mysql_real_escape_string($_GET['keywords']).'.phtml';
	$result['news_link'] = _ROOT_URL.'ru/main/search_news/keyword/'.mysql_real_escape_string($_GET['keywords']).'.phtml';
	$result['person_link'] = _ROOT_URL.'ru/main/search_person/keyword/'.mysql_real_escape_string($_GET['keywords']).'.phtml';
	} else {
	$result['films_link'] = _ROOT_URL.'uk/main/search_films/keyword/'.mysql_real_escape_string($_GET['keywords']).'.phtml';
	$result['cinema_link'] = _ROOT_URL.'uk/main/search_cinema/keyword/'.mysql_real_escape_string($_GET['keywords']).'.phtml';
	$result['news_link'] = _ROOT_URL.'uk/main/search_news/keyword/'.mysql_real_escape_string($_GET['keywords']).'.phtml';
	$result['person_link'] = _ROOT_URL.'uk/main/search_person/keyword/'.mysql_real_escape_string($_GET['keywords']).'.phtml';
	}

   	$result['all_results'] = sys::translate('main::all_results');
   	$result['films_title'] = sys::translate('main::films');
   	$result['persons_title'] = sys::translate('main::persons');
   	$result['cinemas_title'] = sys::translate('main::cinemas');
   	$result['news_title'] = sys::translate('main::news');
	return $result;
}
?>