<?php

function main_user_profile_payment(){
	
	define('_NOINDEX','1');
	sys::useLib('main::users');
	sys::useLib('sys::form');
	sys::useLib('sys::pages');
	sys::useJs('sys::gui');
	sys::useLib('main::payment');

	sys::filterGet('order_by','text','title.asc');
	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;
	
	if($_user['id']==2)	return 401;
	
	$cards = main_payment::getCards();

	$result['objects']=$cards;
	$result['title']=sys::translate('main::payment');
	$result['have_card']=( is_array($cards) && !isset($cards['error']) && (sizeof($cards>0))  )?1:0;
	
//	if(sys::isDebugIP())
//	{
//		sys::printR($cards);
//		sys::printR($result);
//	}
	
	return $result;
}

function splitterWord($word,$maxlength){

	$step = $maxlength;	
	if(mb_strlen($word) > $maxlength){
		return mb_substr($word,0,$maxlength).'...';
	}
	return $word;
	
}

?>