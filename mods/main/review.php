<?
function main_review()
{
	sys::useLib('main::reviews');
	sys::useLib('main::films');
	sys::useLib('main::spec_themes');
	sys::useLib('main::users');
	sys::useLib('main::discuss');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('review_id','int');

	define('_CANONICAL',main_reviews::getReviewUrl($_GET['review_id']),true);

	if(!$_GET['review_id'])
		return 404;




	$r=$_db->query("
		SELECT
		    rev.id,
			rev.title,
			rev.date,
			rev.mark,
			rev.image,
			rev.text,
			rev.user_id,
			rev.film_id,
			rev.comments,
			rev.total_shows,
			rev.public,
			spec.spec_theme_id as spec_theme,
			flm.year,
			flm.id as `film_id`,
			rev.thanks,
			usr.login AS `user`,
			usr_data.main_nick_name,			
			flm_lng.title AS `film`
		FROM `#__main_reviews` AS `rev`

		LEFT JOIN
			`#__main_films_lng` AS `flm_lng`
		ON
			flm_lng.record_id=rev.film_id
		AND
			flm_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN `#__main_films` AS `flm`
		ON flm.id=rev.film_id

		LEFT JOIN
			`#__sys_users` AS `usr`
		ON
			usr.id=rev.user_id

		LEFT JOIN
			`#__sys_users_data` AS `usr_data`
		ON
			usr.id=usr_data.user_id
			
		LEFT JOIN `#__main_reviews_articles_spec_themes` AS `spec`
		ON rev.id=spec.article_id

		WHERE rev.id=".intval($_GET['review_id'])."
	");

	$result=$_db->fetchAssoc($r);

	if(!$result)
		return 404;

	if(!$result['public'] && !sys::checkAccess('main::reviews_confirm'))
		return 404;

	if($result["main_nick_name"]) $result["user"] = $result["main_nick_name"];

	define('_CANONIC', main_films::getFilmUrl($result['film_id']));

	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::reviews_delete') || $result['user_id']==$_user['id'])
		{
			main_reviews::deleteReview($_GET['review_id']);
			sys::redirect('?mod=main&act=reviews');
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::reviews_confirm'))
	{
		main_reviews::confirmReview($_GET['review_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}

	$meta['review']=$result['title'];
	$meta['film']=$result['film'];
	$result['meta']=sys::parseModTpl('main::review','page',$meta);
// inserted by Mike begin
  	    $image="review_".$result['id'].".jpg";
		if(!file_exists('./public/main/reviews/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::reviews_url'].$image;
		$result['image'] = $image;
// inserted by Mike end
		
/*		// comments by Mike	
		if($result['image'])
		{
					$image=$_cfg['main::reviews_url'].$result['image'];

                	list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));
 		}



		if ($wid>=200 && $result['image'])
		{
		 	$image = $_cfg['main::reviews_url'].$result['image'];
		} else {
           	$poster=main_films::getFilmFirstPoster($result['film_id']);

           	if ($poster)
           	{
				$image=$_cfg['main::films_url'].$poster['image'];
           	} else {
         	$image = 'blank_news_img.jpg';
           	}
		}


		$result['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=6';
*/
	$result['text'] = ereg_replace("<([^>]*)(class|lang|style|size|face)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>","<\\1>",$result['text']);

	$result['text'] = str_replace("<div>&nbsp;</div>","",$result['text']);
	$result['text'] = str_replace('<div >&nbsp;</div>',"",$result['text']);

	$result['text'] = str_replace("<p>&nbsp;</p>","",$result['text']);


	if (substr_count($result['text'],'<p >')>0 || substr_count($result['text'],'<p>')>0)
	{
		$change='p';
	} else {
		$change='div';
	}

	$result['text'] = str_replace("<br><br>","<br>",$result['text']);
	$result['text'] = str_replace("<br /><br />","<br>",$result['text']);
	$result['text'] = str_replace("<br/><br/>","<br>",$result['text']);

	$result['text'] = str_replace("<br>","</".$change."><".$change.">",$result['text']);
	$result['text'] = str_replace("<br />","</".$change."><".$change.">",$result['text']);
	$result['text'] = str_replace("<br/>","</".$change."><".$change.">",$result['text']);

	$result['date'] = date('Y-m-d', strtotime($result['date']));
 	$result['date']=sys::russianDate($result['date']);
	$result['film_url']=main_films::getFilmUrl($result['film_id']);
	$result['spec_theme_url']=main_spec_themes::getArticleUrl($obj['spec_theme']);	
	$result['user_url']=main_users::getUserUrl($result['user_id']);
	$result['edit_url']=false;
	$result['delete_review']=false;

	$r=$_db->query("
		SELECT `review_id` FROM `#__main_reviews_thanks`
		WHERE `review_id`='".intval($_GET['review_id'])."'
		AND `user_id`='".$_user['id']."'
	");

	list($result['is_thank'])=$_db->fetchArray($r);

	if(sys::checkAccess('main::reviews_edit') || $result['user_id']==$_user['id'])
		$result['edit_url']=main_reviews::getReviewEditUrl($_GET['review_id']);

	if(sys::checkAccess('main::reviews_delete') || $result['user_id']==$_user['id'])
		$result['delete_review']=true;

	main::countShow($_GET['review_id'],'review');
	return $result;
}
?>