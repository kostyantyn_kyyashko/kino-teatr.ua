<?
function main_cinema_photos()
{
	sys::useLib('main::cinemas');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('cinema_id','int');
	sys::filterGet('photo_id','int');
	sys::filterGet('page','int');



	if(!$_GET['cinema_id'] && !$_GET['photo_id'])
		return 404;

		$q="
			SELECT
				cnm.city_id,
				cnm.id,
				cnm.name,
				pht.cinema_id,
				pht.id AS `photo_id`,
				pht.image,
				pht.user_id,
				pht.shows,
				pht.order_number,
				pht.public,
				cnm_lng.title AS `cinema`,
				pht_lng.title AS `photo`,
				cty_lng.title AS `city`
				
			FROM `#__main_cinemas_photos` AS `pht`

			LEFT JOIN `#__main_cinemas_lng` AS `cnm_lng`
			ON cnm_lng.record_id=pht.cinema_id
			AND cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_cinemas_photos_lng` AS `pht_lng`
			ON pht_lng.record_id=pht.id
			AND pht_lng.lang_id=".intval($_cfg['sys::lang_id'])."

			LEFT JOIN `#__main_cinemas` AS `cnm`
			ON pht.cinema_id=cnm.id

			LEFT JOIN `#__main_countries_cities_lng` AS `cty_lng`
			ON cty_lng.record_id=cnm.city_id
			AND cty_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		";

	if($_GET['photo_id'])
	{
		$q.="
			WHERE pht.id=".intval($_GET['photo_id'])."
		";
	}
	elseif($_GET['cinema_id'])
	{
		if($_GET['page'])
			$limit=intval($_cfg['main::images_on_page']*(intval($_GET['page'])-1));
		else
			$limit=0;
		$q.="
			WHERE pht.cinema_id='".intval($_GET['cinema_id'])."'
			AND pht.public=1
			ORDER BY pht.order_number ASC
			LIMIT ".$limit.",1
		";
	}
//if(sys::isDebugIP()) $_db->printR($q);
	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);

	$cinema_url = sys::getHumanUrl($result['id'],$result['name'],"cinema-photos");
	
	if(!$result)
		return 404;

	if(!$result['public'] && !sys::checkAccess('main::cinemas_photos_confirm'))
		return 404;

	define('_CANONIC', main_cinemas::getCinemaPhotosUrl(intval($result['cinema_id'])));

	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::cinemas_photos_delete') || $result['user_id']==$_user['id'])
		{
			main_cinemas::deletePhoto($result['photo_id']);
			sys::redirect(main_cinemas::getCinemaPhotosUrl($result['cinema_id']),false);
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::cinemas_photos_confirm'))
	{
		main_cinemas::confirmPhoto($result['photo_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}

	if($_cookie)
	{
		$_db->query("
			UPDATE `#__main_cinemas_photos`
			SET `shows`=(`shows`+1)
			WHERE `id`=".doubleval($result['photo_id'])."
		");
	}
	$meta['cinema']=$result['cinema'];
	$meta['photo']=$result['photo'];

		if($result['image'])
			$image=$_cfg['main::cinemas_url'].$result['image'];
		else
         	$image = 'blank_news_img.jpg';


		$result['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=10';

	$result['next_url']=false;
	$result['prev_url']=false;
	$result['delete_photo']=false;
	$result['edit_url']=false;
	$result['add_url']=false;
	$result['url']=main_cinemas::getPhotoUrl($result['photo_id']);
	$result['alt']=$result['cinema'];

	$result['title'] = sys::translate('main::photos').' - '.sys::translate('main::cinema').' '.$result['cinema'] ;
	$result['meta']=sys::parseModTpl('main::cinema_photos','page',$meta);
//if(sys::isDebugIP()) $_db->printR($result);
	if($next_id=main_cinemas::getNextPhotoId($result['cinema_id'],$result['order_number']))
		$result['next_url']=main_cinemas::getPhotosUrl($next_id);
	if($prev_id=main_cinemas::getPrevPhotoId($result['cinema_id'],$result['order_number']))
		$result['prev_url']=main_cinemas::getPhotosUrl($prev_id);

	if(sys::checkAccess('main::cinemas_photos_edit') || $result['user_id']==$_user['id'])
		$result['edit_url']=main_cinemas::getPhotoEditUrl($result['photo_id']);

	if(sys::checkAccess('main::cinemas_photos_add'))
		$result['add_url']=main_cinemas::getPhotoAddUrl($result['cinema_id']);

	if(sys::checkAccess('main::cinemas_photos_delete') || $result['user_id']==$_user['id'])
		$result['delete_photo']=true;
	//=============================================================================

	//Получить инфу об остальных фотках-----------------------------------------------
	$q="
		SELECT 
			pht.`id`,
			pht.`image`,				
			pht_lng.title AS `photo`
			
		FROM `#__main_cinemas_photos` as `pht`
		
		LEFT JOIN `#__main_cinemas_photos_lng` AS `pht_lng`
		ON pht_lng.record_id=pht.id
		AND pht_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			
		WHERE `cinema_id`=".$result['cinema_id']."
		AND `public`=1
		
		ORDER BY `order_number` ASC
	";
//	if(sys::isDebugIP()) $_db->printR($q);
	$result['pages']=sys_pages::pocess($q,$_cfg['main::images_on_page'],false,'?mod=main&act=cinema_photos&cinema_id='.$result['cinema_id'].'&page=%page%');
	$r=$_db->query($q.$result['pages']['limit']);
	$result['previews']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		if($obj['image'])
			$image=$_cfg['main::cinemas_url'].$obj['image'];
		else
         	$image = 'blank_news_img.jpg';


		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=8';
//		$obj['url']=main_cinemas::getPhotosUrl($obj['id'],false);
		$obj['url']=$cinema_url."?photo_id=".$obj['id'];
		if($_GET['page'])
			$obj['url'].='&page='.$_GET['page'];
		//$obj['url']=sys::rewriteUrl($obj['url']);
		$result['previews'][]=$obj;
	}
	//============================================================

	return $result;
}
?>