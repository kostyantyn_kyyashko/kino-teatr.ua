<?
function main_film_discuss()
{
	sys::useLib('main::films');
	sys::useLib('main::discuss');
	sys::useLib('main::users');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');

	define('_CANONICAL',main_films::getFilmDiscussUrl($_GET['film_id']),true);

	if(!$_GET['film_id'])
		return 404;



	$result['film']=$_db->getValue('main_films','title',intval($_GET['film_id']),true);
	$meta['film']=$result['film'];
	$result['title'] = sys::translate('main::discuss_film').' '.$result['film'];
	$result['meta']=sys::parseModTpl('main::film_discuss','page',$meta);
	if(!$result['film'])
		return 404;

	main::countShow($_GET['film_id'],'film');

	sys::jsInclude('sys::bb');
	return $result;
}
?>