<?
function main_ajx_vote_comment()
{
	sys::setTpl();
	sys::useLib('main::discuss');
	global $_user, $_db, $_cfg;

	if(!$_POST['mark'] || $_user['id']==2)
		return false;

	echo json_encode(main_discuss::MarkComment($_POST['type'],$_POST['mark'],$_POST['uid']));

}
?>