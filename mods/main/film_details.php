<?
function main_film_details()
{
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('film_id','int');
	
	if(!$_GET['film_id'])
		return 404;
		
	
		
	$r=$_db->query("
		SELECT 
			flm_lng.title,
			flm_lng.text
		FROM `#__main_films_lng` AS `flm_lng`
		WHERE flm_lng.record_id=".intval($_GET['film_id'])."
		AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
	");
	
	$result=$_db->fetchAssoc($r);
	
	if(!$result)
		return 404;
		
	main::countShow($_GET['film_id'],'film');

	$meta['film']=$result['title'];
	$result['meta']=sys::parseModTpl('main::film_details','page',$meta);
	$result['title']=htmlspecialchars($result['title']);
	$result['back_url']=main_films::getFilmUrl($_GET['film_id']);

	return $result;
}
?>