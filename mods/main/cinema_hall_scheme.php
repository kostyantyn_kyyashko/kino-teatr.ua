<?
function main_cinema_hall_scheme()
{
	sys::useLib('main::cinemas');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('hall_id','int');
	
	if(!$_GET['hall_id'])
		return 404;

	$q="
		SELECT 
			hls.scheme,
			hls_lng.title AS `hall`,
			cnm_lng.title AS `cinema`,
			city_lng.title AS `city`
		FROM 
			`#__main_cinemas_halls` AS `hls`
			
		LEFT JOIN 
            `#__main_cinemas` as `cnm`
        ON
            `cnm`.`id`=`hls`.`cinema_id`

		LEFT JOIN 
            `#__main_countries_cities_lng` as `city_lng`
        ON
            `city_lng`.`record_id`=`cnm`.`city_id`
		AND
			city_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN
			`#__main_cinemas_halls_lng` AS `hls_lng`
		ON 
			hls_lng.record_id=hls.id
		AND
			hls_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			
		LEFT JOIN
			`#__main_cinemas_lng` AS `cnm_lng`
		ON
			cnm_lng.record_id=hls.cinema_id
		AND
			cnm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			
		WHERE
			hls.id=".intval($_GET['hall_id'])."
	";
	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);
	
	if(!$result)
		return 404;
		
	$meta['cinema']=$result['cinema'];	
	$meta['hall']=$result['hall'];
	$result['meta']=sys::parseModTpl('main::cinema_hall_scheme','page',$meta);
	
	$result['image']=$_cfg['main::cinemas_url'].$result['scheme'];
	
	return $result;
}
?>