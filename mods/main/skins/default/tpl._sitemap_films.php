<?sys::setTpl('main::__xml')?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?foreach ($var['cinemas'] as $cinema):?>
	<url>
		<lastmod><?=$cinema['lastmod']?></lastmod>
		<loc><?=$cinema['loc']?></loc>
		<priority>1</priority>
		<changefreq>daily</changefreq>
	</url>
<?endforeach;?>
</urlset>

