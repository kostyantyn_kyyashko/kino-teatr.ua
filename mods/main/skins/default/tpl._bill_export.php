<?sys::setTpl('main::__xml')?>
<billboard>
	<genres>
		<?foreach ($var['genres'] as $obj):?>
			<genre id="<?=$obj['id']?>"><?=$obj['title']?></genre>
		<?endforeach;?>
	</genres>
	<professions>
		<?foreach ($var['professions'] as $obj):?>
			<profession id="<?=$obj['id']?>"><?=$obj['title']?></profession>
		<?endforeach;?>
	</professions>
	<countries>
		<?foreach ($var['countries'] as $obj):?>
			<country id="<?=$obj['id']?>"><?=$obj['title']?></country>
		<?endforeach;?>
	</countries>
	<cities>
		<?foreach ($var['cities'] as $obj):?>
			<city id="<?=$obj['id']?>" country_id="<?=$obj['country_id']?>"><?=$obj['title']?></city>
		<?endforeach;?>
	</cities>
	<studios>
		<?foreach ($var['studios'] as $obj):?>
			<studio id="<?=$obj['id']?>"><?=$obj['title']?></studio>
		<?endforeach;?>
	</studios>
	<shows>
		<?foreach ($var['shows'] as $show):?>
			<show id="<?=$show['id']?>" film_id="<?=$show['film_id']?>" cinema_id="<?=$show['cinema_id']?>" hall_id="<?=$show['hall_id']?>">
				<begin><?=$show['begin']?></begin>
				<end><?=$show['end']?></end>
				<times>
					<?foreach ($show['times'] as $time):?>
						<time time="<?=$time['time']?>" stereo="<?=$time['3D']?>">
							<prices><?=$time['prices']?></prices>
							<note><![CDATA[<?=$time['note']?>]]></note>
							<?if($time['sale_status']!='' and $time['sale_id']==0)
							  {							  	$temp_buy_vivod='NO';
							  	$temp_buy=explode('|',$time['sale_status']);
								if($temp_buy[0]=='imax' and $temp_buy[1]!='')
								{
									$temp_buy_vivod=htmlspecialchars($temp_buy[1].'/?&utm_source=kino-teatr.ua&utm_medium=cpc&utm_campaign=Agent_Kinoteatr');
								}
								if($temp_buy[0]=='mult' and $temp_buy[1]!='')
								{
									$temp_buy_vivod=htmlspecialchars($temp_buy[1].'/?utm_source=kinoteatr&utm_medium=cpc&utm_campaign=schedule');
								}							  }?>
							<buy><?=$temp_buy_vivod?></buy>
						</time>
					<?endforeach;?>
				</times>
			</show>
		<?endforeach;?>
	</shows>

	<films>
		<?foreach ($var['films'] as $film):?>
			<film id="<?=$film['id']?>" stereo="<?=$film['3d']?>">
				<title orig="<?=$film['title_orig']?>"><?=$film['title']?></title>
				<duration><?=$film['duration']?></duration>
				<year><?=$film['year']?></year>
<?if($film['ukraine_premiere']!='0000-00-00'):?>
				<premiere><?=$film['ukraine_premiere']?></premiere>
<?endif;?>
<?if($film['world_premiere']!='0000-00-00'):?>
				<worldpremiere><?=$film['world_premiere']?></worldpremiere>
<?endif;?>
				<age_limit><?=$film['age_limit']?></age_limit>
				<age-restricted><?=$film['age_limit']?></age-restricted>
				<budget currency="<?=$film['budget_currency']?>"><?=$film['budget']?></budget>
				<source_url><?=$film['source_url']?></source_url>
				<intro><![CDATA[<?=$film['intro']?>]]></intro>
				<text></text>
				<rating votes="<?=$film['votes']?>"><?=$film['rating']?></rating>
				<pre_rating pre_votes="<?=$film['pre_votes']?>"><?=$film['pre_rating']?></pre_rating>
				<pro_rating pro_votes="<?=$film['pro_votes']?>"><?=$film['pro_rating']?></pro_rating>
				<photos>
					<?foreach ($film['photos'] as $photo):?>
						<photo src="<?=$photo['src']?>"/>
					<?endforeach;?>
				</photos>
				<trailers>
					<?foreach ($film['trailers'] as $trailer):?>
						<trailer src="<?=$trailer['url']?>" language="<?=$trailer['language']?>"/>
					<?endforeach;?>
				</trailers>
				<posters>
					<?foreach ($film['posters'] as $poster):?>
						<poster order="<?=$poster['order_number']?>" src="<?=$poster['src']?>"/>
					<?endforeach;?>
				</posters>
				<persons>
					<?foreach ($film['persons'] as $person):?>
						<person id="<?=$person['person_id']?>" profession_id="<?=$person['profession_id']?>"  role="<?=$person['role']?>"/>
					<?endforeach;?>
				</persons>
				<genres><?=implode(',',$film['genres'])?></genres>
				<countries><?=implode(',',$film['countries'])?></countries>
				<studios><?=implode(',',$film['studios'])?></studios>
				<reviews>
					<?foreach ($film['reviews'] as $review):?>
						<review url="<?=$review['url']?>"><?=$review['title']?></review>
					<?endforeach;?>
				</reviews>
				<links>
					<?foreach ($film['links'] as $link):?>
						<link url="<?=$link['url']?>"><?=$link['title']?></link>
					<?endforeach;?>
				</links>
			</film>
		<?endforeach;?>
	</films>
	<cinemas>
		<?foreach ($var['cinemas'] as $cinema):?>
			<cinema id="<?=$cinema['id']?>" city_id="<?=$cinema['city_id']?>">
				<title><?=$cinema['title']?></title>
				<address><?=$cinema['address']?></address>
				<phone><?=$cinema['phone']?></phone>
				<site><?=$cinema['site']?></site>
				<text><![CDATA[<?=$cinema['text']?>]]></text>
				<photos>
					<?foreach ($cinema['photos'] as $photo):?>
						<photo src="<?=$photo['src']?>"/>
					<?endforeach;?>
				</photos>
				<halls>
					<?foreach ($cinema['halls'] as $hall):?>
						<hall id="<?=$hall['id']?>" stereo="<?=$hall['3d']?>">
							<title><?=$hall['title']?></title>
							<scheme><?=$hall['scheme']?></scheme>
						</hall>
					<?endforeach;?>
				</halls>
			</cinema>
		<?endforeach;?>
	</cinemas>
	<persons>
		<?foreach ($var['persons'] as $person):?>
			<person id="<?=$person['id']?>">
				<lastname orig="<?=$person['lastname_orig']?>"><?=$person['lastname']?></lastname>
				<firstname orig="<?=$person['firstname_orig']?>"><?=$person['firstname']?></firstname>
				<url><?=$person['url']?></url>
			</person>
		<?endforeach;?>
	</persons>
</billboard>
