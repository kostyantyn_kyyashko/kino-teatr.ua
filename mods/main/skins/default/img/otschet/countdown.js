function countdown(sDestinationDate)
{
var cd = this;

cd.clock 			= document.getElementById("clock");		// ������� "����"
cd.destinationDate	= new Date(sDestinationDate);				// ���� � �������

cd.refreshClock = function()
{
var now 			= new Date();									// ������� ������ �������
var totalRemains 	= (cd.destinationDate.getTime()-now.getTime());		// ������� ������� �� ������� � �������

	if (totalRemains>1)									// ���� ��� �������� ����� �������
	{
	var RemainsSec		= (parseInt(totalRemains/1000));		// ���-�� ������ (������� �� 1000 ��.)
	var RemainsFullDays	= (parseInt(RemainsSec/(24*60*60)));	// ���-�� ���� (������� �� 24�. �� 60 ���. �� 60 ���.), � ������� ����
		if (RemainsFullDays<10){RemainsFullDays="0"+RemainsFullDays};
		
	var secInLastDay	= RemainsSec-RemainsFullDays*24*3600;	// ���-�� ������ ���������� ���
	
													// ���-�� �����, � ������� ����
	var RemainsFullHours= (parseInt(secInLastDay/3600));		if (RemainsFullHours<10){RemainsFullHours="0"+RemainsFullHours};
	var secInLastHour	= secInLastDay-RemainsFullHours*3600;	// ���-�� ������ � ��������� ����
	
													// ���-�� �����, � ������� ����
	var RemainsMinutes	= (parseInt(secInLastHour/60));		if (RemainsMinutes<10){RemainsMinutes="0"+RemainsMinutes};
													// ���-�� ���������� ������, � ������� ����
	var lastSec		= secInLastHour-RemainsMinutes*60;		if (lastSec<10){lastSec="0"+lastSec};
	
	cd.clock.innerHTML = RemainsFullDays	+"<img src='http://kino-teatr.ua/mods/main/skins/default/img/otschet/strut.gif' class='strut'>"				// ��������� ����, ������� � ������� "����"
					+RemainsFullHours	+"<img src='http://kino-teatr.ua/mods/main/skins/default/img/otschet/strut.gif' class='strut'>"
					+RemainsMinutes	+"&nbsp;"
					+"<i>"+lastSec+"</i>";
	} 
	else 
	{
	cd.clock.innerHTML = "";								// ������ ���� �� ��������
	clearInterval(cd.timer);								// ����. ������
	}
} // end refreshClock

cd.refreshClock();										// �������� ���� ��� �������� ��������
cd.timer = setInterval(cd.refreshClock, 1000);				// ���������� ������ ���������� �����, ������ 1 ���. (1000��.)

} 