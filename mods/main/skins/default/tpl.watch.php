
				<div id='watchInCinema'>
					<span id='whatTo'><?=sys::translate('main::whatTo')?></span>
					<span id='seeInCinema'><?=sys::translate('main::watchAtCinemas')?></span>

					<form action='<?=$var['url']?>' method='POST'>
					<select id='watchSelect' name='date'>
						<?foreach ($var['day'] as $obj):?>
								<option value='<?=$obj["day"]?>'><?=$obj["text"]?></option>
						<?endforeach;?>
					</select>
					<input type='hidden' name='city_id' value='<?=$_cfg['main::city_id']?>'>

					<input type='submit' value='<?=sys::translate('main::yesLabel')?>!' class='yesButton'>
					</form>
				</div>
