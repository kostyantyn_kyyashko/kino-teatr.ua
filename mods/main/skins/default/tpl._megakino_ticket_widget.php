<?php
	function modify_barcode($barcode){
		$counter=1;
		$barcode=str_split($barcode);
		$str='';
		foreach($barcode as $letter){
			$str.=$letter;
			if($counter=='4' || $counter=='8')
				$str.='-';
			$counter++;
		}
		
		return $str;
	}
	
	if(isset($var['error'])){
		echo '<div>'.$var['error'].'</div>';
		return;
	}
	
	
?>

<SCRIPT Language="Javascript">
function printit(){ 
if (window.print) {
window.print() ; 
} else {
var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
WebBrowser1.ExecWB(6, 2);//Use a 1 vs. a 2 for a prompting dialog box WebBrowser1.outerHTML = ""; 
}
}
</script>

<div>
	
<table>
<tbody><tr>
   		<td style="font:bold 12px/20px Arial, Helvetica, sans-serif; color:#b82d04; padding:0 0 12px;">
   			
<table style="margin:0; padding:0" width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody><tr>
		<td style="font: bold 22px/24px Arial, Helvetica, sans-serif; color:#0e5b00; padding:0; margin: 0; text-align: center;">
							﷐Квитки оплачені			        </td>
    </tr>
</tbody></table>

   		</td>
   	</tr>
    
    <tr>
   		<td style="padding: 0;">
   			   		</td>
   	</tr>
    
       		   	    
    
    <tr>
   		<td style="padding:0 0 24px;">
   			
<table width="615" cellspacing="0" cellpadding="0" bgcolor="#fff" align="left">
	<tbody><tr>
		<td style="padding:0">
			<table width="615" cellspacing="0" cellpadding="0">
				<tbody><tr>
					<td style="padding:9px 0 0 0;" width="10"></td>
										<td style="padding:9px 0 0 0;" width="166" valign="top">
						<img src="<?=$var['poster']?>" style="vertical-align:top;" alt="">
					</td>
										<td style="padding:9px 0 0 0; vertical-align:top;" width="409">
						<table width="409" cellspacing="0" cellpadding="0">
							<tbody><tr>
								<td style="font:20px/22px Arial, Helvetica, sans-serif; color:#1b1513; padding:0 0 7px 0;">
									<?=$var['film_name']?>																			<span style="display: inline-block; ">
											/ <?php if($var['3d']) echo '3D'; else echo '2D';?>
										</span>
																	</td>
							</tr>
														<tr>
								<td style="font: bold 14px/16px Arial, Helvetica, sans-serif; color:#f93e00; padding:0 0 10px 0;">
									Обмеження за віком:
									<?=$var['age_limit']?>+
								</td>
							</tr>
														<tr>
								<td style="font: bold 20px/22px Arial, Helvetica, sans-serif;padding:0 0 10px 0;">
									 <?=$var['text_data']?>, <?=$var['time_show']?>
									<a href="<?=$var['link_profile']?>"><img src="https://bilet.vkino.com.ua/i/email/external-link-icon.png"></a>
								</td>
							</tr>
							<tr>
								<td style="font: 16px/18px Arial, Helvetica, sans-serif; color:#352f2c; padding:0 0 14px 0;">
									Кинотеатр: <?=$var['cinema_name']?>							</td>
							</tr>
							<?php if($var['hall'] == 'Зал' || $var['hall'] == 'зал'):?>
							
							<?php else:?>
								<tr>
									<td style="font: 16px/18px Arial, Helvetica, sans-serif; color:#352f2c; padding:0 0 14px 0;">
										Зал: <?=$var['hall']?>							</td>
								</tr>
							<?php endif;?>
							<tr>
								
							</tr>
							<tr>
								<td style="font: 16px/18px Arial, Helvetica, sans-serif; color:#352f2c; padding:0 0 2px 0 ;">
									місто <?=$var['city_name']?>								</td>
							</tr>
							

							<tr>
								<td style="font:13px/15px Arial, Helvetica, sans-serif; color:#352f2c; padding:0 0 12px 0;">
									<?=$var['address']?>
																			<br>
										<?=$var['phone']?>
									
																			
																
									
								</td>
							</tr>
							                                                                                      
						</tbody></table>
					</td>
				</tr>
			</tbody></table>

		</td>
	</tr>
</tbody></table>
   		</td>
   	</tr>

   	<tr>
   		<td style="padding: 0 0 12px;">

            <table width="615" cellspacing="0" cellpadding="0" bgcolor="#fff" align="left">
                <tbody><tr>
                    <td style="text-align: left">
                           
      <a target="_blank" href="/orders/4356304/?print=y" class="button action print-tickets" style="color: #fff; text-decoration: none">Надрукувати</a>

                    </td>
                    <td style="text-align: right">
                                            </td>
                </tr>
            </tbody></table>
            
        </td>
    </tr>
	<?php $counter=1;?>

	<?php foreach($var['tickets'] as $ticket):?>
    <tr>
   		<td style="padding: 0;">
   			
<table width="615" cellspacing="0" cellpadding="0" bgcolor="#fff" align="left">
		<tbody><tr>
		<td style="font: bold 14px/18px Arial, Helvetica, sans-serif; color:#352f2c; padding:10px 0 0 2px; margin: 0;">
				Квиток <?=$counter?> из <?=count($var['tickets'])?>		</td>
	</tr>
		<tr>
		<td style="padding: 4px 0;  border-bottom: 4px solid #000; ">
			
				<table width="100%">
					<tbody><tr>
						<td style="font: 20px/22px Arial;font-weight: bold; color: #000; ">
															КОД: <?=modify_barcode($ticket['barcode'])?>
													</td>
						
						<td style="padding: 0;" align="right">
                            <div style="font: bold 14px/14px Arial, Helvetica, sans-serif; color: #000; text-transform: uppercase">
    							Електронний квиток                            </div>

                                                            <div style="font: 14px/14px Arial, Helvetica, sans-serif; color: #000;">
                                    дійсний до <?=$var['arr']['expireAt']?>                           </div>
                            
						</td>
					</tr>
				</tbody></table>
			
		</td>
	</tr>
	<tr>
		<td style="padding:12px 0 16px; ">
			
			<table width="100%" cellspacing="0" cellpadding="0">
				<tbody><tr>
											<td width="100">
							<table style="border: 0px solid green;" width="100%" cellspacing="0" cellpadding="0">
								<tbody><tr>
									<td style="padding: 0 6px 9px 12px; font: bold 24px/24px Arial, Helvetica, sans-serif; color:#352f2c;" width="40" valign="bottom" align="right">
										<?=$ticket['rowNumber']?>									</td>
			
									<td style="padding:0 0 9px 0; font:12px/16px Arial, Helvetica, sans-serif; color:#352f2c;" valign="bottom">
										ряд									</td>
								</tr>
								<tr>
									<td style="padding: 0 6px 9px 12px; font: bold 24px/24px Arial, Helvetica, sans-serif; color:#352f2c;" width="30" valign="bottom" align="right">
										<?=$ticket['placeNumber']?>									</td>
				
									<td style="padding:0 0 9px 0; font:12px/14px Arial, Helvetica, sans-serif; color:#352f2c;" valign="bottom">
										місце									</td>
								</tr>
							</tbody></table>
											
					
											</td><td style="padding:0 0 9px 0; font:14px/18px Arial, Helvetica, sans-serif; color:#352f2c;">
						  <!--img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGkAAABpAQAAAAAR+TCXAAAAy0lEQVQ4ja2UvQ2FMAyEjyplRsgmsBgSBYvxNmEEShcRxzm8H702xkWUr7F8vksAVeHrmP2yIIKJrCUJC3kEccNc0pZrwfAEqun8EErqE+h6Mej4ye/E5kL6N6UT6ZX4rhBizBWUaCnPMbShNczeXnpjKC9tzCvN9UaQJ5Zd4YduGjKCygaUjbzumO6odOPtgprSRsSwZcNjQUy+wwC2xHLTkNpfEP0d+Xy7tc5RTKdP6nrDaDKAH1P6sf2ECivtu7pOvF+3hoS7GsEL7V2whYVi7N0AAAAASUVORK5CYII="-->
						  <img src="http://<?=$_SERVER['HTTP_HOST']?>/barcode.php?barcode=<?=$ticket['barcode']?>">
						</td>

						                
					<td style="padding: 0 0 10px 10px; font:14px/18px Arial, Helvetica, sans-serif; ">
						Ціна:
						
						<?=sprintf("%01.2f",$ticket['price']/100)?> грн
						<br>
						
						Сервісний збір:
						<?=sprintf("%01.2f",round($ticket['price']/100)*0.03)?> грн
	
												
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>
<?$counter++?>

</td>
</tr>
	<?php endforeach;?>

   	<tr>
   		<td style="padding:0 0 24px;">
   			
<table width="615" cellspacing="0" cellpadding="0" bgcolor="#fff" align="">
		<tbody><tr>
		<td style="padding: 0;">
			<ul style="padding: 0 0 0 20px; margin: 0; list-style-type: square;">
				<li style="padding: 0; font: 16px/20px Arial, Helvetica, sans-serif;">
					Маєш смартфон або планшет? На вході в зал пред'яви з екрану коди квитків.				</li>
                <li style="padding: 0 0 12px; font: 16px/20px Arial, Helvetica, sans-serif;">
					Немає мобільного пристрою, але є принтер? Роздрукуй цю сторінку і пред'яви на контролі перед входом в зал. Графічні коди повинні бути добре видно, без смуг та пропусків.				</li>
                <li style="padding: 0 0 12px; font: 16px/20px Arial, Helvetica, sans-serif;">
					Якщо все зазначене не підходить.					Електронні квитки необхідно обміняти в касі до початку сеансу.					Покажіть або назвіть касиру коди квитків з цієї сторінки.				</li>
			</ul>
			
		</td>
	</tr>
</tbody></table>

   		</td>
   	</tr>
	
    
    <tr>
        <td style="padding:0 0 24px;">
            <table style="margin:0; padding:0" width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody><tr>
		<td style="font: bold 22px/24px Arial, Helvetica, sans-serif; color:#352f2c; padding:0; margin: 0;">
            Оплати по замовленню        </td>
    </tr>
	<tr>
		<td style="font: bold 14px/22px Arial, Helvetica, sans-serif; color:#352f2c; padding:0 0 16px; margin: 0;">
			<b>Якщо у списку немає вашого платежу зверніться до служби підтримки.</b>
        </td>
    </tr>
			<tr>
			<td style="padding:0 0 20px; margin: 0;">
				
				<table style="margin:0; padding:0" width="100%" cellspacing="0" cellpadding="0" border="0">
					<tbody><tr>
						<td style="font: 14px/22px Arial, Helvetica, sans-serif; color:#000; padding: 0; margin: 0; background-color: #2ca02c;  width: 10px">
							&nbsp;
						</td>

						<td style="font: 14px/22px Arial, Helvetica, sans-serif; color:#000; padding: 0 0 0 10px; margin: 0;">
							Рахунок №<?=$var['megakino_id']?> від <?=$var['data_buy']?>, сума <?=sprintf("%01.2f",$var['sum'])?> грн, засіб оплати «Платіжною карткою».							<br>
			
															Оплата отримана в <?=$var['data_buy']?>.								
													</td>
					</tr>

									</tbody></table>

			</td>
		</tr>
	</tbody></table>
        </td>
    </tr>
        
	<tr>
		<td>
			<table style="margin:0; padding:0 0 24px;" width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody><tr>
		<td style="font: bold 22px/24px Arial, Helvetica, sans-serif; color:#352f2c; padding:10px 0 0; margin: 0;">
            Повернення замовлення			        </td>
    </tr>
	<tr>
		<td style="font: bold 14px/22px Arial, Helvetica, sans-serif; color:#352f2c; padding:10px 0 16px; margin: 0;">
			<a href="https://vkino.ua/page/pravila#vozvrat">Правила повернення квитків</a>
        </td>
    </tr>
						<tr>
				<td style="font: 14px/22px Arial, Helvetica, sans-serif; color:#352f2c; padding:0 0 16px; margin: 0;">
					Це замовлення неможливо скасувати в автоматичному режимі. Ознайомтеся з правилами повернення і зверніться в службу підтримки.		        </td>
		    </tr>
	
			
</tbody></table>
	
		</td>
	</tr>
	<tr>
		<td style="font: bold 14px/22px Arial, Helvetica, sans-serif; color:#352f2c; padding:10px 0 16px; margin: 0;">
			<a href="javascript: void(0);" onclick="printit();">Друк</a>
        </td>
	</tr>
    <tr>
   		<td style="font:14px/20px Arial, Helvetica, sans-serif; text-align:left; padding: 0 0 12px;">
   			<span style="font-weight: bold;">Номер замовлення vkino.ua - 4356304. Зазначте його при зверненні до служби підтримки покупців.            </span>
            <span>
   			Цей номер не є квитком чи кодом квитка та не надає право проходу чи обміну.            </span>﷯
   		</td>
   	</tr>
</tbody>
</table>
</div>