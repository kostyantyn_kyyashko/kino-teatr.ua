<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=content::getEntryNodeId(19)?>
<?sys::setMeta($var['meta']);?>
<?//main::showGemius('0sibwqN2iMUk43dsvzmHo8SsTO5_HdByi1lhbtysaND.77');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

<div id="main_films">
	<?=main::showLetters('?mod=main&act=films');?>
	<br>
	<form method="GET" action="<?=sys::rewriteUrl('?mod=main&act=films&order_by='.$_GET['order_by'])?>">
		<table class="xForm x">
			<tr>
				<td style="text-align:right;"><?=$var['fields']['title']['title']?>:</td>
				<td><?=sys_form::parseField('title',$var['fields']['title'],'style="width:150px" maxlength="55"')?></td>
				<td><?=$var['fields']['genre_id']['title']?>: <?=sys_form::parseField('genre_id',$var['fields']['genre_id'])?></td>
				<td><?=$var['fields']['year']['title']?>:</td>
				<td><?=sys_form::parseField('year',$var['fields']['year'])?></td>
			</tr>
			<tr>
				<td style="text-align:right;"><?=$var['fields']['city_id']['title']?>:</td>
				<td><?=sys_form::parseField('city_id',$var['fields']['city_id'])?></td>
				<td><?=sys_form::parseField('show',$var['fields']['show'])?> <?=$var['fields']['show']['title']?></td>
				<td></td>
				<td>
					<input type="submit" class="xRed" value="<?=sys::translate('main::find')?>">
				</td>
			</tr>
		</table>
	</form>
	<br>
	<?if($var['objects']):?>
		<?=sys::parseTpl('main::pages',$var['pages'])?>
		<table class="xTable x">
			<tr class="xLinks2">
				<th rowspan="2" <?if($var['order_field']=='title'):?>style="font-weight:bold;"<?endif;?>>
					<nobr>
					<a href="<?=$var['title']['order_url']?>"><?=sys::translate('main::title')?></a>
					<?if($var['order_field']=='title'):?>
						<img src="<?=_IMG_URL?>arrow_<?=$var['order']?>.gif" title="<?=sys::translate('main::'.$var['order'])?>" alt="<?=sys::translate('main::'.$var['order'])?>">
					<?endif;?>
					</nobr>
				</th>
				<th rowspan="2"><?=sys::translate('main::country_es')?></th>
				<th rowspan="2" <?if($var['order_field']=='year'):?>style="font-weight:bold;"<?endif;?>>
					<nobr>
					<a href="<?=$var['year']['order_url']?>"><?=sys::translate('main::year')?></a>
					<?if($var['order_field']=='year'):?>
						<img src="<?=_IMG_URL?>arrow_<?=$var['order']?>.gif" title="<?=sys::translate('main::'.$var['order'])?>" alt="<?=sys::translate('main::'.$var['order'])?>">
					<?endif;?>
					</nobr>
				</th>
				<th rowspan="2"><?=sys::translate('main::genre_es')?></th>
				<th style="text-align:center;" colspan="2"><?=sys::translate('main::rating')?></th>
			</tr>
			<tr class="xLinks2">
				<th <?if($var['order_field']=='rating'):?>style="font-weight:bold;"<?endif;?>>
					<nobr>
					<a title="<?=sys::translate('main::users_rating')?>" href="<?=$var['rating']['order_url']?>"><?=sys::translate('main::usr')?></a>
					<?if($var['order_field']=='rating'):?>
						<img src="<?=_IMG_URL?>arrow_<?=$var['order']?>.gif" title="<?=sys::translate('main::'.$var['order'])?>" alt="<?=sys::translate('main::'.$var['order'])?>">
					<?endif;?>
					</nobr>
				</th>
				<th <?if($var['order_field']=='pro_rating'):?>style="font-weight:bold;"<?endif;?>>
					<nobr>
					<a title="<?=sys::translate('main::reviews_rating')?>" href="<?=$var['pro_rating']['order_url']?>"><?=sys::translate('main::rev')?></a>
					<?if($var['order_field']=='pro_rating'):?>
						<img src="<?=_IMG_URL?>arrow_<?=$var['order']?>.gif" title="<?=sys::translate('main::'.$var['order'])?>" alt="<?=sys::translate('main::'.$var['order'])?>">
					<?endif;?>
					</nobr>
				</th>
			</tr>
			<?foreach ($var['objects'] as $obj):?>
				<tr>
					<td><strong><a title="<?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot;" href="<?=$obj['url']?>"><?=$obj['title']?> / <?=$obj['title_orig']?></a></strong></td>
					<td><?=implode(', ',$obj['countries'])?></td>
					<td><?=$obj['year']?></td>
					<td><?=implode(' / ',$obj['genres'])?></td>
					<td><a title="<?=sys::translate('main::rating_of_film')?> &quot;<?=$obj['title']?>&quot;" href="<?=$obj['rating_url']?>"><?=$obj['rating']?>/<?=$obj['votes']?></a></td>
					<td><a title="<?=sys::translate('main::reviews_on_film')?> &quot;<?=$obj['title']?>&quot;" href="<?=$obj['pro_rating_url']?>"><?=$obj['pro_rating']?>/<?=$obj['pro_votes']?></a></td>
				</tr>
			<?endforeach;?>
		</table>
		<?=sys::parseTpl('main::pages',$var['pages'])?>
	<?else:?>
		<center class="xErr"><?=sys::translate('main::nothing_found')?></center>
	<?endif?>
</div>