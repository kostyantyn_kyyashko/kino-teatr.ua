<?sys::useLib('content');?>
<?sys::setTpl('main::__print')?>
<?sys::setMeta($var['meta']);?>

<script type="text/javascript">
	function showDetails(obj, obj_id)
	{
		Short=document.getElementById('short_'+obj_id);
		Full=document.getElementById('full_'+obj_id);
		if(Full.style.display=='none')
		{
			Full.style.display='';
			Short.style.display='none';
			obj.innerHTML='<?=sys::translate('main::short')?>';
		}
		else
		{
			Full.style.display='none';
			Short.style.display='';
			obj.innerHTML='<?=sys::translate('main::details')?>';
		}
	}
	
	function hideObject(lnk, obj_id, obj_title)
	{
		obj=document.getElementById('object_'+obj_id);
		if(obj.style.display=='')
		{
			obj.style.display='none';
			lnk.innerHTML='<?=sys::translate('main::show2')?> '+obj_title;
		}
		else
		{
			obj.style.display='';
			lnk.innerHTML='<?=sys::translate('main::hide')?> '+obj_title;
		}
	}
	
	function printBill(obj)
	{
		for(i=0; i<document.links.length; i++)
		{
			document.links[i].style.display='none';
		}
		obj.style.display='none';
		print();
		for(i=0; i<document.links.length; i++)
		{
			document.links[i].style.display='';
		}
		obj.style.display='';
	}
</script>
<div id="main_bill_print">	
	<h1><?=$var['meta']['title']?></h1>
	<?if($var['objects']):?>
		<?if($_GET['order']=='cinemas'):?>
			<?foreach ($var['objects'] as $obj):?>
				<a class="xLinks2 hide" href="#" onclick="hideObject(this, <?=$obj['id']?>,'<?=sys::translate('main::cinema')?> &quot;<?=$obj['title']?>&quot;'); return false;"><?=sys::translate('main::hide')?> <?=sys::translate('main::cinema')?> &quot;<?=$obj['title']?></a>
				<div id="object_<?=$obj['id']?>">
					<div class="object">
						<a style="float:right;" class="xLinks2" href="#" onclick="showDetails(this, <?=$obj['id']?>);return false;"><?=sys::translate('main::details')?></a>
						<h2><?=sys::translate('main::cinema')?> &quot;<?=$obj['title']?>&quot;</h2> (<?=$obj['phone']?>)
					</div>
					<table class="x xTable">
						<tbody id="short_<?=$obj['id']?>">
							<tr>
								<th style="width:200px;" class="x"><?=sys::translate('main::film')?></th>
								<th style="width:80px;" class="x"><?=sys::translate('main::hall')?></th>
								<th class="x"><?=sys::translate('main::shows')?></th>
								<th style="width:80px;" class="x"><?=sys::translate('main::prices')?></th>
							</tr>
							<?foreach ($obj['shows'] as $show):?>
								<tr>
									<td><?=$show['film']?></td>
									<td><?=$show['hall']?></td>
									<td><!-- 18:00; -->
										<?foreach ($show['times'] as $time):?>
											<?if($time['past']):?>
												<strike><?=$time['time']?>;</strike>
											<?else:?>
												<?=$time['time']?>;
											<?endif;?>
										<?endforeach;?>
									</td>
									<td><?=$show['price_range']?> <?=sys::translate('main::grn')?></td>
								</tr>
							<?endforeach;?>
						</tbody>
						<tbody style="display:none;" id="full_<?=$obj['id']?>">
						
							<?foreach ($obj['halls'] as $hall):?>
								<?if($hall['title']):?>
									<tr>
										<td class="x xCream" colspan="3">
											<b><?=$hall['title']?></b>
										</td>
									</tr>
								<?endif;?>
								<tr>
									<th style="width:1px" class="x"><?=sys::translate('main::showing')?></th>
									<th class="x"><?=sys::translate('main::prices')?></th>
									<th class="x"><?=sys::translate('main::note')?></th>
								</tr>
								<?foreach ($hall['shows'] as $show):?>
									<tr>
										<td colspan="3"><strong><?=$show['film']?></strong></td>
									</tr>
									<?foreach ($show['times'] as $time):?>
										<tr>
											<td class="x">
												<?if($time['past']):?>
													<strike><?=$time['time']?>;</strike>
												<?else:?>
													<?=$time['time']?>;
												<?endif;?>
											</td>
											<td class="x"><nobr><?=$time['prices']?> <?=sys::translate('main::grn')?></nobr></td>
											<td class="x"><?=$time['note']?>&nbsp;</td>
										</tr>
									<?endforeach;?>
								<?endforeach;?>
							<?endforeach;?>
						</tbody>
					</table>
				</div>
			<?endforeach;;?>
		<?else:?>
			<?foreach ($var['objects'] as $obj):?>
				<a class="xLinks2 hide" href="#" onclick="hideObject(this, <?=$obj['id']?>,'<?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot;'); return false;"><?=sys::translate('main::hide')?> <?=sys::translate('main::film')?> &quot;<?=$obj['title']?></a>
				<div id="object_<?=$obj['id']?>">
					<div class="object">
						<a style="float:right;" class="xLinks2" href="<?=$obj['shows_url']?>" onclick="showDetails(this, <?=$obj['id']?>); return false;"><?=sys::translate('main::details')?></a>
						<h2><?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot;</h2>
					</div>
					<table class="x xTable">
						<tbody id="short_<?=$obj['id']?>">
							<tr>
								<th style="width:200px;" class="x"><?=sys::translate('main::cinema')?></th>
								<th style="width:80px;" class="x"><?=sys::translate('main::hall')?></th>
								<th class="x"><?=sys::translate('main::shows')?></th>
								<th style="width:80px;" class="x"><?=sys::translate('main::prices')?></th>
							</tr>
							<?foreach ($obj['shows'] as $show):?>
								<tr>
									<td><?=$show['cinema']?></td>
									<td><?=$show['hall']?></td>
									<td>
										<?foreach ($show['times'] as $time):?>
											<?if($time['past']):?>
												<strike><?=$time['time']?>;</strike>
											<?else:?>
												<?=$time['time']?>;
											<?endif;?>
										<?endforeach;?>
									</td>
									<td><?=$show['price_range']?> <?=sys::translate('main::grn')?></td>
								</tr>
							<?endforeach;?>
						</tbody>
						<tbody style="display:none;" id="full_<?=$obj['id']?>">
						
							<?foreach ($obj['cinemas'] as $cinema):?>
								<tr>
									<td class="x xCream" colspan="3">
										<b><?=sys::translate('main::cinema')?> &quot;<?=$cinema['title']?>&quot;</b> (<?=$cinema['phone']?>)
									</td>
								</tr>
								<tr>
									<th style="width:1px" class="x"><?=sys::translate('main::showing')?></th>
									<th class="x"><?=sys::translate('main::prices')?></th>
									<th class="x"><?=sys::translate('main::note')?></th>
								</tr>
								<?foreach ($cinema['halls'] as $hall):?>
									<?if($hall['title']):?>
										<tr>
											<td colspan="3">
												<?if($hall['title']):?>
													<strong><?=$hall['title']?></strong>
												<?endif?>
											</td>
										</tr>
									<?endif?>
									<?foreach ($hall['shows'] as $show):?>
										<?foreach ($show['times'] as $time):?>
											<tr>
												<td class="x">
													<?if($time['past']):?>
														<strike><?=$time['time']?>;</strike>
													<?else:?>
														<?=$time['time']?>;
													<?endif;?>
												</td>
												<td class="x"><nobr><?=$time['prices']?> <?=sys::translate('main::grn')?></nobr></td>
												<td class="x"><?=$time['note']?>&nbsp;</td>
											</tr>
										<?endforeach;?>
									<?endforeach;?>
								<?endforeach;?>
							<?endforeach;?>
						</tbody>
					</table>
				</div>
			<?endforeach;;?>
		<?endif?>
		<input style="margin-top:10px;" type="button" value="<?=sys::translate('main::print')?>" onclick="printBill(this)">
	<?else:?>
		<center class="xErr"><?=sys::translate('main::no_shows')?></center>
	<?endif?>
</div>
