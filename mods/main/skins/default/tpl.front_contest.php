<?sys::useLib('main::boxw');?>
<?if (count($var['objects'])>0):?>

<div id='boxBlock'>

<div>
	<div id='boxBlockTitle'>
      <div style='display: inline-block; float: left;'>
         <?=sys::translate('main::contest')?><br>
      </div>

	<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

	</div>

	<div id='boxBlockTop'>&nbsp;</div>


	<?foreach ($var['objects'] as $obj):?>
	<div id='boxItem'>
  		<div id='boxPosition'><?=$obj['position']?>.</div>

		<div id='boxName'  class='contName'>
			<a href='<?=$obj['url']?>' title="<?=$obj['title']?>"><?=$obj['title']?></a>
			<?=$obj['title_orig']?>
		</div>

		<div id='contestRight'>
												<?if($obj['cd']):?>
									<img src='/images/cd.png' title='<?=sys::translate('main::cd')?>' alt='<?=sys::translate('main::cd')?>'>
									<?endif;?>
									<?if($obj['tickets']):?>
									<img src='/images/tickets.png' title='<?=sys::translate('main::tickets')?>' alt='<?=sys::translate('main::tickets')?>'>
									<?endif;?>
									<?if($obj['book']):?>
									<img src='/images/book.png' title='<?=sys::translate('main::book')?>' alt='<?=sys::translate('main::book')?>'>
									<?endif;?>
									<?if($obj['tshirt']):?>
									<img src='/images/tshirt.png' title='<?=sys::translate('main::tshirt')?>' alt='<?=sys::translate('main::tshirt')?>'>
									<?endif;?>
									<?if($obj['prizes']):?>
									<img src='/images/prizes.png' title='<?=sys::translate('main::prizes')?>' alt='<?=sys::translate('main::prizes')?>'>
									<?endif;?>

		</div>

		<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
	</div>
	<?endforeach;?>

</div>



</div>

<?endif;?>