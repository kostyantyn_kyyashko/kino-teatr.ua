<div class="main_media">
	<?if($var['trailer']):?>
		<div class="xTitle3"><h3 class="xTrailer"><?=sys::translate('main::trailers')?></h3></div>
		<a href="<?=$var['trailer']['url']?>"><img width="214" alt="<?=$var['trailer']['alt']?>" title="<?=$var['trailer']['alt']?>" src="<?=$var['trailer']['image']?>"></a>
	<?endif;?>

	<?if($var['photo']):?>
		<div class="xTitle3"><h3 class="xFrames"><?=sys::translate('main::frames')?></h3></div>
		<a href="<?=$var['photo']['url']?>"><img width="214" alt="<?=$var['photo']['alt']?>" title="<?=$var['photo']['alt']?>" src="<?=$var['photo']['image']?>"></a>
	<?endif;?>

	<?if($var['wallpaper']):?>
		<div class="xTitle3"><h3 class="xWallpaper"><?=sys::translate('main::wallpapers')?></h3></div>
		<a href="<?=$var['wallpaper']['url']?>"><img width="214" alt="<?=$var['wallpaper']['alt']?>" title="<?=$var['wallpaper']['alt']?>" src="<?=$var['wallpaper']['image']?>"></a>
	<?endif;?>
</div>

