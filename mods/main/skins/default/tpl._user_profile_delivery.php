<?sys::useLib('content');?>
<?$var['meta']['title']=$var['meta']['title'].' ('.$_user['main_nick_name'].')'?>
<?$var['meta']['meta_title']=$var['meta']['meta_title'].' ('.$_user['main_nick_name'].')'?>
<?$_var['main::title']=$var['meta']['title']?>
<?=sys::setMeta($var['meta']);?>
<?sys::useJs('sys::form');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

<div class='myriad' id='myriad'><?=$_var['main::title']?></div>





					<div id='lineHrWide'>&nbsp;</div>

		<?=main_users::showProfileTabs('delivery')?>

						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
<br /><br />



<div id="main_user_profile_films" class="user-profile-form">
	<form id="form" method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
		<input type="hidden" name="_task">
        <?if(sys::isDelivery()):?>
    		<?if($var['objects']):?>
    		<?=sys::parseTpl('main::pages',$var['pages'])?>
    		<table class="xTable x week">
    			<tr class="xLinks2">
    				<th style="width:20%">&nbsp;</th>
    				<th><label for="w1"><?=sys::translate('main::week1')?></label><br /><input class="xCheckbox check-all" type="checkbox" name="w1" id="w1" /></th>
    				<th><label for="w2"><?=sys::translate('main::week2')?></label><br /><input class="xCheckbox check-all" type="checkbox" name="w2" id="w2" /></th>
    				<th><label for="w3"><?=sys::translate('main::week3')?></label><br /><input class="xCheckbox check-all" type="checkbox" name="w3" id="w3" /></th>
    				<th><label for="w4"><?=sys::translate('main::week4')?></label><br /><input class="xCheckbox check-all" type="checkbox" name="w4" id="w4" /></th>
    				<th><label for="w5"><?=sys::translate('main::week5')?></label><br /><input class="xCheckbox check-all" type="checkbox" name="w5" id="w5" /></th>
    				<th><label for="w6"><?=sys::translate('main::week6')?></label><br /><input class="xCheckbox check-all" type="checkbox" name="w6" id="w6" /></th>
    				<th><label for="w7"><?=sys::translate('main::week7')?></label><br /><input class="xCheckbox check-all" type="checkbox" name="w7" id="w7" /></th>
    			</tr>
    			<?foreach ($var['objects'] as $obj): ?>
				<tr class="film">
                    <td class="delivery-name"><?=$obj['title']?></td>
                    <td><input class="xCheckbox w1 checkbox-item" data-week="1" type="checkbox"<?=$obj['sheduler']['w1']?> name="obj_<?=$obj['id']?>_w1" /></td>
                    <td><input class="xCheckbox w2 checkbox-item" data-week="2" type="checkbox"<?=$obj['sheduler']['w2']?> name="obj_<?=$obj['id']?>_w2" /></td>
                    <td><input class="xCheckbox w3 checkbox-item" data-week="3" type="checkbox"<?=$obj['sheduler']['w3']?> name="obj_<?=$obj['id']?>_w3" /></td>
                    <td><input class="xCheckbox w4 checkbox-item" data-week="4" type="checkbox"<?=$obj['sheduler']['w4']?> name="obj_<?=$obj['id']?>_w4" /></td>
                    <td><input class="xCheckbox w5 checkbox-item" data-week="5" type="checkbox"<?=$obj['sheduler']['w5']?> name="obj_<?=$obj['id']?>_w5" /></td>
                    <td><input class="xCheckbox w6 checkbox-item" data-week="6" type="checkbox"<?=$obj['sheduler']['w6']?> name="obj_<?=$obj['id']?>_w6" /></td>
                    <td><input class="xCheckbox w7 checkbox-item" data-week="7" type="checkbox"<?=$obj['sheduler']['w7']?> name="obj_<?=$obj['id']?>_w7" /></td>
				</tr>
    			<?endforeach;?>
    		</table>
    		<div>
    			<input type="submit"name="cmd_save" value="<?=sys::translate('main::save')?>" class="profileBtn" style="margin:40px auto 0 auto; cursor:pointer;" />
    		</div>
    		<?=sys::parseTpl('main::pages',$var['pages'])?>
    		<?endif;?>
        <?else:?>
            <center class="msg"><?=sys::translate('main::delivery_in_test_mode')?></center>
        <?endif;?>
	</form>
</div>
<script>
$(function(){
    $('input[type=checkbox].check-all').change(function(e){
        var item = $(e.currentTarget);
        if (item.attr('checked')) {
            $('input[type=checkbox].' + item.attr('name')).attr('checked', 'checked');
        } else {
            $('input[type=checkbox].' + item.attr('name')).removeAttr('checked');
        };
    });
    for (var i=1; i<=7; i++) {
        var items = $('input[type=checkbox].w' + i);
        var count = 0;
        for (var k=0; k<items.length; k++) {
            if ($(items[k]).attr('checked')) count++;
        };
        if (count == 8) {
            $('input[type=checkbox]#w' + i).attr('checked', 'checked');
        };
    };
    $('input[type=checkbox].checkbox-item').change(function(e){
        var item = $(e.currentTarget);
        var items = $('input[type=checkbox].w' + item.data('week'));
        var count = 0;
        for (var k=0; k<items.length; k++) {
            if ($(items[k]).attr('checked')) count++;
        };
        if (count == 8) {
            $('input[type=checkbox]#w' + item.data('week')).attr('checked', 'checked');
        } else {
            $('input[type=checkbox]#w' + item.data('week')).removeAttr('checked');
        };
    });
});
</script>
