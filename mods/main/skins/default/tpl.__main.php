<?include(_SKIN_DIR.'tpl.__header_main.php')?>
<?sys::useLib('main::films');?>
<?sys::useLib('main::anounce');?>
<?sys::useLib('main::contest');?>
<?sys::useLib('main::cinemas');?>
<?sys::useLib('main::reviews');?>
<?sys::useLib('main::box');?>
<?sys::useLib('main::news');?>
<?sys::useLib('main::articles');?>
<?sys::useLib('main::gossip');?>
<?sys::useLib('main::serials');?>
<?sys::useLib('main::interview');?>
<?sys::useLib('banners');?> 
<script>
	function loadContent(block)
	{
		if ($('#check'+block).val()=='0')
		{
			ajx_url = '?lang=<?=$_cfg['sys::lang']?>&mod=main&act=ajx_'+block;
			$('#ajax'+block).load(ajx_url, function()
			{
				$('#ajax'+block).ready( function()
					{
						$('#ajax'+block).css('background', 'none');
						$('#ajax'+block).css('height', 'auto');
					});
				$('#check'+block).val('1');
			 });
		}
	}

	function showLiders(block)
	{
		$('#weekPremiers').removeClass('active');
		$('#mostAwaiting').removeClass('active');
		$('#mostDiscussed').removeClass('active');

		$('#'+block).addClass('active');

		$('#weekPremiersBlock').css('display', 'none');
		$('#mostAwaitingBlock').css('display', 'none');
		$('#mostDiscussedBlock').css('display', 'none');

		$('#'+block+'Block').fadeIn('slow');
	}

	function showSerials(block)
	{
		$('#recentlySerials').removeClass('active');
		$('#popularSerials').removeClass('active');
		$('#discussedSerials').removeClass('active');

		$('#'+block).addClass('active');

		$('#recentlySerialsBlock').css('display', 'none');
		$('#popularSerialsBlock').css('display', 'none');
		$('#discussedSerialsBlock').css('display', 'none');

		$('#'+block+'Block').fadeIn('slow');
	}

	function showGossip(block)
	{
		$('#recentlyGossip').removeClass('active');
		$('#popularGossip').removeClass('active');
		$('#discussedGossip').removeClass('active');

		$('#'+block).addClass('active');

		$('#recentlyGossipBlock').css('display', 'none');
		$('#popularGossipBlock').css('display', 'none');
		$('#discussedGossipBlock').css('display', 'none');

		$('#'+block+'Block').fadeIn('slow');
	}

	function showInterview(block)
	{
		$('#recentlyInterview').removeClass('active');
		$('#popularInterview').removeClass('active');
		$('#discussedInterview').removeClass('active');

		$('#'+block).addClass('active');

		$('#recentlyInterviewBlock').css('display', 'none');
		$('#popularInterviewBlock').css('display', 'none');
		$('#discussedInterviewBlock').css('display', 'none');

		$('#'+block+'Block').fadeIn('slow');
	}

	function showArticles(block)
	{
		$('#recentlyArticles').removeClass('active');
		$('#popularArticles').removeClass('active');
		$('#discussedArticles').removeClass('active');

		$('#'+block).addClass('active');

		$('#recentlyArticlesBlock').css('display', 'none');
		$('#popularArticlesBlock').css('display', 'none');
		$('#discussedArticlesBlock').css('display', 'none');

		$('#'+block+'Block').fadeIn('slow');
	}

	function showNews(block)
	{
		$('#recentlyNews').removeClass('active');
		$('#popularNews').removeClass('active');
		$('#discussedNews').removeClass('active');

		$('#'+block).addClass('active');

		$('#recentlyNewsBlock').css('display', 'none');
		$('#popularNewsBlock').css('display', 'none');
		$('#discussedNewsBlock').css('display', 'none');

		$('#'+block+'Block').fadeIn('slow');
	}

	function showReviews(block)
	{
		$('#editorsReviews').removeClass('active');
		$('#usersReviews').removeClass('active');

		$('#'+block).addClass('active');

		$('#editorsReviewsBlock').css('display', 'none');
		$('#usersReviewsBlock').css('display', 'none');

		$('#'+block+'Block').fadeIn('slow');
	}
</script>
			<div id='mainContent'>
				<div id='leftContent'>
					<div id='mainAnounce'></div>
					<div id='liders'>
						<div id='blockHeader'>
       						<h3><?=sys::translate('main::films')?></h3>
       						<ul id='blockHeaderMenu' style='float: right;'>
              					<li class='active' id='weekPremiers'><a href='#' name='weekPremiers' onclick='showLiders(this.name); return false;'><span><?=sys::translate('main::nowInCinemas')?></span></a></li>
              					<li id='mostAwaiting'><a href='#' name='mostAwaiting' onclick='loadContent("Waited"); showLiders(this.name); return false;'><span><?=sys::translate('main::weekPremiers')?></span></a></li>
              					<li id='mostDiscussed'><a href='#' name='mostDiscussed' onclick='loadContent("Premiers"); showLiders(this.name); return false;'><span><?=sys::translate('main::mostAwaiting')?></span></a></li>
       						</ul>
						</div>
						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif'></div>
						<div class='blockLidersPremiers' id='weekPremiersBlock'>
							<?=main_films::showFrontFilmsList()?>
						</div>
						<input type='hidden' id='checkWaited' value='0'>
						<div class='blockLidersPremiers' id='mostAwaitingBlock' style='display: none;'>
						<div id='ajaxWaited'  style='min-height: 250px;'>
						</div>
						</div>
						<input type='hidden' id='checkPremiers' value='0'>
					<div class='blockLidersPremiers' id='mostDiscussedBlock' style='display: none;'>
						<div id='ajaxPremiers'  style='min-height: 250px;'>
							</div>
						</div>
					</div>
					<div id='lineHr'>&nbsp;</div>
					
					<div id='news_block'>
						<div id='blockHeader'>
       						<h3><?=sys::translate('main::news')?></h3>
       						<ul id='blockHeaderMenu'>
              					<li class='active' id='recentlyNews'><a href='#' name='recentlyNews' onclick='showNews(this.name); return false;'><span><?=sys::translate('main::recentNews')?></span></a></li>
              					<li id='popularNews'><a href='#' name='popularNews' onclick='loadContent("PopularNews");showNews(this.name); return false;'><span><?=sys::translate('main::popularNews')?></span></a></li>
              					<li id='discussedNews'><a href='#' name='discussedNews' onclick='loadContent("DiscussedNews");showNews(this.name); return false;'><span><?=sys::translate('main::discussedNews')?></span></a></li>
       						</ul>
       						<div id='blockHeaderHelpers'>
	       						<a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/news_rss.phtml' title='<?=sys::translate('main::rss_canal_news')?>' id='RSS'>RSS</a>
	       						<div id='allNews'><a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?><?endif;?>news.phtml'><?=sys::translate('main::allNews')?></a></div>
       						</div>
						</div>
						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
						<div id='recentlyNewsBlock'>
						<?=main_news::showLastArticles()?>
						</div>
						<div id='popularNewsBlock' style='display: none;'>
							<input type='hidden' id='checkPopularNews' value='0'>
							<div id='ajaxPopularNews'  style='min-height: 300px;'>
							</div>
						</div>
						<div id='discussedNewsBlock' style='display: none;'>
							<input type='hidden' id='checkDiscussedNews' value='0'>
							<div id='ajaxDiscussedNews'  style='min-height: 300px;'>
							</div>
						</div>
					</div>
					<div id='lineBlank'><img src='/images/blank.gif'></div>
					
					<div id='reviews_block'>
						<div id='blockHeader'>
       						<h3><?=sys::translate('main::reviews')?></h3>
       						<ul id='blockHeaderMenu'>
              					<li class='active' id='editorsReviews'><a href='#' name='editorsReviews' onclick='showReviews(this.name); return false;'><span><?=sys::translate('main::recentNews')?></span></a></li>
              					<li id='usersReviews'><a href='#' name='usersReviews' onclick='loadContent("UsersReviews"); showReviews(this.name); return false;'><span><?=sys::translate('main::popularNews')?></span></a></li>
       						</ul>
       						<div id='blockHeaderHelpers'>
							<a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/reviews_rss.phtml' title='<?=sys::translate('main::rss_canal_reviews')?>' id='RSS'>RSS</a>
	       						<div id='allReviews'><a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?><?endif;?>reviews.phtml'><?=sys::translate('main::allReviews')?></a></div>
       						</div>
						</div>
						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
						<div id='editorsReviewsBlock'>
						<?=main_reviews::showLastEditorsReviews();?>
						</div>
						<div id='usersReviewsBlock' style='display: none;'>
						<input type='hidden' id='checkUsersReviews' value='0'>
							<div id='ajaxUsersReviews'  style='background: url("<?=$skin_url?>images/loading.gif") center center no-repeat; min-height: 340px;'>
							</div>
						</div>
					</div>
					<div id='lineBlank'><img src='/images/blank.gif'></div>

			
					<div id='serials_block'>
						<div id='blockHeader'>
       						<h3><?=sys::translate('main::serials')?></h3>
       						<ul id='blockHeaderMenu'>
              					<li class='active' id='recentlySerials'><a href='#' name='recentlySerials' onclick='showSerials(this.name); return false;'><span><?=sys::translate('main::recentNews')?></span></a></li>
              					<li id='popularSerials'><a href='#' name='popularSerials' onclick='loadContent("PopularSerials");showSerials(this.name); return false;'><span><?=sys::translate('main::popularNews')?></span></a></li>
              					<li id='discussedSerials'><a href='#' name='discussedSerials' onclick='loadContent("DiscussedSerials");showSerials(this.name); return false;'><span><?=sys::translate('main::discussedNews')?></span></a></li>
       						</ul>
       						<div id='blockHeaderHelpers'>
	       						<a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/serials_rss.phtml' title='<?=sys::translate('main::rss_canal_serials')?>' id='RSS'>RSS</a>
	       						<div id='allSerials'><a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?><?endif;?>serials.phtml'><?=sys::translate('main::allSerials')?></a></div>
       						</div>
						</div>
						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
						<div id='recentlySerialsBlock'>
						<?=main_serials::showLastArticles()?>
						</div>
						<div id='popularSerialsBlock' style='display: none;'>
							<input type='hidden' id='checkPopularSerials' value='0'>
							<div id='ajaxPopularSerials'  style='min-height: 300px;'>
							</div>
						</div>
						<div id='discussedSerialsBlock' style='display: none;'>
							<input type='hidden' id='checkDiscussedSerials' value='0'>
							<div id='ajaxDiscussedSerials'  style='min-height: 300px;'>
							</div>
						</div>
					</div>
					<div id='lineBlank'><img src='/images/blank.gif'></div>
				
					<div id='articles_block'>
						<div id='blockHeader'>
       						<h3><?=sys::translate('main::articles')?></h3>
       						<ul id='blockHeaderMenu'>
              					<li class='active' id='recentlyArticles'><a href='#' name='recentlyArticles' onclick='showArticles(this.name); return false;'><span><?=sys::translate('main::recentNews')?></span></a></li>
              					<li id='popularArticles'><a href='#' name='popularArticles' onclick='loadContent("PopularArticles");showArticles(this.name); return false;'><span><?=sys::translate('main::popularNews')?></span></a></li>
              					<li id='discussedArticles'><a href='#' name='discussedArticles' onclick='loadContent("DiscussedArticles");showArticles(this.name); return false;'><span><?=sys::translate('main::discussedNews')?></span></a></li>
       						</ul>
       						<div id='blockHeaderHelpers'>
	       						<a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/articles_rss.phtml' title='<?=sys::translate('main::rss_canal_articles')?>' id='RSS'>RSS</a>
	       						<div id='allArticles'><a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?><?endif;?>articles.phtml'><?=sys::translate('main::allArticles')?></a></div>
       						</div>
						</div>
						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
						<div id='recentlyArticlesBlock'>
						<?=main_articles::showLastArticles()?>
						</div>
						<div id='popularArticlesBlock' style='display: none;'>
							<input type='hidden' id='checkPopularArticles' value='0'>
							<div id='ajaxPopularArticles'  style='min-height: 300px;'>
							</div>
						</div>
						<div id='discussedArticlesBlock' style='display: none;'>
							<input type='hidden' id='checkDiscussedArticles' value='0'>
							<div id='ajaxDiscussedArticles'  style='min-height: 300px;'>
							</div>
						</div>
					</div>
					<div id='lineBlank'><img src='/images/blank.gif'></div>

					<div id='interview_block'>
						<div id='blockHeader'>
       						<h3><?=sys::translate('main::interview')?></h3>
       						<ul id='blockHeaderMenu'>
              					<li class='active' id='recentlyInterview'><a href='#' name='recentlyInterview' onclick='showInterview(this.name); return false;'><span><?=sys::translate('main::recentNews')?></span></a></li>
              					<li id='popularInterview'><a href='#' name='popularInterview' onclick='loadContent("PopularInterview");showInterview(this.name); return false;'><span><?=sys::translate('main::popularNews')?></span></a></li>
              					<li id='discussedInterview'><a href='#' name='discussedInterview' onclick='loadContent("DiscussedInterview");showInterview(this.name); return false;'><span><?=sys::translate('main::discussedNews')?></span></a></li>
       						</ul>
       						<div id='blockHeaderHelpers'>
	       						<a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/interview_rss.phtml' title='<?=sys::translate('main::rss_canal_interview')?>' id='RSS'>RSS</a>
	       						<div id='allInterview'><a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?><?endif;?>interview.phtml'><?=sys::translate('main::allInterview')?></a></div>
       						</div>
						</div>
						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
						<div id='recentlyInterviewBlock'>
						<?=main_interview::showLastArticles()?>
						</div>
						<div id='popularInterviewBlock' style='display: none;'>
							<input type='hidden' id='checkPopularInterview' value='0'>
							<div id='ajaxPopularInterview'  style='min-height: 300px;'>
							</div>
						</div>
						<div id='discussedInterviewBlock' style='display: none;'>
							<input type='hidden' id='checkDiscussedInterview' value='0'>
							<div id='ajaxDiscussedInterview'  style='min-height: 300px;'>
							</div>
						</div>
					</div>
					<div id='lineBlank'><img src='/images/blank.gif'></div>

					<div id='gossip_block'>
						<div id='blockHeader'>
       						<h3><?=sys::translate('main::gossip')?></h3>
       						<ul id='blockHeaderMenu'>
              					<li class='active' id='recentlyGossip'><a href='#' name='recentlyGossip' onclick='showGossip(this.name); return false;'><span><?=sys::translate('main::recentNews')?></span></a></li>
              					<li id='popularGossip'><a href='#' name='popularGossip' onclick='loadContent("PopularGossip");showGossip(this.name); return false;'><span><?=sys::translate('main::popularNews')?></span></a></li>
              					<li id='discussedGossip'><a href='#' name='discussedGossip' onclick='loadContent("DiscussedGossip");showGossip(this.name); return false;'><span><?=sys::translate('main::discussedNews')?></span></a></li>
       						</ul>
       						<div id='blockHeaderHelpers'>
	       						<a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/gossip_rss.phtml' title='<?=sys::translate('main::rss_canal_gossip')?>' id='RSS'>RSS</a>
	       						<div id='allGossip'><a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?><?endif;?>gossip.phtml'><?=sys::translate('main::allGossip')?></a></div>
       						</div>
						</div>
						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
						<div id='recentlyGossipBlock'>
						<?=main_gossip::showLastArticles()?>
						</div>
						<div id='popularGossipBlock' style='display: none;'>
							<input type='hidden' id='checkPopularGossip' value='0'>
							<div id='ajaxPopularGossip'  style='min-height: 300px;'>
							</div>
						</div>
						<div id='discussedGossipBlock' style='display: none;'>
							<input type='hidden' id='checkDiscussedGossip' value='0'>
							<div id='ajaxDiscussedGossip'  style='min-height: 300px;'>
							</div>
						</div>
					</div>
					<div id='lineBlank'><img src='/images/blank.gif'></div>			
			
<?
if (sys::isDebugIP()) 
{
?>
<?
}
?>			
			
					<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>

<script type='text/javascript'>
function showHelp()
{
	$('#helpDiv').toggle();
}
</script>

<a href='#' onclick='showHelp();return false;' style='float: right; color: #2379b6;'><?=sys::translate('main::about_project')?></a>
<div id='lineBlank' style='height: 20px;'><img src='/images/blank.gif' height='1'></div>
<div id='helpDiv' style='display: none; text-align: justify;'>
<?=sys::translate('main::seo_main')?>
</div>				</div>
				<div id='rightContent'>

<div style='margin-bottom: 12px;'>
<?=banners::showBanners('v3_premium_main')?>
<br>
<?=banners::showBanners('v3_premium_main3')?>
	<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif'></div><br>
		<script src="<?=$skin_url?>js/jquery.easing.1.3.js"></script>
	<script src="<?=$skin_url?>js/slides.min.jquery.js"></script>

				<input type='hidden' id='checkCinemas' value='0'>
				<input type='hidden' id='checkGenres' value='0'>
				<input type='hidden' id='checkTime' value='0'>
				<input type='hidden' id='checkFilms' value='0'>
					<div class='blocks'>
						<div class='block nearest'>
							<div class='label' onclick='loadContent("Cinemas")' >
								<div class='title'><?=sys::translate('main::nearestCinema')?></div>
								<div class='description'><?=sys::translate('main::findCinemaMap')?></div>
							</div>
								<div class='outer'>
									<div class='botShadow'></div>
									<div class='clear'></div>
								<div class="container"><div class="content">
									<h4><?=sys::translate('main::chooseCinema')?></h4>
									<a href='' class='closeButton'><?=sys::translate('main::close')?></a>
									<div class='clear'></div>
								<div id='ajaxCinemas' style='background: url("<?=$skin_url?>images/loading.gif") center center no-repeat; height: 220px;'>
                                 </div>
								</div></div>
								</div>
						</div>
						<div class='block haveChoice'>
							<div class='label' onclick='loadContent("Genres")'>
								<div class='title'><?=sys::translate('main::haveChoise')?></div>
								<div class='description'><?=sys::translate('main::chooseGenre')?></div>
							</div>
								<div class='outer'>
																	<div class='botShadow'></div>
									<div class='clear'></div>
								<div class="container"><div class="content">
								<h4><?=sys::translate('main::haveChoise')?></h4>
								<a href='' class='closeButton'><?=sys::translate('main::close')?></a>
								<div class='clear' style='height: 20px;'></div>
								<div id='ajaxGenres' style='background: url("<?=$skin_url?>images/loading.gif") center center no-repeat; height: 220px;'>
							</div>
								</div></div>
																<div class='botShadow'></div>
								</div>
						</div>
						<div class='block time'>
							<div class='label' onclick='loadContent("Time")'>
								<div class='title'><?=sys::translate('main::myTime')?></div>
								<div class='description'><?=sys::translate('main::chooseTime')?></div>
							</div>
								<div class='outer'>
																	<div class='botShadow'></div>
									<div class='clear'></div>
								<div class="container"><div class="content">

								<h4><?=sys::translate('main::myTime')?></h4>
								<a href='' class='closeButton'><?=sys::translate('main::close')?></a>
								<div class='clear'></div>
								<div id='ajaxTime' style='background: url("<?=$skin_url?>images/loading.gif") center center no-repeat; height: 220px;'>
								</div>
								</div></div>
																<div class='botShadow'></div>
								</div>
						</div>
						<div class='block wantee'>
							<div class='label' onclick='loadContent("Films")'>
								<div class='title'><?=sys::translate('main::wantToSee')?></div>
								<div class='description'><?=sys::translate('main::filmsListShow')?></div>
							</div>
							<div class='outer'>
																	<div class='botShadow'></div>
									<div class='clear'></div>
								<div class="container">
									<div class="content">
										<h4><?=sys::translate('main::wantToSee')?></h4>
										<a href='' class='closeButton'><?=sys::translate('main::close')?></a>
										<div class='clear'></div>
									<div id='ajaxFilms' style='background: url("<?=$skin_url?>images/loading.gif") center center no-repeat; height: 220px;'>
									</div>
									</div>
								</div>
								<div class='botShadow'></div>
							</div>
						</div>
					</div>
	<div style='margin-bottom: 12px;'>
	<!-- VK Widget -->
<div class='tab-widget-multy twm-wrap' data-active='twm-i1'>
	<div class='twm-wrp-buts'>
			<span class='twm-but' data-go='twm-i1'>
			<img src='/images/facebook-icon32.png' alt='fk'>
		</span>
		<span class='twm-but' data-go='twm-i2'>
			<img src='/images/twitter-icon32.png' alt='tw'>
		</span>
		<span class='twm-but' data-go='twm-i3'>
			<img src='/images/google-plus.jpg' alt='G+'>
		</span>
				<!-- VK <span class='twm-but' data-go='twm-i3'>
			<img src='/images/vk-icon32.png' alt='vk'>
		</span> -->
	</div>
	<div class='twm-wrp-cont'>
		<!-- VK <div class='twm-i twm-i3'>
			<div id="vk_groups"></div>
<script type="text/javascript">
VK.Widgets.Group("vk_groups", {mode: 3, width: "300", no_cover: 0}, 13118282);
</script>
		</div>  -->
		<div class='twm-i twm-i2'>
			<!-- Для twitter -->
		<br>	<iframe allowtransparency="true" scrolling="no" src="https://platform.twitter.com/widgets/follow_button.html?screen_name=Kino_teatr" style="width: 300px; height: 20px;" frameborder="0"></iframe>
		<br></div>
		<div class='twm-i twm-i1'>
		<div class="fb-page" data-href="https://www.facebook.com/kinoteatrua/" data-tabs="timeline" data-width="300" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/kinoteatrua/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/kinoteatrua/">Kino-Teatr.ua</a></blockquote></div>
			<!-- Для FaceBook -->
		</div>
		<div class='twm-i twm-i3'>
<!-- Поместите этот код туда, где должен отображаться значок. -->
<a href="https://plus.google.com/101732602037174602806?prsrc=3" rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
<span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Kino-Teatr.ua</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">в</span>
<img src="//ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
</a><br>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){

	$('.tab-widget-multy').each(function(){
		var th = $(this);
		var code = th.attr('data-code');
		var active = th.attr('data-active');
		if(!code){
			$('.twm-but', th).click(function(){
				var th_but = $(this);
				$('.twm-i', th).removeClass('twm-open');
				$('.twm-but', th).removeClass('twm-active');
				var i = th_but.attr('data-go');
				setTimeout(function(){
					$('.'+i, th).addClass('twm-open');
					th_but.addClass('twm-active');
				}, 50);
			});
			if(active){
				setTimeout(function(){
					$('.'+active, th).addClass('twm-open');
					$('.twm-but[data-go='+active+']', th).addClass('twm-active');
				}, 50);
			}
			th.attr('data-code', 1);
		}
	});
	});
</script>
<!-- VK Widget -->
	</div>
	<div style='margin-bottom: 12px;'>
	<?=main_contest::showFrontEndContest()?>
<?
		print main_films::showFrontPopularFilmsList();  // блок вывода популярных фильмов
		print main_films::showFrontNewTrailersList();	// блок вывода последних новых трейлеров
		print main_films::showFrontNewFilmsList();	// блок вывода последних новых фильмов
		print main_films::showFrontNewPostersList();	// блок вывода последних новых постеров
		print main_films::showFrontNewOzhFilmsList();	// блок вывода последних самых ожидаемых
//		if(sys::isDebugIP()) 
?>

</div>
<?=main_box::showFrontEndBox()?>
	<div style='margin-bottom: 12px;'>
<?=banners::showBanners('v3_premium_main2')?>
</div>
<?php
/*
<!-- start adv -->
<script async="async" src="https://w.uptolike.com/widgets/v1/zp.js?pid=629110" type="text/javascript"></script>
<!-- end adv -->
*/ ?>


					<!--div id='photoBlock'>
						<div id='photoBlockTitle'>
							Фоторепортажи
						</div>

						<a href='#' class='photoTitle'>XII Премия вручения Оскара</a>

						<ul id='photoList'>
							<li><a href='#'><img src='<?=$skin_url?>images/photo1.jpg'></a></li>
							<li><a href='#'><img src='<?=$skin_url?>images/photo2.jpg'></a></li>
							<li><a href='#'><img src='<?=$skin_url?>images/photo3.jpg'></a></li>
							<li><a href='#'><img src='<?=$skin_url?>images/photo4.jpg'></a></li>
							<li><a href='#'><img src='<?=$skin_url?>images/photo5.jpg'></a></li>
							<li><a href='#'><img src='<?=$skin_url?>images/photo3.jpg'></a></li>
							<li><a href='#'><img src='<?=$skin_url?>images/photo4.jpg'></a></li>
							<li><a href='#'><img src='<?=$skin_url?>images/photo1.jpg'></a></li>
							<li><a href='#'><img src='<?=$skin_url?>images/photo5.jpg'></a></li>
							<li><a href='#'><img src='<?=$skin_url?>images/photo2.jpg'></a></li>
						</ul>

						<a href='#' id='allPhoto'>Все репортажи</a>
					</div-->


							<?=banners::showBanners('v3_fullscreen_banner_main')?>

				</div></div>
										<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif'></div>
<?include(_SKIN_DIR.'tpl.__footer_main.php')?>
<!--<?=$_SERVER['REMOTE_ADDR']?>-->
<!--<?=$_user["id"]?>-->
<!--<?=date("d.m.Y H:i:s")?>-->