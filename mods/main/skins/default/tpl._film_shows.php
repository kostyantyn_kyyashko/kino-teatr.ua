<script type="text/javascript" src="/mods/main/skins/default/js/jquery.colorbox.js"></script>
<link rel="stylesheet" type="text/css" href="/mods/main/skins/default/js/colorbox/colorbox.css" />
<script type="text/javascript" src="/mods/main/skins/default/js/eventmap.js"></script>

<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=6?>
<?$_var['active_child_menu']=7?>
<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>
				<div class='myriadFilm' id='myriad'><?=$var['title']?></div>
<?
//WF(
function cmpfilm($a, $b) 
{
	$at = explode(":", strip_tags($a['times'][0]['time']));
	$bt = explode(":", strip_tags($b['times'][0]['time']));
	
	$ad = explode(".",$a['begin']);
	$bd = explode(".",$b['begin']);
	
	$ad = mktime($at[0],$at[1],0,$ad[1],$ad[0],$ad[2]);
	$bd = mktime($bt[0],$bt[1],0,$bd[1],$bd[0],$bd[2]);

	return ($ad < $bd) ? -1 : 1;
}
//)WF
?>

					<div id='lineHrWide'>&nbsp;</div>


	<?=main_films::showFilmTabs($_GET['film_id'],$var['film'],'shows');?>

						<div id='lineBlank' style='height: 14px;'><img src='/images/blank.gif' height='1'></div>


<script type="text/javascript">
	function ShowNote(id)
	{
		$(".noteDiv").css("display","none");
		$("#noteDiv_"+id).toggle("slow");
		return false;
	}
</script>

	<?if($var['cinemas']):?>
			<script src="https://bilet.vkino.com.ua/extras/widget/1.7a/main.min.js"></script>
			<?foreach ($var['cinemas'] as $cinema):?>
			
				<?if ($cinema['count']>0):?>

					<?
						// массив с индексами для *
						$notes = array();
//if(sys::isDebugIP() && ($cinema['title']=="Киото")) {sys::printR($cinema);}
						foreach ($cinema['shows'] as $show)
							foreach ($show['times'] as $time)
							{
								$trim_note = $time["note"];
								if($trim_note && !in_array($trim_note, $notes)) $notes[] = $trim_note;
							}
					?>

					<span id='afishaKtName'><a href='<?=$cinema['url']?>'><?=sys::translate('main::cinema')?> <?=$cinema['title']?></a>
					<?if ($cinema['ticket_url']=='vkino'):?>
						<a href='#' title='<?=sys::translate('main::buy_ticket')?>' id='btn_tckts' onclick="{alert('<?=sys::parseModTpl('main::buy_alert')?>');return false;}"><?=sys::translate('main::buy_ticket')?></a>
                    <?endif;?>
                    </span>
						<div id='afishaItemTitle'>
							<div class='filmName'><?=sys::translate('main::date')?></div>
							<div class='filmZal'><?=sys::translate('main::hall')?></div>
							<div class='filmShows'><?=sys::translate('main::shows')?></div>
							<div class='filmPrices'><?=sys::translate('main::prices')?></div>
						</div>
<?
					//WF(
					$arrshows = $cinema['shows'];
					usort($arrshows, "cmpfilm");
					
					foreach ($arrshows as $show):
					//foreach ($cinema['shows'] as $show):
					//WF)?>
					
					<?
						if(!isset($show['times']) || !sizeof($show['times'])) continue;
					?>
						<div id='afishaItem'>
							<div class='filmName'><?if (($show['begin']) == ($show['end'])):?><?=$show['begin']?><?else:?><?=$show['begin']?> - <?=$show['end']?><?endif?></div>
							<div class='filmZal'>
							<?if($show['hall']):?>
							<?=$show['hall']?>
							<?else:?>
							&mdash;
							<?endif;?>
							</div>
							<div class='filmShows' style='width: 257px;' data-date="<?=$show['begin']?>">

						<?
									$is_2d = 0;
									$has_3d = 0;
									$timesa = array();
									foreach ($show['times'] as $time)
									{
										$has_3d += $time['seans_3d'];
										$star_class = $time['past']?"mark_star_time_past":"mark_star_time";

										$note = array_search($time['note'],$notes,true);
										$note = ($note===FALSE)?"":'<sup class="'.$star_class.'"><a href=# onclick=\'return ShowNote('.$cinema['id'].')\'>'.str_repeat("*",$note+1)."</a></sup>";

										$is_2d = ($show['film_3d'] && $show['3d'] && !$time['seans_3d'])?"<sup class='mark3d'>2D</sup>":"";
										if($time['past'])
										{
											$timesa[] = "<span class='timepast'>".strip_tags($time['time'])."$is_2d</span>$note";
										} else{
											$timesa[] = "<span class='time'>".$time['time']."$is_2d</span>$note";
										}

									}

									$timesar = implode('<span class="delimiter">, </span>', $timesa);
									?>

									<?=$timesar?>

							</div>
							<div class='filmShows' style='width: 50px;'>
								<?if($show['film_3d'] && $show['3d'] && $has_3d):?>
									<center><img src='<?=$skin_url?>images/3dglass.jpg'></center>
								<?else:?>
									&nbsp;
								<?endif;?>
							</div>

							<div class='filmPrices'>

							<?

								$max = 0;
								$min = 1000;

								$price = array();

								foreach ($show['prices'] as $key=>$value)
								{
									$pricearr = array();
									$pricearr = explode(', ', $value);
									foreach ($pricearr as $key=>$value)
									{
										$price[] = $value;
									}
								}



								foreach ($price as $key=>$value)
								{
									if ($value > $max)
										$max = $value;

									if ($value <= $min)
										$min = $value;
								}


							?>

							<?if ($min!=$max):?>
							<?=$min?>-<?=$max?> <?=sys::translate('main::grn')?></div>
                            <?else:?>
							<?=$min?> <?=sys::translate('main::grn')?></div>
                            <?endif;?>

							<!--div class='filmIgo'><a href='#'><img src='images/Igo.jpg'></a></div-->
						</div>
					<?endforeach;?>

					<?if(sizeof($notes)):?>
							<div class="noteDiv" id='noteDiv_<?=$cinema['id']?>'>
							<?
								$infotext = "<table width='100%'>";
								foreach($notes as $index=>$text)
								{
									$total_index = sizeof($notes);
									$infotext .= "<tr><td width='2%' nowrap valign=top>".str_repeat("*",$index+1)."</td><td>-&nbsp;$text</td></tr>";
								};
								$infotext .= "</table>";
								echo $infotext;
							?>
							</div>
					<?endif;?>

				<?endif;?>
						<div id='lineBlank' style='height: 15px;'><img src='/images/blank.gif' height='1'></div>
			<?endforeach;?>

	<?else:?>
		<div id='lineBlank' style='height: 15px;'><img src='/images/blank.gif' height='1'></div>
		<center class="xErr"><?=sys::translate('main::no_shows')?></center>
	<?endif;?>

					<?=banners::showBanners('v3_secondbanner')?>
