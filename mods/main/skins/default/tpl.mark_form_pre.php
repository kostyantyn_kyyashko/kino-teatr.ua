<script type="text/javascript">
function showExpectNum(yes, no, value) {
	$('#waiting_yes').css({"width":yes+"%"}).html((yes>20)?yes:"");
	$('#waiting_no').css({"width":no+"%"}).html((no>20)?no:"");
	$("span.showExpectNum").html("<?=sys::translate($var["premiered"]?'main::expected':'main::expecting')?>")
	
	if(value>0) value = "<?=sys::translate('main::yesExpect')?>";
	else if(value<0) value = "<?=sys::translate('main::noExpect')?>";

	$('#yourExpect').html(value);
	
	if(value!="—") $("#expect_lines, span.showExpectNum").show(500);
}

function vote_mark(value){
	$.ajax({
		url:'?mod=main&act=ajx_vote_expect',
		data:{
			mark:value,
			object_id:<?=intval("0".$var['film_id'])?>
		},
		dataType:'json',
		success:function(data)
		{					
			if(data)
			{
				$("#filmAwaiting").hide(500);
				showExpectNum(data.yes, data.no, value);
			}
		},
		type:'POST'
	});  	
	
	return false;
}
</script>

<div id='searchTitle'  style="margin-top:20px;"><?=sys::translate('main::film_waiting')?>&nbsp; | &nbsp; <?=sys::translate('main::your_mark')?>: <span id="yourExpect" class="yourMark"></span></div>

<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>

	<div class="markForm" id="expect_lines" style="display:none">
		<div id='waiting_bg'>
			<div id='waiting_yes' style="width:0"></div>
			<div id='waiting_no' style="width:0"></div>
		</div>
	</div>
	<span id="imdb" class="showExpectNum" style="display:none"></span>

<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>

<?if($var["is_showing"]) { // buttons ?>

<div id='filmAwaiting'><span id='doYou'><strong><?=sys::translate('main::areYouWaiting')?></strong></span>
	<ul id='awaitingUl'>
		<li><a class="yes" onclick="return vote_mark(1);" href=""><span><?=sys::translate('main::yesLabel')?></span></a></li>
		<li><a class="no" onclick="return vote_mark(-1);" href=""><span><?=sys::translate('main::noLabel')?></span></a></li>
	</ul>
</div>

<?}//endif?>

<script type="text/javascript">
$(function() {
	showExpectNum('<?=$var['yes']?>', '<?=$var['no']?>', '<?=$var['yourMark']?>');
});
</script>
