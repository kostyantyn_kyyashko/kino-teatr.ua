<div id='worldBoxBlock' style='display: none;'>
	<div id='boxBlockTitle'>
      <div style='display: inline-block; float: left;'>
         <?=sys::translate('main::boxOffice')?><br>
         <span class='boxorig'>(<?=sys::translate('main::boxOfficeEn')?>)</span>
      </div>

<ul id="blockHeaderMenuBox">
	<li id="ukraineBox"><a href="#" name="ukraineBox" onclick="showBox(this.name); return false;"><span>Украина</span></a></li>
	<li class="active" id="worldBox"><a href="#" name="worldBox" onclick="showBox(this.name); return false;"><span>Мир</span></a></li>
</ul>

	<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

	</div>

	<div id='boxBlockTop'>&nbsp;</div>


	<?foreach ($var['objects'] as $obj):?>
	<div id='boxItem'>
  		<div id='boxPosition'><?=$obj['position']?>.</div>

		<div id='boxName'>
			<a href='<?=$obj['url']?>' title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=$obj['year']?>"><?=$obj['title']?></a><br>
			<?=$obj['title_orig']?>
		</div>

		<div id='boxMoney'>
			<?=$obj['money']?> $
		</div>

		<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
	</div>
	<?endforeach;?>

	<div id='boxBottom'>
  		<div id='boxDate'><?=$var['start']?> &mdash; <?=$var['finish']?></div>

  		<a href='<?=$var['url']?>' id='boxDetails'><?=sys::translate('main::more')?></a>
		<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
	</div>
</div>