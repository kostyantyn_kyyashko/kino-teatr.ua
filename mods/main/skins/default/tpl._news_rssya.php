<?sys::setTpl('main::__rss');?>
<channel>
<title><title><?=sys::translate('main::kinoportal_kino_teatr')?><?=sys::translate('main::world_news')?></title></title>
<link><?=_ROOT_URL?></link>
<image>
	<url><?=_ROOT_URL?>public/img/logo_ya.gif</url>
	<title><?=sys::translate('main::kinoportal_kino_teatr')?></title>
	<link><?=_ROOT_URL?></link>
</image>
<description><?=sys::parseModTpl('main::news_rss_description')?></description>
<?foreach ($var['objects'] as $obj):?>
<item>
	<title><?=$obj['title']?></title>
	<link><?=$obj['url']?></link>
	<description><?=$obj['intro']?></description>
	<category><?=$obj['category']?></category>
	<pubDate><?=$obj['date']?></pubDate>
	<yandex:full-text><?=htmlentities(strip_tags($obj['text']),false,'utf-8')?></yandex:full-text>
	<categoty><?=$obj['category']?></categoty>
</item>
<?endforeach;?>
</channel>

