<?=sys::useLib('main::films');?>
<? $ratingSpecialFormat = round($var['rating'],1);?>

<script type="text/javascript">
function loadRating(rating)
{
	$('div.markForm img.star').each(
		function(){
			var mark=Math.round($(this).attr('num'));
			var rest=rating-Math.floor(rating);
			if(mark<=Math.floor(rating))
			{
				$(this).attr('src','<?=_IMG_URL.'star.png'?>');
			}
			else if(mark==Math.floor(rating)+1 || (rating<1 && mark==1))
			{
				if(rest<0.25)
					$(this).attr('src','<?=_IMG_URL.'star0.png'?>');
				if(rest>=0.25)
					$(this).attr('src','<?=_IMG_URL.'star1.png'?>');
				if(rest>=0.5)
					$(this).attr('src','<?=_IMG_URL.'star2.png'?>');
				if(rest>=0.75)
					$(this).attr('src','<?=_IMG_URL.'star3.png'?>');
			}
			else
				$(this).attr('src','<?=_IMG_URL.'star0.png'?>');
		}
	)
}
$(function(){

	$("div.markForm img.star").hover(
		function(){
			$(this).css('opacity','0.3');
			var num=Math.round($(this).attr('num'));
			$('div.markForm img.star').each(
				function()
				{
					if(Math.round($(this).attr('num'))<num)
						$(this).css('opacity','0.3');
				}
			)
		},
		function(){
			$(this).css('opacity','1');
			var num=Math.round($(this).attr('num'));
			$('div.markForm img.star').each(
				function()
				{
					if(Math.round($(this).attr('num'))<num)
						$(this).css('opacity','1');
				}
			)
		}
	)

		$("div.markForm a.vote").click(
			function(){
				var mark=$(this).attr('mark');
				$.ajax({
					url:'?mod=main&act=<?=$var['ajx_type']?>',
					data:{
						mark:mark,
						object_id:<?=$var['object_id']?>
					},
					beforeSend:function(){
						$('#rating').html('...');
						$('#imdbRating').html('...');
						$('#yourMark').html('...');
					},
					dataType:'json',
					success:function(data)
					{
						$('div.markForm span.yourMark').html(mark);
						if(data)
						{
							$('#rating').html(data['rating']+' / <span itemprop="bestRating">10</span>');
							$('#imdbRating').html(data['votes']);
							$('#yourMark').html(data['yourMark']);
							loadRating(data['rating']);
						}
					},
					type:'POST'
				})


				return false;
			}
		)
})

</script>

<div id='searchTitle'><?=sys::translate('main::'.$var['form_title'])?> 
&nbsp; | &nbsp; 
<?=sys::translate('main::your_mark')?>: <span id="yourMark" class="yourMark"><?=$var['yourMark']?></span>
</div>
<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
<div id="rating-block">
		<div class="markForm">

		<?for($i=1; $i<11; $i++):?>
			<?
				if($i<=floor($var['rating']))
				{
					$src=_IMG_URL.'star.png';
				}
				elseif($i==floor($var['rating']+1) || ($var['rating']<1 && $i==1))
				{
					if($var['rest']<0.25)
						$src=_IMG_URL.'star0.png';
					if($var['rest']>=0.25) 
						$src=_IMG_URL.'star1.png';
					if($var['rest']>=0.5)
						$src=_IMG_URL.'star2.png';
					if($var['rest']>=0.75)
						$src=_IMG_URL.'star3.png';
				}
	
				else
					$src=_IMG_URL.'star0.png';
			?>
			<a title="<?=sys::translate('main::rating')?> <?=$i?> / 10" class="vote" mark=<?=$i?> href=""><img num="<?=($i)?>" class="star" src="<?=$src?>" alt=""></a>
		<?endfor?>

		</div>
	
		<meta itemprop="ratingValue" content="<?=$ratingSpecialFormat?>">
		<meta itemprop="worstRating" content = "0">	
		<span id="rating"><?=$var['rating']?> / <span itemprop="bestRating">10</span></span>
		<span id='imdb'><?=sys::translate('main::votes')?>: </span>
		<span id='imdbRating' <?if($var['rating']>0):?>itemprop="ratingCount"<?endif;?>><?=$var['votes']?></span>

</div>


