


	<script>
		$(function(){
			$('#slides4').slides({
				preload: false,
				preloadImage: '<?=$skin_url?>images/loading.gif',
				generateNextPrev: true,
				hoverPause: true
			});
		});
	</script>

		<div class='prompt'><?=sys::translate('main::chooseMovieToWatch')?></div>


		<div id="slides4" class='sliders'>
			<div class="slides_container">
                <?$i=1;?>



				<?foreach($var['objects'] as $obj):?>
					<?if($i==1):?>
	    				<div class='element'>
							<ul class='filmsul'>
								<li style="padding: 0;">
					<?else:?>
								<li>
					<?endif;?>

									<a href="<?=$obj['film_url']?>" class="bestFilm">
										<img src="<?=$obj['image']?>" class="bestFilm">
									</a>
									<div style='float: right; width: 113px;'>
										<a href="<?=$obj['film_url']?>" class="title" title='<?=$obj['film']?>'><?=$obj['film']?></a>
										<br><?=$obj['genres']?>
										<?if($obj['trailers']):?><br><br><a href="<?=$obj['trailers']?>" class='trailersBlock'><?=sys::translate('main::trailer')?></a><?endif;?>
										<?if($obj['shows']):?><br><a href="<?=$obj['shows']?>" class='whatWatchBlock'><?=sys::translate('main::whereToWatch')?></a><?endif;?>
										<?if($obj['premiere']):?><a class="firstShow"><?=sys::translate('main::premier')?></a><?endif;?>
									</div>
									</li>

					<?if($i==3):?>
							</ul>
						</div>
						<?$i=0;?>
					<?endif;?>

					<?$i++;?>
				<?endforeach;?>

				<?if($i!=1):?>
						</ul>
					</div>
				<?endif;?>



												</div>
											</div>

								<a href='<?=_ROOT_URL?>bill.phtml' class='all floatLeft'><?=sys::translate('main::allBill')?></a>