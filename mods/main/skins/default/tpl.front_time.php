								<div class='prompt'><?=sys::translate('main::youCanChooseTime')?></div>
								<table class='list'>
									<tr>
										<td>
											<ul>
												<li><a href='<?=sys::rewriteUrl('?mod=main&act=bill&minTime='.($var['curentHour']+1).':00')?>'><?=sys::translate('main::inHour')?></a>
												<li><a href='<?=sys::rewriteUrl('?mod=main&act=bill&minTime='.($var['curentHour']+2).':00')?>'><?=sys::translate('main::in2Hours')?></a>
												<li><a href='<?=sys::rewriteUrl('?mod=main&act=bill&minTime='.($var['curentHour']+3).':00')?>'><?=sys::translate('main::in3Hours')?></a>
											</ul>
										</td>
										<td>
											<ul>
												<li><a href='<?=sys::rewriteUrl('?mod=main&act=bill&minTime=18:00')?>'><?=sys::translate('main::atEvening')?></a>
												<li><a href='<?=sys::rewriteUrl('?mod=main&act=bill&minTime=22:00')?>'><?=sys::translate('main::after22')?></a>
												<li><a href='<?=sys::rewriteUrl('?mod=main&act=bill&date='.$var['nextDay'].'&maxTime=12:00')?>'><?=sys::translate('main::tomorrowMorning')?></a>
											</ul>
										</td>
										<td>
											<ul>
												<li><a href='<?=sys::rewriteUrl('?mod=main&act=bill&date='.$var['nextDay'])?>'><?=$var['nextDayName']?></a>
												<li><a href='<?=sys::rewriteUrl('?mod=main&act=bill&date='.$var['twoDay'])?>'><?=$var['twoDayName']?></a>
												<li><a href='<?=sys::rewriteUrl('?mod=main&act=bill&date='.$var['threeDay'])?>'><?=$var['threeDayName']?></a>
											</ul>
										</td>
									</tr>
								</table>
								<div class='prompt2'><?=sys::translate('main::chooseTimer')?></div>

								<div class='times'>
								<?for ($i=8;$i<=22;$i++):?>
									<a href='<?=sys::rewriteUrl('?mod=main&act=bill&minTime='.$i.':00&maxTime='.($i+1).':00')?>' <?if($i<$var['curentHour']):?>class='disabled'<?endif;?>><?if($i<10):?>0<?=$i?><?else:?><?=$i?><?endif;?>:00</a>
								<?endfor;?>
								</div>

								<a href='<?=_ROOT_URL?>bill.phtml' class='all'><?=sys::translate('main::allBill')?></a>