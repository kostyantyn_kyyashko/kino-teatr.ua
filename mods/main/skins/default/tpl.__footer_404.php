<?sys::useLib('main::films');?>
<?sys::useLib('main::shows');?>
	<table class="catalog">
	<tr>
		<td width="120">
			<a href="<?=_ROOT_URL?>bill.phtml" class="section">Афиша</a>
			<a href="<?=_ROOT_URL?>bill.phtml">Сейчас в кино</a>
			<a href="<?=_ROOT_URL?>films-near.phtml">Скоро в кино</a>
		</td>
		<td width="165">
			<a href="<?=_ROOT_URL?>cinemas.phtml" class="section">Кинотеатры</a>
			<a href="<?=_ROOT_URL?>cinemas-kiev.phtml">Кинотеатры Киев</a>
			<a href="<?=_ROOT_URL?>cinemas-donetsk.phtml">Кинотеатры Донецк</a>
			<a href="<?=_ROOT_URL?>cinemas-mariupol.phtml">Кинотеатры Мариуполь</a>
			<a href="<?=_ROOT_URL?>cinemas-kharkov.phtml">Кинотеатры Харьков</a>
			<a href="<?=_ROOT_URL?>cinemas-dnepropetrovsk.phtml">Кинотеатры Днепропетровск</a>
			<a href="<?=_ROOT_URL?>cinemas-lvov.phtml">Кинотеатры Львов</a>
			<a href="<?=_ROOT_URL?>cinemas-chernovtsy.phtml">Кинотеатры Черновцы</a>
			<a href="<?=_ROOT_URL?>cinemas-ivano-frankovsk.phtml">Кинотеатры Ивано-Франковск</a>
			<a href="<?=_ROOT_URL?>cinemas-odessa.phtml">Кинотеатры Одесса</a>
			<a href="<?=_ROOT_URL?>cinemas-simferopol.phtml">Кинотеатры Симферополь</a>
		</td>
		<td width="134">
			<a href="<?=_ROOT_URL?>films-near.phtml" class="section">Скоро в кино</a>
				<?=main_films::showLastBotEr()?>
		</td>
		<td width="170">
			<a href="<?=_ROOT_URL?>ru/main/last_trailers.phtml" class="section">Сейчас в кино</a>
				<?=main_shows::getTodayFilmsBot($_cfg['main::city_id'],date('Y-m-d'))?>
		</td>
		<td width="86">
			<a href="<?=_ROOT_URL?>ru/main/films_rating.phtml" class="section">Рейтинги</a>
			<a href="<?=_ROOT_URL?>ru/main/films_rating.phtml">Фильмы</a>
			<a href="<?=_ROOT_URL?>ru/main/films_waiting_rating.phtml">Ожидаемые</a>
			<a href="<?=_ROOT_URL?>box.phtml">Бокс-офис</a>
		</td>
		<td width="128">
			<a href="<?=_ROOT_URL?>ru/main/watch_film.phtml	" class="section">Фильмы онлайн</a>
			<a href="<?=_ROOT_URL?>ru/main/film_watch.phtml">Список фильмов</a>
			<a href="<?=_ROOT_URL?>ru/main/online_film.phtml">Фильмы онлайн</a>
			<a href="<?=_ROOT_URL?>ru/main/comedy_smotret_online_besplatno.phtml">Онлайн комедии</a>
			<a href="<?=_ROOT_URL?>ru/main/boevik_smotret_online_besplatno.phtml">Онлайн боевики</a>
			<a href="<?=_ROOT_URL?>ru/main/filmy_smotret_online_2012.phtml">Фильмы онлайн 2012</a>
		</td>
	</tr>
	</table>

	</div>

<div class="footer">
	<div class="copyrightsBg">
		<div id='copyrights'>
			<div id='copyright'>&copy; 2000-<?=date('Y')?> Kino-teatr.ua</div>
			<div id='advertising'>
				<?=sys::translate('main::adQuestions')?> <a href='mailto:ad@kino-teatr.ua'>ad@kino-teatr.ua</a>
			</div>
			<div style='visibility: hidden; float: right;'>
<?=counters::showCounters();?>
</div>
			<ul id='socials'>
				<li>

				</li>
			</ul>


		</div>
	</div>
</div>
			
<?include(_SKIN_DIR.'tpl.__user_attention.php')?>
</body>
</html>
