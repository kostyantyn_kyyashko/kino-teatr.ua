<?sys::useLib('main::boxw');?>

<script>
function showBox(block)
			{
				$('#ukraineBox').removeClass('active');
				$('#worldBox').removeClass('active');

				$('#'+block).addClass('active');

				$('#ukraineBoxBlock').css('display', 'none');
				$('#worldBoxBlock').css('display', 'none');

				$('#'+block+'Block').fadeIn('slow');
			}
</script>

<div id='boxBlock'>

<div id='ukraineBoxBlock'>
	<div id='boxBlockTitle'>
      <div style='display: inline-block; float: left;'>
         <?=sys::translate('main::boxOffice')?><br>
         <span class='boxorig'>(<?=sys::translate('main::boxOfficeEn')?>)</span>
      </div>

<ul id="blockHeaderMenuBox">
	<li class="active" id="ukraineBox"><a href="#" name="ukraineBox" onclick="showBox(this.name); return false;"><span><?=sys::translate('main::Ukraine')?></span></a></li>
	<li id="worldBox"><a href="#" name="worldBox" onclick="showBox(this.name); return false;"><span><?=sys::translate('main::World')?></span></a></li>
</ul>

	<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

	</div>

	<div id='boxBlockTop'>&nbsp;</div>


	<?foreach ($var['objects'] as $obj):?>
	<div id='boxItem'>
  		<div id='boxPosition'><?=$obj['position']?>.</div>

		<div id='boxName'>
			<a href='<?=$obj['url']?>' title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=$obj['year']?>"><?=$obj['title']?></a><br>
			<?=$obj['title_orig']?>
		</div>

		<div id='boxMoney'>
			<?=$obj['money']?> <?=sys::translate('main::uah')?>
		</div>

		<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
	</div>
	<?endforeach;?>

	<div id='boxBottom'>
  		<div id='boxDate'><?=$var['start']?> &mdash; <?=$var['finish']?></div>

  		<a href='<?=$var['url']?>' id='boxDetails'><?=sys::translate('main::more')?></a>
		<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
	</div>
</div>

	<?=main_boxw::showFrontEndboxw()?>



</div>