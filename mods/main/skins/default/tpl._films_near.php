<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=1;?>
<?$_var['active_child_menu']=4?>
<?sys::setMeta($var['meta']);?>
<?//main::showGemius('AqhASyMPx6mphsXwO9dP28eyXkG8HbrHiT0g1Nu76Xb.O7');?>
<?ob_start()?>
<?require "bill_right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>


				<div class='myriad' id='myriad'><?=$_var['main::title']?></div>

					<div id='news_page' class='filmsNearList'>


	<?if($var['objects']):?>

<script type="text/javascript">
function showExpectNum(yes, no, value, film_id) {
	$("div[name='divExpectLine_"+film_id+"'] #waiting_yes").css({"width":yes+"%"}).html((yes>20)?yes:"");
	$("div[name='divExpectLine_"+film_id+"'] #waiting_no").css({"width":no+"%"}).html((no>20)?no:"");
	$("div[name='divExpectLine_"+film_id+"'], p[name='pExpectLineText_"+film_id+"'], ").show(500);
	
	if(value>0) value = "<?=sys::translate('main::yesExpect')?>";
	else if(value<0) value = "<?=sys::translate('main::noExpect')?>";

	$("span[name='spanExpectYourMark_"+film_id+"']").html(value);
	$("div[name='divExpectButtons_"+film_id+"']").hide(500);
	$("p[name='pExpectButtons_"+film_id+"']").empty().remove();
}

function vote_mark(value, film_id){
	$.ajax({
		url:'?mod=main&act=ajx_vote_expect',
		data:{
			mark: value,
			object_id: film_id
		},
		dataType:'json',
		success:function(data)
		{					
			if(data)
			{
				showExpectNum(data.yes, data.no, value, film_id);
			}
		},
		type:'POST'
	});  	
	
	return false;
}
</script>
	
	
<script type="text/javascript">
function loadRating(object_id, rating)
{
	$('div.markForm[object_id='+object_id+'] img.star').each(
		function(){
			var mark=Math.round($(this).attr('num'));
			var rest=rating-Math.floor(rating);
			if(mark<=Math.floor(rating))
			{
				$(this).attr('src','<?=_IMG_URL.'star.png'?>');
			}
			else if(mark==Math.floor(rating)+1 || (rating<1 && mark==1))
			{
				if(rest<0.25)
					$(this).attr('src','<?=_IMG_URL.'star0.png'?>');
				if(rest>=0.25)
					$(this).attr('src','<?=_IMG_URL.'star1.png'?>');
				if(rest>=0.5)
					$(this).attr('src','<?=_IMG_URL.'star2.png'?>');
				if(rest>=0.75)
					$(this).attr('src','<?=_IMG_URL.'star3.png'?>');
			}
			else
				$(this).attr('src','<?=_IMG_URL.'star0.png'?>');
		}
	)
}
$(function(){

	$("div.markForm img.star").hover(
		function(){
			var object_id=$(this).parent().parent().attr('object_id');
			$(this).css('opacity','0.3');
			var num=Math.round($(this).attr('num'));
			$('div.markForm[object_id='+object_id+'] img.star').each(
				function()
				{
					if(Math.round($(this).attr('num'))<num)
						$(this).css('opacity','0.3');
				}
			)
		},
		function(){
			var object_id=$(this).parent().parent().attr('object_id');
			$(this).css('opacity','1');
			var num=Math.round($(this).attr('num'));
			$('div.markForm[object_id='+object_id+'] img.star').each(
				function()
				{
					if(Math.round($(this).attr('num'))<num)
						$(this).css('opacity','1');
				}
			)
		}
	)

		$("div.markForm a.vote").click(
			function(){
				var mark=$(this).attr('mark');
				var object_id=$(this).parent().attr('object_id');
				$.ajax({
					url:'?mod=main&act=ajx_vote_film',
					data:{
						mark:mark,
						object_id:object_id
					},
					beforeSend:function(){
						//$('#rating_'+object_id).html('...');
						//$('#imdbRating_'+object_id).html('...');
						$('#yourMark_'+object_id).html('...');
					},
					dataType:'json',
					success:function(data)
					{
						$('div.markForm[object_id='+object_id+'] span.yourMark').html(mark);
						if(data)
						{
							//$('#rating_'+object_id).html(data['rating']+' / <span itemprop="bestRating">10</span>');
							//$('#imdbRating_'+object_id).html(data['votes']);
							$('#yourMark_'+object_id).html(data['yourMark']);
							$('#span_rating_val_'+object_id).html(parseFloat(data['rating']).toFixed(0)).css({"font-weight":"bolder"});
							$('#span_rating_votes_'+object_id).html(data['votes']).css({"font-weight":"bolder"});
							
							loadRating(object_id, data['rating']);
						}
					},
					type:'POST'
				})


				return false;
			}
		)
})

</script>

<?$last='thisWeek';?>



			<?foreach ($var['objects'] as $obj):?>

				<?$last=$obj['class'];?>


					<?if ($obj['new_year']):?>
					<div id="searchTitle" style='text-align: center; width: 100%; font-size: 18px; color: #898989; background: #f4f4f4; margin-top: 15px; margin-bottom: 30px; line-height: 40px;'><?=$obj['new_year']?></div>
					<?endif;?>


						<div id='searchItemFilms' class='<?=$obj["class"]?>'>
							<a href='<?=$obj['url']?>' class='searchPhoto' title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=$obj['year']?>"><img  src="<?=$obj['poster']?>" title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=$obj['year']?>" alt="<?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot;"></a>

							<div id='searchItemMainText'>

					<div id='actors' style='width: 100px; margin-top: 0px;'>
						<ul id='actorsList'>
 							<?if($obj['revurl']):?>
								<li>
									<a target="_blank" title="<?=sys::translate('main::film')?> <?=$obj['title']?> - <?=sys::translate('main::reviews')?>" href="<?=$obj['revurl']?>"><?=sys::translate('main::reviews')?></a>
								</li>
							<?endif;?>
 							<?if($obj['photurl']):?>
								<li>
									<a target="_blank" title="<?=sys::translate('main::film')?> <?=$obj['title']?> - <?=sys::translate('main::frames')?>" href="<?=$obj['photurl']?>"><?=sys::translate('main::frames')?></a>
								</li>
							<?endif;?>
 							<?if($obj['posturl']):?>
								<li>
									<a target="_blank" title="<?=sys::translate('main::film')?> <?=$obj['title']?> - <?=sys::translate('main::posters')?>" href="<?=$obj['posturl']?>"><?=sys::translate('main::posters')?></a>
								</li>
							<?endif;?>
 							<?if($obj['trailurl']):?>
								<li>
									<a target="_blank" title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=sys::translate('main::watchonline')?>" href="<?=$obj['trailurl']?>"><?=sys::translate('main::trailers')?></a>
								</li>
							<?endif;?>
						</ul>
					</div>
								<div id='searchItemTitle'>
									<a href='<?=$obj['url']?>' class='searchItemLink' title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=$obj['year']?>"><?=$obj['title']?></a><br>
									<span class='searchItemOriginal'><nobr><?=$obj['round']?></nobr></span>
								</div>


								<p class='searchItemText'>
										<?=$obj['rate']?>
										<?=$obj['second_row']?>
								</p>

								<p class='searchItemText'>
										<?=$obj['third_row']?>
								</p>
								
								<p class='searchItemText'>
										<?//if(sys::isDebugIP()) {?>
											<div id='searchTitle'><?=sys::translate('main::'.$obj["stars"]['form_title'])?> 
											&nbsp; | &nbsp; 
											<?=sys::translate('main::your_mark')?>: <span id="yourMark_<?=$obj["stars"]['object_id']?>" class="yourMark"><?=$obj["stars"]['yourMark']?></span>
											</div>
											<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
											<div id="rating-block">
													<div class="markForm" object_id="<?=$obj["stars"]['object_id']?>">
											
													<?for($i=1; $i<11; $i++):?>
														<?
															if($i<=floor($obj["stars"]['rating']))
															{
																$src=_IMG_URL.'star.png';
															}
															elseif($i==floor($obj["stars"]['rating']+1) || ($obj["stars"]['rating']<1 && $i==1))
															{
																if($obj["stars"]['rest']<0.25)
																	$src=_IMG_URL.'star0.png';
																if($obj["stars"]['rest']>=0.25) 
																	$src=_IMG_URL.'star1.png';
																if($obj["stars"]['rest']>=0.5)
																	$src=_IMG_URL.'star2.png';
																if($obj["stars"]['rest']>=0.75)
																	$src=_IMG_URL.'star3.png';
															}
												
															else
																$src=_IMG_URL.'star0.png';
														?>
														<a title="<?=sys::translate('main::rating')?> <?=$i?> / 10" class="vote" mark=<?=$i?> href=""><img num="<?=($i)?>" class="star" src="<?=$src?>" alt=""></a>
													<?endfor?>
											
													</div>
												
											</div>
										<?//}?>
								</p>
								<p class='searchItemText'>
											<div id='searchTitle' style="white-space: nowrap;">
												<?=sys::translate('main::film_waiting')?>&nbsp; | &nbsp; <?=sys::translate('main::your_mark')?>: 
												<span id="yourExpect" name='spanExpectYourMark_<?=$obj["expect"]['film_id']?>' class="yourMark"><?=$obj["expect"]["yourMark"]?></span>
											</div>
								</p>
											
								<p class='searchItemText'>											
												<div class="markForm" name='divExpectLine_<?=$obj["expect"]['film_id']?>' id="expect_lines" style="display:<?=(!$obj["expect"]['voted']?"none":"block")?>">
													<div id='waiting_bg'>
														<div id='waiting_yes' style="width:<?=$obj["expect"]['yes']?>%"><?=(($obj["expect"]['yes']>20)?$obj["expect"]['yes']:"")?></div>
														<div id='waiting_no'  style="width:<?=$obj["expect"]['no']?>%"><?=(($obj["expect"]['no']>20)?$obj["expect"]['no']:"")?></div>
													</div>
												</div>
								</p>
								<p class='searchItemText'  name='pExpectLineText_<?=$obj["expect"]['film_id']?>' style="margin-top:0; display:<?=(!$obj["expect"]['voted']?"none":"block")?>" >											
												<?=str_replace('"+yes+"', $obj["expect"]['yes'], sys::translate('main::expecting'))?>
								</p>
											
											<?if(!$obj["expect"]["voted"]) { // buttons ?>											
								<p class='searchItemText' style="margin-top:0" name='pExpectButtons_<?=$obj["expect"]['film_id']?>'>																					
											<div id='filmAwaiting' name='divExpectButtons_<?=$obj["expect"]['film_id']?>'>
												<span id='doYou'><?=sys::translate('main::areYouWaiting')?></span>
												<ul id='awaitingUl'>
													<li><a class="yes" onclick="return vote_mark(1, '<?=$obj["expect"]['film_id']?>');" href=""><span><?=sys::translate('main::yesLabel')?></span></a></li>
													<li><a class="no" onclick="return vote_mark(-1, '<?=$obj["expect"]['film_id']?>');" href=""><span><?=sys::translate('main::noLabel')?></span></a></li>
												</ul>
											</div>
								</p>			
											<?}//endif buttons?>											
							</div>

								<div id='premiereBigDate'>
								<span><?=$obj['day']?></span><br>
								<?=$obj['month']?>
								</div>

						<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
						</div>

						<div id='lineHr'>&nbsp;</div>


			<?endforeach;?>


	<?endif?>




					</div>



						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>

