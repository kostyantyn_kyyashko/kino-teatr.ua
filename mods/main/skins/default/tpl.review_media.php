				<div id='rightRating'>
     				<div id='yourVote'>
                        <span><?=sys::translate('main::authorRating')?></span>
						<div style='margin-left: 15px;'>
						<?
						for ($i=1;$i<=5;$i++)
						{
							if ($var['mark']/2>=$i)
							{
						?>

							<img src='<?=$skin_url?>images/redStar.png'>
						<?
							} else if (($var['mark']/2)<$i && ($var['mark']/2)>$i-1) {
						?>
							<img src='<?=$skin_url?>images/redStarHalf.png'>
						<?

							} else {
						?>
							<img src='<?=$skin_url?>images/redStarBlank.png'>
						<?

							}

						}
						?>
						</div>

     				</div>
     				<div id='totalRating'>
						<span><?=$var['rating']?></span>
						<?if($var['pre']):?><?=sys::translate('main::ratingFilm')?><?else:?><?=sys::translate('main::waiting')?><?endif;?>
     				</div>
     				<div id='rightVoteCount'><?=sys::translate('main::totalScore')?> <?=$var['votes']?></div>
				</div>

				<div id='rightFilmDescription'>
    				<div id='rightFilmTitle'><a title="<?=sys::translate('main::film')?> <?=$var['title']?> <?=$var['year']?>" href='<?=$var['poster']['url']?>'><?=$var['title']?></a></div>
					<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
    				<div id='rightFilmPoster'><a title="<?=sys::translate('main::film')?> <?=$var['title']?> <?=$var['year']?>" href='<?=$var['poster']['url']?>'><img src="<?=$var['poster']['image']?>" border='0'></a></div>
    				<div id='rightFilmText'>
    				<?if($var['directors']):?>
	    				<strong><?=sys::translate('main::director')?></strong><br>
						<?$i=1?>
						<?foreach($var['directors'] as $director):?>
							<a target="_blank" title="<?=$director['fio']?>" href="<?=$director['url']?>"><?=$director['fio']?></a><?if($i<count($var['directors'])):?>, <?endif;?>
							<?$i++?>
						<?endforeach;?>

						<br><br>
					<?endif?>


    					<strong><?=sys::translate('main::in_roles')?></strong><br>
    					<?$i=1?>
						<?foreach($var['actors'] as $actor):?>
							<a target="_blank" title="<?=$actor['fio']?>" href="<?=$actor['url']?>"><?=$actor['fio']?></a><?if($i<count($var['actors'])):?>, <?endif;?>
							<?$i++?>
						<?endforeach;?>

    				</div>
					<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
				</div>


