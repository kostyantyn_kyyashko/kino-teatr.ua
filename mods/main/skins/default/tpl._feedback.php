<?sys::useLib('content');?>
<?sys::useLib('main::captcha');?>
<?$_var['main::title']=$var['meta']['title']?>
<?sys::useJs('sys::form');?>
<?sys::useJs('main::main')?>
<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>
<?php	
	$captcha_public_key = main_captcha::$public_key;
?>

				<div class='myriad' id='myriad'><?=$_var['main::title']?></div>

					<div id='lineHrWide'>&nbsp;</div>


					<div id='news_page'>

	<?if($var['registred']):?>
		<?=sys::translate('main::thankYouForFeedBack')?>
		<p>&nbsp;<p>&nbsp;<p>&nbsp;
		<form>
		<center>
			<input type="submit" value="<?=sys::translate('main::send_more')?>" class="regBtn" style="height:30px;">
		</center>
		</form>
	<?else:?>

					<form id="form" onsubmit="return checkForm(this)" autocomplete="off" method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
						<table id="registration" class="user-profile-form">
							<tr>
								<th><?=sys::translate('main::feedback_form')?></th>
							</tr>
							<tr>
								<td class='first'>
                                    
									<div class="item<? if ($_err['nickname']) echo ' error' ?>">
    									<?=$var['fields']['nickname']['title']?> <span class="labelStar">*</span>
    									<br> <br>
                                        <?php $var['fields']['nickname']['parse_req'] = false; ?>
    									<div class="field"><?=sys_form::parseField('nickname',$var['fields']['nickname'],'style="width:100%;" maxlength="55" ')?><div class="attention"><div><?=sys::translate('main::registerAttentionDescrNickname')?></div></div></div>
                                    </div>
    								
                                    <div class='item<? if ($_err['email']) echo ' error' ?>'>
    									<?=$var['fields']['email']['title']?> <span class="labelStar">*</span>
    									<br> <br>
    									<div class="field"><?=sys_form::parseField('email',$var['fields']['email'],'style="width:100%;" maxlength="55" ')?><div class="attention"><div><?=sys::translate('main::registerAttentionDescrEmail')?></div></div></div>
    								</div>

    								<div class="item<? if ($_err['feedback_theme']) echo ' error' ?>">
    									<?=$var['fields']['feedback_theme']['title']?> <span class="labelStar">*</span>
    									<br> <br>
    									<div class="field"><?=sys_form::parseField('feedback_theme',$var['fields']['feedback_theme'],'')?><div class="attention" style="margin-top:0;"><div><?=sys::translate('main::registerAttentionDescrSex')?></div></div></div>
    								</div>

    								<div class="item<? if ($_err['text']) echo ' error' ?>">
    									<?=$var['fields']['text']['title']?> <span class="labelStar">*</span>
    									<br> <br>
                                        <?php $var['fields']['text']['parse_req'] = false; ?>
    									<div class="field"><?=sys_form::parseField('text',$var['fields']['text'],'style="width:100%; height:100px; border: 1px solid #D3D3D3; color: #333333;"')?><div class="attention"><div><?=sys::translate('main::registerAttentionDescrText')?></div></div></div>
                                    </div>
								</td>
							</tr>
					<?if($_user['id']==2):?>
							<!--tr>
								<th colspan="2"><?php echo sys::translate('main::control_question');?></th>
							</tr-->
							<tr>
								<td colspan="2" class="question">
									<div class="controlQuestion"><?php //echo $var['question']['question'];?></div>
									<!--input type="hidden" name="question_id" value="<?php //echo $var['question']['id'];?>"-->
									<?foreach ($var['question']['answers'] as $answer):?>
									<!--br><input type="radio" name="answer" value="<?php echo $answer;?>"--><?php //echo $answer;?>
									<?endforeach;?>
									<div class="g-recaptcha" data-sitekey="<?php echo $captcha_public_key;?>"></div>
								</td>
							</tr>
					<?endif?>		
							<tr>
								<td>
									<center>
										<input type="submit" name="cmd_feedback" value="<?=sys::translate('main::send')?>" class="regBtn" style="height:30px;">
									</center>
								</td>
							</tr>
							<tr>
								<td height="200">&nbsp;
								</td>
							</tr>
						</table>
					</form>
	<?endif?>

					</div>
					<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>

