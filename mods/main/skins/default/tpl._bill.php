<script type="text/javascript" src="/mods/main/skins/default/js/jquery.colorbox.js"></script>
<link rel="stylesheet" type="text/css" href="/mods/main/skins/default/js/colorbox/colorbox.css" />
<script type="text/javascript" src="/mods/main/skins/default/js/eventmap.js"></script>

<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title'];?>
<?$_var['active_menu']=1;?>
<?if($_GET['order']=='cinemas'):?>
<?$_var['active_child_menu']=2?>
<?else:?>
<?$_var['active_child_menu']=3?>
<?endif;?>
<?

if((isset($_GET['DEBUG']) && $_GET['DEBUG'] == '8Z)Vh(m2Kb') || $_SERVER['HTTP_HOST'] == 'test.kino-teatr.ua'){
	var_dump($var['objects']);
}
//WF(
function cmp($a, $b) 
{
	$af = strtolower($a['film']);
	$bf = strtolower($b['film']);
    if ($af == $bf) {
        return 0;
    }
    return ($af < $bf) ? -1 : 1;
}
//)WF

	$_var['right']=main_shows::showCalendar(21);
//	$_var['right']=main_shows::showCalendar(21,'onchange="window.location=\''.sys::rewriteUrl('?mod=main&act=bill&order='.$_GET['order'].'&date='.$_GET['date'].'').'?city_id=\'+this.value"');
?>

<?ob_start()?>
 <?require "bill_right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

<?sys::setMeta($var['meta']);?>

	<script type="text/javascript" src="<?=_ROOT_URL?>js/jquery.ui-slider.js"></script>
				<div class='myriadFilm' id='myriad'><?=$_var['main::title']?></div>
				
<?if($var['seo']):?>
<a href id='help' onclick='showHelp();return false;' style='margin-top: 4px;'>&nbsp;</a>
<?endif;?>

						<div id='lineBlank' style='height: 5px;'><img src='/images/blank.gif' height='1'></div>


<div id='news_page'>

<?if($var['seo']):?>
						<!--input type="text" name="" value="Введите улицу на которой находитесь" id="searchStreet"-->
<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
<div id='helpDiv' style='display: none;'>
<?=$var['seo'];?>
</div>

<?endif;?>
						<div id='afishaFilter' style=" ">
						<form method='POST'>

							<div id='filterPrice'>

								<span id='filterTitle'><?=sys::translate('main::choosePrice')?></span>
	                            <div id='filterLeftInput'>
									<input type="text" name="minCost" id="minCostPrice" value="<?=$var['mincost']?>"/>
								</div>
								<div id='filterSlider'>

									<div id='filterSliderStart'><?=sys::translate('main::priceFrom')?></div>
									<div id='filterSliderStop'><?=sys::translate('main::priceTo')?></div>
									<div id="filter"></div>
								</div>
	                            <div id='filterRightInput'>
									<input type="text" name='maxCost' id="maxCostPrice" value="<?=$var['maxcost']?>"/>
								</div>



							</div>

							<div id='filterTime'>
								<span id='filterTitle'><?=sys::translate('main::chooseYourTime')?></span>
	                            <div id='filterLeftInput'>
									<input type="text" name='minTime' id="minCost" value="0:00"/>
								</div>
								<div id='filterSlider'>

									<div id='filterSliderStart'><?=sys::translate('main::timeFrom')?></div>
									<div id='filterSliderStop'><?=sys::translate('main::timeTo')?></div>
									<div id="filterTim"></div>
								</div>
	                            <div id='filterRightInput'>
									<input type="text" name='maxTime' id="maxCost" value="23:59"/>
								</div>
							</div>

						<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
							<center>
								<input type='submit' class='filterFind' value='<?=sys::translate('main::lookFor')?>'>
							</center>

					</form>

						<script type="text/javascript">
							jQuery("#filterTim").slider({
								min: 0,
								max: 1440,
								values: [<?=$var['mintime']?>,<?=$var['maxtime']?>],
								step: 30,
								range: true,
								create: function(event, ui) {
									jQuery("input#minCost").val(
										(jQuery("#filterTim").slider("values",0)-jQuery("#filterTim").slider("values",0)%60)/60+':'+(jQuery("#filterTim").slider("values",0)%60)/10+'0'
									);
									jQuery("input#maxCost").val(
										(jQuery("#filterTim").slider("values",1)-jQuery("#filterTim").slider("values",1)%60)/60+':'+(jQuery("#filterTim").slider("values",1)%60)/10+'0'
									);
							    },
								stop: function(event, ui) {
									jQuery("input#minCost").val(
										(jQuery("#filterTim").slider("values",0)-jQuery("#filterTim").slider("values",0)%60)/60+':'+(jQuery("#filterTim").slider("values",0)%60)/10+'0'
									);
									jQuery("input#maxCost").val(
										(jQuery("#filterTim").slider("values",1)-jQuery("#filterTim").slider("values",1)%60)/60+':'+(jQuery("#filterTim").slider("values",1)%60)/10+'0'
									);
							    },
							    slide: function(event, ui){
									jQuery("input#minCost").val(
										(jQuery("#filterTim").slider("values",0)-jQuery("#filterTim").slider("values",0)%60)/60+':'+(jQuery("#filterTim").slider("values",0)%60)/10+'0'
									);
									jQuery("input#maxCost").val(
										(jQuery("#filterTim").slider("values",1)-jQuery("#filterTim").slider("values",1)%60)/60+':'+(jQuery("#filterTim").slider("values",1)%60)/10+'0'
									);
							    }
							});

							jQuery("#filter").slider({
								min: 0,
								max: 500,
								values: [<?=$var['mincost']?>,<?=$var['maxcost']?>],
								step: 5,
								range: true,
								create: function(event, ui) {
									jQuery("input#minCostPrice").val(jQuery("#filter").slider("values",0));
									jQuery("input#maxCostPrice").val(jQuery("#filter").slider("values",1));
							    },
								stop: function(event, ui) {
									jQuery("input#minCostPrice").val(jQuery("#filter").slider("values",0));
									jQuery("input#maxCostPrice").val(jQuery("#filter").slider("values",1));
							    },
							    slide: function(event, ui){
									jQuery("input#minCostPrice").val(jQuery("#filter").slider("values",0));
									jQuery("input#maxCostPrice").val(jQuery("#filter").slider("values",1));
							    }
							});

							function hideCinemas(id)
							{
								if($('#hide_'+id).hasClass('hide'))
								{
									$('#hide_'+id).removeClass('hide');
									$('#hide_'+id).addClass('show');
									$('#billCinema_'+id).css('display', 'none');

									$.ajax({
										url:'?mod=main&act=ajx_cinema_hide',
										data:{
											id:id,
											hide:1
										},
										dataType:'json',
										type:'POST'
									})

								} else {
									$('#hide_'+id).removeClass('show');
									$('#hide_'+id).addClass('hide');
									$('#billCinema_'+id).css('display', 'block');

									$.ajax({
										url:'?mod=main&act=ajx_cinema_hide',
										data:{
											id:id,
											hide:0
										},
										dataType:'json',
										type:'POST'
									})



								}
							}

						</script>


						</div>

<script type="text/javascript">
	function ShowNote(id)
	{
		$(".noteDiv").css("display","none");
		$("#noteDiv_"+id).toggle("slow");
		return false;
	}
</script>

<script type="text/javascript">
function subscribeCinema(cinema_id, on_off)
{
  var f = $("#user_cinema_subscribe_form");
  f.find(":hidden[name='act']").val(on_off);
  f.find(":hidden[name='id_cinema']").val(cinema_id);
  f[0].submit();
  return false;
} 
</script>

<form method="POST" id="user_cinema_subscribe_form">
	<input type="hidden" name="_task" value="user_cinema_subscribe">
	<input type="hidden" name="act" value="1">
	<input type="hidden" name="id_cinema" value="0">	
</form>	

	<?if($var['objects']):?>
		<script src="https://bilet.vkino.com.ua/extras/widget/1.7a/main.min.js"></script>
		<?if($_GET['order']=='cinemas'):?>

			<?$i=1;?>

			<?foreach ($var['objects'] as $obj):?>
			<?
				// массив с индексами для *
				$notes = array();
				foreach ($obj['shows'] as $show)
					foreach ($show['times'] as $time)
					{
						$trim_note = $time["note"];
						if($trim_note && !in_array($trim_note, $notes)) $notes[] = $trim_note;
					}
			?>
				<?if($obj['countfilms']>0):?>

						<span id='afishaKtName'>
							<a href='<?=$obj['url']?>' title='<?=sys::translate('main::cinema')?> <?=$obj['title']?>'><?=sys::translate('main::cinema')?> <?=$obj['title']?></a>
						 	<a href <?if($obj['hide']):?>class='show'<?else:?>class='hide'<?endif;?> id='hide_<?=$obj['id']?>' onclick='hideCinemas(<?=$obj['id']?>); return false;'>&nbsp;</a>
							<?if ($obj['ticket_url']=='vkino'):?>
								<a href='#' title='<?=sys::translate('main::buy_ticket')?>' id='btn_tckts' onclick="{alert('<?=sys::parseModTpl('main::buy_alert')?>');return false;}"><?=sys::translate('main::buy_ticket')?></a>
		                    <?endif;?>

							<? if($_user['id']!=2):?>
							<? $user_cinema_subscribed = main_cinemas::getUserCinemaSubscribed($_user["id"], $obj['id']); ?>
								<a 	href='#' 
									title='<?=sys::translate($user_cinema_subscribed?'main::untracking':'main::tracking')?> <?=sys::translate('main::cinema_shows')?>' 
									id='btn_tckts' 
									onclick="{return subscribeCinema(<?=intval($obj['id'])?>, <?=$user_cinema_subscribed?0:1?>);}"
								>
								<?=sys::translate($user_cinema_subscribed?'main::unsubscribe':'main::subscribe')?></a>	
	<?else:?>
	<a href='<?=sys::rewriteUrl('?mod=main&act=register')?>' title='<?=sys::translate($user_cinema_subscribed?'main::untracking':'main::tracking')?> <?=sys::translate('main::cinema_shows')?>' id='btn_tckts'"><?=sys::translate('main::subscribe')?></a>		
										
							<? endif; ?>				
						</span>

						<div id='billCinema_<?=$obj['id']?>' <?if($obj['hide']):?>style='display:none;'<?endif;?>>

						<?if($obj['notice']):?>
							<div id="noticeBillDiv"><?=$obj['notice']?></div>
							<div id='lineBlank' style='height: 14px;'><img src='/images/blank.gif' height='1'></div>
						<?endif;?>

						<div id='afishaItemTitle'>
							<div class='filmName'><?=sys::translate('main::film')?></div>
							<div class='filmZal'><?=sys::translate('main::hall')?></div>
							<div class='filmShows'><?=sys::translate('main::shows')?></div>
							<div class='filmPrices'><?=sys::translate('main::prices')?></div>
						</div>

						<?
						//WF(
						$arrshows = $obj['shows'];
						usort($arrshows, "cmp");
						
						foreach ($arrshows as $show):
						//foreach ($obj['shows'] as $show):
						//WF)?>
						<div id='afishaItem'>
							<div class='filmName'><a href='<?=$show['film_url']?>' title="<?=sys::translate('main::film')?> <?=$show['film']?> <?=$show['year']?>"><?=$show['film']?></a></div>
							<div class='filmZal'>
							<?if($show['hall']):?>
								<?if($show['scheme_url']):?>
									<a title="<?=sys::translate('main::hall_scheme')?>" onclick="window.open('<?=$show['scheme_url']?>','','<?=sys::setWinParams('770','570')?>'); return false;" href="<?=$show['scheme_url']?>"><?=$show['hall']?></a>
								<?else:?>
									<?=$show['hall']?>
								<?endif;?>
							<?else:?>
							&mdash;
							<?endif;?>
							</div>
							<div class='filmShows ' style='width: 257px;'>

						<?
									$is_2d = 0;
									$has_3d = 0;
									$timesa = array();
//if(sys::isDebugIP()) sys::printR($show['times']);
									foreach ($show['times'] as $time)
									{
										$has_3d += $time['seans_3d'];
										$star_class = $time['past']?"mark_star_time_past":"mark_star_time";

										$note = array_search($time['note'],$notes,true);
										$note = ($note===FALSE)?"":'<sup class="'.$star_class.'"><a href=# onclick=\'return ShowNote('.$obj['id'].')\'>'.str_repeat("*",$note+1)."</a></sup>";

										$is_2d = ($show['film_3d'] && $show['3d'] && !$time['seans_3d'])?"<sup class='mark3d'>2D</sup>":"";
										if($time['past'])
										{
											$timesa[] = "<span class='timepast'>".strip_tags($time['time'])."$is_2d</span>$note";
										} else{
											$timesa[] = "<span class='time'>".$time['time']."$is_2d</span>$note";
										}
									}

									$timesar = implode('<span class="delimiter">, </span>', $timesa);
									?>

									<?=$timesar?>

							</div>
							<div class='filmShows ' style='width: 50px;'>
								<?if($show['film_3d'] && $show['3d'] && $has_3d):?>
									<center><img src='<?=$skin_url?>images/3dglass.jpg'></center>
								<?else:?>
									&nbsp;
								<?endif;?>
							</div>
							<?if($show['price_range']):?><div class='filmPrices'><?=$show['price_range']?> <?=sys::translate('main::grn')?></div><?endif?>
							<!--div class='filmIgo'><a href='#'><img src='images/Igo.jpg'></a></div-->
						</div>

						<?endforeach;?><?// of foreach ($obj['shows'] as $show)?>


						<?if(sizeof($notes)):?>
								<div class="noteDiv" id='noteDiv_<?=$obj['id']?>'>
								<?
									$infotext = "<table width='100%'>";
									foreach($notes as $index=>$text)
									{
										$total_index = sizeof($notes);
										$infotext .= "<tr><td width='2%' nowrap valign=top>".str_repeat("*",$index+1)."</td><td>-&nbsp;$text</td></tr>";
									};
									$infotext .= "</table>";
									echo $infotext;
								?>
								</div>
						<?endif;?>

						<div id='lineBlank' style='height: 15px;'><img src='/images/blank.gif' height='1'></div>

						<?if($i==3):?>
								<?=banners::showBanners('v3_secondbanner')?>
						<?endif;?>
						</div>
				<?endif;?><?// ?>

			<?$i++;?>

			<?endforeach;?><?// foreach ($var['objects'] as $obj):?>

			<?if($i<=3):?>
								<?=banners::showBanners('v3_secondbanner')?>
			<?endif;?>
		<?else:?>
		<?// 	if($_GET['order']=='cinemas'):?>
					<script>

					function showFBlock(block)
					{
						if ($('#FBlock'+block).css('display')=='none')
						{
							$('#FBlock'+block).fadeIn('slow');
							$('.FBlockBut'+block).html('<?=sys::translate('main::hide')?>');
						} else {
							$('#FBlock'+block).fadeOut('fast');
							$('.FBlockBut'+block).html('<?=sys::translate('main::showF')?>');
						}
					}

					</script>

			<?$i=1;?>

			<?foreach ($var['objects'] as $obj):?>
			<?
				// массив с индексами для *
				$notes = array();
				foreach ($obj['shows'] as $show)
					foreach ($show['times'] as $time)
					{
						$trim_note = $time["note"];
						if($trim_note && !in_array($trim_note, $notes)) $notes[] = $trim_note;
					}
			?>

				<?if($obj['countfilms']>0):?>
				<?
					// By Mike
					// Берем первую запись о сеансе и получаем тип фильма
					$film_3d = "";
					foreach ($obj['shows'] as $show){
						$film_3d = ($show['film_3d']==1)?"&nbsp;<sup class=mark3d>3D</sup>":"";
						break;
					}
					$isBuy = false;
					foreach ($obj['shows'] as $show)
 					 if($show['ticket_url']=='vkino')
					 {
						$isBuy = true;
						break;
					 }
				?>

					<div id='afishaKtName' style='margin-bottom: 0px;'>
							<span style='float: left;'><a href='<?=$obj['url']?>' title='<?=sys::translate('main::film')?> <?=$obj['title']?> <?=$obj['year']?>'><?=$obj['title']?> <?=$film_3d?></a></span>
						</div>


							<div id='searchItemFilms' class='afishaFBlock'>
								<a href='<?=$obj['url']?>' class='searchPhoto'><img  src="<?=$obj['image']?>" title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=$obj['year']?>" alt="Фильм &quot;<?=$obj['title']?>&quot;">
								</a>

								<div id='searchItemMainText'>
									<div id='searchItemTitle'>
										<a href='<?=$obj['url']?>' title='<?=sys::translate('main::film')?> <?=$obj['title']?> <?=$obj['year']?>' class='searchItemLink'><?=$obj['round']?></a><br>
									</div>

									<p class='searchItemText'>
											<?=$obj['rate']?>
											<?=$obj['second_row']?>
									</p>

									<p class='searchItemText'>
											<?=$obj['third_row']?>
									</p>
								</div>

								<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
							</div>
							<div class='FBlockShow'>
																						<?if($isBuy):?>
									<a href='#' title='<?=sys::translate('main::buy_ticket')?>' id="btn_tckts" onclick="{alert('<?=sys::parseModTpl('main::buy_alert')?>');return false;}"><?=sys::translate('main::buy_ticket')?></a>
			                    <?endif;?>
								<a href onclick='showFBlock(<?=$obj['id']?>); return false;' class='FBlockBut<?=$obj['id']?>' id="boxDetails"><?=sys::translate('main::showF')?></a>
							</div>

						<div id='FBlock<?=$obj['id']?>' style='display: none;'>

					<div id='lineBlank' style='height: 15px;'><img src='/images/blank.gif' height='1'></div>
							<div id='afishaItemTitle'>
							<div class='filmName'><?=sys::translate('main::cinema')?></div>
							<div class='filmZal'><?=sys::translate('main::hall')?></div>
							<div class='filmShows '><?=sys::translate('main::shows')?></div>
							<div class='filmPrices'><?=sys::translate('main::prices')?></div>
						</div>
						<?foreach ($obj['shows'] as $show):?>
						<div id='afishaItem'>
							<div class='filmName'><a href='<?=$show['cinema_url']?>' title='<?=sys::translate('main::cinema')?> <?=$show['cinema']?>'><?=$show['cinema']?></a></div>
							<div class='filmZal'>
							<?if($show['hall']):?>
								<?if($show['scheme_url']):?>
									<a title="<?=sys::translate('main::hall_scheme')?>" onclick="window.open('<?=$show['scheme_url']?>','','<?=sys::setWinParams('770','570')?>'); return false;" href="<?=$show['scheme_url']?>"><?=$show['hall']?></a>
								<?else:?>
									<?=$show['hall']?>
								<?endif;?>
							<?else:?>
							&mdash;
							<?endif;?>
							</div>
							<div class='filmShows   ' style='width: 257px;'>

						<?
									$is_2d = 0;
									$has_3d = 0;
									$timesa = array();
//if(sys::isDebugIP()) sys::printR($show['times']);
									foreach ($show['times'] as $time)
									{
										$has_3d += $time['seans_3d'];
										$star_class = $time['past']?"mark_star_time_past":"mark_star_time";

										$note = array_search($time['note'],$notes,true);
										$note = ($note===FALSE)?"":'<sup class="'.$star_class.'"><a href=# onclick=\'return ShowNote('.$obj['id'].')\'>'.str_repeat("*",$note+1)."</a></sup>";

										$is_2d = ($show['film_3d'] && $show['3d'] && !$time['seans_3d'])?"<sup class='mark3d'>2D</sup>":"";
										if($time['past'])
										{
											$timesa[] = "<span class='timepast'>".$time['time']."$is_2d</span>$note";
										} else{
											$timesa[] = "<span class='time'>".$time['time']."$is_2d</span>$note";
										}
									}

									$timesar = implode('<span class="delimiter">, </span>', $timesa);
									?>

									<?=$timesar?>

							</div>

							<div class='filmShows' style='width: 50px;'>
								<?if($show['film_3d'] && $show['3d'] && $has_3d):?>
									<center><img src='<?=$skin_url?>images/3dglass.jpg'></center>
								<?else:?>
									&nbsp;
								<?endif;?>
							</div>


							<div class='filmPrices'><?=$show['price_range']?> <?=sys::translate('main::grn')?></div>
							<!--div class='filmIgo'><a href='#'><img src='images/Igo.jpg'></a></div-->
						</div>

						<?endforeach;?>

						<?if(sizeof($notes)):?>
								<div class="noteDiv" id='noteDiv_<?=$obj['id']?>'>
								<?
									$infotext = "<table width='100%'>";
									foreach($notes as $index=>$text)
									{
										$total_index = sizeof($notes);
										$infotext .= "<tr><td width='2%' nowrap valign=top>".str_repeat("*",$index+1)."</td><td>-&nbsp;$text</td></tr>";
									};
									$infotext .= "</table>";
									echo $infotext;
								?>
								</div>
						<?endif;?>

						</div>

						<div id='lineBlank' style='height: 15px;'><img src='/images/blank.gif' height='1'></div>


                  <?endif;?>


					<?if($i==3):?>
										<?=banners::showBanners('v3_secondbanner')?>
					<?endif;?>
			<?$i++;?>

			<?endforeach;;?>

			<?if ($i<=3):?>
								<?=banners::showBanners('v3_secondbanner')?>
			<?endif;?>
		<?endif?>

	<?else:?>
		<center class="xErr"><?=sys::translate('main::no_shows')?></center>
	<?endif?>
</div>


<?
	$date = isset($_GET["date"])?$_GET["date"]:false;
	if(!$date) $date = isset($_POST["date"])?$_POST["date"]:false;
	if(!$date) $date = date("d.m.Y");
?>
	
<script>
$(function(){
	$("div.filmShows").attr("data-date", '<?=$date?>');
});
</script>

