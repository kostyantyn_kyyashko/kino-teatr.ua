<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=6?>
<?$_var['active_child_menu']=8?>
<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

				<div class='myriadFilm' id='myriad'><?=$var['title']?></div>
				
				<div id='lineHrWide'>&nbsp;</div>			

				<?=main_persons::showPersonTabs($var['person_id'],$var['person'],'films');?>

				<div id='lineBlank' style='height: 14px;'><img src='/images/blank.gif' height='1'></div>

				<div id='news_page'>

		<ul id='konksList'>
	  	<?foreach($var['professions'] as $profession):?>
			<li>
				<div>
					<h2><?=$profession['title']?></h2>
				</div>
			</li>
			
			<?foreach ($profession['objects'] as $obj):?>
			<li>
				<div>
					<a title="<?=sys::translate('main::film')?> <?=$obj['film']?>" target="_blank" href="<?=$obj['film_url']?>"><?=$obj['film']?> / <?=$obj['title_orig']?> (<?=$obj['year']?>)</a> <?if($obj['role']):?>- <?=$obj['role']?><?endif?>
				</div>
			</li>
			<?endforeach;?>
			
		<?endforeach;?>
		</ul>

					</div>

	<?=banners::showBanners('v3_secondbanner')?>
