<?sys::setTpl('main::__rss');?>
<channel>
<title><?=sys::translate('main::kinoportal_kino_teatr')?><?=sys::translate('main::rss_review')?></title>
<link><?=_ROOT_URL?></link>
<image>
	<url><?=_ROOT_URL?>public/img/logo_ya.gif</url>
	<title><?=sys::translate('main::kinoportal_kino_teatr')?></title>
	<link><?=_ROOT_URL?></link>
</image>
<description><?=sys::parseModTpl('main::reviews_rss_description')?></description>

<?foreach ($var['objects'] as $obj):?>
<item>
	<title><?=$obj['title']?></title>
	<link><?=$obj['url']?></link>
	<description><?=$obj['intro']?></description>
	<pubDate><?=$obj['date']?></pubDate>
	<?if($obj['image']):?>
		<enclosure url="<?=$obj['image_src']?>" type="<?=$obj['image_mime']?>" length="<?=$obj['image_size']?>"/>
	<?endif?>
	<yandex:full-text><?=htmlentities(strip_tags($obj['text']),false,'utf-8')?></yandex:full-text>
</item>
<?endforeach;?>
</channel>

