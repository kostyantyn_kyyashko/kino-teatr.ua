<?sys::useLib('content');?>
<?sys::useLib('main::captcha');?>
<?$_var['main::title']=$var['meta']['title']?>
<?sys::useJs('sys::form');?>
<?sys::useJs('main::main')?>
<?sys::setMeta($var['meta']);?>
<?//main::showGemius('AkHg6yLtt6gJIzXxovzv8.WpXtbpebu7zu4AyPlugYP.h7');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>
<?php	
	$captcha_public_key = main_captcha::$public_key;
?>

				<div class='myriad' id='myriad'><?=$_var['main::title']?></div>

					<div id='lineHrWide'>&nbsp;</div>


					<div id='news_page'>

	<?if($var['registred']):?>

	<script>
		alert('<?=sys::translate('main::thankYouFroRegistration')?>');
	</script>

	<?else:?>

		<?if($var['imregistred']):?>

		<script>
			alert('<?=sys::translate('main::thankYouFroRegistration')?>');
		</script>
		<?endif;?>


	<?endif?>

					<form id="form" onsubmit="return checkForm(this)" autocomplete="off" method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
						<table id="registration" class="user-profile-form">
							<tr>
								<th colspan="2"><?=sys::translate('main::info')?></th>
							</tr>
							<tr style="vertical-align:top;">
								<td class='first' style="padding:20px 0 0 20px;">
                                    <div class="item<? if ($_err['login']) echo ' error' ?>">
    									<div class="label"><?=$var['fields']['login']['title']?> <span class="labelStar">*</span></div>
    									<?php $var['fields']['login']['parse_req'] = false; ?>
    									<div class="field"><?=sys_form::parseField('login',$var['fields']['login'],' class="regTextField login" autocomplete="off" tabindex="1"')?><div class="attention"><div><?=sys::translate('main::registerAttentionDescrLogin')?></div></div></div>
                                    </div>
									
                                    <div style="display:none">
									    <div class="item<? if ($_err['nickname']) echo ' error' ?>">
	    									<div class="label"><?=$var['fields']['nickname']['title']?> <span class="labelStar">*</span></div>
	    									
	                                        <?php $var['fields']['nickname']['parse_req'] = false; ?>
	    									<div class="field"><?=sys_form::parseField('nickname',$var['fields']['nickname'],' class="regTextField nickname" autocomplete="off" tabindex="1"')?><div class="attention"><div><?=sys::translate('main::registerAttentionDescrNickname')?></div></div></div>
            	                        </div>
                                    </div>

    								<div class='item<? if ($_err['password']) echo ' error' ?>'>
    									<div class="label"><?=$var['fields']['password']['title']?> <span class="labelStar">*</span></div>
    									
                                        <?php $var['fields']['password']['parse_req'] = false; ?>
    									<div class="field"><?=sys_form::parseField('password',$var['fields']['password'],'class="regTextField" autocomplete="off" tabindex="2" maxlength="16" size="16"')?><div class="attention"><div><?=sys::translate('main::registerAttentionDescrPassword')?></div></div></div>
    								</div>
    								<div class='item<? if ($_err['password2']) echo ' error' ?>'>
    									<div class="label"><?=$var['fields']['password2']['title']?> <span class="labelStar">*</span></div>
    									
                                        <?php $var['fields']['password2']['parse_req'] = false; ?>
    									<div class="field"><?=sys_form::parseField('password2',$var['fields']['password2'],'class="regTextField" autocomplete="off" tabindex="3" maxlength="16" size="16" id="pass2" onchange="main_checkPassword(this);"')?><div class="attention"><div><?=sys::translate('main::registerAttentionDescrPassword2')?></div></div></div>
    								</div>

	    								<div class='item<? if ($_err['email']) echo ' error' ?>'>
	    									<div class="label"><?=$var['fields']['email']['title']?> <span class="labelStar">*</span></div>
	    									
	    									<div class="field"><?=sys_form::parseField('email',$var['fields']['email'],'class="regTextField" autocomplete="off" maxlength="55" tabindex="4" size="25"')?><div class="attention"><div><?=sys::translate('main::registerAttentionDescrEmail')?></div></div></div>
	    								</div>
    								<div style="display:none">
	    								<div class='item<? if ($_err['email2']) echo ' error' ?>'>
	    									<div class="label"><?=$var['fields']['email2']['title']?> <span class="labelStar">*</span></div>
	    									
	    									<div class="field"><?=sys_form::parseField('email2',$var['fields']['email2'],'id="email2" autocomplete="off" style="margin-bottom: 15px;" tabindex="5" class="regTextField" maxlength="55" size="25" onchange="main_checkEmail(this);"')?><div class="attention"><div><?=sys::translate('main::registerAttentionDescrEmail2')?></div></div></div>
	    								</div>
    								</div>	
								</td>
								<td>
    								<div style="display:none">
	                                    <div class="item<? if ($_err['firstname']) echo ' error' ?>">
	    									<div class="label"><?=$var['fields']['firstname']['title']?> <span class="labelStar">*</span></div>
	    									
	                                        <?php $var['fields']['firstname']['parse_req'] = false; ?>
	    									<div class="field"><?=sys_form::parseField('firstname',$var['fields']['firstname'],'maxlength="55" autocomplete="off" tabindex="6" class="regTextField" ')?><div class="attention"><div><?=sys::translate('main::registerAttentionDescrFirstname')?></div></div></div>
	                                    </div>
	    								<div class="item<? if ($_err['lastname']) echo ' error' ?>">
	    									<div class="label"><?=$var['fields']['lastname']['title']?> <span class="labelStar">*</span></div>
	    									
	                                        <?php $var['fields']['lastname']['parse_req'] = false; ?>
	    									<div class="field"><?=sys_form::parseField('lastname',$var['fields']['lastname'],'maxlength="55" autocomplete="off" tabindex="7" class="regTextField"')?><div class="attention"><div><?=sys::translate('main::registerAttentionDescrLastname')?></div></div></div>
	    								</div>
	    								<div class="item<? if ($_err['birth_date']) echo ' error' ?>">
	    									<div class="label"><?=$var['fields']['birth_date']['title']?> (<?=strftime($_cfg['sys::date_format'])?>) <span class="labelStar">*</span></div>
	    									
	                                        <?php $var['fields']['birth_date']['parse_req'] = false; ?>
	    									<div class="field"><?=sys_form::parseField('birth_date',$var['fields']['birth_date'],'tabindex="8" autocomplete="off"')?><div class="attention" style="margin-top:0;"><div><?=sys::translate('main::registerAttentionDescrBirth')?></div></div></div>
	    								</div>
	    								<div class="item<? if ($_err['sex']) echo ' error' ?>">
	    									<?=$var['fields']['sex']['title']?> <span class="labelStar">*</span>
	    									
	    									<div class="field"><?=sys_form::parseField('sex',$var['fields']['sex'],'tabindex="9" autocomplete="off"')?><div class="attention" style="margin-top:0;"><div><?=sys::translate('main::registerAttentionDescrSex')?></div></div></div>
	    								</div>
	    								<div class="item<? if ($_err['city_id']) echo ' error' ?>">
	    									<?=$var['fields']['city_id']['title']?> <span class="labelStar">*</span>
	    									
	    									<div class="field"><?=sys_form::parseField('city_id',$var['fields']['city_id'], ' tabindex="10" autocomplete="off" style="margin-bottom: 15px;"')?><div class="attention" style="margin-top:0;"><div><?=sys::translate('main::registerAttentionDescrCity')?></div></div></div>
	    								</div>
    								</div>
    								
									<div class="item<? if ($_err['phone']) echo ' error' ?>">
    									<div class="label"><?=$var['fields']['phone']['title']?> </div>
    									<div class="field">+<?=sys_form::parseField('phone',$var['fields']['phone'])?><div class="attention" style="margin-top:0;"></div></div>
    								</div>
								</td>
							</tr>

							<tr style="display:none">
								<th colspan="2"><?=sys::translate('main::settings')?></th>
							</tr>

							<tr style="display:none">
								<td  class='first'>
									<div class="label"><?=$var['fields']['lang_id']['title']?>: <span class="labelStar">*</span></div>
									<?=sys_form::parseField('lang_id',$var['fields']['lang_id'], ' tabindex="11" autocomplete="off" style="margin-bottom: 15px;"')?>
								</td>
								<td>
									<div class="label"><?=$var['fields']['timezone']['title']?>: <span class="labelStar">*</span></div>
									<?=sys_form::parseField('timezone',$var['fields']['timezone'], ' tabindex="12" autocomplete="off" style="margin-bottom: 15px;"')?>
								</td>
							</tr>
							<!--tr>
								<th colspan="2"><?php echo sys::translate('main::control_question');?></th>
							</tr-->
							
							<tr>
								<td colspan="2" class="question">
									<div class="controlQuestion"><?php //echo $var['question']['question'];?></div>
									<!--input type="hidden" name="question_id" value="<?php //echo $var['question']['id'];?>"-->
									<?foreach ($var['question']['answers'] as $answer):?>
									<!--br><input type="radio" name="answer" value="<?php echo $answer;?>"--><?php //echo $answer;?>
									<?endforeach;?>
									<div class="g-recaptcha" data-sitekey="<?php echo $captcha_public_key;?>"></div>
								</td>
							</tr>
							<tr>
								<td colspan="2">
								<br>
									<center>
										<input type="submit" name="cmd_register" value="<?=sys::translate('main::register2')?>" class="regBtn">
									</center>
								</td>
							</tr>
						</table>
					</form>

					</div>
					<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
<br>
<br>
<?=sys::parseModTpl('main::login_text','html')?>