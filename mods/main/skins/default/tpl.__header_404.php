<?
sys::useLib('counters');
sys::useLib('banners');
sys::useLib('main::menu');
sys::useLib('content');
sys::useLib('sys::form');
sys::jsInclude('sys::sys');

?>

<!DOCTYPE html>
<html lang='ru' xml:lang='ru' xmlns='http://www.w3.org/1999/xhtml'>

<head>
	<title><?=htmlspecialchars($_meta_title)?></title>
	<meta name="keywords" content="<?=htmlspecialchars($_meta_keywords)?>">
	<meta name="DESCRIPTION" content="<?=htmlspecialchars($_meta_description)?>">
	<meta name="google-site-verification" content="vMm6kJxMqityxU8m-A-SbMiEaqVBrhVS5yfGrLlJzps" />

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<link rel="stylesheet" href="<?=$skin_url?>404.css" type="text/css" />
	<script type="text/javascript" src="<?=$skin_url?>js/jquery.js"></script>

    <?=sys::attentionUserInfo()?>
</head>

<body onclick="setTimeout(hideList, 50);" onload="$('#myriad').css('visibility', 'visible');">

<div class="container">
	<a href='<?=_ROOT_URL?>' class='logo'>&nbsp;</a>
	<div class="search">
	<form action="<?=_ROOT_URL?>ru/main/search.phtml" name="searchform" onsubmit="return SubmitSearch()" method="GET" id="search">
		<input type='text' id='searchField' value='Поиск' onclick='if (this.value==this.defaultValue) this.value=""'>
		<input type='image' src='<?=$skin_url?>images/searchButton.png' id='searchButton'>
	</form>
	</div>


