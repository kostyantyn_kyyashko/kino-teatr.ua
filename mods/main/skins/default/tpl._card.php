<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title'];?>
<?sys::setMeta($var['meta']);?>
<?sys::useJs('sys::form');?>
<?//main::showGemius('102aeOc.4zH56sxGrbFf_ZRLTGSutdBWG0HXY4ygiNL.n7');?>

<div id="main_card">
	<?if(!$_GET['sent']):?>
		<div class="xInfo">
			<?=sys::translate('main::invite_on_film')?> <a href="<?=$var['film']['url']?>"><?=$var['film']['title']?></a>
			<?=sys::translate('main::in_cinema')?> <a href="<?=$var['cinema']['url']?>"><?=$var['cinema']['title']?></a>
			<?=$var['date']?> <?=$_GET['time']?>
		</div>
		<?if($var['preview']):?>
			<div id="main_card_prevew" style="display:none;">
				<?=$var['preview']?>
			</div>
			<br>
			<iframe frameborder="0" style="width:100%; overflow-y:scroll; overflow-x:hidden; height:500px;" src="<?=$var['preview_url']?>"></iframe>	
			<br>
		<?endif?>
		<br>
		<form id="form" onsubmit="return checkForm(this)" method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
			<table class="x xTable">
				<tr>
					<th width="1"><nobr><?=$var['fields']['recipient_name']['title']?>:*</nobr></th>
					<td width="100%">
						<?=sys_form::parseField('recipient_name',$var['fields']['recipient_name'],'maxlength="255" style="width:100%;"')?>
					</td>
				</tr>
				<tr>
					<th width="1"><nobr><?=$var['fields']['recipient_email']['title']?>:*</nobr></th>
					<td width="100%">
						<?=sys_form::parseField('recipient_email',$var['fields']['recipient_email'],'maxlength="255" style="width:100%;"')?>
					</td>
				</tr>
				<tr>
					<th width="1"><nobr><?=$var['fields']['sender_name']['title']?>:*</nobr></th>
					<td width="100%">
						<?=sys_form::parseField('sender_name',$var['fields']['sender_name'],'maxlength="255" style="width:100%;"')?>
					</td>
				</tr>
				<tr>
					<th width="1"><nobr><?=$var['fields']['sender_email']['title']?>:*</nobr></th>
					<td width="100%">
						<?=sys_form::parseField('sender_email',$var['fields']['sender_email'],'maxlength="255" style="width:100%;"')?>
					</td>
				</tr>
				<tr>
					<th colspan="2">
						<?=sys::translate('main::text')?>
					</th>
				</tr>
				<tr>
					<td colspan="2">
						<?=sys_form::parseField('text',$var['fields']['text'],'style="width:100%; height:150px;"')?>
					</td>
				</tr>
				<tr>
					<th colspan="2">
						<?=sys::translate('main::card')?>
					</th>
				</tr>
				<tr>
					<td class="x" colspan="2" style="text-align:center;">
						<?foreach ($var['cards'] as $card):?>
							<div class="xInfo" style="padding:5px; float:left; margin:0 5px 5px 0;">
								<a target="_blank" href="<?=$card['url']?>"><img width="214" height="160" src="<?=$card['src']?>" alt=""></a><br>
								<input class="xCheckbox" <?if($_POST['card_id']==$card['id']):?>checked<?endif;?> type="radio" name="card_id" value="<?=$card['id']?>">
							</div>
						<?endforeach;?>	
					</td>
				</tr>
				<!--
				<tr>
					<th>
						<?=sys::translate('main::text_variant')?>
					</th>
					<td>
						<input name="text_align" type="radio" <?if($_POST['text_align']=='left'):?>checked<?endif;?> value="left" class="xCheckbox"><img src="<?=_IMG_URL?>text_left.gif">
						<input name="text_align" type="radio" <?if($_POST['text_align']=='under'):?>checked<?endif;?> value="under" class="xCheckbox"><img src="<?=_IMG_URL?>text_under.gif">
						<input name="text_align" type="radio" <?if($_POST['text_align']=='right'):?>checked<?endif;?>  value="right" class="xCheckbox"><img src="<?=_IMG_URL?>text_right.gif">
					</td>
				</tr>
				-->
				<tr>
					<td colspan="2" style="text-align:center;">
						<input class="xRed" type="submit" name="cmd_send" value="<?=sys::translate('main::send')?>">
						<input class="xRed" type="submit" name="cmd_preview" value="<?=sys::translate('main::preview')?>">
					</td>
				</tr>
			</table>
		</form><br>
		<center><b><?=sys::translate('main::send_card')?></b></center>
	<?else:?>
		<div class="xContent">
			<?=$var['meta']['text']?>
		</div>
	<?endif?>
		
	
</div>


