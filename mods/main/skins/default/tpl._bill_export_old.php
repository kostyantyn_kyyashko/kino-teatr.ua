<?sys::setTpl('main::__xml')?>
<billboard>
	<genres>
		<?foreach ($var['genres'] as $obj):?>
			<genre id="<?=$obj['id']?>"><?=$obj['title']?></genre>
		<?endforeach;?>
	</genres>
	<professions>
		<?foreach ($var['professions'] as $obj):?>
			<profession id="<?=$obj['id']?>"><?=$obj['title']?></profession>
		<?endforeach;?>
	</professions>
	<countries>
		<?foreach ($var['countries'] as $obj):?>
			<country id="<?=$obj['id']?>"><?=$obj['title']?></country>
		<?endforeach;?>
	</countries>
	<cities>
		<?foreach ($var['cities'] as $obj):?>
			<city id="<?=$obj['id']?>" country_id="<?=$obj['country_id']?>"><?=$obj['title']?></city>
		<?endforeach;?>
	</cities>
	<studios>
		<?foreach ($var['studios'] as $obj):?>
			<studio id="<?=$obj['id']?>"><?=$obj['title']?></studio>
		<?endforeach;?>
	</studios>
	<shows>
		<?foreach ($var['shows'] as $show):?>
			<show id="<?=$show['id']?>" film_id="<?=$show['film_id']?>" cinema_id="<?=$show['cinema_id']?>" hall_id="<?=$show['hall_id']?>">
				<begin><?=$show['begin']?></begin>
				<end><?=$show['end']?></end>
				<times>
					<?foreach ($show['times'] as $time):?>
						<time time="<?=$time['time']?>">
							<prices><?=$time['prices']?></prices>
							<note><![CDATA[<?=$time['note']?>]]></note>
						</time>
					<?endforeach;?>
				</times>
			</show>
		<?endforeach;?>
	</shows>

	<films>
		<?foreach ($var['films'] as $film):?>
			<film id="<?=$film['id']?>">
				<title orig="<?=$film['title_orig']?>"><?=$film['title']?></title>
				<duration><?=$film['duration']?></duration>
				<year><?=$film['year']?></year>
<?if($film['ukraine_premiere']!='0000-00-00'):?>
				<premiere><?=$film['ukraine_premiere']?></premiere>
<?endif;?>
				<age_limit><?=$film['age_limit']?></age_limit>
				<budget currency="<?=$film['budget_currency']?>"><?=$film['budget']?></budget>
				<intro><![CDATA[<?=$film['intro']?>]]></intro>
				<text><![CDATA[<?=$film['text']?>]]></text>
				<rating votes="<?=$film['votes']?>"><?=$film['rating']?></rating>
				<pro_rating votes="<?=$film['pro_votes']?>"><?=$film['pro_rating']?></pro_rating>
				<photos>
					<?foreach ($film['photos'] as $photo):?>
						<photo src="<?=$photo['src']?>"/>
					<?endforeach;?>
				</photos>
				<trailers>
					<?foreach ($film['trailers'] as $trailer):?>
						<trailer src="<?=$trailer['url']?>"/>
					<?endforeach;?>
				</trailers>
				<posters>
					<?foreach ($film['posters'] as $poster):?>
						<poster order="<?=$poster['order_number']?>" src="<?=$poster['src']?>"/>
					<?endforeach;?>
				</posters>
				<persons>
					<?foreach ($film['persons'] as $person):?>
						<person id="<?=$person['person_id']?>" profession_id="<?=$person['profession_id']?>"  role="<?=$person['role']?>"/>
					<?endforeach;?>
				</persons>
				<genres><?=implode(',',$film['genres'])?></genres>
				<countries><?=implode(',',$film['countries'])?></countries>
				<studios><?=implode(',',$film['studios'])?></studios>
				<reviews>
					<?foreach ($film['reviews'] as $review):?>
						<review url="<?=$review['url']?>"><?=$review['title']?></review>
					<?endforeach;?>
				</reviews>
				<links>
					<?foreach ($film['links'] as $link):?>
						<link url="<?=$link['url']?>"><?=$link['title']?></link>
					<?endforeach;?>
				</links>
			</film>
		<?endforeach;?>
	</films>
	<cinemas>
		<?foreach ($var['cinemas'] as $cinema):?>
			<cinema id="<?=$cinema['id']?>" city_id="<?=$cinema['city_id']?>">
				<title><?=$cinema['title']?></title>
				<address><?=$cinema['address']?></address>
				<phone><?=$cinema['phone']?></phone>
				<site><?=$cinema['site']?></site>
				<text><![CDATA[<?=$cinema['text']?>]]></text>
				<photos>
					<?foreach ($cinema['photos'] as $photo):?>
						<photo src="<?=$photo['src']?>"/>
					<?endforeach;?>
				</photos>
				<halls>
					<?foreach ($cinema['halls'] as $hall):?>
						<hall id="<?=$hall['id']?>">
							<title><?=$hall['title']?></title>
							<scheme><?=$hall['scheme']?></scheme>
						</hall>
					<?endforeach;?>
				</halls>
			</cinema>
		<?endforeach;?>
	</cinemas>
	<persons>
		<?foreach ($var['persons'] as $person):?>
			<person id="<?=$person['id']?>">
				<lastname orig="<?=$person['lastname_orig']?>"><?=$person['lastname']?></lastname>
				<firstname orig="<?=$person['firstname_orig']?>"><?=$person['firstname']?></firstname>
				<url><?=$person['url']?></url>
			</person>
		<?endforeach;?>
	</persons>
</billboard>
