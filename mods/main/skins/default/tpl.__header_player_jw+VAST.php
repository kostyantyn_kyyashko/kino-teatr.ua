<?
sys::useLib('counters');
sys::useLib('banners');
sys::useLib('main::menu');
sys::useLib('content');
sys::useLib('sys::form');
sys::jsInclude('sys::sys');
global $_css_include;

// Redirect 301 ��� ������� (temporary)
$test_arr = explode("/",$_SERVER['REQUEST_URI']);
if(array_search('film_id', $test_arr) > 0 && array_search('film', $test_arr) > 0)
{
    sys::useLib('main::films');
    $film_url_redirect = main_films::GetFilmUrlForRedirect($_SERVER['REQUEST_URI']);
    if($film_url_redirect != ''){
        header("HTTP/1.1 301 Moved Permanently");
        header('Location: '.$film_url_redirect);
        exit();
    }
}
//==========================================
?>
<!DOCTYPE html>
<html lang='ru' xml:lang='ru' xmlns='http://www.w3.org/1999/xhtml' <?=$_var['og']?'prefix="og: http://ogp.me/ns# video: http://ogp.me/ns/video# ya: http://webmaster.yandex.ru/vocabularies/"':"";// разметка https://help.yandex.ru/webmaster/video/open-graph-markup.xml?> >
<head>
	<title><?=htmlspecialchars($_meta_title)?></title>
	<meta name='yandex-verification' content='6d58b9dcfe59710b' />
	<meta name="keywords" content="<?=htmlspecialchars($_meta_keywords)?>">
	<meta name="DESCRIPTION" content="<?=htmlspecialchars($_meta_description)?>">
	<?=$_meta_other?>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<?if (_CANONIC!='_CANONIC'):?>
<link rel="canonical" href="<?=_CANONIC?>" />
<?endif;?>
<?if (_ALTERNATE!='_ALTERNATE'):?>
<link rel="alternate" <?=_ALTERNATE?> /> <?// href="//kino-teatr.ua/" hreflang="ru" или href="//kino-teatr.ua/uk/" hreflang="uk"?>

<?endif;?>

	<link rel="shortcut icon" type="image/ico" href="//kino-teatr.ua/favicon.ico" />
<link rel="icon" type="image/ico" href="//kino-teatr.ua/favicon.ico" />
<link rel="apple-touch-icon" href="//kino-teatr.ua/images/icons/60.png">
<link rel="apple-touch-icon" sizes="57x57" href='//kino-teatr.ua/images/icons/57.png'>
<link rel="apple-touch-icon" sizes="72x72" href='//kino-teatr.ua/images/icons/72.png'>
<link rel="apple-touch-icon" sizes="76x76" href='//kino-teatr.ua/images/icons/76.png'>
<link rel="apple-touch-icon" sizes="114x114" href='//kino-teatr.ua/images/icons/114.png'>
<link rel="apple-touch-icon" sizes="120x120" href='//kino-teatr.ua/images/icons/120.png'>
<link rel="apple-touch-icon" sizes="144x144" href='//kino-teatr.ua/images/icons/144.png'>
<link rel="apple-touch-icon" sizes="152x152" href='//kino-teatr.ua/images/icons/152.png'>
<link rel="shortcut icon" sizes="196x196" href='//kino-teatr.ua/images/icons/196.png'>
<link rel="shortcut icon" sizes="128x128"  href='//kino-teatr.ua/images/icons/128.png'>
<link rel="shortcut icon" sizes="96x96"  href='//kino-teatr.ua/images/icons/96.png'>
<link rel="shortcut icon" sizes="72x72"  href='//kino-teatr.ua/images/icons/72.png'>
<link rel="shortcut icon" sizes="48x48"  href='//kino-teatr.ua/images/icons/48.png'>
	<link rel="stylesheet" href="<?=$skin_url?>blocks.css" type="text/css" />
	<link rel="stylesheet" href="<?=$skin_url?>new_brand3.css" type="text/css" />
	<!-- bannerky -->
	<?/* script type="text/javascript" src="<?=_3PARY_URL?>adriver.core.2.js"></script */?>
	<script type='text/javascript' src='https://ad.kino-teatr.ua/www/delivery/spcjs.php?id=1'></script>
	<!-- end bannerky -->

	<script type="text/javascript" src="<?=$skin_url?>js/jquery.js"></script>
	<script type="text/javascript" src="<?=$skin_url?>js/portal.js"></script>
	<script type="text/javascript" src="<?=$skin_url?>js/overlib/overlib.js"></script>
	<!-- Widgets -->
<script type="text/javascript" src="<?=$skin_url?>js/share.js?10"></script>
<script src="<?=$skin_url?>js/openapi.js" type="text/javascript" charset="windows-1251"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>

	<!-- End Widgets -->
	<script type="text/javascript">

		type = 0;
		objid = 0;

		function SubmitSearch()
		{
			var returnval;


			if(!active)
			{
			returnval = true;
			} else {
			var url;

			url = objid;
			window.location = url;
			returnval = false;
			}


			return returnval;
		}


	</script>
	<link rel="stylesheet" type="text/css" href="<?=$skin_url?>droplinebar.css" />
	<script src="<?=$skin_url?>js/droplinemenu.js" type="text/javascript"></script>
	<script type="text/javascript">
		droplinemenu.buildmenu("mydroplinemenu");

		$(function() {
			$("#searchField").focus(function(){
				$(this).animate({ width:"407px"}, 350);
				if(this.value == this.defaultValue) this.value = '';
			}).blur(function(){
				$(this).animate({ width:"240px"}, 300);
				if(this.value == '') this.value = this.defaultValue;
				setTimeout("$('#listbox2').css('display','none')",200);
			});
		});

	</script>
	<?/*
	<script type="text/javascript" src="<?=$skin_url?>js/easySlider1.7.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#slider").easySlider({

			});
		});
	</script>
	*/?>
	<script src="<?=$skin_url?>js/cufon-yui.js" type="text/javascript"></script>
	<script src="<?=$skin_url?>js/myriad-pro.cufonfonts.js" type="text/javascript"></script>
	<script type="text/javascript">
		Cufon.replace('#myriad', { fontFamily: 'Myriad Pro Bold Condensed', hover: true });
	</script>
<link href="<?=$skin_url?>screen.css" rel="stylesheet" type="text/css" media="screen" />

<?/* banner catfish
<script language=JavaScript>
function closeclouds()
{
	$('#fixmetoo').css('display', 'none');
}
</script>
<style type="text/css">
#fixmetoo { position: fixed; right: 0px; bottom: 0px; }

div > div#fixmetoo { position: fixed; }
pre.fixit { overflow:auto;border-left:1px dashed #000;border-right:1px dashed #000;padding-left:2px; }
		</style>

		<!--[if gte IE 5.5]><![if lt IE 7]>
		<style type="text/css">
div#fixmetoo {
right: auto; bottom: auto;
left: expression( ( 0 - fixmetoo.offsetWidth + ( document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.clientWidth ) + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + 'px' );
top: expression( ( 0 - fixmetoo.offsetHeight + ( document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.clientHeight ) + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + 'px' );
}
		</style>
		<![endif]><![endif]-->
end banner	*/?>		
<script src="//jw.ads-player.com/player/v/7.11.3/jwplayer.js"></script>
<script>jwplayer.key="s3vu1E0JoBPSIH9Vs/dZC7fCEmQmaLob5qvGpg=="</script>             

    <?=sys::attentionUserInfo()?>
	<script charset="UTF-8" src="//cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/6a9b391d34454d0379806d0565e2a1fb_0.js" async></script>
</head>
<body>
<!--facebook wiget -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.9";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- end facebook wiget -->

	<h1><?=htmlspecialchars($_var['main::title'])?></h1>

	<?if (_AUTH=='_AUTH'):?>
	<a href="https://plus.google.com/105739983871549030806?rel=author" rel='author' rel='nofollow' class='google'>Google</a>
	<?endif;?>

<div  id="overDiv"  style="position:absolute;  visibility:hidden;  z-index:10000;"></div>  
<div id='fixmetoo' style='z-index: 6500; width: 100%;'></div>
	<script type="text/javascript">
		window.onload = function(e){
			$('#myriad').css('visibility', 'visible');
			
			/*Mike header*/
			$("#ourBranding").bind("click", function()
			{
				$.ajax({
				   type: "GET",
				   url: '?mod=main&act=ajx_brending_click',
				});
				return true;
			});

					<?=banners::showBanners('branding_images')?>
		$('#footHide').css('display', 'inline-block');
		$('#socHide').load('?lang=<?=$_cfg['sys::lang']?>&mod=main&act=ajx_soc');
		$('#intHide').load('?lang=<?=$_cfg['sys::lang']?>&mod=main&act=ajx_int');
		<?/*  new adriver("fullscreen_banner", {sid: 169393, bt:52, bn:5, pz:5});
		new adriver("banner_right", {sid: 169393, bt:52, bn:2, pz:2});
		new adriver("topBanner", {sid: 169393, bt:52, bn:6, pz:6});
		new adriver("fixmetoo", {sid: 169393, bt:52, bn:7, pz:7});
		new adriver("secondbanner", {sid: 169393, bt:52, bn:9, pz:9});
		new adriver("topInnerBanner", {sid: 169393, bt:52, bn:8, pz:8});
		new adriver("counter", {sid: 169393, bt:52, bn:4, pz:4}); */?>

			$('#gplus').load('?lang=<?=$_cfg['sys::lang']?>&mod=main&act=ajx_counters');
		}
	</script>
<div id='grayHeaderLine'>
	<div id='headerWrap'>
<script>
function changeMyCity(i, city)
{

$('#cityLink').html(city);


<?

if (substr($_SERVER['REQUEST_URI'], 0, 16)=='/ru/main/cinemas' ||
	substr($_SERVER['REQUEST_URI'], 0, 16)=='/uk/main/cinemas' ||
	substr($_SERVER['REQUEST_URI'], 0, 9)=='/cinemas-' ||
	substr($_SERVER['REQUEST_URI'], 0, 12)=='/uk/cinemas-' ||
	substr($_SERVER['REQUEST_URI'], 0, 11)=='/uk/cinemas' ||
	substr($_SERVER['REQUEST_URI'], 0, 8)=='/cinemas'
)
{
	echo "ajx_url = '?mod=main&act=ajx_bill&city_id='+i+'&cinemas=1'";
} else if (substr($_SERVER['REQUEST_URI'], 0, 13)=='/afisha-kino-') {
	echo "ajx_url = '?mod=main&act=ajx_bill&city_id='+i+'&films=1'+'&lang=ru'";
} else if (substr($_SERVER['REQUEST_URI'], 0, 16)=='/uk/afisha-kino-') {
	echo "ajx_url = '?mod=main&act=ajx_bill&city_id='+i+'&films=1'+'&lang=uk'";

} else {
	echo "ajx_url = '?mod=main&act=ajx_bill&city_id='+i";
}
?>


$.ajax({
   type: "GET",
   url: ajx_url,
   data: {newcity:i},
   success: function(html){

<?




if (substr($_SERVER['REQUEST_URI'], 0, 10)=='/bill.phtm' ||
	substr($_SERVER['REQUEST_URI'], 0, 13)=='/uk/bill.phtm' ||
	substr($_SERVER['REQUEST_URI'], 0, 10)=='/kinoafish' ||
	substr($_SERVER['REQUEST_URI'], 0, 10)=='/uk/kinoaf')
{
		echo 'window.location = html';
} else if (substr($_SERVER['REQUEST_URI'], 0, 16)=='/ru/main/cinemas' ||
	substr($_SERVER['REQUEST_URI'], 0, 16)=='/uk/main/cinemas' ||
	substr($_SERVER['REQUEST_URI'], 0, 9)=='/cinemas-' ||
	substr($_SERVER['REQUEST_URI'], 0, 12)=='/uk/cinemas-' ||
	substr($_SERVER['REQUEST_URI'], 0, 11)=='/uk/cinemas' ||
	substr($_SERVER['REQUEST_URI'], 0, 8)=='/cinemas') {
		echo 'window.location = html';
} else if (substr($_SERVER['REQUEST_URI'], 0, 25)=='/ru/main/bill/order/films' ||
	substr($_SERVER['REQUEST_URI'], 0, 25)=='/uk/main/bill/order/films') {
		echo 'window.location = html';
} else if (substr($_SERVER['REQUEST_URI'], 0, 13)=='/afisha-kino-' ||
	substr($_SERVER['REQUEST_URI'], 0, 16)=='/uk/afisha-kino-') {
		echo 'window.location = html';
} else {
		echo 'location.reload(true);';
}



?>


   }
});



}

</script>
<script>
openedcit = null;

$('body').bind('click', function(e){
if(openedcit==1)
{
$('#myCityList').fadeOut('fast');
openedcit = null;
}
});


function showMyCity(i)
{
$('#myCityList').fadeIn('fast', function(e){openedcit = 1;});

}

function changeLang()
{
	document.location.href = '<?=sys::changeLang()?>';
}


</script>
<?$city['values']=main_countries::getCountryCitiesTitles(29)?>
<?$city['value']=$_cfg['main::city_id']?>
<?$city['input']='selectbox'?>
<?$city['title']=sys::translate('main::city')?>
		<div id='yourCity'><a href='#' onclick='showMyCity(1); return false;' id='cityLink'><?=$city['values'][$city['value']]?></a></div>
		<ul id='language'>
			<?foreach ($_langs as $lng):?>
				<?if($_cfg['sys::lang']==$lng['code']):?>
					<li class='active' id='lang<?=$lng['code']?>'><a><span><?=$lng['short_title']?></span></a></li>
				<?else:?>
					<li id='lang<?=$lng['code']?>'><a href='#lang' onclick="return changeLang();" title="<?=$lng['title']?>"><span><?=$lng['short_title']?></span></a></li>
				<?endif?>

			<?endforeach;?>

		</ul>
		<?if($_user['id']==2):?>
<div id='loginForm1' style='display: none; height: 40px; '>
	<div id='loginForm'>
	<form method="POST" action="/ru/main/login.phtml">
		<input type="hidden" name="url" value="//<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>">
			<input type="text" onfocus='this.value=""' maxlength="32" name="login" class="regTextField" value='<?=sys::translate('main::login')?>'>
			<input type="password" onfocus='this.value=""' name="password" class="regTextField" value='<?=sys::translate('main::password')?>'>
			<input type="submit" value="<?=sys::translate('main::entry')?>" class="regBtn">
		</form>
	</div>
</div>
<div id='changeLogin'>
<!-- start ulogin -->
		<div id='openApi'>
			<span><?=sys::translate('main::enterWith')?></span>

<style>
#uLogin img:hover
{
	cursor: pointer;
}
</style>

<script src="//ulogin.ru/js/ulogin.js"></script>


<div id="uLogin" x-ulogin-params="display=buttons;fields=first_name,last_name;
optional=email,bdate,photo_big,sex;providers=facebook,twitter,vkontakte;
hidden=;redirect_uri=http%3A%2F%2F<?=$_SERVER['HTTP_HOST']?>%2Fru%2Fmain%2Ffb_auth.phtml%3Freturn%3D<?=urlencode('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'])?>;
receiver=http%3A%2F%2F<?=$_SERVER['HTTP_HOST']?></div>%2Fxd_custom.html">
<img src="/facebook.png" x-ulogin-button = "facebook"/>
<img src="/twitter.png" x-ulogin-button = "twitter"/>
<!-- img src="/vkontakte.png" x-ulogin-button = "vkontakte"/ -->
</div>
		</div>
<!-- end ulogin -->
<script>
function showLoginBox()
{
	$('#changeLogin').fadeTo('fast','0', function(){$('#changeLogin').css('display', 'none');});
	$('#search').fadeTo('fast','0', function(){$('#search').css('display', 'none');$('#loginForm1').css('display', 'block');});


}
</script>
		<a href='/ru/main/register.phtml' id='register'><?=sys::translate('main::register')?></a>
		<ul id='enter'>
			<li><a href onclick='showLoginBox(); return false;'><span><?=sys::translate('main::enter')?></span></a></li>
		</ul>
</div>
        <?else:?>
        	<div id='afterLogin'>
				<?=sys::translate('main::welcome')?> <?=$_user['login']?>!&nbsp;
				<a href="<?=sys::rewriteUrl('?mod=main&act=user_profile')?>"><?=sys::translate('main::profile')?></a>&nbsp;
				<a href="<?=sys::rewriteUrl('?mod=main&act=login')?>"><?=sys::translate('main::exit')?></a>
			</div>
            <style>
            	div.listbox2
            	{
            		right: 60px !important;
            	}
            </style>
		<?endif;?>

						<form  ACTION="<?=sys::rewriteUrl('?mod=main&act=search')?>"  NAME="searchform" onSubmit="return SubmitSearch()" method="GET" id="search">

									<input type='text' autocomplete='off' id='searchField' value='<?=sys::translate('main::search')?>' name="keyword" onclick='if (this.value==this.defaultValue) this.value=""'>
									<input type='image' src='<?=$skin_url?>images/searchButton.png' id='searchButton'>
						</form>
								<script>
								function Timeout(fn, interval) {
								    var id = setTimeout(fn, interval);
								    this.cleared = false;
								    this.clear = function () {
								        this.cleared = true;
								        clearTimeout(id);
								    };
								}

								$('#searchField').bind('keyup',function(e){

									var kec = e.which;

									if(kec != 40 && kec != 38) {

									    ajx_url = '?mod=main&act=ajx_search&mode=option';
	                        			field = 'films';
	                        			num_of_chars = '1';
	                        			obj = document.getElementById('searchField');

										list=document.getElementById('_ajx_'+field+'_list');
										box=list.parentNode;
										if(obj.value.length>num_of_chars)
										{


											$.ajax({
											   type: "GET",
											   url: ajx_url,
											   data: {field:field,keywords:obj.value},
											   success: function(html){
													box.style.display='block';
										    		$("#_ajx_"+field+"_list").html(html);
										 		 }
											 });
										}

									} else if(kec == 40) {
											var i;
											for (i=0;i<=total;i++)
											{
												$("#lnk"+active).removeClass('hover');
											}
											active++;
											if (active>total)
											{
												active = 0;
											} else {
												$("#lnk"+active).addClass('hover');
												type = $("#lnk"+active).attr('type');
												objid = $("#lnk"+active).attr('objid');
											}

											if (!active)
											{
												type = 0;
												objid = 0;
											}

									} else if(kec == 38) {
											var i;
											for (i=0;i<=total;i++)
											{
												$("#lnk"+active).removeClass('hover');
											}
											active--;
											if (active<0)
											{
												active = total;
												$("#lnk"+active).addClass('hover');
												type = $("#lnk"+active).attr('type');
												objid = $("#lnk"+active).attr('objid');
											} else if (active>0) {
												$("#lnk"+active).addClass('hover');
												type = $("#lnk"+active).attr('type');
												objid = $("#lnk"+active).attr('objid');
											}

											if (!active)
											{
												type = 0;
												objid = 0;
											}



									}


								});

								</script>
						<div class="listbox2" id='listbox2' onclick='changeFlag("1");setTimeout(hideList, 50); '>
							<div class="panel"  id="_ajx_films_list" onclick='changeFlag("1");setTimeout(hideList, 50); '>&nbsp;</div>
						</div>

<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
<?=main::showMainBill()?>
	</div>
</div>
<div id='wrapper' style='position: relative;'>
	<div id='wrap' style='position: relative;'>
<style>
#Advertisement
{
	margin-left: 13px;
	position: relative;
	top: 2px;
}

</style>
<style>
#container_wrapper,#container2_wrapper,#container3_wrapper,#container4_wrapper,#container5_wrapper
{
	height: 340px !important;
}
</style>
<!-- LINK Brending  -->
<?=banners::showBanners('branding_links')?>
<!--  END LINK Brending  -->
<script type='text/javascript'>
function showHelp()
{
	$('#helpDiv').toggle();
}
</script>

		<div id='content'>

			<div id='topMenuBlock'>
				<a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?endif;?>' class='logo'>&nbsp;</a>

				<input type='hidden' id='menucount' value='50'>
								<?=main_menu::showMenu()?>
			</div>
<div style='position: relative; background: #fff;'>
<?=banners::showBanners('v3_top_secondary')?>
</div>
			

