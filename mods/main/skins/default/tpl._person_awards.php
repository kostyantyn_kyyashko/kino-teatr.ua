<?sys::useLib('content');?>
<?$_var['right']=main_persons::showPersonMedia($_GET['person_id'], $var['person'])?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=6?>
<?$_var['active_child_menu']=8?>
<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

<?
//if(sys::isDebugIP()) sys::printR($var); 
?>	

<div id="main_person_awards">

	<div class='myriadFilm' id='myriad'><?=$var['meta']['title']?></div>
	<div id='lineHrWide'>&nbsp;</div>
	<?=main_persons::showPersonTabs($_GET['person_id'],$var['person'],'awards');?>
	<div id='lineBlank' style='height: 25px;'><img src='/images/blank.gif' height='1'></div>

		<div>
				<?
					$year = "";
					$nomination = "";
				?>
					<ul id='awardsPersonList'>																					
					<?foreach ($var['objects'] as $obj):?>
						
						<?if($obj["year"] != $year):?>
						<? 
							$year = $obj["year"];
							$nomination = "";
						?> 
						<li>
							<div id='awardsListTitle'>
								<h2><?=$obj["year"]?></h2>
							</div>	
						</li>												
						<?endif;?>			
						
						<?if($obj["nomination"] != $nomination):?>
						<? 
							$nomination = $obj["nomination"];
						?> 
						<li>
							<div id='awardsListTitle'>
							<h3><?=$obj["nomination"]?></h3>
							</div>
						
						</li>												
						<?endif;?>			
						
						<li>
							<div>
								<?=sys::translate('main::film')?> <a href=<?=$obj["film_url"]?>><?=($film = $obj["film"])?></a>
								-
								<?=$obj["title"]?>
								<?if($obj['result']=='nom'):?>(<?=sys::translate('main::nomination')?>)<?endif?>
								<div id='awardsListPrise'><img src='/images/awards_<?=($obj['result']=='nom')?"nominant":"winner"?>.png' style="margin-top: 10px;"></div>
							
							</div>
						</li>												
													
					<?endforeach;?>										
					</ul>										
		</div>	

		<div id='lineBlank' style='height: 25px;'><img src='/images/blank.gif' height='1'></div>
	
</div>
