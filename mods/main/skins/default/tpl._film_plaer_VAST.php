<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=6?>
<?$_var['active_child_menu']=7?>
<?sys::setMeta($var['meta']);?>
<?$_var['og']=$var['og'];// разметка https://help.yandex.ru/webmaster/video/open-graph-markup.xml?>
<?ob_start()?>
<?require "bill_right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

<div  itemscope itemtype="//schema.org/Movie">
<meta itemprop="dateCreated" content ="<?if(($var['dateCreated']) == '0000-00-00'):?><?if ($var['trailer_date']):?><?=$var['trailer_date']?><?else:?>2008-04-04<?endif;?><?else:?><?=$var['dateCreated']?><?endif;?>">


<script type="text/javascript">
$(function(){
	$('a.blog_code').click(
		function()
		{
			$('textarea[name=forum_code]').css('display','none');
			if($('textarea[name=blog_code]').css('display')!='none')
				$('textarea[name=blog_code]').css('display','none');
			else
			{
				$('textarea[name=blog_code]').css('display','');
				$('textarea[name=blog_code]')[0].focus();
				$('textarea[name=blog_code]')[0].select();
			}
			return false;
		}
	);

	$('a.forum_code').click(
		function()
		{
			$('textarea[name=blog_code]').css('display','none');
			if($('textarea[name=forum_code]').css('display')!='none')
				$('textarea[name=forum_code]').css('display','none');
			else
			{
				$('textarea[name=forum_code]').css('display','');
				$('textarea[name=forum_code]')[0].focus();
				$('textarea[name=forum_code]')[0].select();
			}
			return false;
		}
	);


})
</script>
				<div class='myriadFilm' id='myriad'>
				 <?=($var['serial'])?sys::translate('main::serials_article'):sys::translate('main::film')?> <?=$var['title']?>
				</div>

					<div id='lineHrWide'>&nbsp;</div>


				<?=main_films::showFilmTabs($_GET['film_id'],$var['title'],'info');?>
						<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

					<div id='posterBlock'>
						<a	href='<?=$var['poster']['url']?>' id='filmPoster' title='<?=($var['serial'])?sys::translate('main::serials_article'):sys::translate('main::film')?> <?=$var['title']?> - <?=sys::translate('main::posters')?>'><img src='<?=$var['poster']['image']?>' alt="<?=($var['serial'])?sys::translate('main::serials_article'):sys::translate('main::film')?> <?=$var['title']?>" title="<?=($var['serial'])?sys::translate('main::serials_article'):sys::translate('main::film')?> <?=$var['title']?> - <?=sys::translate('main::posters')?>" itemprop="image"></a>
<!-- место размещения виджетов социалок под постером -->

						<!-- <div id="vk_like"></div> -->

						<div id='lineBlank' style='height: 5px;'><img src='/images/blank.gif' height='1'></div>
                        <script src="https://connect.facebook.net/ru_RU/all.js#xfbml=1"></script><fb:like href="" layout="button_count" show_faces="false" width="100" font=""></fb:like>
						<div id='lineBlank' style='height: 5px;'><img src='/images/blank.gif' height='1'></div>
						<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal" rel='nofollow'>Tweet</a><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>
<!-- 
					<script>
						VK.init({apiId: 2657621, onlyWidgets: true});
						 VK.Widgets.Like('vk_like', {width: 126, type: 'mini',pageImage:'<?=$var['poster']['image']?>',pageTitle: 'Я рекомендую фильм: <?=$var["title"]?>', pageDescription: 'Я рекомендую фильм: <?=$var["title"]?>'}, <?=$_GET['film_id']?>);
					</script>
					-->
<!-- конец места размещения виджетов социалок под постером -->					
					</div>
					<?if($var['actors']):?>
					<div id='actors'>
						<strong class='red'><?=sys::translate('main::in_roles')?>: </strong><br>
						<ul id='actorsList'>
							<?foreach($var['actors'] as $actor):?>
								<li>
								<span itemprop="actor" itemscope itemtype="//schema.org/Person">
									<a target="_blank" title="<?=$actor['fio']?>" href="<?=$actor['url']?>" itemprop="name"><?=$actor['fio']?></a>
									</span>
								</li>
							<?endforeach;?>
								<li>
									<a href="<?=$var['persons_url']?>"><?=sys::translate('main::moreM')?></a>
								</li>
								
						</ul>
						<?if(main_films::isFilmBuy($_GET['film_id'])):?>
							<br>
							<a href='<?=main_films::getFilmShowsUrl($_GET['film_id'])?>' id="btn_tckts" title="<?=($var['serial'])?sys::translate('main::serials_article'):sys::translate('main::film')?> <?=$var['title']?> - <?=sys::translate('main::buy_ticket')?>"><?=sys::translate('main::buy_ticket')?></a>	
						<?endif;?>	
					</div>
					<?endif;?>

					<div id='filmInfo'>
						<strong class='red' itemprop="name"><?=$var['title']?></strong> 
							<?if($var['film_3d']):?>
							<sup class="mark3d">3D</sup>
							<?endif?>
						
						<?if ($var['title_orig']):?><br><span class='grey' itemprop="alternativeHeadline"><?=$var['title_orig']?> <?if($var['title_alt']):?>/ <?=$var['title_alt']?><?endif;?></span><?endif;?><br>
					<?if($var['year']):?>
						<strong><?=sys::translate('main::year')?>: </strong><a href='<?=sys::rewriteUrl('?mod=main&act=films&year='.$var['year'])?>' title='<?=sys::translate('main::films')?> <?=$var['year']?>'><?=$var['year']?></a><br>
					<?endif?>

					
					
					<?if($var['countries']):?>
						<strong><?=sys::translate('main::country')?>: </strong><?=implode(', ',$var['countries'])?><br>
					<?endif;?>

					<?if($var['directors']):?>
						<strong><?=sys::translate('main::directors')?>: </strong>
						<?$i=1?>
						<?foreach($var['directors'] as $director):?>
						<span itemprop="director" itemscope itemtype="//schema.org/Person">
							<a title="<?=$director['fio']?>" target="_blank" href="<?=$director['url']?>" itemprop="name"><?=$director['fio']?></a><?if($i<count($var['directors'])):?>,<?endif;?>
							</span>
							<?$i++?>
						<?endforeach;?>
						<br>
					<?endif?>

					<?if($var['studios']):?>
						<strong><?=sys::translate('main::studio')?>: </strong><?=implode(', ',$var['studios'])?><br>
					<?endif;?>

					<?if($var['genres']):?>
						<strong><?=sys::translate('main::genre')?>: </strong> <span itemprop="genre"><?=implode(', ',$var['genres'])?></span><br>
					<?endif;?>

					<?if($var['budget']):?>
						<strong><?=sys::translate('main::budget')?>: </strong><?=$var['budget']?><br>
					<?endif?>

					<?if($var['ukraine_premiere']):?>
						<strong><?=sys::translate('main::ukraine_premiere')?>: </strong><a href='<?=sys::rewriteUrl('?mod=main&act=films&uap='.$var['ukraine_premiere'])?>'><?=$var['ukraine_premiere']?></a><br>
					<?endif;?>

					<?if($var['world_premiere']):?>
						<strong><?=sys::translate('main::world_premiere')?>: </strong> <a href='<?=sys::rewriteUrl('?mod=main&act=films&wop='.$var['world_premiere'])?>'><?=$var['world_premiere']?></a><br>
					<?endif;?>

					<?if($var['duration']):?>
						<strong><?=sys::translate('main::duration')?>: </strong><?=$var['duration']?> <?=sys::translate('main::min')?><br>
					<?endif;?>

					<?if($var['age_limit']):?>
						<strong><?=sys::translate('main::age_limits')?>: </strong> <?=$var['age_limit']?><br>
					<?endif?>
					
					<?if($var['distributor']):?>
						<strong><?=sys::translate('main::distributor')?>: </strong><?=$var['distributor']?><br>
					<?endif;?>
					
					<?if($var['box_office_total']):?>
						<strong><?=sys::translate('main::boxuOffice')?>: </strong><?=$var['box_office_total']?> <?=sys::translate('main::grn')?><br>
					<?endif;?>
					<?if($var['imdb_id'] && $var['imdb_id']<>'---'):?>
											<strong><?=sys::translate('main::imdb')?>: </strong>&nbsp;&nbsp;<span class="imdbRatingPlugin" data-user="ur56002283" data-title="<?=$var['imdb_id']?>" data-style="t1">
<a href="//www.imdb.com/title/<?=$var['imdb_id']?>/?ref_=tt_plg_rt" rel="nofollow" target="_blank"><img alt="<?=$var['title']?>" src="<?=$skin_url?>imdb/imdb_46x22.png"></a> </span>
<script>
(function(d,s,id){var js,stags=d.getElementsByTagName(s)[0];
if(d.getElementById(id)){return;}js=d.createElement(s);js.id=id;
js.src="<?=$skin_url?>imdb/rating.min.js";
stags.parentNode.insertBefore(js,stags);})(document,'script','imdb-rating-api');    
</script> 
						<br>
					<?endif;?>
							
					<br>
					<?if(sys::checkAccess('main::films_tracking')):?>
					<form style="text-align:left; margin-bottom:10px;" action="<?=$_SERVER['REQUEST_URI']?>" method="POST">
						<?if($var['tracking']):?>
							<input title="<?=sys::translate('main::untracking_film')?>" type="submit" class="xRed" name="cmd_untracking" value="<?=sys::translate('main::untracking')?>">
						<?else:?>
							<input title="<?=sys::translate('main::tracking_film')?>" type="submit" class="xRed" name="cmd_tracking" value="<?=sys::translate('main::tracking')?>">
						<?endif;?>
					</form>
				<?else:?>
				<input title="<?=sys::translate('main::tracking_film')?>" type="button" class="xRed" value="<?=sys::translate('main::tracking')?>" onclick="window.location='<?=sys::rewriteUrl('?mod=main&act=login')?>'">
				<?endif;?>
					</div>

		<div id='lineBlank' style='height: 25px;'><img src='/images/blank.gif' height='1'></div>

		<span itemprop="aggregateRating" itemscope itemtype="//schema.org/AggregateRating">
					<?=main_films::showFilmRatingForm($_GET['film_id'])?>
		</span>
			

<script>
function showSeo()
{
	$('#seoBtn').css('display', 'none');
	$('#seofull').css('display', 'block');
}
</script>
<div id='lineBlank' style='height: 36px;'><img src='/images/blank.gif' height='1'></div>
<!-- begin banner sputnika 
						<?// if ($var['sputnik']>0):?>
					<a href="//kino-sputnik.com.ua/afisha/" rel="nofollow" target="_blank"><img src="<?//=$skin_url?>banner/660x30_2014_v4sputnik.png" width="660" height="30" border="0" alt=""></a><br><br><br>
					<?// endif;?>
end banner sputnika  -->	
					<div id='searchTitle'><?if($var['serial']):?><?=sys::translate('main::serialDescription')?><?else:?><?=sys::translate('main::filmDescription')?><?endif;?> <?=$var['title']?></div>
					<div id='lineHrWide'>&nbsp;</div>

					<?if(!$var['directors']):?>
						<a href="<?=_ROOT_URL?>" itemprop="director" style='display: none;'><?=sys::translate('main::notknown')?></a>
					<?endif?>

					<div id='filmText' itemprop="description">

					<? if (!$var['intro'] && !$var['text']): ?>
					<span class='display: none;'><?=$var['title']?></span>
					<?endif;?>
						<?=$var['intro']?>
<?if ($var['text']):?>
<div id='seosm'><?if($var['text']):?><a href='#' onclick='showSeo(); return false;' id='seoBtn'><?=sys::translate('main::more')?></a><?endif;?></div>
<?endif;?>
<?if ($var['text']):?>
<div id='seofull'><?=$var['text']?></div>
<?endif;?>
<!-- редактирование синопсиса -->
<? /* 
<?if(sys::checkAccess('main::sinopsis_edit')):?>

	<div id='lineBlank' style='height: 12px;'><img src='/images/blank.gif' height='1'></div>

	<a href onclick='$("#editor").toggle("fast"); return false;'>Редактировать</a>

	<div id='lineBlank' style='height: 12px;'><img src='/images/blank.gif' height='1'></div>

<script type="text/javascript" src="/jwysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="/jwysiwyg/controls/default.js"></script>
	<link rel="stylesheet" href="/jwysiwyg/jquery.wysiwyg.css" type="text/css" media="screen" charset="utf-8" />


	<style type="text/css" media="screen">
		#wysiwyg{ width:664px; height:300px; }
	</style>

<div id='editor'>


<form method='POST'>
<textarea id="wysiwyg" style='width:664px; height:300px;' name='editedtext'><?=$var['intro']?></textarea>
<input type='submit' value='Сохранить'>
</form>

<script type="text/javascript">
$(function() {
    $('#wysiwyg').wysiwyg();
});
</script>

</div>

<script type="text/javascript">
$(function() {
    $('#editor').toggle();
});
</script>
<?endif;?>
*/?>

					</div>


						<div id='lineBlank' style='height: 15px;'><img src='/images/blank.gif' height='1'></div>

	<div style="float:left"><a class="blog_code" href="#"><?=sys::translate('main::blog_code')?></a>&nbsp; &nbsp;<a class="forum_code" href="#"><?=sys::translate('main::forum_code')?></a></div>

						<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

		<div>
		<textarea readonly style="width:100%; height:100px; display:none; margin-top: 15px;"  name="blog_code">
			<?ob_start();?>



				<a href='<?=$var['poster']['url']?>' id='filmPoster' style='float: left;margin-right: 15px;'><img src='<?=$var['poster']['image']?>' alt="<?=($var['serial'])?sys::translate('main::serials_article'):sys::translate('main::film')?> - <?=$var['title']?>" title="<?=$var['title']?>"></a>

			<a href="<?=main_films::getFilmUrl($_GET['film_id'])?>" title="<?=($var['serial'])?sys::translate('main::serials_article'):sys::translate('main::film')?> <?=$var['title']?>"><strong><?=($var['serial'])?sys::translate('main::serials_article'):sys::translate('main::film')?> <?=$var['title']?></strong> /<?=$var['title_orig']?></a><br>
			<?if($var['genres']):?>
				<b><?=sys::translate('main::genre')?></b>:&nbsp;<?=implode(', ',$var['genres'])?><br>
			<?endif;?>
			<?if($var['directors']):?>
				<b><?=sys::translate('main::director_s')?></b>:&nbsp;
				<?$i=1?>
				<?foreach($var['directors'] as $director):?>
					<a title="<?=sys::translate('main::regiser')?>&nbsp;<?=$director['fio']?>" target="_blank" href="<?=$director['url']?>"><?=$director['fio']?></a><?if($i<count($var['directors'])):?>,<?endif;?>
					<?$i++?>
				<?endforeach;?>
				<br>
			<?endif?>
			<?if($var['actors']):?>
				<b><?=sys::translate('main::in_roles')?></b>:&nbsp;
				<?$i=1?>
				<?foreach($var['actors'] as $actor):?>
					<a target="_blank" title="<?=sys::translate('main::actor')?>&nbsp;<?=$actor['fio']?>" href="<?=$actor['url']?>"><?=$actor['fio']?></a><?if($i<count($var['actors'])):?>,&nbsp;<?endif;?>
					<?$i++?>
				<?endforeach;?>
				<br>
			<?endif;?>
			<?if($var['countries']):?>
				<b><?=sys::translate('main::country')?>:</b>&nbsp;
				<?=implode(', ',$var['countries'])?>
				<br>
			<?endif;?>
			<?if($var['studios']):?>
				<b><?=sys::translate('main::studio')?>:</b>&nbsp;
				<?=implode(', ',$var['studios'])?>
				<br>
			<?endif;?>
			<?if($var['year']):?>
				<b><?=sys::translate('main::year')?>:</b>&nbsp;
				<?=$var['year']?>
				<br>
			<?endif?>
			<?if($var['budget']):?>
				<b><?=sys::translate('main::budget')?>:</b>&nbsp;
				<?=$var['budget']?>
				<br>
			<?endif?>
			<?if($var['ukraine_premiere']):?>
				<b><?=sys::translate('main::ukraine_premiere')?>:</b>&nbsp;
				<?=$var['ukraine_premiere']?>
				<br>
			<?endif;?>
			<?if($var['world_premiere']):?>
				<b><?=sys::translate('main::world_premiere')?>:</b>&nbsp;
				<?=$var['world_premiere']?>
				<br>
			<?endif;?>
			<?if($var['duration']):?>
				<b><?=sys::translate('main::duration')?>:</b>&nbsp;
				<?=$var['duration']?> <?=sys::translate('main::min')?>
				<br>
			<?endif;?>
			<?if($var['age_limit']):?>
				<b><?=sys::translate('main::age_limits')?>:</b>&nbsp;
				<?=$var['age_limit']?>
				<br>
			<?endif?>
			<?=$var['intro']?>
			<a href="<?=main_films::getFilmUrl($_GET['film_id'])?>"><?=sys::translate('main::more_about_film')?></a>&nbsp;
			<a href="<?=main_films::getFilmTrailersUrl($_GET['film_id'])?>"><?=sys::translate('main::trailers')?></a>&nbsp;
			<a href="<?=main_films::getFilmPostersUrl($_GET['film_id'])?>"><?=sys::translate('main::posters')?></a>&nbsp;
			<a href="<?=main_films::getFilmDiscussUrl($_GET['film_id'])?>"><?=sys::translate('main::discuss')?></a>&nbsp;
			<br>
			<a href="<?=main_films::getFilmRatingUrl($_GET['film_id'])?>" title="<?=sys::translate('main::rating_of_film')?> <?=$var['title']?>"><img alt="<?=sys::translate('main::rating_of_film')?> <?=$var['title']?>" src="<?=_ROOT_URL?>rating_<?=$_GET['film_id']?>.gif">
			<?echo sys::stripNewLine(ob_get_clean())?>
		</textarea>
		<textarea readonly style="width:100%; height:100px; display:none; margin-top: 15px;"  name="forum_code">
			<?ob_start()?>
				[img]<?=$var['poster']['image']?>[/img]
				[br]
			[url=<?=main_films::getFilmUrl($_GET['film_id'])?>][b]<?=($var['serial'])?sys::translate('main::serials_article'):sys::translate('main::film')?> <?=$var['title']?> /<?=$var['title_orig']?>[/b][/url]
			[br]
			<?if($var['genres']):?>
				[b]<?=sys::translate('main::genre')?>[/b]: <?=implode(', ',$var['genres'])?>
				[br]
			<?endif;?>
			<?if($var['directors']):?>
				[b]<?=sys::translate('main::director_s')?>[/b]:
				<?$i=1?>
				<?foreach($var['directors'] as $director):?>
					[url=<?=$director['url']?>]<?=$director['fio']?>[/url] <?if($i<count($var['directors'])):?>, <?endif;?>
					<?$i++?>
				<?endforeach;?>
				[br]
			<?endif?>
			<?if($var['actors']):?>
				[b]<?=sys::translate('main::in_roles')?>[/b]:
				<?$i=1?>
				<?foreach($var['actors'] as $actor):?>
					 [url=<?=$actor['url']?>]<?=$actor['fio']?>[/url]<?if($i<count($var['actors'])):?>, <?endif;?>
					<?$i++?>
				<?endforeach;?>
				[br]
			<?endif;?>
			<?if($var['countries']):?>
				[b]<?=sys::translate('main::country')?>:[/b] <?=implode(', ',$var['countries'])?>
				[br]
			<?endif;?>
			<?if($var['studios']):?>
				[b]<?=sys::translate('main::studio')?>:[/b] <?=implode(', ',$var['studios'])?>
				[br]
			<?endif;?>
			<?if($var['year']):?>
				[b]<?=sys::translate('main::year')?>:[/b] <?=$var['year']?>
				[br]
			<?endif?>
			<?if($var['budget']):?>
				[b]<?=sys::translate('main::budget')?>:[/b] <?=$var['budget']?>
				[br]
			<?endif?>
			<?if($var['ukraine_premiere']):?>
				[b]<?=sys::translate('main::ukraine_premiere')?>:[/b] <?=$var['ukraine_premiere']?>
				[br]
			<?endif;?>
			<?if($var['world_premiere']):?>
				[b]<?=sys::translate('main::world_premiere')?>:[/b] <?=$var['world_premiere']?>
				[br]
			<?endif;?>
			<?if($var['duration']):?>
				[b]<?=sys::translate('main::duration')?>:[/b] <?=$var['duration']?> <?=sys::translate('main::min')?>
				[br]
			<?endif;?>
			<?if($var['age_limit']):?>
				[b]<?=sys::translate('main::age_limits')?>:[/b] <?=$var['age_limit']?>
				[br]
			<?endif?>
			<?=strip_tags($var['intro'])?>
			[br]
			[url=<?=main_films::getFilmUrl($_GET['film_id'])?>]<?=sys::translate('main::more_about_film')?>[/url] [url=<?=main_films::getFilmTrailersUrl($_GET['film_id'])?>]<?=sys::translate('main::trailers')?>[/url] [url=<?=main_films::getFilmPostersUrl($_GET['film_id'])?>]<?=sys::translate('main::posters')?>[/url] [url=<?=main_films::getFilmDiscussUrl($_GET['film_id'])?>]<?=sys::translate('main::discuss')?>[/url]
			[br]
			[url=<?=main_films::getFilmRatingUrl($_GET['film_id'])?>][img]<?=_ROOT_URL?>rating_<?=$_GET['film_id']?>.gif[/img][/url]
			<?echo str_replace('[br]',"\n",sys::stripNewLine(ob_get_clean()))?>
		</textarea>
	</div>
<?=banners::showBanners('v3_secondbanner')?>

					<?if ($var['trailer']):?>
<div itemprop="video" itemscope itemtype="//schema.org/VideoObject">
<meta itemprop="thumbnailUrl" content ="<?=$_cfg['main::films_url'].$var['image']?>">
<meta itemprop="uploadDate" content ="<?=$var['trailer_date']?>">
						<div id='lineBlank' style='height: 24px;'><img src='/images/blank.gif' height='1'></div>
						<div id='lineHrWide'>&nbsp;</div>
						<div id='lineBlank' style='height: 7px;'><img src='/images/blank.gif' height='1'></div>
					<div id='searchTitle' itemprop="name"><?if($var['serial']):?><?=sys::translate('main::serialTrailer')?><?else:?><?=sys::translate('main::filmTrailer')?><?endif;?> <?=$var['title']?></div>
					<div id='lineBlank' style='height: 13px;'><img src='/images/blank.gif' height='1'></div>
<!-- start trailer -->

				<center>
				<div id="container">Loading the player ...</div>
				<script type="text/javascript"> jwplayer("container").setup({
					id: 'player',
					width: 664,
					height: 360,
					file: "<?=$var['file']?>",
					image: "/public/main/resize.php?f=<?=$var['image']?>&w=664",
					startparam: "ec_seek",
					advertising: {
					client: 'vast',
					tag: 'https://ima3vpaid.appspot.com/?adTagUrl=https%3A%2F%2Fgoogleads.g.doubleclick.net%2Fpagead%2Fads%3Fclient%3Dca-video-pub-8309773808661346%26slotname%3D9405725010%26ad_type%3Dvideo_text_image%26description_url%3Dhttps%253A%252F%252Fkino-teatr.ua%252F%26videoad_start_delay%3D0&type=all'
  }
				}); </script>
					</center>
	<div id='lineBlank' style='height: 7px;'><img src='/images/blank.gif' height='1'></div>
<!-- end trailer -->
					<a href='<?=main_films::getFilmTrailersUrl($_GET['film_id'])?>' class='watchonlinea' title='<?=($var['serial'])?sys::translate('main::serials_article'):sys::translate('main::film')?> <?=$var['title']?> <?=sys::translate('main::watchonline')?>' itemprop="description"><?=($var['serial'])?sys::translate('main::serials_article'):sys::translate('main::film')?> <?=$var['title']?> <?=sys::translate('main::watchonline')?></a>

</div><br>
<script src='//tnt-play.ru/kinoafisha.html?u=16' type='text/javascript'></script>
                    <?endif;?>
						<div id='lineBlank' style='height: 21px;'><img src='/images/blank.gif' height='1'></div>


	
<?if($var['reviews']):?>
<script>
function showReview(ii)
{
	$('.intro_'+ii).css('display','none');
	$('.rev_'+ii).css('display', 'block');
}

function hideReview(ii)
{
	$('.rev_'+ii).css('display', 'none');
	$('.intro_'+ii).css('display','block');
}

</script>
					<div id='searchTitle'><?if($var['serial']):?><?=sys::translate('main::reviews_on_serial')?><?else:?><?=sys::translate('main::reviews_on_film')?><?endif;?> <?=$var['title']?></div>
					<?if(sys::checkAccess('main::reviews_add')):?>
						<a href='<?=main_reviews::getReviewAddUrl($_GET['film_id'])?>' id='additionalButton'><?=sys::translate('main::add_review')?></a>
					<?else:?>
						<a href='<?=sys::rewriteUrl('?mod=main&act=login')?>' id='additionalButton'><?=sys::translate('main::add_review')?></a>
					<?endif?>

					<div id='lineHrWide'>&nbsp;</div>
					
						<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>


			<?foreach ($var['reviews'] as $review):?>

										<div id='userTextHeader'>
							<?/* <div id='userPicture'><img src='<?=$review['image']?>'></div> */?>
							<div id='userInfo'>
								<a href='<?=$review['url']?>' title='<?=$review['title']?>'><?=$review['title']?></a>
								 <?=$review['user']?>, <?=$review['date']?>
							</div>
						</div>

						<div id='lineBlank' style='height: 11px;'><img src='/images/blank.gif' height='1'></div>

					<div id='filmText' class='intro_<?=$review['id']?>'>
							<?=$review['intro']?>

							<a href='#' class='revLink' onclick='showReview(<?=$review['id']?>); return false;'><?=sys::translate('main::more')?></a>
					</div>

					<?if ($review['text']):?>
					<div id='filmText' class='rev_<?=$review['id']?>' style='display:none;'>
							<?=$review['text']?>
							<a href='#' class='revLink' onclick='hideReview(<?=$review['id']?>); return false;'><?=sys::translate('main::less')?></a>
					</div>
					<?endif;?>

						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
						<div id='lineHrWide'>&nbsp;</div>
						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
			<?endforeach;?>

	<?else:?>


<div id='searchTitle'><?if($var['serial']):?><?=sys::translate('main::reviews_on_serial')?><?else:?><?=sys::translate('main::reviews_on_film')?><?endif;?> <?=$var['title']?></div>

<div id='lineHrWide'>&nbsp;</div>
						<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
			<?=sys::translate('main::noreviews')?>			
					<?if(sys::checkAccess('main::reviews_add')):?>
						<a href='<?=main_reviews::getReviewAddUrl($_GET['film_id'])?>' id='additionalButton'><?=sys::translate('main::add_review')?></a>
					<?else:?>
						<a href='<?=sys::rewriteUrl('?mod=main&act=login')?>' id='additionalButton'><?=sys::translate('main::add_review')?></a>
					<?endif?>

    <?endif;?>


						<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

					<?// =banners::showBanners('v3_secondbanner')?>

					<?//=main_discuss::showDiscussMain($_GET['film_id'],'film')?>
					
					<?=main_discuss::showDiscussMain($_GET['film_id'],'film',main_films::getFilmUrl($_GET['film_id']))?>

					<?=main_films::showFilmsLinks4($_GET['film_id'])?>  
<br>
<?=banners::showBanners('V3_page_down')?>
						<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>


           <script>

           function show()
           {
           		document.getElementById('myriad').style.visibility = 'visible';
           }

			setTimeout(show,2000);
           </script>



</div>