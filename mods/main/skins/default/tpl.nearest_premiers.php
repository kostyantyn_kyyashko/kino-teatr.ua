


<ul class='lidersUl' >
	<?foreach ($var['objects'] as $obj):?>
		<li class='<?=$obj['class']?>'>
			<a href='<?=$obj['url']?>' class='liderImgLink' title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=$obj['year']?>">
				<img src='<?=$obj['image']?>'>
			</a>
			<div class='liderName'><?=$obj['title']?></div>
			<div class='liderDate'><?=$obj['date']?></div>
			<ul>
				<?if($obj['isTrailer']=='1'):?><li class='trailerLi'><a href='<?=$obj['trailers_url']?>' title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=sys::translate('main::watchonline')?>"><?=sys::translate('main::trailer')?></a></li><?endif;?>
				<?if($obj['isShows']=='1'):?><li class='watchLi'><a href='<?=$obj['shows_url']?>' title="<?=sys::translate('main::film')?> <?=$obj['title']?> - <?=sys::translate('main::filmshows')?>"><?=sys::translate('main::whereToWatch')?></a></li><?endif;?>
			</ul>
		</li>
	<?endforeach;?>
</ul>




