
<script>
    var session_id = '';
    var sessionTime = 1800;
    var showId = '<?php echo $var['show_id'];?>';
    var eventId = '<?php echo $var['event_id'];?>';
    var siteId = '<?php echo $var['site_id'];?>';
    var film_id = '<?php echo $var['film_id'];?>';
    var hall_id = '<?php echo $var['hall_id'];?>';
    var cinema_id = '<?php echo $var['cinema_id'];?>';
    var lang_id = '<?php echo $var['lang_id'];?>';
	var cinema_name = '<?php echo $var['cinema_name'];?>';
	<?php if($var['cards_box']):?>
		var have_cards = 1;
	<?php else:?>
		var have_cards = 0;
	<?php endif;?>
</script>

<style type="text/css">
	.booking {
		text-align: center;
	}
	.booking div {
		margin-top: 20px;
	}
	.booking table td {
		font-size: 16px;
	}
	
	.booking_result div{
		margin-bottom: 0px;
		margin-top: 0px;
		text-align: center;
	}
	.booking_result table tr td {
		font-size: 12px;
	}
	
	.buying_table
	{
		font-size: 14px;
	}

</style>

<?php
?>

<!--div class="popup-scroll places-theatre clearfix"-->
    <input id="basket-count-items" value="0" type="hidden" />

    <div class="afisha-image clearfix">
		<?php
				if(file_exists('/public/main/cinemas/'.$var['image_cinema']))
					$poster = '<img src="/public/main/rescrop.php?f=public/main/cinemas/'.$var['image_cinema'].'&t=5" alt="event" width="65" class="wg-poster">';
				else
					$poster = '';
				echo $poster;
			?>

        <div class="popup-place">
            <!--h1>
				Форсаж 8
                                    <sup class="filmAgeLimit">16+</sup>
                
							</h1-->
			<h2> <?php echo $var['film'];?>
				
                                    <sup class="filmAgeLimit"><?php echo $var['age_limit'];?>+</sup>
			</h2>
            <p><?php echo sys::russianDate($var['date']);?> <?php echo $var['time'];?>
              <span class="wg-hall"><?php echo $var['hall'];?></span> ·  
            <span id="site-name">Кінотеатр <?php echo $var['cinema_name'];?></span></p>
			<?php
				if(file_exists('/public/main/films/x1_'.$var['image']))
					$poster = '<img src="/public/main/films/x1_'.$var['image'].'" alt="place">';
				else
					$poster = '';
			?>

            <div><?php echo $poster;?><span class="wg-site-address">м. <?php echo $var['city'];?>, <?php echo $var['address'];?></span></div>
        </div>

        <!--div class="wg-timer">
            Час на оплату: <span id="mapTimer">29:03</span>
        </div-->
    </div>

    <div class="price-range">
    	<div class="price-range-drug">
	        <ul class="wg-chairs-prices">
             <!--li>
                <div style="background-color:#006600" class="wg-chair-color"></div>
                <div class="wg-price"><?php echo $var['prices'];?> грн</div>
            </li-->
			<?php echo $var['show_prices'];?>
        </ul>
        </div>
    </div>
	

    <div class="hall-image">
        <div class="wg-row-map">

			<?php
				$background_width = $var['background_width'] / 2.15;
			
			?>
			<div class="wg-map-hall wg-map-hall-florentsia-15" style="float: none; width: 70%; margin: 0px auto; height: 382px;">
			<div class="wg-sectors">

<div class="wg-rows">

	<?php echo $var['map'];?>
                   
				  
				  
	                                                </div>
</div>

<ul class="wg-chairs-prices-bottom">
	
</ul>

<input id="wg-max-sector-width" value="1450" type="hidden">
<input id="wg-max-sector-height" value="1259" type="hidden">
<input id="wg-place-size" value="50" type="hidden">
<img class="wg-map-pattern" src="<?php echo $var['background_map'];?>" width="<?=$var['background_width']*0.4?>" data-src="<?=$var['background_width']?>">
</div>

        </div>
    </div>
		<div class="booking" style="display: none; height: 400px; margin-left: 15%; margin-top: 100px; margin-bottom: 0px;">
		<div class="booking_result" style="margin-bottom: 15px;width: 400px; display: none;">
			
		</div>
		<form action="/basket.php?method=basket-book" style="height: 380px; text-align: center;" onsubmit="booking(this);return false;">
			<table cellpadding="4" cellspacing="2">
				<tr>
					<td align="left">
						<label>Имя</label>
					</td>
					<td align="left" valign="top">
						<input type="text" name="name" style="width: 200px;" class="name" <?php if($var['username']) echo 'value="'.$var['username'].'"';?>/>
					</td>
				</tr>
				<tr>
					<td style="font-size: 10px;">&nbsp;</td>
					<td style="font-size: 10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="left" valign="top">
						<label>Email</label>
					</td>
					<td align="left">
						<input type="text" name="email" style="width: 200px;" class="email" <?php if($var['user_mail']) echo 'value="'.$var['user_mail'].'"';?>/>
					</td>
				</tr>
				<tr>
					<td style="font-size: 10px;">&nbsp;</td>
					<td style="font-size: 10px;">&nbsp;</td>
				</tr>
				<tr>
					<td align="left" valign="top">
						<label>Телефон</label>
					</td>
					<td align="left">
					<?php if($var['user_phone']):?>
						<input type="tel" name="phone" style="width: 200px;" class="phone" value="<?=$var['user_phone']?>"/>
					<?php else:?>
						<input type="tel" name="phone" style="width: 200px;" class="phone"/>
					<?php endif;?>
					</td>
				</tr>
				
				
				<tr>
					<td align="left">
						<label>Комментарий</label>
					</td>
					<td align="left">
						<textarea name="description" rows="2" cols="18" style="width: 200px;" class="description"></textarea>
					</td>
				</tr>
			<?php if($var['cards_box']):?>
			<tr class="payment_info">
				<td align="left">
					<label>Карта оплаты <a href="<?=$var['add_url']?>" target="_blank" style="margin-left: 5px; font-size: 14px; text-decoration: none; color: brown;" title="Добавить карту">+</a></label>
				</td>
				<td align="left" valign="top">
					<table>
						<tr>
							<td valign="top"><?=$var['cards_box'];?></td>
							<td valign="top"><img src="/mods/main/skins/default/images/masterpass4.png" width="100"/></td>
						</tr>
					</table>
					<?php
					?>
				</td>
			</tr>	
			<?php else:?>
			<tr class="payment_info">
				<td align="left">
					<label>Карта оплаты</label>
				</td>
				<td align="left" valign="top">
					<table>
						<tr>
							<td valign="top"><a href="<?=$var['add_url']?>" target="_blank" style="font-size: 12px;">Добавить карту</a></td>
							<td valign="top" style="padding-left: 5px;"><img src="/mods/main/skins/default/images/masterpass4.png" width="100"/></td>
						</tr>
					</table>
					<?php
					?>
				</td>
			</tr>	
			
			<?php endif;?>
			<tr>
					<td style="font-size: 10px;">&nbsp;</td>
					<td style="font-size: 10px;">&nbsp;</td>
				</tr>
			<tr>
				<td style="padding-left: 0px;" class="container_submit" align="left" nowrap="">
					<span class="container_booking"><input type="submit" value="Бронировать" onclick="set_submit(this,'book');" class="btn_booking"/></span>
					<?php if($var['cards_box']):?>
						<span style="margin-left: 5px;" class="container_buying"><input type="submit" value="Купить" style="width: 60px;" onclick="set_submit(this,'buy');" class="btn_buying"/></span>
					<?php endif;?>
				</td>
				<td class="container_button" align="right">
					<input type="button" value="Отменить"  onclick="cancel_booking(this);return false;"/>
				</td>
			</tr>
			<tr>
				<td style="font-size: 14px;">&nbsp;</td>
				<td style="font-size: 14px;">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="left">
					<label style="font-size: 12px;">Для бронирования укажите email или телефон</label>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="left">
					<label style="font-size: 12px;">Для покупки укажите телефон</label>
				</td>
			</tr>
			</table>
			
			<div>
			</div>
		</form>
	</div>

    <div class="booked-places-counter wg-cart-line" style="text-align: center; margin-left: 30%;">
		<?php if($var['basket']):?>
		<div class="content_basket"><?php echo $var['basket'];?></div>
	<?php else:?>
		<div class="content_basket" style="display: none;">	</div>
	<?php endif;?>
	</div>
    <div class="booked-places-counter wg-cart-line checkout_info"><h3>Оберіть місця на схемі залу та натисніть на них</h3></div>
	<div style="clear: both;"></div>
	<?php
		$button_width = $var['background_width'] * 0.4;
		if(!$button_width)
			$button_width = 300;
	?>
	
	<div class="container_3ds">
		
	</div>

    <a id="wg-checkout" class="button-common" href="#" style="<?php if(!$var['basket']) echo 'display: none;'; else echo 'display: block;';?> text-align: center; margin-left: 25%; width: <?php echo $background_width;?>" onclick="show_booking(this);return false;">ОФОРМИТИ ЗАМОВЛЕННЯ</a>
	
    
<!--/div-->
<input id="wg-event-id" value="<?php echo $var['event_id'];?>" type="hidden" />

