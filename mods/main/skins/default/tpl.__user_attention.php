<?
$msg = sys::parseModTpl('main::user_attention');
$allow = false;
$redirect = false;
if ($msg && !$_COOKIE['main::user_attention']) {
    setcookie('main::user_attention', 1, 2147483647, '/');
    if ($msg && ($_user['login'] == '' ||
            $_user['main_nick_name'] == '' ||
            $_user['main_first_name'] == '' ||
            $_user['main_last_name'] == '' ||
            $_user['email'] == '' ||
            $_user['main_birth_date'] == '' ||
            $_user['main_sex'] == '')) {
        $allow = true;
        $redirect = true;
        $_lang = $_COOKIE['sys::lang'] ? $_COOKIE['sys::lang'] : 'ru';
        $pu = parse_url($_SERVER['REQUEST_URI']);
        $path = explode('/', $pu['path']);
        if (@$path[2] == 'main' && @$path[3] == 'user_profile.phtml')
            $redirect = false;
    }
}
?>
<? if ($allow): ?>
<script>
    $(function(){
        alert('<?=$msg?>');
        <? if ($redirect): ?>
        window.location.href = '/<?=$_lang?>/main/user_profile.phtml';
        <? endif; ?>
    });
</script>
<? endif; ?>