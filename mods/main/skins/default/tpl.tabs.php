<ul id='filmMenu'>
<?foreach ($var['objects'] as $key=>$obj):?>
	<?if($key==$var['active']) $class='current'; else $class=false;?>
	<li>
		<a 
			title="<?=$obj['alt']?>" 
			href="<?=$obj['url']?>" 
			class='<?=$class?>'
		>
			<?if ($key=='online'):?>
				<img 
					src='/mods/main/skins/default/images/onlineBtn.png' 
					style='float: left;  margin-top: 3px; margin-right: 10px;'
					>
			<?endif;?>
			<?=$obj['title']?>
		</a>
			<?if ($obj["help_icon"]):?>
			 <div style="margin-top: 8px; float: right; width:4px;">
			  <img src='<?=$obj["help_icon"]?>' onclick="javascript:{return false;}" title="<?=$obj["help_title"]?>" style="left: -20px; position: relative;">			
			 </div>
			<?endif;?>

			<?if ($obj["buy_ticket"]):?>
			<?
			  if ($obj["icon_href"])
			 	$href="window.document.location='".$obj["icon_href"]."';";
			  else 
			    $href = "return false;";
			?>
			
			 <div style="margin-top: 8px; float: right; width:4px;">
			  <img src='<?=$obj["icon"]?>' onclick="javascript:{<?=$href?>}" title="<?=$obj["icon_title"]?>" style="left: -20px; position: relative;">			
			 </div>
			<?endif;?>
	</li>
<?endforeach;?>
</ul>
