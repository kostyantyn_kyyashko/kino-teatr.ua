<?=sys::useLib('main::films');?>
<script type="text/javascript">
function loadRating(rating)
{
	$('div.markForm<?=$var['id']?> img.star').each(
		function(){
			var mark=Math.round($(this).attr('num'));
			var rest=rating-Math.floor(rating);
			if(mark<=Math.floor(rating))
			{
				$(this).attr('src','<?=_IMG_URL.'star.png'?>');
			}
			else if(mark==Math.floor(rating)+1 || (rating<1 && mark==1))
			{
				if(rest<0.25)
					$(this).attr('src','<?=_IMG_URL.'star0.png'?>');
				if(rest>=0.25)
					$(this).attr('src','<?=_IMG_URL.'star1.png'?>');
				if(rest>=0.5)
					$(this).attr('src','<?=_IMG_URL.'star2.png'?>');
				if(rest>=0.75)
					$(this).attr('src','<?=_IMG_URL.'star3.png'?>');
			}
			else
				$(this).attr('src','<?=_IMG_URL.'star0.png'?>');
		}
	)
}
$(function(){

	$("div.markForm<?=$var['id']?> img.star").hover(
		function(){
			$(this).css('opacity','0.3');
			var num=Math.round($(this).attr('num'));
			$('div.markForm img.star').each(
				function()
				{
					if(Math.round($(this).attr('num'))<num)
						$(this).css('opacity','0.3');
				}
			)
		},
		function(){
			$(this).css('opacity','1');
			var num=Math.round($(this).attr('num'));
			$('div.markForm<?=$var['id']?> img.star').each(
				function()
				{
					if(Math.round($(this).attr('num'))<num)
						$(this).css('opacity','1');
				}
			)
		}
	)

	<?if($_user['id']!=2):?>
		$("div.markForm<?=$var['id']?> a.vote").click(
			function(){
				var mark=$(this).attr('mark');
				$.ajax({
					url:'?mod=main&act=ajx_vote_<?=$var['type']?>',
					data:{
						mark:mark,
						object_id:<?=$var['object_id']?>
					},
					beforeSend:function(){
						$('#topStar<?=$var['id']?>').html('...');
						$('#imdbRating<?=$var['id']?>').html('...');
					},
					dataType:'json',
					success:function(data)
					{
						$('div.markForm<?=$var['id']?> span.yourMark').html(mark);
						if(data)
						{
							$('#topStar<?=$var['id']?>').html(data['rating']);
							$('#imdbRating<?=$var['id']?>').html(data['votes']);
							loadRating(data['rating']);
						}
					},
					type:'POST'
				})


				return false;
			}
		)
	<?endif?>
})

</script>


<div class="markForm<?=$var['id']?> topMark">
	<?for($i=1; $i<11; $i++):?>
		<?
			if($i<=floor($var['rating']))
			{
				$src=_IMG_URL.'star.png';
			}
			elseif($i==floor($var['rating']+1) || ($var['rating']<1 && $i==1))
			{
				if($var['rest']<0.25)
					$src=_IMG_URL.'star0.png';
				if($var['rest']>=0.25)
					$src=_IMG_URL.'star1.png';
				if($var['rest']>=0.5)
					$src=_IMG_URL.'star2.png';
				if($var['rest']>=0.75)
					$src=_IMG_URL.'star3.png';
			}

			else
				$src=_IMG_URL.'star0.png';
		?>
		<a class="vote" mark=<?=$i?> href="<?=sys::rewriteUrl('?mod=main&act=login')?>"><img num="<?=($i)?>" class="star" src="<?=$src?>" alt=""></a>
	<?endfor?>
</div>

					<span id='imdb'><?=sys::translate('main::votes')?>: </span>
					<span id='imdbRating<?=$var['id']?>' class='topImdbRating'><?=$var['votes']?></span>



