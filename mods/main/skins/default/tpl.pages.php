<?if(count($var['pages'])>1):?>
					<div id='pages'>

					<?if($var['prev_page']):?>
						<a id='prevPage' title="<?=sys::translate('sys::prev_page')?>" href="<?=$var['prev_page']?>">&nbsp;</a>
					<?else:?>
						<a class='prevInactive' id='prevPage'>&nbsp;</a>
					<?endif?>


					<ul id='pagesUl'>
						<?foreach ($var['pages'] as $number=>$url):?>
							<?if($number==$var['current_page']):?>
								<li class='active'><a ><?=$number?></a></li>
							<?else:?>
								<li><a href="<?=$url?>"><?=$number?></a></li>
							<?endif?>
						<?endforeach;?>

					</ul>

					<?if($var['next_page']):?>
						<a title="<?=sys::translate('sys::next_page')?>" href="<?=$var['next_page']?>" id='nextPage'>&nbsp;</a>
					<?else:?>
						<a class='nextInactive' id='nextPage'>&nbsp;</a>
					<?endif?>


					</div>
<?endif;?>


