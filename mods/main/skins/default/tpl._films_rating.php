<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=18?>
<?$_var['active_child_menu']=19?>
<?sys::setMeta($var['meta']);?>
<?//main::showGemius('0sibwqN2iMUk43dsvzmHo8SsTO5_HdByi1lhbtysaND.77');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

				<div class='myriad' id='myriad'><?=sys::translate('main::films_ratings')?></div>

<?if($var['seo']):?>
<a href id='help' onclick='showHelp();return false;'>&nbsp;</a>
<?endif;?>

						<div id='lineBlank' style='height: 5px;'><img src='/images/blank.gif' height='1'></div>


<?if($var['seo']):?>
<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
<div id='helpDiv' style='display: none;'>
<?=$var['seo'];?>
</div>

<?endif;?>


						<div id='lineBlank' style='height: 5px;'><img src='/images/blank.gif' height='1'></div>
					<div id='news_page'>

	<form method="GET" action="<?=sys::rewriteUrl('?mod=main&act=films_rating&order_by='.$_GET['order_by'])?>">
	<div id='filmsFilter'>
		<div id='filterFirstLine'>
			 <?=$var['fields']['title']['title']?>: <?=sys_form::parseField('title',$var['fields']['title'],' maxlength="55"')?>
			 <span>&nbsp;</span>
			 <?=$var['fields']['genre_id']['title']?>: <?=sys_form::parseField('genre_id',$var['fields']['genre_id'])?>
			 <span>&nbsp;</span>
			 <?=$var['fields']['year']['title']?>: <?=sys_form::parseField('year',$var['fields']['year'])?>
		</div>
		<div id='filterSecondLine'>

			<div id='filterCheckBox'>
			      <?=$var['fields']['show']['title']?> <?=sys_form::parseField('show',$var['fields']['show'])?>
			</div>

			<input type="submit" class="filterFind" value="<?=sys::translate('main::find')?>">

		</div>
	</div>
	</form>
	<br>
	
	<?if($var['objects']):?>


<div id='news_page' class='filmsNearList'>

			<?foreach ($var['objects'] as $obj):?>
				<?$last=$obj['class'];?>


					<?if ($obj['new_year']):?>
					<div id="searchTitle" style='text-align: center; width: 100%; font-size: 18px; color: #898989; background: #f4f4f4; margin-top: 15px; margin-bottom: 30px; line-height: 40px;'><?=$obj['new_year']?></div>
					<?endif;?>


						<div id='searchItemFilms' class='<?=$obj["class"]?>'>
							<a href='<?=$obj['url']?>' class='searchPhoto' title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=$obj['year']?>"><img  src="<?=$obj['poster']?>" title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=$obj['year']?>" alt="<?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot;"></a>

							<div id='searchItemMainText'>

					<div id='actors' style='width: 100px; margin-top: 0px;'>
						<ul id='actorsList'>
 							<?if($obj['revurl']):?>
								<li>
									<a target="_blank" title="<?=sys::translate('main::film')?> <?=$obj['title']?> - <?=sys::translate('main::reviews')?>" href="<?=$obj['revurl']?>"><?=sys::translate('main::reviews')?></a>
								</li>
							<?endif;?>
 							<?if($obj['photurl']):?>
								<li>
									<a target="_blank" title="<?=sys::translate('main::film')?> <?=$obj['title']?> - <?=sys::translate('main::frames')?>" href="<?=$obj['photurl']?>"><?=sys::translate('main::frames')?></a>
								</li>
							<?endif;?>
 							<?if($obj['posturl']):?>
								<li>
									<a target="_blank" title="<?=sys::translate('main::film')?> <?=$obj['title']?> - <?=sys::translate('main::posters')?>" href="<?=$obj['posturl']?>"><?=sys::translate('main::posters')?></a>
								</li>
							<?endif;?>
 							<?if($obj['trailurl']):?>
								<li>
									<a target="_blank" title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=sys::translate('main::watchonline')?>" href="<?=$obj['trailurl']?>"><?=sys::translate('main::trailers')?></a>
								</li>
							<?endif;?>
						</ul>
					</div>
								<div id='searchItemTitle'>
									<a href='<?=$obj['url']?>' class='searchItemLink' title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=$obj['year']?>"><?=$obj['title']?></a><br>
									<span class='searchItemOriginal'><nobr><?=$obj['round']?></nobr></span>
								</div>

								
								<p class='searchItemText'>
										<?=$obj['rate']?>
										<?=$obj["expect"]?>
										<?=$obj['page_views']?>
								</p>
								<p class='searchItemText'>
										<?=$obj['second_row']?>
								</p>

								<p class='searchItemText'>
										<?=$obj['third_row']?>
								</p>
																			
							</div>

								<div id='premiereBigDate'>
								<span><?=$obj['day']?></span><br>
								<?=$obj['month']?>
								</div>
							
								<div class='ranksListStar'>
								    <span class="thumbs-star"></span>
									<span><?=$obj['rating']?></span>
								</div>
								
								<div class="ranksListThumbs">
									<span class="thumbs-good"></span>
									<span><?=$obj['expect_yes']?></span>
								</div>	 
								
								<div class="ranksListThumbs">
									<span class="thumbs-bad"></span>
									<span><?=$obj['expect_no']?></span> 
								</div>
								
						<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
						</div>

						<div id='lineHr'>&nbsp;</div>

			<?endforeach;?>

			</div>

<div id='ranksFooter'>
		<?=sys::parseTpl('main::pages',$var['pages'])?>
</div>
	<?else:?>
		<center class="xErr"><?=sys::translate('main::nothing_found')?></center>
	<?endif?>

</div>

						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>

				<?=banners::showBanners('v3_secondbanner')?>