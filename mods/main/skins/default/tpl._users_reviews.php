<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=9?>
<?$_var['active_child_menu']=14?>
<?sys::setMeta($var['meta']);?>
<?//main::showGemius('BxXlPWesbcOpRUSgTXzXkYZiLcBRDab_s0kaAps_0Lv.j7');?>
<?ob_start()?>
 <?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>


				<div class='myriad' id='myriad'><?=$var['pagename']?></div>

				<ul id='NewsPageAdditional'>
					<li><a href='<?=$var["jour_revs"]?>'><span><?=sys::translate('main::writists')?></span></a></li>
					<li class='active'><a href='<?=$var["user_revs"]?>'><span><?=sys::translate('main::users')?></span></a></li>
					<li><a href='<?=$var["popular_revs"]?>'><span><?=sys::translate('main::popularNews')?></span></a></li>
				</ul>


					<div id='lineHrWide'>&nbsp;</div>

	<div id='news_page'>
	<?$i=1;?>

	<?foreach ($var['objects'] as $obj):?>

							<div id='NewsItem'>
							<a href='<?=$obj['url']?>' class='mainNewsPhoto'><img src="<?=$obj['image']?>" alt="<?=$obj['title']?>" title="<?=$obj['title']?>"></a>

							<div id='NewsItemTitle'>
								<a href='<?=$obj['url']?>' class='mainNewsLink' title='<?=$obj['title']?>'><?=$obj['title']?></a><br>
								<span class='NewsItemDate'><?=$obj['user']?>, <?=$obj['date']?>,
								<?=sys::translate('main::review_on_film')?>
								<a href="<?=$obj['film_url']?>" title="<?=sys::translate('main::film')?> <?=$obj['film']?> <?=$obj['year']?>"><?=$obj['film']?></a></span>
							</div>

							<p class='NewsItemText'>
								<?=$obj['intro']?>
							</p>

							<div id='NewsItemBottom'>
								<div class='NewsItemViews'><?=$obj['shows']?></div>
								<div class='NewsItemDiscuss'><?=$obj['comments']?></div>
							</div>

						</div>


						<div id='lineHr'>&nbsp;</div>

			<?if($i==5):?>
					<div id='secondbanner' style='margin-bottom: 12px;'></div>
			<?endif;?>

		<?$i++;?>

	<?endforeach;?>
	</div>


		<?=sys::parseTpl('main::pages',$var['pages'])?>

						<div id='lineBlank' style='height: 20px;'><img src='/images/blank.gif' height='1'></div>
