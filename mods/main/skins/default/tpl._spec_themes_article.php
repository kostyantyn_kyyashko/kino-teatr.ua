<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=9?>
<?$_var['active_child_menu']=46?>

<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

	<div class='myriadFilm' id='myriad'><?=$_var['main::title']?> &nbsp;</div>

	<ul id='blockHeaderMenu'>
		<li class='active'>
			<a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?><?endif;?>spec_themes.phtml'>
				<span><?=sys::translate('main::all_spec_themes')?></span> 
			</a>
		</li> 
	</ul>
	<div id='lineHrWide'>&nbsp;</div>
	<div id='lineBlank' style='height: 20px;'><img src='/images/blank.gif' height='1'></div>

	<div class="xContent">
		<?=$var['text']?>
	</div>

	<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

	<div id='secondbanner' style='margin-bottom: 12px;'></div>


					<div id='news_page'>

<?$i=1;?>

					<?foreach ($var['objects'] as $obj):?>
						<div id='NewsItem'>
							<a href='<?=$obj['url']?>' class='mainNewsPhoto'>
							 <img src="<?=$obj['image']?>" alt="<?=$obj['title']?>" title="<?=$obj['title']?>">
							</a>
							
							<div id='NewsItemTitle'>							
							    <span class="NewsItemDate"><?=$obj['the_type']?></span> 
							    <span class='NewsItemDate'><nobr><?=$obj['date']?></nobr></span><br>
								<a href='<?=$obj['url']?>' class='mainNewsLink' title='<?=$obj['title']?>'><?=$obj['title']?></a>
								<?if($obj["exclusive"]):?><sup class="xExtra"><?=sys::translate('main::exclusive')?></sup><?endif;?>
								
							</div>

							<p class='NewsItemText'>
								<?=$obj['intro']?>
							</p>

							<div id='NewsItemBottom'>
								<div class='NewsItemViews'><?=$obj['total_shows']?></div>
								<div class='NewsItemDiscuss'><?=$obj['comments']?></div>
								<div class=""><?=sys::translate('main::author')?>: <?=$obj['user']?></div>
							</div>

						</div>


						<div id='lineHr'>&nbsp;</div>
<?$i++;?>
					<?endforeach;?>
					</div>
	<?=sys::parseTpl('main::pages',$var['pages'])?>
    <div style='clear: both; margin-bottom: 20px;'>&nbsp;</div> 
	<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>

	   