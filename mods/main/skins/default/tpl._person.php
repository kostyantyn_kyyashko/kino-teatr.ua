<?sys::useLib('content');?>
<?sys::useLib('main::discuss');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=6?>
<?$_var['active_child_menu']=8?>
<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

<div itemscope itemtype="//schema.org/Person">

	<div class='myriadFilm' id='myriad'><?=$var['title']?></div>
	<div id='lineHrWide'>&nbsp;</div>
	<?=main_persons::showPersonTabs($_GET['person_id'],$var['fio'],'info');?>
	<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

					<div id='posterBlock'>
						<a href='<?=$var['photo']['url']?>' id='filmPoster'><img src='<?=$var['photo']['image']?>' alt="<?=sys::translate('main::person')?> - <?=$var['fio']?>" title="<?=$var['fio']?>" itemprop="image"></a>
						<div id='lineBlank' style='height: 5px;'><img src='/images/blank.gif' height='1'></div>
                        <script src="//connect.facebook.net/ru_RU/all.js#xfbml=1"></script><fb:like href="" layout="button_count" show_faces="false" width="100" font=""></fb:like>
						<div id='lineBlank' style='height: 5px;'><img src='/images/blank.gif' height='1'></div>
						<a href="//twitter.com/share" class="twitter-share-button" data-count="horizontal" rel='nofollow'>Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
					</div>

					<div id='filmPersons'>
							<strong class='red' itemprop="name"><?=$var['title']?></strong> <?if ($var['title_orig']):?><span class='grey'>(<?=$var['title_orig']?>)</span><?endif;?><br>
							<?if($var['birthdate']):?>								
								<strong><?=sys::translate('main::birthdate')?>: </strong><span><?=$var['birthdate']?></span>
								<span itemprop="birthDate" style="visibility:hidden"><?=$var['birthdate_iso']?></span>
								<br>								
							<?endif?>
							<?if($var['deathdate']):?>						
								<strong><?=sys::translate('main::deathdate')?>: </strong><span><?=$var['deathdate']?></span>
								<span itemprop="birthDate" style="visibility:hidden"><?=$var['deathdate_iso']?></span>
								<br>								
							<?endif?>

							<?foreach ($var['links'] as $link):?>
								<strong><?=sys::translate('main::links')?>:</strong> <a href="<?=$link['url']?>" rel='nofollow' target='_blank'><?=$link['title']?></a><br>
							<?endforeach;?>

							<?if($var['biography']):?>
								<div id='lineBlank' style='height: 20px;'><img src='/images/blank.gif' height='1'></div>			
								<div id='searchTitle'><?=sys::translate('main::biography')?></div>
								<div id='lineHrWide'>&nbsp;</div>
								<div id='filmText'>
									<?=$var['biography']?>
								</div>	
							<?endif;?>
					</div>		</div>
					
<? /* старый код вывода наград					
							<?if($var['awards']):?>
								<div id='lineBlank' style='height: 20px;'><img src='/images/blank.gif' height='1'></div>			
								<div id='searchTitle'><?=sys::translate('main::awards')?></div>
								<div id='lineHrWide'>&nbsp;</div>
								<div>
										<?
											$film = "";
										?>
											<ul id='awardsPersonList'>										
											
												<li>
													<div id='awardsListNumber'><?=sys::translate('main::year')?></div>
													<div id='awardsPersonListTitle'>
														<?=sys::translate('main::award')." / ".sys::translate('main::nomination')?>
													</div>
													<div id='awardsListPrise' style="color:#333333;padding-right:30px;"><?=sys::translate('main::status')?></div>
												</li>

											<?foreach ($var['awards'] as $obj):?>
													<?if($film != $obj["film"]):?>
													<li>
														<div>
														 <strong class='red'><?=sys::translate('main::film')?> <a href=<?=$obj["url"]?>><?=($film = $obj["film"])?></a></strong>
														</div>
													</li>
													<?endif;?>
											
													<li>
														<div id='awardsListNumber'><?=$obj['year']?></div>
														<div id='awardsPersonListTitle' class='awardsWithPersons'>
															<span style="color: #666;"><?=$obj['nomination']?></span><br><?=$obj['title']?>
																	
														</div>
														<div id='awardsListPrise'><img src='/images/awards_<?=($obj['result']=='nom')?"nominant":"winner"?>.png' style="margin-top: 10px;"></div>
													</li>
											<?endforeach;?>										
											</ul>										
								</div>	
							<?endif;?>							
							


						<div id='lineBlank' style='height: 25px;'><img src='/images/blank.gif' height='1'></div>

<?//if(sys::isDebugIP() || isset($_GET["test"])):?>	
<?//sys::printR($var['awards']);?>					
							<?if($var['awards']):?>
								<?
								reset($var['awards']);
								?>
								<div id='lineBlank' style='height: 20px;'><img src='/images/blank.gif' height='1'></div>			
								<div id='searchTitle'><?=sys::translate('main::awards')?></div>
								<div id='lineHrWide'>&nbsp;</div>
								<div>
										<?
											$year = "";
											$nomination = "";
										?>
											<ul id='awardsPersonList'>																					
											<?foreach ($var['awards'] as $obj):?>
												
												<?if($obj["year"] != $year):?>
												<? 
													$year = $obj["year"];
													$nomination = "";
												?> 
												<li>
													<div id='awardsListTitle'>
														<h2><?=$obj["year"]?></h2>
													</div>	
												</li>												
												<?endif;?>			
												
												<?if($obj["nomination"] != $nomination):?>
												<? 
													$nomination = $obj["nomination"];
												?> 
												<li>
													<div id='awardsListTitle'>
													<h3><?=$obj["nomination"]?></h3>
													</div>
												</li>												
												<?endif;?>			
												
												<li>
													<div>
														<?=sys::translate('main::film')?> <a href=<?=$obj["url"]?>><?=($film = $obj["film"])?></a>
														-
														<?=$obj["title"]?>
														
													</div>
												</li>												
																			
											<?endforeach;?>										
											</ul>										
								</div>	
							<?endif;?>								
						<div id='lineBlank' style='height: 25px;'><img src='/images/blank.gif' height='1'></div>
<?//endif;?>
*/ ?>					
					<?=main_persons::showPersonRatingForm($_GET['person_id']);?>
					<!--span id='imdb'>Рейтинг IMDB: </span>
					<span id='imdbRating'>234,2</span-->
<?/*
								
								<?if(($var['edit_url'])):?>
									<div id='lineBlank' style='height: 12px;'><img src='/images/blank.gif' height='1'></div>
									<a href onclick='$("#editor").toggle("fast"); return false;'><?=sys::translate("main::edit")?></a>							
								<?endif;?>

<!-- редактирование биографии -->
<?if(($var['biography']) && ($var['edit_url'])):?>

<div id='lineBlank' style='height: 12px;'><img src='/images/blank.gif' height='1'></div>

<script type="text/javascript" src="/jwysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="/jwysiwyg/controls/default.js"></script>
	<link rel="stylesheet" href="/jwysiwyg/jquery.wysiwyg.css" type="text/css" media="screen" charset="utf-8" />


	<style type="text/css" media="screen">
		#wysiwyg{ width:660px; height:300px; }
	</style>

<div id='editor'>

<form method='POST'>
<textarea id="wysiwyg" style='width:660px; height:300px;' name='editedtext'><?=$var['biography']?></textarea>
<input type='submit' value='Сохранить'>
</form>

<script type="text/javascript">
$(function() {
    $('#wysiwyg').wysiwyg();
});
</script>

</div>

<script type="text/javascript">
$(function() {
    $('#editor').toggle();
});
</script>

					</div>
<?endif; // of biography?>
*/?>
		<div id='lineBlank' style='height: 36px;'><img src='/images/blank.gif' height='1'></div>
		<?//=main_persons::personFilms($_GET['person_id'])// фильмография?>
		<?=banners::showBanners('v3_secondbanner')?>
		
		<?if($var['previews']):?>
		<div class='videosList'>
		<div id='videosListTitle'><?=sys::translate('main::posters')?></div>
		<div id='lineHrWide'>&nbsp;</div>

				<?foreach ($var['previews'] as $preview):?>

					<?if(($i+1)%4==0) $class='no-margin ';else $class=false;?>
						<div class="<?=$class?>poster_preview" >
						<a title="<?=sys::translate('main::posters_from_film')?> <?=$var['film']?>" href='<?=$preview['url']?>'>
							<img src="<?=$preview['image']?>" 
								 alt="<?=sys::translate('main::posters')?>: <?=$var['fio']?> <?=sys::translate('main::in_film')?>: «<?=$preview['film']?>»" 
								 title="<?=sys::translate('main::poster')?>: <?=$var['fio']?> <?=sys::translate('main::in_film')?>: «<?=$preview['film']?>»">
						</a>
							<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
						</div>

					<?if(($i+1)%4==0):?>

					<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

					<?endif;?>
                   <?$i++?>

				<?endforeach;?>
		</div>

		<?endif;?>
		
		
		
		
		<?=main_discuss::showDiscussMain($_GET['person_id'],'person')?>
		<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

           <script>
           function show()
           {
           		document.getElementById('myriad').style.visibility = 'visible';
           }
			setTimeout(show,2000);
           </script>
</div>


