<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?//$_var['active_menu']=21?>
<?//$_var['active_child_menu']=44?>
<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

<div class='myriad' id='myriad'><?=$_var['main::title']?></div>

<div id='lineHrWide'>&nbsp;</div>

<div id='news_page'>

<?foreach ($var['objects'] as $obj):?>
	<div id='NewsItem'>
		<a href='<?=$obj['url']?>' class='mainNewsPhoto'>
			<img style="display:none;" src="<?=$obj['image']?>" alt="<?=$obj['title']?>" title="<?=$obj['title']?>">
		</a>
		
		<div id='NewsItemTitle'>
			<a href='<?=$obj['url']?>' class='mainNewsLink' title='<?=$obj['title']?>'><?=$obj['title']?></a><br>
			<span class="NewsItemDate"><nobr><?=$obj['game_type']?></nobr></span>
		</div>

		<p class='NewsItemText'>
			<?=$obj['intro']?>
		</p>
	</div>


	<div id='lineHr'>&nbsp;</div>

<?endforeach;?>
</div>

<div style='clear: both; margin-bottom: 20px;'>&nbsp;</div>
<?=banners::showBanners('v3_secondbanner')?>
<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>


<script>
$(function(){
 $("a.mainNewsPhoto img").css({"width":"180px","margin-left":"25px"}).show(500);
});
</script>