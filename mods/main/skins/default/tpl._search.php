<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=content::getEntryNodeId(19)?>
<?sys::setMeta($var['meta']);?>
<?//main::showGemius('0sibwqN2iMUk43dsvzmHo8SsTO5_HdByi1lhbtysaND.77');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

				<div class='myriad' id='myriad'><?=$_var['main::title']?></div>

					<div id='lineHrWide'>&nbsp;</div>

					<div id='news_page'>


	<?if($var['films']):?>

					<div id='searchTitle'><?=$var['films_title']?></div>
					<div id='lineHrWide'>&nbsp;</div>

			<?foreach ($var['films'] as $obj):?>


						<div id='searchItemFilms'>
							<a href='<?=$obj['url']?>' class='searchPhoto'><img  src="<?=$obj['poster']?>" title="<?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot;" alt="<?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot;"></a>

							<div id='searchItemMainText'>
								<div id='searchItemTitle'>
									<a href='<?=$obj['url']?>' class='searchItemLink'><?=$obj['title']?></a><br>
									<span class='searchItemOriginal'><nobr><?=$obj['round']?></nobr></span>
								</div>

								<p class='searchItemText'>
										<?=$obj['rate']?>
										<?=$obj['second_row']?>
								</p>

								<p class='searchItemText'>
										<?=$obj['third_row']?>
								</p>
							</div>

						<div id='lineBlank' style='height: 1px;'><img src='/mods/main/skins/default/images/blank.gif' height='1'></div>
						</div>

						<div id='lineHr'>&nbsp;</div>

			<?endforeach;?>


	<?endif?>

	<?if($var['fnear']):?>

					<div id='searchTitle'><?=sys::translate('main::nearVariants')?> (<?=$var['films_title']?>)</div>
					<div id='lineHrWide'>&nbsp;</div>

			<?foreach ($var['fnear'] as $obj):?>


						<div id='searchItemFilms'>
							<a href='<?=$obj['url']?>' class='searchPhoto'><img  src="<?=$obj['poster']?>" title="<?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot;" alt="<?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot;"></a>

							<div id='searchItemTitle'>
								<a href='<?=$obj['url']?>' class='searchItemLink'><?=$obj['title']?></a><br>
								<span class='searchItemOriginal'><nobr><?=$obj['round']?></nobr></span>
							</div>

							<p class='searchItemText'>
									<?=$obj['rate']?>
									<?=$obj['second_row']?>
							</p>

							<p class='searchItemText'>
									<?=$obj['third_row']?>
							</p>
						</div>

						<div id='lineHr'>&nbsp;</div>

			<?endforeach;?>


	<?endif?>


	<?if($var['films']):?>
			<div><a href='<?=$var["films_link"]?>' class='allResults'><?=$var['all_results']?></a></div>
            <div style='clear: both; margin-bottom: 20px;'>&nbsp;</div>
					<?=banners::showBanners('v3_secondbanner')?>
	<?endif?>






	<?if($var['persons']):?>

			<div id='searchTitle'><?=$var['persons_title']?></div>
			<div id='lineHrWide'>&nbsp;</div>

			<?foreach ($var['persons'] as $obj):?>

						<div id='searchItemFilms'>
							<a href='<?=$obj['url']?>' class='searchPhoto'><img src="<?=$obj['poster']?>" title="<?=$obj['title']?>" alt="<?=$obj['title']?>"></a>

							<div id='searchItemTitle'>
								<a href='<?=$obj['url']?>' class='searchItemLink'><?=$obj['title']?></a><br>
								<span class='searchItemOriginal'><nobr><?=$obj['round']?></nobr></span>
							</div>

							<p class='searchItemText'>
								<?=$obj['rate']?>
								<?=$obj['biography']?>
							</p>

						</div>

						<div id='lineHr'>&nbsp;</div>



			<?endforeach;?>

	<?endif?>

	<?if($var['pnear']):?>

			<div id='searchTitle'><?=sys::translate('main::nearVariants')?> (<?=$var['persons_title']?>)</div>
			<div id='lineHrWide'>&nbsp;</div>

			<?foreach ($var['pnear'] as $obj):?>

						<div id='searchItemFilms'>
							<a href='<?=$obj['url']?>' class='searchPhoto'><img src="<?=$obj['poster']?>" title="<?=$obj['title']?>" alt="<?=$obj['title']?>"></a>

							<div id='searchItemTitle'>
								<a href='<?=$obj['url']?>' class='searchItemLink'><?=$obj['title']?></a><br>
								<span class='searchItemOriginal'><nobr><?=$obj['round']?></nobr></span>
							</div>

							<p class='searchItemText'>
								<?=$obj['rate']?>
								<?=$obj['biography']?>
							</p>

						</div>

						<div id='lineHr'>&nbsp;</div>

			<?endforeach;?>

	<?endif?>

	<?if($var['persons']):?>
			<div><a href='<?=$var["person_link"]?>' class='allResults'><?=$var['all_results']?></a></div>
            <div style='clear: both; margin-bottom: 20px;'>&nbsp;</div>
	<?endif?>



	<?if($var['cinemas']):?>

					<div id='searchTitle'><?=$var['cinemas_title']?></div>
					<div id='lineHrWide'>&nbsp;</div>

			<?foreach ($var['cinemas'] as $obj):?>

						<div id='searchItemFilms'>
							<a href='<?=$obj['url']?>' class='searchPhoto'><img src='/mods/main/skins/default/images/kt_image_blank.jpg'></a>

							<div id='searchItemTitle'>
								<a href='<?=$obj['url']?>' class='searchItemLink'><?=$obj['title']?></a><br>
								<span class='searchItemOriginal'><nobr><?=$obj['round']?></nobr></span>
							</div>

							<p class='searchItemText'>
								<?=$obj['adress']?>
								<?=$obj['phone']?>
								<?=$obj['website']?>
							</p>

						</div>

						<div id='lineHr'>&nbsp;</div>

			<?endforeach;?>

			<div><a href='<?=$var["cinema_link"]?>' class='allResults'><?=$var['all_results']?></a></div>
            <div style='clear: both; margin-bottom: 20px;'>&nbsp;</div>

	<?endif?>




	<?if($var['news']):?>
					<div id='searchTitle'><?=$var['news_title']?></div>
					<div id='lineHrWide'>&nbsp;</div>


			<?foreach ($var['news'] as $obj):?>


						<div id='NewsItemSearch'>
							<a href='<?=$obj['url']?>' class='mainNewsPhoto'><img src='<?=$obj['image']?>'></a>

							<div id='NewsItemSearchTitle'>
								<a href='<?=$obj['url']?>' class='mainNewsLink'><?=$obj['title']?></a>
								<span class='NewsItemDate'><nobr><?=$obj['date']?></nobr></span>
							</div>

							<p class='NewsItemText'>
								<?=$obj['intro']?>
							</p>
						</div>
						<div id='lineHr'>&nbsp;</div>

			<?endforeach;?>

			<div><a href='<?=$var["news_link"]?>' class='allResults'><?=$var['all_results']?></a></div>
            <div style='clear: both; margin-bottom: 20px;'>&nbsp;</div>

	<?endif?>

	<?if(!$var['films'] && !$var['news'] && !$var['cinemas'] && !$var['persons'] && !$var['fnear'] && !$var['pnear']):?>

	<div id='nothingFound'><?=sys::translate('main::nothing_found')?></div>

	<?endif?>






					</div>



						<div id='lineBlank' style='height: 10px;'><img src='/mods/main/skins/default/images/blank.gif' height='1'></div>




