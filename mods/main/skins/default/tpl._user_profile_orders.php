<?sys::useLib('content');?>
<?$var['meta']['title']=$var['meta']['title'].' ('.$_user['main_nick_name'].')'?>
<?$var['meta']['meta_title']=$var['meta']['meta_title'].' ('.$_user['main_nick_name'].')'?>
<?$_var['main::title']=$var['meta']['title']?>
<?=sys::setMeta($var['meta']);?>
<?sys::useJs('sys::form');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>
<?php $types = array('buy'=>'Покупка','book'=>'Бронь');?>
<link rel="stylesheet" href="/mods/main/skins/default/js/colorbox/colorbox.css" />
<script type="text/javascript" src="/mods/main/skins/default/js/eventmap.js"></script>
<script type="text/javascript" src="/mods/main/skins/default/js/jquery.colorbox.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".inline").colorbox({inline:true});	
	});
</script>

<div class='myriad' id='myriad'><?=$_var['main::title']?></div>





					<div id='lineHrWide'>&nbsp;</div>

		<?=main_users::showProfileTabs('my')?>

						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
<br /><br />

<div>
	
</div>

<div id="main_user_profile_films" class="user-profile-form">


    		<table class="xTable x week">
    			<tr class="xLinks2">
    				<th align="center">Фильм</th>
    				<th align="center">Кинотеатр</th>
    				<th align="center">Зал</th>
    				<th align="center">Сеанс</th>
    				<th align="center">Информация</th>
    				<th align="center">Цена, грн</th>
    				<th align="center">Дата</th>
					<?php if(empty($_GET['type'])):?>
					    <th align="center">Тип</th>
					<?php endif;?>
					<th align="center"></th>
    			</tr>
    			<?foreach ($var['objects'] as $obj): ?>
				<tr class="film">
                    <td><a href="<?=$obj['film_url']?>" target="_blank"><?=$obj['film_name']?></a></td>
                    <td><a href="<?=$obj['cinema_url']?>" target="_blank"><?=$obj['cinema_name']?></a></td>
					<td align="center"><?=$obj['hall_name']?></td>
					<td align="center"><?=$obj['time_show']?></td>
					<td align="center"><?=$obj['info']?></td>
					<td align="center"><span style="text-align: center !important; margin-left: 40%;"><?=$obj['sum']?></span></td>
					<td align="center">
					<?php
						if($obj['type']=='book')
							echo $obj['data_order'];
						else
							echo $obj['data_buy'];
					
					?>
						
					</td>
					<?php if(empty($_GET['type'])):?>
						
					    <td align="center"><?php echo $types[$obj['type']];?></td>
					<?php endif;?>
					<td>
						<div>
					<?php
						$event_time = $obj['event_time'];
						$limit_cancel_time = time() - 1800;
						if(!$event_time)
							$enable_cancel = false;
						elseif($event_time < $limit_cancel_time)
							$enable_cancel = false;
						else
							$enable_cancel = true;
					
					?>
					<?php if($enable_cancel):?>
						<span>
						<?php if($obj['type'] == 'book'):?>
							<a href="/basket.php?method=cancel-order&orderId=<?php echo $obj['megakino_id'];?>&site_id=<?php echo $obj['site_id'];?>" onclick="cancel_order_from_profile(this);return false;" title="Отменить">
								<img src="/mods/main/skins/default/images/cancel2.png" width="12" height="12" align="Отменить"/>
							</a>
						<?php else:?>
						<?php if($_user['login'] == 'mastercard' || $_user['login'] == 'ipay' || $_user['login'] == 'vitaliy43'):?>
							<a href="/payment.php?method=PaymentCancel&megakino_id=<?php echo $obj['megakino_id'];?>&pmt_id=<?=$obj['pmt_id']?>&site_id=<?=$obj['site_id']?>" onclick="cancel_payment_from_profile(this);return false;" title="Отменить">
								<img src="/mods/main/skins/default/images/cancel2.png" width="12" height="12" align="Отменить"/>
							</a>
							<?php endif;?>
						<?php endif;?>
						</span>
					<?php endif;?>
					<?php
					
						if($obj['type'] == 'buy'){
							if($obj['tickets_count']==1){
								echo '<span><a href="/payment.php?method=Print&megakino_id='.$obj['megakino_id'].'&lang_id='.intval($_cfg['sys::lang_id']).'&pdf=1&page=1" title="Распечатать билет"><img src="/mods/main/skins/default/images/print.png" width="12" height="12" /></a></span>';
							}	
							else{
								echo '<span><a href="#links-'.$obj['megakino_id'].'" title="Распечатать билеты" class="inline"><img src="/mods/main/skins/default/images/print.png" width="12" height="12" /></a></span>';
								echo '<div style="display: none;">';
								$html_tickets = '';
								$html_tickets .= '<div id="links-'.$obj['megakino_id'].'" style="padding:20px; background:#fff;">';
									$counter=1;
									foreach($obj['tickets'] as $ticket){
										$html_tickets .= '<div>';
										$html_tickets .= '<a href=\'/payment.php?method=Print&megakino_id='.$obj['megakino_id'].'&lang_id='.intval($_cfg['sys::lang_id']).'&pdf=1&page='.$counter.'\' title=\'Распечатать билет\'>Ряд: '.$ticket['rowNumber'].', Место: '.$ticket['placeNumber'].'</a>';
										$html_tickets .= '</div>';
										$counter++;
									}
								$html_tickets .= '</div>';
								echo $html_tickets;

								echo '</div>';
							}
								
								
								
						}
						
					?>
						</div>
					</td>
				</tr>
    			<?endforeach;?>
    		</table>
			<?=sys::parseTpl('main::pages',$var['pages'])?>


</div>

