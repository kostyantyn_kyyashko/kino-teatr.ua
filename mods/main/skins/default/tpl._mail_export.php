<?sys::setTpl('main::__xml')?>
<billboard>
	<genres>
		<?foreach ($var['genres'] as $obj):?>
			<genre id="<?=$obj['id']?>"><?=$obj['title']?></genre>
		<?endforeach;?>
	</genres>
	<professions>
		<?foreach ($var['professions'] as $obj):?>
			<profession id="<?=$obj['id']?>"><?=$obj['title']?></profession>
		<?endforeach;?>
	</professions>
	<countries>
		<?foreach ($var['countries'] as $obj):?>
			<country id="<?=$obj['id']?>"><?=$obj['title']?></country>
		<?endforeach;?>
	</countries>
	<cities>
		<?foreach ($var['cities'] as $obj):?>
			<city id="<?=$obj['id']?>" country_id="<?=$obj['country_id']?>"><?=$obj['title']?></city>
		<?endforeach;?>
	</cities>    */ ?>
	<cinemas>
		<?foreach ($var['cinemas'] as $cinema):?>
			<cinema id="<?=$cinema['id']?>" city_id="<?=$cinema['city_id']?>">
				<city_id><?=$cinema['city_id']?></city_id>
				<type>Кинотеатр</type>
				<title><?=$cinema['title']?></title>
				<id><?=$cinema['id']?></id>
				<address><?=$cinema['address']?></address>
				<phone><?=$cinema['phone']?></phone>
				<?if($cinema['site']):?>
					<site><?=$cinema['site']?></site>
				<?endif;?>
				<text><![CDATA[<?=$cinema['text']?>]]></text>
					<?if($cinema['photo']):?>
						<photo src="<?=$cinema['photo']?>"/>
					<?endif;?>

				<halls>
					<?foreach ($cinema['halls'] as $hall):?>
						<hall id="<?=$hall['id']?>">
							<title><?=$hall['title']?></title>
							<id><?=$hall['id']?></id>
						</hall>
					<?endforeach;?>
				</halls>
			</cinema>
		<?endforeach;?>
	</cinemas>

	<films>
		<?foreach ($var['films'] as $film):?>
			<film id="<?=$film['id']?>">
				<id><?=$film['id']?></id>
				<type>Фильм</type>
				<genres><?=implode(',',$film['genres'])?></genres>
				<title><?=$film['title']?></title>
				<title_orig><?=$film['title_orig']?></title_orig>
				<intro><![CDATA[<?=$film['intro']?>]]></intro>
				<text><![CDATA[<?=$film['text']?>]]></text>
				<rating votes="<?=$film['votes']?>"><?=$film['rating']?></rating>
				<poster src="<?=$film['poster']?>"/>
				<countries><?=implode(',',$film['countries'])?></countries>
				<ukraine_premiere><?=$film['ukraine_premiere']?></ukraine_premiere>
				<world_premiere><?=$film['world_premiere']?></world_premiere>
				<kids><?=$film['children']?></kids>
				<persons>
					<?foreach ($film['persons'] as $person):?>
						<person id="<?=$person['person_id']?>" profession_id="<?=$person['profession_id']?>"  role="<?=$person['role']?>"/>
					<?endforeach;?>
				</persons>
			</film>
		<?endforeach;?>
	</films>

	<persons>
		<?foreach ($var['persons'] as $person):?>
			<person id="<?=$person['id']?>">
				<id><?=$person['id']?></id>
				<name><?=$person['firstname']?> <?=$person['lastname']?></name>
				<name_orig><?=$person['firstname_orig']?> <?=$person['lastname_orig']?></name_orig>
				<birthdate><?=$person['birthdate']?></birthdate>
				<photo src="<?=$person['photo']?>"/>
				<biography><![CDATA[<?=$person['biography']?>]]></biography>
			</person>
		<?endforeach;?>
	</persons>

	<shows>
		<?foreach ($var['shows'] as $show):?>
			<show id="<?=$show['id']?>" film_id="<?=$show['film_id']?>" cinema_id="<?=$show['cinema_id']?>" hall_id="<?=$show['hall_id']?>">
				<film_id><?=$show['film_id']?></film_id>
				<cinema_id><?=$show['cinema_id']?></cinema_id>
				<hall_id><?=$show['hall_id']?></hall_id>
				<begin><?=$show['begin']?></begin>
				<end><?=$show['end']?></end>
				<times>
					<?foreach ($show['times'] as $time):?>
						<time time="<?=$time['time']?>">
							<prices><?=$time['prices']?></prices>
							<note><![CDATA[<?=$time['note']?>]]></note>
						</time>
					<?endforeach;?>
				</times>
			</show>
		<?endforeach;?>
	</shows>

</billboard>
