<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?//$_var['active_menu']=21?>
<?//$_var['active_child_menu']=44?>
<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>
<?unset($_var['banner_right'])?>
 
<div class='myriad' id='myriad'><?=$_var['main::title']?></div>

<ul id="blockHeaderMenu">
	<li class=active>
		<a href="games.phtml"><?=sys::translate('main::games_all')?></a>
	</li>
</ul>

<div id='lineHrWide'>&nbsp;</div>
<div id='game_content'></div>
<div style='clear: both; margin-bottom: 20px;'>&nbsp;</div>
<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>

<script>
$(function(){
 $("#rightContent").remove();
 $("#leftContent").css({"width":"980px"});
 $("#game_content").html('<iframe width="100%" height="600" frameborder="0" scrolling="no" src="<?=$var['src']?>"></iframe>');
});
</script>