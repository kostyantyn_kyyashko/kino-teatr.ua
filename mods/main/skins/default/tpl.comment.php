<?if(!isset($var['value'])) $var['value']=false;?>
<?if(!isset($var['name'])) $var['name']='text';?>
<?sys::useLib('main::captcha');?>
<?php
	if($_SERVER['HTTP_HOST'] == 'kino-teatr.ua')
		$captcha_link = 'https://kino-teatr.ua/captcha.php';
	else
		$captcha_link = 'http://test.kino-teatr.ua/captcha.php';

?>
<?php	
	$captcha_public_key = main_captcha::$public_key;
?>

						<div id='userTextHeader'>
							<div id='userPicture'><img src='<?=$var['myavatar']?>'></div>
							<div id='userInfo'>
								<?if($_user['id']==2):?>
									<b><?=sys::translate('main::name')?></b> <input value="<?=sys::translate('main::guest')?>" maxlength="55" type="text" name="name">
									<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
								<?else:?>
									<a href='<?=$var['myurl']?>'><?=$var['mylogin']?></a>
								<?endif?>
								<?=$var['nowdate']?>
							</div>
						</div>

<script>
function insertTag(tag, name)
{
	$('#'+name).val($('#'+name).val()+'['+tag+'][/'+tag+']');
}
</script>

						<div id='lineBlank' style='height: 11px;'><img src='/images/blank.gif' height='1'></div>
						<span id='yourComment'><?=sys::translate('main::yourComment')?></span>
						<input class='comBtn' type='image' src='/mods/main/skins/default/images/bold.jpg' onclick="insertTag('b','leaveComment'); return false;">
						<input class='comBtn' type='image' src='/mods/main/skins/default/images/italic.jpg' onclick="insertTag('i','leaveComment'); return false;">
						<input class='comBtn' type='image' src='/mods/main/skins/default/images/underlined.jpg' onclick="insertTag('u','leaveComment'); return false;">
						<textarea id='leaveComment' name="<?=$var['name']?>"><?=$var['value']?></textarea>
						<div id='lineBlank' style='height: 13px;'><img src='/images/blank.gif' height='1'></div>

						<?if($_user['id']==2):?>
							<input type="hidden" name="question_id" value="<?php echo $var['question']['id'];?>" />
							<table class="x xTable">
								
								
									<?php if($var['type']):?>
										<tr>
									<td colspan="2" text-align='left' ><strong><?php echo $var['question']['question'];?>?></strong></td>
								</tr>
								<tr>
									<td colspan="2" text-align='left' >&nbsp;</td>
								</tr>
										<?foreach ($var['question']['answers'] as $answer):?>
										<tr>
											<td width="1">
												<input type="radio" class="xCheckbox" name="answer" value="<?php echo $answer;?>?>">
											</td>
											<td width="100%" style='text-align: left;'>
												<?php echo $answer;?>
											</td>
										</tr>
								<?endforeach;?>
										
									<?php else:?>
										<div class="g-recaptcha" data-sitekey="<?php echo $captcha_public_key;?>" ></div>
									<?php endif;?>
							</table>
							
							<br />
						<?endif?>


