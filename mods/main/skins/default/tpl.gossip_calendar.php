<div class="main_gossipCalendar">
	<table class="x xTable">
		<?foreach ($var['years'] as $year):?>
			<tr>
				<th class="x" colspan="3"><strong><?=$year['title']?></strong></th>
			</tr>
			<?$i=0;?>
			<?foreach ($year['months'] as $month):?>
				<?if($i%3==0):?>
					<tr>
				<?endif;?>
					<?if($month['current'])$class='xCurrent'; else $class=false;?>
						<td class="x <?=$class?>">
							<?if($month['url'] && !$month['current']):?>
								<a title="<?=$month['alt']?>" href="<?=$month['url']?>"><?=$month['title']?>.</a>
							<?else:?>
								<?=$month['title']?>.
							<?endif;?>
						</td>
				<?if(($i+1)%3==0):?>
					</tr>
				<?endif;?>
				<?$i++?>
			<?endforeach;?>
		<?endforeach;?>
	</table>
</div>

