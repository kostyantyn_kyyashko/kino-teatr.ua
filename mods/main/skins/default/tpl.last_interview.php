<? sys::useLib('main::spec_themes'); ?>

				<div id='interviewBlock'>
						<div id='interviewBlockTitle'>
							<?=sys::translate('main::interview')?>
						</div>


		<?foreach ($var['objects'] as $obj):?>
		<?$obj['spec_theme_url']=main_spec_themes::getArticleUrl($obj['spec_theme']);?>

		<?if($obj['nr']==1):?>
						<div id='interviewMainItem'>
	                           	<a href='<?=$obj['url']?>' class='interviewMainPhoto'><img src='<?=$obj["image"]?>'></a>
	                           	<div id='interviewMainText'>
			<?if($obj["exclusive"]):?><sup class="xExtra"><?=sys::translate('main::exclusive')?></sup><?endif;?>
			<?if($obj["spec_theme"]):?><sup class="supertopic">
				<a href='<?=$obj['spec_theme_url']?>'><?=sys::translate('main::spec_theme')?></a>
			</sup><?endif;?>
	                           		<a href='<?=$obj['url']?>' title='<?=$obj['title']?>'><?=$obj['title']?></a>
	                           		<p><?=$obj['intro']?></p>
									<span class='interviewMainDate'>
										&mdash; <?=$obj['date']?>
									</span>
	                           	</div>
								<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
						</div>
		<?else:?>

						<div id='interviewBlockItem'>
	                           	<a href='<?=$obj['url']?>' class='interviewBlockPhoto'><img src='<?=$obj["image"]?>'></a>
	                           	<div id='interviewBlockText'>
			<?if($obj["exclusive"]):?><sup class="xExtra"><?=sys::translate('main::exclusive')?></sup><?endif;?>
			<?if($obj["spec_theme"]):?><sup class="supertopic">
				<a href='<?=$obj['spec_theme_url']?>'><?=sys::translate('main::spec_theme')?></a>
			</sup><?endif;?>
	                           		<a href='<?=$obj['url']?>' title='<?=$obj['title']?>'><?=$obj['title']?></a>
	                           	</div>
								<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
						</div>
		<?endif;?>

	<?endforeach;?>



						<a href='<?=sys::rewriteUrl('?mod=main&act=interview')?>' id='allInterview'><?=sys::translate('main::allInterview')?></a>

								<div id='lineBlank' style='height: 15px;'><img src='/images/blank.gif'></div>

					</div>