<?sys::useLib('content');?>
<?// $_var['right']=main_films::showFilmMedia($_GET['film_id'], $var['film'])?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=6?>
<?$_var['active_child_menu']=7?>
<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

<div class='myriadFilm' id='myriad'><?=sys::translate('main::awards_film').' '.$var['title']?></div>

<div id='lineHrWide'>&nbsp;</div>

<?=main_films::showFilmTabs($_GET['film_id'],$var['film'],'awards');?>

<div id='lineBlank' style='height: 25px;'><img src='/images/blank.gif' height='1'></div>

<div id='news_page'>

<?if(count($var['objects'])>0):?>
<?
	$nomination = "";
	$year = 0;
?>
	<ul id='awardsList'>

	<?foreach ($var['objects'] as $obj):?>
			<?if( ($nomination != $obj["nomination"]) || ($year != $obj['year']) ):?>
			<li>
				<div id='awardsListTitle'>
				 <h2><?=($year = $obj['year'])?> <?=($nomination = $obj["nomination"])?></h2>
				</div>
			</li>
			<?endif;?>
			<li>
				<!-- <div id='awardsListNumber'><?=$obj['year']?></div> -->
				<div id='awardsListTitle' <?if($obj['persons']):?>class='awardsWithPersons'<?endif;?>>
					<?=$obj['title']?>
					<?if ($obj['persons']):?>
						<br>
						<?foreach ($obj['persons'] as $person):?>
							<a href="<?=$person['url']?>"><?=$person['fio']?></a><?if($i<count($obj['persons'])):?> <?endif?>
						<?endforeach;?>
					<?endif?>
				</div>
				<div id='awardsListPrise'><img src='/images/awards_<?=($obj['result']=='nom')?"nominant":"winner"?>.png' style="margin-top: 10px;"></div>
			</li>
	<?endforeach;?>

	</ul>

<?else:?>

	<div id='nothingFound'><?=sys::translate('main::noAwards')?></div>

<?endif;?>

</div>
