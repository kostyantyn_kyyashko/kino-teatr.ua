<?sys::setTpl('main::__rssfull');?>
<channel>
<rss2lj:owner xmlns:rss2lj="http://rss2lj.net/NS">kino_teatr_gossip</rss2lj:owner>
<title><?=sys::translate('main::title_rssfull')?></title>
<atom:link href="https://kino-teatr.ua/ru/main/gossip_rssfull.phtml" rel="self" type="application/rss+xml"/>
<link><?=_ROOT_URL?></link>
<description><?=sys::parseModTpl('main::gossip_rss_description')?></description>
<language><?=$_cfg['sys::lang']?></language>
<pubDate><?=date('r')?></pubDate>

<generator>Grifix</generator>
<lastBuildDate><?=date('r')?></lastBuildDate>
<copyright>Copyright 2006, Олег Бойко</copyright>
<category><?=sys::translate('main::portal_gossip')?></category>
<managingEditor>admin@kino-teatr.kiev.ua (Олег Бойко)</managingEditor>
<webMaster>admin@kino-teatr.kiev.ua (Олег Бойко)</webMaster>
<docs>http://beshenov.ru/rss2.html</docs>
<ttl>60</ttl>

<?foreach ($var['objects'] as $obj):?>
<item>
	<title><?=$obj['title']?></title>
	<link><?=$obj['url']?></link>
	<description><?=$obj['intro']?></description>
	<content:encoded><?=$obj['intro']?></content:encoded>
	<pubDate><?=$obj['date']?></pubDate>
	<?if($obj['image']):?>
		<enclosure url="<?=$obj['image_src']?>" type="<?=$obj['image_mime']?>" length="<?=$obj['image_size']?>"/>
	<?endif?>
	<guid isPermaLink="true"><?=$obj['url']?></guid>
	<categoty><?=$obj['category']?></categoty>
</item>
<?endforeach;?>
</channel>

