<?
sys::useLib('counters');
sys::useLib('main::menu');
sys::useLib('main::captcha');
sys::useLib('banners');
sys::useLib('content');
sys::useLib('sys::form');
sys::jsInclude('sys::sys');
?>

<?$_var['active_menu']=1?>

<!DOCTYPE html>
<html lang='ru' xml:lang='ru' xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<title><?=htmlspecialchars($_meta_title)?></title>
<meta name='loginza-verification' content='e1186b66f667592cb696e94ddda1501d' />
<meta name='loginza-verification' content='f7576a22af517cbadff58ec17cd3380f' />
<meta name='yandex-verification' content='6d58b9dcfe59710b' />
<meta name="dumedia-verify" content="16769c82fdbc">
<!-- 4a8df159183797b475108d0470c6e30b -->
<meta name="openstat-verification" content="29182f3f3e6ba59cfd5449d9d7bb9623784ac328" />
	<meta name="keywords" content="<?=htmlspecialchars($_meta_keywords)?>">
	<meta name="DESCRIPTION" content="<?=htmlspecialchars($_meta_description)?>">
	<?=$_meta_other?>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="<?=$skin_url?>blocks.css" type="text/css" />
	<link rel="stylesheet" href="<?=$skin_url?>new_brand3.css" type="text/css" />
	<link rel="shortcut icon" type="image/ico" href="//kino-teatr.ua/favicon.ico" />
<link rel="icon" type="image/ico" href="//kino-teatr.ua/favicon.ico" />
<link rel="apple-touch-icon" href="//kino-teatr.ua/images/icons/60.png">
<link rel="apple-touch-icon" sizes="57x57" href='//kino-teatr.ua/images/icons/57.png'>
<link rel="apple-touch-icon" sizes="72x72" href='//kino-teatr.ua/images/icons/72.png'>
<link rel="apple-touch-icon" sizes="76x76" href='//kino-teatr.ua/images/icons/76.png'>
<link rel="apple-touch-icon" sizes="114x114" href='//kino-teatr.ua/images/icons/114.png'>
<link rel="apple-touch-icon" sizes="120x120" href='//kino-teatr.ua/images/icons/120.png'>
<link rel="apple-touch-icon" sizes="144x144" href='//kino-teatr.ua/images/icons/144.png'>
<link rel="apple-touch-icon" sizes="152x152" href='//kino-teatr.ua/images/icons/152.png'>
<link rel="shortcut icon" sizes="196x196" href='//kino-teatr.ua/images/icons/196.png'>
<link rel="shortcut icon" sizes="128x128"  href='//kino-teatr.ua/images/icons/128.png'>
<link rel="shortcut icon" sizes="96x96"  href='//kino-teatr.ua/images/icons/96.png'>
<link rel="shortcut icon" sizes="72x72"  href='//kino-teatr.ua/images/icons/72.png'>
<link rel="shortcut icon" sizes="48x48"  href='//kino-teatr.ua/images/icons/48.png'>
<link rel="alternate" <?if($_cfg['sys::lang']=='ru'):?>href="//kino-teatr.ua/uk/" hreflang="uk"<?else:?>href="//kino-teatr.ua/" hreflang="ru"<?endif;?> />
<!-- bannerky -->
	<?/* script type="text/javascript" src="<?=_3PARY_URL?>adriver.core.2.js"></script */?>
	<script type='text/javascript' src='https://ad.kino-teatr.ua/www/delivery/spcjs.php?id=1'></script>
	<!-- end bannerky -->
	<!-- Widgets -->
	<script type="text/javascript" src="<?=$skin_url?>js/share.js?10"></script>
	<script src="<?=$skin_url?>js/auth.fb.js?<?=rand(1000,10000)?>"></script>
<script src="<?=$skin_url?>js/auth.g.js?<?=rand(1000,10000)?>"></script>
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'ru'}
</script>
	<!-- End Widgets -->
	<script type="text/javascript" src="<?=$skin_url?>js/jquery.js"></script>
	<script type="text/javascript">

		type = 0;
		objid = 0;

		function SubmitSearch()
		{
			var returnval;


			if(!active)
			{
			returnval = true;
			} else {
			var url;

			url = objid;
			window.location = url;
			returnval = false;
			}


			return returnval;
		}

	</script>
	<link rel="stylesheet" type="text/css" href="<?=$skin_url?>droplinebar.css" />
	<script src="<?=$skin_url?>js/droplinemenu.js" type="text/javascript"></script>
	<script type="text/javascript">
		droplinemenu.buildmenu("mydroplinemenu");

		$(function() {
			$("#searchField").focus(function(){
				$(this).animate({ width:"407px"}, 350);
				if(this.value == this.defaultValue) this.value = '';
			}).blur(function(){
				$(this).animate({ width:"240px"}, 300);
				if(this.value == '') this.value = this.defaultValue;
				setTimeout("$('#listbox2').css('display','none')",200);
			});
		});

	</script>
<link href="<?=$skin_url?>screen.css" rel="stylesheet" type="text/css" media="screen" />
<?/* banner catfish
<script language=JavaScript>
function closeclouds()
{
	$('#fixmetoo').css('display', 'none');
}
</script>
<style type="text/css">
#fixmetoo { position: fixed; right: 0px; bottom: 0px; }

div > div#fixmetoo { position: fixed; }
pre.fixit { overflow:auto;border-left:1px dashed #000;border-right:1px dashed #000;padding-left:2px; }

		</style>
		<!--[if gte IE 5.5]><![if lt IE 7]>
		<style type="text/css">
div#fixmetoo {
right: auto; bottom: auto;
left: expression( ( 0 - fixmetoo.offsetWidth + ( document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.clientWidth ) + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + 'px' );
top: expression( ( 0 - fixmetoo.offsetHeight + ( document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.clientHeight ) + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + 'px' );
}
		</style>
		<![endif]><![endif]-->
end banner */?>
			<script>
			function closeTopBanner()
			{
				$('#topBanner').css("display", "none");
			}
			</script>

			<script>
			function strpos( haystack, needle, offset){
   				 var i = haystack.indexOf( needle, offset ); // returns -1
			    return i >= 0 ? i : false;
			}
			</script>

		<link type="text/css" href="<?=$skin_url?>pikachoose-whiteout-theme.css" rel="stylesheet" />
		<script type="text/javascript" src="<?=$skin_url?>jquery.pikachoose.js"></script>
    <?=sys::attentionUserInfo()?>
	<!-- start mobilniy banner -->
	<!-- start propeller -->
<? //script src="//go.mobtrks.com/notice.php?p=1221881&interstitial=1"></script ?>
<!-- end propeller -->

<!-- start mobilniy banner -->
	<script charset="UTF-8" src="//cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/6a9b391d34454d0379806d0565e2a1fb_0.js" async></script>
</head>
<body>
<h1><?=htmlspecialchars($_meta_title)?></h1>
<div id='fixmetoo' style='z-index: 6500; width: 100%;'></div>
	<script type="text/javascript">
		window.onload = function(e){
			/*Mike header_main*/
			
			$("#ourBranding").bind("click", function()
			{
				$.ajax({
				   type: "GET",
				   url: '?mod=main&act=ajx_brending_click',
				});
				return true;
			});
			
			<?=banners::showBanners('branding_images')?>
			
			$('#footHide').css('display', 'inline-block');
			$('#mainAnounce').load('?lang=<?=$_cfg['sys::lang']?>&mod=main&act=ajx_botban');
						<?/* new adriver("banner", {sid: 169393, bt:52, bn:2, pz:2});
			 new adriver("fullscreen_banner", {sid: 169393, bt:52, bn:5, pz:5});
			new adriver("topBanner", {sid: 169393, bt:52, bn:6, pz:6});
			new adriver("fixmetoo", {sid: 169393, bt:52, bn:7, pz:7});
			new adriver("secondbanner", {sid: 169393, bt:52, bn:9, pz:9});
			new adriver("counter", {sid: 169393, bt:52, bn:4, pz:4});
			new adriver("fullscreen_banner", {sid: 169393, bt:52, bn:5, pz:5}); */?>			

	}
	</script>
<?=main::useSocials()?>
<div id='grayHeaderLine'>
	<div id='headerWrap'>
<script>
function changeMyCity(i, city)
{

$('#cityLink').html(city);
ajx_url = '?mod=main&act=ajx_bill&city_id='+i

$.ajax({
   type: "GET",
   url: ajx_url,
   data: {newcity:i},
   success: function(html){
<?


switch(substr($_SERVER['REQUEST_URI'], 0, 10))
{
/*
	case '/bill.phtm':
		echo 'window.location = html';
	break;

	case '/uk/bill.p':
		echo 'window.location = html';
	break;

	case '/kinoafish':
		echo 'window.location = html';
	break;

	case '/uk/kinoaf':
		echo 'window.location = html';
	break;   */

	default:
		echo 'location.reload(true);';
}



?>
   }
});



}

</script>
<script>
openedcit = null;

$('body').bind('click', function(e){
if(openedcit==1)
{
$('#myCityList').fadeOut('fast');
openedcit = null;
}
});


function showMyCity(i)
{
$('#myCityList').fadeIn('fast', function(e){openedcit = 1;});

}

function changeLang()
{
	document.location.href = '<?=sys::changeLang()?>';
}


</script>
<?$city['values']=main_countries::getCountryCitiesTitles(29)?>
<?$city['value']=$_cfg['main::city_id']?>
<?$city['input']='selectbox'?>
<?$city['title']=sys::translate('main::city')?>
		<div id='yourCity'><a href='#' onclick='showMyCity(1); return false;' id='cityLink'><?=$city['values'][$city['value']]?></a></div>
		<ul id='language'>
			<?foreach ($_langs as $lng):?>
				<?if($_cfg['sys::lang']==$lng['code']):?>
					<li class='active' id='lang<?=$lng['code']?>'><a><span><?=$lng['short_title']?></span></a></li>
				<?else:?>
					<li id='lang<?=$lng['code']?>'><a href='#lang' onclick="return changeLang();" title="<?=$lng['title']?>"><span><?=$lng['short_title']?></span></a></li>
				<?endif?>

			<?endforeach;?>
		</ul>

		<?if($_user['id']==2):?>
<div id='loginForm1' style='display: none; height: 40px; '>
	<div id='loginForm'>
	<form method="POST" action="/ru/main/login.phtml">
		<input type="hidden" name="url" value="//<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>">
			<input type="text" onfocus='this.value=""' maxlength="32" name="login" class="regTextField" value='<?=sys::translate('main::login')?>'>
			<input type="password" onfocus='this.value=""' name="password" class="regTextField" value='<?=sys::translate('main::password')?>'>
			<input type="submit" value="<?=sys::translate('main::entry')?>" class="regBtn">
		</form>
	</div>
</div>

<div id='changeLogin'>

<!-- start ulogin -->
<div id='openApi'>
<span><?=sys::translate('main::enterWith')?></span>

<style>
#uLogin img:hover {
	cursor: pointer;
}
#___signin_0, g-signin {
	display:none !important;
}
</style>
<script src="<?=$skin_url?>js/auth.fb.js?<?=rand(1000,10000)?>"></script>
<script src="<?=$skin_url?>js/auth.g.js?<?=rand(1000,10000)?>"></script>
<script src = "https://plus.google.com/js/client:platform.js" async defer></script>

<div id="uLogin">
<img src="/ico/facebook24.gif" onclick="FBcheckLoginState()" />
<img src="/ico/google24.png" onclick="GcheckLoginState()" />

<button id="google-auth-btn" class="g-signin" style="display:none"
          data-scope="profile"
          data-clientid="329278728251-v06bbnf39a2dhi7203h1f7uvusks2kod.apps.googleusercontent.com"
          data-callback="GoogleOnSignInCallback"
          data-theme="dark"
          data-cookiepolicy="single_host_origin"
/>
</button>
</div>
</div>

<!-- end ulogin -->

<script>
function showLoginBox()
{
	$('#changeLogin').fadeTo('fast','0', function(){$('#changeLogin').css('display', 'none');});
	$('#search').fadeTo('fast','0', function(){$('#search').css('display', 'none');$('#loginForm1').css('display', 'block');});


}
</script>
	<a href='<?=sys::rewriteUrl('?mod=main&act=register')?>' id='register'><?=sys::translate('main::register')?></a>
		<ul id='enter'>
			<li><a href onclick='showLoginBox(); return false;'><span><?=sys::translate('main::enter')?></span></a></li>
		</ul>
</div>
        <?else:?>
        	<div id='afterLogin'>
				<?=sys::translate('main::welcome')?> <?=$_user['login']?>!&nbsp;
				<a href="<?=sys::rewriteUrl('?mod=main&act=user_profile')?>"><?=sys::translate('main::profile')?></a>&nbsp;
				<a href="<?=sys::rewriteUrl('?mod=main&act=login')?>"><?=sys::translate('main::exit')?></a>
			</div>
            <style>
            	div.listbox2
            	{
            		right: 60px !important;
            	}
            </style>
		<?endif;?>

						<form  ACTION="<?=sys::rewriteUrl('?mod=main&act=search')?>"  NAME="searchform" onSubmit="return SubmitSearch()" method="GET" id="search">

									<input type='text'  autocomplete='off' id='searchField' value='<?=sys::translate('main::search')?>' name="keyword" onclick='if (this.value==this.defaultValue) this.value=""'>
									<input type='submit' id='searchButton'>
						</form>
								<script>

								function Timeout(fn, interval) {
								    var id = setTimeout(fn, interval);
								    this.cleared = false;
								    this.clear = function () {
								        this.cleared = true;
								        clearTimeout(id);
								    };
								}


								$('#searchField').bind('keyup',function(e){

									var kec = e.which;

									if(kec != 40 && kec != 38) {

									    ajx_url = '?mod=main&act=ajx_search&mode=option';
	                        			field = 'films';
	                        			num_of_chars = '1';
	                        			obj = document.getElementById('searchField');

										list=document.getElementById('_ajx_'+field+'_list');
										box=list.parentNode;
										if(obj.value.length>num_of_chars)
										{


											$.ajax({
											   type: "GET",
											   url: ajx_url,
											   data: {field:field,keywords:obj.value},
											   success: function(html){
													box.style.display='block';
										    		$("#_ajx_"+field+"_list").html(html);
										 		 }
											 });
										}

									} else if(kec == 40) {
											var i;
											for (i=0;i<=total;i++)
											{
												$("#lnk"+active).removeClass('hover');
											}
											active++;
											if (active>total)
											{
												active = 0;
											} else {
												$("#lnk"+active).addClass('hover');
												type = $("#lnk"+active).attr('type');
												objid = $("#lnk"+active).attr('objid');
											}

											if (!active)
											{
												type = 0;
												objid = 0;
											}

									} else if(kec == 38) {
											var i;
											for (i=0;i<=total;i++)
											{
												$("#lnk"+active).removeClass('hover');
											}
											active--;
											if (active<0)
											{
												active = total;
												$("#lnk"+active).addClass('hover'); 
												type = $("#lnk"+active).attr('type');
												objid = $("#lnk"+active).attr('objid');
											} else if (active>0) {
												$("#lnk"+active).addClass('hover');
												type = $("#lnk"+active).attr('type');
												objid = $("#lnk"+active).attr('objid');
											}

											if (!active)
											{
												type = 0;
												objid = 0;
											}



									}


								});

								</script>
						<div class="listbox2" id='listbox2' onclick='changeFlag("1");setTimeout(hideList, 50); '>
							<div class="panel"  id="_ajx_films_list" onclick='changeFlag("1");setTimeout(hideList, 50); '>&nbsp;</div>
						</div>

		<div id='lineBlank'><img src='images/blank.gif' height='1'></div>

<?=main::showMainBill()?>
	</div>
</div>


<div id='wrapper' style='position: relative;'>
	<div id='wrap' style='position: relative;'>

<style>
#Advertisement
{
	margin-left: 13px;
	position: relative;
	top: 2px;
}

#myElement_wrapper
{
	left: 13px;
}
</style>
<? /*
<script>
function showVideo()
{

	if ($('#myElement_wrapper').css('height')!='200px')
	{
		jwplayer("myElement").setup({
			file: "http://kino-teatr.ua/mods/main/skins/default/img_brend/30minutes.flv",
			autostart:true
		});
	} else {
		window.open("http://ad.adriver.ru/cgi-bin/click.cgi?sid=1&ad=367226&bt=21&pid=1012519&bid=2186736&bn=2186736&rnd=1245254592","_blank" );
	}
}
</script>

*/ ?>
<!-- LINK Brending  -->
<?=banners::showBanners('branding_links')?>
<!--  END LINK Brending  -->
		<div id='content'>
			<div id='topMenuBlock'>
				<a href='<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?endif;?>' class='logo'>&nbsp;</a>
				<input type='hidden' id='menucount' value='50'>
				<?=main_menu::showMenu()?>
			</div>
			<div id='todayBlock'>
			<!-- start topBanner -->
			<?=banners::showBanners('v3_top_main')?>
		<!-- end topBanner -->

			    <?=main::watchInCinema()?>
				<?=main::todayInCinema()?>
			    <script type="text/javascript">
			    $(document).ready(function() {
			        $('.viewport').mouseenter(function(e) {
			            $(this).children('a').children('img').animate({ height: '100', left: '0', top: '0', width: '368'}, 100);
			        }).mouseleave(function(e) {
			            $(this).children('a').children('img').animate({ height: '110', left: '-15', top: '-5', width: '398'}, 100);
			        });
			    });
			    </script>
				
				
			</div>

