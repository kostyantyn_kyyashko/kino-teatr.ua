<?sys::useLib('content');?>
<?$_var['main::title']=$var['page']['title']?>
<?$_var['active_menu']=content::getEntryNodeId(21)?>
<?sys::setMeta($var['page']);?>
<?//main::showGemius('BxXlPWesbcOpRUSgTXzXkYZiLcBRDab_s0kaAps_0Lv.j7');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?> 
<?sys::useJs('sys::form');?>
<?sys::jsInclude('sys::form');?>
<?sys::useJs('sys::gui');?>
<?sys::useJs('main::main')?>
<br>
<div id="main_review_edit" class="user-profile-form">
	<form enctype="multipart/form-data" id="form" onsubmit="if(checkForm(this)){alert('<?=sys::parseModTpl('main::review_alert')?>');$('#form input[name=cmd_save]').css('display','none')}else{return false}" method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
	<table class="xTable" width="100%">
		<tr>
			<td colspan="2" style="text-align:center">
				<?=sys::parseModTpl('main::review_rules','html');?><br>
			</td>
		</tr>
		<tr>
			<th>&nbsp;<?=$var['fields']['title']['title']?>*:</th>
			<td><?=sys_form::parseField('title',$var['fields']['title'],'size="55" maxlength="100"');?></td>
		</tr>
		<tr>
			<th>&nbsp;<?=$var['fields']['date']['title']?>*:</th>
			<td><?=sys_form::parseField('date',$var['fields']['date']);?></td>
		</tr>
		<tr>
			<th colspan="2">&nbsp;<?=$var['fields']['intro']['title']?>*:</th>
		</tr>
		<tr>
			<td colspan="2">&nbsp;<?=sys_form::parseField('intro',$var['fields']['intro'],'style="width:100%; height:150px;"');?></td>
		</tr>
		<tr>
			<th colspan="2">&nbsp;<?=$var['fields']['text']['title']?>:*</th>
		</tr>
		<tr>
			<td colspan="2">&nbsp;<?=sys_form::parseField('text',$var['fields']['text']);?></td>
		</tr>
		<tr>
			<th width="30%">&nbsp;<?=$var['fields']['mark']['title']?>*:</th>
			<td><?=sys_form::parseField('mark',$var['fields']['mark']);?><strong>/10</strong></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;<strong><?=$var['fields']['image']['title']?> (<?=sys::translate('main::size')?> 250*300 px)*:</strong> &nbsp;<?=sys_form::parseField('image',$var['fields']['image']);?>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
				<input class="xRed" type="submit" class="xButton" name="cmd_save" value="<?=sys::translate('main::save')?>">
				<input class="xRed" type="submit" class="xButton" name="cmd_cancel" value="<?=sys::translate('main::cancel')?>">
			</td>
		</tr>
	</table>
	</form>
</div>