<?sys::useLib('content');?>
<?$_var['right']=main_films::showFilmMedia($var['film_id'], $var['film'])?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=content::getEntryNodeId(19)?>
<?sys::setJsFooter('document.getElementById("poster_'.$var['poster_id'].'").parentNode.parentNode.parentNode.parentNode.parentNode.scrollLeft=document.getElementById("poster_'.$var['poster_id'].'").parentNode.offsetLeft-200;');?>
<?sys::setMeta($var['meta']);?>
<?//main::showGemius('AqhArSMPxvA.scYfA7xHU8eAXte8pru76demTisMwSP.o7');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?> 
<div id="main_film_posters">
<?=main_films::showFilmTabs($var['film_id'],$var['film'],'posters');?>
	<a onclick="window.open('<?=$var['url']?>','','<?=sys::setWinParams(800,600)?>'); return false;" class="xImage" href="<?=$var['url']?>" title="<?=sys::translate('main::posters_from_film')?> <?=$var['film']?>"><img width="495" src="<?=$var['image']?>" alt="<?=sys::translate('main::posters_from_film')?> <?=$var['film']?>" title="<?=sys::translate('main::posters_from_film')?> <?=$var['film']?>"></a>
	<center><a onclick="window.open('<?=$var['url']?>','','<?=sys::setWinParams(800,600)?>'); return false;" class="zoom" href="<?=$var['url']?>"><?=sys::translate('main::zoom')?></a></center>
	<center class="prevNext xLinks1"><?if($var['prev_url']):?><a title="<?=sys::translate('main::prev_frame')?>" href="<?=$var['prev_url']?>">&laquo;</a><?endif;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?if($var['next_url']):?><a title="<?=sys::translate('main::next_frame')?>" href="<?=$var['next_url']?>">&raquo;</a>&nbsp;<?endif;?></center>
	<br>
	<div style="height:30px">
		<div class="image_looks"><?=sys::translate('main::looks')?>: <b><?=$var['shows']?></b></div>
		<div class="image_buttons">
			<form method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
				<input type="hidden" name="_task">
				<?if($var['edit_url']):?>	
					<input type="button" onclick="window.location='<?=$var['edit_url']?>'" class="xSilver" value="<?=sys::translate('main::edit')?>">
				<?endif;?>
				<?if($var['delete_poster']):?>	
					<input type="button" onclick="if(confirm('<?=sys::translate('main::are_you_sure')?>')){this.form.elements['_task'].value='delete';this.form.submit();}" class="xSilver" value="<?=sys::translate('main::delete')?>">
				<?endif;?>
				<?if(!$var['public']):?>	
					<input type="submit" name="cmd_confirm" class="xSilver" value="<?=sys::translate('main::confirm')?>">
				<?endif;?>
			</form>
		</div>
	</div>
	<div class="xHr">&nbsp;</div>
	<div class="xPreviews">
		<table align="center">
			<tr>
				<?foreach ($var['previews'] as $preview):?>
					<?if($preview['id']==$var['poster_id']) $class='xCurrent';else $class=false;?>
					<td class="x <?=$class?>"><a id="poster_<?=$preview['id']?>" class="xImage" href="<?=$preview['url']?>" title="<?=sys::translate('main::posters_from_film')?> <?=$var['film']?>"><img width="100" height="130" src="<?=$preview['image']?>" alt="<?=sys::translate('main::posters_from_film')?> <?=$var['film']?>" title="<?=sys::translate('main::posters_from_film')?> <?=$var['film']?>"></a></td>
				<?endforeach;?>
			</tr>
		</table>
	</div>
	<?=sys::parseTpl('main::pages',$var['pages'])?>
	<div class="xHr">&nbsp;</div>
	<?if($var['add_url']):?>
		<input class="xRed image_add" type="button" value="<?=sys::translate('main::add_poster')?>" onclick="window.location='<?=$var['add_url']?>'">
		<br>
	<?endif;?>
</div>
