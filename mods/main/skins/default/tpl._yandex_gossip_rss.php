<?sys::setTpl('main::__rss');?>
<channel>
<rss2lj:owner xmlns:rss2lj="http://rss2lj.net/NS">kino_teatr_gossip</rss2lj:owner>
<title><?=sys::parseModTpl('main::gossip_rss_description')?> Kino-Teatr.ua</title>
<link><?=_ROOT_URL?></link>
<description><?=sys::parseModTpl('main::gossip_rss_description')?></description>
<language><?=$_cfg['sys::lang']?></language>
<pubDate><?=date('r')?></pubDate>

<generator>Grifix</generator>
<lastBuildDate><?=date('r')?></lastBuildDate>
<copyright>Copyright 2000-<?=date('r')?>, Олег Бойко</copyright>
<category><?=sys::translate('main::portal_gossip')?></category>
<managingEditor>admin@kino-teatr.kiev.ua (Олег Бойко)</managingEditor>
<webMaster>admin@kino-teatr.kiev.ua (Олег Бойко)</webMaster>
<docs>http://beshenov.ru/rss2.html</docs>
<ttl>60</ttl>

<?foreach ($var['objects'] as $obj):?>
<item>
	<title><?=$obj['title']?></title>
	<link><?=$obj['url']?></link>
	<dc:creator><?=$obj['login']?>, Kino-Teatr.ua</dc:creator>
	<description><?=$obj['intro']?></description>
	<content:encoded><![CDATA[<?=$obj['text']?>]]></content:encoded>
	<pubDate><?=$obj['date']?></pubDate>
	<?if($obj['image']):?>
		<enclosure url="<?=$obj['image_src']?>" type="<?=$obj['image_mime']?>" length="<?=$obj['image_size']?>"/>
	<?endif?>
	<guid isPermaLink="true"><?=$obj['url']?></guid>
	<categoty><?=$obj['category']?></categoty>
</item>
<?endforeach;?>
</channel>

