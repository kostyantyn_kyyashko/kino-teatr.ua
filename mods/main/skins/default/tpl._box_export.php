<?sys::setTpl('main::__xml')?>
<boxoffice>
		<?foreach ($var['films'] as $film):?>
			<movie>
				<week><?=$film['week']?></week>
				<pos><?=$film['pos']?></pos>
				<id><?=$film['id']?></id>
				<title-r><?=$film['title-r']?></title-r>
				<title-o><?=$film['title-o']?></title-o>
				<year><?=$film['year']?></year>
				<date><?=$film['date']?></date>
				<screens><?=$film['screens']?></screens>
				<usd><?=$film['usd']?></usd>
				<total><?=$film['total']?></total>

			</movie>
		<?endforeach;?>
</boxoffice>
