<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?sys::setMeta($var['meta']);?>
<?//main::showGemius('Akfq7yOPI15fkPwsW1Mjtsey7DS8HR.Cs0fUiOBiFDH.w7');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>


				<div class='myriad' id='myriad'><?=$var['login']?></div>


					<div id='lineHrWide'>&nbsp;</div>

	<?=main_users::showUserTabs($_GET['user_id'],'info');?>

						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>


<div id='userProfileAvatar'>
	<img src='<?=$var['avatar']?>' align="right">
</div>

<div id='userProfileInfo'>
	<b><?=sys::translate('main::reg_date')?>:</b> <?=$var['date_reg']?><br>
	<b><?=sys::translate('main::birth_date')?>:</b> <?=$var['birth_date']?>	<br>
	<b><?=sys::translate('main::age')?>:</b> <?=$var['age']?><br>
	<b><?=sys::translate('main::sex')?>:</b> <?=$var['sex']?><br>
	<?if($var['info']):?>
		<b><?=sys::translate('main::about_me')?>:</b> <?=$var['info']?><br>
	<?endif;?>
	<?if($var['genres']):?>
		<?$zpt = "";?>
		<b><?=sys::translate('main::genres_preferences')?>:</b> 
		<?foreach ($var['genres'] as $obj):?><?=$zpt.$obj['titles']?><?$zpt = ", ";?><?endforeach;?>
	    <br>
	<?endif;?>
	<b><?=sys::translate('main::last_visit')?>:</b> <?=$var['date_last_visit']?><br>
</div>


<script>
function showMore(template)
{
	name = '.'+template;
	button = '.b_'+template;
	if ($(name).css('display')=='none')
	{
		$(name).fadeIn('slow');
		$(button).html('<?=sys::translate('main::hide')?>');
	} else {
		$(name).fadeOut('fast');
		$(button).html('<?=sys::translate('main::show_all')?>');
	}
}
</script>


<div id='lineBlank' style='height:10px;width:300px;'><img src='/images/blank.gif' height='1'></div>

<? if($var['liked_films']): ?>
<div id="blockHeader"><h3 style="margin-top:30px;"><?=sys::translate('main::liked_films')?></h3></div>
<div id="lineBlank" style="height: 10px;"></div>

<div class="blockLidersPremiers" id="weekPremiersBlock">
<ul class='lidersUl' >
	<?$i=1; $j=1; $class = $style = "";?>
	<?foreach ($var['liked_films'] as $obj):?>
	<?
		$clear = "";
		if($i==5) 
		{
			$obj['class']='no-margin';
			$i=0;
			$clear = '<li style="width:100%;height:30px;clear:both;"></li>';
		}
		if($j++>10) 
		{
			$class = " profileTopics_liked_films";
			$style = "style='display: none;'";
			if($clear) $clear = '<li class="'.trim($class).'" style="width:100%;height:30px;clear:both;display: none;"></li>';
		}
		else $class = $style = "";
	?>

		<li class='<?=$obj['class']?><?=$class?>' <?=$style?>>			
			<a href='<?=$obj['film_url']?>' title="<?=sys::translate('main::film')?> <?=$obj['title']?> - <?=$obj['year']?>" style="height:186px;">
				<img src='<?=$obj['image']?>'>
			</a>
			<div class=''><?=sys::translate('main::year')?>: <?=$obj['year']?></div>
			<div class=''><?=sys::translate('main::user_rating')?>: <?=$obj['mark']?></div>
			<div class='liderName'><?=$obj['title']?></div>
			<ul>
				<?if($obj['trailers']):?><li class='trailerLi'><a href='<?=$obj['trailers']?>' title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=sys::translate('main::watchonline')?>"><?=sys::translate('main::trailers')?></a></li><?endif;?>
				<?if($obj['shows']):?><li class='watchLi'><a href='<?=$obj['shows']?>' title="<?=sys::translate('main::film')?> <?=$obj['title']?> - <?=sys::translate('main::filmshows')?>"><?=sys::translate('main::whereToWatch')?></a></li><?endif;?>
			</ul>
		</li>
		<?=$clear?>
		<?$i++;?>
	<?endforeach;?>
</ul>

    <?if($j>11):?>
	<div class='FBlockShow'>
		<span style="float: left; margin-top: 6px; margin-right: 10px;">Всего: <?=sizeof($var['liked_films'])?> </span><a onclick="showMore('<?=trim($class)?>'); return false;" class="b_<?=trim($class)?>" id="boxDetails" href=""><?=sys::translate('main::show_all')?></a>
	</div>
	<?endif;?>

</div>
<div id='lineHrWide'>&nbsp;</div>
<? endif;//of if($var['liked_films']): ?>
					


<? if($var['not_liked_films']): ?>
<div id="blockHeader"><h3 style="margin-top:30px;"><?=sys::translate('main::not_liked_films')?></h3></div>
<div id="lineBlank" style="height: 10px;"></div>

<div class="blockLidersPremiers" id="weekPremiersBlock">
<ul class='lidersUl' >
	<?$i=1; $j=1; $class = $style = "";?>
	<?foreach ($var['not_liked_films'] as $obj):?>
	<?
		$clear = "";
		if($i==5) 
		{
			$obj['class']='no-margin';
			$i=0;
			$clear = '<li style="width:100%;height:30px;clear:both;"></li>';
		}
		if($j++>10) 
		{
			$class = " profileTopics_not_liked_films";
			$style = "style='display: none;'";
			if($clear) $clear = '<li class="'.trim($class).'" style="width:100%;height:30px;clear:both;display: none;"></li>';
		}
		else $class = $style = "";
	?>

		<li class='<?=$obj['class']?><?=$class?>' <?=$style?>>			
			<a href='<?=$obj['film_url']?>' title="<?=sys::translate('main::film')?> <?=$obj['title']?> - <?=$obj['year']?>" style="height:186px;">
				<img src='<?=$obj['image']?>'>
			</a>
			<div class=''><?=sys::translate('main::year')?>: <?=$obj['year']?></div>
			<div class=''><?=sys::translate('main::user_rating')?>: <?=$obj['mark']?></div>
			<div class='liderName'><?=$obj['title']?></div>
			<ul>
				<?if($obj['trailers']):?><li class='trailerLi'><a href='<?=$obj['trailers']?>' title="<?=sys::translate('main::film')?> <?=$obj['title']?> <?=sys::translate('main::watchonline')?>"><?=sys::translate('main::trailers')?></a></li><?endif;?>
			</ul>
		</li>
		<?=$clear?>
		<?$i++;?>
	<?endforeach;?>
</ul>
	<?if($j>11):?>
	<div class='FBlockShow'>
		<span style="float: left; margin-top: 6px; margin-right: 10px;">Всего: <?=sizeof($var['not_liked_films'])?> </span><a onclick="showMore('<?=trim($class)?>'); return false;" class="b_<?=trim($class)?>" id="boxDetails" href=""><?=sys::translate('main::show_all')?></a>
	</div>
	<?endif;?>

</div>
<div id='lineHrWide'>&nbsp;</div>
<? endif;//of if($var['not_liked_films']): ?>


<? if($var['liked_persons']): ?>
<div id="blockHeader"><h3 style="margin-top:30px;"><?=sys::translate('main::liked_persons')?></h3></div>
<div id="lineBlank" style="height: 10px;"></div>

<div class="blockLidersPremiers" id="weekPremiersBlock">
<ul class='lidersUl' >
	<?$i=1; $j=1; $class = $style = "";?>
	<?foreach ($var['liked_persons'] as $obj):?>
	<?
		$clear = "";
		if($i==5) 
		{
			$obj['class']='no-margin';
			$i=0;
			$clear = '<li style="width:100%;height:30px;clear:both;"></li>';
		}
		if($j++>10) 
		{
			$class = " profileTopics_liked_persons";
			$style = "style='display: none;'";
			if($clear) $clear = '<li class="'.trim($class).'" style="width:100%;height:30px;clear:both;display: none;"></li>';
		}
		else $class = $style = "";
	?>

		<li class='<?=$obj['class']?><?=$class?>' <?=$style?>>			
			<a href='<?=$obj['person_url']?>' title="<?=sys::translate('main::person')?> <?=$obj['title']?> - <?=$obj['year']?>" style="height:186px;">
				<img src='<?=$obj['image']?>'>
			</a>
			<div class=''><?=sys::translate('main::user_rating')?>: <?=$obj['mark']?></div>
			<div class='liderName'><?=$obj['title']?></div>
			<ul>
				<?if($obj['trailers']):?><li class='trailerLi'><a href='<?=$obj['trailers']?>' title="<?=sys::translate('main::person')?> <?=$obj['title']?> <?=sys::translate('main::watchonline')?>"><?=sys::translate('main::trailers')?></a></li><?endif;?>
				<?if($obj['shows']):?><li class='watchLi'><a href='<?=$obj['shows']?>' title="<?=sys::translate('main::person')?> <?=$obj['title']?> - <?=sys::translate('main::personshows')?>"><?=sys::translate('main::whereToWatch')?></a></li><?endif;?>
			</ul>
		</li>
		<?=$clear?>
		<?$i++;?>
	<?endforeach;?>
</ul>
	<?if($j>11):?>
	<div class='FBlockShow'>
		<span style="float: left; margin-top: 6px; margin-right: 10px;">Всего: <?=sizeof($var['liked_persons'])?> </span><a onclick="showMore('<?=trim($class)?>'); return false;" class="b_<?=trim($class)?>" id="boxDetails" href=""><?=sys::translate('main::show_all')?></a>
	</div>
	<?endif;?>

</div>
<div id='lineHrWide'>&nbsp;</div>
<? endif;//of if($var['liked_persons']): ?>
					

<? if($var['not_liked_persons']): ?>
<div id="blockHeader"><h3 style="margin-top:30px;"><?=sys::translate('main::not_liked_persons')?></h3></div>
<div id="lineBlank" style="height: 10px;"></div>

<div class="blockLidersPremiers" id="weekPremiersBlock">
<ul class='lidersUl' >
	<?$i=1; $j=1; $class = $style = "";?>
	<?foreach ($var['not_liked_persons'] as $obj):?>
	<?
		$clear = "";
		if($i==5) 
		{
			$obj['class']='no-margin';
			$i=0;
			$clear = '<li style="width:100%;height:30px;clear:both;"></li>';
		}
		if($j++>10) 
		{
			$class = " profileTopics_not_liked_persons";
			$style = "style='display: none;'";
			if($clear) $clear = '<li class="'.trim($class).'" style="width:100%;height:30px;clear:both;display: none;"></li>';
		}
		else $class = $style = "";
		?>

		<li class='<?=$obj['class']?><?=$class?>' <?=$style?>>			
			<a href='<?=$obj['person_url']?>' title="<?=sys::translate('main::person')?> <?=$obj['title']?> - <?=$obj['year']?>" style="height:186px;">
				<img src='<?=$obj['image']?>'>
			</a>
			<div class=''><?=sys::translate('main::user_rating')?>: <?=$obj['mark']?></div>
			<div class='liderName'><?=$obj['title']?></div>
			<ul>
				<?if($obj['trailers']):?><li class='trailerLi'><a href='<?=$obj['trailers']?>' title="<?=sys::translate('main::person')?> <?=$obj['title']?> <?=sys::translate('main::watchonline')?>"><?=sys::translate('main::trailers')?></a></li><?endif;?>
				<?if($obj['shows']):?><li class='watchLi'><a href='<?=$obj['shows']?>' title="<?=sys::translate('main::person')?> <?=$obj['title']?> - <?=sys::translate('main::personshows')?>"><?=sys::translate('main::whereToWatch')?></a></li><?endif;?>
			</ul>
		</li>
		<?=$clear?>
		<?$i++;?>
	<?endforeach;?>
</ul>

	<?if($j>11):?>
	<div class='FBlockShow'>
		<span style="float: left; margin-top: 6px; margin-right: 10px;">Всего: <?=sizeof($var['not_liked_persons'])?> </span><a onclick="showMore('<?=trim($class)?>'); return false;" class="b_<?=trim($class)?>" id="boxDetails" href=""><?=sys::translate('main::show_all')?></a>
	</div>
	<?endif;?>

</div>
<div id='lineHrWide'>&nbsp;</div>
<? endif;//of if($var['not_liked_persons']): ?>
					


<? if($var['reviews']): ?>
<div id="blockHeader"><h3 style="margin-top:30px;"><?=sys::translate('main::reviews')?></h3></div>
	<?$i=1; $class = $style = "";?>
	<?foreach ($var['reviews'] as $obj):?>
		<? 
			if($i++>3) 
			{
				$class = " profileTopics_reviews";
				$style = "style='display: none;'";
			}
			else $class = $style = "";
		?>

		<div class="profileTopics<?=$class?>" <?=$style?>>			
			<a href='<?=$obj['url']?>' class="profileTopicsPhoto" title='<?=$obj['title']?>'>
				<img src='<?=$obj['image']?>' alt='<?=$obj['title']?>'>
			</a>
			<a href='<?=$obj['url']?>' class="profileTopicsLink" title='<?=$obj['title']?>'><?=$obj['title']?></a>
			<p class="profileTopicsText"><?=$obj['intro']?></p>
			<p class="mainNewsDate">
				<span style="float:right;"><?=$obj['date']?></span>
			</p>
		</div>
		
	<?endforeach;?>

	<?if($i>4):?>
	<div class='FBlockShow'>
		<span style="float: left; margin-top: 6px; margin-right: 10px;">Всего: <?=sizeof($var['reviews'])?> </span><a onclick="showMore('<?=trim($class)?>'); return false;" class="b_<?=trim($class)?>" id="boxDetails" href=""><?=sys::translate('main::show_all')?></a>
	</div>
	<?endif;?>
	<div id='lineHrWide'>&nbsp;</div>
<? endif;//of if($var['reviews']): ?>
	
		


                        


<? if($var['thanks']): ?>
<div id="blockHeader"><h3 style="margin-top:30px;"><?=sys::translate('main::2thanks')?></h3></div>
	<?$i=1; $class = $style = "";?>
	<?foreach ($var['thanks'] as $obj):?>
		<? 
			if($i++>3) 
			{
				$class = " profileTopics_thanks";
				$style = "style='display: none;'";
			}
			else $class = $style = "";
		?>
		<div class="profileTopics<?=$class?>" <?=$style?>>			
			<a href='<?=$obj['url']?>' class="profileTopicsPhoto" title='<?=$obj['title']?>'>
				<img src='<?=$obj['image']?>' alt='<?=$obj['title']?>'>
			</a>
		<p class="mainNewsTitle">
		
			<b><?=sys::translate('main::autoru')?></b> <a href='<?=$obj['author_url']?>'><?=$obj['author']?></a> <br>
			<b><?=sys::translate('main::for_the_review')?></b> <a href='<?=$obj['url']?>' title='<?=$obj['title']?>'><?=$obj['title']?></a> <br>
			<b><?=sys::translate('main::of_the_film')?></b>  <a href='<?=$obj['film_url']?>'><?=$obj['film_title']?></a>
		
		</p>	
		<br>
			<!--a href='<?=$obj['url']?>' class="profileTopicsLink" title='<?=$obj['title']?>'><?=$obj['title']?></a-->
			<p class="profileTopicsText"><?=$obj['intro']?></p>
			<p class="mainNewsDate">
				<span style="float:right;">
					<?=$obj['date']?>
				</span>
			</p>
		</div>
		
	<?endforeach;?>

	<?if($i>4):?>
	<div class='FBlockShow'>
		<span style="float: left; margin-top: 6px; margin-right: 10px;">Всего: <?=sizeof($var['thanks'])?> </span><a onclick="showMore('<?=trim($class)?>'); return false;" class="b_<?=trim($class)?>" id="boxDetails" href=""><?=sys::translate('main::show_all')?></a>
	</div>
	<?endif;?>
	
<? endif;//of if($var['thanks']): ?>




<?if(sys::isDebugIP()):?>

<? if($var['added_persons']): ?>
<div id="blockHeader"><h3 style="margin-top:30px;"><?=sys::translate('main::added_persons')?></h3></div>
<div id="lineBlank" style="height: 10px;"></div>

<div class="blockLidersPremiers" id="weekPremiersBlock">
<ul class='lidersUl' >
	<?$i=1; $j=1; $class = $style = "";?>
	<?foreach ($var['added_persons'] as $obj):?>
	<?
		$clear = "";
		if($i==5) 
		{
			$obj['class']='no-margin';
			$i=0;
			$clear = '<li style="width:100%;height:30px;clear:both;"></li>';
		}
		if($j++>10) 
		{
			$class = " profileTopics_added_persons"; // !!!
			$style = "style='display: none;'";
			if($clear) $clear = '<li class="'.trim($class).'" style="width:100%;height:30px;clear:both;display: none;"></li>';
		}
		else $class = $style = "";
	?>

		<li class='<?=$obj['class']?><?=$class?>' <?=$style?>>			
			<a href='<?=$obj['person_url']?>' title="<?=sys::translate('main::person')?> <?=$obj['title']?> - <?=$obj['year']?>" style="height:186px;">
				<img src='<?=$obj['image']?>'>
			</a>
			<div class='liderName'><?=$obj['title']?></div>
			<ul>
				<?if($obj['trailers']):?><li class='trailerLi'><a href='<?=$obj['trailers']?>' title="<?=sys::translate('main::person')?> <?=$obj['title']?> <?=sys::translate('main::watchonline')?>"><?=sys::translate('main::trailers')?></a></li><?endif;?>
				<?if($obj['shows']):?><li class='watchLi'><a href='<?=$obj['shows']?>' title="<?=sys::translate('main::person')?> <?=$obj['title']?> - <?=sys::translate('main::personshows')?>"><?=sys::translate('main::whereToWatch')?></a></li><?endif;?>
			</ul>
		</li>
		<?=$clear?>
		<?$i++;?>
	<?endforeach;?>
</ul>
	<?if($j>11):?>
	<div class='FBlockShow'>
		<span style="float: left; margin-top: 6px; margin-right: 10px;">Всего: <?=sizeof($var['added_persons'])?> </span><a onclick="showMore('<?=trim($class)?>'); return false;" class="b_<?=trim($class)?>" id="boxDetails" href=""><?=sys::translate('main::show_all')?></a>
	</div>
	<?endif;?>

</div>
<div id='lineHrWide'>&nbsp;</div>
<? endif;//of if($var['added_persons']): ?>

<?endif;?>

<div style="padding-top:50px;" id="lineBlank"><img src="/mods/main/skins/default/images/blank.gif"></div>
