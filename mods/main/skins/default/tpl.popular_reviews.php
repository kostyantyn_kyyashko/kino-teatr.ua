					<div id='news'>
						<div id='blockHeader'>
       						<h3><?=sys::translate('main::otherReviews')?></h3>
						</div>

						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>


						<div id='popularNewsBlock'>
							<?foreach ($var['objects'] as $obj):?>

		<?if($obj['nr']==1):?>
		<div id='mainNews'>
		<div style='height: 266px;'>
			<a href='<?=$obj['url']?>' class='mainNewsPhoto'><img src='<?=$obj["image"]?>'></a>

			<a href='<?=$obj['url']?>' class='mainNewsLink' title='<?=$obj['title']?>'><?=$obj['title']?></a>

			<p class='mainNewsText'>
				<?=$obj['intro']?>
			</p>

			<p class='mainNewsDate'>
				&mdash; <?=$obj['date']?>
			</p>

		</div>
			<div id='lineHr'>&nbsp;</div>

		</div>

		<?else:?>

		<?if($obj['nr']==4):?>

			<div id='lineHr'>&nbsp;</div>
			</div>
		<?endif;?>


		<?if($obj['nr']==2):?>
			<div id='secondNews'>
		<?endif;?>

		<?if($obj['nr']==4):?>
			<div id='secondNews' class='nomargin'>
		<?endif;?>

		 <div class='newsItem<?=$obj["bottom"]?>'>
                         	<a href='<?=$obj['url']?>' class='newsItemPhoto'><img src='<?=$obj["image"]?>'></a>
				<a href='<?=$obj['url']?>' class='newsItemLink' title='<?=$obj['title']?>'><?=$obj['title']?></a>
				<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
				<p class='newsItemText'>
					<?=$obj['intro']?>
				</p>
                        </div>

		<?if($obj['nr']==5):?>

			<div id='lineHr'>&nbsp;</div>
		<?endif;?>


		<?endif;?>



	<?endforeach;?>

						</div>
</div></div>

