<? sys::useLib('main::spec_themes'); ?>

	<?foreach ($var['objects'] as $obj):?>
	
	<?$obj['spec_theme_url']=main_spec_themes::getArticleUrl($obj['spec_theme']);?>
	
		<?if($obj['nr']==1):?>
		<div id='mainNews'>
		<div style='height: 266px;'>
			<a href='<?=$obj['url']?>' class='mainNewsPhoto' title='<?=$obj['title']?>'><img src='<?=$obj["image"]?>' alt='<?=$obj['title']?>'></a>

			<?if($obj["exclusive"]):?><sup class="xExtra"><?=sys::translate('main::exclusive')?></sup><?endif;?>
			<?if($obj["spec_theme"]):?><sup class="supertopic">
				<a href='<?=$obj['spec_theme_url']?>'><?=sys::translate('main::spec_theme')?></a>
			</sup><?endif;?>

			<a href='<?=$obj['url']?>' class='mainNewsLink' title='<?=$obj['title']?>'><?=$obj['title']?></a>

			<p class='mainNewsText'>
				<?=$obj['intro']?>
			</p>

			<p class='mainNewsDate'>
				<span><?=$obj['date']?></span>
				<div id='newsComments'><?=$obj['comments']?></div>
				<div id='newsShows'><?=$obj['shows']?></div>
			</p>

		</div>

		</div>

		<?else:?>

		<?if($obj['nr']==2):?>
			<div id='secondNews'>
		<?endif;?>

		<?if($obj['nr']==4):?>
			<div id='secondNews'>
		<?endif;?>

		 <div class='newsItem'>
                <a href='<?=$obj['url']?>' class='newsItemPhoto' title='<?=$obj['title']?>'><img src='<?=$obj["image"]?>' alt='<?=$obj['title']?>'></a>
				
                <?if($obj["exclusive"]):?><sup class="xExtra"><?=sys::translate('main::exclusive')?></sup><?endif;?>
				<?if($obj["spec_theme"]):?><sup class="supertopic">
					<a href='<?=$obj['spec_theme_url']?>'><?=sys::translate('main::spec_theme')?></a>
				</sup><?endif;?>

				<a href='<?=$obj['url']?>' class='newsItemLink' title='<?=$obj['title']?>'><?=$obj['title']?></a>
				<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
				<p class='newsItemText'>
					<?=$obj['intro']?>
				</p>
			<p class='mainNewsDate'>
				<span><?=$obj['date']?></span>
				<div id='newsComments'><?=$obj['comments']?></div>
				<div id='newsShows'><?=$obj['shows']?></div>
			</p>


		</div>

		<?if($obj['nr']==3):?>
				<div id='lineBlank' style='height: 14px;'><img src='/images/blank.gif' height='1'></div>
		<?endif;?>



		<?if($obj['nr']==3 || $obj['nr']==5):?>

			</div>
		<?endif;?>


		<?endif;?>



	<?endforeach;?>

	<div class='hrThree'>&nbsp;</div>