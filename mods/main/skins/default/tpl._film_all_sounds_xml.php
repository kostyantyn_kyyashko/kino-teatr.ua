<?sys::setTpl('main::__xml')?>

<mp3gallery>

	<albums startAlbumNo = "1">

		<!-- if this album tag will not be here, the player will work as usual -->
        <album id="1">

			<author><![CDATA[<?=$var['film']?>]]></author>

			<image><?=$var["image"]?></image>

			<name alsoInList = "false"></name>
			<caption></caption>

			<tracks>


<?foreach ($var['objects'] as $obj):?>

				<item id="<?=$obj['i']?>">
					<title><![CDATA[<?=$obj['name']?>]]></title>
					<song><?=$obj['file']?></song>
				</item>
<?endforeach;?>


			</tracks>
		</album>




	</albums>
</mp3gallery>
