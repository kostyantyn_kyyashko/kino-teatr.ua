<?=sys::useLib('main::films');?>
<? $ratingSpecialFormat = round($var['rating'],1);?>
<script type="text/javascript">
function loadRating(rating)
{
	$('div.markForm img.star').each(
		function(){
			var mark=Math.round($(this).attr('num'));
			var rest=rating-Math.floor(rating);
			if(mark<=Math.floor(rating))
			{
				$(this).attr('src','<?=_IMG_URL.'star.png'?>');
			}
			else if(mark==Math.floor(rating)+1 || (rating<1 && mark==1))
			{
				if(rest<0.25)
					$(this).attr('src','<?=_IMG_URL.'star0.png'?>');
				if(rest>=0.25)
					$(this).attr('src','<?=_IMG_URL.'star1.png'?>');
				if(rest>=0.5)
					$(this).attr('src','<?=_IMG_URL.'star2.png'?>');
				if(rest>=0.75)
					$(this).attr('src','<?=_IMG_URL.'star3.png'?>');
			}
			else
				$(this).attr('src','<?=_IMG_URL.'star0.png'?>');
		}
	)
}
$(function(){

	$("div.markForm img.star").hover(
		function(){
			$(this).css('opacity','0.3');
			var num=Math.round($(this).attr('num'));
			$('div.markForm img.star').each(
				function()
				{
					if(Math.round($(this).attr('num'))<num)
						$(this).css('opacity','0.3');
				}
			)
		},
		function(){
			$(this).css('opacity','1');
			var num=Math.round($(this).attr('num'));
			$('div.markForm img.star').each(
				function()
				{
					if(Math.round($(this).attr('num'))<num)
						$(this).css('opacity','1');
				}
			)
		}
	)

	<?//if($_user['id']!=2):?>
		$("div.markForm a.vote").click(
			function(){
				var mark=$(this).attr('mark');
				$.ajax({
					url:'?mod=main&act=ajx_vote_<?=$var['type']?>',
					data:{
						mark:mark,
						object_id:<?=$var['object_id']?>
					},
					beforeSend:function(){
						$('#rating').html('...');
						$('#imdbRating').html('...');
					},
					dataType:'json',
					success:function(data)
					{
						$('div.markForm span.yourMark').html(mark);
						if(data)
						{
							$('#rating').html(data['rating']);
							$('#imdbRating').html(data['votes']);
							loadRating(data['rating']);
						}
					},
					type:'POST'
				})


				return false;
			}
		)
	<?//endif?>
})

</script>

					<div id='searchTitle'><?=sys::translate('main::'.$var['type'].'_rating')?></div>

						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>

	<div id="rating-block">
<span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
<div class="markForm">

	<?for($i=1; $i<11; $i++):?>
		<?
			if($i<=floor($var['rating']))
			{
				$src=_IMG_URL.'star.png';
			}
			elseif($i==floor($var['rating']+1) || ($var['rating']<1 && $i==1))
			{
				if($var['rest']<0.25)
					$src=_IMG_URL.'star0.png';
				if($var['rest']>=0.25)
					$src=_IMG_URL.'star1.png';
				if($var['rest']>=0.5)
					$src=_IMG_URL.'star2.png';
				if($var['rest']>=0.75)
					$src=_IMG_URL.'star3.png';
			}

			else
				$src=_IMG_URL.'star0.png';
		?>
		<a class="vote" mark=<?=$i?> href="<?=sys::rewriteUrl('?mod=main&act=login')?>"><img num="<?=($i)?>" class="star" src="<?=$src?>" alt=""></a>
	<?endfor?>
	
		<meta itemprop="ratingValue" content="<?=$ratingSpecialFormat?>">
	<span id="rating"><?=$var['rating']?>/<span itemprop="bestRating">10</span></span>

</div>

<span id='imdb'><?=sys::translate('main::votes')?>: </span>
					<span id='imdbRating' <?if($var['rating']>0):?>itemprop="ratingCount"<?endif;?>><?=$var['votes']?></span>
</span>
</div>


