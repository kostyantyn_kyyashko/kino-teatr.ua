<div class="xTitle"><h3 class="xDoc"><?=sys::translate('main::blog_about_cinema')?></h3></div>
<ul class="xTop2 xLinks1Nv2">
<?php
/**********************************************************
 * Parse XML data into an array structure                 *
 * Usage: array parse_rss ( string data )                 *
 **********************************************************/
function parse_rss($reg_exp, $xml_data) {
    preg_match_all($reg_exp, $xml_data, $temp);
    return array(
        'count'=>count($temp[0]),
        'title'=>$temp[1],
        'link'=>$temp[2],
        'desc'=>$temp[3]
    );
}

/**********************************************************
 * Parse Array data into an HTML structure                *
 * Usage: string parse_rss ( array data )                 *
 **********************************************************/
function output_rss($pattern, $rss_data) {
    //$count = $rss_data['count']; //количество = количеству в ленте
    $count = 3; //количество новостей
    for($i=0; $i<$count; $i++) {
        $temp .= sprintf($pattern,
            $rss_data['link'][$i],
            html_entity_decode($rss_data['title'][$i]),
            html_entity_decode($rss_data['desc'][$i])
        );
    }
    return $temp;
}

/**********************************************************
 * Settings                                               *
 **********************************************************/
$url = 'http://blog.kino-teatr.ua/feed/';

$reg_exp  = '#<item>.*?<title>(.*?)<\/title>.*?';
$reg_exp .='<link>(.*?)<\/link>.*?<description>';
$reg_exp .='(.*?)<\/description>.*?<\/item>#si';

$pattern = '<li><a href="%s" target="_blank">%s</a><br>%s';

/**********************************************************
 * Main script                                            *
 **********************************************************/
if ( $xml_data = file_get_contents($url) ) {
    $rss_data = parse_rss($reg_exp, $xml_data);
    echo output_rss($pattern, $rss_data);
}
/**********************************************************
 * The END                                                *
 **********************************************************/
?>
</ul>
