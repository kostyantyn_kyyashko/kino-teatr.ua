					<div id='searchTitle'><?=sys::translate('main::other_films')?></div>
					<div id='lineHrWide'>&nbsp;</div>

						<div class='blockLidersPremiers' id='anotherFilms'>
							<ul class='lidersUl'>
								<?foreach ($var['objects'] as $obj):?>
									<li class='<?=$obj['class']?> anotherFilm'>
										<a href='<?=$obj['url']?>' class='liderImgLink' title="<?=$obj['alt']?>">
											<img src='<?=$obj['image']?>'>
										</a>
										<div class='liderName'><?=$obj['title']?></div>
										<ul>
											<?if($obj['isTrailer']=='1'):?><li class='trailerLi'><a href='<?=$obj['trailers_url']?>' title="<?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot; - <?=sys::translate('main::trailers')?>">Трейлер</a></li><?endif;?>
											<?if($obj['isShows']=='1'):?><li class='watchLi'><a href='<?=$obj['shows_url']?>' title="<?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot; - <?=sys::translate('main::shows')?>">Где посмотреть</a></li><?endif;?>
										</ul>
									</li>
									<?if($obj['clear']==1):?>
															<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
									<?endif;?>
								<?endforeach;?>
							</ul>
						</div>

						<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
