<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=5?>
<?$_var['right']=main_cinemas::showCinemaPhoto($_GET['cinema_id'], $var['cinema'])?>
<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>
<?//main::showGemius('nAfq8Yv.IyV9etYDFq3In6cVXmDsa7rXSUSm7zLtaYD.W7');?>
<div id="main_cinema_shows">
	<form method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
	<input type="hidden" name="_task">
	<?=main_cinemas::showCinemaTabs($_GET['cinema_id'],$var['cinema'],'halls');?>
	<?if($var['notice']):?>
		<div class="xInfo xContent">
			<?=$var['notice']?>
		</div>
	<?endif?>
	<?if($var['edit']):?>
		<input class="xRed" type="button" value="<?=sys::translate('main::edit_notice')?>">
		<br>
		<br>
	<?endif;?>
	<?if($var['edit']):?>
		<input type="button" onclick="window.location='<?=main_cinemas::getHallAddUrl($_GET['cinema_id'])?>'" class="xRed" value="<?=sys::translate('main::add_hall')?>">
		<br><br>
	<?endif?>
	<?if($var['ticket_url']):?>
		<a class="xLinks1Nv" href="<?=$var['ticket_url']?>"><?=sys::translate('main::order_ticket')?></a>
		<br><br>
	<?endif?>
	<?if($var['halls']):?>
		<?foreach ($var['halls'] as $hall):?>
			<div id="hall_<?=$hall['id']?>" class="xTitle">
				<?if($hall['title']):?>
					<h2><?=$hall['title']?></h2>
				<?endif?>
				<?if($hall['scheme_url']):?>
					<a onclick="window.open('<?=$hall['scheme_url']?>','','<?=sys::setWinParams('770','570')?>'); return false;" href="<?=$hall['scheme_url']?>">(<?=sys::translate('main::hall_scheme')?>)</a>
				<?endif;?>
			</div>
			<table class="x xTable">
				<tr>
					<th class="x"><?=sys::translate('main::time')?></th>
					<th class="x"><?=sys::translate('main::prices')?></th>
					<th class="x"><?=sys::translate('main::note')?></th>
				</tr>
				<?foreach ($hall['shows'] as $show):?>
					<tr>
						<td colspan="3" class="x show xLinks1">
							<?if($var['edit']):?>
								<div class="xButtons">
									<input onclick="window.location='<?=main_shows::getShowEditUrl($show['show_id'])?>'" class="xSilver" type="button" value="<?=sys::translate('main::edit')?>">
									<input class="xSilver" type="button" onclick="if(confirm('<?=sys::translate('main::are_you_sure')?>')){this.form.elements['_task'].value='deleteShow.<?=$show['show_id']?>';this.form.submit();}" class="xSilver" value="<?=sys::translate('main::delete')?>">
								</div>
							<?endif?>
							<strong><?=$show['begin']?>-<?=$show['end']?> <a href="<?=$show['film_url']?>"><?=$show['film']?></a></strong>
						</td>
					</tr>
					<?foreach ($show['times'] as $time):?>
						<tr>
							<td style="width:1px" class="x"><?=$time['time']?></td>
							<td style="width:1px" width="1" class="x"><nobr><?=$time['prices']?> <?=sys::translate('main::grn')?></nobr></td>
							<td style="width:100%;" class="x"><?=$time['note']?>&nbsp;</td>
						</tr>
					<?endforeach;;?>
				<?endforeach;?>
				<?if($var['edit']):?>
					<tr>
						<td style="text-align:right" colspan="3">
							<input onclick="window.location='<?=main_cinemas::getHallEditUrl($hall['id'])?>'" class="xRed" type="button" value="<?=sys::translate('main::edit_hall')?>">
							<input class="xRed" type="button" onclick="if(confirm('<?=sys::translate('main::are_you_sure')?>')){this.form.elements['_task'].value='deleteHall.<?=$hall['id']?>';this.form.submit();}" value="<?=sys::translate('main::delete_hall')?>">
							<input class="xRed" value="<?=sys::translate('main::move_up')?>" type="button" onclick="this.form.elements['_task'].value='moveHallUp.<?=$hall['id']?>';this.form.submit()">
							<input class="xRed" value="<?=sys::translate('main::move_down')?>" type="button" onclick="this.form.elements['_task'].value='moveHallDown.<?=$hall['id']?>';this.form.submit()">
						</td>
					</tr>
				<?endif?>
			</table>
			<br>
		<?endforeach;?>
	<?else:?>
		<center class="xErr"><?=sys::translate('main::no_shows')?></center>
	<?endif;?>
	</form>
</div>
