<? sys::useLib('main::spec_themes'); ?>

						<div id='articles' class='nomargin'>
							<div id='spletniTitle'>
								<?=sys::translate('main::gossip')?>
							</div>
							<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

						<div style='height: 220px;'>
<?foreach ($var['objects'] as $obj):?>
		<?$obj['spec_theme_url']=main_spec_themes::getArticleUrl($obj['spec_theme']);?>

                            <div id='articlesItem'>
	                       	<a href='<?=$obj['url']?>' class='newsItemPhoto'><img src='<?=$obj["image"]?>'></a>
							<?if($obj["exclusive"]):?><sup class="xExtra"><?=sys::translate('main::exclusive')?></sup><?endif;?>
							<?if($obj["spec_theme"]):?><sup class="supertopic">
								<a href='<?=$obj['spec_theme_url']?>'><?=sys::translate('main::spec_theme')?></a>
							</sup><?endif;?>
							<a href='<?=$obj['url']?>' class='newsItemLink' title='<?=$obj['title']?>'><?=$obj['title']?></a>

							<p class='mainNewsDate' style='margin-top: 9px; float: left;'>
								<span><?=$obj['date']?></span>
								<div id='newsShows'  style='margin-top: 9px; float: left;'><?=$obj['shows']?></div>
							</p>

								<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
							</div>
<?endforeach;?>
					</div>
<br>
							<div id="allSpletni">
										<a href="<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/main/<?else:?>ru/main/<?endif;?>gossip.phtml"><?=sys::translate('main::allGossip')?></a>
							</div>
						</div>


