<?$city['values']=main_countries::getCountryCitiesTitles(29)?>
<?$city['value']=$_cfg['main::city_id']?>
<?$city['input']='selectbox'?>
<?$city['title']=sys::translate('main::city')?>
<div class="calendar">
	<?=sys_form::parseField('city_id',$city,$var['html_params'])?>
	<table class="calendar x">
		<tr>
			<th class="x"><?=sys::translate('main::mon')?></th>
			<th class="x"><?=sys::translate('main::tue')?></th>
			<th class="x"><?=sys::translate('main::wed')?></th>
			<th class="x"><?=sys::translate('main::thu')?></th>
			<th class="x"><?=sys::translate('main::fri')?></th>
			<th class="x xRed"><?=sys::translate('main::sat')?></th>
			<th class="x xRed"><?=sys::translate('main::sun')?></th>
		</tr>
		<tr>
			<?$i=1;?>
			<?foreach ($var['objects'] as $key=>$obj):?>
				<?if($obj['today']) $class='today'; elseif(!$obj['url']) $class='empty'; else $class=false;?>
				<?if(($i+1)%7==0 || $i%7==0) $class2='off'; else $class2=false;?>
				<?if($obj['current']) $class3='xCurrent'; else $class3=false;?>
			
				<td class="x <?=$class?> <?=$class2?> <?=$class3?>">
					<?if($obj['url']):?>
						<a title="<?=$obj['alt']?>" onclick="window.location='<?=$obj['url']?>?city_id='+getChildByTagName(this.parentNode.parentNode.parentNode.parentNode.parentNode,'SELECT').value; return false;" href="<?=$obj['url']?>"><?=$obj['day']?></a>
					<?else:?>
						<?=$obj['day']?>
					<?endif;?>
				</td>
					<?if($i%7==0 && $i<35):?>
						</tr><tr>
					<?endif;?>
					<?$i++;?>
			<?endforeach;;?>
		</tr>
	</table>
</div>

