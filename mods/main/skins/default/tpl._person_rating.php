<?sys::useLib('content');?>
<?$_var['right']=main_persons::showPersonMedia($_GET['person_id'], $var['person'])?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=6?>
<?$_var['active_child_menu']=8?>
<?sys::setMeta($var['meta']);?>
<?//main::showGemius('nA5KT4xhM1992iwEznZjKadD.jfs5.uXSeF2xttiKYz.37');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>
<div id="main_person_rating">
	<?=main_persons::showPersonTabs($_GET['person_id'],$var['person'],'rating');?>
	<form method="GET" action="<?=sys::rewriteUrl('?mod=main&act=person_rating&person_id='.$_GET['person_id'].'&order_by='.$_GET['order_by'])?>">
		<input align="right" type="submit" class="xRed" value="<?=sys::translate('main::filter')?>">
		<b><?=sys::translate('main::age')?></b> <?=sys::translate('main::from2')?>
		<?=sys_form::parseField('age_min',$var['fields']['age_min'],'size="2" maxlength="2"')?>
		<?=sys::translate('main::to')?>
		<?=sys_form::parseField('age_max',$var['fields']['age_max'],'size="2" maxlength="2"')?>
		&nbsp;<b><?=sys::translate('main::sex')?></b>
		<?=sys_form::parseField('sex',$var['fields']['sex'])?>
	</form>
	<br>
	<table class="xTable x">
		<tr class="xLinks2">
			<th <?if($var['order_field']=='user'):?>style="font-weight:bold;"<?endif;?>>
				<nobr>
				<a href="<?=$var['user']['order_url']?>"><?=sys::translate('main::user')?></a>
				<?if($var['order_field']=='user'):?>
					<img src="<?=_IMG_URL?>arrow_<?=$var['order']?>.gif" title="<?=sys::translate('main::'.$var['order'])?>" alt="<?=sys::translate('main::'.$var['order'])?>">
				<?endif;?>
				</nobr>
			</th>
			<th <?if($var['order_field']=='mark'):?>style="font-weight:bold;"<?endif;?>>
				<nobr>
				<a href="<?=$var['mark']['order_url']?>"><?=sys::translate('main::mark')?></a>
				<?if($var['order_field']=='mark'):?>
					<img src="<?=_IMG_URL?>arrow_<?=$var['order']?>.gif" title="<?=sys::translate('main::'.$var['order'])?>" alt="<?=sys::translate('main::'.$var['order'])?>">
				<?endif;?>
				</nobr>
			</th>
		</tr>
		<?foreach ($var['objects'] as $obj):?>
			<tr>
				<td><a href="<?=$obj['url']?>"><?=$obj['user']?></a></td>
				<td><?=$obj['mark']?></td>
			</tr>
		<?endforeach;?>
	</table>
	<?=sys::parseTpl('main::pages',$var['pages'])?>
</div>
