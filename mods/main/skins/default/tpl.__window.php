<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title><?=htmlspecialchars($_meta_title)?></title>
	<meta name="keywords" content="<?=htmlspecialchars($_meta_keywords)?>">
	<meta name="DESCRIPTION" content="<?=htmlspecialchars($_meta_description)?>">	
	<?=$_meta_other?>
	<style type="text/css">
		@import url(<?=$skin_url?>css.default.css);
	</style>
	<!--[if IE]>
		<style type="text/css">
			@import url(<?=$skin_url?>css.ie.css);
		</style>
	<![endif]-->
    <?if(count($_js_include)):?>
        <?foreach($_js_include as $src):?>
            <script type="text/javascript" src="<?=$src?>"></script>
        <?endforeach;?>
    <?endif?>
	<?if($_js_header):?>
    	<script type="text/javascript">
    	<?=$_js_header?>
    	</script>
    <?endif;?>
    <?=$var['header']?>
</head>
<body style="text-align:center">
	<?=$var['main']?>
</body>
</html>
<?if($_js_footer):?>
	<script type="text/javascript">
	<?=$_js_footer?>
	</script>
<?endif;?>
<?if($_err):?>
<script type="text/javascript">
	alert("<?=sys::parseErr($_err)?>");
</script>
<?endif;?>