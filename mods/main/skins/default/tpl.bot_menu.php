<?sys::useLib('main::films');?>

		<div id='footerMenu'>
			<a href='<?=_ROOT_URL?>' id='logoFooter'>&nbsp;</a>

					<ul id="footerCinemas">
						<li><a href="https://twitter.com/Kino_teatr" target="_blank" rel="nofollow"><i class="ico-tw"></i>Twitter</a></li>
						<li><a href="https://www.facebook.com/pages/Kino-Teatrua/217273141643149" target="_blank" rel="nofollow"><i class="ico-fb"></i>Facebook</a></li>
						<li><a href="https://plus.google.com/101732602037174602806" target="_blank" rel='nofollow' rel="author"><i class="ico-google"></i>Google+</a></li>
						<!-- <li><a href="//vk.com/kino_teatr_ua" target="_blank" rel="nofollow"><i class="ico-vk"></i><?=sys::translate('main::vkontakte')?></a></li> -->
						<li><a href="https://www.youtube.com/channel/UClbZ5MwDwj3tmBP-k8cy2Vw" target="_blank" rel="nofollow"><i class="ico-yt"></i>YouTube</a></li>
					</ul>
					
</ul>
<ul id="footerSocial">
						<li><a href="<?=_ROOT_URL?>"><i class="ico-rss"></i><?=sys::translate('main::all_rss_canal')?>, XML</a></li>
						<i><a href="http://kino-teatr.ua:8081/services/" target="_blank"><i class="ico-api"></i>API</a></li>
						<li><a href="<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/user_profile_delivery.phtml" target="_blank" rel="nofollow"><i class="ico-mail"></i>E-mail <?=sys::translate('main::delivery')?></a></li>
						<li><a href="<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/page/id/46.phtml"><i class="ico-smartphone"></i><?=sys::translate('main::mobileapps')?></a></li>
</ul>					
<ul id="footerSocial">
<li><a href="<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/page/id/54.phtml">&nbsp;<?=sys::translate('main::contacts')?></a></li>
<li><a href="<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/feedback.phtml">&nbsp;<?=sys::translate('main::feedback_letter')?></a></li>
<li><a href="<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/page/id/54.phtml">&nbsp;<?=sys::translate('main::adLink')?></a></li>
<li><a href="<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/page/id/55.phtml">&nbsp;<?=sys::translate('main::regulations')?></a></li>
<li><a href="<?=_ROOT_URL?><?if($_cfg['sys::lang']=='uk'):?>uk/<?else:?>ru/<?endif;?>main/page/id/57.phtml">&nbsp;<?=sys::translate('main::privacy_policy')?></a></li>

</div>

