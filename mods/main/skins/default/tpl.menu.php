<?$i=0?>
<ul id="menu">
	<?foreach ($var['objects'] as $obj):?>
		<?if($i==count($var['objects'])-1) $class='last'; else $class=false;?>
		<?if($obj['id']==$var['active_id']):?>
			<li class="active <?=$class?>"><a href="<?=$obj['url']?>" title="<?=$obj['alt']?>"><?=$obj['title']?></a></li>
		<?else:?>
			<li class="<?=$class?>"><a href="<?=$obj['url']?>" title="<?=$obj['alt']?>"><?=$obj['title']?></a></li>
		<?endif?>
		<?$i++;?>
	<?endforeach;?>
</ul>