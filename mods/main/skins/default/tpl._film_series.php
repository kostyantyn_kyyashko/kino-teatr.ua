<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_child_menu']=7?>
<?$_var['active_menu']=6?>
<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

	<div class='myriadFilm' id='myriad'><?=$var['title']?></div>
	<div id='lineHrWide'>&nbsp;</div>

	<?=main_films::showfilmTabs($_GET['film_id'],$var['film'],'series');?>

	<div id='lineBlank' style='height: 25px;'><img src='/images/blank.gif' height='1'></div>
	<div id='lineHrWide'>&nbsp;</div>
	<br>
	<?if(sys::checkAccess('main::films_tracking')):?>
	<form style="text-align:left; margin-bottom:10px;" action="<?=$_SERVER['REQUEST_URI']?>" method="POST">
		<?if($var['tracking']):?>
			<input title="<?=sys::translate('main::untracking_film')?>" type="submit" class="xRed" name="cmd_untracking" value="<?=sys::translate('main::untracking')?>">
		<?else:?>
			<input title="<?=sys::translate('main::tracking_film')?>" type="submit" class="xRed" name="cmd_tracking" value="<?=sys::translate('main::tracking')?>">
		<?endif;?>
	</form>
<?else:?>
<input title="<?=sys::translate('main::tracking_film')?>" type="button" class="xRed" value="<?=sys::translate('main::tracking')?>" onclick="window.location='<?=sys::rewriteUrl('?mod=main&act=login')?>'">
<?endif;?>

	<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
	<div id='lineHrWide'>&nbsp;</div>
	
	<div id='news_page'>
		<?foreach ($var['objects'] as $season_id=>$season):?>
			<div id="boxBlockTitle">
		      <div style="display: inline-block; float: left;">
		         Сезон <?=$season_id?>
		      </div>		
			  <div style="height: 1px;" id="lineBlank"><img height="1" src="/images/blank.gif"></div>
			</div>		
		<?
		//if(sys::isDebugIP())sys::printR($season);
		// ksort($season); 
		?>			
			<?foreach ($season as $id=>$seria):?>			
				<div id="boxBlockTop">&nbsp;</div>
				<div id="boxItem">
		  			<div id="boxDate" style="font-weight: normal;"><?=$seria["date"]?></div>
		  			<div id="boxDate"><?=($seria["seria"]>0?("Серия ".$seria["seria"]):"special")?></div>
					<div id="boxDate" style="font-weight: normal; width: 490px;"><?=$seria["title"]?></div>
					<div id="lineBlank"><img height="1" src="/images/blank.gif"></div>
				</div>
			<?endforeach;?>
		<?endforeach;?>
		</div>
	
		<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
		<?=banners::showBanners('v3_secondbanner')?>
	</div>
