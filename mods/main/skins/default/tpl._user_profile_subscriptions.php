<?sys::useLib('content');?>
<?$var['meta']['title']=$var['meta']['title'].' ('.$_user['main_nick_name'].')'?>
<?$var['meta']['meta_title']=$var['meta']['meta_title'].' ('.$_user['main_nick_name'].')'?>
<?$_var['main::title']=$var['meta']['title']?>
<?=sys::setMeta($var['meta']);?>
<?sys::useJs('sys::form');?>
<?sys::useJs('main::main')?>
<?//main::showGemius('Aqfg6yMPt6g.USAIu6ObTqbjTF3s9NACSZEke7LObCL.q7');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

<div class='myriad' id='myriad'><?=$_var['main::title']?></div>





					<div id='lineHrWide'>&nbsp;</div>

		<?=main_users::showProfileTabs('subscriptions')?>

<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>

<div class="user-profile-form">
	<form id="form" method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
		<input type="hidden" name="_task">
		<?if($var["films"]['objects']):?>
		<table class="xTable">
			
			<tr class="xLinks2">
				<th colspan="3" class="order" style="padding:0 20px;" width="100%">					
					<?=sys::translate('main::films_subscribe_user_profile_title')?>
				</th>
			</tr>

			<?foreach ($var["films"]['objects'] as $obj):?>
				<tr class="film">
                    <td style="font-weight:normal; border-left:0; padding-right:0; text-align:right; font-size:11px; color:#666; padding:0 0 0 20px; font-weight:bold;"><?=sys::translate('main::film')?></td>
                    <td style="width:100%;"><a title="<?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot;" href="<?=$obj['url']?>"><?=$obj['title']?></a></td>
                    <td style="border-left:0;"><input type="button" value="<?=sys::translate('main::untracking')?>" class="xSilver" onclick="this.form.elements['_task'].value='unTrack.<?=$obj['film_id']?>'; this.form.submit();"></td>
				</tr>
			<?endforeach;?>
		</table>		
		<?else:?>
			<center class="msg"><?=sys::translate('main::you_not_tracking_films')?></center>
		<?endif?>
	</form>
</div>

<div class="user-profile-form">
	<form id="form" method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
		<input type="hidden" name="_task">
		<?if($var["discuss"]['objects']):?>
		<table class="xTable x">

			<tr class="xLinks2">
				<th colspan="3" class="order" style="padding:0 20px;" width="100%">					
					<?=sys::translate('main::discuss_subscribe_user_profile_title')?>
				</th>
			</tr>

			<?foreach ($var["discuss"]['objects'] as $obj):?>
				<tr class="film">
                    <td style="font-weight:normal; border-left:0; padding-right:0; text-align:right; font-size:11px; color:#666; padding:0 0 0 20px; font-weight:bold;"><?=sys::translate('main::'.$obj['object_type'])?></td>
                    <td style="width:100%; border-left:0; text-align:left;"><a href="<?=$obj['url']?>"><?=$obj['title']?></a><? if($obj['film']) echo sprintf('<div style="color:#999; font-weight:normal;">%s</div>', $obj['film']); ?></td>
                    <td style="border-left:0;"><input type="button" value="<?=sys::translate('main::unsubscribe')?>" class="xSilver" onclick="this.form.elements['_task'].value='unSubscribe.<?=$obj['object_id']?>.<?=$obj['object_type']?>'; this.form.submit();"></td>
				</tr>
			<?endforeach;?>
		</table>
		<?else:?>
			<center class="msg"><?=sys::translate('main::you_not_subscribe_discuss')?></center>
		<?endif?>
	</form>
</div>

<div class="user-profile-form">
	<form id="form" method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
		<input type="hidden" name="_task">
		<?if($var["cinemas"]['objects']):?>
		<table class="xTable">
			
			<tr class="xLinks2">
				<th colspan="3" class="order" style="padding:0 20px;" width="100%">					
					<?=sys::translate('main::cinemas_subscribe_user_profile_title')?>
				</th>
			</tr>

			<?foreach ($var["cinemas"]['objects'] as $obj):?>
				<tr class="film">
                    <td style="font-weight:normal; border-left:0; padding-right:0; text-align:right; font-size:11px; color:#666; padding:0 0 0 20px; font-weight:bold;"><?=sys::translate('main::cinema')?></td>
                    <td style="width:100%;"><a title="<?=sys::translate('main::cinema')?> &quot;<?=$obj['title']?>&quot;" href="<?=$obj['url']?>"><?=$obj['title']?></a></td>
                    <td style="border-left:0;"><input type="button" value="<?=sys::translate('main::unsubscribe')?>" class="xSilver" onclick="this.form.elements['_task'].value='unSubscribeCinema.<?=$obj['cinema_id']?>'; this.form.submit();"></td>
				</tr>
			<?endforeach;?>
		</table>		
		<?else:?>
			<center class="msg"><?=sys::translate('main::you_not_subscribe_cinemas')?></center>
		<?endif?>
	</form>
</div>
