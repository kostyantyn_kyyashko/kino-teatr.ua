<?sys::setTpl('main::__rssfull');?>
<channel>
<rss2lj:owner xmlns:rss2lj="http://rss2lj.net/NS">kino_teatr_news</rss2lj:owner>
<title><?=sys::translate('main::title_rssfull')?></title>
<link><?=_ROOT_URL?></link>
<description><?=sys::parseModTpl('main::news_rss_description')?></description>
<language><?=$_cfg['sys::lang']?></language>
<pubDate><?=date('r')?></pubDate>

<generator>Kino-teatr.ua</generator>
<lastBuildDate><?=date('r')?></lastBuildDate>
<copyright>Copyright 2006-<?=date('Y')?>, Олег Бойко</copyright>
<category><?=sys::translate('main::portal_news')?></category>
<managingEditor>admin@kino-teatr.ua (Олег Бойко)</managingEditor>
<webMaster>admin@kino-teatr.ua (Олег Бойко)</webMaster>
<ttl>60</ttl>

<?foreach ($var['objects'] as $obj):?>
<item>
	<title><?=$obj['title']?></title>
	<link><?=$obj['url']?></link>
	<pubDate><?=$obj['date']?></pubDate>
	<dc:creator>Kino-Teatr.ua</dc:creator>
		<description><?=$obj['intro']?></description>
	<content:encoded><![CDATA[<?=$obj['text']?>]]></content:encoded>
		<?if($obj['image']):?>
		<enclosure url="<?=$obj['image_src']?>" type="<?=$obj['image_mime']?>" length="<?=$obj['image_size']?>"/>
	<?endif?>
	<guid isPermaLink="true"><?=$obj['url']?></guid>
	<categoty><?=$obj['category']?></categoty>
</item>
<?endforeach;?>
</channel>
