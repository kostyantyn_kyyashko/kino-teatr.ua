$(function() {

    var adp = $('.adserv_player');

    if (adp.size()) {

        adp.each(function(k) {

            $.ajax({
                url: $(this).data('adserv'),
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                dataType: 'xml',
                context: this,
                cache: false,
                success: function(r) {
                    xml = $(r);
                    adserv_wrapper = xml.find('VASTAdTagURI').text();
                    if (adserv_wrapper) {
                        adserv_impression_add = xml.find('Impression').text();
                        adserv_click_add = xml.find('ClickTracking').text();
                        $.ajax({
                            url: adserv_wrapper,
                            crossDomain: true,
                            xhrFields: {
                                withCredentials: true
                            },
                            dataType: 'xml',
                            context: this,
                            cache: false,
                            success: function(r) {
                                xml = $(r);
                                adserv_impression = xml.find('Impression').text();
                                adserv_click = xml.find('ClickThrough').text();
                                adserv_ad = xml.find('MediaFile:first-child').text();
                                console.log('wrapper: ' + adserv_wrapper);
                                console.log('impression-2: ' + adserv_impression);
                                console.log('impression_add-2: ' + adserv_impression_add);
                                console.log('click-2: ' + adserv_click);
                                console.log('click_add-2: ' + adserv_click_add);
                                console.log('ad-2: ' + adserv_ad);
                                if (adserv_click && adserv_ad && adserv_impression) {
                                    if ($(this).parent().css('position') == 'relative') {
                                        adserv_top = adserv_left = 0;
                                    } else {
                                        adserv_top = $(this).offset().top;
                                        adserv_left = $(this).offset().left;
                                    }
                                    $(this).before('<iframe id="adserv_iframe_' + k + '" class="adserv_wrapper" style="position: absolute; opacity: 0; cursor: pointer; background: #000; z-index: 999; top: ' + adserv_top + 'px; left: ' + adserv_left + 'px; width: ' + $(this).width() + 'px!important; height: ' + $(this).height() + 'px!important" width="' + $(this).width() + '" height="' + $(this).height() + '" src="https://vadideo.com/nakladka_v3/source.php?impression=' + adserv_impression + '&impression_add=' + adserv_impression_add + '&click=' + adserv_click + '&click_add=' + adserv_click_add + '&ad=' + adserv_ad + '&node=' + k + '" frameborder="0" scrolling="no" allowfullscreen></iframe>');
                                }
                                //impression
                                if (typeof adserv_impression !== "undefined" && adserv_impression !== '') {
                                    console.log('stat-1: ' + adserv_impression);
                                    $('body').append('<img style="display: none;" src="' + adserv_impression + '" alt="">');
                                    // $(new Image()).attr('src', adserv_impression).one('load', function() {});
                                }
                                if (typeof adserv_impression_add !== "undefined" && adserv_impression_add !== '') {
                                    console.log('stat-2: ' + adserv_impression_add);
                                    $('body').append('<img style="display: none;" src="' + adserv_impression_add + '" alt="">');
                                    // $(new Image()).attr('src', adserv_impression_add).one('load', function() {});
                                }
                            }
                        })
                    } else {
                        adserv_impression = xml.find('Impression').text();
                        adserv_click = xml.find('ClickThrough').text();
                        adserv_ad = xml.find('MediaFiles > MediaFile:first-child').text();
                        console.log('impression-1: ' + adserv_impression);
                        console.log('click-1: ' + adserv_click);
                        console.log('ad-1: ' + adserv_ad);
                        if (adserv_click && adserv_ad && adserv_impression) {
                            if ($(this).parent().css('position') == 'relative') {
                                adserv_top = adserv_left = 0;
                            } else {
                                adserv_top = $(this).offset().top;
                                adserv_left = $(this).offset().left;
                            }
                            $(this).before('<iframe id="adserv_iframe_' + k + '" class="adserv_wrapper" style="position: absolute; opacity: 0; cursor: pointer; background: #000; z-index: 999; top: ' + adserv_top + 'px; left: ' + adserv_left + 'px; width: ' + $(this).width() + 'px!important; height: ' + $(this).height() + 'px!important" width="' + $(this).width() + '" height="' + $(this).height() + '" src="https://vadideo.com/nakladka_v3/source.php?impression=' + adserv_impression + '&impression_add=&click=' + adserv_click + '&click_add=&ad=' + adserv_ad + '&node=' + k + '" frameborder="0" scrolling="no" allowfullscreen></iframe>');
                        }
                        //impression
                        if (typeof adserv_impression !== "undefined" && adserv_impression !== '') {
                            console.log('stat-1: ' + adserv_impression);
                            $('body').append('<img style="display: none;" src="' + adserv_impression + '" alt="">');
                            // $(new Image()).attr('src', adserv_impression).one('load', function() {});
                        }
                    }
                }
            });
        });

        setTimeout(function() {
            adp.each(function() {
                $(this).prev('.adserv_wrapper').offset({top: $(this).offset().top, left: $(this).offset().left})
            });
        }, 1000);

        $(window).resize(function() {
            adp.each(function() {
                $(this).prev('.adserv_wrapper').offset({top: $(this).offset().top, left: $(this).offset().left})
            });
        });

        $(window).scroll(function() {
            adp.each(function() {
                $(this).prev('.adserv_wrapper').offset({top: $(this).offset().top, left: $(this).offset().left})
            });
        });

        window.addEventListener('message', adserv_listener, false);
    }

});

function adserv_listener(e) {
    var elem = document.getElementById(e.data);
    if (elem) {
        if ($(elem).css('opacity') == 0) $(elem).css('opacity', '1');
        else elem.remove();
    } else $('body').append('<img style="display: none;" src="' + e.data + '" alt="">');
}
