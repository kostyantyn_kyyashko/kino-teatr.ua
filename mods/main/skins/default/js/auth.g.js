
var manualGoogleClick = false;

function GcheckLoginState() {
		manualGoogleClick = true;
	$("#___signin_0 button").trigger("click");		// -> GoogleOnSignInCallback
}	

function GoogleOnSignInCallback(resp) {	
//	console.log(resp);
	if(!manualGoogleClick) return;
	gapi.client.load('plus', 'v1', function(){
		gapi.client.plus.people.get({userId: 'me'}).execute(GpostDataLogin);		
		manualGoogleClick = false;
	});	
}

function GpostDataLogin(response)  
{
//	console.log(response);
	if(response.code) return;
	
	$.ajax(
	  {
	    type: 'POST',
	    dataType: 'json',
	    url: '?mod=main&act=ajx_auth_g',
	    data: 
	    {
	      response: response,
	      _task: "login"
	    },
	    success: function(data) 
	    {
	      if(data.success==true) 
	      {
	      	var path = window.location.pathname.split("/");
	      	if(path[path.length-1] == "login.phtml")
	      	{
	      		path[path.length-1] = "user_profile.phtml";
	      		path = path.join("/"); 
	      		window.location.href = path;
	      	}
	      	else
	      	{
	      	 window.location.reload();
	      	}
	      }
	      else
	      	alert(data.msg?data.msg:"Авторизация прошла неудачно!");			      	
	    }
	  }) // ajax  	
  }
    
  function GchangeLinked(id) {  	  
  	  if(id) {
		$.ajax(
		  {
		    type: 'POST',
		    dataType: 'json',
		    url: '?mod=main&act=ajx_auth_g',
		    data: 
		    {
		      id: id,
		      _task: "unlink"
		    },
		    success: function(data) 
		    {
  	      	  //window.location.reload();
		    }
		  }) // ajax  	  	  	
  	  } 
  }