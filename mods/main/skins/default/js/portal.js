function goPartner(sender, id) 
{
	$.ajax({
		url:'?mod=main&act=ajx_partner',
		type:'POST',
		async: false,
		data:{
			partner_id:id
		},
		beforeSend:function(){
		},
		dataType:'json',
		success:function(data)
		{
			if(data)
			{
				var href = $(sender).attr("href");
				
				$(sender)
				.attr("href", href+data['link'])
				.attr("target", "_blank");
				
				setTimeout(function(){
					$(sender).attr("href", href);
				}, 250);	
							
				return true;
			}
		}
	})
	return false;	
}