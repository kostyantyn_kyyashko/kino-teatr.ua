(function () {
    window.App = {
        ModalWindows: {
            modalLoginSelector: '#wg-modal-login',
            reloadPage: false
        },
        Actions: {},
        Pages: {}
    }
    $(document).ready(function () {
        App.init();
    });
    App.init = function () {
        $(document).on('click', '#wg-submit-login-form', App.ModalWindows.sendLoginForm);
        $(document).on('click', '#wg-login', {reloadPage: true}, App.ModalWindows.openLoginWindow);
        $(document).on('click', '#wg-submit-accept-code-form', App.ModalWindows.sendAcceptCodeForm);
        $(document).on('click', '#wg-another-mail', {selector: App.ModalWindows.modalLoginSelector}, App.ModalWindows.loadLoginForm);
        $(document).on('click', '#wg-receive-password', App.ModalWindows.sendAnotherPassword);
    },
        //Modal windows
        App.ModalWindows.openLoginWindow = function (reloadPage) {
            App.ModalWindows.reloadPage = reloadPage;
            $("#wg-modal-login").dialog({
                open: function (event, ui) {
                    App.ModalWindows.loadLoginForm(this.modalLoginSelector);
                },
                close: function () {
                    $(this).dialog('destroy')
                },
                resizable: false,
                height: 645,
                width: 480,
                modal: true
            });
            return false;
        },
        //adjust size of dialog window by content size
        App.ModalWindows.adjustWindowSize = function () {
            $('.wg-form-hint').click(function () {
                $(this).parent().slideUp();
                $('#wg-form-hint-text').slideDown();
                //$("#wg-modal-login").find("div:first").resize();
            });
            $('#wg-form-hint-text .button-back').click(function () {
                $('.wg-form-hint').parent().slideDown();
                $('#wg-form-hint-text').slideUp();
                //$("#wg-modal-login").find("div:first").resize();
            });
            $("#wg-modal-login").find("div:first").resize(function () {
                //adjust size of dialog window by content size
                var width = $(this).outerWidth(true);
                var height = $(this).outerHeight(true);
                $('#wg-modal-login').dialog("option", "width", width);
                $('#wg-modal-login').dialog("option", "height", height);
            });

            $("#wg-modal-login").find("div:first").resize();


            //exclude close button
            var close_button = $('#wg-modal-login').find(".close-button").appendTo($('#wg-modal-login').parent());

            //activate close button
            close_button.click(function () {
                $('#wg-modal-login').dialog("close");
            })

        },
        App.ModalWindows.sendLoginForm = function () {
            var email = $('#Users_email').val();
            $(this).addClass('wg-button-loader').addClass('disabled');
            $.ajax({
                url: AppBaseUrl + '/ajax/login',
                data: {'Users[verifyCode]': $('#Users_verifyCode').val(), 'Users[email]': email},
                type: "POST",
                success: function (html) {
                    $("#wg-modal-login").html(html);
                    $('#email').val(email);
                    App.ModalWindows.adjustWindowSize();
                }
            });
        },
        App.ModalWindows.sendAcceptCodeForm = function () {
            $(this).addClass('wg-button-loader').addClass('disabled');
            $.ajax({
                url: AppBaseUrl + '/ajax/AcceptCode',
                data: {'AcceptCode[secret_code]': $('#wg-secret-code').val(), 'AcceptCode[email]': $('#wg-email').val()},
                type: "POST",
                success: function (html) {
                    if (html.length > 20) {
                        $("#wg-modal-login").html(html);
                        App.ModalWindows.adjustWindowSize();
                    } else {
                        if (!App.ModalWindows.reloadPage) {
                            location.href = $('#wg-checkout').attr('data-checkout-url');
                        } else {
                           // location.reload();
                           $('#wg-modal-login').dialog("close");
                           $('.guestFooterLink').css({display: 'none'});
                           $('.loggedFooterLink').css({display: ''});
                        }
                    }
                }
            });
        },
        App.ModalWindows.loadLoginForm = function (selector) {
            if (typeof selector !== "string")
                selector = "#wg-modal-login";
            $.ajax({
                dataType: 'text',
                url: AppBaseUrl + '/ajax/Login',
                type: "POST",
                success: function (html) {
                    $('#wg-modal-login').html(html);
                    App.ModalWindows.adjustWindowSize();
                }
            });
        },

        App.ModalWindows.sendAnotherPassword = function () {
            $.ajax({
                data: 'anotherpass=1',
                url: AppBaseUrl + '/ajax/Login',
                type: "POST",
                success: function (html) {
                    $('#wg-modal-login').html(html);
                    App.ModalWindows.adjustWindowSize();
                }
            });
        },
        //Actions
        App.Actions.hideAjaxLoader = function () {
            $('.wg-ajax-loader').hide();
        },
        App.Actions.alert = function (msg) {
            $(".btn-ok").show();
            $(".btn-yes,.btn-no").hide();
            var modal = $('#alert-modal');
            if (typeof msg == "string") {
                modal.find('.modal-body').html(msg);
            } else {
                modal.find('.modal-body').html(msg.data);
            }
            modal.modal('show');
            return false;
        },
        App.Actions.confirm = function (msg, callback) {
            $(".btn-yes,.btn-no").show();
            $(".btn-ok").hide();
            var modal = $('#alert-modal');
            if (typeof msg == "string") {
                modal.find('.modal-body').html(msg);
            } else {
                modal.find('.modal-body').html(msg.data);
            }
            modal.modal('show');
            modal.on('click', '.btn-yes', callback);
            return false;
        },
        App.Actions.checkLogin = function () {
            var self = $(this);
            $.ajax({
                url: AppBaseUrl + '/ajax/checkLogin?rnd=' + Math.random() ,
                type: "GET",
                dataType: "json",
                success: function (json) {
                    if (json.success === true) {
                        location.href = self.attr('data-checkout-url');
                    } else if (json.success == 'nope') {
                        alert('Покупайте билеты только на ТестОперетту!');
                    }
                    else {
                        App.ModalWindows.openLoginWindow(false);
                    }
                }
            });
        },
        App.Actions.confirmClearBasket = function () {
            var $basketCountItems = $('#basket-count-items'),
                self = $(this);
            if ($basketCountItems.length && $basketCountItems.val() > 0) {
                if (confirm("Вы уходите? Мы удаляем ваши билеты")) {
                    $.ajax({
                        url: AppBaseUrl + '/rootserver/basketClean',
                        type: "GET",
                        data: {
                            sessionId: basketSessionId,
                            rnd: Math.random()
                        },
                        dataType: "json",
                        success: function (json) {
                            location.href = self.attr('href');
                        }
                    });
                    return false;
                } else {
                    return false;
                }
            }
        },

        App.Timer = function (time, minutes, element, onFinished) {
            var obj = {
                element: element,
                time: time,
                minutes: minutes,
                on_finished: onFinished,

                draw: function () {
                    var currentTime = this.getTime();

                    var r_time = ( this.start_time + this.time) - currentTime;

                    if (r_time < 0) {
                        clearInterval(this.interval);

                        if (typeof onFinished != 'undefined') {
                            onFinished.call(this);
                        }

                        return;
                    }

                    if (this.minutes) {
                        var sec = r_time % 60;
                        r_time = Math.floor(r_time / 60);
                        if (sec < 10) {
                            sec = "0" + sec;
                        }
                        var min = r_time % 60;
                        r_time = Math.floor(r_time / 60);

                        element.html(min + ':' + sec);
                    }
                    else {
                        element.html(r_time);
                    }
                },

                getTime: function () {
                    var d = new Date();
                    return Math.floor(d.getTime() / 1000);
                },

                clear: function () {
                    clearInterval(this.interval);
                },

                refresh: function () {
                    this.start_time = obj.getTime();
                }
            };

            obj.start_time = obj.getTime();

            obj.interval = window.setInterval(function () {
                obj.draw()
            }, 250);

            return obj;
        },
        
        App.statInfoShow = function(id){
        	$('#mainContent').css({display: 'none'});
        	$('.statContent').css({display: 'none'});
        	$('#' + id + 'Content').css({display: ''});
        },
        App.statInfoClose = function(id){
        	
        	$('#mainContent').css({display: ''});
        	$('.statContent').css({display: 'none'});
        }
}());
