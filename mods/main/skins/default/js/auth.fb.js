  function FBpostDataLogin(response)  
{
	$.ajax(
	  {
	    type: 'POST',
	    dataType: 'json',
	    url: '?mod=main&act=ajx_auth_fb',
	    data: 
	    {
	      id: response.authResponse.userID,
	      token: response.authResponse.accessToken,
	      _task: "login"
	    },
	    success: function(data) 
	    {
	      if(data.success==true) 
	      {
	      	var path = window.location.pathname.split("/");
	      	if(path[path.length-1] == "login.phtml")
	      	{
	      		path[path.length-1] = "user_profile.phtml";
	      		path = path.join("/");
	      		window.location.href = path;
	      	}
	      	else
	      	 window.location.reload();
	      }
	      else
	      	alert(data.msg?data.msg:"Авторизация прошла неудачно!");			      	
	    }
	  }) // ajax  	
  }

  function FBpostDataLinkLogin(response)  
  {
	$.ajax(
	  {
	    type: 'POST',
	    dataType: 'json',
	    url: '?mod=main&act=ajx_auth_fb',
	    data: 
	    {
	      id: response.authResponse.userID,
	      token: response.authResponse.accessToken,
	      _task: "link"
	    },
	    success: function(data) 
	    {
      	  window.location.reload();
	    }
	  }) // ajax  	
  }
  
  function FBstatusChangeCallback(response) 
  {  	
  	if(!response.status ||  (response.status!=="connected") ) 
  	{  		
		FB.login(function(response) {
		  if (response.status === 'connected') FBpostDataLogin(response);		  
		}, {scope: 'email,public_profile'});
  	}	
  	else if(response.status==="connected")	FBpostDataLogin(response);
  };

  function FBlinkSetCallback(response) 
  {  	
  	if(!response.status ||  (response.status!=="connected") ) 
  	{  		
		FB.login(function(response) {
		  if (response.status === 'connected') FBpostDataLinkLogin(response);		  
		}, {scope: 'email,picture,first_name,last_name,gender'});
  	}	
  	else if(response.status==="connected")	FBpostDataLinkLogin(response);
  };
  
  window.fbAsyncInit = function() 
  {
    	FB.init({
      		appId      : '314473142398393',
      		cookie     : true,
      		xfbml      : true,
      		version    : 'v2.11'
  		});
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/ru_RU/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));      

  function FBcheckLoginState() {  	  
	  FB.getLoginStatus(function(response) {
	    FBstatusChangeCallback(response);
	  });
  }	
   
  function FBchangeLinked(id) {  	  
  	  if(id) {
		$.ajax(
		  {
		    type: 'POST',
		    dataType: 'json',
		    url: '?mod=main&act=ajx_auth_fb',
		    data: 
		    {
		      id: id,
		      _task: "unlink"
		    },
		    success: function(data) 
		    {
  	      	  window.location.reload();
		    }
		  }) // ajax  	  	  	
  	  } else 
  	  {
		  FB.getLoginStatus(function(response) 
		  {
			  	FBlinkSetCallback(response);  
		  });  	   	
  	  }	
  }