var num_tickets = 0;
	var sum_basket = 0;
	var basket_content = '';
var type_submit = 'book';
	
	function show_eventmap(object,show_id){
		var url = $(object).attr('href');
		var date = $(object).closest('.filmShows').attr('data-date');
		if(typeof(date) != 'undefined' && date != ''){
			url += '&date='+date;
		}
		var current_url = encodeURIComponent(document.location.href);
		var container = '<div style="width: 50%; height: 50%;"><img src="/mods/main/skins/default/img/ajax-loader.gif" /></div>';
		
		$.ajax({
		type: "GET",
   	 	url: url+'&current_url='+current_url,
		dataType: 'json',
    	cache: false,
		error: function(){
			alert(arrErrNotices.loadError);
			$.colorbox.close();
		},
    	success: function(data){
			var places = data.places / 1.75;
			if(places < 500)
				places = 500;
			if(places > 1000)
				places = 1000;
			$.colorbox({
				html: data.html, 
				open: true, 
				opacity: 0.5, 
				width: places, 
				scrolling: false, 
				height: 750,
				onCleanup: function() {
					if($('#cboxLoadedContent').find('.content_basket').is(':visible')){
						var obj = $('#cboxLoadedContent').find('.clean');
						basket_clean_simple(obj);
					}

				}
			});
		}
    	});
		
	}
	
function show_unauthorized(object){
	
	var url = $(object).attr('href');
		
		$.ajax({
		type: "GET",
   	 	url: url,
    	cache: false,
		error: function(){
			alert(arrErrNotices.loadError);
			$.colorbox.close();
		},
    	success: function(data){

    		data += '<div id="social-login-div" style="height:50px;margin:10px 0;background:#d2151b;"></div>';
			$.colorbox({
				html: data, 
				open: true, 
				opacity: 0.5				
			});
			
			var clon = $("div#openApi").clone(true);
			clon.find("#uLogin").attr("id", "uLogin-eventmap").css({"width":"500px"});
			clon.appendTo("#social-login-div");
			clon.find("span:eq(0)").css({"font-size":"16px", "width":"120px"});
			clon.find("img:eq(0)").attr("src", "/ico/facebook32_2.png").css({"cursor":"pointer"});
			clon.find("img:eq(1)").attr("src", "/ico/google32_2.png").css({"cursor":"pointer"});
			clon.find("style").remove();
		}
    	});	
}
	
function ValidMail(email) {
    var re = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
    var valid = re.test(email);
	console.info('valid '+valid);
    if (valid == true) 
		return  '';
	return arrErrNotices.mail;
}
	
	 
function ValidPhone(phone) {
    var re = /^\d[\d\(\)\ -]{4,14}\d$/;
    var valid = re.test(phone);
    if (valid)
		return  '';
    else 
		return arrErrNotices.phone;
}


	function test_buying(object){
//		$(object).find('.container_buying').html(container_buying);
//		$(object).find('.btn_booking').removeAttr('disabled');
//		alert('122');
		$('#cboxLoadedContent').find('.price-range').hide();
		$('#cboxLoadedContent').find('.hall-image').hide();
		var hall_image = $(object).closest('#cboxLoadedContent').find('.hall-image').height();
		$('#cboxLoadedContent').find('.booking').show();
		$('#cboxLoadedContent').find('.booking form').show();
		$('#cboxLoadedContent').find('#wg-checkout').hide();
		$('#cboxLoadedContent').find('.checkout_info').hide();
		$('#cboxLoadedContent').find('.booking_result').show();
		$('#cboxLoadedContent').find('.booking_result').next().hide();
		var html = '<a hre="">Результат</a>';
		$('#cboxLoadedContent').find('.booking_result').html(html);
		$('#cboxLoadedContent').find('.payment_info').show();
	}

	function buying(object){
		
		var phone =$(object).find('.phone').val();
		var name =$(object).find('.name').val();
		var url = '/payment.php?method=PaymentCreate';
		var description =$(object).find('.description').val();
		var container_buying = $(object).find('.btn_buying').parent().html();
		var container = '<span><img src="/mods/main/skins/default/img/ajax-loader-small.gif" /></span>';

		if(phone == ''){
			alert(arrErrNotices.needPhone);
			return false;
		}
		var result = ValidPhone(phone);
		
		if(result != ''){
			alert(result);
			return false;
		}
		var params = $(object).serialize();
		name = encodeURIComponent(name);
		description = encodeURIComponent(description);
		var card_alias = $(object).closest('#cboxLoadedContent').find('.card_alias option:selected').val();

		
		$.ajax({
		type: "GET",
   	 	url: url+'&name='+name+'&description='+description+'&site_id='+siteId+'&card_alias='+card_alias+'&film_id='+film_id+'&hall_id='+hall_id+'&cinema_id='+cinema_id+'&event_id='+eventId+'&show_id='+showId+'&cinema_name='+cinema_name+'&session_id='+session_id+'&lang_id='+lang_id,
		dataType: 'json',
    	cache: false,
		beforeSend: function(){
			$(object).find('.container_buying').html(container);
			$(object).find('.btn_booking').attr('disabled','disabled');
		},
		error: function(){
			alert(arrErrNotices.loadError);
			$(object).find('.container_buying').html(container_buying);
			$(object).find('.btn_booking').removeAttr('disabled');
		},
    	success: function(data){
			if(typeof(data.error) != 'undefined' && data.error != ''){
				alert(data.error);
				$(object).find('.container_buying').html(container_buying);
				$(object).find('.btn_booking').removeAttr('disabled');
				return false;
			}
			if(data.type == 'otp'){
				$(object).find('.container_buying').html(container_buying);
				$(object).find('.btn_booking').removeAttr('disabled');
				$(object).closest('#cboxLoadedContent').find('.booking_result').show();
				$(object).closest('#cboxLoadedContent').find('.booking_result').next().hide();
				$(object).closest('#cboxLoadedContent').find('.booking_result').html(data.html);
				$(object).find('.payment_info').show();
			}
			else if(data.type == 'without_verification'){
//				alert(data.html);
				$(object).find('.container_buying').html(container_buying);
				$(object).find('.btn_booking').removeAttr('disabled');
				$('#cboxLoadedContent').find('.booking_result').show();
				$('#cboxLoadedContent').find('.booking_result').next().hide();
				$('#cboxLoadedContent').find('.booking_result').html(data.html);
				$(object).find('.payment_info').show();
				basket_clean_after_book();
				$('#cboxLoadedContent').find('.container_buying').html(container_buying);
//				$('#cboxLoadedContent').find('.container_otp').html(container_otp);
				$('#cboxLoadedContent').find('.container_button').html('<input type="button" value="'+arrErrNotices.cancel+'" align="right" onclick="cancel_payment(this,'+data.pmt_id+','+data.megakino_id+');return false;"/>');
			}
			else{
				three_ds_verification(data,object,container_buying);
			}

		}
    	});
	}
	
	function three_ds_verification(data,object,container_buying){
		
		var pareq = data.pareq;
		var content = '';
		content += '<form name="MPIform" action="'+data.form_url+'" method="POST" class="MPIform">';
    	content += '<input type="hidden" name="PaReq" value="'+pareq+'">';
    	content += '<input type="hidden" name="MD" value="'+data.md+'">';
    	content += '<input type="hidden" name="TermUrl" value="'+data.term_url+'">';
		content += '</form>';
		$(object).closest('#cboxLoadedContent').find('.container_3ds').html(content);
		$(object).closest('#cboxLoadedContent').find('.container_3ds form').submit();

	}
	
	function test_form(){
		content += '<form name="MPIform" action="https://ecommerce.liqpay.com/ecommerce/acs" method="POST" class="MPIform">';
    	content += '<input type="hidden" name="PaReq" value="124">';
    	content += '<input type="hidden" name="MD" value="YzBkNjJmMDctZGM1YS00NzYyLWJkYmUtMmFlNzAwNzM2YjNm">';
    	content += '<input type="hidden" name="TermUrl" value="http://test.kinoteatr.ua/payment.php?three_ds=1">';
		content += '</form>';
		var newWin = window.open("about:blank", "3DS-форма", "width=200,height=200");
		newWin.document.write(content);
		newWin.focus();
		newWin.document.forms[0].submit();

	}
	
	function set_type(object){
		var type_payment = $(object).find('option:selected').val();
		if(type_payment != 'cash'){
//			$(object).closest('form').find('#booking').val('Купить');
			$(object).closest('form').find('input[type="submit"]').val(arrErrNotices.puy);
		}
		else{
			$(object).closest('form').find('input[type="submit"]').val(arrErrNotices.toReserv);
		}
	}
	
	function set_submit(object,type){
		type_submit = type;
	}
		
	function otp_verification(object){
		var code = $(object).closest('.verification').find('.code').val();
		var token = $(object).closest('.verification').find('.token').val();
		var pmt_id = $(object).closest('.verification').find('.pmt_id').val();
		var invoice = $(object).closest('.verification').find('.invoice').val();
		var cinema_name = $(object).closest('.verification').find('.cinema_name').val();
		var film_id = $(object).closest('.verification').find('.film_id').val();
		var hall_id = $(object).closest('.verification').find('.hall_id').val();
		var cinema_id = $(object).closest('.verification').find('.cinema_id').val();
		var site_id = $(object).closest('.verification').find('.site_id').val();
		var container_buying = $(object).closest('#cboxLoadedContent').find('.btn_buying').parent().html();
		var container_otp = $(object).parent().html();
		var container = '<span><img src="/mods/main/skins/default/img/ajax-loader-small.gif" /></span>';
		
		if(code == ''){
			alert('Надо указать код!');
			return false;
		}
		
		var url = '/payment.php?method=Otp&code='+code+'&token='+token+'&pmt_id='+pmt_id+'&invoice='+invoice+'&cinema_name='+cinema_name;
		url += '&film_id='+film_id+'&hall_id='+hall_id+'&cinema_id='+cinema_id+'&site_id='+site_id+'&lang_id='+lang_id;

		$.ajax({
		type: "GET",
   	 	url: url,
		dataType: 'json',
    	cache: false,
		beforeSend: function(){
			$(object).find('.container_buying').html(container);
			$(object).closest('#cboxLoadedContent').find('.container_otp').html(container);
			$(object).closest('#cboxLoadedContent').find('.btn_booking').attr('disabled','disabled');
		},
		error: function(){
			alert(arrErrNotices.loadError);
			$(object).find('.container_buying').html(container_buying);
				$('#cboxLoadedContent').find('.container_otp').html(container_otp);
			$(object).find('.btn_booking').removeAttr('disabled');
		},
    	success: function(data){
			$('#cboxLoadedContent').find('.booking_result').show();
			$('#cboxLoadedContent').find('.booking_result').html(data.html);
			$(object).find('.container_buying').html(container_buying);
			$(object).find('.container_otp').html(container_otp);
			$(object).closest('#cboxLoadedContent').find('.btn_booking').removeAttr('disabled');
			if(typeof(data.status) != 'undefined' && data.status == 5){
				basket_clean_after_book();
				$(object).closest('#cboxLoadedContent').find('.container_buying').html(container_buying);
				$('#cboxLoadedContent').find('.container_otp').html(container_otp);
				$('#cboxLoadedContent').find('.container_button').html('<input type="button" value="'+arrErrNotices.cancel+'" align="right" onclick="cancel_payment(this,'+data.pmt_id+','+data.megakino_id+');return false;"/>');

			}
			else{
				alert(arrErrNotices.verificationError);
				$(object).closest('#cboxLoadedContent').find('.container_buying').html(container_buying);
				$('#cboxLoadedContent').find('.container_otp').html(container_otp);
				$(object).closest('#cboxLoadedContent').find('.btn_booking').removeAttr('disabled');
			}
			
		}
    	});
	}
	
	function booking(object){

		var url = $(object).attr('action');
		if(type_submit == 'buy'){
			buying(object);
			return false;
		}
		
		var email =$(object).find('.email').val();
		var phone =$(object).find('.phone').val();
		var description =$(object).find('.description').val();
		var name =$(object).find('.name').val();
		if(email == '' && phone == ''){
			alert(arrErrNotices.noticeNeedMailOrPhone);			
			return false;
		}
		
		if(email == '' && phone != ''){
			var result = ValidPhone(phone);
		}
		else if(phone == '' && email != ''){
			var result = ValidMail(email);
		}
		else if(phone != '' && email != ''){
			var result = '';
		}

		if(result != ''){
			alert(result);
			return false;
		}
		name = encodeURIComponent(name);
		description = encodeURIComponent(description);
		
		url += '&name='+name+'&description='+description+'&site_id='+siteId+'&cinema_name='+cinema_name+'&film_id='+film_id+'&hall_id='+hall_id;
		url += '&cinema_id='+cinema_id+'&event_id='+eventId+'&show_id='+showId+'&email='+email+'&phone='+phone+'&lang_id='+lang_id;

		
		$.ajax({
		type: "GET",
   	 	url: url,
		dataType: 'json',
    	cache: false,
		error: function(){
			alert(arrErrNotices.loadError);
		},
    	success: function(data){
			if(data.code == 0 && typeof(data.orderId) != 'undefined'){
				$(object).find('.container_button').html('<input type="button" value="Отменить" align="right" onclick="cancel_order(this,'+data.orderId+');return false;"/>');
				$(object).closest('#cboxLoadedContent').find('.booking_result').show();
				$(object).closest('#cboxLoadedContent').find('.booking_result').html(data.html);
				$(object).find('.payment_info').show();
				$(object).closest('#cboxLoadedContent').find('#wg-checkout').hide();
				basket_clean_after_book(object);
				
			}
			else{
				
			}
		}
    	});
		
		return false;
	}
	
	function show_booking(object){
		$(object).closest('#cboxLoadedContent').find('.price-range').hide();
		$(object).closest('#cboxLoadedContent').find('.hall-image').hide();
		var hall_image = $(object).closest('#cboxLoadedContent').find('.hall-image').height();
		$(object).closest('#cboxLoadedContent').find('.booking').show();
		$(object).closest('#cboxLoadedContent').find('.booking form').show();
		$(object).closest('#cboxLoadedContent').find('#wg-checkout').hide();
		$(object).closest('#cboxLoadedContent').find('.checkout_info').hide();
//		$(object).closest('#cboxLoadedContent').find('.booking').height(hall_image);

	}
	
	function cancel_booking(object){
		$(object).closest('#cboxLoadedContent').find('.price-range').show();
		$(object).closest('#cboxLoadedContent').find('.hall-image').show();
		$(object).closest('#cboxLoadedContent').find('.booking').hide();
		$(object).closest('#cboxLoadedContent').find('.booking_result').hide();
		$(object).closest('#cboxLoadedContent').find('.booking input[type="text"]').val('');
		$(object).closest('#cboxLoadedContent').find('.booking form').attr('action','/basket.php?method=basket-book');
		$(object).closest('#cboxLoadedContent').find('.booking .container_submit .btn_booking').val(arrErrNotices.toReserv);
		$(object).closest('#cboxLoadedContent').find('#wg-checkout').show();
	}
	
	function cancel_buying(object){
		$(object).closest('#cboxLoadedContent').find('.price-range').show();
		$(object).closest('#cboxLoadedContent').find('.hall-image').show();
		$(object).closest('#cboxLoadedContent').find('.booking').hide();
		$(object).closest('#cboxLoadedContent').find('.booking .booking_result').hide();
		$(object).closest('#cboxLoadedContent').find('.booking input').val('');
		$(object).closest('#cboxLoadedContent').find('.booking form').attr('action','/basket.php?method=basket-buy');
		$(object).closest('#cboxLoadedContent').find('.booking .container_submit .btn_buying').val(arrErrNotices.puy);
		$(object).closest('#cboxLoadedContent').find('#wg-checkout').show();
	}
	
	function cancel_order(object,orderId){
		
		$.ajax({
		type: "GET",
   	 	url: '/basket.php?method=cancel-order&orderId='+orderId+'&site_id='+siteId,
		dataType: 'json',
    	cache: false,
		error: function(){
			alert(arrErrNotices.loadError);
		},
    	success: function(data){
			if(data.code == 0){
				cancel_booking(object);
			}
		}
    	});
	}
	
	function cancel_otp(object){
//		console.info('133');
//		console.info($(object).closest('.booking-result').height());
		$(object).closest('#cboxLoadedContent').find('.booking').hide();
		$(object).closest('#cboxLoadedContent').find('.hall-image').show();
		$(object).closest('#cboxLoadedContent').find('#wg-checkout').show();
		$(object).closest('#cboxLoadedContent').find('.content_basket').show();
		$(object).closest('#cboxLoadedContent').find('.booking_result').html('');
		$(object).find('.payment_info').show();
//		cancel_buying(otp);
//		basket_clean_after_book();
	}
	
	function cancel_payment(object,pmt_id,megakino_id){
		
		$.ajax({
		type: "GET",
   	 	url: '/payment.php?method=PaymentCancel&pmt_id='+pmt_id+'&megakino_id='+megakino_id,
		dataType: 'json',
    	cache: false,
		error: function(){
			alert(arrErrNotices.loadError);
		},
    	success: function(data){
			if(data.status == 9){
				cancel_booking(object);
			}
		}
    	});
	}
	
	function cancel_order_from_profile(object){
		
		if(confirm(arrErrNotices.reservConfirm)){
			$.ajax({
			type: "GET",
   	 		url: object.href,
			dataType: 'json',
    		cache: false,
			error: function(){
				alert(arrErrNotices.loadError);
			},
    		success: function(data){
				if(data.code == 0)
					$(object).closest('tr').remove();
			}
    		});
		}
		
		
	}
	
	function cancel_payment_from_profile(object){
		
		var container = $(object).html();
		var ajax_loader = '<div><img src="/mods/main/skins/default/img/ajax-loader-small.gif" /></div>';

		if(confirm(arrErrNotices.cancelConfirm)){
			$.ajax({
			type: "GET",
   	 		url: object.href,
			dataType: 'json',
    		cache: false,
			beforeSend: function(){
				$(object).html(ajax_loader);
			},
			error: function(){
				alert(arrErrNotices.loadError);
			},
    		success: function(data){
				$(object).html(container);
				if(data.status == 9)
					$(object).closest('tr').remove();
				else
					alert(arrErrNotices.cantCancelPuy);
			}
    		});
		}
		
		
	}
	
	function refund(object,orderId){
		
		$.ajax({
		type: "GET",
   	 	url: '/basket.php?method=refund&orderId='+orderId,
		dataType: 'json',
    	cache: false,
		error: function(){
			alert(arrErrNotices.loadError);
		},
    	success: function(data){
			if(data.code == 0){
				cancel_buying(object);
			}
		}
    	});
	}
	
	function basket_clean_simple(){
		var method = 'basket-clean';
		var url = '/basket.php?site_id='+siteId+'&method='+method;
		$.ajax({
		type: "GET",
   	 	url: url,
    	cache: false,
		dataType: 'json',
    	success: function(data){
		
		}
    	});
	}
	
	function basket_clean_after_book(object){
		$('#cboxLoadedContent').find('.wg-cart-line').hide();
		$('#cboxLoadedContent').find('#wg-checkout').hide();
			$(".wg-places").each(function(){
        		$(this).removeClass('wg-places-status1');
				$(this).addClass('wg-places-status0');
				$(this).removeClass('wg-checked');
				$(this).attr('data-check',0);
				$(this).css('background-color',$(this).attr('data-color'));

      		});
		
	}
	
	function basket_clean(object){
		
		var method = 'basket-clean';
		var url = '/basket.php?site_id='+siteId+'&method='+method;
		if(confirm(arrErrNotices.areYouSure)){
			$.ajax({
		type: "GET",
   	 	url: url,
    	cache: false,
		dataType: 'json',
		error: function(){
			alert(arrErrNotices.loadError);
//			$.colorbox.close();
		},
    	success: function(data){
			if(data.code == 0 || data.code == 'null'){
				$(object).closest('#cboxLoadedContent').find('.wg-cart-line').hide();
				 $(".wg-places").each(function(){
        			$(this).removeClass('wg-places-status1');
					$(this).addClass('wg-places-status0');
					$(this).removeClass('wg-checked');
					$(this).attr('data-check',0);
					$(this).css('background-color',$(this).attr('data-color'));

      			});
				$(object).closest('#cboxLoadedContent').find('#wg-checkout').hide();

			}
			else{
				$(object).css('background-color',$(object).attr('data-color'));
				$(object).attr('data-check',0);
				$(object).removeClass('wg-checked');
				if(data.code == 18)
					alert(arrErrNotices.cantSelect);
				else if(data.code == 19)
					alert(arrErrNotices.missingInBasket);
				else if(data.code == 17)
					alert(arrErrNotices.alreadyInBasket);
			}
		}
    	});
		}
		
	}
	
	function remove_basket(object,place_id){
		
		$('#cboxLoadedContent .wg-place-'+place_id).removeClass('wg-places-status1');
		$('#cboxLoadedContent .wg-place-'+place_id).addClass('wg-places-status0');
		$('#cboxLoadedContent .wg-place-'+place_id).removeClass('wg-checked');
		$('#cboxLoadedContent .wg-place-'+place_id).attr('data-check',0);
		$('#cboxLoadedContent .wg-place-'+place_id).css('background-color',$('#cboxLoadedContent .wg-place-'+place_id).attr('data-color'));;
		
		var method = 'unlock';
		var url = '/basket.php?site_id='+siteId+'&event_id='+eventId+'&place_id='+place_id+'&method='+method+'&have_cards='+have_cards;
//		return false;
		
		$.ajax({
		type: "GET",
   	 	url: url,
    	cache: false,
		dataType: 'json',
		error: function(){
			alert(arrErrNotices.loadError);
//			$.colorbox.close();
		},
    	success: function(data){
			if(data.code == 0){
				$(object).closest('#cboxLoadedContent').find('.content_basket').html(data.basket);
			}
			else{
//				$(object).css('background-color',$(object).attr('data-color'));
				$(object).closest('#cboxLoadedContent').find('.wg-place-'+place_id).css('background-color',$(object).closest('#cboxLoadedContent').find('.wg-place-'+place_id).attr('data-color'));
				$(object).closest('#cboxLoadedContent').find('.wg-place-'+place_id).attr('data-check',0);
				$(object).closest('#cboxLoadedContent').find('.wg-place-'+place_id).removeClass('wg-checked');
				if(data.code == 18)
					alert(arrErrNotices.cantSelect);
				else if(data.code == 19)
					alert(arrErrNotices.missingInBasket);
				else if(data.code == 17)
					alert(arrErrNotices.alreadyInBasket);
			}
		}
    	});
	}
	
	
	
	function set_place(object){
		var color = $(object).css('background-color');
		var check = $(object).attr('data-check');
		var place_id = $(object).attr('data-id');

		if(check == 0){
			$(object).css('background-color','#5bb9d4');
			$(object).attr('data-check',1);
			$(object).addClass('wg-checked');
		}
		else{
			$(object).css('background-color',$(object).attr('data-color'));
			$(object).attr('data-check',0);
			$(object).removeClass('wg-checked');
		}
		var num_checked = $(object).closest('#cboxLoadedContent').find('.wg-checked').size();
//		console.info('num_checked '+num_checked);
		if(num_checked > 0){
			$(object).closest('#cboxLoadedContent').find('#wg-checkout').show();
			$(object).closest('#cboxLoadedContent').find('.content_basket').show();
		}
		else{
			$(object).closest('#cboxLoadedContent').find('#wg-checkout').hide();
			$(object).closest('#cboxLoadedContent').find('.content_basket').hide();
		}

		if(typeof(siteId) == 'undefined' || typeof(eventId) == 'undefined')
			return false;
			
		if(check == 0){
			var method = 'lock';
		}
		else{
			var method = 'unlock';
		}
		
		var url = '/basket.php?site_id='+siteId+'&event_id='+eventId+'&place_id='+place_id+'&method='+method+'&have_cards='+have_cards;
		
		$.ajax({
		type: "GET",
   	 	url: url,
    	cache: false,
		dataType: 'json',
		error: function(){
			alert(arrErrNotices.loadError);
//			$.colorbox.close();
		},
    	success: function(data){
			if(typeof(data.session_id) != 'undefined')
				session_id = data.session_id;
			if(data.code == 0){
				$(object).closest('#cboxLoadedContent').find('.wg-cart-line').show();
				$(object).closest('#cboxLoadedContent').find('.content_basket').html(data.basket);
			}
			else if(data.code == 18){
				$(object).css('background-color',$(object).attr('data-color'));
				$(object).attr('data-check',0);
				$(object).removeClass('wg-checked');
				alert(arrErrNotices.cantSelect);
			}
		}
    	});
	}
	
	function show_cart(object){
		var sum_prices = 0;
		
		$(object).closest('.wg-row-map').find('wg-places').each(function() {
			var price = $(this).attr('data-price');
			var row = $(this).attr('data-row');
			var number = $(this).attr('data-number');
		});
	}
	