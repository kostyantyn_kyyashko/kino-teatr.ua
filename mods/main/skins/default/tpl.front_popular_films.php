<?sys::useLib('main::boxw');?>
<?if (count($var['objects'])>0):?>

<div id='boxBlock'>

<div>
	<div id='boxBlockTitle'>
      <div style='display: inline-block; float: left;'>
         <?=sys::translate($var['caption'])?> <br><span class="boxorig">(<?=sys::translate('main::looking_films')?>)</span>
      </div>

	<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

	</div>

	<div id='boxBlockTop'>&nbsp;</div>


	<?$i=1;?>
	<?foreach ($var['objects'] as $obj):?>
	<div id='boxItem'>
  		<div id='boxPosition'><?=$i++?>.</div>
		<div id='boxName'><a href='<?=$obj['url']?>' title="<?=$obj['alt']?>"><?=$obj['title']?></a><br><?=$obj['title_orig']?></div>
		<div id='boxMoney'>
			<?=$obj['today_shows']?> / <?=$obj['total_shows']?>
		</div>
		<div id='lineBlank'><img src='/images/blank.gif' height='1'></div>
	</div>
	<?endforeach;?>

</div>



</div>

<?endif;?>