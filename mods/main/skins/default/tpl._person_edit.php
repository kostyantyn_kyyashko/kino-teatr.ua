<?sys::useLib('content');?>
<?$_var['main::title']=$var['page']['title']?>
<?$_var['active_menu']=content::getEntryNodeId(21)?>
<?sys::setMeta($var['page']);?>
<?//main::showGemius('BxXlPWesbcOpRUSgTXzXkYZiLcBRDab_s0kaAps_0Lv.j7');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?> 
<?sys::useJs('sys::form');?>
<?sys::jsInclude('sys::form');?>
<?sys::useJs('sys::gui');?>
<?sys::useJs('main::main')?>
<br>

<?
//var_dump($var);
?>
<div id="main_person_edit" class="user-profile-form">
	<form enctype="multipart/form-data" id="form" onsubmit="if(checkForm(this)){alert('<?=sys::parseModTpl('main::person_alert')?>');$('#form input[name=cmd_save]').css('display','none')}else{return false}" method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
	<table class="xTable" width="100%">
		<tr>
			<td colspan="2" style="text-align:center">
				<?=sys::parseModTpl('main::person_rules','html');?><br>
			</td>
		</tr>
			<th>&nbsp;<?=sys::translate('main::person')?>*:</th>
			<td><?=sys_form::parseField('name',$var['fields']['name'],'size="55" maxlength="255"');?></td>
		<tr>
		</tr>
		<tr>
			<th>&nbsp;<?=$var['fields']['lastname_orig']['title']?>*:</th>
			<td><?=sys_form::parseField('lastname_orig',$var['fields']['lastname_orig'],'size="55" maxlength="255"');?></td>
		</tr>
		<tr>
			<th>&nbsp;<?=$var['fields']['firstname_orig']['title']?>*:</th>
			<td><?=sys_form::parseField('firstname_orig',$var['fields']['firstname_orig'],'size="55" maxlength="255"');?></td>
		</tr>
		<tr>
			<th>&nbsp;<?=$var['fields']['lastname_ru']['title']?>*:</th>
			<td><?=sys_form::parseField('lastname_ru',$var['fields']['lastname_ru'],'size="55" maxlength="255"');?></td>
		</tr>
		<tr>
			<th>&nbsp;<?=$var['fields']['lastname_uk']['title']?>*:</th>
			<td><?=sys_form::parseField('lastname_uk',$var['fields']['lastname_uk'],'size="55" maxlength="255"');?></td>
		</tr>
		<tr>
			<th>&nbsp;<?=$var['fields']['firstname_ru']['title']?>*:</th>
			<td><?=sys_form::parseField('firstname_ru',$var['fields']['firstname_ru'],'size="55" maxlength="255"');?></td>
		</tr>
		<tr>
			<th>&nbsp;<?=$var['fields']['firstname_uk']['title']?>*:</th>
			<td><?=sys_form::parseField('firstname_uk',$var['fields']['firstname_uk'],'size="55" maxlength="255"');?></td>
		</tr>
		<tr>
			<th>&nbsp;<?=$var['fields']['birthdate']['title']?>*:</th>
			<td><?=sys_form::parseField('birthdate',$var['fields']['birthdate']);?></td>
		</tr>
		<tr>
			<th colspan="2">&nbsp;<?=$var['fields']['biography_ru']['title']?>:*</th>
		</tr>
		<tr>
			<td colspan="2">&nbsp;<?=sys_form::parseField('biography_ru',$var['fields']['biography_ru'],'style="width:100%; height:150px;"');?></td>
		</tr>
		<tr>
			<th colspan="2">&nbsp;<?=$var['fields']['biography_uk']['title']?>:*</th>
		</tr>
		<tr>
			<td colspan="2">&nbsp;<?=sys_form::parseField('biography_uk',$var['fields']['biography_uk'],'style="width:100%; height:150px;"');?></td>
		</tr>

		
		<? 		
		if(!isset($var['additional']) || !$var['additional']['have_portrait']) { ?>
		<tr>
			<th><?=sys::translate('main::portrait')?></th>
			<td>
				<div style="position:relative; z-index:11">
				<div>
				 <input size="55" type="file" title="<?=sys::translate('main::portrait')?>" name="photos[]"> 				 
				 </div></div>
			</td>
		</tr>
		<? } ?>
		
		<tr>
			<th><?=sys::translate('main::photos')?></th>
			<td>
				<div style="position:relative; z-index:11">
				<div>
				 <input size="55" type="file" title="<?=sys::translate('main::photos')?>" name="photos[]"> 
				 <img title="<?=sys::translate('main::add_photo')?>" alt="<?=sys::translate('main::add_photo')?>" onclick="addFilebox(this,'photos'); return false;" align="absmiddle" class="image" alt="" src="/mods/sys/backend/skins/default/img/btn.add.gif"></button>
				 </div></div>
			</td>
		</tr>
		
		<tr>
			<td colspan="2" style="text-align:center">
				<input class="xRed" type="submit" class="xButton" name="cmd_save" value="<?=sys::translate('main::save')?>">
				<input class="xRed" type="button" class="xButton" name="cmd_cancel" onClick="javascript:window.document.location.href='<?=main_films::getFilmUrl($_GET['film_id'])?>'" value="<?=sys::translate('main::cancel')?>">
			</td>
		</tr>
	</table>
	</form>
	
</div>