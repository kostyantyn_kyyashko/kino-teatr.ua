<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=5?>
<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

<script type="text/javascript">
function subscribeCinema(cinema_id, on_off)
{
  var f = $("#user_cinema_subscribe_form");
  f.find(":hidden[name='act']").val(on_off);
  f.find(":hidden[name='id_cinema']").val(cinema_id);
  f[0].submit();
  return false;
} 
</script>

<form method="POST" id="user_cinema_subscribe_form">
	<input type="hidden" name="_task" value="user_cinema_subscribe">
	<input type="hidden" name="act" value="1">
	<input type="hidden" name="id_cinema" value="0">	
</form>	



				<div class='myriadFilm' id='myriad'><?=sys::translate('main::cinema')?> <?=$var['title']?></div>

<?if($var['seo']):?>
<a href id='help' onclick='showHelp();return false;' style='margin-top: 4px;'>&nbsp;</a>
<?endif;?>

					<div id='lineHrWide'>&nbsp;</div>

	<?=main_cinemas::showCinemaTabs($_GET['cinema_id'],$var['title'],'info');?>

						<div id='lineBlank' style='height: 20px;'><img src='/images/blank.gif' height='1'></div>

					<div id='news_page'>

<?if($var['seo']):?>
						<!--input type="text" name="" value="Введите улицу на которой находитесь" id="searchStreet"-->
<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
<div id='helpDiv' style='display: none;'>
<?=$var['seo'];?>
</div>

<?endif;?>

						<div id="cinemaAddress">
							<span ><?=sys::translate('main::address')?>:</span> <?=sys::translate('main::cit')?><?=$var['city']?>, <?=$var['address']?>
							<br><span><?=sys::translate('main::phone')?>:</span>  <?=$var['phone']?>
							<?if ($var['flora']>0):?>
							<br><a href='//kinokiev.com' target='_blank' rel='nofollow'><strong><?=sys::translate('main::site')?></a></strong>
							<?else:?>
							
							<?if($var['site']):?>
							<br><a href='<?=$var['site']?>' target='_blank' rel='nofollow'><strong><?=sys::translate('main::site')?></strong></a>
							<?endif;?>
							
							<?endif;?>
							

							<? if($_user['id']!=2):?>
							<br>
							<? $user_cinema_subscribed = main_cinemas::getUserCinemaSubscribed($_user["id"], intval($_GET['cinema_id'])); ?>
								<a 	href='#' 
									title='<?=sys::translate($user_cinema_subscribed?'main::untracking':'main::tracking')?> <?=sys::translate('main::cinema_shows')?>' 
									id='btn_tckts' 
									onclick="{return subscribeCinema(<?=intval($_GET['cinema_id'])?>, <?=$user_cinema_subscribed?0:1?>);}"
									style="float:unset;"
								>
								<?=sys::translate($user_cinema_subscribed?'main::unsubscribe':'main::subscribe')?></a>		
									<?else:?>
	<a href='<?=sys::rewriteUrl('?mod=main&act=register')?>' title='<?=sys::translate($user_cinema_subscribed?'main::untracking':'main::tracking')?> <?=sys::translate('main::cinema_shows')?>' id='btn_tckts' style="float:unset;"><?=sys::translate('main::subscribe')?></a>		
							<? endif; ?>				
							
						</div>
						<div id="cinemabanner">
							<?=$var['banner']?>
						</div>
						<div id='lineBlank' style='height: 25px;'><img src='/images/blank.gif' height='1'></div>

<link href="//code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
function initialize() {
	myLatlng = new google.maps.LatLng(<?=(float) $var['latitude']?>,<?=(float) $var['longitude']?>);
  var myOptions = {
    zoom: 15,
    center: myLatlng,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }

  var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

  setMarkers(map, beaches);
}

var beaches = [
  ['<?=sys::translate('main::cit')?><?=$var['city']?>, <?=preg_replace("\n"," ",$var['address'])?>']
];

function setMarkers(map, locations) {
		var image = new google.maps.MarkerImage('<?=$skin_url?>images/marker.png',
	      new google.maps.Size(45, 67),
	      new google.maps.Point(0,0),
	      new google.maps.Point(23, 67));
		var shadow = new google.maps.MarkerImage('<?=$skin_url?>images/marker_shadow.png',
	      new google.maps.Size(54, 26),
	      new google.maps.Point(0,0),
	      new google.maps.Point(0, 26));
        var shape = {
		      coord: [1, 1, 1, 45, 45, 45, 45 , 1],
		      type: 'poly'
		  };
  for (var i = 0; i < locations.length; i++) {
    var beach = locations[i];

    var address = beach[0];

				myLatlng = new google.maps.LatLng(<?=(float) $var['latitude']?>,<?=(float) $var['longitude']?>);
        		var marker = new google.maps.Marker({
            		map: map,
            		shadow: shadow,
            		position: myLatlng,
			        icon: image,
			        shape: shape
        		});

  }
}
</script>
<div id="map_canvas" style="height:350px; width: 664px;"></div>
<script>
function showSeo()
{
	$('#seoBtn').css('display', 'none');
	$('#seofull').css('display', 'block');
}
</script>
						<div id='lineBlank' style='height: 25px;'><img src='/images/blank.gif' height='1'></div>

					<div id='searchTitle'><?=sys::translate('main::cinemaDescription')?></div>
					<div id='lineHrWide'>&nbsp;</div>
					<div id='filmText'>
						<?=$var['text']?>

<?if ($var['seosm']):?>
<div id='lineBlank' style='height: 15px;'><img src='/images/blank.gif' height='1'></div>
<div id='seosm'><?=$var['seosm']?> <?if($var['seofull']):?><a href='#' onclick='showSeo(); return false;' id='seoBtn'><?=sys::translate('main::more')?></a><?endif;?></div>
<?endif;?>

<?if ($var['seofull']):?>
<div id='lineBlank' style='height: 15px;'><img src='/images/blank.gif' height='1'></div>
<div id='seofull'><?=$var['seofull']?></div>
<?endif;?>


<?if(sys::checkAccess('main::cinemas')):?>

	<div id='lineBlank' style='height: 12px;'><img src='/images/blank.gif' height='1'></div>

	<a href onclick='$("#editor").toggle("fast"); return false;'>Редактировать</a>

	<div id='lineBlank' style='height: 12px;'><img src='/images/blank.gif' height='1'></div>

<script type="text/javascript" src="/jwysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="/jwysiwyg/controls/default.js"></script>
	<link rel="stylesheet" href="/jwysiwyg/jquery.wysiwyg.css" type="text/css" media="screen" charset="utf-8" />


	<style type="text/css" media="screen">
		#wysiwyg{ width:664px; height:300px; }
	</style>

<div id='editor'>



<form method='POST'>
<textarea id="wysiwyg" style='width:664px; height:300px;' name='editedtext'><?=$var['text']?></textarea>
<input type='submit' value='Сохранить'>
</form>

<script type="text/javascript">
$(function() {
    $('#wysiwyg').wysiwyg();
});
</script>

</div>

<script type="text/javascript">
$(function() {
    $('#editor').toggle();
});
</script>
<?endif;?>

					</div>

						<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
		
	<!-- Widgets -->
	<div style='float: left; margin-top: 20px; width: 107px !important; overflow-x: hidden;'><a href="//twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
 	</div>
	<div style='float: left; margin-top: 20px; margin-right: 10px;'><script src="//connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="" layout="button_count" show_faces="false" width="100" font=""></fb:like>
 	</div>
	<!-- END Widgets -->
	

						<div id='lineBlank' style='height: 15px;'><img src='/images/blank.gif' height='1'></div>

					<?=banners::showBanners('v3_secondbanner')?>

	<?=main_discuss::showDiscussMain($_GET['cinema_id'],'cinema',main_cinemas::getCinemaUrl($_GET['cinema_id']))?>

						<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>

					</div>

	<script>
		initialize();
	</script>



