<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=content::getEntryNodeId(22)?>
<?sys::setMeta($var['meta']);?>
<?//main::showGemius('nG3q8YvhIyTT7vxNlhUjTPWp7DPpeR.CbqDUiNDwlEb.A7');?>
<div id="main_interview_search">
	<form method="GET" action="<?=sys::rewriteUrl('?mod=main&act=interview_search');?>">
		<table class="xForm x">
			<tr>
				<td><?=sys_form::parseField('keywords',$var['fields']['keywords'],'style="width:100%" maxlength="55"')?></td>
				<td style="width:1px;"><input type="submit" class="xRed" value="<?=sys::translate('main::find')?>"></td>
			</tr>
		</table>
	</form>
	<br>
	<?if($var['objects']):?>
		<?foreach ($var['objects'] as $obj):?>
			<h2><a class="xLinks1" href="<?=$obj['url']?>"><?=$obj['title']?></a></h2>
			<p>...<?=$obj['intro']?>...</p>
		<?endforeach;?>
		<?=sys::parseTpl('main::pages',$var['pages'])?>
	<?elseif($_GET['keywords']):?>
		<center class="xErr"><?=sys::translate('main::nothing_found')?></center>
	<?endif?>
</div>