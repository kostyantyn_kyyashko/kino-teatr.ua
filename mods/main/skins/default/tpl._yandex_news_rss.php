<?sys::setTpl('main::__yrss');?>

<channel>

<title><?=sys::translate('main::title_rssfull')?></title>
<link><?=_ROOT_URL?></link>
<description><?=sys::parseModTpl('main::news_rss_description')?></description>

<yandex:logo>//kino-teatr.ua/images/icons/Logo_kino-teatr.png</yandex:logo>
<yandex:logo type="square">//kino-teatr.ua/images/icons/180.png</yandex:logo> 

<?foreach ($var['objects'] as $obj):?>
<item>
	<title><?=$obj['title']?></title>
	<link><?=$obj['url']?></link>
	<pubDate><?=$obj['date']?></pubDate>
	<yandex:full-text><?=$obj['text']?></yandex:full-text>
	<description><?=$obj['intro']?></description>
	<categoty><?=$obj['category']?></categoty>
	<author><?=$obj['login']?></author>

	<?/*if($obj['image']):
	<enclosure url="<?=$obj['image_src']?>" type="<?=$obj['image_mime']?>" />
	endif*/?>
</item>
<?endforeach;?>
</channel>

