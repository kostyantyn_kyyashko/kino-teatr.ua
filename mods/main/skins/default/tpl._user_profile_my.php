<?sys::useLib('content');?>
<?$var['meta']['title']=$var['meta']['title'].' ('.$_user['main_nick_name'].')'?>
<?$var['meta']['meta_title']=$var['meta']['meta_title'].' ('.$_user['main_nick_name'].')'?>
<?$_var['main::title']=$var['meta']['title']?>
<?=sys::setMeta($var['meta']);?>
<?sys::useJs('sys::form');?>
<?sys::useJs('main::main')?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

<div class='myriad' id='myriad'><?=$_var['main::title']?></div>


		<div id='lineHrWide'>&nbsp;</div>

		<?=main_users::showProfileTabs('my')?>

	 <div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
<br /><br />
<div id="main_user_profile_films" class="user-profile-form">
	<form id="form" method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
		<input type="hidden" name="_task">
		<?if($var['objects']):?>
		<?=sys::parseTpl('main::pages',$var['pages'])?>
		<table class="xTable">
			<tr class="xLinks2">
				<th colspan="2" class="order" style="padding:0 20px;" width="100%" <?if($var['order_field']=='title'):?>style="font-weight:bold;"<?endif;?>>
					<?if($var['order_field']=='title'):?>
						<div style="float:left;padding:0 5px 0 0;"><img src="<?=_IMG_URL?>sort-<?=$var['order']?>.png" title="<?=sys::translate('main::'.$var['order'])?>" alt="<?=sys::translate('main::'.$var['order'])?>"></div>
					<?endif;?>
					<span style="float:left; color:#666; font-size:9px;" ><?=sys::translate('main::title')?></span>
				</th>
			</tr>
			<?foreach ($var['objects'] as $obj):?>
				<tr class="film">
                    <td style="width:100%;"><a title="<?=sys::translate('main::film')?> &quot;<?=$obj['title']?>&quot;" href="<?=$obj['url']?>"><?=$obj['title']?></a></td>                    
				</tr>
			<?endforeach;?>
		</table>
		<?endif?>
	</form>
</div>
