<?
 //print "<hr>By Mike: /mods/main/skins/default/tpl._cinema_shows.php<p><pre>";
 //var_dump($var['objects']);
 //print "</pre><hr>";
?>
<script type="text/javascript" src="/mods/main/skins/default/js/jquery.colorbox.js"></script>
<link rel="stylesheet" type="text/css" href="/mods/main/skins/default/js/colorbox/colorbox.css" />
<script type="text/javascript" src="/mods/main/skins/default/js/eventmap.js"></script>

<?sys::useLib('content');?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=5?>
<?sys::setMeta($var['meta']);?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>
<?//main::showGemius('nAfq8Yv.IyV9etYDFq3In6cVXmDsa7rXSUSm7zLtaYD.W7');?>


<div class='myriadFilm' id='myriad'><?=$var['title']?></div>
<div id='lineHrWide'>&nbsp;</div>
<?=main_cinemas::showCinemaTabs($_GET['cinema_id'],$var['cinema'],'shows');?>
<div id='lineBlank' style='height: 14px;'><img src='/images/blank.gif' height='1'></div>

<?if($var['notice']):?>
	<div id="noticeDiv"><?=$var['notice']?></div>
	<div id='lineBlank' style='height: 14px;'><img src='/images/blank.gif' height='1'></div>
<?endif;?>

<script type="text/javascript">
	function ShowNote(id)
	{
//		$(".noteDiv").css("display","none");
		$("#"+id).toggle("slow");
		return false;
	}
</script>

<script type="text/javascript">
function subscribeCinema(cinema_id, on_off)
{
  var f = $("#user_cinema_subscribe_form");
  f.find(":hidden[name='act']").val(on_off);
  f.find(":hidden[name='id_cinema']").val(cinema_id);
  f[0].submit();
  return false;
} 
</script>

<form method="POST" id="user_cinema_subscribe_form">
	<input type="hidden" name="_task" value="user_cinema_subscribe">
	<input type="hidden" name="act" value="1">
	<input type="hidden" name="id_cinema" value="0">	
</form>	

<? if($_user['id']!=2):?>
	<div id="searchTitle"
		style="text-align:center;width:100%;">
		<a 	href='#' 
			title='<?=sys::translate($var["user_cinema_subscribed"]?'main::untracking':'main::tracking')?> <?=sys::translate('main::cinema_shows')?>' 
			id='btn_tckts' 
			onclick="{return subscribeCinema(<?=intval($_GET['cinema_id'])?>, <?=$var["user_cinema_subscribed"]?0:1?>);}"
			style="float:unset;display:inline-block;"
		>
		<?=sys::translate($var["user_cinema_subscribed"]?'main::unsubscribe':'main::subscribe')?></a>					    				
	</div>
	<?else:?>
		<div id="searchTitle"
		style="text-align:center;width:100%;">
	<a href='<?=sys::rewriteUrl('?mod=main&act=register')?>' title='<?=sys::translate($user_cinema_subscribed?'main::untracking':'main::tracking')?> <?=sys::translate('main::cinema_shows')?>' id='btn_tckts' style="float:unset;"><?=sys::translate('main::subscribe')?></a>		
	</div>
<? endif; ?>				


<?if($var['dates']):?>
	<script src="https://bilet.vkino.com.ua/extras/widget/1.7a/main.min.js"></script>
	<?foreach ($var['dates'] as $date_key => $films):?>

		<?if($date_key):?>
			<div id="searchTitle"
				style="text-align: center; width: 100%; font-size: 18px; color: #898989; background: #f4f4f4; margin-top: 15px; margin-bottom: 15px; line-height: 40px;">
				<?=sys::russianDate($date_key)?>
			    				
				<?if ($var['ticket_url']=='vkino'):?>
					<a 	href='#' 
						title='<?=sys::translate('main::buy_ticket')?>' 
						id='btn_tckts' 
						onclick="{alert('<?=sys::parseModTpl('main::buy_alert')?>');return false;}"
						style="margin-top:10px;margin-right:10px;"
					><?=sys::translate('main::buy_ticket')?></a>
		        <?endif;?>
			</div>
		<?endif?>

		<div id='afishaItemTitle'>
			<div class='filmName'><?=sys::translate('main::film')?></div>
			<div class='filmZal'><?=sys::translate('main::hall')?></div>
			<div class='filmShows' style='width: 257px;'><?=sys::translate('main::shows')?></div>
			<div class='filmShows' style='width: 50px;'>&nbsp;</div>
			<div class='filmPrices'><?=sys::translate('main::prices')?></div>
		</div>
		<?
			$saved_film_id = 0;
			$gray_style = 'style="background-color:#f4f4f4;"';
			$bgcolor = $gray_style;
		?>
		<?foreach ($films as $film_id=>$film):?>
		<?
			// массив с индексами для *
			$notes = array();
			foreach ($film['halls'] as $hall)
				foreach ($hall['times'] as $time)
				{
					$trim_note = $time["note"];
					if($trim_note && !in_array($trim_note, $notes)) $notes[] = $trim_note;
				}
			$note_div_id = "noteDiv_".preg_replace('/\./i',"",$date_key).$film_id;
		?>
		<div <?=$bgcolor?>>
		<?foreach ($film['halls'] as $hall_id=>$hall):?>
			<div id='afishaItem'>
				<div class='filmName'>
					<?if($saved_film_id == $film_id):?>
					&nbsp;
					<?else:?>
						<?$saved_hall_id = 0;?>
						<a href='<?=$film['film_url']?>' title="<?=sys::translate('main::film')?> <?=$film['film']?> <?=$film['year']?>"><?=$film['film']?></a>
				    <?endif;?>
				</div>
				<div class='filmZal'>
					<?if($saved_hall_id == $hall_id):?>
					&nbsp;
					<?else:?>
						<?if($hall['scheme_url']):?>
							<a title="<?=sys::translate('main::hall_scheme')?>" onclick="window.open('<?=$hall['scheme_url']?>','','<?=sys::setWinParams('770','570')?>'); return false;" href="<?=$hall['scheme_url']?>"><?=$hall['title']?></a>
						<?else:?>
							<?=$hall['title']?>
						<?endif;?>
					<?endif;?>
				</div>
				<div class='filmShows' style='width: 257px;' data-date="<?php echo $date_key;?>">
					<?
						$is_2d = 0;
						$has_3d = 0;
						$timesa = array();
//if(sys::isDebugIP()) 
//{	
//	if($film['film'] == "Лига справедливости")
//	{
//		sys::printR($hall['times']);
//	}
//}		

						foreach ($hall['times'][0] as $time)
						{
							//$time = $time[0];
	//if(sys::isDebugIP()) sys::printR($time);						
							$has_3d += $time['seans_3d'];
							$star_class = $time['past']?"mark_star_time_past":"mark_star_time";

							$note = array_search($time['note'],$notes,true);
							$note = ($note===FALSE)?"":'<sup class="'.$star_class.'"><a href=# onclick="return ShowNote(\''.$note_div_id.'\')">'.str_repeat("*",$note+1)."</a></sup>";

							$is_2d = ($film['film_3d'] && $hall['3d'] && !$time['seans_3d'])?"<sup class='mark3d'>2D</sup>":"";
							
							if($time['past']) $time['time'] = strip_tags($time['time']);
							$timesa[] = ("<!--".$time['show_id']."--><span class='".($time['past']?"timepast":"time")."'>".$time['time']."$is_2d</span>$note");
//							if(sys::isDebugIP()) sys::printR($time['time']);												
						}

						$timesar = implode('<span class="delimiter">, </span>', $timesa);
					?>
					<?=$timesar?>
				</div>
				<div class='filmShows' style='width: 50px;'>
						<?if($film['film_3d'] && $hall['3d'] && $has_3d):?>
						<center><img src='<?=$skin_url?>images/3dglass.jpg'></center>
						<?else:?>
						&nbsp;
						<?endif;?>
				</div>
				<div class='filmPrices'><?=$show['price_range']?><?=implode("-", $hall['price_range'])?> <?=sys::translate('main::grn')?></div>
			</div>
			<?
				$saved_film_id = $film_id;
				$saved_hall_id = $hall_id;
			?>
		<?endforeach; // of foreach ($film['halls'] as $hall_id=>$hall)?>

			<?if(sizeof($notes)):?>
					<div class="noteDiv" id='<?=$note_div_id?>'>
					<?
						$infotext = "<table width='100%'>";
						foreach($notes as $index=>$text)
						{
							$total_index = sizeof($notes);
							$infotext .= "<tr><td width='2%' nowrap valign=top>".str_repeat("*",$index+1)."</td><td>-&nbsp;$text</td></tr>";
						};
						$infotext .= "</table>";
						echo $infotext;
					?>
					</div>
			<?endif;?>

		</div>
		<? $bgcolor = ($bgcolor=="")?$gray_style:"";?>
		<?endforeach; // of foreach ($films as $film_id=>$film)?>
	<?endforeach; // of foreach ($var['dates'] as $date_key => $films)?>

					<div id='lineBlank' style='height: 15px;'><img src='/images/blank.gif' height='1'></div>
<?else:// of if($var['dates'])?>
	<center class="xErr"><?=sys::translate('main::no_shows')?></center>
<?endif;// of if($var['dates'])?>

<?=banners::showBanners('v3_secondbanner')?>


