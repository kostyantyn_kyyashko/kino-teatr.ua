<?sys::useLib('content');?>
<?//$_var['right']=main_films::showFilmMedia($_GET['film_id'], $var['film'])?>
<?$_var['main::title']=$var['meta']['title']?>
<?$_var['active_menu']=content::getEntryNodeId(19)?>
<?sys::setMeta($var['meta']);?>
<?//main::showGemius('AqhKsSMPM6ZfkWa2W9MYqseA7FS8ph.SiWnU6DL0lD7.I7');?>
<?ob_start()?>
<?require "right.inc";?>
<?$_var['banner_right']=ob_get_clean()?>

	<div class='myriadFilm' id='myriad'><?=($var["serial"]?sys::translate('main::serial_rating'):sys::translate('main::film_rating'))?> <?=$var['film']?></div>

<?if($var['seo']):?>
<a href id='help' onclick='showHelp();return false;'>&nbsp;</a>
<?endif;?>

	<div id='lineBlank' style='height: 5px;'><img src='/images/blank.gif' height='1'></div>


<?if($var['seo']):?>
<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
<div id='helpDiv' style='display: none;'>
<?=$var['seo'];?>
</div>

<?endif;?>


				<div id='lineBlank' style='height: 5px;'><img src='/images/blank.gif' height='1'></div>
<script type="text/javascript">
$(function(){
	$('a.blog_code').click(
		function()
		{
			$('textarea[name=forum_code]').css('display','none');
			if($('textarea[name=blog_code]').css('display')!='none')
				$('textarea[name=blog_code]').css('display','none');
			else
			{
				$('textarea[name=blog_code]').css('display','');
				$('textarea[name=blog_code]')[0].focus();
				$('textarea[name=blog_code]')[0].select();
			}
			return false;
		}
	);	
	
	$('a.forum_code').click(
		function()
		{
			$('textarea[name=blog_code]').css('display','none');
			if($('textarea[name=forum_code]').css('display')!='none')
				$('textarea[name=forum_code]').css('display','none');
			else
			{
				$('textarea[name=forum_code]').css('display','');
				$('textarea[name=forum_code]')[0].focus();
				$('textarea[name=forum_code]')[0].select();
			}
			return false;
		}
	);	

		$('a.another_code').click(
		function()
		{
			$('textarea[name=forum_code]').css('display','none');
			$('textarea[name=blog_code]').css('display','none');
			if($('textarea[name=another_code]').css('display')!='none')
				$('textarea[name=another_code]').css('display','none');
			else
			{
				$('textarea[name=another_code]').css('display','');
				$('textarea[name=another_code]')[0].focus();
				$('textarea[name=another_code]')[0].select();
				$('img[name=another_code]').css('display','');
			}
			return false;
		}
	);	
	
})
</script>				



<script type="text/javascript">
function showExpectNum(yes, no, value, film_id) {
	$("div[name='divExpectLine_"+film_id+"'] #waiting_yes").css({"width":yes+"%"}).html((yes>20)?yes:"");
	$("div[name='divExpectLine_"+film_id+"'] #waiting_no").css({"width":no+"%"}).html((no>20)?no:"");
	$("div[name='divExpectLine_"+film_id+"'], p[name='pExpectLineText_"+film_id+"'], ").show(500);
	
	if(value>0) value = "<?=sys::translate('main::yesExpect')?>";
	else if(value<0) value = "<?=sys::translate('main::noExpect')?>";
	else return; 

	$("span[name='spanExpectYourMark_"+film_id+"']").html(value);
	$("div[name='divExpectButtons_"+film_id+"']").hide(500);
	$("p[name='pExpectButtons_"+film_id+"']").empty().remove();
}


function update_values(film_id) {
	$.ajax({
		url:'?mod=main&act=ajx_film_rating_values',
		data:{
			object_id: film_id
		},
		dataType:'json',
		success:function(data)
		{					
			if(data)
			{
				//expect_no, expect_yes, pre_sum, pre_votes, sum, votes
				//showExpectNum(data.expect_yes, data.expect_no, null, film_id);				
				$($("#counter span span")[0]).text(data.pre_sum + " / " + data.pre_votes);
				$($("#counter span span")[1]).text(data.sum + " / " + data.votes);
				$("#counter img").attr("src","/rating_"+film_id+".gif?"+Math.random());
			}
		},
		type:'POST'
	});  	
	
	return false;	
}

function vote_mark(value, film_id){
	$.ajax({
		url:'?mod=main&act=ajx_vote_expect',
		data:{
			mark: value,
			object_id: film_id
		},
		dataType:'json',
		success:function(data)
		{					
			if(data)
			{
				showExpectNum(data.yes, data.no, value, film_id);
				update_values(film_id);
			}
		},
		type:'POST'
	});  	
	
	return false;
}

function loadRating(object_id, rating)
{
	$('div.markForm[object_id='+object_id+'] img.star').each(
		function(){
			var mark=Math.round($(this).attr('num'));
			var rest=rating-Math.floor(rating);
			if(mark<=Math.floor(rating))
			{
				$(this).attr('src','<?=_IMG_URL.'star.png'?>');
			}
			else if(mark==Math.floor(rating)+1 || (rating<1 && mark==1))
			{
				if(rest<0.25)
					$(this).attr('src','<?=_IMG_URL.'star0.png'?>');
				if(rest>=0.25)
					$(this).attr('src','<?=_IMG_URL.'star1.png'?>');
				if(rest>=0.5)
					$(this).attr('src','<?=_IMG_URL.'star2.png'?>');
				if(rest>=0.75)
					$(this).attr('src','<?=_IMG_URL.'star3.png'?>');
			}
			else
				$(this).attr('src','<?=_IMG_URL.'star0.png'?>');
		}
	)
}
$(function(){

	$("div.markForm img.star").hover(
		function(){
			var object_id=$(this).parent().parent().attr('object_id');
			$(this).css('opacity','0.3');
			var num=Math.round($(this).attr('num'));
			$('div.markForm[object_id='+object_id+'] img.star').each(
				function()
				{
					if(Math.round($(this).attr('num'))<num)
						$(this).css('opacity','0.3');
				}
			)
		},
		function(){
			var object_id=$(this).parent().parent().attr('object_id');
			$(this).css('opacity','1');
			var num=Math.round($(this).attr('num'));
			$('div.markForm[object_id='+object_id+'] img.star').each(
				function()
				{
					if(Math.round($(this).attr('num'))<num)
						$(this).css('opacity','1');
				}
			)
		}
	)

		$("div.markForm a.vote").click(
			function(){
				var mark=$(this).attr('mark');
				var object_id=$(this).parent().attr('object_id');
				$.ajax({
					url:'?mod=main&act=ajx_vote_film',
					data:{
						mark:mark,
						object_id:object_id
					},
					beforeSend:function(){
						$('#yourMark_'+object_id).html('...');
					},
					dataType:'json',
					success:function(data)
					{
						$('div.markForm[object_id='+object_id+'] span.yourMark').html(mark);
						if(data)
						{
							$('#yourMark_'+object_id).html(data['yourMark']);
							$('#span_rating_val_'+object_id).html(parseFloat(data['rating']).toFixed(0)).css({"font-weight":"bolder"});
							$('#span_rating_votes_'+object_id).html(data['votes']).css({"font-weight":"bolder"});
							
							loadRating(object_id, data['rating']);
							update_values(object_id);
						}
					},
					type:'POST'
				})


				return false;
			}
		)
})

</script>


<div id="main_film_rating">
	<?=main_films::showFilmTabs($_GET['film_id'],$var['film'],'rating');?>
	<div id='lineBlank' style='height: 5px;'><img src='/images/blank.gif' height='1'></div>
<br>
				<div id='searchTitle' style="white-space: nowrap;">
				<?=$var['page_views']?>
				</div>
    
<?//if(sys::isDebugIP()) {?>
				<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
				<div id='searchTitle'><?=sys::translate('main::'.$var["stars"]['form_title'])?> 
				&nbsp; | &nbsp; 
				<?=sys::translate('main::your_mark')?>: <span id="yourMark_<?=$var["stars"]['object_id']?>" class="yourMark"><?=$var["stars"]['yourMark']?></span>
				</div>

<?//sys::printR($var["stars"]);?>				
				
				
				<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
				<div id="rating-block">
						<div class="markForm" object_id="<?=$var["stars"]['object_id']?>">
				
						<?for($i=1; $i<11; $i++):?>
							<?
								if($i<=floor($var["stars"]['rating']))
								{
									$src=_IMG_URL.'star.png';
								}
								elseif($i==floor($var["stars"]['rating']+1) || ($var["stars"]['rating']<1 && $i==1))
								{
									if($var["stars"]['rest']<0.25)
										$src=_IMG_URL.'star0.png';
									if($var["stars"]['rest']>=0.25) 
										$src=_IMG_URL.'star1.png';
									if($var["stars"]['rest']>=0.5)
										$src=_IMG_URL.'star2.png';
									if($var["stars"]['rest']>=0.75)
										$src=_IMG_URL.'star3.png';
								}
					
								else
									$src=_IMG_URL.'star0.png';
							?>
							<a title="<?=sys::translate('main::rating')?> <?=$i?> / 10" class="vote" mark=<?=$i?> href=""><img num="<?=($i)?>" class="star" src="<?=$src?>" alt=""></a>
						<?endfor?>
				
						</div>
						<!-- start rating kino-teatr -->
								<span style="float:right; padding: 0 0 0 12px;">
		<nobr>
			<div style="width:100px;float:left;">До премьеры</div>:&nbsp;<span><?=$var['stat']['pre_sum']?> / <?=$var['stat']['pre_votes']?></span><br>
			<div style="width:100px;float:left;">После премьеры</div>:&nbsp;<span><?=$var['stat']['sum']?> / <?=$var['stat']['votes']?></span><br>
			<div style="width:100px;float:left;">Рецензенты</div>:&nbsp;<span><?=$var['stat']['reviewer_sum']?> / <?=$var['stat']['reviewer_votes']?></span>
		</nobr>
		</span>
		<img style="float:right;" alt="" src="/rating_<?=$_GET['film_id']?>.gif">
<!-- end rating kino-teatr -->
				</div>

			<?if(!$var["premieredZeroExpected"]){?>
				<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>

				<div id='searchTitle' style="white-space: nowrap;">
					<?=sys::translate('main::film_waiting')?>&nbsp; | &nbsp; <?=sys::translate('main::your_mark')?>: 
					<span id="yourExpect" name='spanExpectYourMark_<?=$var["expect"]['film_id']?>' class="yourMark"><?=$var["expect"]["yourMark"]?></span>
				</div>
				
				<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>

				<div class="markForm" name='divExpectLine_<?=$var["expect"]['film_id']?>' id="expect_lines" style="display:<?=(($var["expect"]['voted'] || $var["premiered"])?"block":"none")?>">
					<div id='waiting_bg'>
						<div id='waiting_yes' style="width:<?=$var["expect"]['yes']?>%"><?=(($var["expect"]['yes']>20)?$var["expect"]['yes']:"")?></div>
						<div id='waiting_no'  style="width:<?=$var["expect"]['no']?>%"><?=(($var["expect"]['no']>20)?$var["expect"]['no']:"")?></div>
					</div>
				</div>

				<div class="ranksListThumbs">
					<span class="thumbs-bad"></span>
					<span><?=$var['expect_no']?></span> 
				</div>
				
				<div class="ranksListThumbs">
					<span class="thumbs-good"></span>
					<span><?=$var['expect_yes']?></span>
				</div>	 
				
				<?if(!$var["expect"]["voted"] && !$var["premiered"]) { // buttons ?>											
					<p class='searchItemText' style="margin-top:0" name='pExpectButtons_<?=$var["expect"]['film_id']?>'>																					
						<div id='filmAwaiting' name='divExpectButtons_<?=$var["expect"]['film_id']?>'>
							<span id='doYou'><?=sys::translate('main::areYouWaiting')?></span>
							<ul id='awaitingUl'>
								<li><a class="yes" onclick="return vote_mark(1, '<?=$var["expect"]['film_id']?>');" href=""><span><?=sys::translate('main::yesLabel')?></span></a></li>
								<li><a class="no" onclick="return vote_mark(-1, '<?=$var["expect"]['film_id']?>');" href=""><span><?=sys::translate('main::noLabel')?></span></a></li>
							</ul>
						</div>
					</p>			
				<?}//endif buttons?>											

				
				<p class='searchItemText'  name='pExpectLineText_<?=$var["expect"]['film_id']?>' style="margin-top:0; display:<?=(!$var["expect"]['voted']?"none":"block")?>" >											
					<?=str_replace('"+yes+"', $var["expect"]['yes'], sys::translate('main::expecting'))?>
				</p>
			<?} // of if(!$var["premieredZeroExpected"])?>				
						
				<div id='lineBlank' style='height: 10px;'><img src='/images/blank.gif' height='1'></div>
<?//}?>
	
	<div id="counter">
<!-- start rating kino-teatr -->

<!-- end rating kino-teatr -->
		<span style="float:left; padding: 12px 0 10px 12px;">
		<b>
			&nbsp;&nbsp;&nbsp;<a class="blog_code" href="#"><?=sys::translate('main::counter_blog_code')?></a>
			&nbsp;&nbsp;&nbsp;<a class="forum_code" href="#"><?=sys::translate('main::counter_forum_code')?></a><br><br>
			&nbsp;&nbsp;&nbsp;<a class="another_code" href="#"><?=sys::translate('main::counter_another_code')?></a><br>
		</b>
		</span>
			
		<p>
		<br>
		<textarea readonly name="blog_code" style="width:660px; height:65px; display:none; border-style: dashed;"><a href="<?=main_films::getFilmUrl($_GET['film_id'])?>" title="<?=sys::translate('main::rating_of_film')?> <?=$var['film']?>"><img alt="<?=sys::translate('main::rating_of_film')?> <?=$var['film']?>" border="0" src="<?=_ROOT_URL?>rating_<?=$_GET['film_id']?>.gif"></a>
		</textarea>
		<textarea readonly name="forum_code" style="width:660px; height:65px; display:none; border-style: dashed;">[url=<?=main_films::getFilmUrl($_GET['film_id'])?>][img]<?=_ROOT_URL?>rating_<?=$_GET['film_id']?>.gif[/img][/url]</textarea>
		<div id='lineBlank' style='height: 1px;'><img src='/images/blank.gif' height='1'></div>
		
		<img alt="<?=sys::translate('main::rating_of_film')?> <?=$var['film']?>" name="another_code" border="0" style="float:left; display:none; padding: 12px  10px 12px 25px;" src="<?=_ROOT_URL?>rating03_<?=$_GET['film_id']?>.gif"> <textarea readonly name="another_code" style="float:right; width:500px; height:65px; display:none; border-style: dashed;"><a href="<?=main_films::getFilmUrl($_GET['film_id'])?>" title="<?=sys::translate('main::rating_of_film')?> <?=$var['film']?>"><img alt="<?=sys::translate('main::rating_of_film')?> <?=$var['film']?>" border="0" src="<?=_ROOT_URL?>rating03_<?=$_GET['film_id']?>.gif"></a></textarea>
		<div id='lineBlank' style='height: 5px;'><img src='/mods/main/skins/default/images/blank.gif' height='1'></div>
		<img alt="<?=sys::translate('main::rating_of_film')?> <?=$var['film']?>" name="another_code" border="0" style="float:left; display:none; padding: 12px  10px 12px 25px;" src="<?=_ROOT_URL?>rating02_<?=$_GET['film_id']?>.gif"> <textarea readonly name="another_code" style="float:right; width:500px; height:65px; display:none; border-style: dashed;"><a href="<?=main_films::getFilmUrl($_GET['film_id'])?>" title="<?=sys::translate('main::rating_of_film')?> <?=$var['film']?>"><img alt="<?=sys::translate('main::rating_of_film')?> <?=$var['film']?>" border="0" src="<?=_ROOT_URL?>rating02_<?=$_GET['film_id']?>.gif"></a></textarea>
	
	</div>
	<div id='lineBlank' style='height: 5px;'><img src='/images/blank.gif' height='1'></div>
	
	
	
	<form method="GET" action="<?=$var['form_action']?>">
	<div id='filmsFilter'>
		<?=sys::translate('main::age')?> <?=sys::translate('main::from2')?>
		<?=sys_form::parseField('age_min',$var['fields']['age_min'],'size="2" maxlength="2"')?>
		<?=sys::translate('main::to')?>
		<?=sys_form::parseField('age_max',$var['fields']['age_max'],'size="2" maxlength="2"')?>
		&nbsp; &nbsp; &nbsp; <?=sys::translate('main::sex')?>
		<?=sys_form::parseField('sex',$var['fields']['sex'])?>
		&nbsp; &nbsp; &nbsp; <?=sys_form::parseField('fan',$var['fields']['fan'])?> <?=sys::translate('main::genre_fan')?> 
		&nbsp; &nbsp; &nbsp; <input class="filterFind" type="submit" class="xRed" value="<?=sys::translate('main::filter')?>">		
	</div>	
	</form>
	
	
	
	<br>
	
	
				<div id='lineBlank' style='height: 5px;'><img src='/images/blank.gif' height='1'></div>
	<?if($var['objects']):?>
	
							<ul id='ranksList'>


							<li>
								<div id='ranksListTitle' style="width:100%;">
									<span style="float:left !important;">
										<a href="<?=$var['user']['order_url']?>" <?if($var['order_field']=='user'):?>style="font-weight:bold;"<?endif;?>><?=sys::translate('main::user')?></a></a> 
										<?if($var['order_field']=='user'):?>
											<img src="<?=_IMG_URL?>arrow_<?=$var['order']?>.gif" border="0" title="<?=sys::translate('main::'.$var['order'])?>" alt="<?=sys::translate('main::'.$var['order'])?>">
										<?endif;?>
									</span>
									
									
									<span style="float:right !important;margin-right: 15px;">
									<nobr>
										<a href="<?=$var['pre_mark']['order_url']?>" <?if($var['order_field']=='pre_mark'):?>style="font-weight:bold;"<?endif;?>><?=sys::translate('main::before')?></a> 
										<?if($var['order_field']=='pre_mark'):?>
											<img src="<?=_IMG_URL?>arrow_<?=$var['order']?>.gif" border="0" title="<?=sys::translate('main::'.$var['order'])?>" alt="<?=sys::translate('main::'.$var['order'])?>">
										<?endif;?>
										
										 &nbsp; / &nbsp;
										
										<a href="<?=$var['mark']['order_url']?>" <?if($var['order_field']=='mark'):?>style="font-weight:bold;"<?endif;?>><?=sys::translate('main::after')?></a>
										<?if($var['order_field']=='mark'):?>
											<img src="<?=_IMG_URL?>arrow_<?=$var['order']?>.gif" title="<?=sys::translate('main::'.$var['order'])?>" alt="<?=sys::translate('main::'.$var['order'])?>">
										<?endif;?>
									</nobr>
									</span>
								</div>

							</li>
			<?foreach ($var['objects'] as $obj):?>			

							<li>
								<div id='ranksListTitle'><a href='<?=$obj['url']?>' title="<?=$obj['user']?>"><?=$obj['user']?></a> <?=($obj['is_reviewer'])?"<sup> рецензент</sup>":""?></div>
								<div id='ranksListStar' style="width:220px;"><span style="display: table-cell;">&nbsp;<?=$obj['pre_mark']?>&nbsp;/&nbsp;<?=$obj['mark']?></span></div>
								<div id='ranksListLine'><img src='/images/blank.gif' height='1' width='1'></div>
							</li>

			<?endforeach;?>

			</ul>
		
			<?if($var['pages']['last_page_number']>1):?>
			<div id='ranksFooter'>
					<?=sys::parseTpl('main::pages',$var['pages'])?>
			</div>
			<?endif?>
	<?else:?> 
		<center class="xErr"><?=sys::translate('main::nothing_found')?></center>
	<?endif?>
</div>

