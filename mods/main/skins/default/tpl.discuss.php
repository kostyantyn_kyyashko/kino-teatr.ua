

<script type="text/javascript">

	<?if($_user['id']!=2):?>
	function rate(type,mark){
		$.ajax({
			url:'<?=sys::rewriteUrl("?mod=main&act=ajx_vote_comment")?>',
			data:{
				mark:mark,
				type:type,
				uid:<?=$_user['id']?>,
			},
			dataType:'json',
			success:function(data)
			{
				if(data)
				{
					$('#rate_'+mark).html(data['rating']);
					$('#plusminus_'+mark).html('');
				}
			},
			type:'POST'
		})
  	}
  	<?endif;?>
	
	function show_quote(id){
		$('#quote_'+id).show();
		$('#edit_'+id).hide();
//		 Recaptcha.reload(); 
	}
	
	function refresh_captcha(object,current_url){
		var rand_id = Math.random();
		var id = $(object).attr('data-id');
		$(object).attr('src',current_url+'?id='+id+'&'+rand_id);
	
	}
	
	function check_answer_form(object){
		var id = $(object).find('.img-captcha').attr('data-id');
		var form_id = $(object).attr('id');
		var code = $(object).find('.captcha').val();
		if(code == ''){
			alert('Не введен код!');
			$(object).find('.captcha').focus();
			return false;
		}
		
			check_captcha(id,object);


	}
	
	
	
	function check_captcha(id,object){
		var code = $(object).closest('form').find('.captcha').val();
		if(code == ''){
			$(object).closest('form').find('.img-captcha').attr('data-check',0)
			return false;
		}
		$.ajax({
			type: "POST",
    		url: '/captcha.php',
			data: 'check_captcha=1&code='+code+'&id='+id,
			dataType: 'json',
    		cache: false,
			async: true,
			beforeSend:  function(){
//            	alert('Ошибка!');
       		 },
    		success: function(data){
    			if(data.answer == 1){
					send_form(object);
				}
				else{
					alert('Неправильный код!');
				}
					
			}	
    	});
	}
	
	function send_form(object) {
		
	 var postData = $(object).serializeArray();
    $.ajax(
    {
        url : '/ajax_discuss.php',
        type: "POST",
        data : postData,
		dataType: 'json',
        success:function(data) 
        {
            if(data.answer == 0 || typeof(data.error) != 'undefined'){
				alert(data.error);
			}
			else if(data.answer == 1){
				document.location.reload();
			}
			else{
				alert('Неизвестная ошибка!');
			}
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            //if fails      
        }
    });
	
}


</script>



					<a name="discuss"></a>
					<div id='lineBlank' style='height: 15px;'><img src='/images/blank.gif' height='1'></div>
					<div id='searchTitle'><?=sys::translate('main::lastComments')?></div>
					<div id='lineHrWide'>&nbsp;</div>
					<div id=''>
						<div id='sendComment'>
							<form method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
								<?=sys::parseTpl('main::comment',$var);?>
								<input type="submit" id='commentSend' name="cmd_add" value='<?=sys::translate('main::send')?>'>
										<?if(sys::checkAccess('main::discuss_subscribe')):?>
			<input align="right" id="additionalButton" style="float:right;" type="submit" <?if($var['subscribe']):?> name="cmd_unsubscribe" value="<?=sys::translate('main::unsubscribe_discuss')?>"<?else:?> name="cmd_subscribe" value="<?=sys::translate('main::subscribe_discuss')?>"<?endif;?>">
						<?else:?>
						<a href='<?=sys::rewriteUrl('?mod=main&act=register')?>' id='additionalButton'><?=sys::translate('main::subscribe_discuss')?></a>
		<?endif?>
							</form>
						</div>
						<div id='lineBlank' style='height: 32px;'><img src='/images/blank.gif' height='1'></div>
					</div>

	<?if ($var['objects']):?>

		<?foreach ($var['objects'] as $obj):?>

						<form id='commentsForm' method="POST" action="<?=$_SERVER['REQUEST_URI']?>">
							<input type="hidden" name="_task">
							<div id='userTextHeader'>
								<div id='userPicture'>
									<?if($obj['avatar']):?>
										<img src="<?=$obj['avatar']?>" alt="<?=$obj['user']?>" title="<?=$obj['user']?>">
									<?endif?>
								</div>
								<div id='userInfo'>
								<?if($obj['user_url']):?>
									<a href="<?=$obj['user_url']?>"><?=$obj['user']?></a>
								<?else:?>
									<span><?=$obj['user']?></span>
								<?endif;?>
									<?=$obj['date']?><?if(sys::checkAccess('main::discuss_messages_edit') || ($obj['user_id']==$_user['id'] && $_user['id']!=2)):?>, IP:<?=$obj['ip']?><?endif;?>
								</div>

								<?if(sys::checkAccess('main::discuss_messages_mark') && !$obj['mark'] && $obj['user_id']!=$_user['id']):?>
								<div id='plusminus_<?=$obj["id"]?>'>
									<ul id='plusminusUl'>
										<li><a href='' onclick='rate("plus",<?=$obj["id"]?>); return false;' name="cmd_plus" ><span>+1</span></a></li>
										<li><a href=''onclick='rate("minus",<?=$obj["id"]?>); return false;' name="cmd_minus"><span>-1</span></a></li>
									</ul>
								</div>
								<?endif;?>
							</div>

							<div id='lineBlank' style='height: 11px;'><img src='/images/blank.gif' height='1'></div>

							<div id='filmText'>
								<?=$obj['bb_text']?>

								<div id='lineBlank' style='height: 9px;'><img src='/images/blank.gif' height='1'></div>

								<span><?=sys::translate('main::message_rating')?>: <b id='rate_<?=$obj["id"]?>'><?=$obj['rating']?></b></span>

								<div id='commentSubMenu'>
									<?if(sys::checkAccess('main::discuss_messages_delete')):?>
										<a href='' onclick="if(confirm('<?=sys::translate('main::are_you_sure')?>')){document.getElementById('commentsForm').elements['_task'].value='delete.<?=$obj['id']?>'; document.getElementById('commentsForm').submit();}; return false;" ><?=sys::translate('main::delete')?></a>
									<?endif?>
									<?if(sys::checkAccess('main::discuss_messages_edit') || ($obj['user_id']==$_user['id'] && $_user['id']!=2)):?>
										<!--a href='' onclick="getElementById('quote_<?=$obj['id']?>').style.display='none'; getElementById('edit_<?=$obj['id']?>').style.display='block'; return false;"><?=sys::translate('main::edit')?></a-->
										<a href='' onclick="show_quote(<?=$obj['id']?>);return false;"><?=sys::translate('main::edit')?></a>
									<?endif?>
									<?if(sys::checkAccess('main::discuss_messages_add')):?>
										<!--a href='' onclick="getElementById('quote_<?=$obj['id']?>').style.display='block'; getElementById('edit_<?=$obj['id']?>').style.display='none'; return false;"><?=sys::translate('main::quote')?></a-->
										<a href='' onclick="show_quote(<?=$obj['id']?>); return false;"><?=sys::translate('main::quote')?></a>
									<?endif?>
								</div>
		        			</div>

						</form>

							<!--form id="quote_<?=$obj['id']?>" method="POST" action="<?=$_SERVER['REQUEST_URI']?>" style="display:none" onsubmit="check_answer_form(this);return false;"-->
							<form id="quote_<?=$obj['id']?>" method="POST" action="<?=$_SERVER['REQUEST_URI']?>" style="display:none">
								<div id='lineBlank' style='height: 20px;'><img src='/images/blank.gif' height='1'></div>
								<?=sys::parseTpl('main::comment',array('value'=>'[QUOTE date="'.$obj['date'].'" author="'.$obj['user'].'"]'.$obj['text'].'[/QUOTE]','question'=>$var['question'], 'myavatar'=>$var['myavatar'], 'myurl'=>$var['myurl'], 'mylogin'=>$var['mylogin'], 'nowdate'=>$var['nowdate'], 'comment_id'=>$obj['id'], 'type'=>'quote' ));?>
								<!--input type="submit" id='commentSend' name="cmd_add" value='<?=sys::translate('main::send')?>'-->
								<input type="submit" id='commentSend' name="cmd_add" value='<?=sys::translate('main::send')?>'>
								<input type="hidden" name="object_id" value="<?=$var['object_id']?>"/>
								<input type="hidden" name="object_type" value="<?=$var['object_type']?>"/>
								<input type="hidden" name="chpu_base_url" value="<?=$var['chpu_base_url']?>"/>
								<input type="button" id='commentSend' value='<?=sys::translate('main::cancel')?>' onclick="this.parentNode.style.display='none'">
							</form>

							<form id="edit_<?=$obj['id']?>" method="POST" action="<?=$_SERVER['REQUEST_URI']?>" style="display:none">
								<div id='lineBlank' style='height: 20px;'><img src='/images/blank.gif' height='1'></div>
								<?=sys::parseTpl('main::comment',array('value'=>$obj['text'], 'myavatar'=>$var['myavatar'], 'myurl'=>$var['myurl'], 'mylogin'=>$var['mylogin'], 'nowdate'=>$var['nowdate'],'comment_id'=>$obj['id'], 'type'=>'edit' ));?>
								<input type="hidden" name="id" value="<?=$obj['id']?>">
								<input type="submit" id='commentSend' name="cmd_edit" value='<?=sys::translate('main::send')?>' >
								<input type="button" id='commentSend' value='<?=sys::translate('main::cancel')?>' onclick="this.parentNode.style.display='none'">
								<br>
								<br>
							</form>






							<div id='lineBlank' style='height: 9px;'><img src='/images/blank.gif' height='1'></div>
						<div id='lineHrWide'>&nbsp;</div>


		<?endforeach;?>
	<?endif;?>



	<?=sys::parseTpl('main::pages',$var['pages'])?>

						<div id='lineBlank' style='height: 35px;'><img src='/images/blank.gif' height='1'></div>






