<?
function main_last_sound()
{
	sys::useLib('main::films');
	sys::useLib('sys::pages');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('film_id','int');
	sys::filterGet('trailer_id','int');
	sys::filterGet('page','int');
	sys::filterGet('film','text');
	sys::filterGet('letter','text');


	if ($_GET['film'])
		$_REQUEST['film'] = urldecode($_GET['film']);


	define('_CANONICAL',main_films::getFilmTrailersUrl($_GET['film_id']),true);

	$cache_name=__FUNCTION__.'_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,_HOUR))
		return unserialize($cache);


		$q="
			SELECT
				trl.film_id,
				trl.id AS `trailer_id`,
				trl.image,
				trl.file,
				trl.url,
				trl.user_id,
				trl.shows,
				trl.width,
				trl.height,
				trl.order_number,
				trl.public,
				flm_lng.title AS `film`,
				flm_pst.image as `poster`
			FROM `#__main_films_trailers` AS `trl`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=trl.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			LEFT JOIN `#__main_films` AS `flm`
			ON flm.id=trl.film_id
			LEFT JOIN `#__main_films_photos` AS `flm_pst`
			ON flm_pst.film_id=trl.film_id
		";




		$q.="
			WHERE trl.public=1 ";

	if ($_REQUEST['film'])
	{
		$q.="
			AND flm_lng.title LIKE '".str_replace("'","",$_REQUEST['film'])."%'
		";
	}

	if($_GET['trailer_id'])
	{
		$q.="
			AND trl.id=".intval($_GET['trailer_id'])."
		";
	}

	if($_GET['letter'])
	{
		$q.="
			AND
				(
				flm_lng.title LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%'
			OR
				flm.title_orig LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%'
				)
		";
	}


		if(!$_GET['trailer_id'])
			{
				$q .= "ORDER BY trl.id DESC ";
			}
		$q .= "
			LIMIT 1
		";




	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);


	if(!$result['public'] && !sys::checkAccess('main::films_trailers_confirm'))
		return 404;

	main::countShow($result['film_id'],'film');

	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::films_trailers_delete') || $result['user_id']==$_user['id'])
		{
			main_films::deleteTrailer($result['trailer_id']);
			sys::redirect(main_films::getFilmtrailersUrl($result['film_id']),false);
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::films_trailers_confirm'))
	{
		main_films::confirmtrailer($result['trailer_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}

	if($_cookie)
	{
		$_db->query("
			UPDATE `#__main_films_trailers`
			SET `shows`=(`shows`+1)
			WHERE `id`=".doubleval($result['trailer_id'])."
		");
	}
	$meta['film']=$result['film'];
	$meta['title']=sys::translate('main::soundtracks');

	$meta['film']=$result['film'];

	$meta['persons']=false;
	$result['persons']=main_films::gettrailerPersons($result['trailer_id']);

	$result['image']=$_cfg['main::films_url'].'x4_'.$result['image'];
	if($result['file'])
		$result['file']=$_cfg['main::films_url'].$result['file'];
	else
		$result['file']=$result['url'];


	preg_match('#'._ROOT_URL.'public/main/films/(.*?).flv#is', $result['file'] , $img);

	if (!$img[1])
	{
		$img[1] = 'trailer_'.$result['trailer_id'];
	}

	$result['post']=$img[1].'.jpg';

	$result['next_url']=false;
	$result['prev_url']=false;
	$result['delete_trailer']=false;
	$result['edit_url']=false;
	$result['add_url']=false;
	$result['url']=main_films::getFilmTrailerUrl($result['trailer_id']);
	$result['alt']=false;
	if(!$result['width'])
		$result['width']=495;
	if(!$result['height'])
		$result['height']=372;

	if($result['width']>495)
	{
		$result['width']=495;
		$result['height']=372;
	}


	foreach ($result['persons'] as $person)
	{
		$result['alt'].=$person['fio'].', ';
	}

	if($result['alt'])
	{
		$meta['persons']=sys::cutStrRight($result['alt'],2);
		$result['alt']=sys::cutStrRight($result['alt'],2);
		$result['alt']=$result['alt'].' '.sys::translate('main::in_film').' &quot;'.htmlspecialchars($result['film']).'&quot';
	}


	$meta['letter']=mysql_real_escape_string(urldecode($_GET['letter']));
	$meta['page']=$_GET['page'];

	if(!$_GET['letter'] && !$_GET['page'])
	{
		$result['meta']=sys::parseModTpl('main::last_sound','page',$meta);
	} else if ($_GET['letter'] && !$_GET['page']) {
		$result['meta']=sys::parseModTpl('main::last_sound_letter','page',$meta);
	} else if ($_GET['letter'] && $_GET['page']) {
		$result['meta']=sys::parseModTpl('main::last_sound_letter_page','page',$meta);
	} else if (!$_GET['letter'] && $_GET['page']) {
		$result['meta']=sys::parseModTpl('main::last_sound_page','page',$meta);
	}

	if($next_id=main_films::getNexttrailerId($result['film_id'],$result['order_number']))
		$result['next_url']=main_films::gettrailersUrl($next_id);
	if($prev_id=main_films::getPrevtrailerId($result['film_id'],$result['order_number']))
		$result['prev_url']=main_films::gettrailersUrl($prev_id);

	if(sys::checkAccess('main::films_trailers_edit') || $result['user_id']==$_user['id'])
		$result['edit_url']=main_films::gettrailerEditUrl($result['trailer_id']);

	if(sys::checkAccess('main::films_trailers_add'))
		$result['add_url']=main_films::gettrailerAddUrl($result['film_id']);

	if(sys::checkAccess('main::films_trailers_delete') || $result['user_id']==$_user['id'])
		$result['delete_trailer']=true;
	//=============================================================================

	//Получить инфу об остальных трейлрах-----------------------------------------------
	$q="
		SELECT 
			trl.film_id,
			trl.id,
			flm_lng.title AS `film`
		FROM `#__main_films_soundtracks` AS `trl`
			LEFT JOIN `#__main_films_lng` AS `flm_lng`
			ON flm_lng.record_id=trl.film_id
			AND flm_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			LEFT JOIN `#__main_films` AS `flm`
			ON flm.id=trl.film_id
		WHERE trl.public=1 ";

	if ($_REQUEST['film'])
	{
		$q.="
			AND flm_lng.title LIKE '".str_replace("'","",$_REQUEST['film'])."%'
		";
	}

	if($_GET['letter'])
	{
		$q.="
			AND
				(
				flm_lng.title LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%'
			OR
				flm.title_orig LIKE '".mysql_real_escape_string(urldecode($_GET['letter']))."%'
				)
		";
	}

	$q .= "
			GROUP BY trl.film_id
			ORDER BY trl.id DESC
	";

	if ($_REQUEST['film'])	$qf = 'film='.$_REQUEST['film'];
	if ($_GET['letter'])	$ql = 'letter='.$_GET['letter'];
	if ($_GET['page'])		$qp = 'page='.$_GET['page'];

	if($_GET['wallpaper_id'])	$pu = sys::getHumanUrl($_GET['wallpaper_id'],$result['name'],"last_sound");
	 else $pu = preg_replace("/\/\.phtml/",".phtml",sys::getHumanUrl('','',"last_sound"));
	
	$letters = $search_act = $pu; 
		
	// подготовить вывод указателя по буквам (отбросим старый параметр letter)
	if($qf) $letters .= ((strpos($letters,"?")===false)?"?":"&").$qf;
	if($qp) $letters .= ((strpos($letters,"?")===false)?"?":"&").$qp;
	$result['letters'] = main::showLetters_1($letters);
	
	// подготовить поля формы поиска
	$result['search_form']['action'] = $pu;
	$result['search_form']['letter'] = $_GET['letter'];
	$result['search_form']['page'] = $_GET['page'];
	$result['search_form']['film'] = $_GET['film'];

	// для остальных ссылок надо подставить только поиск по слову (film) и по указателю буквы (letter)
	if($qf) $pu .= ((strpos($pu,"?")===false)?"?":"&").$qf;
	if($ql) $pu .= ((strpos($pu,"?")===false)?"?":"&").$ql;
	 
	// Заменить старые ссылки на страницы на новые
	$result['pages']=sys_pages::pocess($q);
	$result['pages']['first_page'] = sys_pages::setPageUrl($result['pages']['first_page'], $pu);
	$result['pages']['prev_page']  = sys_pages::setPageUrl($result['pages']['prev_page'],  $pu);
	$result['pages']['next_page']  = sys_pages::setPageUrl($result['pages']['next_page'],  $pu);
	$result['pages']['last_page']  = sys_pages::setPageUrl($result['pages']['last_page'],  $pu);
	foreach ($result['pages']['pages'] as $i=>$p)
	 $result['pages']['pages'][$i] = sys_pages::setPageUrl($result['pages']['pages'][$i],  $pu);

	$r=$_db->query($q.$result['pages']['limit']);
	$result['previews']=array();
	$i=0;

	while ($obj=$_db->fetchAssoc($r))
	{


		$qw="
			SELECT
				image
			FROM `#__main_films_posters`
			WHERE film_id='".intval($obj['film_id'])."'
			AND public=1
			AND order_number=1
			L