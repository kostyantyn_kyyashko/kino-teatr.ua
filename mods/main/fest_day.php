<?

function maxsite_str_word($text, $counttext = 10, $sep = ' ') {
	$words = split($sep, $text);
	if ( count($words) > $counttext )
		$text = join($sep, array_slice($words, 0, $counttext));
	return $text;
}

function main_fest_day()
{

	sys::useLib('main::fest');
	sys::useLib('main::films');
	sys::useLib('main::users');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('fest_id','int');



	if(!$_GET['day_id'])
		return 404;

	$arr = explode('-', $_GET['day_id']);

	$fest_id = $arr[0];
	$result['fid'] = $fest_id;


	$r=$_db->query("
		SELECT
			art.date_w_s,
			art.fest_id,
			art.comments,
			art.public,
			flm.title,
			flm.intro,
			fl.year,
			fl.id,
			fl.rating,
			fl.votes,
			fl.title_orig,
			fl.ukraine_premiere,
			art.name as `film_id`,
			MONTH(art.date_w_s) AS `month`,
			fst.title as `festival`,
			fl.year
		FROM `#__main_fest_articles` AS `art`

		LEFT JOIN
			`#__main_films_lng` AS `flm`
		ON
			art.name = flm.record_id
		AND
			flm.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN
			`#__main_films` AS `fl`
		ON
			art.name = fl.id



		LEFT JOIN
			`#__main_fest_lng` AS `fst`
		ON
			art.fest_id = fst.record_id
		AND
			fst.lang_id=".$_cfg['sys::lang_id']."

		WHERE art.fest_id=".intval($fest_id)."
		AND DAY(art.date_w_s)=".$arr[1]."
		AND MONTH(art.date_w_s)=".$arr[2]."
		  ORDER BY art.date_w_s
	");






	while($obj=$_db->fetchAssoc($r))
	{
		$festival = $obj['festival'];
		$id='';		$id = date('dmY', strtotime($obj['date_w_s']));
		$obj['time'] = date('H:i', strtotime($obj['date_w_s']));
		$obj['intro'] = strip_tags(maxsite_str_word($obj['intro'], 40).'...');


		$qw=$_db->query("
			SELECT
				lng.title as `country`

			FROM `#__main_films_countries` AS `cnt`

			LEFT JOIN
				 `#__main_countries_lng` AS `lng`
			ON
				cnt.country_id=lng.record_id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE cnt.film_id=".intval($obj['id'])."
			LIMIT 1
		");



		while($object=$_db->fetchAssoc($qw))
		{
			$obj['country'] = $object['country'];

		}

		$qw=$_db->query("
			SELECT
				lng.title as `genre`

			FROM `#__main_films_genres` AS `cnt`

			LEFT JOIN
				 `#__main_genres_lng` AS `lng`
			ON
				cnt.genre_id=lng.record_id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."

			WHERE cnt.film_id=".intval($obj['id'])."
		");

        $obje['genres'] = array();

		while($object=$_db->fetchAssoc($qw))
		{
			$obje['genres'][] = $object['genre'];
		}

		$obj['genres'] = implode(', ', $obje['genres']);


		if ($obj['year']>0)
		{
			$obj['year'] = $obj['year'];
		} else {
			$obj['year']='';
		}

		$obj['second'] = array();
		if ($obj['year'])
		$obj['second'][] = $obj['year'];
		if ($obj['country'])
		$obj['second'][] = $obj['country'];
		if ($obj['genres'])
		$obj['second'][] = $obj['genres'];

		if ($obj['year'] || $obj['country'] || $obj['genres'])
		$obj['second_row'] = implode(' | ', $obj['second']).'<br>';


		$obj['round'] = '';

		if ($obj['title_orig'])
		{
			$obj['round'] .= '<font style="color: #AAA; font-size: 10px;">'.$obj['title_orig'].'</font>';
		}


		if ($obj['rating']>0)
		{
			$obj['rate'] = sys::translate('main::rating').': <strong>'.round($obj['rating'], 1).'</strong> / '.sys::translate('main::votes').': '.$obj['votes'].'<br>';
		}


		$obj['url']=main_films::getFilmUrl($obj['id']);

        $object['actors'] = array();
        $obje['actors'] = array();

        $pi = 0;
			$obje['actors']=main_films::getFilmActors($obj['id']);

            if (is_array($obje['actors'][0]))
            {

	            for ($i=0;$i<=1;$i++)
	            {
	            	if ($obje['actors'][$i]['url'])
	            	{
						$object['actors'][] = '<a href="'.$obje['actors'][$i]['url'].'" title="'.$obje['actors'][$i]['fio'].'">'.$obje['actors'][$i]['fio'].'</a>';
						$pi++;
					}
	            }

            }

		if ($pi>0)
		{
			$obj['actors'] = sys::translate('main::actors').': '.implode(', ', $object['actors']);
		}

        $object['directors'] = array();
        $obje['directors'] = array();

        $di = 0;
			$obje['directors']=main_films::getFilmDirectors($obj['id']);


            if (is_array($obje['directors'][0]))
            {

	            for ($i=0;$i<1;$i++)
	            {
						$object['directors'][] = '<a href="'.$obje['directors'][$i]['url'].'" title="'.$obje['directors'][$i]['fio'].'">'.$obje['directors'][$i]['fio'].'</a>';
						$di++;
	            }

            }

		if ($di>0)
		{
			$obj['directors'] = sys::translate('main::directors').': '.implode(', ', $object['directors']);
		}

		$obj['third'] = array();
		if ($obj['directors'])
		$obj['third'][] = $obj['directors'];
		if ($obj['actors'])
		$obj['third'][] = $obj['actors'];

		if ($obj['directors'] || $obj['actors'])
		$obj['third_row'] = implode('<br>', $obj['third']);



	$poster=main_films::getFilmFirstPoster($obj['id']);


		if($poster)
		{
			$image=$_cfg['main::films_url'].$poster['image'];
		} else {
         	$image = 'blank_news_img.jpg';
		}


			$obj['poster'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=23';

		if(main_films::isFilmReviews($obj['id']))
		{
			$obj['revurl']=main_films::getFilmReviewsUrl($obj['id']);
		}

		if(main_films::isFilmPhotos($obj['id']))
		{
			$obj['photurl']=main_films::getFilmPhotosUrl($obj['id']);
		}

		if(main_films::isFilmPosters($obj['id']))
		{
			$obj['posturl']=main_films::getFilmPostersUrl($obj['id']);
		}

		if(main_films::isFilmTrailers($obj['id']))
		{
			$obj['trailurl']=main_films::getFilmTrailersUrl($obj['id']);
		}





				$result['films'][$id][]=$obj;


				$result['films'][$id]['dayid']=date('dmY', strtotime($obj['date_w_s']));

				$result['films'][$id]['festid']=$obj['fest_id'];



				$result['films'][$id]['dayid']=date('dmY', strtotime($obj['date_w_s']));



//				$result['films'][$id]['month']=date('m', strtotime($obj['date_w_s']));
	}

ksort($result['films']);


	if(!$result)
		return 404;


	$m = sys::translate('main::month_'.$arr[2].'_a');

	$result['title'] = $arr[1].' '.$m.' на фестивале "'.$festival.'"';
	$meta['article']=$result['title'];
	$result['meta']=sys::parseModTpl('main::fest_day','page',$meta);
	if($result['image'])
	{
		$size=getimagesize($_cfg['main::fest_dir'].$result['image']);
		$result['width']=$size[0];
		$result['image']=$_cfg['main::fest_url'].$result['image'];
	}
	$result['alt']=htmlspecialchars($result['alt']);
	$result['source']=htmlspecialchars($result['source']);

	$date=sys::db2Timestamp($result['date']);
	$result['year']=date('Y',$date);
	$result['month']=date('m',$date);

	$result['date']=sys::db2Date($result['date'],false,$_cfg['sys::date_format']);
	$result['user_url']=main_users::getUserUrl($result['user_id']);



	return $result;


}
?>