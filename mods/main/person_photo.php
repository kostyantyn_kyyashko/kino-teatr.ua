<?
function main_person_photo()
{
	sys::useLib('main::persons');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('photo_id','int');
	
	if(!$_GET['photo_id'])
		return 404;

		$q="
			SELECT 
				pht.person_id,
				pht.image,
				CONCAT(prs_lng.firstname,' ',prs_lng.lastname) AS `person`
			FROM `#__main_persons_photos` AS `pht`
			LEFT JOIN `#__main_persons_lng` AS `prs_lng`
			ON prs_lng.record_id=pht.person_id
			AND prs_lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE pht.id=".intval($_GET['photo_id'])."
			AND pht.public=1
		";
	
	$r=$_db->query($q);
	$result=$_db->fetchAssoc($r);
	
	
	if(!$result)
		return 404;
		
	$meta['person']=$result['person'];	
	$result['meta']=sys::parseModTpl('main::person_photo','page',$meta);
	
	$result['image']=$_cfg['main::persons_url'].$result['image'];
	main::countShow($result['person_id'],'person');
	return $result;
}
?>