<?
function main_contest_article_discuss()
{
	sys::useLib('main::contest');
	sys::useLib('main::discuss');
	sys::useLib('main::users');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('article_id','int');

	if(!$_GET['article_id'])
		return 404;

	$r=$_db->query("
		SELECT
			art.date,
			art.contest_id,
			art_lng.title
		FROM
			`#__main_contest_articles` AS `art`

		LEFT JOIN
			`#__main_contest_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".$_cfg['sys::lang_id']."

		WHERE art.id=".intval($_GET['article_id'])."
		AND art.public=1
	");
	$result=$_db->fetchAssoc($r);
	if(!$result)
		return 404;
	main::countShow($_GET['article_id'],'article');

	$meta['article']=$result['title'];
	$result['meta']=sys::parseModTpl('main::contest_article_discuss','page',$meta);
	$date=sys::db2Timestamp($result['date']);
	$result['year']=date('Y',$date);
	$result['month']=date('m',$date);
	sys::jsInclude('sys::bb');
	return $result;
}
?>