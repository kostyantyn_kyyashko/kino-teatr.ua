<?
function main_ajx_PopularInterview()
{
	sys::setTpl();
	sys::filterGet('keywords');
	sys::filterGet('field');
	sys::filterGet('mode');
	sys::useLib('main::interview');
	global $_db, $_cfg, $_err, $_user, $_cookie;

	echo main_interview::showLastPopularArticles();
}
?>