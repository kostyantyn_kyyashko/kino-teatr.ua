<?
function main_review_discuss()
{
	sys::useLib('main::reviews');
	sys::useLib('main::discuss');
	sys::useLib('main::films');
	sys::useLib('main::users');
	global $_db, $_cfg, $_err, $_user, $_cookie;
	sys::filterGet('review_id','int');
	
	if(!$_GET['review_id'])
		return 404;
		
	$r=$_db->query("
		SELECT
			rev.title,
			flm_lng.title AS `film`,
			rev.film_id
		FROM
			`#__main_reviews` AS `rev`
			
		LEFT JOIN 
			`#__main_films_lng` AS `flm_lng`
		ON 
			flm_lng.record_id=rev.film_id
		AND 
			flm_lng.lang_id=".$_cfg['sys::lang_id']."
			
		WHERE rev.id=".intval($_GET['review_id'])."
		AND rev.public=1
	");
	$result=$_db->fetchAssoc($r);
	if(!$result)
		return 404;
	$meta['review']=$result['title'];	
	$meta['film']=$result['film'];
	$result['meta']=sys::parseModTpl('main::review_discuss','page',$meta);
	sys::jsInclude('sys::bb');
	main::countShow($_GET['review_id'],'review');
	return $result;
}
?>