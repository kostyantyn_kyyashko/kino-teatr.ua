<?
function main_interview_article()
{
	sys::useLib('main::interview');
	sys::useLib('main::users');
	sys::useLib('main::discuss');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('interview_id','int');

	define('_CANONICAL',main_interview::getArticleUrl($_GET['article_id']),true);

	if(!$_GET['article_id'])
		return 404;

	$r=$_db->query("
		SELECT
			art.date,
			art.image,
			art.tags,
			art.interview_id,
			art.user_id,
			art.comments,
			art.source_url,
			art.public,
			art_lng.title,
			art_lng.text,
			art_lng.source,
			art_lng.alt,
			usr.login AS `user`,
			usr_data.main_nick_name
		FROM `#__main_interview_articles` AS `art`

		LEFT JOIN
			`#__main_interview_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN
			`#__sys_users` AS `usr`
		ON
			usr.id=art.user_id

		LEFT JOIN
			`#__sys_users_data` AS `usr_data`
		ON
			usr.id=usr_data.user_id

		WHERE art.id=".intval($_GET['article_id'])."
	");

	$result=$_db->fetchAssoc($r);

	if(!$result)
		return 404;

	if($result["main_nick_name"]) $result["user"] = $result["main_nick_name"];
	
	if($_REQUEST['editedtext'] && sys::checkAccess('main::interview_edit'))
	{

		$_REQUEST['editedtext'] = str_replace("'",'"',$_REQUEST['editedtext']);


		$r=$_db->query("
			UPDATE
				`#__main_interview_articles_lng`
			SET
				text = '".str_replace("'",'"',$_REQUEST['editedtext'])."'

			WHERE
				record_id=".intval($_GET['article_id'])."
			AND
				lang_id = ".$_cfg['sys::lang_id']."
		");

	}

	if(!$result['public'] && !sys::checkAccess('main::interview_confirm'))
		return 404;

	main::countShow($_GET['article_id'],'interview_article');

	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::interview_delete') || $result['user_id']==$_user['id'])
		{
			main_interview::deleteArticle($_GET['article_id']);
			sys::redirect('?mod=main&act=interview');
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::interview_confirm'))
	{
		main_interview::confirmArticle($_GET['article_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}


	$meta['article']=$result['title'];
	$result['meta']=sys::parseModTpl('main::interview_article','page',$meta);
	if($result['image'])
	{
		$size=getimagesize($_cfg['main::interview_dir'].$result['image']);
		$result['width']=$size[0];
		$result['image']=$_cfg['main::interview_url'].$result['image'];
	}
	$result['alt']=htmlspecialchars($result['alt']);
	$result['source']=htmlspecialchars($result['source']);

	$result['text'] = ereg_replace("<([^>]*)(class|lang|style|size|face)=(\"[^\"]*\"|'[^']*'|[^>]+)([^>]*)>","<\\1>",$result['text']);

	$result['text'] = str_replace("<div>&nbsp;</div>","",$result['text']);
	$result['text'] = str_replace('<div >&nbsp;</div>',"",$result['text']);

	$result['text'] = str_replace("<p>&nbsp;</p>","",$result['text']);

	$date=sys::db2Timestamp($result['date']);
	$result['year']=date('Y',$date);
	$result['month']=date('m',$date);

	$result['tag'] = array();

	if ($_cfg['sys::lang_id']=='1')
	{
		$tags = explode(',',$result['tags']);
	} else {
		$tags = explode(',',$result['tags_ua']);
	}

	foreach ($tags as $key=>$value)
	{
		if ($value)
		$result['tag'][] = '<a href="'.sys::rewriteUrl('?mod=main&act=news').'?tag='.trim($value).'">'.trim($value).'</a>';
	}



	$result['tags'] = implode(', ', $result['tag']);

	$result['date']=sys::russianDate($result['date']);
	$result['user_url']=main_users::getUserUrl($result['user_id']);
	$result['edit_url']=false;
	$result['delete']=false;


	//�����������--------------------------------------------
	$q="
		SELECT
			msg.id,
			msg.date,
			msg.user_id,
			msg.user_name,
			msg.rating,
			usr.login AS `user`,
			msg.text,
			grp.group_id AS `star`,
			data.main_avatar AS `avatar`

		FROM `#__main_discuss_messages` AS `msg`

		LEFT JOIN `#__sys_users` AS `usr`
		ON usr.id=msg.user_id

		LEFT JOIN `#__sys_users_data` AS `data`
		ON data.user_id=msg.user_id

		LEFT JOIN `#__sys_users_groups` as grp
		ON grp.user_id=msg.user_id
		AND grp.group_id=23

		WHERE msg.object_id='".intval($_GET['article_id'])."'
		AND msg.object_type='film'

		ORDER BY msg.date DESC

		LIMIT 0,5
	";


	$r=$_db->query($q);
	$result['comments']=array();


	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::db2Date($obj['date'],true);
		if($obj['user_id']==2)
		{
			$obj['user']=$obj['user_name'];
			$obj['user_url']=false;
		}
		else
			$obj['user_url']=main_users::getUserUrl($obj['user_id']);
		$obj['bb_text']=sys::parseBBCode($obj['text']);
		$result['comments'][]=$obj;
	}

	if(sys::checkAccess('main::interview_edit') || $result['user_id']==$_user['id'])
		$result['edit_url']=main_interview::getArticleEditUrl($_GET['article_id']);

	if(sys::checkAccess('main::interview_delete') || $result['user_id']==$_user['id'])
		$result['delete']=true;
	return $result;
}
?>