<?
function main_boxw_article()
{
	sys::useLib('main::boxw');
	sys::useLib('main::films');
	sys::useLib('main::users');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('boxw_id','int');

	if(!$_GET['article_id'])
		return 404;

	$r=$_db->query("
		SELECT
			art.date,
			art.image,
			art.boxw_id,
			art.user_id,
			art.comments,
			art.public,
			art_lng.title,
			art_lng.intro,
			art_lng.text,
			usr.login AS `user`
		FROM `#__main_boxw_articles` AS `art`

		LEFT JOIN
			`#__main_boxw_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".$_cfg['sys::lang_id']."

		LEFT JOIN
			`#__sys_users` AS `usr`
		ON
			usr.id=art.user_id

		WHERE art.id=".intval($_GET['article_id'])."
	");

	$result=$_db->fetchAssoc($r);


	$r=$_db->query("
		SELECT
			SUM(money) as last_week
		FROM
			`#__main_boxw_positions`
		WHERE
			boxw_id='".intval($_GET['article_id']-1)."'
	");



		while ($obj=$_db->fetchAssoc($r))
		{
			$last_week = $obj['last_week'];
		}

		$result['last_week'] = number_format($last_week,0, ",", " ");

	$r=$_db->query("
		SELECT
			date_w_f as date,
			date_w_s,
			date_w_f
		FROM
			`#__main_boxw_articles`
		WHERE
			id='".intval($_GET['article_id'])."'
	");

		while ($obj=$_db->fetchAssoc($r))
		{
			$peri=' за '.date('d', strtotime($obj['date_w_s'].' +1 days')).' - '.sys::russianDate($obj['date_w_f'].' +1 days');
			$findate = $obj['date'];
		}

	$r=$_db->query("
		SELECT
			boxw.real_id,
			boxw.film_id,
			boxw.film_name,
			boxw.position,
			boxw.studio,
			boxw.money,
			boxw.screens,
			boxw.total,
			flm.year,
			flm.title_orig,
			flm.ukraine_premiere,
			flm.world_premiere,
			lng.title,
			dst.title as dist
		FROM
			`#__main_boxw_positions` AS `boxw`
		LEFT JOIN
			`#__main_films` AS `flm`
		ON
			flm.id=boxw.film_id
		LEFT JOIN
			`#__main_films_lng` AS `lng`
		ON
			boxw.film_id=lng.record_id
		AND
			lng.lang_id=".$_cfg['sys::lang_id']."
		LEFT JOIN
			`#__main_films_distributors` AS `dstr`
		ON
			boxw.film_id=dstr.film_id
		LEFT JOIN
			`#__main_distributors_lng` AS `dst`
		ON
			dstr.distributor_id=dst.record_id
		AND
			dst.lang_id=".$_cfg['sys::lang_id']."
		WHERE
			boxw.boxw_id='".intval($_GET['article_id'])."'
		ORDER BY
			boxw.position

	");


	$total_week = 0;

		while ($obj=$_db->fetchAssoc($r))
		{

				$obj['week']=$boxw_id;
				$obj['pos']=$obj['position'];

				if ($obj['position']=='1')
				{					$filmid = $obj['film_id'];
				}

				$obj['id']=$obj['film_id'];
				$obj['title-r']=$obj['title'];

				if (!$obj['title-r'])
				{					$obj['title-r'] = $obj['film_name'];
				}

				$obj['title-o']=$obj['title_orig'];
				$obj['year']=$obj['year'];
				$obj['date']=$obj['ukraine_premiere'];
				$obj['screens']=$obj['screens'];
				$obj['dist']=$obj['dist'];
				$obj['usd']=number_format($obj['money'],0, ",", " ");
				$obj['total']=number_format($obj['total'],0, ",", " ");

				$total_week += $obj['money'];

				$now = strtotime($findate)+86400;
				$selected_date = strtotime($obj['world_premiere']);
				$seconds_diff = $now - $selected_date;
				$days_diff = $seconds_diff / (60*60*24);

				$obj['days']=round($days_diff);

				$rw=$_db->query('
					SELECT
						*
					FROM
						`#__main_boxw_positions`
					WHERE
						film_name="'.$obj["film_name"].'"
					AND
						real_id="'.intval($obj["real_id"]-1).'"
				');



					while ($object=$_db->fetchAssoc($rw))
					{

							if ($obj['money']>=$object['money'])
							{
								$obj['last_money'] = '+'.round($obj['money']/$object['money']*100-100);
							} else {
								$obj['last_money'] = round($obj['money']/$object['money']*100-100);
							}

						$obj['last_pos'] = $object['position'];
						if (($obj['screens']-$object['screens'])!=0)
						{
							$obj['last_screens'] = '<br>('.($obj['screens']-$object['screens']).')';
						} else {							$obj['last_screens'] = '';
						}
					}

				if (!$obj['last_pos'])
					$obj['last_pos'] = '-';

				if (!$obj['last_money'])
					$obj['last_money'] = '-';

				$result['films'][$obj['position']]=$obj;
		}



	$result['total_week'] = number_format($total_week,0, ",", " ");

	if ($total_week>=$last_week)
	{		$week_change = '+'.round($total_week/$last_week*100-100);
	} else {		$week_change = round($total_week/$last_week*100-100);
	}

	$result['week_change'] = $week_change;


	if(!$result)
		return 404;

	if(!$result['public'] && !sys::checkAccess('main::boxw_confirm'))
		return 404;




	main::countShow($_GET['article_id'],'article');




	if(isset($_POST['_task']) && $_POST['_task']=='delete')
	{
		if(sys::checkAccess('main::boxw_delete') || $result['user_id']==$_user['id'])
		{
			main_boxw::deleteArticle($_GET['article_id']);
			sys::redirect('?mod=main&act=boxw');
		}
	}

	if(isset($_POST['cmd_confirm']) && $_POST['cmd_confirm'] && sys::checkAccess('main::boxw_confirm'))
	{
		main_boxw::confirmArticle($_GET['article_id']);
		sys::redirect($_SERVER['REQUEST_URI'],false);
	}


	$meta['article']=$result['title'];
	$meta['type']=sys::translate('main::world_box_now');
	$meta['period']=$peri;
	$result['meta']=sys::parseModTpl('main::box_article','page',$meta);

   	$poster=main_films::getFilmFirstPoster($filmid);

           	if ($poster)
           	{
				$image=$_cfg['main::films_url'].$poster['image'];
           	} else {
         		$image = 'blank_news_img.jpg';
           	}


		$result['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=26';



	$result['alt']=htmlspecialchars($result['alt']);
	$result['source']=htmlspecialchars($result['source']);

	$date=sys::db2Timestamp($result['date']);
	$result['year']=date('Y',$date);
	$result['month']=date('m',$date);

 	$result['date']=sys::russianDate($result['date']);
	$result['user_url']=main_users::getUserUrl($result['user_id']);
	$result['edit_url']=false;
	$result['delete']=false;


	if(sys::checkAccess('main::boxw_edit') || $result['user_id']==$_user['id'])
		$result['edit_url']=main_boxw::getArticleEditUrl($_GET['article_id']);

	if(sys::checkAccess('main::boxw_delete') || $result['user_id']==$_user['id'])
		$result['delete']=true;


	return $result;


}
?>