<?
function main_counter14()
{
	sys::useLib('main::films');
	global $_db, $_cfg, $_err, $_user;
	sys::filterGet('film_id','int');

	$uri=explode('_',$_SERVER['REQUEST_URI']);
	$_GET['film_id']=$uri[1];

	if(!$_GET['film_id'])
		return 404;

	$r=$_db->query("
		SELECT
			`rating`,
			`pre_rating`,
			`pre_votes`,
			`votes`,
			`pre_sum`,
			`sum`
		FROM
			`#__main_films`
		WHERE
			`id`='".intval($_GET['film_id'])."'
	");

	$film=$_db->fetchAssoc($r);


	if(!$film)
		return 404;

	if(!$film['rating'])
	{
		$film['rating']=$film['pre_rating'];
		$film['votes']=$film['pre_votes'];
	}

	$film['rating']=main_films::formatFilmRating($film['rating']);




	for($i=0.25; $i<11; $i=$i+0.25)
	{
		if($film['rating']<$i)
		{
			$stars='stars_'.($i-0.25).'.gif';
			break;
		}
	}

	$stars=imagecreatefromgif(_IMG_DIR.'counter/'.$stars);
	$img=imagecreatefromgif(_IMG_DIR.'counter/cnt.gif');

	$font=_SECURE_DIR.'fonts/tahomabd.ttf';
	$color=imagecolorallocate($img,199,4,82);
	$fontSize=11;
	$box=imagettfbbox($fontSize,0,$font,$film['rating']);
	$x=105-$box[4];
	imagettftext($img,$fontSize,0,$x,14,$color,$font,$film['rating']);

	$color=imagecolorallocate($img,44,52,68);
	$fontSize=8;
	$box=imagettfbbox($fontSize,0,$font,$film['votes']);
	$x=105-$box[4];
	imagettftext($img,$fontSize,0,$x,24,$color,$font,$film['votes']);


	imagecopy($img,$stars,0,26,0,0,108,13);
	header('Content-type: image/gif');
	imagegif($img);
	imagedestroy($img);
	sys::setTpl();
}
?>