<?
function translit_sourse($str)
{
    $tr = array(
        "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
        "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
        "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
        "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
        "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
        "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
        "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        "the"=>"","the "=>""," the"=>""," the "=>"-",
        "The"=>"","The "=>""," The"=>""," The "=>"-",
        "... "=> "", " - "=> "-"," "=> "-", "."=> "", "/"=> "-"
    );
    return strtolower(strtr($str,$tr));
}


function main_bill_export()
{
	global $_db, $_cfg, $_err, $_user, $_cookie;

	sys::useLib('main::films');
	sys::useLib('main::shows');
	sys::useLib('main::cinemas');
	sys::useLib('main::persons');
	sys::useLib('main::reviews');
	sys::useLib('main::countries');
	sys::useLib('sys::form');

	/* $ips=file(_SECURE_DIR.'export.txt');
	foreach ($ips as $ip)
	{
		$ip=explode('#',$ip);
		$ips2[]=trim($ip[0]);
	}
	if(!in_array($_SERVER['REMOTE_ADDR'],$ips2))
		return 404;

	if(!sys::checkAccess('main::bill_export'))
		return 403; */

	$cache_name='main_bill_xml_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::billboard_xml_cache_period']*60))
	return unserialize($cache);
	$result=array();

	//Жанры----------------------------------------------------------------
	$r=$_db->query("
		SELECT
			gnr.id,
			lng.title
		FROM
			#__main_genres AS gnr
		LEFT JOIN
			#__main_genres_lng AS lng
		ON
			lng.record_id=gnr.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			gnr.order_number
	");
	$result['genres']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['genres'][]=$obj;
	}

	//Профессии----------------------------------------
	$r=$_db->query("
		SELECT
			prf.id,
			lng.title
		FROM
			#__main_professions AS prf
		LEFT JOIN
			#__main_professions_lng AS lng
		ON
			lng.record_id=prf.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			prf.order_number
	");
	$result['professions']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['professions'][]=$obj;
	}
	//===============================================================================


	//Страны------------------------------------------------------------------------
	$r=$_db->query("
		SELECT
			cnt.id,
			lng.title
		FROM
			#__main_countries AS cnt
		LEFT JOIN
			#__main_countries_lng AS lng
		ON
			lng.record_id=cnt.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			cnt.order_number
	");
	$result['countries']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['countries'][]=$obj;
	}

	$r=$_db->query("
		SELECT
			cit.id,
			cit.country_id,
			lng.title
		FROM
			#__main_countries_cities AS cit
		LEFT JOIN
			#__main_countries_cities_lng AS lng
		ON
			lng.record_id=cit.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			cit.order_number
	");
	$result['cities']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['cities'][]=$obj;
	}
	//===============================================================================


	//Студии-------------------------------------------------------------------
	$r=$_db->query("
		SELECT
			std.id,
			lng.title
		FROM
			#__main_studios AS std
		LEFT JOIN
			#__main_studios_lng AS lng
		ON
			lng.record_id=std.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		ORDER BY
			std.id
	");
	$result['studios']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['studios'][]=$obj;
	}
	//========================================================================


	//Показы-----------------------------------------------------------------
	$r=$_db->query("
			SELECT
			shw.begin,
			shw.end,
			shw.film_id,
			shw.hall_id,
			shw.id,
			hls.cinema_id
		FROM
			#__main_shows AS shw
		LEFT JOIN
			#__main_cinemas_halls AS hls
		ON
			hls.id=shw.hall_id
		WHERE
			shw.end>='".mysql_real_escape_string(date('Y-m-d'))."'
		ORDER BY
			shw.end ASC
	");

	$result['shows']=array();
	try {
	while ($obj=$_db->fetchAssoc($r))
	{
		$films[$obj['film_id']]=$obj['film_id'];
		$cinemas[$obj['cinema_id']]=$obj['cinema_id'];
		$halls[$obj['hall_id']]=$obj['hall_id'];

		if(sys::isDebugIP())
		 if(!is_numeric($obj['id'])) print("\$obj['id'] = " . $obj['id'] . "\r\n");

		$shows[$obj['id']]=$obj['id']; // very more records!!! Memory allocation fault!!!

		$obj['times']=array();
		$result['shows'][$obj['id']]=$obj;
	}
	} catch(Exception $e) {
		if(sys::isDebugIP()) $_db->printR("Ошибка! Вероятно не хватает памяти.");
	}

	//=======================================================================

	//Премьеры

	$r=$_db->query("
		SELECT id,ukraine_premiere FROM #__main_films WHERE ukraine_premiere>'".date('Y-m-d')."'
	");
	while (list($film_id)=$_db->fetchArray($r))
	{
		$films[$film_id]=$film_id;
	}



	//Часы показов--------------------------------------------------------

	$r=$_db->query("
		SELECT
			tms.time,
			tms.prices,
			tms.3D,
			tms.sale_id,
			tms.sale_status,
			tms_lng.note,
			tms.show_id
		FROM
			#__main_shows_times AS tms
		LEFT JOIN
			#__main_shows_times_lng AS tms_lng
		ON
			tms_lng.record_id=tms.id
		AND
			tms_lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			tms.show_id IN(".implode(',',$shows).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['prices']=str_replace(';',',',$obj['prices']);
		$result['shows'][$obj['show_id']]['times'][]=$obj;
	}
	//=========================================================================

	//Фильмы------------------------------------------------------------------
	$r=$_db->query("
		SELECT
			flm.id,
			lng.title,
			flm.name,
			flm.title_orig,
			flm.duration,
			flm.year,
			flm.ukraine_premiere,
			flm.world_premiere,
			flm.age_limit,
			flm.budget,
			flm.budget_currency,
			lng.intro,
			lng.text,
			flm.sum,
			flm.pre_sum,
			flm.rating,
			flm.pre_rating,
			flm.pre_votes,
			flm.votes,
			flm.pro_rating,
			flm.pro_votes,
			flm.3d
		FROM
			#__main_films AS flm
		LEFT JOIN
			#__main_films_lng AS lng
		ON
			lng.record_id=flm.id
		AND
			lng.lang_id=".$_cfg['sys::lang_id']."
		WHERE
			flm.id IN(".implode(',',$films).")

	");
	$result['films']=array();
	{
		while ($obj=$_db->fetchAssoc($r))
		{
			if ($obj['title_orig']){$urlstr = translit_sourse($obj['title_orig']);}
			else{$urlstr = translit_sourse($obj['name']);}
			$urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

			$obj['title']=htmlspecialchars($obj['title']);
			$obj['title_orig']=htmlspecialchars($obj['title_orig']);
			$obj['photos']=array();
			$obj['trailers']=array();
			$obj['posters']=array();
			$obj['persons']=array();
			$obj['genres']=array();
			$obj['countries']=array();
			$obj['studios']=array();
			$obj['reviews']=array();
			$obj['links']=array();

			$obj['source_url']=_ROOT_URL.'film/'.$urlstr.'-'.$obj['id'].'.phtml';

			if($obj['votes']>5) // только при наличии 5 голосов после премьеры показывать реальный рейтинг
				$obj['rating']=$obj['sum']/$obj['votes'];
			else // до 5 голосов показывать предварительный усредненный рейтинг
				$obj['rating']=($obj['pre_sum']+$obj['sum'])/($obj['pre_votes']+$obg['votes']);

			$obj['votes']=$obj['pre_votes']+$obj['votes'];
			$obj['rating']=main_films::formatFilmRating($obj['rating']);

			$obj['pre_rating']=main_films::formatFilmRating($obj['pre_rating']);

			$result['films'][$obj['id']]=$obj;
		}
	}
	//============================================================================

	//Фотки фильмов-------------------------------------------------------------
	$r=$_db->query("
		SELECT
			film_id,
			image
		FROM
			#__main_films_photos
		WHERE
			film_id IN(".implode(',',$films).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['src']=$_cfg['main::films_url'].$obj['image'];
		$result['films'][$obj['film_id']]['photos'][]=$obj;
	}
	//==========================================================

	//Трейлеры фильмов-------------------------------------------
	$r=$_db->query("
		SELECT
			film_id,
			file,
			url,
			language
		FROM
			#__main_films_trailers
		WHERE
			film_id IN(".implode(',',$films).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		if($obj['file'])
			$obj['url']=$_cfg['main::films_url'].$obj['file'];
		$obj['url']=htmlspecialchars($obj['url']);
		$result['films'][$obj['film_id']]['trailers'][]=$obj;
	}
	//=======================================================================

	//Постеры фильмов-------------------------------------------------
	$r=$_db->query("
		SELECT
			film_id,
			image,
			order_number
		FROM
			#__main_films_posters
		WHERE
			film_id IN(".implode(',',$films).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['src']=$_cfg['main::films_url'].$obj['image'];
		$result['films_posters'][]=$obj;
		$result['films'][$obj['film_id']]['posters'][]=$obj;
	}
	//================================================================

	//Перcоны фильмов------------------------------------------------------
	$r=$_db->query("
		SELECT
			fp.film_id,
			fp.person_id,
			fp.profession_id,
			lng.role
		FROM
			#__main_films_persons AS fp
		LEFT JOIN
			#__main_films_persons_lng AS lng
		ON
			lng.record_id=fp.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			fp.film_id IN(".implode(',',$films).")
	");
	$persons=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$persons[$obj['person_id']]=$obj['person_id'];
		$obj['role']=htmlspecialchars($obj['role']);
		$result['films'][$obj['film_id']]['persons'][]=$obj;
	}
	//==============================================================

	//Жанры фильмов--------------------------------------
	$r=$_db->query("
		SELECT
			film_id,
			genre_id
		FROM
			#__main_films_genres
		WHERE
			film_id IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$result['films'][$obj['film_id']]['genres'][]=$obj['genre_id'];
	}
	//================================================

	//Страны фильмов--------------------------------------
	$r=$_db->query("
		SELECT
			film_id,
			country_id
		FROM
			#__main_films_countries
		WHERE
			film_id IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$result['films'][$obj['film_id']]['countries'][]=$obj['country_id'];
	}
	//================================================

	//Студии фильмов--------------------------------------
	$r=$_db->query("
		SELECT
			film_id,
			studio_id
		FROM
			#__main_films_studios
		WHERE
			film_id IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$result['films'][$obj['film_id']]['studios'][]=$obj['studio_id'];
	}
	//================================================

	//Рецензии на фильмы--------------------------------------
	$r=$_db->query("
		SELECT
			film_id,
			id,
			title
		FROM
			#__main_reviews
		WHERE
			film_id IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_reviews::getReviewUrl($obj['id']);
		$result['films'][$obj['film_id']]['reviews'][]=$obj;
	}
	//================================================

	//ссылки на фильмы--------------------------------------
	$r=$_db->query("
		SELECT
			lnk.film_id,
			lnk.url,
			lng.title
		FROM
			#__main_films_links AS lnk
		LEFT JOIN
			#__main_films_links_lng AS lng
		ON
			lng.record_id=lnk.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			lnk.film_id IN(".implode(',',$films).")
	");
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$result['films'][$obj['film_id']]['links'][]=$obj;
	}
	//================================================

	//Кинотеатры-----------------------------------------------------
	$r=$_db->query("
		SELECT
			cnm.id,
			cnm.city_id,
			lng.title,
			cnm.site,
			cnm.phone,
			lng.address,
			lng.text
		FROM
			#__main_cinemas AS cnm
		LEFT JOIN
			#__main_cinemas_lng AS lng
		ON
			lng.record_id=cnm.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			cnm.id IN(".implode(',',$cinemas).")
	");
	$result['cinemas']=array();
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['address']=htmlspecialchars($obj['address']);
		$obj['phone']=htmlspecialchars($obj['phone']);
		$obj['site']=htmlspecialchars($obj['site']);
		$obj['halls']=array();
		$obj['photos']=array();
		$result['cinemas'][$obj['id']]=$obj;
	}


	//============================================================

	//Фотки кинотеатра------------------------------------------------
	$r=$_db->query("
		SELECT
			cinema_id,
			image
		FROM
			#__main_cinemas_photos
		WHERE
			cinema_id IN(".implode(',',$cinemas).")
	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['src']=$_cfg['main::cinemas_url'].$obj['image'];
		$result['cinemas'][$obj['cinema_id']]['photos'][]=$obj;
	}
	//=============================================================

	//Залы кинотеатра--------------------------------------------
	$r=$_db->query("
		SELECT
			hls.id,
			hls.cinema_id,
			lng.title,
			hls.scheme,
			hls.3d
		FROM
			#__main_cinemas_halls AS hls
		LEFT JOIN
			#__main_cinemas_halls_lng AS lng
		ON
			lng.record_id=hls.id
		AND
			lng.lang_id=".intval($_cfg['sys::lang_id'])."
		WHERE
			hls.id IN(".implode(',',$halls).")

	");
	while ($obj=$_db->fetchAssoc($r))
	{
		$obj['title']=htmlspecialchars($obj['title']);
		if($obj['scheme'])
			$obj['scheme']=$_cfg['main::cinemas_url'].$obj['scheme'];
		$result['cinemas'][$obj['cinema_id']]['halls'][]=$obj;
	}
	//=============================================================

	//Персоны-------------------------------------------------------
	$result['persons']=array();
	if($persons)
	{
		$r=$_db->query("
			SELECT
				prs.id,
				prs.lastname_orig,
				prs.firstname_orig,
				lng.lastname,
				lng.firstname
			FROM
				#__main_persons AS prs
			LEFT JOIN
				#__main_persons_lng AS lng
			ON
				lng.record_id=prs.id
			AND
				lng.lang_id=".intval($_cfg['sys::lang_id'])."
			WHERE
				prs.id IN(".implode(',',$persons).")
		");

		while ($obj=$_db->fetchAssoc($r))
		{
			$obj['lastname']=htmlspecialchars($obj['lastname']);
			$obj['firstname']=htmlspecialchars($obj['firstname']);
			$obj['lastname_orig']=htmlspecialchars($obj['lastname_orig']);
			$obj['firstname_orig']=htmlspecialchars($obj['firstname_orig']);
			$obj['url']=main_persons::getPersonUrl($obj['id']);
			$result['persons'][]=$obj;
		}
	}
	//===============================================================
	sys::setCache($cache_name,serialize($result));

	return $result;
}
?>