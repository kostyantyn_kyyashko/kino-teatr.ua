<?php

function main_user_profile_orders(){
	
	define('_NOINDEX','1');
	sys::useLib('main::users');
	sys::useLib('sys::form');
	sys::useLib('sys::pages');
	sys::useJs('sys::gui');
	sys::useLib('main::orders');

	sys::filterGet('order_by','text','title.asc');
	global $_db, $_cfg, $_err, $_user, $_cookie, $_langs;
	
	if($_user['id']==2)	return 401;
	
	$result['objects']=main_orders::getOrders($_user['id']);
	$result['title']=sys::translate('main::orders');
	$result['pages']=$result['objects']['pages'];
	unset($result['objects']['pages']);

	return $result;
}

?>