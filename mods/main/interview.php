<?
function main_interview()
{
	global $_db, $_cfg, $_err, $_user;
	sys::useLib('main::interview');
	sys::useLib('main::spec_themes');
	sys::useLib('main::users');
	sys::useLib('sys::pages');

	sys::filterGet('month');
	sys::filterGet('year');
	sys::filterGet('interview_id');

	if ($_GET['page']!='')
	{
		define('_NOINDEX',1,true);
	}


		$lang=$_cfg['sys::lang'];

		if ($lang=='ru')
		{
		define('_CANONICAL',_ROOT_URL.'interview.phtml',true);
		} else {
		define('_CANONICAL',_ROOT_URL.$lang.'/interview.phtml',true);
		}

	$cache_name='main_interview_'.implode('_',$_user['groups']).'_'.$_cfg['sys::lang'].'_'.md5(implode('_',$_GET)).'.dat';
	if($cache=sys::getCache($cache_name,$_cfg['main::interview_page_cache_period']*60))
		return unserialize($cache);

	/*if($_GET['year']<2000 || $_GET['year']>intval(date('Y')) || $_GET['month']<1 || $_GET['month']>12)
		return 404;*/


	if($_GET['interview_id'])
		$result['section']=$meta['section']=$_db->getValue('main_interview','title',intval($_GET['interview_id']),true);
	else
		$result['section']=$meta['section']=sys::translate('main::all_sections');

	if(!$result['section'])
		return 404;

	if(isset($_POST['_task']) && $_POST['_task'])
	{
		$task=explode('.',$_POST['_task']);
		if($task[0]=='delete' && sys::checkAccess('main::interview_delete'))
			main_interview::deleteArticle($task[1]);
	}

	$result['date']=$meta['date']=mb_strtolower(sys::translate('main::month_'.intval($_GET['month']))).' '.$_GET['year'];
	if ($_GET['page'])
	{
		$meta['page'] = sys::translate('main::page').' '.$_GET['page'];
	} else {
		$meta['page'] = '';
	}

	$result['meta']=sys::parseModTpl('main::interview','page',$meta);
	$result['title']=sys::translate('main::interview_cinema');
	$month_begin=date('Y-m-d H:i:s',main_interview::getMonthBegin($_GET['month'],$_GET['year']));
	$month_end=date('Y-m-d H:i:s',main_interview::getMonthEnd($_GET['month'],$_GET['year']));
	$q="
		SELECT
			art.id,
			art.date,
			art.total_shows,
			art.comments,
			art.user_id,
			art.exclusive,
			spec.spec_theme_id as spec_theme,
			art.small_image AS `image`,
			art.image AS `big_image`,
			art_lng.title,
			art_lng.intro,
			usr.login AS `user`,
			usr_data.main_nick_name
		FROM `#__main_interview_articles` AS `art`

		LEFT JOIN
			`#__main_interview_articles_lng` AS `art_lng`
		ON
			art_lng.record_id=art.id
		AND
			art_lng.lang_id=".intval($_cfg['sys::lang_id'])."

		LEFT JOIN `#__main_interview` AS `nws`
		ON art.interview_id=nws.id

		LEFT JOIN `#__main_interview_articles_spec_themes` AS `spec`
		ON art.id=spec.article_id
		
		LEFT JOIN
			`#__sys_users` AS `usr`
		ON
			usr.id=art.user_id

		LEFT JOIN
			`#__sys_users_data` AS `usr_data`
		ON
			usr.id=usr_data.user_id
			
		WHERE
			/*art.date BETWEEN '".$month_begin."' AND '".$month_end."'
			AND */(art.city_id=".intval($_cfg['main::city_id'])." OR art.city_id=0)
			AND art.public=1
			AND art.date<'".gmdate('Y-m-d H:i:s')."'
			AND nws.showtab=1
	";
	if($_GET['interview_id'])
	{
		$q.="
			AND art.interview_id=".intval($_GET['interview_id'])."
		";
	}

	if($_REQUEST['tag'] && $_cfg['sys::lang_id']=='1')
	{
		$q.="
			AND (
				art.tags LIKE '%".$_REQUEST['tag']."%'
			OR
				art.tags LIKE '%".$_REQUEST['tag']."'
			OR
				art.tags LIKE '".$_REQUEST['tag']."%'
			)
		";

	$result['tag']=1;
	}

	if($_REQUEST['tag'] && $_cfg['sys::lang_id']=='3') {
		$q.="
			AND (
				art.tags_ua LIKE '%".$_REQUEST['tag']."%'
			OR
				art.tags_ua LIKE '%".$_REQUEST['tag']."'
			OR
				art.tags_ua LIKE '".$_REQUEST['tag']."%'
			)
		";

	$result['tag']=1;
	}
	$q.="
		ORDER BY art.date DESC
	";

	$result['pages']=sys_pages::pocess($q,$_cfg['main::interview_on_page']);
	$r=$_db->query($q.$result['pages']['limit']);
	$result['objects']=array();
	while($obj=$_db->fetchAssoc($r))
	{
		$obj['date']=sys::russianDate($obj['date']);
		$obj['shows']=$obj['total_shows'];
		$obj['user_url']=main_users::getUserUrl($obj['user_id']);

// inserted by Mike begin
  	    $image="x1_".$obj['id'].".jpg";
		if(!file_exists('./public/main/interview/'.$image)) $image = $_cfg['sys::root_url'].'blank_news_img.jpg';
		 else $image=$_cfg['main::interview_url'].$image;
		$obj['image'] = $image;
// inserted by Mike end
		
/*		// comments by Mike
		if($obj['image'])
			$image=$_cfg['main::interview_url'].$obj['image'];

		list($wid, $hei, $type) = getimagesize('/var/www/html/multiplex/multiplex.in.ua/'.str_replace(_ROOT_URL,'',$image));


		if ($wid<213 && $obj['big_image'])
		{
		 	$image = $_cfg['main::interview_url'].$obj['big_image'];
		} else if ($wid>=213)
		{
		 	$image = $_cfg['main::interview_url'].$obj['image'];
		} else {
         	$image = 'blank_news_img.jpg';
		}

		$obj['image'] = _ROOT_URL.'public/main/rescrop.php?f='.str_replace(_ROOT_URL,'',$image).'&t=1';
*/		
		$obj['title']=htmlspecialchars($obj['title']);
		$obj['url']=main_interview::getArticleUrl($obj['id']);
		$obj['spec_theme_url']=main_spec_themes::getArticleUrl($obj['spec_theme']);

		if(sys::checkAccess('main::interview_edit') || $_user['id']==$obj['user_id'])
			$obj['edit_url']=main_interview::getArticleEditUrl($obj['id']);
		else
			$obj['edit_url']=false;

		if(sys::checkAccess('main::interview_delete') || $_user['id']==$obj['user_id'])
			$obj['delete']=true;
		else
			$obj['delete']=false;
		$result['objects'][]=$obj;
	}

	if(!$result['objects']/* && !main_interview::interviewReirect($_GET['year'],$_GET['month'],$_GET['interview_id'])*/)
		return 404;

	if(sys::checkAccess('main::interview'))
		$result['add_url']=main_interview::getArticleAddUrl();
	else
		$result['add_url']=false;

		$result['all_news']=sys::rewriteUrl('?mod=main&act=interview');
		$result['popular_news']=sys::rewriteUrl('?mod=main&act=popular_interview');
		$result['discussed_news']=sys::rewriteUrl('?mod=main&act=discussed_interview');

	sys::setCache($cache_name,serialize($result));
	return $result;
}
?>